# FUKURO ROBOT WORKSPACE #

## [WIKI HOMEPAGE click here](https://bitbucket.org/fukuro_ugm/fukuro_ws/wiki/Home) ##

Software robot fukuro. Vision, Control, Strategy, etc.

### Dependencies ###

* [ROS Kinetic](http://www.ros.org/)
* [Qt (with ROS Plugin)](https://github.com/ros-industrial/ros_qtc_plugin/wiki)
* [GCC 7](https://gist.github.com/jlblancoc/99521194aba975286c80f93e47966dc5)
* [Tensorflow 1.12.0](https://www.tensorflow.org/install/)
* [CUDA 10.0](https://developer.nvidia.com/cuda-downloads)
* [cuDNN 7.4](https://developer.nvidia.com/cudnn)
* [Protocol Buffer 3.6.1](https://askubuntu.com/questions/1072683/how-can-i-install-protoc-on-ubuntu-16-04)
* [OpenCV](https://opencv.org/)

### How to Install? ###

~~~~
$ git clone https://username@bitbucket.org/fukuro_ugm/fukuro_ws.git
$ cd fukuro_ws/src
$ catkin_init_workspace
$ cd ..
$ catkin_make
~~~~

### Contribution guidelines ###

* disarankan dengan [pull request](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html) 
* [slack](https://fukurowl.slack.com/signup)
* ignore folder devel & build

### Useful Links ###

* [git tutorial](https://www.atlassian.com/git/tutorials)
* package ROS menggunakan [CMake](https://cmake.org/)
* syntax [markdown](https://bitbucket.org/tutorials/markdowndemo) (untuk readme.md)
