#ifndef DIALOG_H
#define DIALOG_H

#include <QTimer>
#include <QDialog>
#include <QGraphicsItem>
#include <ros/ros.h>
#include "fukuro_common/Localization.h"
#include "fukuro_common/RobotControl.h"
#include "fukuro_common/LocalizationService.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
  Q_OBJECT

public:
  explicit Dialog(ros::NodeHandle& node_, QWidget *parent = 0);
  void updateLoc(const fukuro_common::Localization::ConstPtr& loc);
  void publish();
  ~Dialog();


private:
  class Field : public QGraphicsItem
  {
  public:
    Field(fukuro_common::RobotControl& msg_);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

  private:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

  private:
    QVector<QLineF> lines;
    QVector<QLineF> circle_lines;
    QRectF center_circle;
    QPointF click;
    fukuro_common::RobotControl& ctrl_msg;
  };

  class Robot : public QGraphicsItem
  {
  public:
    Robot();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setRobotPose(QPointF pos_, double angle_);
    QRectF boundingRect() const;
  private:
    QPointF robot_pos;
    double angle;
  };

private:
  Ui::Dialog *ui;
  ros::NodeHandle& node;
  ros::Publisher robot_control_pub;
  Field *field;
  Robot *robot;
  fukuro_common::RobotControl robot_ctrl_msg;
  ros::ServiceClient loc_client;
  QTimer *timer;
};

#endif // DIALOG_H
