#include "dialog.h"
#include "ui_dialog.h"
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

#define ANGLE_INCREMENT 3

#define FIELD_WIDTH 900
#define FIELD_HEIGHT 600

#define XLINE1 450
#define XLINE2 XLINE1-38
#define XLINE3 XLINE1-113
#define XLINE4 0
#define XLINE5 -(XLINE3)
#define XLINE6 -(XLINE2)
#define XLINE7 -(XLINE1)

#define YLINE1 300
#define YLINE2 213
#define YLINE3 138
#define YLINE4 -(YLINE3)
#define YLINE5 -(YLINE2)
#define YLINE6 -(YLINE1)

#define CENTER_RADIUS 100

Dialog::Dialog(ros::NodeHandle &node_, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Dialog),
  node(node_),
  robot(new Robot),
  timer(new QTimer(this))
{
  ui->setupUi(this);

  field = new Field(robot_ctrl_msg);

  ui->graphicsView->setScene(new QGraphicsScene(-FIELD_WIDTH/2-100,-FIELD_HEIGHT/2-100,FIELD_WIDTH+200,FIELD_HEIGHT+200,this));
  ui->graphicsView->scene()->addItem(field);
  ui->graphicsView->scene()->addItem(robot);
  ui->graphicsView->setRenderHint(QPainter::Antialiasing);
  ui->graphicsView->scale(0.85,0.85);
  this->setFixedSize(this->size()+QSize(50,50));
  this->setWindowTitle("Fukuro Robot Control Tuning");

  std::string robot_name;
  if(!ros::get_environment_variable(robot_name,"FUKURO"))
  {
    ROS_ERROR("robot name empty");
    exit(-1);
  }

  robot_control_pub = node.advertise<fukuro_common::RobotControl>(robot_name+"/robot_control",10);
  loc_client = node.serviceClient<fukuro_common::LocalizationService>("/localization_service");

  connect(ui->dribbler_chekbox,&QCheckBox::toggled,[=](bool checked)
  {
    robot_ctrl_msg.dribbler = checked ? 1 : 0;
  });
  connect(ui->reset_btn,&QPushButton::clicked,[=]
  {
    fukuro_common::LocalizationService loc;
    loc.request.x = ui->x_spinbox->value();
    loc.request.y = ui->y_spinbox->value();
    loc.request.w = ui->w_spinbox->value();
    loc.request.initial_pos = 1;
    loc_client.call(loc);
  });
  connect(timer,&QTimer::timeout,[=]
  {
    publish();
  });
  timer->start(500);
}

void Dialog::updateLoc(const fukuro_common::Localization::ConstPtr &loc)
{
  robot->setRobotPose(QPointF(loc->belief.x,-loc->belief.y),-loc->belief.theta);
  robot->update();
}

void Dialog::publish()
{
  robot_control_pub.publish(robot_ctrl_msg);
}

Dialog::~Dialog()
{
  delete ui;
}

Dialog::Field::Field(fukuro_common::RobotControl &msg_) :
  ctrl_msg(msg_)
{
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE7,YLINE1)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE6),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE1,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE7,YLINE1),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE4,YLINE1),
                         QPointF(XLINE4,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE2,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE6,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE3,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE5,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE1,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE4),
                         QPointF(XLINE1,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE1,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE5),
                         QPointF(XLINE1,YLINE5)));

  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE7,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE4),
                         QPointF(XLINE7,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE7,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE5),
                         QPointF(XLINE7,YLINE5)));

  center_circle.setLeft(-CENTER_RADIUS);
  center_circle.setTop(-CENTER_RADIUS);
  center_circle.setHeight(2*CENTER_RADIUS);
  center_circle.setWidth(2*CENTER_RADIUS);

  QPointF circle_line0(center_circle.width()/2,0.0);
  QLineF line(QPointF(0.0,0.0),circle_line0);
  circle_lines.push_back(QLineF(circle_line0,line.p2()));
  for(int i=ANGLE_INCREMENT; i<=360; i+=ANGLE_INCREMENT)
  {
      line.setAngle(i);
      QPointF p0 = circle_lines.back().p2();
      circle_lines.push_back(QLineF(p0,line.p2()));
  }
}

void Dialog::Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::transparent);
  painter->setBrush(Qt::green);
  painter->drawRect(boundingRect());
  painter->setPen(QPen(Qt::white,3.3));
  painter->setBrush(Qt::transparent);
  painter->drawLines(lines);
  painter->drawEllipse(center_circle);
}

QRectF Dialog::Field::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

void Dialog::Field::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  click = event->pos();
}

void Dialog::Field::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
  QPointF release = event->pos();
  double angle = QLineF(click,release).angle();
  ctrl_msg.target_pose.x = click.x()/100.0;
  ctrl_msg.target_pose.y = -click.y()/100.0;
  if(angle>180.0)
    angle -= 360.0;
  ctrl_msg.target_pose.theta = angle*M_PI/180.0;
  qDebug() << "mouse release :" << ctrl_msg.target_pose.x << ctrl_msg.target_pose.y << ctrl_msg.target_pose.theta;
}

Dialog::Robot::Robot()
{

}

void Dialog::Robot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->translate(robot_pos);
  painter->rotate(angle);
  painter->drawEllipse(QPointF(0.0,0.0),25,25);
  painter->drawLine(QPointF(0.0,0.0),QPointF(30.0,0.0));
}

void Dialog::Robot::setRobotPose(QPointF pos_, double angle_)
{
  robot_pos = pos_;
  angle = angle_;
}

QRectF Dialog::Robot::boundingRect() const
{
  QRectF ret(QPointF(-50,-50),QPointF(50,50));
  ret.moveCenter(robot_pos);
//  return ret;
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}
