#include <ros/ros.h>
#include <QApplication>
#include "dialog.h"
#include "fukuro_common/Localization.h"
#include "fukuro_common/RobotControl.h"

int main(int argc, char** argv)
{
  QApplication appt(argc,argv);
  ros::init(argc,argv,"fukuro_robot_control_test");
  ros::NodeHandle node;
  Dialog dialog(node);
  ros::Subscriber loc_sub = node.subscribe("/localization",2,&Dialog::updateLoc,&dialog);
  ros::AsyncSpinner spinner(2);
  dialog.show();
  spinner.start();
  appt.exec();
  return 0;
}
