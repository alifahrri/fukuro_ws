#ifndef IMAGETRANSPORTER_H
#define IMAGETRANSPORTER_H

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>

class ImageTransporter
{
public:
  ImageTransporter(ros::NodeHandle& n) :
    it(n)
  {
    sub = it.subscribe("usb_cam/image_raw",1,&ImageTransporter::image_callback,this);
  }

  cv::Mat getImage() { return last_image; }
private:
  void image_callback(const sensor_msgs::ImageConstPtr& msg)
  {
    try {
      last_image = cv_bridge::toCvCopy(msg,"bgr8")->image;
    }
    catch (cv_bridge::Exception& e){
      ROS_ERROR(e.what());
    }
  }

private:
  image_transport::ImageTransport it;
  image_transport::Subscriber sub;
  cv::Mat last_image;
};

#endif // IMAGETRANSPORTER_H
