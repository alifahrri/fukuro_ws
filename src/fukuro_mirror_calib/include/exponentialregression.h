#ifndef EXPONENTIALREGRESSION_H
#define EXPONENTIALREGRESSION_H

#include <cmath>
#include <vector>

class ExpRegression
{
public:
  typedef std::pair<double,double> Data;
  typedef std::vector<Data> DataList;
  ExpRegression();
  ExpRegression(DataList data_);
  double getA();
  double getB();
  void compute();
  void compute(DataList data_);
  double at(double key);
private:
  DataList data;
  double A;
  double B;
};

#endif // EXPONENTIALREGRESSION_H
