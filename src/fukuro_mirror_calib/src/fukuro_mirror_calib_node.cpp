#include <ros/ros.h>
#include <QApplication>
#include <fukuro_mirror_calib/imagetransporter.h>
#include "dialog.h"

using ros::AsyncSpinner;

int main(int argc, char **argv)
{
  QApplication app(argc,argv);
  ros::init(argc,argv,"fukuro_mirror_calib");
  ros::NodeHandle n;

  AsyncSpinner spinner(1);
  ImageTransporter transporter(n);
  Dialog dialog(&transporter);
  dialog.showMaximized();
  if(spinner.canStart())
    spinner.start();
  app.exec();
  return 0;
}
