#include "exponentialregression.h"
#include <cstdlib>

ExpRegression::ExpRegression()
{

}

ExpRegression::ExpRegression(ExpRegression::DataList data_) :
  data(data_)
{

}

double ExpRegression::getA()
{
  return A;
}

double ExpRegression::getB()
{
  return B;
}

void ExpRegression::compute()
{
  double a;
  double b;
  double term1(0.0), term2(0.0), term3(0.0), term4(0.0), term5(0.0);
  for(size_t i=0; i<data.size(); i++)
  {
    double x = data[i].first;
    double y = data[i].second;
    term1 += (x*x*y);
    term2 += (y*std::log(y));
    term3 += (x*y);
    term4 += (x*y*std::log(y));
    term5 += (y);
  }
  a = (term1*term2-term3*term4)/(term5*term1-term3*term3);
  b = (term5*term4-term3*term2)/(term5*term1-term3*term3);
  A = std::exp(a);
  B = b;
}

void ExpRegression::compute(ExpRegression::DataList data_)
{
  data = data_;
  compute();
}

double ExpRegression::at(double key)
{
  return (A*std::exp(B*key));
}
