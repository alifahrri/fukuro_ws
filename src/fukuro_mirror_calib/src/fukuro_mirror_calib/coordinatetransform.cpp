#include "coordinatetransform.h"
#include <QSettings>
#include <iostream>

CoordinateTransform::CoordinateTransform()
{

}

CoordinateTransform::CoordinateTransform(CoordinateTransform::ParameterList param_list) :
  parameter_list(param_list)
{

}

void CoordinateTransform::add(double key, CoordinateTransform::Parameter param)
{
  parameter_list[key] = param;
}

void CoordinateTransform::save(std::__cxx11::string ini_file)
{
  QSettings settings(QString::fromStdString(ini_file),QSettings::IniFormat);
  auto keys = settings.allKeys();
  for(auto key : keys)
    settings.remove(key);
  int key_count = parameter_list.size();
  int i = 0;
  settings.setValue(QString("Size"),key_count);
  for(auto it = parameter_list.begin(); it!=(parameter_list.end()); it++)
  {
    settings.beginGroup(QString("Parameter%1").arg(i++));
    settings.setValue(QString("Angle"),it->first);
    settings.setValue(QString("A"),it->second.first);
    settings.setValue(QString("B"),it->second.second);
    settings.endGroup();
  }
}

void CoordinateTransform::load(std::__cxx11::string ini_file)
{
  QSettings settings(QString::fromStdString(ini_file),QSettings::IniFormat);
  int key_count = settings.value(QString("Size")).toInt();
  for(int i=0; i<key_count; i++)
  {
    settings.beginGroup(QString("Parameter%1").arg(i));
    double angle = settings.value(QString("Angle")).toDouble();
    double A = settings.value(QString("A")).toDouble();
    double B = settings.value(QString("B")).toDouble();
    parameter_list[angle] = Parameter(A,B);
    settings.endGroup();
  }
}

void CoordinateTransform::reset()
{
  parameter_list.clear();
}

bool CoordinateTransform::hasParamAt(double key)
{
  return parameter_list.find(key)!=parameter_list.end();
}

CoordinateTransform::Parameter CoordinateTransform::getParam(double key)
{
  Parameter ret;
  if(parameter_list.find(key)!=parameter_list.end())
    ret = parameter_list[key];
  return ret;
}

CoordinateTransform::ParameterList CoordinateTransform::getParamList()
{
  return parameter_list;
}

double CoordinateTransform::getValue(double angle, double radius)
{
  double radius_m(0.0);
  double A(0.0);
  double B(0.0);
  if(!parameter_list.size())
  {
    std::cout << "parameter empty!!!\n";
    radius_m = 0.0;
  }
  else if(parameter_list.size()==1)
  {
    A = parameter_list.begin()->second.first;
    B = parameter_list.begin()->second.second;
    radius_m = A*std::exp(B*radius);
  }
  else
  {
//    if(parameter_list.find(angle)!=parameter_list.end())
//    {
//      A = parameter_list[angle].first;
//      B = parameter_list[angle].second;
//      radius_m = A*std::exp(B*radius);
//    }
//    else
    {
      double angle0(0.0), angle1(0.0);
      double A0(0.0), A1(0.0), B0(0.0), B1(0.0);
      double d_angle(0.0);
      double _angle(0.0);
      if(angle<parameter_list.begin()->first)
      {
        angle0 = (--parameter_list.end())->first;
        angle1 = parameter_list.begin()->first;
        _angle = fabs(360.0-angle0) + angle;
        d_angle = fabs(360.0-angle0) + angle1;
      }
      else if(angle>(--parameter_list.end())->first)
      {
        angle0 = (--parameter_list.end())->first;
        angle1 = parameter_list.begin()->first;
        _angle = angle-angle0;
        d_angle = fabs(360.0-angle0) + angle1;
      }
      else
        for(auto it=parameter_list.begin(); it!=(--parameter_list.end()); it++)
          if(angle>=it->first)
          {
            angle0 = it->first;
            auto it1 = it;
            it1++;
            angle1 = it1->first;
            _angle = fabs(angle-angle0);
            d_angle = fabs(angle1-angle0);
          }

      A0 = parameter_list[angle0].first;
      A1 = parameter_list[angle1].first;
      B0 = parameter_list[angle0].second;
      B1 = parameter_list[angle1].second;
//      double da = (angle0 > angle1) ? fabs(angle-angle1) : fabs(angle-angle0);
//      A = A0 + da * (A1-A0) / fabs(angle1-angle0);
//      B = B0 + da * (B1-B0) / fabs(angle1-angle0);
      A = A0 + _angle * (A1-A0) / d_angle;
      B = B0 + _angle * (B1-B0) / d_angle;
      /*
      std::cout << "A0 : " << A0 << " B0 : " << B0
                << " A1 : " << A1 << " B1 : " << B1
                << " A : " << A
                << " B : " << B
                << " angle0 : " << angle0
                << " angle1 : " << angle1
                << " angle : " << angle
                << '\n';
                */
      radius_m = A*std::exp(B*radius);
    }
  }
  return radius_m;
}

