#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QPoint>
#include "fukuro_mirror_calib/imagetransporter.h"
#include "transformdialog.h"
#include "qcustomplot.h"
#include "exponentialregression.h"
#include "coordinatetransform.h"
#include "roiitem.h"

namespace Ui {
class Dialog;
}

struct Point
{
  Point() { key = 0.0; value = 0.0; }
  Point(double key_, double value_) { key = key_; value = value_; }
  double key;
  double value;
};

inline bool operator <(const Point& left, const Point& right)
{
    return left.key < right.key;
}

typedef std::set<Point> PointList;
typedef std::map<double,PointList> TestPoint;

class Dialog : public QDialog
{
  Q_OBJECT

public:
  explicit Dialog(ImageTransporter* im_trans, QWidget *parent = 0);
  ~Dialog();
private slots:
  void load_image();
  void load_result();
  void save_result();
  void transform_image();
  void add_test_point();
  void remove_test_point();
  void compute_regression();
  void reset();
  void show_test_point(QString text);
private:
  void set_test_point(double angle, double radius);
  void show_result(double angle);
  QPointF transform_point(int x, int y);
  QPointF to_point(double angle, double radius);
private:
  Ui::Dialog *ui;
  TransformDialog tf_dialog;
  ImageTransporter *transporter;
  std::string calibration_result_dir;
  QPoint roi_center;
  int roi_radius;
  bool result_loaded;
  ROIItem roi;
  QGraphicsPixmapItem *image_pixmap;
  TestPoint test_point;
  QCPGraph* test_point_graph;
  QCPGraph* exponent_graph;
  CoordinateTransform transform;
};

#endif // DIALOG_H
