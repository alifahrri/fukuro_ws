#ifndef ROIITEM_H
#define ROIITEM_H

#include <QGraphicsItem>
#include <functional>
#include <cmath>

typedef std::function<void(double,double)> TestPointCallback;
typedef std::function<void(double)> TestResultCallback;

class ROIItem : public QGraphicsItem
{
public:
  ROIItem();
  void setROI(QPoint center_, double radius_);
  void setTestPoint(double angle_, double radius_=0.0);
  void setTestPointCallback(TestPointCallback cb) { tp_cb = cb; }
  void setTestResultCallback(TestResultCallback cb) { tr_cb = cb; }
  double getTestPointRadius() { return r; }
  double getTestPointAngle() { return angle; }
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  QRectF boundingRect() const;
private:
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
private:
  QPoint center;
  double radius;
  double angle;
  double r;
  QLineF line;
  QPointF test_point;
  TestPointCallback tp_cb;
  TestResultCallback tr_cb;
  bool left_click;
};

#endif // ROIITEM_H
