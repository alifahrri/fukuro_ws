#include "dialog.h"
#include "ui_dialog.h"
#include <cstdlib>
#include <QRgb>
#include <QGraphicsScene>
#include <QDebug>

#define IMAGE_WIDTH 640
#define IMAGE_HEIGHT 480
#define CALIBRATION_PATH "/home/fahri/fukuro_ws/src/omni_vision/calib_results"
#define TRANSFORM_IMAGE_SIZE 800
#define TRANSFORM_RATIO 50 //100 px = 1 m

Dialog::Dialog(ImageTransporter *im_trans, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Dialog),
  transporter(im_trans),
  calibration_result_dir(CALIBRATION_PATH),
  result_loaded(false),
  image_pixmap(new QGraphicsPixmapItem)
{
  setWindowTitle("Fukuro Mirror Calibration");
  ui->setupUi(this);
  ui->image_view->setScene(new QGraphicsScene(0,0,IMAGE_WIDTH,IMAGE_HEIGHT,this));
  ui->image_view->scene()->addItem(image_pixmap);
  ui->image_view->scene()->addItem(&roi);

  connect(ui->update_img_btn,SIGNAL(clicked(bool)),this,SLOT(load_image()));
  connect(ui->load_result_btn,SIGNAL(clicked(bool)),this,SLOT(load_result()));
  connect(ui->save_btn,SIGNAL(clicked(bool)),this,SLOT(save_result()));
  connect(ui->add_btn,SIGNAL(clicked(bool)),this,SLOT(add_test_point()));
  connect(ui->remove_btn,SIGNAL(clicked(bool)),this,SLOT(remove_test_point()));
  connect(ui->apply_btn,SIGNAL(clicked(bool)),this,SLOT(compute_regression()));
  connect(ui->transform_btn,SIGNAL(clicked(bool)),this,SLOT(transform_image()));
  connect(ui->angle_combo_box,SIGNAL(currentTextChanged(QString)),this,SLOT(show_test_point(QString)));
  connect(ui->reset_btn,SIGNAL(clicked(bool)),this,SLOT(reset()));

  ui->image_view->setRenderHints(QPainter::Antialiasing |
                                 QPainter::SmoothPixmapTransform |
                                 QPainter::HighQualityAntialiasing );

  test_point_graph = ui->plotter->addGraph();
  test_point_graph->setBrush(Qt::red);
  test_point_graph->setLineStyle(QCPGraph::lsNone);
  test_point_graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,Qt::red,6.0));
  exponent_graph = ui->plotter->addGraph();

  roi.setTestPointCallback([&](double angle, double radius)
  {
    this->set_test_point(angle,radius);
  });
  roi.setTestResultCallback([&](double angle)
  {
    this->show_result(angle);
  });
  calibration_result_dir = std::string("/home/") +
                           std::getenv("USER") +
                           std::string("/src/omni_vision/calib_results");

  setWindowTitle("Fukuro Mirror Calibration");
}

Dialog::~Dialog()
{
  delete ui;
}

void Dialog::set_test_point(double angle, double radius)
{
  ui->doubleSpinBox->setValue(angle);
  ui->doubleSpinBox_2->setValue(radius);
}

void Dialog::show_result(double angle)
{
  static double y_max(0.0);
  test_point_graph->clearData();
  exponent_graph->clearData();
  for(int i=0; i<(int)(0.9*roi_radius); i++)
  {
    double value = transform.getValue(angle,(double)i);
    exponent_graph->addData(i,value);
    if(value > y_max)
      y_max = value;
  }
  ui->plotter->rescaleAxes(true);
  ui->plotter->yAxis->setRangeUpper(y_max);
  ui->plotter->replot();
}

void Dialog::reset()
{
  transform.reset();
  ui->angle_combo_box->clear();
  test_point.clear();
  ui->tableWidget->clear();
}

void Dialog::add_test_point()
{
  PointList& p = test_point[ui->doubleSpinBox->value()];
  ui->angle_combo_box->blockSignals(true);
  ui->angle_combo_box->clear();
  for(auto it = test_point.begin(); it != test_point.end(); it++)
    ui->angle_combo_box->addItem(QString::number(it->first));
  p.insert(Point(ui->doubleSpinBox_2->value(),ui->doubleSpinBox_3->value()));
  ui->angle_combo_box->blockSignals(false);
  ui->angle_combo_box->setCurrentText(QString::number(ui->doubleSpinBox->value()));
  show_test_point(QString::number(ui->doubleSpinBox->value()));
}

void Dialog::remove_test_point()
{
  double key = ui->doubleSpinBox->value();
  if(test_point.find(key)!=test_point.end())
  {
    int row = ui->tableWidget->currentRow();
    if(row>=0)
    {
      double px = ui->tableWidget->item(row,0)->data(0).toDouble();
      double mt = ui->tableWidget->item(row,1)->data(0).toDouble();
      PointList& t = test_point[key];
      t.erase(Point(px,mt));
      show_test_point(QString::number(key));
    }
  }
}

void Dialog::compute_regression()
{
  ExpRegression::DataList data_list;
  for(size_t i=0; i<ui->tableWidget->rowCount(); i++)
  {
    double key = ui->tableWidget->item(i,0)->data(0).toDouble();
    double value = ui->tableWidget->item(i,1)->data(0).toDouble();
    ExpRegression::Data data(key,value);
    data_list.push_back(data);
  }
  ExpRegression reg(data_list);
  reg.compute();
  double angle = ui->angle_combo_box->currentText().toDouble();
  CoordinateTransform::Parameter param(reg.getA(),reg.getB());
  transform.add(angle,param);
  exponent_graph->clearData();
  for(int i=0; i<(int)(data_list.back().first); i++)
    exponent_graph->addData(i,transform.getValue(angle,(double)i));
  ui->plotter->rescaleAxes(true);
  ui->plotter->replot();
}

void Dialog::show_test_point(QString text)
{
  double key = text.toDouble();
  if(test_point.find(key)!=test_point.end())
  {
    PointList& p = test_point[key];
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(2);
    ui->tableWidget->setRowCount(p.size());
    test_point_graph->clearData();
    int index = 0;
    for(auto i : p)
    {
      test_point_graph->addData(i.key,i.value);
      ui->tableWidget->setItem(index,0,new QTableWidgetItem(QString::number(i.key)));
      ui->tableWidget->setItem(index,1,new QTableWidgetItem(QString::number(i.value)));
      index++;
    }
    ui->plotter->rescaleAxes(true);
    ui->plotter->replot();
    roi.setTestPoint(key);
    if(ui->doubleSpinBox->value()!=key)
    {
      ui->doubleSpinBox->setValue(key);
      ui->doubleSpinBox_2->setValue(roi.getTestPointRadius());
    }
    exponent_graph->clearData();
    if(transform.hasParamAt(key))
    {
      for(int i=0; i<(int)(0.9*roi_radius); i++)
        exponent_graph->addData(i,transform.getValue(key,(double)i));
      ui->plotter->rescaleAxes(true);
      ui->plotter->replot();
    }
  }
}

void Dialog::load_image()
{
  if(transporter->getImage().empty())
    return;
  cv::Mat mat;
  cv::cvtColor(transporter->getImage(),mat,CV_BGR2RGB);
  QImage image((const unsigned char*)(mat.data),
               mat.cols,mat.rows,
               mat.cols*mat.channels(),
               QImage::Format_RGB888);
  image_pixmap->setPixmap(QPixmap::fromImage(image));
  image_pixmap->setX(0);
  image_pixmap->setY(0);
  roi.setEnabled(true);
  roi.setVisible(true);
  ui->image_view->scene()->update(-IMAGE_WIDTH/2,-IMAGE_HEIGHT/2,IMAGE_WIDTH*2,IMAGE_HEIGHT*2);
}

void Dialog::load_result()
{
  std::string str_tmp =  calibration_result_dir+ "/";
  QString initial_path = QString::fromStdString(str_tmp);
  QFileDialog FileDialog;
  QString path = FileDialog.getExistingDirectory(this, tr("Open Calibration Files"),
                                                 initial_path,
                                                 QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
  calibration_result_dir = path.toStdString();
  std::string file_path  = calibration_result_dir + "/ROI.xml";
  cv::FileStorage roi_file(file_path,cv::FileStorage::READ);
  int tmp1,tmp2;
  roi_file["center_point_row"]>>tmp2;
  roi_file["center_point_column"]>>tmp1;
  roi_center = QPoint(tmp1,tmp2);
  roi_file["big_radius"] >> roi_radius;
  roi.setROI(roi_center,roi_radius);
  roi_file.release();
  result_loaded = true;
  transform.load(calibration_result_dir + "/fukuro_mirror_calibration.ini");
  ui->angle_combo_box->clear();
  ui->tableWidget->clear();
  test_point.clear();
  auto param_list = transform.getParamList();
  for(const auto& p : param_list)
  {
    ui->angle_combo_box->addItem(QString::number(p.first));
    PointList p_list;
    for(int i=1; i<30; i+=2)
    {
      p_list.insert(Point(i*10,transform.getValue(p.first,i*10)));
    }
    test_point[p.first] = p_list;
  }
}

void Dialog::save_result()
{
  transform.save(calibration_result_dir+"/fukuro_mirror_calibration.ini");
}

inline
QPointF Dialog::to_point(double angle, double radius)
{
  double radius_m = transform.getValue(angle,radius);
  QLineF line(0.0,0.0,radius_m,0.0);
  line.setLength(radius_m*TRANSFORM_RATIO);
  line.setAngle(angle);
//  qDebug() << "radius :" << radius_m << "line :" << line;
  return line.p2();
}

inline
QPointF Dialog::transform_point(int x, int y)
{
  QPoint p(x,y);
  QLineF line(roi_center.x(),roi_center.y(),p.x(),p.y());
  double angle = line.angle();
  double radius = line.length();
  QPointF ret = to_point(angle,radius);
//  qDebug() << "transform point :" << ret << "angle,radius :" << angle << "," <<radius;
  return ret;
//  return to_point(angle,radius);
}

void Dialog::transform_image()
{
  QImage image(image_pixmap->pixmap().toImage());
  QImage transformed(QSize(TRANSFORM_IMAGE_SIZE,TRANSFORM_IMAGE_SIZE),image.format());
  transformed.fill(Qt::black);
  QImage transform_function(IMAGE_WIDTH,IMAGE_HEIGHT,QImage::Format_Grayscale8);
  QPoint transform_center(TRANSFORM_IMAGE_SIZE/2,TRANSFORM_IMAGE_SIZE/2);
  for(int i=0; i<image.size().width(); i++)
    for(int j=0; j<image.size().height(); j++)
    {
      QPointF point_m(transform_point(i,j));
      double radius_m = QLineF(QPointF(0.0,0.0),point_m).length();
      point_m += transform_center;
      if(point_m.x()>0 && point_m.y()>0)
        if((point_m.x())<=TRANSFORM_IMAGE_SIZE && (point_m.y())<=TRANSFORM_IMAGE_SIZE)
        {
          QPoint t_point(point_m.x(), point_m.y());
          transformed.setPixel(t_point,image.pixel(i,j));
        }
      /*
      if(QLineF(roi_center,QPoint(i,j)).length()<roi_radius)
      {
        int value = abs((radius_m/300.0)*255);
        int color = (value > 255) ? 255 : value;
        transform_function.setPixel(i,j,qRgb(color,color,color));
      }
      */
    }

  image_pixmap->setPixmap(QPixmap::fromImage(transformed.scaledToWidth(IMAGE_WIDTH,Qt::SmoothTransformation)));
  image_pixmap->setX((IMAGE_WIDTH-image_pixmap->pixmap().width())/2);
  image_pixmap->setY((IMAGE_HEIGHT-image_pixmap->pixmap().height())/2);
  image_pixmap->update();
  roi.setEnabled(false);
  roi.setVisible(false);
  roi.update();
  /*
  ui->image_view->scene()->update(-IMAGE_WIDTH/2,-IMAGE_HEIGHT/2,IMAGE_WIDTH*2,IMAGE_HEIGHT*2);
  tf_dialog.setPixmap(QPixmap::fromImage(transform_function));
  tf_dialog.show();
  */
}
