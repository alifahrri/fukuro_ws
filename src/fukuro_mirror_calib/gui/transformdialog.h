#ifndef TRANSFORMDIALOG_H
#define TRANSFORMDIALOG_H

#include <QDialog>
#include <QPixmap>
#include <QGraphicsPixmapItem>

namespace Ui {
class TransformDialog;
}

class TransformDialog : public QDialog
{
  Q_OBJECT

public:
  explicit TransformDialog(QWidget *parent = 0);
  void setPixmap(const QPixmap &pixmap);
  ~TransformDialog();

private:
  Ui::TransformDialog *ui;
  QGraphicsPixmapItem* tf_pixmap;
};

#endif // TRANSFORMDIALOG_H
