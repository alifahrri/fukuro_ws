#include "transformdialog.h"
#include "ui_transformdialog.h"

#define TF_IMAGE_WIDTH 640
#define TF_IMAGE_HEIGHT 480

TransformDialog::TransformDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::TransformDialog)
{
  ui->setupUi(this);
  setWindowTitle("Mirror Map");

  ui->graphicsView->setScene(new QGraphicsScene(QRectF(0,0,TF_IMAGE_WIDTH,TF_IMAGE_HEIGHT),this));
  ui->graphicsView->setBackgroundBrush(Qt::black);
  tf_pixmap = ui->graphicsView->scene()->addPixmap(QPixmap());

  ui->graphicsView->setRenderHints(QPainter::Antialiasing |
                                 QPainter::SmoothPixmapTransform |
                                 QPainter::HighQualityAntialiasing );

  setFixedSize(TF_IMAGE_WIDTH+50,TF_IMAGE_HEIGHT+50);
}

void TransformDialog::setPixmap(const QPixmap& pixmap)
{
  tf_pixmap->setPixmap(pixmap);
  update();
}

TransformDialog::~TransformDialog()
{
  delete ui;
}
