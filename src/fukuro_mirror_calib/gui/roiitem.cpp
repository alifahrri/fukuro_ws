#include "roiitem.h"
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

ROIItem::ROIItem() :
  center(QPoint(0,0)),
  radius(0.0),
  line(QLineF(0.0,0.0,0.0,0.0)),
  tp_cb(NULL),
  tr_cb(NULL),
  left_click(true)
{

}

void ROIItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  if(radius<=0.0)
    return;
  painter->setPen(QPen(Qt::red,1.5));
  painter->setBrush(Qt::NoBrush);
  painter->drawEllipse(center.x()-radius,center.y()-radius,radius*2,radius*2);
  painter->setBrush(Qt::red);
  painter->drawEllipse(center.x()-3.0,center.y()-3.0,6.0,6.0);
  if(line.length()>0.0)
  {
    painter->setPen(QPen(Qt::blue,1.5));
    painter->setBrush(Qt::blue);
    painter->drawLine(line);
    painter->drawEllipse(line.p2(),3.0,3.0);
    painter->setPen(QPen(Qt::green,1.5));
    painter->setBrush(Qt::green);
    painter->drawEllipse(test_point,3.0,3.0);
  }
}

QRectF ROIItem::boundingRect() const
{
  return QRectF(0.0,0.0,960,720);
}

void ROIItem::setROI(QPoint center_, double radius_)
{
  center = center_;
  radius = radius_;
}

void ROIItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  QLineF line_tmp(center,event->pos());
  if(fabs(line_tmp.length()-radius)<3.0)
  {
    line = line_tmp;
    line.setLength(radius);
    double tp_radius = QLineF(center,test_point).length();
    QLineF line_test(line);
    line_test.setLength(tp_radius);
    test_point = line_test.p2();
    angle = line.angle();
    r = tp_radius;
  }
  else if(fabs(line.angle()-line_tmp.angle())<3.0)
  {
    if(line_tmp.length()<radius)
    {
      QLineF line_test(line);
      line_test.setLength(line_tmp.length());
      test_point = line_test.p2();
      r = line_tmp.length();
    }
  }
  if(event->button()==Qt::LeftButton)
  {
    left_click = true;
    if(tp_cb)
      tp_cb(angle,r);
  }
  else if(event->button()==Qt::RightButton)
  {
    left_click = false;
    if(tr_cb)
      tr_cb(angle);
  }
  update();
}

void ROIItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  QLineF line_tmp(center,event->pos());
  if(fabs(line_tmp.length()-radius)<3.0)
  {
    line = line_tmp;
    line.setLength(radius);
    double tp_radius = QLineF(center,test_point).length();
    QLineF line_test(line);
    line_test.setLength(tp_radius);
    test_point = line_test.p2();
    angle = line.angle();
    r = tp_radius;
  }
  else if(fabs(line.angle()-line_tmp.angle())<3.0)
  {
    if(line_tmp.length()<radius)
    {
      QLineF line_test(line);
      line_test.setLength(line_tmp.length());
      test_point = line_test.p2();
      r = line_tmp.length();
    }
  }
  if(left_click)
  {
    if(tp_cb)
      tp_cb(angle,r);
  }
  else
  {
    if(tr_cb)
      tr_cb(angle);
  }
  update();
}

void ROIItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

}

void ROIItem::setTestPoint(double angle_, double radius_)
{
  angle = angle_;
  line.setAngle(angle_);
  QLineF line_tmp(line);
  if(radius_>0.0)
    line_tmp.setLength(radius_);
  else
    line_tmp.setLength(r);
  test_point = line_tmp.p2();
  update();
}
