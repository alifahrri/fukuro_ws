#include "fukuromaingui.h"
#include "ui_fukuromaingui.h"
#include <exception>
#include <sstream>

std::string robot_name;

FukuroMainGUI::FukuroMainGUI(ros::NodeHandle &node_, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FukuroMainGUI),
    timer(new QTimer), isSTM(false), isArduino(false), isCompass(false), isCommunication(false)
{
    ui->setupUi(this);

    //com_sub = node_.subscribe(robot_name+std::string("/communication"),50,&FukuroMainGUI::communicationcallback,this);

    timer->start(50);

    int agent = std::atoi(std::getenv("AGENT"));
    tx_port = 23898;

    switch (agent) {
    case 1:{
        rx_port = 10599;
        ui->rx_port->setValue(10599);
        break;
    }
    case 2:{
        rx_port = 16180;
        ui->rx_port->setValue(16180);
        break;
    }
    case 3:{
        rx_port = 31415;
        ui->rx_port->setValue(31415);
        break;
    }
    case 4:{
        rx_port = 46692;
        ui->rx_port->setValue(46692);
        break;
    }
    case 5:{
        rx_port = 10898;
        ui->rx_port->setValue(10898);
        break;
    }
    case 6:{
        rx_port = 13080;
        ui->rx_port->setValue(13080);
        break;
    }
    }


    ui->address->setText(QString("224.16.32.90")); //default Multicast IPv4 for fukuro
    address = ui->address->text().toStdString();
    ui->tx_port->setValue(23898);


    if(!ros::get_environment_variable(robot_name,"FUKURO"))
        exit(-1);
    ros::service::waitForService(robot_name+"/keypad_service");

    connect(ui->pushButton,&QPushButton::clicked,[=]
    {
        setKey(0);
    });

    connect(ui->pushButton_2,&QPushButton::clicked,[=]
    {
        setKey(1);
    });

    connect(ui->pushButton_3,&QPushButton::clicked,[=]
    {
        setKey(2);
    });

    connect(ui->pushButton_4,&QPushButton::clicked,[=]
    {
        setKey(3);
    });

    connect(ui->pushButton_5,&QPushButton::clicked,[=]
    {
        setKey(4);
    });

    connect(ui->pushButton_6,&QPushButton::clicked,[=]
    {
        setKey(5);
    });

    connect(ui->pushButton_7,&QPushButton::clicked,[=]
    {
        setKey(6);
    });

    connect(ui->pushButton_8,&QPushButton::clicked,[=]
    {
        setKey(7);
    });

    connect(ui->pushButton_9,&QPushButton::clicked,[=]
    {
        setKey(8);
    });

    connect(ui->pushButton_10,&QPushButton::clicked,[=]
    {
        setKey(9);
    });

    connect(ui->pushButton_11,&QPushButton::clicked,[=]
    {
        setKey(10);
    });

    connect(ui->pushButton_12,&QPushButton::clicked,[=]
    {
        setKey(11);
    });

    connect(ui->pushButton_13,&QPushButton::clicked,[=]
    {
        setKey(12);
    });

    connect(ui->pushButton_14,&QPushButton::clicked,[=]
    {
        setKey(13);
    });

    connect(ui->pushButton_15,&QPushButton::clicked,[=]
    {
        setKey(14);
    });

    connect(ui->pushButton_16,&QPushButton::clicked,[=]
    {
        setKey(15);
    });

    connect(ui->pushButton_17,&QPushButton::clicked,[=]
    {
        setKey(16);
    });

    connect(ui->pushButton_18,&QPushButton::clicked,[=]
    {
        setKey(17);
    });

    connect(ui->pushButton_19,&QPushButton::clicked,[=]
    {
        setKey(18);
    });

    connect(ui->pushButton_20,&QPushButton::clicked,[=]
    {
        setKey(19);
    });

    connect(ui->pushButton_21,&QPushButton::clicked,[=]
    {
        setKey(20);
    });

    connect(ui->pushButton_22,&QPushButton::clicked,[=]
    {
        setKey(21);
    });

    connect(ui->pushButton_23,&QPushButton::clicked,[=]
    {
        setKey(22);
    });

    connect(ui->pushButton_24,&QPushButton::clicked,[=]
    {
        setKey(23);
    });

    connect(ui->pushButton_25,&QPushButton::clicked,[=]
    {
        setKey(24);
    });

    connect(ui->pushButton_26,&QPushButton::clicked,[=]
    {
        setKey(25);
    });

    connect(ui->pushButton_27,&QPushButton::clicked,[=]
    {
        setKey(26);
    });

    connect(ui->pushButton_28,&QPushButton::clicked,[=]
    {
        setKey(27);
    });

    connect(ui->pushButton_29,&QPushButton::clicked,[=]
    {
        setKey(28);
    });

    connect(ui->pushButton_30,&QPushButton::clicked,[=]
    {
        setKey(29);
    });

    connect(ui->pushButton_31,&QPushButton::clicked,[=]
    {
        setKey(30);
    });

    connect(ui->pushButton_32,&QPushButton::clicked,[=]
    {
        setKey(31);
    });

    connect(ui->pushButton_33,&QPushButton::clicked,[=]
    {
        setKey(32);
    });

    connect(ui->pushButton_34,&QPushButton::clicked,[=]
    {
        setKey(33);
    });

    connect(ui->pushButton_35,&QPushButton::clicked,[=]{

    });

    connect(ui->pushButton_36,&QPushButton::clicked,[=]{

    });

    //Communication
    connect(ui->connectButton,&QPushButton::clicked,[=]{
        communicationconnect();
    });

    //HWController
    connect(ui->refresh_button,&QPushButton::clicked,[=]{
        refresh();
    });

    connect(ui->connect_button,&QPushButton::clicked,[=]{
        STMConnect();
    });

    connect(ui->connect_arduino_button,&QPushButton::clicked,[=]{
        ArduinoConnect();
    });

    connect(ui->btnCmps,&QPushButton::clicked,[=]{
        Compass();
    });

    char* user_name = std::getenv("USER");
    QString pixmap_path = QString("/home/") + QString(user_name) + QString("/fukuro_ws/src/fukuro_main_gui/icon/fukuro_logo.png");
    QPixmap fukuro_logo = QPixmap(pixmap_path);
    ui->fukuro_logo->setPixmap(fukuro_logo);

    QObject::connect(timer,&QTimer::timeout,[&](){
        if(isSTM){
            ui->connect_button->setText("Disconnect");
        }
        else{
            ui->connect_button->setText("Connect");
        }
        if(isArduino){
            ui->connect_arduino_button->setText("Disconnect Arduino");
        }
        else{
            ui->connect_arduino_button->setText("Connect Arduino");
        }
        if(isCompass){
            ui->btnCmps->setText("Stop");
        }
        else{
            ui->btnCmps->setText("Start");
        }
        if(isCommunication){
            ui->connectButton->setText("Disconnect");
        }
        else{
            ui->connectButton->setText("Connect");
        }
    });

    setWindowTitle("Fukuro Main GUI");
    this->setFixedSize(this->size());
}

FukuroMainGUI::~FukuroMainGUI()
{
    delete ui;
}

std::string FukuroMainGUI::getRole(int keyIn){
    if(keyIn <= 29)
        return this->role[keyIn];
    return "auto";
}

void FukuroMainGUI::setKey(int keyIn){
    fukuro_common::KeypadService key_service;
    key_service.request.key = keyIn;
    std::string rolein = getRole(keyIn);
    QString role = QString::fromStdString(rolein);

    if(ros::service::call(robot_name+"/keypad_service",key_service))
    {
        //ui->text_edit->appendPlainText(QString("Request %1 OK\t| Role: %2").arg(keyIn).arg(role));
    }
    else
    {
        //ui->text_edit->appendPlainText(QString("Service request failed"));
    }
}

/*
void FukuroMainGUI::communicationcallback(const fukuro_common::Communication::ConstPtr& msg){
    address = msg->address;
    rx_port = msg->rx_port;
    tx_port = msg->tx_port;
    //if(ui->address->text().isEmpty()){
    ui->address->setText(QString(address.c_str()));
    ui->tx_port->setValue(tx_port);
    ui->rx_port->setValue(rx_port);
    //}
}
*/

void FukuroMainGUI::communicationconnect(){

    fukuro_common::CommunicationService com_serv;
    address = ui->address->text().toStdString();
    tx_port = ui->tx_port->value();
    rx_port = ui->rx_port->value();

    com_serv.request.address = address;
    com_serv.request.tx_port = tx_port;
    com_serv.request.rx_port = rx_port;

    com_serv.request.connect = !com_serv.request.connect;

    if(ros::service::call(robot_name+"/communication_service",com_serv))
    {
        isCommunication? isCommunication = false : isCommunication = com_serv.response.success;
    }
    else{
        isCommunication = false;
    }


}

void FukuroMainGUI::refresh(){
    fukuro_common::HWControllerService hw_service;
    hw_service.request.refresh = true;
    if(ros::service::call(robot_name+"/hw_service",hw_service))
    {
        ui->comboBox->clear();
        ui->comboBox_2->clear();
        //serial_port_list.clear();
        //manufacturer_list.clear();
        std::vector<std::string> portlist = hw_service.response.port_list;
        std::vector<std::string> manuflist = hw_service.response.manufacturer_list;
        try{
            //std::vector<std::string>::iterator it, ti;
            //for(it = hw_service.response.port_list.begin(), ti = hw_service.response.manufacturer_list.begin(); it!=hw_service.response.port_list.end(); it++,ti++)
            for(int i=0; i<portlist.size();i++)
            {
                //std::cout<<"[PASSED]"<<std::endl;
                std::string port;
                std::string manufacturer;
                port = portlist[i];
                manufacturer = manuflist[i];
                //std::stringstream ss;
                //ss<<"Port "<<i/2;
                //port = ss.str();
                //ss.str("");
                //ss<<"Manufacturer "<<i/2;
                //manufacturer = ss.str();
                //ss.str("");
                //port = *it;
                //manufacturer = *ti;
                ui->comboBox->addItem(QString(port.c_str())+QString(" : ")+QString(manufacturer.c_str()));
                ui->comboBox_2->addItem(QString(port.c_str())+QString(" : ")+QString(manufacturer.c_str()));
                //serial_port_list.push_back(s.portName());
                //manufacturer_list.push_back(s.manufacturer());
                //std::cout << s.portName().toStdString() << " : "
                //          << s.description().toStdString() << '\n'
                //          << "manufacturer : "
                //          << s.manufacturer().toStdString() << '\n'
                //          << "serial number : "
                //          << s.serialNumber().toStdString() << '\n';
            }
        }
        catch(std::exception e){
            std::cout<<e.what()<<std::endl;
        }
    }
    else{

    }

}

void FukuroMainGUI::STMConnect(){
    fukuro_common::HWControllerService hw_service;
    hw_service.request.STMConnect = ui->comboBox->currentIndex();
    //isSTM? hw_service.request.isSTM = false:
    hw_service.request.isSTM = true;
    if(ros::service::call(robot_name+"/hw_service",hw_service))
    {
        std::cout<<"[STM32] Call was successful!"<<std::endl;
        std::cout<<"[STM32] Before: ";
        isSTM?std::cout<<"True"<<std::endl:std::cout<<"False"<<std::endl;
        isSTM?isSTM = false : isSTM = hw_service.response.STMSuccess;
        std::cout<<"[STM32] After: ";
        isSTM?std::cout<<"True"<<std::endl:std::cout<<"False"<<std::endl;
    }
    else{
        std::cout<<"[STM32] Call was failed!"<<std::endl;
        isSTM = false;
    }
}

void FukuroMainGUI::ArduinoConnect(){
    fukuro_common::HWControllerService hw_service;
    hw_service.request.ArduinoConnect = ui->comboBox_2->currentIndex();
    //isArduino? hw_service.request.isArduino = false:
    hw_service.request.isArduino = true;
    if(ros::service::call(robot_name+"/hw_service",hw_service))
    {
        std::cout<<"[ARDUINO] Call was successful!"<<std::endl;
        std::cout<<"[ARDUINO] Before: ";
        isArduino?std::cout<<"True"<<std::endl:std::cout<<"False"<<std::endl;
        isArduino?isArduino=false:isArduino = hw_service.response.ArduinoSuccess;
        std::cout<<"[ARDUINO] After: ";
        isArduino?std::cout<<"True"<<std::endl:std::cout<<"False"<<std::endl;
    }
    else{
        isArduino = false;
        std::cout<<"[ARDUINO] Failed to call!"<<std::endl;
    }
}

void FukuroMainGUI::Compass(){
    fukuro_common::HWControllerService hw_service;
    hw_service.request.Compass = true;
    if(ros::service::call(robot_name+"/hw_service",hw_service))
    {
        std::cout<<"[COMPASS] Call was successful!"<<std::endl;
        isCompass?isCompass=false:isCompass = hw_service.response.Compassuccess;
        if(isCompass) lastcompass = compass;
    }
    else{
        std::cout<<"[COMPASS] Call was failed!"<<std::endl;
        isCompass = false;
    }
}

void FukuroMainGUI::CompassCallback(double compass){
    this->compass = compass;
    if(isCompass){
        double setArrowCompass = this->compass - lastcompass;
        if(setArrowCompass>360.0) setArrowCompass -= 360.0;
        if(setArrowCompass<0.0) setArrowCompass += 360.0;
        ui->CompassArrow->setArrow(setArrowCompass);
        ui->CompassRead->display(setArrowCompass);
        ui->CompassArrow->update();
    }
    else{
        ui->CompassArrow->setArrow(this->compass);
        ui->CompassRead->display(this->compass);
        ui->CompassArrow->update();
    }
}
