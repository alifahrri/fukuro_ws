#ifndef FUKUROMAINGUI_H
#define FUKUROMAINGUI_H

#include <QTimer>
#include <QDialog>
#include <ros/ros.h>
#include "iostream"
#include <QString>
// #include <fukuro_common/HWController.h>
#include <fukuro_common/Communication.h>
#include <fukuro_common/HWControllerService.h>
#include <fukuro_common/CommunicationService.h>
#include "fukuro_common/KeypadService.h"
#include "fukuro_common/Compass.h"

namespace Ui {
class FukuroMainGUI;
}

class FukuroMainGUI : public QDialog
{
    Q_OBJECT

public:
    explicit FukuroMainGUI(ros::NodeHandle &node_, QWidget *parent = 0);
    ~FukuroMainGUI();

private:
    Ui::FukuroMainGUI *ui;

public:
    void setKey(int keyIn);
    std::string getRole(int keyIn);
    //void communicationcallback(const fukuro_common::Communication::ConstPtr &msg);
    void communicationconnect();
    void refresh();
    void STMConnect();
    void ArduinoConnect();
    void Compass();
    void CompassCallback(double compass);

private:
    QTimer *timer;
    std::string address;
    double compass, lastcompass;
    //serial_port_list;
    //manufacturer_list;
    int tx_port, rx_port;
    bool isArduino;
    bool isSTM;
    bool isCompass;
    bool isCommunication;
    //ros::Subscriber com_sub;
    std::string role[30]={"stop", "defender2", "defender2", "defender", "defender", "striker",
                          "striker_dribble", "striker", "striker_dribble", "timer_striker", "timer_striker2", "striker",
                          "timer_striker", "striker", "timer_striker", "striker", "striker_dribble", "striker",
                          "striker_dribble", "timer_defense2", "timer_defense2", "timer_defense", "timer_defense","timer_striker",
                          "timer_striker2", "striker", "timer_striker", "striker", "timer_striker", "goalie"};

};

#endif // FUKUROMAINGUI_H
