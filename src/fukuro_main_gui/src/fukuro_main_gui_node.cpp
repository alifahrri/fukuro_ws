#include <ros/ros.h>
#include <QApplication>
#include "fukuromaingui.h"

typedef boost::function<void(const fukuro_common::Compass::ConstPtr&)> CompassCallback;

int main(int argc, char** argv)
{
  QApplication app(argc,argv);
  ros::init(argc,argv,"fukuro_main_gui");
  char* env_robot_name = std::getenv("FUKURO");
  std::string robot_name = std::string(env_robot_name);
  ros::NodeHandle nh;
  FukuroMainGUI dialog(nh);
  dialog.show();
  CompassCallback cmps_callback = [&](const fukuro_common::Compass::ConstPtr& compass){
      dialog.CompassCallback(compass->cmps);
  };
  ros::Subscriber compass_sub = nh.subscribe(robot_name+std::string("/compass"),10,cmps_callback);
  ros::AsyncSpinner spinner(2);
  if(spinner.canStart())
      spinner.start();
  app.exec();
  return 0;
}
