#include "ros/ros.h"
#include "mcldialog.h"
#include "QApplication"
#include <QTimer>

int main(int argc, char** argv)
{
  QApplication app(argc,argv);
  ros::init(argc,argv,"fukuro_amcl_viewer");
  ros::NodeHandle node;
  MCLDialog mcl_dialog;
  ros::Subscriber loc_sub = node.subscribe("/localization",1,&MCLDialog::updateMCL,&mcl_dialog);
  ros::Subscriber white_sub = node.subscribe("/omnivision/Whites",1,&MCLDialog::updateWhites,&mcl_dialog);
  // ros::Subscriber global_white_sub = node.subscribe("/localization/global_whites",1,&MCLDialog::updateWhites,&mcl_dialog);
  ros::AsyncSpinner spinner(1);
  if(spinner.canStart())
    spinner.start();
  mcl_dialog.show();

  QTimer timer;
  timer.connect(&timer, &QTimer::timeout, [&]{
      if(!ros::ok())
        mcl_dialog.close();
    });
  timer.start(1000);

  app.exec();
  return 0;
}
