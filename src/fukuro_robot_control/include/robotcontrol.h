#ifndef ROBOTCONTROL_H
#define ROBOTCONTROL_H

#include <tuple>
#include <ros/ros.h>
#include <ros/param.h>
#include <chrono>
#include "fukuro_common/OdometryInfo.h"
#include "fukuro_robot_control/trajectoryplanner.h"

#ifndef FIRST_BUILD
//#include "fukuro_robot_control/FukuroRobotControlConfig.h"
#endif

#include "fukuro_common/RobotControl.h"
#include "fukuro_common/HWControllerCommand.h"
#include "fukuro_common/VelCmd.h"
#include "fukuro_common/Localization.h"
#include "fukuro_common/DribblerControl.h"
#include "fukuro_common/RobotControlInfo.h"
#include "fukuro_common/PlannerInfo.h"
#include "fukuro_common/PlannerService.h"

namespace fukuro {

class RobotControl
{
public:
    typedef std::chrono::high_resolution_clock Clock;
    typedef std::chrono::duration<double,std::milli> Duration;
    typedef std::tuple<double,double,double,bool> Pose;
    typedef std::tuple<double,double,double> PID;
    typedef std::tuple<double,double,double, double> DribblerKP;
    struct Limit
    {
        double velocity = 1.5;
        double acceleration = 1.0;
    };

    static Pose makePose(double x, double y, double w, bool local);
    static PID makePID(double p, double i, double d);
    static DribblerKP makeDribblerKP(double kp_x, double kp_y, double kp_w, double kp_x_min);

    enum controller_t {
        PIDController,
        TimeOptimalController
    };
    enum control_state_t{ No_PID,
                          PID_control,
                          Steady};
    enum w_control_state_t {
        w_PID,
        w_stop,
        w_big_error,
        w_small_error};

    RobotControl(ros::NodeHandle& node_, controller_t controller_ = PIDController);
    void updatePath(const fukuro_common::PlannerInfo::ConstPtr &path);
    void updateTarget(const fukuro_common::RobotControl::ConstPtr &target_pose);
    void updateRobotPose(const fukuro_common::Localization::ConstPtr &loc);
    void updateRobotVel(const fukuro_common::OdometryInfo::ConstPtr &odom);
#ifndef FIRST_BUILD
    //void reconfigure(const fukuro_robot_control::FukuroRobotControlConfig& config, uint32_t level);
#endif
    void loadYAMLSettings(std::string node_name);
    void process();

private:
    void publish();
    void pidControl();
    void timeOptimalControl();

    double& x(Pose& pose);
    double& y(Pose& pose);
    double& w(Pose& pose);
    bool& is_local(Pose& pose);

    double& kP(PID& pid);
    double& kI(PID& pid);
    double& kD(PID& pid);

    double& kPx(DribblerKP& kp);
    double& kPy(DribblerKP& kp);
    double& kPw(DribblerKP& kp);
    double& kPx_min(DribblerKP& kp);

private:
    ros::NodeHandle& node;
    ros::Publisher hwcontrol_pub;
    ros::Publisher control_info_pub;
    ros::Publisher dribbler_pub;
    ros::Publisher vel_pub;
    ros::Duration odom_period;
    ros::Time odom_last_update;
    std::string robot_name;
    Pose control_signal;
    Pose robot_pose;
    Pose robot_vel;
    Pose waypoint;
    Pose target;
    PID pid_constant;
    PID pid_angle;
    DribblerKP dribbler_kp;
    Clock::time_point t_0;
    control_state_t control_state;
    controller_t controller;

    //Time opt control stuff
    RobotTrajectory::Generator trajectory_generator;
    RobotTrajectory::State initial_state;
    RobotTrajectory::State final_state;
    Limit linear;
    Limit angular;

    //PID stuff
    w_control_state_t w_control_state;
    bool reset_control;
    bool dribbler;
    bool lin_pid_control;
    bool ang_pid_control;
    bool plan;
    bool del_start;
    bool enable_nopid = true;
    double linear_i_term;
    double angular_i_term;
    double error;
    double error_angle;
    double last_error;
    double last_error_angle;
    double v_max;
    double w_max;
    double dribbler_pwm0;
    double dribbler_speed;
    double lin_pid_control_radius;
    double ang_pid_control_angle;
};

}
#endif // ROBOTCONTROL_H
