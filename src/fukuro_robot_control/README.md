## fukuro_robot_control
position control using PD-control law  

### Launcers  
- `robot_control_core.launch` : launch `fukuro_robot_control_node` as required process and write output to screen; launch parameter name: `controller`, default: `pid`, description: determine to use pid control or by generating trajectory; recommended : use pid, the implementation trajectory controller is ill-defined and is not well-tested (24-12-2018)   
- `robot_control_test.launch` : launch `fukuro_robot_control_node` and `mcl_hwctrl_test.launch`  
- `robot_control_bag_test.launch` : launch `robot_control_node` and `mcl_bag_test.launch`  

### Nodes  
- `fukuro_robot_control_node` : the core of robot control  

### Environment Variables 
- `$FUKURO` : robot name, required

### Subscribed Topics  
- `/$FUKURO/robot_control` : target pose, type: `fukuro_common/RobotControl`,   
- `/localization` : get information about robot pose, type : `fukuro_common/Localization`,  
- `/path_planning` : get information about the planned path, type : `fukuro_common/PlannerInfo`,  
- `/$FUKURO/odometry` : get robot's odometry information, type : `fukuro_common/OdometyrInfo`  

### Published Topics  
- `/$FUKURO/vel_cmd` : commanded local velocity to hardware controller  
- `/$FUKURO/dribbler` : dribbler command for hardware controller  
- `/$FUKURO/hwcontrol_command` : compact hw control command definition  
- `/$FUKURO/robot_control_info` : control information  

### Service Clients  
- `/planner_service` : called if `plan` is enabled, type : `fukuro_common/PlannerService`  
