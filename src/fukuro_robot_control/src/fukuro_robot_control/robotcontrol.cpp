#include "robotcontrol.h"
#include "std_msgs/String.h"
#include <string>

#define MAX_LINEAR_I_TERM 1.0
#define MAX_ANGULAR_I_TERM 0.75
//#define PID_CONTROL

#define DEFAULT_ODOM_RATE 30.0
#define DEFAULT_PUBLISH_RATE 30.0

namespace fukuro {

  inline
  RobotControl::Pose RobotControl::makePose(double x, double y, double w, bool local)
  {
    return std::make_tuple(x,y,w,local);
  }

  inline
  RobotControl::PID RobotControl::makePID(double p, double i, double d)
  {
    return std::make_tuple(p,i,d);
  }

  inline
  RobotControl::DribblerKP RobotControl::makeDribblerKP(double kp_x, double kp_y, double kp_w, double kp_x_min)
  {
    return std::make_tuple(kp_x,kp_y,kp_w,kp_x_min);
  }

  RobotControl::RobotControl(ros::NodeHandle& node_, controller_t controller_) :
    node(node_),
    target(makePose(0.0,0.0,0.0,true)),
    pid_constant(makePID(1.0,1.0,1.0)),
    dribbler_kp(makeDribblerKP(1.0,1.0,1.0,1.0)),
    reset_control(true),
    dribbler(false),
    lin_pid_control(false),
    ang_pid_control(false),
    plan(false),
    linear_i_term(0.0),
    angular_i_term(0.0),
    error(0.0),
    error_angle(0.0),
    last_error(0.0),
    last_error_angle(0.0),
    v_max(2.0),
    w_max(1.0),
    dribbler_pwm0(0.0),
    dribbler_speed(0.0),
    lin_pid_control_radius(0.3),
    ang_pid_control_angle(0.1),
    control_state(PID_control),
    w_control_state(w_stop),
    controller(controller_)
  {
    if(!ros::get_environment_variable(robot_name,"FUKURO"))
      {
        ROS_ERROR("robot name empty!!! export FUKURO=fukuro1");
        exit(-1);
      }

    odom_period = ros::Duration(ros::Rate(30.0));

    x(robot_vel) = 0.0;
    y(robot_vel) = 0.0;
    w(robot_vel) = 0.0;

    x(robot_pose) = 0.0;
    y(robot_pose) = 0.0;
    w(robot_pose) = 0.0;

    bool nopid;
    if(ros::param::get("/robot_control_nopid",nopid)) {
      enable_nopid = nopid;
      ROS_WARN("enable_nopid : %d", enable_nopid);
    }

    //    ROS_INFO("waiting for /planner service");
    //    ros::service::waitForService("/planner_service");
    vel_pub = node.advertise<fukuro_common::VelCmd>(robot_name+"/vel_cmd",1);
    dribbler_pub = node.advertise<fukuro_common::DribblerControl>(robot_name+"/dribbler",1);
    hwcontrol_pub = node.advertise<fukuro_common::HWControllerCommand>(robot_name+"/hwcontrol_command",1);
    control_info_pub = node.advertise<fukuro_common::RobotControlInfo>(robot_name+"/robot_control_info",1);
  }

  void RobotControl::updatePath(const fukuro_common::PlannerInfo::ConstPtr &path)
  {
    if(!plan)
      return;
    w(waypoint) = w(target);
    if(!path->solved || path->solutions.size()<2)
      {
        x(waypoint) = x(target);
        y(waypoint) = y(target);
      }
    else
      {
        x(waypoint) = path->solutions.at(1).x/100.0;
        y(waypoint) = path->solutions.at(1).y/100.0;
      }
  }

  void RobotControl::updateTarget(const fukuro_common::RobotControl::ConstPtr &target_pose)
  {
    ROS_INFO("");
    bool local = is_local(target);
    auto last_target_x = x(target);
    auto last_target_y = y(target);
    x(target) = target_pose->target_pose.x;
    y(target) = target_pose->target_pose.y;
    w(target) = target_pose->target_pose.theta;
    is_local(target) = target_pose->option.data == std::string("local") ? true : false;
    if(target_pose->plan && !is_local(target)/* && (last_target_x!=x(target)) && (last_target_y!=y(target))*/)
      {
        if(!plan) {
            fukuro_common::PlannerService planer_goal;
            planer_goal.request.goal.x = x(target)*100.0;
            planer_goal.request.goal.y = y(target)*100.0;
            if(!ros::service::call("/planner_service",planer_goal)) {
                ROS_ERROR("setting planner goal failed");
                plan = false;
              }
            else {
                plan = true;
              }
          }
      }
    else {
        plan = false;
      }
    if(local!=is_local(target))
      reset_control = true;
    dribbler = target_pose->dribbler;
  }

  void RobotControl::updateRobotPose(const fukuro_common::Localization::ConstPtr &loc)
  {
    if(std::isnan(loc->belief.x) || std::isnan(loc->belief.theta) || std::isnan(loc->belief.y))
      return;
    x(robot_pose) = loc->belief.x/100.0;
    y(robot_pose) = loc->belief.y/100.0;
    double angle_deg = loc->belief.theta > 180.0 ? loc->belief.theta-360.0 : loc->belief.theta;
    double angle_rad = angle_deg*M_PI/180.0;
    w(robot_pose) = angle_rad;
    is_local(robot_pose) = false;
    ROS_INFO("robot_pose(%.2f,%.2f,%.2f)", x(robot_pose), y(robot_pose), w(robot_pose));
  }

  void RobotControl::updateRobotVel(const fukuro_common::OdometryInfo::ConstPtr &odom)
  {
    auto now = ros::Time::now();
    if(odom_last_update.isValid())
        odom_period = now - odom_last_update;
    auto f = 1.0f/odom_period.toSec();
    f = (f<15.0) ? DEFAULT_ODOM_RATE : f;
    x(robot_vel) = odom->vel.x * f / 100.0f;
    y(robot_vel) = odom->vel.y * f / 100.0f;
    w(robot_vel) = odom->vel.theta * f * M_PI / 180.0f;
    odom_last_update = now;
    ROS_INFO("f:%.3f; period:%.3f; robot_vel(%.2f,%.2f,%.2f)", f, odom_period.toSec(), x(robot_vel), y(robot_vel), w(robot_vel));
  }

#ifndef FIRST_BUILD
  /*
  void RobotControl::reconfigure(const fukuro_robot_control::FukuroRobotControlConfig &config, uint32_t level)
  {
    ROS_INFO("reconfigure kp_lin : %f, ki_lin : %f, kd_lin : %f, "
             "kp_ang : %f, ki_ang : %f, kd_ang : %f, v_max : %f, "
             "w_max : %f, drib_kp_x : %f, drib_kp_y : %f, drib_kp_w : %f, drib_kp_x- : %f, drib_pwm0 : %f",
             config.kp_linear, config.ki_linear, config.kd_linear,
             config.kp_angular, config.ki_angular, config.kd_angular,
             config.v_max, config.w_max, config.dribbler_kp_x, config.dribbler_kp_y,
             config.dribbler_kp_w, config.dribbler_kp_x_minus, config.dribbler_pwm0);
    pid_constant = makePID(config.kp_linear,config.ki_linear,config.kd_linear);
    pid_angle = makePID(config.kp_angular,config.ki_angular,config.kd_angular);
    v_max = config.v_max;
    w_max = config.w_max;
    dribbler_pwm0 = config.dribbler_pwm0;
    kPx(dribbler_kp) = config.dribbler_kp_x;
    kPy(dribbler_kp) = config.dribbler_kp_y;
    kPw(dribbler_kp) = config.dribbler_kp_w;
    kPx_min(dribbler_kp) = config.dribbler_kp_x_minus;
  }
  */
#endif

  void RobotControl::loadYAMLSettings(std::__cxx11::string node_name)
  {
    std::cout << "node name : " << node_name << '\n';
    std::string dynparam_load_cmd = std::string("rosrun dynamic_reconfigure dynparam load ");
    dynparam_load_cmd += node_name + " ~/fukuro_ws/src/fukuro_robot_control/settings/";
    dynparam_load_cmd += robot_name + ".yaml &  ";
    std::cout << dynparam_load_cmd << '\n';
    std::system(dynparam_load_cmd.c_str());
  }

  void RobotControl::process()
  {
    if(del_start){t_0=Clock::now();
        del_start=false;}

    switch (controller) {
      case PIDController:
        pidControl();
        break;
      case TimeOptimalController:
        timeOptimalControl();
        break;
      default:
        break;
      }
    publish();
    /*
  ROS_INFO("process : robot_pose->(%f,%f,%f) target->(%f,%f,%f)",
           x(robot_pose),y(robot_pose),w(robot_pose),
           x(target),y(target),w(target));
           */
  }

  inline
  void RobotControl::publish()
  {
    ROS_INFO("");
    fukuro_common::HWControllerCommand cmd;
    fukuro_common::VelCmd vel_cmd_msg;
    vel_cmd_msg.Vx = x(control_signal);
    vel_cmd_msg.Vy = y(control_signal);
    vel_cmd_msg.w  = w(control_signal);
    //    vel_pub.publish(vel_cmd_msg);
    fukuro_common::DribblerControl dribbler_msg;
    dribbler_msg.speed = (dribbler_speed > 255) ? 255 : (dribbler_speed < 0) ? 0 : dribbler_speed;
    dribbler_msg.dir_in = 1;
    //    dribbler_pub.publish(dribbler_msg);
    fukuro_common::RobotControlInfo control_info_msg;
    control_info_msg.plan = plan;
    if(plan)
      {
        control_info_msg.setpoint.x = x(waypoint);
        control_info_msg.setpoint.y = y(waypoint);
        control_info_msg.setpoint.theta = w(waypoint);
      }
    else if(!is_local(target))
      {
        control_info_msg.setpoint.x = x(target);
        control_info_msg.setpoint.y = y(target);
        control_info_msg.setpoint.theta = w(target);
      }
    else
      {
        double c = cos(w(robot_pose));
        double s = sin(w(robot_pose));
        double global_x = c*x(target)-s*y(target);
        double global_y = s*x(target)+c*y(target);
        double global_w = w(robot_pose)+w(target);
        control_info_msg.setpoint.x = global_x;
        control_info_msg.setpoint.y = global_y;
        control_info_msg.setpoint.theta = global_w;
      }
    cmd.vel = vel_cmd_msg;
    cmd.dribbler = dribbler_msg;
    hwcontrol_pub.publish(cmd);
    control_info_msg.error_radius = error;
    control_info_msg.error_angle = error_angle;
    control_info_pub.publish(control_info_msg);
  }

  inline
  void RobotControl::pidControl()
  {
    double local_target_angle(0.0);

#ifdef PID_CONTROL
    if(last_error<lin_pid_control_radius)
      if(!lin_pid_control)
        {
          lin_pid_control = true;
          linear_i_term = 0.0;
        }
      else
        lin_pid_control = false;

    if(last_error_angle<ang_pid_control_angle)
      if(!ang_pid_control)
        {
          ang_pid_control = true;
          angular_i_term = 0.0;
        }
      else
        ang_pid_control = false;
#endif

    if(reset_control)
      {
        last_error = 0.0;
        last_error_angle = 0.0;
        reset_control = false;
      }

    if(is_local(target))
      {
        error = sqrt(x(target)*x(target)+y(target)*y(target));
        error_angle = w(target);
        local_target_angle = atan2(y(target),x(target));
      }
    else
      {
        double c = cos(w(robot_pose));
        double s = sin(w(robot_pose));
        double dx(0.0);
        double dy(0.0);
        if(plan)
          {
            dx = x(waypoint)-x(robot_pose);
            dy = y(waypoint)-y(robot_pose);
          }
        else
          {
            dx = x(target)-x(robot_pose);
            dy = y(target)-y(robot_pose);
          }
        double local_target_x = c*dx+s*dy;
        double local_target_y = -s*dx+c*dy;
        local_target_angle = atan2(local_target_y,local_target_x);
        error = sqrt(local_target_x*local_target_x+local_target_y*local_target_y);
        error_angle = w(target)-w(robot_pose);
        if(error_angle>M_PI)
          error_angle = -2*M_PI+error_angle;
        else if(error_angle<-M_PI)
          error_angle = 2*M_PI-error_angle;
      }

    double d_error = error - last_error;
    double d_error_angle = error_angle - last_error_angle;

    last_error = error;
    last_error_angle = error_angle;

    double cmd_vel = kP(pid_constant)*error + kD(pid_constant)*d_error;
    double cmd_w = kP(pid_angle)*error_angle + kD(pid_angle)*d_error_angle;

#ifdef PID_CONTROL
    if(lin_pid_control)
      {
        linear_i_term += kI(pid_constant)*error;
        if(linear_i_term>MAX_LINEAR_I_TERM)
          linear_i_term = MAX_LINEAR_I_TERM;
        cmd_vel += linear_i_term;
      }
    if(ang_pid_control)
      {
        angular_i_term += kI(pid_angle)*error_angle;
        if(angular_i_term>MAX_ANGULAR_I_TERM)
          angular_i_term = MAX_ANGULAR_I_TERM;
        cmd_w += angular_i_term;
      }
#endif

    std::cout <<linear_i_term;

    if(fabs(cmd_vel)>v_max)
      cmd_vel = cmd_vel < 0 ? -v_max : v_max;
    if(fabs(cmd_w)>w_max)
      cmd_w = cmd_w < 0 ? -w_max : w_max;

    //double start_speed = cmd_vel<0 ? -0.5:0.5;

    if(enable_nopid) {
      switch (control_state)
      {
      case No_PID:
        {
          x(control_signal) = cos(local_target_angle)*0.35;
          y(control_signal) = sin(local_target_angle)*0.35;
          w(control_signal) = cmd_w;
          if(last_error>0.3 || last_error<0.05)
            control_state = PID_control;
          break;
        }

        //  case Starting:
        //     {
        //      del_start=true;
        //      x(control_signal) = cos(local_target_angle)*start_speed;
        //      y(control_signal) = sin(local_target_angle)*start_speed;
        //      w(control_signal) = 0;
        //      auto t_now = Clock::now();
        //      Duration delay = t_now-t_0;
        //      if(error<0.1 && error_angle<0.1)control_state= Steady;
        //      if (delay>Duration(1200))control_state= PID_control;
        //      break;
        //     }
        //  default: control_state=Steady;

      case PID_control:
        {
          x(control_signal) = cos(local_target_angle)*cmd_vel;
          y(control_signal) = sin(local_target_angle)*cmd_vel;
          w(control_signal) = cmd_w;
          if(last_error<0.3 && last_error>0.05)
            control_state = No_PID;
          break;
        }
      }
    }
    //double w_small = 0.7;
    double w_big = 0.8;

    switch (w_control_state) {

      case w_small_error:
        if(fabs(error_angle) < 0.1)
          w_control_state = w_stop;
        if(fabs(error_angle) > 1.0)
          w_control_state = w_big_error;
        w(control_signal) = cmd_w;
        break;

      case w_big_error:
        if(fabs(error_angle) < 1.0)
          w_control_state = w_small_error;
        w(control_signal) = error_angle < 0 ? -w_big : w_big;
        break;

      case w_stop:
        if(fabs(error_angle) < 1.0 && fabs(error_angle) > 0.1 )
          w_control_state = w_small_error;
        if(fabs(error_angle) > 1.0)
          w_control_state = w_big_error;
        w(control_signal) = 0;
        break;
      }

    if(dribbler)
      {
        //double dribbler_x = x(control_signal) > 0 ? -kPx(dribbler_kp)*x(control_signal) : kPx_min(dribbler_kp)*fabs(x(control_signal));
        dribbler_speed = dribbler_pwm0;
        //+ dribbler_x +
        //kPy(dribbler_kp)*y(control_signal) +
        //kPw(dribbler_kp)*w(control_signal);
      }
    else
      dribbler_speed = 0.0;

  }

  void RobotControl::timeOptimalControl()
  {
    auto now = ros::Time::now();
    auto c = cos(w(robot_pose));
    auto s = sin(w(robot_pose));

    final_state.x = x(target);
    final_state.y = y(target);
    final_state.w = w(target);

    if(is_local((target))) {
        final_state.x = c*x(target) - s*y(target) + x(robot_pose);
        final_state.y = s*x(target) + c*y(target) + y(robot_pose);
        final_state.w += w(robot_pose);
      }

    initial_state.x = x(robot_pose);  initial_state.dx = x(robot_vel);
    initial_state.y = y(robot_pose);  initial_state.dy = y(robot_vel);
    initial_state.w = w(robot_pose);  initial_state.dw = w(robot_vel);

    ROS_INFO("computing.. init(%.2f,%.2f,%.2f,%.2f,%.2f,%.2f); target(%.2f,%.2f,%.2f)",
             initial_state.x, initial_state.y, initial_state.w, initial_state.dx, initial_state.dy, initial_state.dw,
             final_state.x, final_state.y, final_state.w);
    trajectory_generator.setLimit(linear.velocity, linear.acceleration, angular.velocity, angular.acceleration);
    trajectory_generator.generate(initial_state, final_state);
    //    auto time = (1.0f/DEFAULT_PUBLISH_RATE) + compute_time;
    //    auto state = trajectory_generator.getControl(time);
    auto dt = 1.0f/DEFAULT_PUBLISH_RATE;
    auto time = 0.03f / dt;
    auto trajectory = trajectory_generator.getTrajectory(0.0f,1.0f,dt);
    std::stringstream trj;
    for(const auto& s : trajectory.first)
      trj << "(" << s.x << "," << s.y << "," << s.w << "); ";
    ROS_INFO("trajectory : %s",trj.str().c_str());
    auto idx = std::min(int(time), (int)trajectory.first.size());
    auto state = trajectory.first.at(idx);
    x(control_signal) = c*state.dx + s*state.dy;
    y(control_signal) = -s*state.dx + c*state.dy;
    w(control_signal) = state.dw;
    auto compute_time = (ros::Time::now()-now).toSec();
    ROS_INFO("time :%.2f ===>>> "
             "ctrl signal(global)->(%.2f,%.2f,%.2f) || "
             "ctrl signal->(%.2f,%.2f,%.2f) || "
             "est. pos->(%.2f,%.2f,%.2f) || "
             "compute time : %.4f",
             time,
             state.dx, state.dy, state.dw,
             x(control_signal), y(control_signal), w(control_signal),
             state.x, state.y, state.w,
             compute_time);
  }

  inline
  double &RobotControl::x(RobotControl::Pose &pose)
  {
    return std::get<0>(pose);
  }

  inline
  double &RobotControl::y(RobotControl::Pose &pose)
  {
    return std::get<1>(pose);
  }

  inline
  double &RobotControl::w(RobotControl::Pose &pose)
  {
    return std::get<2>(pose);
  }

  inline
  bool &RobotControl::is_local(RobotControl::Pose &pose)
  {
    return std::get<3>(pose);
  }

  inline
  double &RobotControl::kP(RobotControl::PID &pid)
  {
    return std::get<0>(pid);
  }

  inline
  double &RobotControl::kI(RobotControl::PID &pid)
  {
    return std::get<1>(pid);
  }

  inline
  double &RobotControl::kD(RobotControl::PID &pid)
  {
    return std::get<2>(pid);
  }

  inline
  double &RobotControl::kPx(RobotControl::DribblerKP &kp)
  {
    return std::get<0>(kp);
  }

  inline
  double &RobotControl::kPy(RobotControl::DribblerKP &kp)
  {
    return std::get<1>(kp);
  }

  inline
  double &RobotControl::kPw(RobotControl::DribblerKP &kp)
  {
    return std::get<2>(kp);
  }

  inline
  double &RobotControl::kPx_min(RobotControl::DribblerKP &kp)
  {
    return std::get<3>(kp);
  }

}
