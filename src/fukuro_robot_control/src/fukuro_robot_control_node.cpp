#include <ros/ros.h>
#include "fukuro_common/RobotControl.h"
#include "fukuro_common/OdometryInfo.h"
#include "robotcontrol.h"
#include <dynamic_reconfigure/server.h>

int main(int argc, char** argv)
{
    ros::init(argc,argv,"fukuro_robot_control");
    ros::NodeHandle node;
    std::string control("time_optimal"); //time_optimal or pid
    ros::param::get("controller", control);
    auto controller = fukuro::RobotControl::PIDController;
    if(control == "pid")
        controller = fukuro::RobotControl::PIDController;
    fukuro::RobotControl robot_control(node, controller);
    std::string robot_name;
    if(!ros::get_environment_variable(robot_name,"FUKURO"))  {
        ROS_ERROR("robot name is empty!! export FUKURO=fukuro1");
        exit(-1);
    }
    ros::Subscriber robot_control_sub = node.subscribe(robot_name+"/robot_control",1,
                                                       &fukuro::RobotControl::updateTarget,
                                                       &robot_control);
    ros::Subscriber loc_sub = node.subscribe("/localization",1,
                                             &fukuro::RobotControl::updateRobotPose,
                                             &robot_control);
    ros::Subscriber planner_sub = node.subscribe("/path_planning",5,
                                                 &fukuro::RobotControl::updatePath,
                                                 &robot_control);
    ros::Subscriber odom_sub = node.subscribe(robot_name+"/odometry",1,
                                              &fukuro::RobotControl::updateRobotVel,
                                              &robot_control);
#ifndef FIRST_BUILD
    //dynamic_reconfigure::Server<fukuro_robot_control::FukuroRobotControlConfig> reconfigure_server;
    //reconfigure_server.setCallback(boost::bind(&fukuro::RobotControl::reconfigure,&robot_control,_1,_2));
#endif
    robot_control.loadYAMLSettings(ros::this_node::getName());
    ros::Rate loop(30);
    ros::AsyncSpinner spinner(3);
    spinner.start();
    while(ros::ok()) {
        robot_control.process();
        loop.sleep();
    }
    ros::waitForShutdown();
    return 0;
}
