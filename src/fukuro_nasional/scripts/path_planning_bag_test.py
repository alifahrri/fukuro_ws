#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import rospy
import os
from fukuro_common.msg import *
from fukuro_common.srv import *

robot_control_pub = None
kicker_service = None
strategy_info_pub = None

robot_control_msg = RobotControl()
strategy_info_msg = StrategyInfo()

strategy_state_list = None
ball_visible = False
local_ball_x = 0.0
local_ball_y = 0.0
robot_pose = (0.0,0.0,0.0)
role_list = ['defender', 'striker']
#robot_role = 'defender'
robot_role = 'striker'
last_ball_visible_time = rospy.Time()

home_pos = (0.0,0.0,0.0)
kick_pos = (3.0,0.0,0.0)

cari_bola_pos = [(-1.5,0.0,0.0),(1.5,0.0,0.0)]
waktu_cari_bola = 2.0
waypoint_cari_bola = 0
init_cari_bola = True
waktu_mulai_cari_bola = None

radius_dribble_auto_on = 0.3 #meter
angle_dribble_auto_on = 0.3 #radian
radius_mulai_dribble_bola = 0.2 #meter
angle_mulai_dribble_bola = 0.15 #radian

min_error_posisi_kick = 0.1 #meter
min_error_sudut_kick = 0.1 #radian

min_error_posisi_homing = 0.1 #meter
min_error_sudut_homing = 0.1 #radian

ball_shield_angle = 0.0

strategy_state = 'go_to_home'

def stop() :
#    rospy.loginfo('stop')
    go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)

def go_to_home() :
    global strategy_state
    rospy.loginfo('goto home')
    dx = home_pos[0]-robot_pose[0]
    dy = home_pos[1]-robot_pose[1]
    dw = home_pos[2]-robot_pose[2]
    error = math.sqrt(dx*dx+dy*dy)
    error_sudut = math.fabs(dw)
    if error < min_error_posisi_homing and error_sudut < min_error_sudut_homing :
        strategy_state = robot_role
#    go_to_pos(home_pos[0], home_pos[1], home_pos[2], planning=True)
    go_to_pos(home_pos[0], home_pos[1], ball_shield_angle, planning=True)

def defender() :
    global strategy_state

def striker() :
    global strategy_state
    strategy_state = 'cari_bola'

def go_to_home_cari_bola() :
    global strategy_state
    rospy.loginfo('goto home')
    go_to_pos(home_pos[0], home_pos[1], home_pos[2],planning=True)
    if ball_visible :
        strategy_state = 'dekati_bola'

def cari_bola() :
    global init_cari_bola, waktu_mulai_cari_bola, waypoint_cari_bola, strategy_state
    if ball_visible :
        strategy_state = 'dekati_bola'
    if init_cari_bola :
        waktu_mulai_cari_bola = rospy.get_rostime()
        init_cari_bola = False
    pos = cari_bola_pos[waypoint_cari_bola]
    go_to_pos(pos[0],pos[1],pos[2],planning=True)
    durasi_cari_bola = rospy.get_rostime() - waktu_mulai_cari_bola
    if durasi_cari_bola.secs > waktu_cari_bola :
        init_cari_bola = True
        waypoint_cari_bola = waypoint_cari_bola + 1
        if waypoint_cari_bola > 1 :
            waypoint_cari_bola = 0
    rospy.loginfo('cari bola')

def dekati_bola() :
    global strategy_state
    radius = math.sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y)
    angle = math.atan2(local_ball_y,local_ball_x)
    dribble = False
    if radius < radius_dribble_auto_on and angle < angle_dribble_auto_on :
        dribble = True
    if radius <= radius_mulai_dribble_bola and angle <= angle_mulai_dribble_bola :
        strategy_state = 'dribble_ball'
    if not ball_visible :
        strategy_state = 'cari_bola'
    go_to_pos(local_ball_x, local_ball_y, angle, local='local', dribbler=dribble)
    rospy.loginfo('dekati bola')

def dribble_ball() :
    global strategy_state
    dx = kick_pose[0]-robot_pose[0]
    dy = kick_pose[1]-robot_pose[1]
    dw = kick_pose[2]-robot_pose[2]
    error = math.sqrt(dx*dx+dy*dy)
    error_sudut = fabs(dw)
    radius_bola = math.sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y)
    angle_bola = math.atan2(local_ball_y,local_ball_x)
    if error < min_error_posisi_kick and error_sudut < min_error_sudut_kick :
        strategy_state = 'kick_ball'
    if radius_bola > radius_mulai_dribble_bola and angle_bola > angle_mulai_dribble_bola :
        strategy_state = 'dribble_ball'
    go_to_pos(kick_pos[0],kick_pos[1],ball_shield_angle,planning=True)
    rospy.loginfo('dribble ball')

def kick_ball() :
    global strategy_state
    kick()
    strategy_state = 'cari_bola'
    rospy.loginfo('kick ball')

strategy_list = {
    'stop' : stop,
    'go_to_home' : go_to_home,
    'go_to_home_cari_bola' : go_to_home_cari_bola,
    'cari_bola' : cari_bola,
    'dekati_bola' : dekati_bola,
    'dribble_ball' : dribble_ball,
    'kick_ball' : kick_ball,
    'defender' : defender,
    'striker' : striker
}

#################################################################################################

def handle_strategy_request(req) :
    global strategy_state, home_pos, kick_pos, cari_bola_pos, robot_role
    ok = 0
    for s, f in strategy_list.items() :
        if req.strategy_state == s :
            strategy_state = req.strategy_state
            ok = 1
    if '--kick_pos' in req.option or '-k' in req.option :
        kick_pos = (req.kick_pos.x, req.kick_pos.y, req.kick_pos.theta)
    if '--home_pos' in req.option or '-h' in req.option :
        home_pos = (req.home_pos.x, req.home_pos.y, req.home_pos.theta)
    if req.role in role_list :
        robot_role = req.role
    rospy.loginfo('strategy request %s %s %s %s' %(strategy_state,kick_pos,home_pos,cari_bola_pos))
    return StrategyServiceResponse(ok)

def update_state() :
    strategy_list[strategy_state]()
#    rospy.loginfo('update state')

def go_to_pos(x,y,w,local='',dribbler=False,planning=False):
    global robot_control_msg
    robot_control_msg.target_pose.x = x
    robot_control_msg.target_pose.y = y
    robot_control_msg.target_pose.theta = w
    robot_control_msg.plan = planning
    if local is 'local':
        robot_control_msg.option.data = 'local'
    else :
        robot_control_msg.option.data = ''
    if dribbler :
        robot_control_msg.dribbler = 1
    else :
        robot_control_msg.dribbler = 0

def world_model_callback(msg) :
    global ball_visible, local_ball_x, local_ball_y, robot_pose, last_ball_visible_time
    now = rospy.get_rostime()
    if msg.ball_visible :
        ball_visible = True
        last_ball_visible_time = now
    elif now - last_ball_visible_time > rospy.Duration(1) :
        ball_visible = False
    local_ball_x = msg.local_balls_kf.x
    local_ball_y = msg.local_balls_kf.y
    robot_pose = (msg.pose.x, msg.pose.y, msg.pose.theta)
#    rospy.loginfo('world model callback : %s %f %f' %(msg.robot_name,local_ball_x,local_ball_y))

def strategy_pos_callback(msg) :
    global ball_shield_angle
    ball_shield_angle = msg.ball_shield_angle

def robot_control_info_callback(msg) :
    global error_radius, error_angle
    error_radius = msg.error_radius
    error_angle = msg.error_angle

def kick():
    try:
        global kicker_service
        respl = kicker_service(1)
    except rospy.ServiceException, e:
        rospy.logerr('Kick service call failed: %s' %e)

def publish():
    global robot_control_pub
    global robot_control_msg
    global strategy_info_pub
    global strategy_info_msg
    strategy_info_msg.strategy_state = strategy_state
    robot_control_pub.publish(robot_control_msg)
    strategy_info_pub.publish(strategy_info_msg)

def strategy():
    robot_name = os.getenv('FUKURO')
    rospy.init_node('fukuro_strategy_nasional')
#    print('waiting for service')
#    rospy.wait_for_service(robot_name+'/kick_service')
    global kicker_service
    global robot_control_pub
    global strategy_info_pub
    kicker_service = rospy.ServiceProxy(robot_name+'/kick_service',Shoot)
    robot_control_pub = rospy.Publisher(robot_name+'/robot_control',RobotControl,queue_size=1)
    strategy_info_pub = rospy.Publisher(robot_name+'/strategy_info',StrategyInfo,queue_size=1)
    strategy_server = rospy.Service(robot_name+'/strategy_service',StrategyService,handle_strategy_request)
    rospy.Subscriber(robot_name+'/world_model', WorldModel, world_model_callback)
    rospy.Subscriber('/strategy_pos', StrategyPositioning, strategy_pos_callback)
    print 'ready'
    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
#        rospy.loginfo('strategy loop')
        update_state()
        publish()
        rate.sleep()

if __name__ == "__main__":
    strategy()
