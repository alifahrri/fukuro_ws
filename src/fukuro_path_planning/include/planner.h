#ifndef PLANNER_H
#define PLANNER_H

#include <ros/ros.h>
#include "robotmap.h"
#include "fukuro_common/Obstacles.h"
#include "fukuro_common/PlannerInfo.h"
#include "fukuro_common/Localization.h"
#include "fukuro_common/PlannerInfoService.h"
#include "fukuro_common/PlannerService.h"
#include "fukuro_common/WorldModel.h"

namespace fukuro {
class Planner
{
public:
  Planner(ros::NodeHandle node);
  void updateWorld(const fukuro_common::WorldModel::ConstPtr& world);
  /*
  void updateLoc(const fukuro_common::Localization::ConstPtr& loc);
  void update(const fukuro_common::Obstacles::ConstPtr& obstacles);
  */
  bool infoServer(fukuro_common::PlannerInfoService::Request& req, fukuro_common::PlannerInfoService::Response& res);
  bool setGoal(fukuro_common::PlannerService::Request& req, fukuro_common::PlannerService::Response& res);
  void publish();
  void solve();
private:
  RobotMap* map;
  ros::Publisher planner_pub;
  boost::mutex mutex;
};
}

#endif 
