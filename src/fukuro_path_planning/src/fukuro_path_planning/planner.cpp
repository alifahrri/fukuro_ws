#include "planner.h"

#define FIELD_WIDTH 1100
#define FIELD_HEIGHT 800
#define GRID_SIZE 10
#define OBSTACLE_RADIUS 60
//#define TEST

fukuro::Planner::Planner(ros::NodeHandle node)
{
  map = new RobotMap(FIELD_WIDTH,FIELD_HEIGHT,GRID_SIZE,"thetastar");
  map->setGoal(0.0,0.0);
  planner_pub = node.advertise<fukuro_common::PlannerInfo>("/path_planning",10);
#ifdef TEST
  map->addCircleObstacle(0,0,50);
#endif
}

void fukuro::Planner::updateWorld(const fukuro_common::WorldModel::ConstPtr &world)
{
  mutex.lock();
  ROS_INFO("update world");
  auto start_x = world->pose.x*100.0;
  auto start_y = world->pose.y*100.0;
  if(start_x>(FIELD_WIDTH/2.0))
    start_x = FIELD_WIDTH/2.0-GRID_SIZE;
  else if(start_x<(-FIELD_WIDTH/2.0))
    start_x = -(FIELD_WIDTH/2.0-GRID_SIZE);
  if(start_y>(FIELD_HEIGHT/2.0))
    start_y = FIELD_HEIGHT/2.0-GRID_SIZE;
  else if(start_y<(-FIELD_HEIGHT/2.0))
    start_y = -(FIELD_HEIGHT/2.0-GRID_SIZE);
  map->setStart(start_x,
                start_y);
  map->clearObstacles();
  map->addSquareObstacle(-450,-100,40);
  map->addSquareObstacle(-470,-100,40);
  map->addSquareObstacle(-490,-100,40);
  map->addSquareObstacle(-510,-100,40);
  map->addSquareObstacle(-530,-100,40);
  map->addSquareObstacle(-550,-100,40);
  map->addSquareObstacle(-450,100,40);
  map->addSquareObstacle(-470,100,40);
  map->addSquareObstacle(-490,100,40);
  map->addSquareObstacle(-510,100,40);
  map->addSquareObstacle(-530,100,40);
  map->addSquareObstacle(-550,100,40);
  map->addSquareObstacle(450,-100,40);
  map->addSquareObstacle(470,-100,40);
  map->addSquareObstacle(490,-100,40);
  map->addSquareObstacle(510,-100,40);
  map->addSquareObstacle(530,-100,40);
  map->addSquareObstacle(550,-100,40);
  map->addSquareObstacle(450,100,40);
  map->addSquareObstacle(470,100,40);
  map->addSquareObstacle(490,100,40);
  map->addSquareObstacle(510,100,40);
  map->addSquareObstacle(530,100,40);
  map->addSquareObstacle(550,100,40);
  for(size_t i=0; i<world->obstacles.size(); i++)
  {
    map->addCircleObstacle(world->obstacles.at(i).x*100.0,
                           world->obstacles.at(i).y*100.0,
                           OBSTACLE_RADIUS);
  }
  mutex.unlock();
}

/*
void fukuro::Planner::updateLoc(const fukuro_common::Localization::ConstPtr &loc)
{
  ROS_INFO("update start");
  map->setStart(loc->belief.x,
                loc->belief.y);
}

void fukuro::Planner::update(const fukuro_common::Obstacles::ConstPtr &obstacles)
{
  ROS_INFO("update obstacles");
  map->clearObstacles();
  for(size_t i=0; i<obstacles->obstacles.size(); i++)
  {
    map->addCircleObstacle(obstacles->obstacles.at(i).x,
                           obstacles->obstacles.at(i).y,
                           OBSTACLE_RADIUS);
  }
}
*/

bool fukuro::Planner::infoServer(fukuro_common::PlannerInfoService::Request &req, fukuro_common::PlannerInfoService::Response &res)
{
  res.width = map->width();
  res.height = map->height();
  res.grid_size = map->gridSize();
  return true;
}

bool fukuro::Planner::setGoal(fukuro_common::PlannerService::Request &req, fukuro_common::PlannerService::Response &res)
{
  mutex.lock();
  map->setGoal(req.goal.x,req.goal.y);
  res.ok = 1;
  mutex.unlock();
  return true;
}

void fukuro::Planner::publish()
{
  mutex.lock();
  fukuro_common::PlannerInfo msg;
  auto solution = map->getSolution();
  auto obstacles = map->getObstacles();
  auto start = map->startPos();
  auto goal  = map->goalPos();
  for(const auto& s : solution)
  {
    fukuro_common::Point2d sol;
    sol.x = s.first;
    sol.y = s.second;
    msg.solutions.push_back(sol);
  }
  for(const auto& o : obstacles)
  {
    fukuro_common::Point2d obs;
    obs.x = o.first;
    obs.y = o.second;
    msg.obstacles.push_back(obs);
  }
  msg.start.x = start.first;
  msg.start.y = start.second;
  msg.goal.x = goal.first;
  msg.goal.y = goal.second;
  msg.search_time = map->lastSolveTime();
  msg.solved = map->solved();
  mutex.unlock();
  planner_pub.publish(msg);
}

void fukuro::Planner::solve()
{
  mutex.lock();
  ROS_INFO("solve");
  map->spinOnce();
  mutex.unlock();
}
