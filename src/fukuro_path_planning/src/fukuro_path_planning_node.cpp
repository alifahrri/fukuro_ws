#include <ros/ros.h>
#include "planner.h"

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_path_planning");
  ros::NodeHandle node;
  fukuro::Planner planner(node);
  std::string robot_name;
  if(!ros::get_environment_variable(robot_name,"FUKURO"))
  {
    ROS_ERROR("robot name empty! export FUKURO=fukuro1");
    exit(-1);
  };
  ros::Subscriber world_sub = node.subscribe(robot_name+"/world_model",5,&fukuro::Planner::updateWorld,&planner);
//  ros::Subscriber localization_sub = node.subscribe("/localization",10,&fukuro::Planner::updateLoc,&planner);
//  ros::Subscriber obstacle_sub = node.subscribe("/omnivision/Obstacles",10,&fukuro::Planner::update,&planner);
  ros::ServiceServer planner_info_server = node.advertiseService("/planner_info_service",&fukuro::Planner::infoServer,&planner);
  ros::ServiceServer planner_server = node.advertiseService("/planner_service",&fukuro::Planner::setGoal,&planner);
  ros::AsyncSpinner spinner(2);
  spinner.start();
  ros::Rate loop(33);
  while(ros::ok())
  {
    planner.solve();
    planner.publish();
    loop.sleep();
  }
  ros::waitForShutdown();
  return 0;
}
