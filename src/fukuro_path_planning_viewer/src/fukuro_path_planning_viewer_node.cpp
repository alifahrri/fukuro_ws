#include <ros/ros.h>
#include <QApplication>
#include "plannerdialog.h"

int main(int argc, char **argv)
{
  ros::init(argc,argv,"fukuro_path_planning_viewer");
  QApplication app(argc,argv);
  ros::NodeHandle node;
  PlannerDialog dialog;
  ros::Subscriber planner_sub = node.subscribe("/path_planning",5,&PlannerDialog::updatePlanner,&dialog);
  ros::AsyncSpinner spinner(2);
  spinner.start();
  dialog.showMaximized();
  app.exec();
  return 0;
}
