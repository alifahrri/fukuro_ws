#include "plannerdialog.h"
#include "ui_plannerdialog.h"
#include "fukuro_common/PlannerInfoService.h"
#include <QDebug>
#include <QGraphicsSceneMouseEvent>

#define ANGLE_INCREMENT 3

PlannerDialog::PlannerDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::PlannerDialog),
  field(new Field),
  grid(new Grid)
{
  ui->setupUi(this);
  ROS_INFO("Waiting for service...");
  ros::service::waitForService("/planner_info_service");
  fukuro_common::PlannerInfoService info;
  if(ros::service::call("/planner_info_service",info))
  {
    double width = info.response.width;
    double height = info.response.height;
    double grid_size = info.response.grid_size;
    grid->width = width;
    grid->height = height;
    grid->grid_size = grid_size;
    std::cout << "width : " << width << '\n';
    std::cout << "height : " << height << '\n';
    std::cout << "grid size : " << grid_size << '\n';
    QColor unvisited(Qt::lightGray);
    unvisited.setAlphaF(0.15);
    QRectF rect0(QPointF(-grid_size/2.0,-grid_size/2.0),
                 QPointF(grid_size/2.0,grid_size/2.0));
    for(int i=-width/2; i<=width/2.0; i=i+grid_size)
      for(int j=-height/2; j<=height/2.0; j=j+grid_size)
      {
        rect0.moveCenter(QPointF(i,-j));
        grid->rect.push_back(Grid::Vertex(rect0,unvisited));
      }
  }

  grid->goal_callback = [](double goal_x, double goal_y)
  {
    fukuro_common::PlannerService goal;
    goal.request.goal.x = goal_x;
    goal.request.goal.y = goal_y;
    if(ros::service::call("/planner_service",goal))
      ROS_INFO("set goal success");
    else
      ROS_INFO("set goal failed");
  };

  ui->graphicsView->setScene(new QGraphicsScene(-FIELD_WIDTH/2-100,-FIELD_HEIGHT/2-100,FIELD_WIDTH+200,FIELD_HEIGHT+200,this));
  ui->graphicsView->scene()->addItem(field);
  ui->graphicsView->scene()->addItem(grid);
  ui->graphicsView->scale(0.8,0.8);
  ui->graphicsView->setRenderHint(QPainter::Antialiasing);
  ui->graphicsView->setBackgroundBrush(Qt::darkGreen);
  this->setFixedSize(this->size()+QSize(100,100));
  this->setWindowTitle("Fukuro Path Planner Viewer");
}

PlannerDialog::~PlannerDialog()
{
  delete ui;
}

PlannerDialog::Field::Field()
{
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE7,YLINE1)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE6),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE1,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE7,YLINE1),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE4,YLINE1),
                         QPointF(XLINE4,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE2,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE6,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE3,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE5,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE1,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE4),
                         QPointF(XLINE1,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE1,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE5),
                         QPointF(XLINE1,YLINE5)));

  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE7,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE4),
                         QPointF(XLINE7,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE7,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE5),
                         QPointF(XLINE7,YLINE5)));

  center_circle.setLeft(-CENTER_CIRCLE_RADIUS);
  center_circle.setTop(-CENTER_CIRCLE_RADIUS);
  center_circle.setHeight(2*CENTER_CIRCLE_RADIUS);
  center_circle.setWidth(2*CENTER_CIRCLE_RADIUS);

  QPointF circle_line0(center_circle.width()/2,0.0);
  QLineF line(QPointF(0.0,0.0),circle_line0);
  circle_lines.push_back(QLineF(circle_line0,line.p2()));
  for(int i=ANGLE_INCREMENT; i<=360; i+=ANGLE_INCREMENT)
  {
      line.setAngle(i);
      QPointF p0 = circle_lines.back().p2();
      circle_lines.push_back(QLineF(p0,line.p2()));
  }
}

void PlannerDialog::updatePlanner(const fukuro_common::PlannerInfo::ConstPtr &msg)
{
  std::vector<std::pair<double,double>> obstacles;
  std::vector<std::pair<double,double>> solution;
  auto start = msg->start;
  auto goal = msg->goal;
  for(size_t i=0; i<msg->obstacles.size(); i++)
    obstacles.push_back(std::make_pair(msg->obstacles.at(i).x,
                                       msg->obstacles.at(i).y));
  for(size_t i=0; i<msg->solutions.size(); i++)
    solution.push_back(std::make_pair(msg->solutions.at(i).x,
                                      msg->solutions.at(i).y));
  grid->resetColor();
  grid->setObstacles(obstacles);
  grid->setSolution(solution);
  grid->setStart(start.x,start.y);
  grid->setGoal(goal.x,goal.y);
  grid->update();
}

void PlannerDialog::Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::transparent);
  painter->setBrush(Qt::green);
  painter->drawRect(boundingRect());
  painter->setPen(QPen(Qt::white,3.3));
  painter->setBrush(Qt::transparent);
  painter->drawLines(lines);
  painter->drawEllipse(center_circle);
}

QRectF PlannerDialog::Field::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

PlannerDialog::Grid::Grid() :
  goal_callback(NULL)
{

}

void PlannerDialog::Grid::setColor(double x, double y, QColor color)
{
  int column_count = width/grid_size+1;
  int row_count = height/grid_size+1;
  int idx = ((int)(x/grid_size+width/2.0/grid_size))*row_count+((int)(y/grid_size+height/2.0/grid_size));
  rect[idx].second = color;
  changed_rect.push_back(idx);
}

void PlannerDialog::Grid::setStart(double x, double y)
{
  setColor(x,y,QColor(Qt::red));
}

void PlannerDialog::Grid::setGoal(double x, double y)
{
  setColor(x,y,QColor(Qt::blue));
}

void PlannerDialog::Grid::setObstacles(std::vector<std::pair<double, double> > obs)
{
  for(auto& o : obs)
    setColor(o.first,o.second,QColor(Qt::black));
}

void PlannerDialog::Grid::setSolution(std::vector<std::pair<double, double> > sol)
{
  solutions.clear();
  int column_count = width/grid_size+1;
  int row_count = height/grid_size+1;
  QPointF p0;
  if(sol.size())
  {
    double x = sol.at(0).first;
    double y = sol.at(0).second;
    int idx = ((int)(x/grid_size+width/2.0/grid_size))*row_count+((int)(y/grid_size+height/2.0/grid_size));
    p0 = QPointF(rect[idx].first.center());
  }
  for(auto& s : sol)
  {
    double x = s.first;
    double y = s.second;
    int idx = ((int)(x/grid_size+width/2.0/grid_size))*row_count+((int)(y/grid_size+height/2.0/grid_size));
    QPointF p1 = rect[idx].first.center();
    solutions.push_back(QLineF(p0,p1));
    setColor(s.first, s.second, QColor(Qt::white));
    p0 = p1;
  }
}

void PlannerDialog::Grid::resetColor()
{
  QColor unvisited(Qt::lightGray);
  unvisited.setAlphaF(0.1);
  for(auto i : changed_rect)
    rect[i].second = unvisited;
  changed_rect.clear();
}

void PlannerDialog::Grid::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::white);
  for(size_t i=0; i<rect.size(); i++)
  {
    auto r = rect[i];
    painter->setBrush(r.second);
    painter->drawRect(r.first);
  }
  painter->setPen(Qt::darkBlue);
  painter->drawLines(solutions);
}

QRectF PlannerDialog::Grid::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

void PlannerDialog::Grid::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  if(goal_callback)
    goal_callback(event->pos().x(),-event->pos().y());
}
