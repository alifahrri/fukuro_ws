#ifndef FIELDITEM_H
#define FIELDITEM_H

#include "fukuro/core/field.hpp"

#include <QGraphicsItem>

class FieldItem : public QGraphicsItem
{
public:
    FieldItem();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;

private:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
#ifndef MSL_FIELD
    float centerCircleRadius;
    float horizontalLine;
    float verticalLine;
    float arcRadius;
    float arcWidth;
#else
    QVector<QLineF> lines;
    QRectF center_circle;
#endif
};

#endif // FIELDITEM_H
