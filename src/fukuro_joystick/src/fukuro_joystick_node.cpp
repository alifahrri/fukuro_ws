#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include "fukuro_common/HWControllerCommand.h"
#include "fukuro_common/Shoot.h"
#include <cmath>

#define MAX_SPEED (1.5)

namespace fukuro {
  class JoystickController {
  public:
    JoystickController(ros::NodeHandle &node);
    void joyCallback(const sensor_msgs::JoyConstPtr &joy);
  public:
    std::string robot_name;
  private:
    ros::Publisher hwctrl_pub;
  };

  JoystickController::JoystickController(ros::NodeHandle &node)
  {
    ros::get_environment_variable(robot_name, "FUKURO");
    hwctrl_pub = node.advertise<fukuro_common::HWControllerCommand>(robot_name+"/hwcontrol_command",1);
  }

  void JoystickController::joyCallback(const sensor_msgs::JoyConstPtr &joy)
  {
    fukuro_common::HWControllerCommand hwctrl_msg;
    auto x_axis = joy->axes[1];
    auto y_axis = joy->axes[0];
    auto x_btn = joy->axes[6];
    auto y_btn = joy->axes[5];

    hwctrl_msg.vel.Vx = (x_btn>0.0 ? 1.0 : (x_btn<0.0 ? -1.0 : x_axis));
    hwctrl_msg.vel.Vy = (y_btn>0.0 ? 1.0 : (y_btn<0.0 ? -1.0 : y_axis));

    if(joy->buttons[3]) {
        hwctrl_msg.dribbler.dir_in = 1;
        hwctrl_msg.dribbler.speed = 64;
      }

    if(joy->buttons[4])
      hwctrl_msg.vel.w = 0.25;
    else if(joy->buttons[5])
      hwctrl_msg.vel.w = -0.25;
    else if(joy->buttons[6])
      hwctrl_msg.vel.w = 0.75;
    else if(joy->buttons[7])
      hwctrl_msg.vel.w = -0.75;

    if(joy->buttons[2]) {
        fukuro_common::ShootRequest req;
        fukuro_common::ShootResponse res;
        fukuro_common::Shoot service;
        service.request = req;
        service.response = res;
        req.kick_request = 2;
        ros::service::call(robot_name+"/kick_service",service);
      }
    hwctrl_pub.publish(hwctrl_msg);
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "fukuro_joystick_node");
  ros::NodeHandle nh;
  fukuro::JoystickController joystick(nh);
  ros::Subscriber joy_sub = nh.subscribe("joy",1,&fukuro::JoystickController::joyCallback,&joystick);
  ros::spin();
  return 0;
}
