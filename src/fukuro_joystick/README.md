## fukuro_joystick  
transform joystick message to fukuro hwcontroller command  

### Launcer  
- `fukuro_joy.launch` : launch `fukuro_joystick_node` and `joy_node`; 

### Nodes  
- `fukuro_joystick_node` : translates joystick message to hardware controller command;  

### Environment Variable
- `$FUKURO` : robot name, required

### Subscribed Topics 
- `/joy` : this topic is published by `joy_node`  

### Published Topics
- `/$FUKURO/hwcontrol_command` : translated joy message;  

### Service Client  
- `$FUKURO/kick_service` : service called if `joy->buttons[2]` is pressed. This service should be provided by the hardware controller.   