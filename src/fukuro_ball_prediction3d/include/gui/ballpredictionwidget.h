#ifndef BALLPREDICTIONWIDGET_H
#define BALLPREDICTIONWIDGET_H

#if 1
#include <QWidget>
#include <QGraphicsItem>
#include <QtCore>
#else
#include <qt5/QtWidgets/QWidget>
#include <qt5/QtWidgets/QGraphicsItem>
#include <qt5/QtCore/QtCore>
#endif

#include "fukuro/core/field.hpp"
#include "fukuro_common/BallPrediction.h"
#include "fukuro_common/BallPrediction3d.h"
#include "fukuro_common/InterceptPoint.h"
#include "qcustomplot.h"

namespace Ui {
class BallPredictionWidget;
}

class BallPredictionWidget : public QWidget
{
  Q_OBJECT

public:
  explicit BallPredictionWidget(QWidget *parent = 0);
  void update(const fukuro_common::BallPrediction3d::ConstPtr &msg);
  void updateInterceptPoint(const fukuro_common::InterceptPoint::ConstPtr &msg);
  ~BallPredictionWidget();

private:
  class Field : public QGraphicsItem
  {
  public:
    Field();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

  private:
    QVector<QLineF> lines;
    QVector<QLineF> circle_lines;
    QRectF center_circle;
  };

  class Prediction3D : public QGraphicsItem
  {
  public:
    Prediction3D();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    QVector<QPointF> balls;
    QPointF p0;
    QPointF p1;
  };

  class InterceptPoint : public QGraphicsItem
  {
  public:
    InterceptPoint();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    QPointF pt;
  };

private:
  Ui::BallPredictionWidget *ui;
  QCPGraph *xt_predict_graph;
  QCPGraph *yt_predict_graph;
  QCPGraph *zt_predict_graph;
  Prediction3D *ball_model;
  QCPGraph *xt_graph;
  QCPGraph *yt_graph;
  QCPGraph *zt_graph;
  Field *field;
  InterceptPoint *intercept_pt;
};

#endif // BALLPREDICTIONWIDGET_H
