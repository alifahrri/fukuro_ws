#ifndef BALLREGRESSION3D
#define BALLREGRESSION3D

#include <vector>
#include <ros/ros.h>
#include <fukuro_common/FrontVision3d.h>
#include <fukuro_common/Localization.h>
#include <fukuro_common/RGBDVision.h>

class BallRegression3D
{
public:
  struct Pos3DStamped
  {
    double x;
    double y;
    double z;
    double time;
  };

  struct Pose2D
  {
    double x;
    double y;
    double w;
  };

  struct LinearModel
  {
    double a;
    double b;
    double operator()(double x);
  };

  struct QuadraticModel
  {
    double a;
    double b;
    double c;
    double operator()(double x);
  };

  struct BallModel3D
  {
    LinearModel x;
    LinearModel y;
    QuadraticModel z;
    Pos3DStamped operator()(double t);
  };

public:
  BallRegression3D(ros::NodeHandle &node);
  void processFrontVis(const fukuro_common::FrontVision3dConstPtr &vision);
  void processRGBD(const fukuro_common::RGBDVisionConstPtr &vision);
  void processMCL(const fukuro_common::LocalizationConstPtr &mcl);
  void process();
  void publish();

private:
  std::vector<Pos3DStamped> ball_pos;
  ros::Publisher pub;
  BallModel3D ball_model;
  Pose2D robot_pose;
  size_t n_buffer = 30;
  ros::Time last_update;
  bool localized = false;
};

#endif // BALLREGRESSION3D
