#include <ros/ros.h>
#include <QApplication>
#include <QTimer>
#include "gui/ballpredictionwidget.h"
//#include "ballpredictionwidget.h"

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_ball_prediction3d_gui");
  ros::NodeHandle node;

  QApplication app(argc,argv);
  BallPredictionWidget widget;

  std::string robot;
  ros::get_environment_variable(robot,"FUKURO");
  ros::Subscriber sub = node.subscribe(robot+"/ball_prediction3d",1,&BallPredictionWidget::update,&widget);
  ros::Subscriber intercept_sub = node.subscribe(robot+"/goalie_intercept",1,&BallPredictionWidget::updateInterceptPoint,&widget);

  ros::AsyncSpinner spinner(1);

  widget.show();
  spinner.start();

  QTimer timer;
  timer.connect(&timer,&QTimer::timeout,[&]{
      if(!ros::ok())
        widget.close();
    });
  timer.start(1000);

  return app.exec();
}
