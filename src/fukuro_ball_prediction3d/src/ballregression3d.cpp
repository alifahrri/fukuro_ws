#include "ballregression3d.h"
#include "fukuro_common/BallPrediction3d.h"
#include <eigen3/Eigen/Dense>

#define DEG2RAD M_PI/180.0
#define MAX_ERROR (100.0) //cm

BallRegression3D::BallRegression3D(ros::NodeHandle &node)
{
  std::string robot;
  ros::get_environment_variable(robot,"FUKURO");
  pub = node.advertise<fukuro_common::BallPrediction3d>(robot+"/ball_prediction3d",3);
}

void BallRegression3D::processRGBD(const fukuro_common::RGBDVisionConstPtr &vision)
{
  if(!localized || !vision->balls.size())
    return;
  auto c = cos(robot_pose.w*DEG2RAD);
  auto s = sin(robot_pose.w*DEG2RAD);
  auto bx = vision->balls.at(0).x;
  auto by = vision->balls.at(0).y;
  auto bz = vision->balls.at(0).z;
  auto gx = c*bx - s*by + robot_pose.x;
  auto gy = s*bx + c*by + robot_pose.y;
  auto gz = bz;
  auto now = ros::Time::now().toSec();
  Pos3DStamped ball({gx,gy,gz,now});
  if(ball_pos.size()>n_buffer)
    ball_pos.erase(ball_pos.begin());
  ball_pos.push_back(ball);
  ROS_INFO("Process RGBD : ball(%f,%f,%f)->%f", ball.x, ball.y, ball.z, ball.time);
  last_update = ros::Time::now();
}

void BallRegression3D::processFrontVis(const fukuro_common::FrontVision3dConstPtr &vision)
{
  if(!localized)
    return;
  else if(!vision->ball.size()) {
      if(ball_pos.size())
        ball_pos.erase(ball_pos.begin());
      return;
    }
  auto c = cos(robot_pose.w*DEG2RAD);
  auto s = sin(robot_pose.w*DEG2RAD);
  auto ball = vision->ball.at(0);
  auto bx = ball.x*100.0;
  auto by = ball.y*100.0;
  auto bz = ball.z*100.0;
  auto gx = c*bx - s*by + robot_pose.x;
  auto gy = s*bx + c*by + robot_pose.y;
  auto gz = bz;
  auto now = ros::Time::now().toSec();
  Pos3DStamped ball3d({gx,gy,gz,now});
  if(ball_pos.size()<2) {
    ball_pos.push_back(ball3d);
  } 
  else {
    auto abs_error_sum = 0.0;
    auto n = ball_pos.size();
    for(size_t i=0; i<n; ++i) {
      const auto& ball = ball_pos.at(i);
      auto error = std::hypot(ball.x-ball3d.x, ball.y-ball3d.y);
      abs_error_sum += error;
    }
    auto avg_error = abs_error_sum / n;
    if(avg_error < MAX_ERROR) {
      if(ball_pos.size()>n_buffer)
        ball_pos.erase(ball_pos.begin());
      ball_pos.push_back(ball3d);
    }
    else {
      ball_pos.erase(ball_pos.begin());
    }
  }
  ROS_INFO("Process Front Vision : ball(%.3f,%.3f,%.3f)",ball.x,ball.y,ball.z);
  last_update = ros::Time::now();
}

void BallRegression3D::processMCL(const fukuro_common::LocalizationConstPtr &mcl)
{
  localized = true;
  robot_pose.x = mcl->belief.x;
  robot_pose.y = mcl->belief.y;
  robot_pose.w = mcl->belief.theta;
  ROS_INFO("Process Localization : robot_pose(%f,%f,%f)",robot_pose.x,robot_pose.y,robot_pose.w);
}

void BallRegression3D::process()
{
  if(ball_pos.size()<2)
    return;

  //x is time, y is ball's x pos, y pos or z pos
  double sy[3]  = {0.0,0.0,0.0};    //sum of ball's x, y and z values;
  double sxy[3] = {0.0,0.0,0.0};    //sum of ball's x*t, y*t, and z*t values
  double sx[4] = {0.0,0.0,0.0,0.0}; //sum of ball's t, t*t, t*t*t, and t*t*t*t
  double sxxy = 0.0;                //sum of ball's z*z*t values

  auto n  = ball_pos.size();
  auto t0 = ball_pos.at(0).time;
  for(size_t i=0; i<ball_pos.size(); i++) {
      const auto t = ball_pos.at(i).time-t0;
      const auto& x = ball_pos.at(i).x;
      const auto& y = ball_pos.at(i).y;
      const auto& z = ball_pos.at(i).z;
      //      sx  += t;
      //      sqx += t*t;
      auto t2 = t*t;
      auto t3 = t2*t;
      auto t4 = t3*t;
      sx[0] += t;
      sx[1] += t2;
      sx[2] += t3;
      sx[3] += t4;

      sy[0] += x;
      sy[1] += y;
      sy[2] += z;

      sxy[0] += t*x;
      sxy[1] += t*y;
      sxy[2] += t*z;

      sxxy = t*z*z;
      const auto& ball = ball_pos.at(i);
      ROS_INFO("process : ball_pos(%.2f,%.2f,%.2f,%.2f)", ball.x, ball.y, ball.z, ball.time);
    }
  auto sxsx = sx[0]*sx[0];
  auto tmp = (n*sx[1]-sxsx);
  ball_model.x.a = (sy[0]*sx[1] - sx[0]*sxy[0])/tmp;
  ball_model.x.b = (n*sxy[0] - sx[0]*sy[0])/tmp;
  ball_model.y.a = (sy[1]*sx[1] - sx[0]*sxy[1])/tmp;
  ball_model.y.b = (n*sxy[1] - sx[0]*sy[1])/tmp;

  Eigen::Matrix3d A;
  Eigen::Vector3d b;

  A <<  n,     sx[0], sx[1],
        sx[0], sx[1], sx[2],
        sx[1], sx[2], sx[3];
  b << sy[2], sxy[2], sxxy;

  Eigen::ColPivHouseholderQR<Eigen::Matrix3d> dec(A);
  Eigen::Vector3d v = dec.solve(b);

  ball_model.z.a = v[0];
  ball_model.z.b = v[1];
  ball_model.z.c = v[2];

  ROS_INFO("ball_model(%.2f,%.2f,%.2f,%.2f)",ball_model.x.a,ball_model.x.b,ball_model.y.a,ball_model.y.b);
}

void BallRegression3D::publish()
{
  fukuro_common::BallPrediction3d msg;
  if(ball_pos.size()<2)
    goto PUBLISH;
  msg.ax = ball_model.x.a;
  msg.bx = ball_model.x.b;
  msg.ay = ball_model.y.a;
  msg.by = ball_model.y.b;
  msg.az = ball_model.z.a;
  msg.bz = ball_model.z.b;
  msg.cz = ball_model.z.c;
  {
    auto ti = ball_pos.front().time;
    auto tf = ball_pos.back().time;
    auto p1 = ball_model(tf-ti+1.0);
    auto p2 = ball_model(tf-ti+2.0);
    auto p3 = ball_model(tf-ti+3.0);
    msg.p1s.x = p1.x;
    msg.p1s.y = p1.y;
    msg.p2s.x = p2.x;
    msg.p2s.y = p2.y;
    msg.p3s.x = p3.x;
    msg.p3s.y = p3.y;
  }
PUBLISH :
  for(size_t i=0; i<ball_pos.size(); i++) {
      auto ball = ball_pos.at(i);
      fukuro_common::Point3dStamped p;
      p.x = ball.x;
      p.y = ball.y;
      p.z = ball.z;
      p.t = ball.time;
      msg.pos.push_back(p);
    }
  msg.last_ball.data = last_update;
  pub.publish(msg);
  ROS_INFO("ball model published!");
}

double BallRegression3D::LinearModel::operator()(double x)
{
  return b*x+a;
}

double BallRegression3D::QuadraticModel::operator()(double x)
{
  return c*x*x+b*x+a;
}

BallRegression3D::Pos3DStamped BallRegression3D::BallModel3D::operator()(double t)
{
  return Pos3DStamped({x(t),y(t),z(t),t});
}
