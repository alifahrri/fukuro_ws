#include <ros/ros.h>
#include "ballregression3d.h"

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_ball_prediction2d");
  ros::NodeHandle node;
  BallRegression3D ball_regression(node);

  ros::Subscriber loc_sub = node.subscribe("/localization",1,&BallRegression3D::processMCL,&ball_regression);
  ros::Subscriber frontvis_sub = node.subscribe("/front_vision/3d",1,&BallRegression3D::processFrontVis,&ball_regression);
  ros::Rate rate(30.0);
  ros::AsyncSpinner spinner(1);
  spinner.start();
  while(ros::ok()) {
      ball_regression.process();
      ball_regression.publish();
      rate.sleep();
    }
  return 0;
}
