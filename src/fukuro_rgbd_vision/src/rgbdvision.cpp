#include "rgbdvision.h"

#define DOWNSCALE           30
#define IMG_WIDTH_ORI       1920
#define IMG_HEIGHT_ORI      1080
#define IMG_WIDTH_RESIZED   854
#define IMG_HEIGHT_RESIZED  480
#define DEPTH_WIDTH         640
#define DEPTH_HEIGHT        480
#define ALIGN_SHIFT         (IMG_WIDTH_RESIZED-DEPTH_WIDTH)

using namespace fukuro;
using std::string;
namespace encodings=sensor_msgs::image_encodings;

RGBDVision::RGBDVision(ros::NodeHandle &node)
{
  last_updated = ros::Time::now();
  string fukuro;
  string user;
  string agent;
  string fukuro_ws;
  ros::get_environment_variable(fukuro,"FUKURO");
  ros::get_environment_variable(user,"USER");
  ros::get_environment_variable(agent,"AGENT");
  ros::get_environment_variable(fukuro_ws,"FUKUROWS");

  string calibration_path(string("/home/")+user+"/fukuro_ws/src/fukuro_rgbd_calibration/calib_results/"+agent);

  warp_mat = cv::Mat::eye(3,3,CV_64FC1);
  camera_mat = cv::Mat::eye(3,3,CV_64FC1);

  color_segment = new ColorSegment(calibration_path + "/CTable.dat");
  this->loadSettings(calibration_path);

  this->publisher = node.advertise<fukuro_common::RGBDVision>("/rgbd_vision",3);
  ROS_WARN("READY");
}

RGBDVision::~RGBDVision()
{
  //	cv::destroyAllWindows();
}

void RGBDVision::publish()
{
  ROS_INFO("publishing message");
  fukuro_common::RGBDVision rgbd_msg;
  auto depth_it = ball_depth_values.begin();
  auto depth_end = ball_depth_values.end();
  for(const auto &b : ball_segments)
  {
    fukuro_common::BBox box;
    box.x = b.bbox.x0;
    box.y = b.bbox.y0;
    box.w = b.bbox.width;
    box.h = b.bbox.height;
    rgbd_msg.balls.push_back(fukuro_common::Point3d());
    rgbd_msg.box.push_back(box);
    if(depth_it != depth_end)
    {
      rgbd_msg.raw.push_back((*depth_it));
      ++depth_it;
    }
  }
  this->publisher.publish(rgbd_msg);
}

void RGBDVision::updateColor(const sensor_msgs::ImageConstPtr& image_msg)
{
  last_updated = ros::Time::now();
	auto cv_ptr = cv_bridge::toCvShare(image_msg,encodings::BGR8);
  cv::resize(cv_ptr->image,color,cv::Size(IMG_WIDTH_RESIZED,IMG_HEIGHT_RESIZED));
  img_buffer.updateColor(color(cv::Rect(ALIGN_SHIFT/2,0,DEPTH_WIDTH,DEPTH_HEIGHT)));
  ROS_INFO("update color");
}

void RGBDVision::updateDepth(const sensor_msgs::ImageConstPtr& image_msg)
{
	auto cv_ptr = cv_bridge::toCvShare(image_msg,sensor_msgs::image_encodings::TYPE_16UC1);

  //  cv_ptr->image.copyTo(depth_raw);
  alignDepth(cv_ptr->image,depth_aligned);

  img_buffer.updateDepth(depth_aligned);
  ROS_INFO("update depth");
}

void RGBDVision::updateDepthInfo(const sensor_msgs::CameraInfoConstPtr &info)
{
  for(size_t i=0; i<9; i++)
    camera_mat.at<double>(i) = info->K.at(i);
  ROS_INFO("depth camera info updated");
}

bool RGBDVision::process(cv::Mat *color, cv::Mat *depth, bool show_result)
{
  auto dt = ros::Time::now() - last_updated;
  if(dt.toSec()>1.0)
    return false;
  cv::Mat *c, *d;
  c = (color ? color : new cv::Mat());
  d = (depth ? depth : new cv::Mat());

  ROS_INFO("process");
  auto ok = img_buffer.getImage(*c,*d);
  if(c->empty() || !ok)
    return false;
  ball_segments.clear();
  cv::Mat segment_result;
  color_segment->process(*c,segment_result);
  ball_seeker.process(segment_result,ball_segments,DOWNSCALE);

  readDepthValue((*d),ball_segments,ball_depth_values);

  if(show_result)
    showResult((*c), (*d));

  return true;
}

void RGBDVision::showResult(cv::Mat &img, cv::Mat &im_depth)
{
  ROS_INFO("processing result");
  auto ok = true;
  if(img.empty() || im_depth.empty())
    ok = img_buffer.getImage(img,im_depth);
  if(!ok)
    return;

  std::vector<std::string> texts;
  std::stringstream ss;
  for(const auto& v : ball_depth_values)
  {
    auto s = std::to_string(v);
    texts.push_back(s);
    ss << s << " ";
  }
  ball_seeker.drawSegments(img,ball_segments,texts);

  ROS_INFO("depths : %s", ss.str().c_str());
}

void RGBDVision::loadSettings(const std::string &dir)
{
  cv::FileStorage warp_settings(dir+"/warp.xml", cv::FileStorage::READ);
  auto warp_node = warp_settings["WarpSettings"];
  auto it = warp_node.begin();
  auto it_end = warp_node.end();
  std::vector<double> ori;
  std::vector<double> warped;
  while(it != it_end)
  {
    (*it)["origin"] >> ori;
    (*it)["warped"] >> warped;
    ++it;
  }
  warp_settings.release();

  cv::Point2f src[4];
  cv::Point2f dst[4];
  for(size_t i=0; i<8; i+=2)
  {
    src[i/2] = cv::Point2f(ori[i]+320,ori[i+1]+240);
    dst[i/2] = cv::Point2f(warped[i]+320,warped[i+1]+240);
  }
  this->warp_mat = cv::getPerspectiveTransform(src,dst);
}

void RGBDVision::alignDepth(const cv::Mat &depth_raw, cv::Mat &aligned)
{
  aligned.create(depth_raw.rows,depth_raw.cols,depth_raw.type());
  //  cv::undistort(depth_raw,aligned,camera_mat,nullptr);
  cv::warpPerspective(depth_raw,aligned,warp_mat,aligned.size());
}

void RGBDVision::readDepthValue(const cv::Mat &depth, const std::vector<ImageSegment> &ball, std::vector<int> &value)
{
  ROS_INFO("read depth");
  value.clear();

  for(const auto& b : ball)
  {
    auto n_valid_pts = 0;
    auto sum = 0;
    for(const auto& p : b.points)
    {
      auto cx = p.x /*- ALIGN_SHIFT;*/;
      auto cy = p.y;
      if(cx > 0)
      {
        sum += depth.at<uint16_t>(cv::Point(cx,cy));
        n_valid_pts++;
      }
    }
    auto v = (n_valid_pts ? sum/n_valid_pts : -1);
    value.push_back(v);
  }
}

void RGBDVision::readDepthValue(const std::vector<ImageSegment> &ball, std::vector<int> &value)
{
  value.clear();

  for(const auto& b : ball)
  {
    auto n_valid_pts = 0;
    auto sum = 0;
    for(const auto& p : b.points)
    {
      auto cx = p.x /*- ALIGN_SHIFT;*/;
      auto cy = p.y;
      if(cx > 0)
      {
        sum += depth_aligned.at<uint16_t>(cv::Point(cx,cy));
        n_valid_pts++;
      }
    }
    auto v = (n_valid_pts ? sum/n_valid_pts : -1);
    value.push_back(v);
  }
}

void ImageBuffer::updateColor(const cv::Mat &img)
{
  image_mutex.lock();
  if(img_buffer.size()>n_buffer)
    img_buffer.erase(img_buffer.begin());
  img_buffer.push_back(img.clone());
  image_mutex.unlock();
}

void ImageBuffer::updateDepth(const cv::Mat &depth)
{
  depth_mutex.lock();
  if(depth_buffer.size()>n_buffer)
    depth_buffer.erase(depth_buffer.begin());
  depth_buffer.push_back(depth.clone());
  depth_mutex.unlock();
}

bool ImageBuffer::getImage(cv::Mat &img, cv::Mat &depth)
{
  auto ok = false;
  image_mutex.lock();
  depth_mutex.lock();

  if(img_buffer.empty() || depth_buffer.empty())
    goto DONE;

  {
    auto idx = std::min(img_buffer.size(),depth_buffer.size())-1;
    img = img_buffer.at(idx).clone();
    depth = depth_buffer.at(idx).clone();
  }

  ok = true;

DONE:
  depth_mutex.unlock();
  image_mutex.unlock();
  return ok;
}
