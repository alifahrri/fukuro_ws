#include <ros/ros.h>

#include "colorsegment.h"
#include "ballseeker.h"
#include "rgbdvision.h"

#define TEST_BALL_SEEKER_RECT

int main(int argc, char **argv)
{
  ros::init(argc, argv, "fukuro_rgbd_vision");
  ros::NodeHandle node;
#ifdef TEST_BALL_SEEKER_RECT
  fukuro::RGBDVision rgbdvision(node);
  ros::Subscriber color_sub = node.subscribe("/camera/color/image_raw",1,&fukuro::RGBDVision::updateColor,&rgbdvision);
  ros::Subscriber depth_sub = node.subscribe("/camera/depth/image_raw",1,&fukuro::RGBDVision::updateDepth,&rgbdvision);
  ros::Subscriber depth_info_sub = node.subscribe("/camera/depth/camera_info",1,&fukuro::RGBDVision::updateDepthInfo,&rgbdvision);

  ros::AsyncSpinner spinner(3);
  spinner.start();

  ros::Rate rate(30);
  while(ros::ok())
  {
    ROS_INFO("main loop");
    cv::Mat color;
    cv::Mat depth;
    auto need_publish = rgbdvision.process(&color, &depth, true);
    if(!color.empty())
      cv::imshow("result",color);
    if(!depth.empty())
      cv::imshow("depth",depth);
    if(need_publish)
      rgbdvision.publish();
    cv::waitKey(1);
    rate.sleep();
  }
#endif

  return 0;
}
