#include "colorsegment.h"
#include <ros/ros.h>
#include <iostream>
#include <fstream>

#define COLORSEGMENT_TEST

using namespace fukuro;

ColorSegment::ColorSegment(std::__cxx11::string table_dir)
{
  ROS_INFO("load table : %s",table_dir.c_str());

  auto settings_file = table_dir + "/calibration.xml";
  cv::FileStorage ctable_settings(settings_file, cv::FileStorage::READ);
  auto calib_res = ctable_settings["Color_Calib"];
  auto it = calib_res.begin();
  auto it_end = calib_res.end();
  while (it != it_end) {
    (*it)["size"] >> (int&)this->size;
    ++it;
  }
  ctable_settings.release();

  this->table.resize(size*size*size);
  ROS_INFO("table resized : %d",table.size());

  std::ifstream cs_table_read(table_dir.c_str(), std::ios::binary | std::ios::in);
  cs_table_read.read((char*)table.data(),sizeof(ColorTable::value_type)*table.size());
  cs_table_read.close();

  ROS_INFO("table loaded");
}

bool ColorSegment::process(const cv::Mat &in, cv::Mat &out)
{
  if(in.cols==0 || in.rows==0)
    return false;
#ifdef COLORSEGMENT_TEST
  out.create(in.rows,in.cols,CV_8UC3);
#else
  out.create(in.rows,in.cols,CV_8UC1);
#endif

  if(in.channels()<3)
    return false;

  cv::Mat in_yuv;
  cv::cvtColor(in,in_yuv,CV_BGR2YUV);

  uchar *ptr_result = out.data;
  uchar *ptr_image = in_yuv.data;

  for(int i=0; i<in.rows; i++)
    for(int j=0; j<in.cols; j++)
    {
      int idx = out.step[0]*i + out.step[1]*j;
      int k = in.step[0]*i + in.step[1]*j;
#ifdef COLORSEGMENT_TEST
      auto color = table[ptr_image[k]*size*size + ptr_image[k+1]*size + ptr_image[k+2]];
      cv::Scalar segment_color;
      switch (color) {
      case 0:
        segment_color = cv::Scalar(0,0,255);
        break;
      case 1:
        segment_color = cv::Scalar(120,120,120);
        break;
      case 2:
        segment_color = cv::Scalar(0,255,0);
        break;
      case 3:
        segment_color = cv::Scalar(255,255,255);
        break;
      default:
        break;
      }
      ptr_result[idx]  = segment_color(0);
      ptr_result[idx+1]  = segment_color(1);
      ptr_result[idx+2]  = segment_color(2);
#else
      ptr_result[idx] = table[ptr_image[k]/4*64*64 + ptr_image[k+1]/4*64 + ptr_image[k+2]/4];
#endif
    }

  return true;
}
