# The fukuRŌ RGB-D Vision Package

This package provides ball information in local 3d frame from RGB-D camera by doing the followings :  

* Reads color and depth camera images by subcribing to `/camera/color/image_raw' and '/camera/depth/image_raw`  
* Reads depth camera matrix by subscribing to `/camera/depth/camera_info`  
* Loads the saved (YUV) color table in `/home/USER/fukuro_ws/fukuro_rgbd_calibration/calib_results/AGENT/CTable.dat`  
* Loads the saved warp parameter in `/home/USER/fukuro_ws/fukuro_rgbd_calibration/calib_results/AGENT/warp.xml`  
* the `USER` and `AGENT` is read from the environment variable  
* Segments the rgb image using the loaded color table  
* Warp the depth image using the loaded warp parameter to align with rgb images  
* Performs region-growing segmentation on color coded images and results bounding-boxes representing the ball pos  
* Read the depth from the aligned depth image for each detected bboxes  
* advertise RGBDVision message by publishing in `/rgbd_vision`  

### To Do Lists : 
* Transform the detected bboxes to 3d coordinates
* Reduce CPU Load