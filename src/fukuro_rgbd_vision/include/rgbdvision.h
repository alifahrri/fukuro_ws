#ifndef RGBDVISION_H
#define RGBDVISION_H

#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_transport/image_transport.h>
#include <image_transport/camera_subscriber.h>

#include "fukuro_common/RGBDVision.h"

#include "colorsegment.h"
#include "ballseeker.h"

namespace fukuro {

struct ImageBuffer
{
  ImageBuffer() {}
  std::mutex image_mutex;
  std::mutex depth_mutex;
  void updateColor(const cv::Mat &img);
  void updateDepth(const cv::Mat &depth);
  bool getImage(cv::Mat &img, cv::Mat &depth);
  std::vector<cv::Mat> img_buffer;
  std::vector<cv::Mat> depth_buffer;
  std::size_t n_buffer = 10;
};

class RGBDVision
{

public:
  RGBDVision(ros::NodeHandle &node);
  ~RGBDVision();
  void publish();
  void updateColor(const sensor_msgs::ImageConstPtr& image_msg);
  void updateDepth(const sensor_msgs::ImageConstPtr& image_msg);
  void updateDepthInfo(const sensor_msgs::CameraInfoConstPtr &info);
  bool process(cv::Mat *color = nullptr, cv::Mat *depth = nullptr, bool show_result = false);
  void showResult(cv::Mat &img, cv::Mat &im_depth);

private:
  void loadSettings(const std::string &dir);
  void alignDepth(const cv::Mat &depth_raw, cv::Mat &aligned);
  void readDepthValue(const std::vector<ImageSegment> &ball, std::vector<int> &value);
  void readDepthValue(const cv::Mat &depth, const std::vector<ImageSegment> &ball, std::vector<int> &value);

private:
  ros::Time last_updated;
  ros::Publisher publisher;

private:
  cv::Mat color;
  cv::Mat warp_mat;
  cv::Mat depth_raw;
  cv::Mat camera_mat;
  cv::Mat depth_aligned;

private:
  std::size_t n_buffer;
  std::mutex color_mutex;
  std::mutex depth_mutex;
  std::vector<int> ball_depth_values;
  std::vector<ImageSegment> ball_segments;

private:
  BallSeeker ball_seeker;
  ImageBuffer img_buffer;
  ColorSegment *color_segment;
};

}

#endif //RGBDVISION_H
