#ifndef COLORSEGMENT_H
#define COLORSEGMENT_H

#include <opencv2/opencv.hpp>

namespace fukuro {

  typedef std::vector<uint8_t> ColorTable;

  class ColorSegment
  {
  private:
    ColorTable table;
    size_t size = 256;
    //    unsigned char table[64*64*64];

  public:
    ColorSegment(std::string table_dir);
    bool process(const cv::Mat &in, cv::Mat &out);
  };
}

#endif // COLORSEGMENT_H
