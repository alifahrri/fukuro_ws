#ifndef BALLSEEKER_H
#define BALLSEEKER_H

#include <opencv2/opencv.hpp>

namespace fukuro {

struct Point
{
  int x;
  int y;
};

struct Box
{
  int x0;
  int y0;
  int width;
  int height;
  int x1;
  int y1;
};

struct ImageSegment
{
  std::vector<cv::Point> points;
  Box bbox = Box({-1,-1,-1,-1,-1,-1});
};

class BallSeeker{
public:
  BallSeeker();
  bool process(cv::Mat &image, std::vector<ImageSegment> &segments, size_t down_scale = 1);
  void drawSegments(cv::Mat &image, std::vector<ImageSegment> &segments, std::vector<std::string> text = std::vector<std::string>());
};

}
#endif // BALLSEEKER_H
