#include <ros/ros.h>
#include <QApplication>
#include <QTimer>
#include "mclwidget.h"
#include "plannerwidget.h"
#include "sensorwidget.h"
#include "monitordialog.h"
#include <exception>
#include <iostream>

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_monitor");
  ros::NodeHandle node;
  QApplication app(argc,argv);
  MonitorDialog monitor(node);
  QTimer timer;
  timer.connect(&timer,&QTimer::timeout,[&]{
      if(!ros::ok())
          monitor.close();
  });
  try{
    monitor.show();
    ros::AsyncSpinner spinner(6);
    if(spinner.canStart())
      spinner.start();
    else
      exit(-1);
    timer.start(1000);
    app.exec();
  }
  catch(std::exception e){
    std::cout<<"[Fukuro Monitor Error] "<<e.what()<<std::endl;
    //monitor.show();
    //ros::AsyncSpinner spinner(6);
    //if(spinner.canStart())
      //spinner.start();
    //else
      //exit(-1);
    //app.exec();
  }
  return 0;
}
