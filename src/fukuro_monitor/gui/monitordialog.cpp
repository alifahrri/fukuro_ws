#include "monitordialog.h"
#include "ui_monitordialog.h"
#include "mclwidget.h"
#include "plannerwidget.h"
#include "sensorwidget.h"
#include "worldmodelwidget.h"

MonitorDialog::MonitorDialog(ros::NodeHandle &node_, QWidget *parent) :
    node(node_),
    QDialog(parent),
    ui(new Ui::MonitorDialog), isManualPositioning(false)
{
    ui->setupUi(this);
    std::string robot_name;
    if(!ros::get_environment_variable(robot_name,"FUKURO"))
        exit(-1);
    loc_sub = node.subscribe("/localization",10,&MCLWidget::updateMCL,ui->amcl_widget);
    global_white_sub = node.subscribe("/localization/global_whites",10,&MCLWidget::updateWhites,ui->amcl_widget);
    planner_sub = node.subscribe("/path_planning",10,&PlannerWidget::updatePlanner,ui->planner_widget);
    whites_sub = node.subscribe("/omnivision/Whites",10,&SensorWidget::updateWhites,ui->vision_widget);
    balls_sub = node.subscribe("/omnivision/Balls",10,&SensorWidget::updateBalls,ui->vision_widget);
    obstacles_sub = node.subscribe("/omnivision/Obstacles",10,&SensorWidget::updateObstacles,ui->vision_widget);
    world_model_sub = node.subscribe(robot_name+"/world_model",10,&WorldModelWidget::updateWorld,ui->world_model_widget);
    //  robot_control_info_sub = node.subscribe(robot_name+"/robot_control_info",10,&WorldModelWidget::updateControlInfo,ui->world_model_widget);
    strategy_sub = node.subscribe(robot_name+"/strategy_info",10,&MonitorDialog::updateStrategy,this);
    robot_control_info_sub = node.subscribe(robot_name+"/robot_control_info",10,&MonitorDialog::updateRobotControlInfo,this);
    robot_control_sub = node.subscribe(robot_name+"/robot_control",10,&MonitorDialog::updateRobotControl,this);
    vel_cmd_sub = node.subscribe(robot_name+"/vel_cmd",10,&MonitorDialog::updateVelCmd,this);
    vel_cmd_manual_sub = node.subscribe(robot_name+"/vel_cmd_manual",10,&MonitorDialog::updateVelCmdManual,this);
    teammates_sub = node.subscribe(robot_name+"/teammates",10,&MonitorDialog::updateTeammates,this);
    setWindowTitle("Fukuro Monitor");
    this->setFixedSize(this->size());
    connect(this,&MonitorDialog::setStrategy,[=](QString state, QString role)
    {
        ui->state_label->setText(state);
        ui->role_label->setText(role);
    });
    connect(this,&MonitorDialog::setRobotControl,[=](qreal target_x,qreal target_y,qreal target_theta,QString option,QString control,quint8 dribbler,bool plan)
    {
        ui->target_label->setText(QString("(%1,%2,%3)").arg(target_x).arg(target_y).arg(target_theta));
        ui->option_label->setText(option);
        ui->control_label->setText(control);
        ui->dribbler_label->setText(QString::number(dribbler));
        ui->plan_label->setText(plan ? QString("true") : QString("false"));
    });
    connect(this,&MonitorDialog::setRobotControlInfo,[=](qreal error_radius, qreal error_angle, qreal setpoint_x, qreal setpoint_y, qreal setpoint_w)
    {
        ui->error_radius_label->setText(QString::number(error_radius));
        ui->error_angle_label->setText(QString::number(error_angle));
        ui->setpoint_label->setText(QString("(%1,%2,%3)").arg(setpoint_x).arg(setpoint_y).arg(setpoint_w));
    });
    connect(this,&MonitorDialog::setVelCmd,[=](qreal vx, qreal vy, qreal w)
    {
        if(!isManualPositioning){
            ui->vx_label->setText(QString::number(vx));
            ui->vy_label->setText(QString::number(vy));
            ui->w_label->setText(QString::number(w));
        }
    });
    connect(this,&MonitorDialog::setVelCmdManual,[=](qreal vx, qreal vy, qreal w)
    {
        if(isManualPositioning){
            ui->vx_label->setText(QString::number(vx));
            ui->vy_label->setText(QString::number(vy));
            ui->w_label->setText(QString::number(w));
        }
    });
    connect(this,&MonitorDialog::setTeammates,[=](bool manual)
    {
        isManualPositioning = manual;
        //isManualPositioning? std::cout<<"[DEBUG] MANUAL POSITIONING: TRUE"<<std::endl:std::cout<<"[DEBUG] MANUAL POSITIONING: FALSE"<<std::endl;
    });
}

MonitorDialog::~MonitorDialog()
{
    delete ui;
}

void MonitorDialog::updateStrategy(const fukuro_common::StrategyInfo::ConstPtr &state)
{
    emit setStrategy(QString::fromStdString(state->strategy_state),
                     QString::fromStdString(state->role));
}

void MonitorDialog::updateRobotControl(const fukuro_common::RobotControl::ConstPtr &control)
{
    emit setRobotControl(control->target_pose.x,
                         control->target_pose.y,
                         control->target_pose.theta,
                         QString::fromStdString(control->option.data),
                         QString::fromStdString(control->control.data),
                         control->dribbler,
                         control->plan);
}

void MonitorDialog::updateRobotControlInfo(const fukuro_common::RobotControlInfo::ConstPtr &info)
{
    emit setRobotControlInfo(info->error_radius,
                             info->error_angle,
                             info->setpoint.x,
                             info->setpoint.y,
                             info->setpoint.theta);
    ui->world_model_widget->updateControlInfo(info);
}

void MonitorDialog::updateVelCmd(const fukuro_common::VelCmd::ConstPtr &cmd)
{
    emit setVelCmd(cmd->Vx,cmd->Vy,cmd->w);
}

void MonitorDialog::updateVelCmdManual(const fukuro_common::HWControllerManual::ConstPtr &cmd)
{
    emit setVelCmdManual(cmd->Vx,cmd->Vy,cmd->w);
}

void MonitorDialog::updateTeammates(const fukuro_common::Teammates::ConstPtr &tm){
    emit setTeammates(tm->isManualPositioning?true:false);
}
