#include "sensorwidget.h"
#include "ui_sensorwidget.h"
#include "fukuro_common/Point2d.h"
#include <QDebug>
#include <QGraphicsSceneHoverEvent>

#define SCENE_WIDTH 640
#define SCENE_HEIGHT 480

SensorWidget::SensorWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::SensorWidget),
  robot(new RobotItem),
  whites(new WhitesItem),
  balls(new BallsItem),
  obstacles(new ObstaclesItem),
  timer(new QTimer(this))
{
  ui->setupUi(this);
  ui->graphicsView->setScene(new QGraphicsScene(-SCENE_WIDTH/2,-SCENE_HEIGHT/2,SCENE_WIDTH,SCENE_HEIGHT,this));
  ui->graphicsView->scene()->addItem(whites);
  ui->graphicsView->scene()->addItem(balls);
  ui->graphicsView->scene()->addItem(obstacles);
  ui->graphicsView->scene()->addItem(robot);
  ui->graphicsView->setRenderHint(QPainter::Antialiasing);
//  setFixedSize(SCENE_WIDTH+50, SCENE_WIDTH+50);
  setWindowTitle("Fukuro Vision Sensor");
  connect(timer,&QTimer::timeout,[=]{
//    qDebug() << "[SensorWidget] timeout update";
    whites->update(whites->boundingRect());
    balls->update(balls->boundingRect());
    obstacles->update(obstacles->boundingRect());
    ui->graphicsView->scene()->update();
    ui->graphicsView->update();
    update(this->rect());
  });
  timer->start(250);
}

void SensorWidget::updateWhites(const fukuro_common::Whites::ConstPtr &white)
{
  whites->setWhites(white);
}

void SensorWidget::updateBalls(const fukuro_common::Balls::ConstPtr &ball)
{
  balls->setBalls(ball);
}

void SensorWidget::updateObstacles(const fukuro_common::Obstacles::ConstPtr &obs)
{
  obstacles->setObstacles(obs);
}

SensorWidget::~SensorWidget()
{
  delete ui;
}

SensorWidget::RobotItem::RobotItem()
{
  line_width = 600;
  line_height = 600;
  int line_step = 30;
  for(int w=-line_width/2; w<=line_width/2; w+=line_step)
  {
    lines.push_back(QLineF(QPointF(w,-line_height/2),QPointF(w,line_height/2)));
  }
  for(int h=-line_height/2; h<=line_height/2; h+=line_step)
  {
    lines.push_back(QLineF(QPointF(-line_width/2,h),QPointF(line_width/2,h)));
  }
}

void SensorWidget::RobotItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->rotate(-90);
  painter->setPen(Qt::red);
  painter->drawEllipse(QPointF(0.0,0.0),25.0,25.0);
  painter->drawLine(QPointF(0.0,0.0),QPointF(30.0,0.0));
  painter->setPen(Qt::gray);
  painter->drawLines(lines);
}

QRectF SensorWidget::RobotItem::boundingRect() const
{
  return QRectF(-line_width/2,-line_height/2,line_width,line_height);
}

SensorWidget::WhitesItem::WhitesItem()
{
  this->setAcceptHoverEvents(true);
}

void SensorWidget::WhitesItem::setWhites(const fukuro_common::Whites::ConstPtr &white)
{
  whites.clear();
  for(auto w : white->whites)
    whites.push_back(QPointF(w.x,w.y));
  this->update();
}

void SensorWidget::WhitesItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->rotate(-90);
  auto matrix0 = painter->matrix();
  painter->setPen(Qt::green);
  QString text;
  for(auto& w : whites)
  {
    painter->setMatrix(matrix0);
    QPointF offset(w.x()*100,-w.y()*100);
    painter->translate(offset);
    if((offset-hover_text).manhattanLength()<15)
    {
      text = QString("(%1,%2)").arg(w.x(),0,'g',2).arg(w.y(),0,'g',2);
    }
    painter->setPen(Qt::green);
    painter->drawEllipse(QPointF(0.0,0.0),3.0,3.0);
  }
  if(draw_text)
  {
    if(!text.isEmpty())
    {
      painter->setMatrix(matrix0);
      painter->rotate(90);
      painter->setPen(Qt::black);
      painter->translate(hover_text);
      painter->drawText(QPointF(0.0,0.0),text);
    }
  }
}

QRectF SensorWidget::WhitesItem::boundingRect() const
{
  return QRectF(-SCENE_WIDTH/2,-SCENE_WIDTH/2,SCENE_WIDTH,SCENE_WIDTH);
}

void SensorWidget::WhitesItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
  draw_text = true;
  hover_text = event->pos();
}

void SensorWidget::WhitesItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
  draw_text = true;
}

void SensorWidget::WhitesItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
  hover_text = event->pos();
}

SensorWidget::BallsItem::BallsItem()
{

}

void SensorWidget::BallsItem::setBalls(const fukuro_common::Balls::ConstPtr &ball)
{
  balls.clear();
  for(auto& b : ball->balls)
    balls.push_back(QPointF(b.x,-b.y));
  this->update();
}

void SensorWidget::BallsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->rotate(-90);
  auto matrix0 = painter->matrix();
  painter->setPen(Qt::red);
  painter->setBrush(Qt::red);
  for(auto& b : balls)
  {
    painter->setMatrix(matrix0);
    painter->translate(b.x()*100,b.y()*100);
    painter->drawEllipse(QPointF(0.0,0.0),15.0,15.0);
  }
}

QRectF SensorWidget::BallsItem::boundingRect() const
{
  return QRectF(-SCENE_WIDTH/2,-SCENE_WIDTH/2,SCENE_WIDTH,SCENE_WIDTH);
}

SensorWidget::ObstaclesItem::ObstaclesItem()
{

}

void SensorWidget::ObstaclesItem::setObstacles(const fukuro_common::Obstacles::ConstPtr &obstacle)
{
  obstacles.clear();
  for(auto& o : obstacle->obstacles)
    obstacles.push_back(QPointF(o.x,o.y));
  this->update();
}

void SensorWidget::ObstaclesItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->rotate(-90);
  auto matrix0 = painter->matrix();
  painter->setPen(Qt::black);
  painter->setPen(Qt::darkGray);
  painter->setBrush(Qt::gray);
  for(auto& o : obstacles)
  {
    painter->setMatrix(matrix0);
    painter->translate(o.x()*100,-o.y()*100);
    painter->drawEllipse(QPointF(0.0,0.0),25.0,25.0);
  }
}

QRectF SensorWidget::ObstaclesItem::boundingRect() const
{
  return QRectF(-SCENE_WIDTH/2,-SCENE_WIDTH/2,SCENE_WIDTH,SCENE_WIDTH);
}
