﻿#ifndef PLANNERDIALOG_H
#define PLANNERDIALOG_H

#include <QWidget>
#include <QGraphicsItem>
#include <ros/ros.h>
#include <functional>
#include "fukuro_common/PlannerInfo.h"
#include "fukuro_common/PlannerService.h"
#include "fukuro/core/field.hpp"

namespace Ui {
class PlannerWidget;
}

class PlannerWidget : public QWidget
{
  Q_OBJECT

public:
  explicit PlannerWidget(QWidget *parent = 0);
  void updatePlanner(const fukuro_common::PlannerInfo::ConstPtr& msg);
  ~PlannerWidget();

private:
  class Field : public QGraphicsItem
  {
  public:
    Field();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

  private:
    QVector<QLineF> lines;
    QVector<QLineF> circle_lines;
    QRectF center_circle;
  };

  class Grid : public QGraphicsItem
  {
  public:
    typedef QPair<QRectF,QColor> Vertex;
    typedef std::function<void(double,double)> SetGoalCallback;
  public:
    Grid();
    void setColor(double x, double y, QColor color);
    void setStart(double x, double y);
    void setGoal(double x, double y);
    void setObstacles(std::vector<std::pair<double,double>> obs);
    void setSolution(std::vector<std::pair<double,double>> sol);
    void resetColor();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  public:
    QVector<Vertex> rect;
    QVector<int> changed_rect;
    QVector<QLineF> solutions;
    double width;
    double height;
    double grid_size;
    SetGoalCallback goal_callback;
  private:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
  private:
  };


private:
  Ui::PlannerWidget *ui;
  Grid *grid;
  Field *field;
  QTimer *timer;
};

inline
bool operator <(const QPointF& p1, const QPointF& p2)
{
  return (p1.x() < p2.x()) && (p1.y() < p2.y());
}

#endif // PLANNERDIALOG_H
