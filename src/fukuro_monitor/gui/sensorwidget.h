#ifndef SENSORDIALOG_H
#define SENSORDIALOG_H

#include <QWidget>
#include <QTimer>
#include <QGraphicsItem>
#include "fukuro_common/Whites.h"
#include "fukuro_common/Balls.h"
#include "fukuro_common/Obstacles.h"

namespace Ui {
class SensorWidget;
}

class SensorWidget : public QWidget
{
  Q_OBJECT

public:
  explicit SensorWidget(QWidget *parent = 0);
  void updateWhites(const fukuro_common::Whites::ConstPtr& white);
  void updateBalls(const fukuro_common::Balls::ConstPtr& ball);
  void updateObstacles(const fukuro_common::Obstacles::ConstPtr& obs);
  ~SensorWidget();

private:
  Ui::SensorWidget *ui;

private:
  class RobotItem : public QGraphicsItem
  {
  public:
    RobotItem();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  private:
    QVector<QLineF> lines;
    int line_width;
    int line_height;
  };

  class WhitesItem : public QGraphicsItem
  {
  public:
    WhitesItem();
    void setWhites(const fukuro_common::Whites::ConstPtr& white);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  private:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
  private:
    QVector<QPointF> whites;
    QPointF hover_text;
    bool draw_text;
  };

  class BallsItem : public QGraphicsItem
  {
  public:
    BallsItem();
    void setBalls(const fukuro_common::Balls::ConstPtr& ball);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  private:
    QVector<QPointF> balls;
  };

  class ObstaclesItem : public QGraphicsItem
  {
  public:
    ObstaclesItem();
    void setObstacles(const fukuro_common::Obstacles::ConstPtr& obstacle);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  private:
    QVector<QPointF> obstacles;
  };

private:
  RobotItem* robot;
  WhitesItem* whites;
  BallsItem* balls;
  ObstaclesItem* obstacles;
  QTimer *timer;
};

#endif // SENSORDIALOG_H
