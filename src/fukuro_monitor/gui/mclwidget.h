#ifndef MCLDIALOG_H
#define MCLDIALOG_H

#include <QDialog>
#include <QGraphicsItem>
#include <QTimer>
#include "fukuro_common/Localization.h"
#include "fukuro_common/Whites.h"
#include "fukuro/core/field.hpp"

namespace Ui {
class MCLWidget;
}

class MCLWidget : public QWidget
{
  Q_OBJECT

public:
  explicit MCLWidget(QWidget *parent = 0);
  void updateMCL(const fukuro_common::Localization::ConstPtr& loc);
  void updateWhites(const fukuro_common::Whites::ConstPtr& white);
  ~MCLWidget();

private:
  Ui::MCLWidget *ui;

private:
  class Field : public QGraphicsItem
  {
  public:
    Field();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

  private:
    QVector<QLineF> lines;
    QVector<QLineF> circle_lines;
    QRectF center_circle;
  };

  class MCLItem : public QGraphicsItem
  {
  public:
    typedef QPair<QPointF,double> Pose2D;
    typedef QVector<Pose2D> Particles;
    MCLItem();
    void setParticles(Pose2D belief_, Particles particles_, Pose2D best_estimate_);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    Pose2D bel() { return belief; }
  private:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
  private:
    Pose2D belief;
    Pose2D best_estimate;
    Particles particles;
    QPointF hover_point;
    bool draw_text;
  };

  class WhitesItem : public QGraphicsItem
  {
  public:
    WhitesItem();
    void setWhites(QVector<QPointF> whites_);
    void setBelief(MCLItem::Pose2D bel_);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  private:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
  private:
    QVector<QPointF> whites;
    MCLItem::Pose2D bel;
    QPointF hover_point;
    bool draw_text;
  };

private:
  Field *field;
  MCLItem *mcl_item;
  WhitesItem *whites_item;
  QTimer *timer;
};

#endif // MCLDIALOG_H
