#include "worldmodelwidget.h"
#include "ui_worldmodelwidget.h"
#include <QTimer>

#define ANGLE_INCREMENT 3

#define CENTER_RADIUS CENTER_CIRCLE_RADIUS

WorldModelWidget::WorldModelWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::WorldModelWidget),
  field(new Field),
  robot(new Robot),
  obstacles(new Obstacles),
  balls(new Balls),
  timer(new QTimer(this))
{
  ui->setupUi(this);
  ui->graphicsView->setScene(new QGraphicsScene(this));
  ui->graphicsView->scene()->addItem(field);
  ui->graphicsView->scene()->addItem(robot);
  ui->graphicsView->scene()->addItem(obstacles);
  ui->graphicsView->scene()->addItem(balls);
  ui->graphicsView->setBackgroundBrush(Qt::darkGreen);
  ui->graphicsView->scale(0.75,0.75);
  connect(timer,&QTimer::timeout,[=]
  {
    robot->update();
    ui->graphicsView->scene()->update();
    ui->graphicsView->update();
    update(this->rect());
  });
  timer->start(250);
  ui->graphicsView->setRenderHints(QPainter::SmoothPixmapTransform | QPainter::Antialiasing);
}

void WorldModelWidget::updateWorld(const fukuro_common::WorldModel::ConstPtr &world)
{
  robot->robot_pos.setX(world->pose.x*100);
  robot->robot_pos.setY(-world->pose.y*100);
  robot->angle = world->pose.theta*180.0/M_PI;
  obstacles->obstacles.clear();
  for(size_t i=0; i<world->obstacles.size(); i++)
  {
    auto& o = world->obstacles.at(i);
    obstacles->obstacles.push_back(QPointF(o.x*100,-o.y*100));
  }
  balls->ball_pos.setX(world->global_balls_kf.x*100);
  balls->ball_pos.setY(-world->global_balls_kf.y*100);
  if(world->ball_visible)
    balls->ball_visible = true;
  else
    balls->ball_visible = false;
}

void WorldModelWidget::updateControlInfo(const fukuro_common::RobotControlInfo::ConstPtr &control)
{
  robot->setpoint_pos.setX(control->setpoint.x*100);
  robot->setpoint_pos.setY(-control->setpoint.y*100);
  robot->setpoint_angle = control->setpoint.theta*180.0/M_PI;
}

WorldModelWidget::~WorldModelWidget()
{
  delete ui;
}

WorldModelWidget::Field::Field()
{
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE7,YLINE1)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE6),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE1,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE7,YLINE1),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE4,YLINE1),
                         QPointF(XLINE4,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE2,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE6,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE3,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE5,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE1,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE4),
                         QPointF(XLINE1,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE1,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE5),
                         QPointF(XLINE1,YLINE5)));

  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE7,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE4),
                         QPointF(XLINE7,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE7,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE5),
                         QPointF(XLINE7,YLINE5)));

  center_circle.setLeft(-CENTER_RADIUS);
  center_circle.setTop(-CENTER_RADIUS);
  center_circle.setHeight(2*CENTER_RADIUS);
  center_circle.setWidth(2*CENTER_RADIUS);

  QPointF circle_line0(center_circle.width()/2,0.0);
  QLineF line(QPointF(0.0,0.0),circle_line0);
  circle_lines.push_back(QLineF(circle_line0,line.p2()));
  for(int i=ANGLE_INCREMENT; i<=360; i+=ANGLE_INCREMENT)
  {
      line.setAngle(i);
      QPointF p0 = circle_lines.back().p2();
      circle_lines.push_back(QLineF(p0,line.p2()));
  }
}

void WorldModelWidget::Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::transparent);
  painter->setBrush(Qt::green);
  painter->drawRect(boundingRect());
  painter->setPen(QPen(Qt::white,3.3));
  painter->setBrush(Qt::transparent);
  painter->drawLines(lines);
  painter->drawEllipse(center_circle);
}

QRectF WorldModelWidget::Field::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

WorldModelWidget::Robot::Robot()
{
  std::string user_name(std::getenv("USER"));
  std::string path = std::string("/home/")+user_name.c_str()+"/fukuro_ws/src/fukuro_common/pixmap/fukuro.png";
  QImage robot_image(path.c_str());
  pixmap = QPixmap::fromImage(robot_image.scaledToHeight(50,Qt::SmoothTransformation));
  robot_pos = QPointF(0.0,0.0);
  angle = 0.0;
}

void WorldModelWidget::Robot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  auto matrix_ori = painter->matrix();
  painter->translate(robot_pos);
  painter->rotate(-angle);
  painter->rotate(-90);
  painter->drawPixmap(QPoint(-pixmap.height()/2,-pixmap.width()/2),pixmap);
  painter->setMatrix(matrix_ori);
  painter->translate(setpoint_pos);
  painter->rotate(-setpoint_angle);
  painter->rotate(-90);
  painter->setOpacity(0.3);
  painter->drawPixmap(QPoint(-pixmap.height()/2,-pixmap.width()/2),pixmap);
}

QRectF WorldModelWidget::Robot::boundingRect() const
{
  return QRectF(-30,-30,30,30);
}

WorldModelWidget::Obstacles::Obstacles()
{

}

void WorldModelWidget::Obstacles::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::NoPen);
  painter->setBrush(Qt::black);
  for(auto& o : obstacles)
    painter->drawEllipse(o,25,25);
}

QRectF WorldModelWidget::Obstacles::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

void WorldModelWidget::Balls::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  if(!ball_visible)
    return;
  painter->setPen(Qt::NoPen);
  painter->setBrush(Qt::red);
  painter->translate(ball_pos);
  painter->drawEllipse(QPoint(0,0),10,10);
}

QRectF WorldModelWidget::Balls::boundingRect() const
{
  return QRectF(-10,-10,20,20);
}
