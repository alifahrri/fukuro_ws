#ifndef MONITORDIALOG_H
#define MONITORDIALOG_H

#include <QDialog>
#include <ros/ros.h>
#include "fukuro_common/StrategyInfo.h"
#include "fukuro_common/RobotControl.h"
#include "fukuro_common/RobotControlInfo.h"
#include "fukuro_common/VelCmd.h"
#include "fukuro_common/HWControllerManual.h"
#include "fukuro_common/Teammates.h"

namespace Ui {
class MonitorDialog;
}

class MonitorDialog : public QDialog
{
  Q_OBJECT

public:
  explicit MonitorDialog(ros::NodeHandle &node_,QWidget *parent = 0);
  ~MonitorDialog();

private:
  Ui::MonitorDialog *ui;
  ros::NodeHandle &node;
  ros::Subscriber loc_sub;
  ros::Subscriber global_white_sub;
  ros::Subscriber planner_sub;
  ros::Subscriber whites_sub;
  ros::Subscriber balls_sub;
  ros::Subscriber obstacles_sub;
  ros::Subscriber world_model_sub;
  ros::Subscriber robot_control_info_sub;
  ros::Subscriber strategy_sub;
  ros::Subscriber robot_control_sub;
  ros::Subscriber vel_cmd_sub;
  ros::Subscriber vel_cmd_manual_sub;
  ros::Subscriber teammates_sub;

private:
  void updateStrategy(const fukuro_common::StrategyInfo::ConstPtr& state);
  void updateRobotControl(const fukuro_common::RobotControl::ConstPtr& control);
  void updateRobotControlInfo(const fukuro_common::RobotControlInfo::ConstPtr& info);
  void updateVelCmd(const fukuro_common::VelCmd::ConstPtr& cmd);
  void updateVelCmdManual(const fukuro_common::HWControllerManual::ConstPtr& cmd);
  void updateTeammates(const fukuro_common::Teammates::ConstPtr& tm);
  bool isManualPositioning;

signals:
  void setStrategy(QString,QString);
  void setRobotControl(qreal,qreal,qreal,QString,QString,quint8,bool);
  void setRobotControlInfo(qreal,qreal,qreal,qreal,qreal);
  void setVelCmd(qreal,qreal,qreal);
  void setVelCmdManual(qreal, qreal, qreal);
  void setTeammates(bool);
};

#endif // MONITORDIALOG_H
