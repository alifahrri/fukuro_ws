#include "mclwidget.h"
#include "ui_mclwidget.h"
#include <ros/ros.h>
#include <QPainter>
#include <QGraphicsSceneHoverEvent>
#include <chrono>

#define ANGLE_INCREMENT 3

#define CENTER_RADIUS CENTER_CIRCLE_RADIUS
#define GLOBAL_WHITES

MCLWidget::MCLWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::MCLWidget),
  field(new Field),
  mcl_item(new MCLItem),
  whites_item(new WhitesItem),
  timer(new QTimer(this))
{
  ui->setupUi(this);
  ui->graphicsView->setScene(new QGraphicsScene(-FIELD_WIDTH/2-100,-FIELD_HEIGHT/2-100,FIELD_WIDTH+200,FIELD_HEIGHT+200,this));
  ui->graphicsView->scene()->addItem(field);
  ui->graphicsView->scene()->addItem(mcl_item);
  ui->graphicsView->scene()->addItem(whites_item);
  ui->graphicsView->setRenderHint(QPainter::Antialiasing);
//  double scale_width = this->size().width()/(FIELD_WIDTH);
//  double scale_height = this->size().height()/(FIELD_HEIGHT);
//  double scale = scale_width < scale_height ? scale_width : scale_height;
  ui->graphicsView->scale(0.75,0.75);
  ui->graphicsView->scene()->setBackgroundBrush(Qt::darkGreen);
//  this->setFixedSize(this->size()+QSize(50,50));
  this->setWindowTitle("Fukuro AMCL Viewer");
  connect(timer,&QTimer::timeout,[=]
  {
    mcl_item->update(mcl_item->boundingRect());
    whites_item->update(whites_item->boundingRect());
    update(this->rect());
    ui->graphicsView->scene()->update();
    ui->graphicsView->update();
  });
  timer->start(250);
}

MCLWidget::~MCLWidget()
{
  delete ui;
}

MCLWidget::Field::Field()
{
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE7,YLINE1)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE6),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE1,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE7,YLINE1),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE4,YLINE1),
                         QPointF(XLINE4,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE2,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE6,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE3,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE5,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE1,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE4),
                         QPointF(XLINE1,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE1,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE5),
                         QPointF(XLINE1,YLINE5)));

  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE7,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE4),
                         QPointF(XLINE7,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE7,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE5),
                         QPointF(XLINE7,YLINE5)));

  center_circle.setLeft(-CENTER_RADIUS);
  center_circle.setTop(-CENTER_RADIUS);
  center_circle.setHeight(2*CENTER_RADIUS);
  center_circle.setWidth(2*CENTER_RADIUS);

  QPointF circle_line0(center_circle.width()/2,0.0);
  QLineF line(QPointF(0.0,0.0),circle_line0);
  circle_lines.push_back(QLineF(circle_line0,line.p2()));
  for(int i=ANGLE_INCREMENT; i<=360; i+=ANGLE_INCREMENT)
  {
      line.setAngle(i);
      QPointF p0 = circle_lines.back().p2();
      circle_lines.push_back(QLineF(p0,line.p2()));
  }
}

void MCLWidget::Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::transparent);
  painter->setBrush(Qt::green);
  painter->drawRect(boundingRect());
  painter->setPen(QPen(Qt::white,3.3));
  painter->setBrush(Qt::transparent);
  painter->drawLines(lines);
  painter->drawEllipse(center_circle);
}

QRectF MCLWidget::Field::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

void MCLWidget::updateMCL(const fukuro_common::Localization::ConstPtr& loc)
{
  static auto last_update = ros::Time::now();
  auto now = ros::Time::now();
  ros::Duration dt = now-last_update;
  if(dt>ros::Duration(ros::Rate(10)))
  {
    MCLItem::Pose2D bel(QPointF(loc->belief.x,loc->belief.y),loc->belief.theta);
    MCLItem::Pose2D best(QPointF(loc->best_estimation.x,loc->best_estimation.y),loc->best_estimation.theta);
    MCLItem::Particles particles;
    whites_item->setBelief(bel);
    for(auto p : loc->particles)
      particles.push_back(MCLItem::Pose2D(QPointF(p.x,p.y),p.theta));
    mcl_item->setParticles(bel,particles,best);
    mcl_item->update();
    last_update = now;
  }
}

void MCLWidget::updateWhites(const fukuro_common::Whites::ConstPtr &white)
{
  static auto last_update = ros::Time::now();
  auto now = ros::Time::now();
  ros::Duration dt = now-last_update;
  if(dt>ros::Duration(ros::Rate(10)))
  {
    QVector<QPointF> whites;
    for(auto w : white->whites)
      whites.push_back(QPointF(w.x,w.y));
    whites_item->setWhites(whites);
    whites_item->update();
    last_update = now;
  }
}

MCLWidget::MCLItem::MCLItem()
{
  this->setAcceptHoverEvents(true);
}

void MCLWidget::MCLItem::setParticles(Pose2D belief_, Particles particles_, Pose2D best_estimate_)
{
  belief = belief_;
  particles = particles_;
  best_estimate = best_estimate_;
}

void MCLWidget::MCLItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  auto matrix0 = painter->matrix();
  QPointF belief_point(belief.first.x(),-belief.first.y());
  QPointF best_point(best_estimate.first.x(),-best_estimate.first.y());
  painter->setPen(Qt::gray);
  QString text;
  for(auto& p : particles)
  {
    painter->setMatrix(matrix0);
    QPointF translate_point(p.first.x(),-p.first.y());
    painter->translate(translate_point);
    painter->rotate(-p.second);
    if((translate_point-hover_point).manhattanLength()<10)
      text = QString("(%1,%2,%3)").arg(p.first.x()).arg(p.first.y()).arg(p.second);
    painter->drawEllipse(QPointF(0.0,0.0),5.0,5.0);
    painter->drawLine(QPointF(0.0,0.0),QPointF(10.0,0.0));
  }
  if(draw_text)
  {
    painter->setMatrix(matrix0);
    painter->setPen(Qt::black);
    auto draw_point = hover_point;
    if(!text.isEmpty())
      painter->drawText(hover_point,text);
    if((best_point-hover_point).manhattanLength()<20)
    {
      draw_point-=QPointF(0.0,10.0);
      text = QString("(%1,%2,%3)").arg(best_estimate.first.x()).arg(best_estimate.first.y()).arg(best_estimate.second);
      painter->drawText(draw_point,text);
    }
    if((belief_point-hover_point).manhattanLength()<20)
    {
      draw_point-=QPointF(0.0,10.0);
      text = QString("(%1,%2,%3)").arg(belief.first.x()).arg(belief.first.y()).arg(belief.second);
      painter->drawText(draw_point,text);
    }
  }
  painter->setMatrix(matrix0);
  painter->translate(belief_point);
  painter->rotate(-belief.second);
  painter->setPen(Qt::red);
  painter->drawEllipse(QPointF(0.0,0.0),25.0,25.0);
  painter->drawLine(QPointF(0.0,0.0),QPointF(50.0,0.0));
  painter->setMatrix(matrix0);
  painter->translate(best_point);
  painter->rotate(-best_estimate.second);
  painter->setPen(Qt::blue);
  painter->drawEllipse(QPointF(0.0,0.0),25.0,25.0);
  painter->drawLine(QPointF(0.0,0.0),QPointF(50.0,0.0));
}

QRectF MCLWidget::MCLItem::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

void MCLWidget::MCLItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
  draw_text = true;
  hover_point = event->pos();
}

void MCLWidget::MCLItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
  draw_text = false;
}

void MCLWidget::MCLItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
  hover_point = event->pos();
}

MCLWidget::WhitesItem::WhitesItem()
{
  this->setAcceptHoverEvents(true);
}

void MCLWidget::WhitesItem::setWhites(QVector<QPointF> whites_)
{
  whites = whites_;
}

void MCLWidget::WhitesItem::setBelief(MCLWidget::MCLItem::Pose2D bel_)
{
  bel=bel_;
}

void MCLWidget::WhitesItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::white);
#ifndef GLOBAL_WHITES
  painter->translate(bel.first.x(),-bel.first.y());
  painter->rotate(-bel.second);
  for(auto& w : whites)
  {
    painter->drawEllipse(QPointF(w.x()*100,-w.y()*100),3.0,3.0);
  }
#else
  QString text;
  for(auto& w : whites)
  {
    QPointF draw_point(w.x(),-w.y());
    if((draw_point-hover_point).manhattanLength()<30)
      text = QString("(%1,%2)").arg(w.x()).arg(w.y());
    painter->drawEllipse(draw_point,3.0,3.0);
  }
  if(draw_text)
    if(!text.isEmpty())
    {
      painter->setPen(Qt::darkBlue);
      painter->drawText(hover_point,text);
    }
#endif
}

QRectF MCLWidget::WhitesItem::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

void MCLWidget::WhitesItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
  draw_text = true;
  hover_point = event->pos();
}

void MCLWidget::WhitesItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
  draw_text = false;
}

void MCLWidget::WhitesItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
  hover_point = event->pos();
}
