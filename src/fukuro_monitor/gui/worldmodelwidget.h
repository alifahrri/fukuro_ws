#ifndef WOLDMODELWIDGET_H
#define WOLDMODELWIDGET_H

#include <QWidget>
#include <QGraphicsItem>
#include "fukuro_common/WorldModel.h"
#include "fukuro_common/RobotControlInfo.h"
#include "fukuro/core/field.hpp"

namespace Ui {
class WorldModelWidget;
}

class WorldModelWidget : public QWidget
{
  Q_OBJECT

public:
  explicit WorldModelWidget(QWidget *parent = 0);
  void updateWorld(const fukuro_common::WorldModel::ConstPtr& world);
  void updateControlInfo(const fukuro_common::RobotControlInfo::ConstPtr& control);
  ~WorldModelWidget();

private:
  Ui::WorldModelWidget *ui;

private:
  class Field : public QGraphicsItem
  {
  public:
    Field();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

  private:
    QVector<QLineF> lines;
    QVector<QLineF> circle_lines;
    QRectF center_circle;
  };

  class Robot : public QGraphicsItem
  {
  public:
    Robot();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  private:
    QPixmap pixmap;
  public:
    QPointF robot_pos;
    QPointF setpoint_pos;
    qreal angle;
    qreal setpoint_angle;
  };

  class Obstacles : public QGraphicsItem
  {
  public:
    Obstacles();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  public:
    QVector<QPointF> obstacles;
  };

  class Balls : public QGraphicsItem
  {
  public:
    Balls() : ball_visible(false) {}
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    bool ball_visible;
    QPointF ball_pos;
  };

private:
  Field *field;
  Robot *robot;
  Obstacles *obstacles;
  Balls *balls;
  QTimer *timer;
};

#endif // WOLDMODELWIDGET_H
