## fukuro_monitor  
all-in-one monitoring gui for fukuro  

### Nodes
- `fukuro_monitor_node` : the core node  

### Environment Variable  
- `$FUKURO` : robot name, required

### Subscribed Topics  
- `/localization`  
- `/localization/global_whites`  
- `/path_planning`  
- `/omnivision/Whites`  
- `/omnivision/Balls`  
- `$FUKURO/world_model`  
- `$FUKURO/robot_control_info`  
- `$FUKURO/robot_control`  
- `$FUKURO/vel_cmd`  
- `$FUKURO/vel_cmd_manual`  
- `$FUKURO/teammates`  

### Usage  
create your own `qt` widget, add your widget to `MonitorDialog`, subscribe to your required topic at the constructor of `MonitorDialog`. Note that if you use `QGraphicView` approach, you need to update you scene (not your graphic view nor item) to properly update the view of your scene (see `mclwidget.cpp` for example).  

#### Notes
instead of using `rqt_plugin` this package (and all other gui node in this project) uses `qt` application library directly with ROS. It is unknown whether this approach makes any performance boost or not, but I believe this provides more flexible programming models to integrate gui to ROS.  