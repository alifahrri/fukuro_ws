#include "strategyparam.h"
#include "std_msgs/String.h"
#include <string>

namespace fukuro {

StrategyParam::StrategyParam(ros::NodeHandle& node_) :
    node(node_)
{
    if(!ros::get_environment_variable(robot_name,"FUKURO"))
    {
        ROS_ERROR("robot name empty!!! export FUKURO=fukuro1");
        exit(-1);
    }

    strategy_pub = node.advertise<fukuro_common::StrategyParam>(robot_name+"/strategy_param",1024);
}

/*
void StrategyParam::reconfigure(const fukuro_strategy_param::StrategyConfig &config, uint32_t level)
{
    strategy_param_msg.angle_dribble_auto_on = config.angle_dribble_auto_on;
    strategy_param_msg.angle_mulai_dribble_bola = config.angle_mulai_dribble_bola;
    strategy_param_msg.error_positioning = config.error_positioning;
    strategy_param_msg.error_sudut_positioning = config.error_sudut_positioning;
    strategy_param_msg.min_error_posisi_homing = config.min_error_posisi_homing;
    strategy_param_msg.min_error_posisi_kick = config.min_error_posisi_kick;
    strategy_param_msg.min_error_sudut_homing = config.min_error_sudut_homing;
    strategy_param_msg.min_error_sudut_kick = config.min_error_sudut_kick;
    strategy_param_msg.radius_dribble_auto_on = config.radius_dribble_auto_on;
    strategy_param_msg.radius_mulai_dribble_bola = config.radius_mulai_dribble_bola;
    strategy_param_msg.radius_mulai_kick_off = config.radius_mulai_kick_off;
}
*/

void StrategyParam::loadYAMLSettings(std::__cxx11::string node_name)
{
    std::cout << "node name : " << node_name << '\n';
    std::string dynparam_load_cmd = std::string("rosrun dynamic_reconfigure dynparam load ");
    dynparam_load_cmd += node_name + " ~/fukuro_ws/src/fukuro_strategy_param/settings/";
    dynparam_load_cmd += robot_name + ".yaml &  ";
    std::cout << dynparam_load_cmd << '\n';
    std::system(dynparam_load_cmd.c_str());
}

void StrategyParam::process()
{
    publish();
}

inline
void StrategyParam::publish()
{
    strategy_pub.publish(strategy_param_msg);
}

}
