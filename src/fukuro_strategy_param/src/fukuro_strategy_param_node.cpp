#include <ros/ros.h>
#include "fukuro_common/StrategyParam.h"
#include "strategyparam.h"
#include <dynamic_reconfigure/server.h>

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_strategy_param");
  ros::NodeHandle node;
  fukuro::StrategyParam strategy_param(node);
  std::string robot_name;
  if(!ros::get_environment_variable(robot_name,"FUKURO"))
  {
    ROS_ERROR("robot name is empty!! export FUKURO=fukuro1");
    exit(-1);
  }
  //
  //dynamic_reconfigure::Server<fukuro_strategy_param::StrategyConfig> reconfigure_server;
  //
  //reconfigure_server.setCallback(boost::bind(&fukuro::StrategyParam::reconfigure,&strategy_param,_1,_2));
  strategy_param.loadYAMLSettings(ros::this_node::getName());
  ros::Rate loop(33);
  ros::AsyncSpinner spinner(2);
  spinner.start();
  while(ros::ok())
  {
    strategy_param.process();
    loop.sleep();
  }
  ros::waitForShutdown();
  return 0;
}
