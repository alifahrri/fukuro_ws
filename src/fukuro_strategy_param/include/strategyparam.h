#ifndef STRATEGY_PARAM_H
#define STRATEGY_PARAM_H

#include <tuple>
#include <ros/ros.h>
#include <ros/param.h>
#include <chrono>
//
//#include "fukuro_strategy_param/StrategyConfig.h"
#include "fukuro_common/StrategyParam.h"

namespace fukuro {

class StrategyParam
{
public:

  StrategyParam(ros::NodeHandle& node_);
  //
  //void reconfigure(const fukuro_strategy_param::StrategyConfig& config, uint32_t level);
  void loadYAMLSettings(std::string node_name);
  void process();

private:
  void publish();
  void control();

private:
  ros::NodeHandle& node;
  ros::Publisher strategy_pub;
  std::string robot_name;
  fukuro_common::StrategyParam strategy_param_msg;

};

}
#endif // ROBOTCONTROL_H
