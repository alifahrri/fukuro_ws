#ifndef MOTOR_HPP
#define MOTOR_HPP

#include <vector>
#include <array>
#include <map>

#define N_LINEAR_SPEED    6
#define N_ROTATION_SPEED  6
#define N_MAX_MOTOR       4

namespace fukuro {

enum speed_t {
  Speed025 = 0,   //speed = 25 cm per sec
  Speed050 = 1,
  Speed075 = 2,
  Speed100 = 3,
  Speed125 = 4,
  Speed150 = 5    //speed = 150 cm per sec
};

enum rotation_speed_t {
  SpeedPhi6 = 0,  //speed = phi/6 per sec
  SpeedPhi5 = 1,
  SpeedPhi4 = 2,
  SpeedPhi3 = 3,
  SpeedPhi2 = 4,
  SpeedPhi1 = 5   //speed = phi per sec
};

typedef std::map<speed_t,double> MotorLinearSpeedMap;
typedef std::vector<MotorLinearSpeedMap> MotorLinearSpeedMaps;

struct MotorParameter
{

  static constexpr
  std::array<speed_t, N_LINEAR_SPEED> linear_speeds {
  {
    speed_t::Speed025,
    speed_t::Speed050,
    speed_t::Speed075,
    speed_t::Speed100,
    speed_t::Speed125,
    speed_t::Speed150
  } };

  static constexpr
  std::array<rotation_speed_t, N_ROTATION_SPEED> rotation_speeds {
  {
    rotation_speed_t::SpeedPhi6,
    rotation_speed_t::SpeedPhi5,
    rotation_speed_t::SpeedPhi4,
    rotation_speed_t::SpeedPhi3,
    rotation_speed_t::SpeedPhi2,
    rotation_speed_t::SpeedPhi1
  } };

  MotorParameter() {
    linear_map_cw.resize(N_MAX_MOTOR);
    linear_map_ccw.resize(N_MAX_MOTOR);

    auto ccw = linear_map_ccw.begin();
    auto ccw_end = linear_map_ccw.end();
    for(auto &cw : linear_map_cw)
    {
      for(size_t i=0; i<6; i++)
      {
        auto s = static_cast<speed_t>(i);
        auto pair = std::make_pair(s,0.0);
        cw.insert(pair);
        ccw->insert(pair);
      }
      ccw++;
    }
  }

  MotorLinearSpeedMaps linear_map_cw;
  MotorLinearSpeedMaps linear_map_ccw;
  std::map<rotation_speed_t, double> rotation_map_cw;
  std::map<rotation_speed_t, double> rotation_map_ccw;
  double linear(double speed);
  double rotation(double speed);
};

}
#endif // MOTOR_HPP
