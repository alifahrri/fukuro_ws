#ifndef HWCONTROLLER_H
#define HWCONTROLLER_H
#include <QSerialPort>
#include <QIODevice>
#include <QObject>
#include <QVector>
#include <QString>
#include <thread>
#include <mutex>
#include <chrono>
#include <array>
#include <atomic>
#include <functional>
#include <iostream>
#include <ros/ros.h>
#include "fukuro_common/WorldModel.h"
#include "fukuro_common/Compass.h"
#include "fukuro_common/Encoder.h"
#include "fukuro_common/Teammates.h"
#include "fukuro_common/Shoot.h"
#include "fukuro_common/HWControllerManual.h"
#include "fukuro_common/HWController.h"
#include "fukuro_common/HWControllerCommand.h"
#include "fukuro_common/HWControllerService.h"
#include "fukuro_common/HWControllerParamService.h"

//#include "fukuro/core/ann.hpp"
//#include "fukuro/core/csvreader.hpp"

#define DEBUG_HWCONTROLLER
#define ROBOT_CMD
#define USE_CMPS11

#define N_LINEAR_SPEED    6
#define N_ROTATION_SPEED  6

namespace fukuro {

//@TODO : remove these callbacks
typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::duration<double,std::milli> Duration;
typedef std::function<void(int,int,int,int,int,bool,double)> RawDataUpdatedCallback;
typedef std::function<void(double,double,double,double,double,bool,double)> DataUpdatedCallBack;
typedef std::function<void(double,double,double)> RobotVelocityUpdatedCallback;
typedef std::function<void(double,double,double,double,double,double)> OdometryCallback;
typedef std::function<void(std::vector<int>,std::vector<int>)> ControlCallback;
typedef std::function<void(int,int,int,int,int,int)> PWMUpdatedCallback;
typedef std::function<void(int,int,int,int,int,double,double,double,double,double)> PWMEncoderCallback;
typedef std::function<void(int)> ArduinoCallback;
typedef std::function<void(double)> ArduinoCMPSCallback;

class HWController : public QObject
{
    Q_OBJECT
public:

    enum rx_state_t {
        start_U,
        start_G,
        start_M,
        state_VE1A,
        state_VE1B,
        state_VE2A,
        state_VE2B,
        state_VE3A,
        state_VE3B,
        state_VE4A,
        state_VE4B,
        state_direction,
        state_kick,
        state_adc_charge,
        checksum
    };

    enum rx_state_arduino_t {
      Arduino_start_U,
      Arduino_start_G,
      Arduino_start_M,
      state_cmps_high_byte,
      state_cmps_low_byte,
      state_key
    };

    enum speed_t {
      Speed025 = 0,   //speed = 25 cm per sec
      Speed050 = 1,
      Speed075 = 2,
      Speed100 = 3,
      Speed125 = 4,
      Speed150 = 5    //speed = 150 cm per sec
    };

    enum rotation_speed_t {
      SpeedPhi6 = 0,  //speed = phi/6 per sec
      SpeedPhi5 = 1,
      SpeedPhi4 = 2,
      SpeedPhi3 = 3,
      SpeedPhi2 = 4,
      SpeedPhi1 = 5   //speed = phi per sec
    };

    enum base_t {
      BaseTypeA = 0,
      BaseTypeB = 1,
    };

    static constexpr
    std::array<speed_t, N_LINEAR_SPEED> linear_speeds {
    {
      speed_t::Speed025,
      speed_t::Speed050,
      speed_t::Speed075,
      speed_t::Speed100,
      speed_t::Speed125,
      speed_t::Speed150
    } };

    static constexpr
    std::array<rotation_speed_t, N_ROTATION_SPEED> rotation_speeds {
    {
      rotation_speed_t::SpeedPhi6,
      rotation_speed_t::SpeedPhi5,
      rotation_speed_t::SpeedPhi4,
      rotation_speed_t::SpeedPhi3,
      rotation_speed_t::SpeedPhi2,
      rotation_speed_t::SpeedPhi1
    } };

    HWController(double loop_rate=300.0, bool ctrl=false);
    HWController(ros::NodeHandle &node);
    ~HWController();

    bool connect(std::string port, int baud_rate = 115200);
    bool connectArduino(std::string port, int baud_rate = 115200);
    bool disconnect();
    bool disconnectArduino();

    //@TODO : remove
    void setPID(std::vector<double> kp_, std::vector<double> kd_);
    void setMotorVelocity(double w1, double w2, double w3, double w4, double w5);
    void setFF(int ff1, int ff2, int ff3, int ff4, int ff5, int kick);
    void setCallback(DataUpdatedCallBack cb) { data_cb = cb; }
    void setArduinoCallback(ArduinoCallback cb) { arduino_cb = cb; }
    void setArduinoCMPSCallback(ArduinoCMPSCallback cb) { arduino_cmps_cb = cb; }
    void setRawDataCallback(RawDataUpdatedCallback cb) { raw_data_cb = cb; }
    void setRobotVelCallback(RobotVelocityUpdatedCallback cb) { robot_vel_cb = cb; }
    void setControlCallback(ControlCallback cb) { control_cb = cb; }
    void setOdometryCallback(OdometryCallback cb) { odometry_cb = cb; }
    void setPWMUpdateCallback(PWMUpdatedCallback cb) { pwm_update_cb = cb; }
    void setTargetRobotVel(double vx, double vy, double w);

    void setMotorParameter(int i, double ratio, double step, bool cw=true, speed_t speed=Speed100);
    void setDribbler(bool d, bool in=true, int speed=150);
    void resetOdometry(double px, double py, double theta);
    void getGoalDistance(double theta, double x);

    void requestExtensi() {
        std::cout << "[HWController] request extensi" << std::endl;
        extensi_request = true;
    }
    void requestKick(int codename);

    void stop() {
        running = false;
    }

    void resetSTM();
    std::vector<double> motorParam(speed_t speed);
    void setOffsetCmps();

private:
    void init();
    void loop();
    void updateSerialInfo();
    void updateData();

    //@TODO : remove these
    void updateRobotVel();
    void updateOdometry();
    void motorControl(int i);
    void updateMotorVelocity();

    void motorControl();
    bool updateState(uchar data);
    bool updateStateArduino(uchar data);
    void motor_multiplier(double speed, double wheel[], double *ratio);
    double linear(double x0, double x1, double y0, double y1, double xi);
    double goal_dist;
    bool goal_dist_state;
    int codename;

private slots:
    void readSerial();
    void readSerialArduino();
    void sendData();

signals:
    void sendSerialData();

public:
    void start();
    void publish();
    void publishCompass();
    void getRobotVel(double *vx, double *vy, double *w);
    void loadSettings(const std::string &filename);
    void saveSettings(const std::string &filename);
    void updateTeammates(const fukuro_common::TeammatesConstPtr &msg);
    void updateKickDel(const fukuro_common::WorldModelConstPtr &msg);
    void updateHWControlCmd(const fukuro_common::HWControllerCommandConstPtr &msg);
    void updateHWControlManual(const fukuro_common::HWControllerManualConstPtr &msg);
    bool kickService(fukuro_common::ShootRequest &req, fukuro_common::ShootResponse &res);
    bool hwctrlService(fukuro_common::HWControllerServiceRequest &req, fukuro_common::HWControllerServiceResponse &res);
    bool controlParamService(fukuro_common::HWControllerParamServiceRequest &req, fukuro_common::HWControllerParamServiceResponse &res);
    std::string robotName();

private :
    struct MotorParameter
    {
      MotorParameter();
      std::map<speed_t, double> linear_map_cw;
      std::map<speed_t, double> linear_map_ccw;
      std::map<rotation_speed_t, double> rotation_map_cw;
      std::map<rotation_speed_t, double> rotation_map_ccw;
      double linear(double speed);
      double rotation(double speed);
    };
    template <size_t n>
    struct Encoder
    {
      Encoder()
      {
        for(size_t i=0; i<n; i++)
        {
          current_tick[i] = 0;
          total_tick[i] = 0;
        }
      }
      int current_tick[n];
      int total_tick[n];
      size_t size = n;
      void operator()(int e[])
      {
        for(size_t i=0; i<n; i++)
        {
          current_tick[i] = e[i];
          total_tick[i] += e[i];
        }
      }
    };

private:
    ros::Publisher hwctrl_pub;
    ros::Publisher encoder_pub;
    ros::Publisher compass_pub;
    Encoder<4> encoder;

private:
    QSerialPort *serial;
    QSerialPort *serial_arduino;
    QVector<QString> motor_param_list;
    std::atomic_bool running      = {false}; //list-initialization stuff
    std::atomic_bool kick_request = {false};
    std::atomic_bool data_updated = {false};
    std::atomic_bool extensi_request  = {false};
    std::atomic_bool serial_connected = {false};
    std::atomic_bool writing_serial   = {false};
    std::atomic_bool serial_arduino_connected = {false};
    std::atomic_bool is_manual_positioning = {false};
    std::thread thread;
    std::mutex mutex;
    std::string robot_name;
    std::vector<std::string> portlist;
    std::vector<std::string> manufacturer;
    rx_state_t rx_state = start_U;
    rx_state_arduino_t rx_state_arduino = Arduino_start_U;

    //@TODO : remove these callbacks
    DataUpdatedCallBack data_cb         = nullptr;
    ArduinoCallback arduino_cb          = nullptr;
    ArduinoCMPSCallback arduino_cmps_cb = nullptr;
    RawDataUpdatedCallback raw_data_cb  = nullptr;
    RobotVelocityUpdatedCallback robot_vel_cb = nullptr;
    ControlCallback control_cb          = nullptr;
    OdometryCallback odometry_cb        = nullptr;
    PWMUpdatedCallback pwm_update_cb    = nullptr;

    Clock::time_point update_data_time_point;
    Duration wait_ms;
    double cmps;
    int ff[5]   = {0,0,0,0,0};
    int ve[5]   = {0,0,0,0,0};
    int pwm [5] = {0,0,0,0,0};
    int p_term[5] = {0,0,0,0,0};
    int d_term[5] = {0,0,0,0,0};
    int ff_counter[5]   = {0,0,0,0,0};
    int control_pwm[5]  = {0,0,0,0,0};
    double kp[5] = {0.1,0.1,0.1,0.1,0.1};
    double kd[5] = {0.15,0.15,0.15,0.15,0.15};
    double error[5]   = {0.0,0.0,0.0,0.0,0.0};
    double d_error[5] = {0.0,0.0,0.0,0.0,0.0};
    int ve_setpoint[4] = {0,0,0,0};
    double wheel[4] = {0.0,0.0,0.0,0.0};
    double vt[5]    = {0.0,0.0,0.0,0.0,0.0};
    double ve_rad_s[5]  = {0.0,0.0,0.0,0.0,0.0};
    double robot_vx = 0.0;
    double robot_vy = 0.0;
    double robot_w  = 0.0;
    double target_robot_vx  = 0.0;
    double target_robot_vy  = 0.0;
    double target_robot_w   = 0.0;
    double robot_x      = 0.0;
    double robot_y      = 0.0;
    double robot_theta  = 0.0;
    MotorParameter motor_param[4];
    base_t base;
    std::string base_type;

    //@TODO : simplify these using struct
    double motor_ratio_cw_025[4]  = {0,0,0,0};
    double motor_ratio_ccw_025[4] = {0,0,0,0};
    double motor_ratio_cw_050[4]  = {0,0,0,0};
    double motor_ratio_ccw_050[4] = {0,0,0,0};
    double motor_ratio_cw_075[4]  = {0,0,0,0};
    double motor_ratio_ccw_075[4] = {0,0,0,0};
    double motor_ratio_cw_100[4]  = {0,0,0,0};
    double motor_ratio_ccw_100[4] = {0,0,0,0};
    double motor_ratio_cw_125[4]  = {0,0,0,0};
    double motor_ratio_ccw_125[4] = {0,0,0,0};
    double motor_ratio_cw_150[4]  = {0,0,0,0};
    double motor_ratio_ccw_150[4] = {0,0,0,0};
    double motor_step_cw[4]   = {0,0,0,0};
    double motor_step_ccw[4]  = {0,0,0,0};
    double dt;
    char buffer[20];
    bool ir;
    bool pwm_test=0;
    bool enable_control;
    int adc_charge;
    int kick_del;
    double offsetCmps;
    double finalCmps;
    double encoder_ppr;
};

}


#endif // HWCONTROLLER_H
