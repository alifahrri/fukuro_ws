#ifndef ROBOTSTATE
#define ROBOTSTATE

#include <tuple>
#include <vector>
#include <cmath>
#include <atomic>
#include <iostream>

#define PI2 2*M_PIl

namespace fukuro {

struct RobotState {
    RobotState() {}
    RobotState(double x, double y, double w) { state = std::make_tuple(x,y,w); }
    RobotState operator+(RobotState s) {
        return RobotState(
                    x()+s.x(),
                    y()+s.y(),
                    w()+s.w());
    }
    RobotState operator-(RobotState s) {
        return RobotState(
                    x()-s.x(),
                    y()-s.y(),
                    w()-s.w());
    }
    RobotState operator*(double s) {
        return RobotState(
                    x()*s,
                    y()*s,
                    w()*s);
    }
    RobotState operator/(double s) {
        return RobotState(
                    x()/s,
                    y()/s,
                    w()/s);
    }
	RobotState operator*(RobotState s) {
		return RobotState(
					x()*s.x(),
					y()*s.y(),
					w()*s.w());
	}

    bool operator !=(RobotState s) {
        return (x()!=s.x())||(y()!=s.y());
    }

    bool operator<(RobotState& s) {
        return x()<s.x();
    }

    double& x() { return std::get<0>(state); }
    double& y() { return std::get<1>(state); }
    double& w() { return std::get<2>(state); }
    std::tuple<double,double,double> state;
//    std::tuple<std::atomic<double>,std::atomic<double>,std::atomic<double>> state;
};

typedef std::pair<RobotState,RobotState> RobotState6;
typedef std::tuple<RobotState,RobotState,RobotState> RobotState9;

inline RobotState& pos(RobotState9& rs)
{
    return std::get<0>(rs);
}

inline RobotState& pos(RobotState6& rs)
{
    return std::get<0>(rs);
}

inline std::pair<double,double> angular_dis(double w0, double w1)
{
    double cw_dist=0.0, ccw_dist=0.0;
    if(w0>w1) {
        cw_dist = w0-w1;
        ccw_dist = PI2 - cw_dist;
    }
    else if(w0<w1) {
        ccw_dist = w1-w0;
        cw_dist = PI2 - ccw_dist;
    }
    return std::make_pair(cw_dist,ccw_dist);
}

inline std::pair<double,double> angular_dis(RobotState9& p0, RobotState9& p1)
{
    double cw_dist=0.0, ccw_dist=0.0;
    if(pos(p0).w()>pos(p1).w()) {
        cw_dist = pos(p0).w()-pos(p1).w();
        ccw_dist = PI2 - cw_dist;
    }
    else if(pos(p0).w()<pos(p1).w()) {
        ccw_dist = pos(p1).w()-pos(p0).w();
        cw_dist = PI2 - ccw_dist;
    }
    return std::make_pair(cw_dist,ccw_dist);
}

inline std::pair<double,double> angular_dis(RobotState& r0, RobotState& r1)
{
    double cw_dist=0.0, ccw_dist=0.0;
    if(r0.w()>r1.w()) {
        cw_dist = r0.w()-r1.w();
        ccw_dist = PI2 - cw_dist;
    }
    else if(r0.w()<r1.w()) {
        ccw_dist = r1.w()-r0.w();
        cw_dist = PI2 - ccw_dist;
    }
    return std::make_pair(cw_dist,ccw_dist);
}

inline RobotState& vel(RobotState9& rs)
{
    return std::get<1>(rs);
}

inline RobotState& vel(RobotState6& rs)
{
    return std::get<1>(rs);
}

inline RobotState& accel(RobotState9& rs)
{
    return std::get<2>(rs);
}

inline double length(RobotState s)
{
    return sqrt(s.x()*s.x()+s.y()*s.y());
}

inline double length(RobotState s0, RobotState s1)
{
    return sqrt(pow(s0.x()-s1.x(),2)+pow(s0.y()-s1.y(),2));
}

inline void normalise_angle(double& a)
{
    while((a>M_PI) || (a<-M_PI)) {
        if(a>M_PI) {
            a = -M_PI+fabs((a-M_PI));
        }
        else if(a<(-M_PI)) {
            double tm = (fabs(a)-M_PI);
            a = M_PI-tm;
        }
    }
}

inline void add_ranged_angle(RobotState& r, double a)
{
    r.w() += a;
    normalise_angle(r.w());
}

inline int quad(double& w)
{
    normalise_angle(w);
    if((w>=0) && (w<=M_PI_2))
        return 1;
    else if((w>M_PI_2) && (w<=M_PI))
        return 2;
    else if((w<0) && (w>=-M_PI_2))
        return 4;
    else if((w<-M_PI_2) && (w>=-M_PI))
        return 3;
    return 0;
}

inline int quad(RobotState& rs)
{
    normalise_angle(rs.w());
    if((rs.w()>=0) && (rs.w()<=M_PI_2))
        return 1;
    else if((rs.w()>M_PI_2) && (rs.w()<=M_PI))
        return 2;
    else if((rs.w()<0) && (rs.w()>=-M_PI_2))
        return 4;
    else if((rs.w()<-M_PI_2) && (rs.w()>=-M_PI))
        return 3;
//    quad(rs);
    std::cout << "quad 0!!! " << rs.w() << "\n";
    return 0;
}

inline double angle(RobotState s0, RobotState s1, RobotState s2) {
    double a = length(s0,s1);
    double b = length(s1,s2);
    double c = length(s0,s2);
    double u = (a*a)+(b*b)-(c*c);
    double l = (2*a*b);
    double tmp = u/l;
    return acos(tmp);
}

inline double angle(RobotState s0, RobotState s1) {
    return atan2(s1.y()-s0.y(),s1.x()-s0.x());
}

/*
inline void set_linear_velocity(RobotState& r, double mag, double angle)
{
    r.x() = -mag*sin(angle-M_PIl*2);
    r.y() = mag*cos(angle-M_PIl*2);
    std::cout << "set linear vel : " << r.x() << "," << r.y() << " angle : " << angle << '\n';
}
*/

inline void set_linear_velocity(RobotState6& r, RobotState& v, double f=1.0)
{
    r.second.x() = v.x()*f;
    r.second.y() = v.y()*f;
}

inline RobotState linear_velocity(double mag, double angle)
{
    return RobotState(mag*cos(angle),mag*sin(angle),0);
}

inline std::ostream& operator << (std::ostream& out, RobotState& s) {
    out << s.x() << "," << s.y() << "," << s.w();
    return out;
}

inline RobotState get_local_velocity(RobotState9& rs)
{
	RobotState ret;
	double c = cos(pos(rs).w());
	double s = sin(pos(rs).w());
	ret.x() = c*vel(rs).x()+s*vel(rs).y();
	ret.y() = -s*vel(rs).x()+c*vel(rs).y();
	ret.w() = vel(rs).w();
	return ret;
}

inline RobotState6 operator - (RobotState6 l, RobotState6 r)
{
    RobotState6 ret = std::make_pair(pos(l)-pos(r),vel(l)-vel(r));
    return ret;
}

}
#endif // ROBOTSTATE
