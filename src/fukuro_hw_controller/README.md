# the fukuRŌ hardware controller

### what does it do?    
this package should do the followings :   

*  provides motor control, which can be achieved by :  
    -  convert robot velocity to motor velocity  
    +  feedforward pwm value based on motor velocity  
    *  read encoder to provides data for odometry (and possibility of feedback motor control)  
    -  read compass  
    +  publish control pwm, encoder value and compass reading  
-  tuning and configuration :  
    *  for better flexibility, the core of hardware control should be separated from gui  
    +  then feedforward tuning should be done via ros service interface  
-  load and save feedforwad pwm and encoder ppr settings from and to settings file : `/settings/robotname.ini`

### Changelog :

- subscribed topic to be `/robotname/hwcontrol_command` of type `HWControllerCommand` from package `fukuro_common`
- published topic to be `/robotname/hwcontrol_info` of type `HWController` msg from `fukuro_common`
    packed into `Commpass` an `Encoder` msg type plus `int8` for ir, and `String` for base type
- advertise service `/robotname/hw_service` of type `HWControllerService` from `fukuro_common`
- advertise service `/robotname/hw_param_service` for type `HWControllerParamService` from `fukuro_common`
- strip gui
    
### To Do List :   

- clean the mess   
- separate gui   
- implements needed functionality :
    - tuning service
    - connect service
- test   
- PID Motor Control
