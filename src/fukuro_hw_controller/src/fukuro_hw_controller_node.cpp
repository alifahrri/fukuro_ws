#include <ros/ros.h>
#include <csignal>
#include <fukuro_common/WorldModel.h>
#include <fukuro_common/OdometryInfo.h>
#include <fukuro_common/OdometryService.h>
#include <fukuro_common/VelCmd.h>
#include <fukuro_common/DribblerControl.h>
#include <fukuro_common/Shoot.h>
#include <fukuro_common/KeypadService.h>
#include <fukuro_common/Compass.h>
#include <fukuro_common/PWM.h>
#include "mainwindow.h"
#include "mainwindowwrapper.h"
#include <QCoreApplication>
#include <string>
#include "hwcontroller.h"
#include <fukuro_common/HWControllerService.h>
#include <fukuro_common/Teammates.h>
#include <fukuro_common/HWControllerManual.h>

std::vector<std::string> portlist;
std::vector<std::string> manufacturer;
bool isManualPositioning = false;

//@TODO : REMOVE
#if 0
typedef boost::function<void(const fukuro_common::Teammates::ConstPtr&)> TeammatesCallback;
typedef boost::function<void(const fukuro_common::WorldModel::ConstPtr&)> FukuroWorldModelCallback;
typedef boost::function<void(const fukuro_common::VelCmd::ConstPtr&)> FukuroVelCmdCallback;
typedef boost::function<void(const fukuro_common::HWControllerManual::ConstPtr&)> FukuroVelCmdManualCallback;
typedef boost::function<void(const fukuro_common::DribblerControl::ConstPtr&)> DribblerCmdCallback;
typedef boost::function<bool(fukuro_common::OdometryService::Request&,fukuro_common::OdometryService::Response&)> OdometryServiceCallback;
typedef boost::function<bool(fukuro_common::Shoot::Request&,fukuro_common::Shoot::Response&)> KickServiceCallback;
typedef boost::function<bool(fukuro_common::HWControllerService::Request&,fukuro_common::HWControllerService::Response&)> HWServiceCallback;

QApplication *app = NULL;

namespace
{
volatile std::sig_atomic_t gSignalStatus;
}

void signal_handler(int signal)
{
    gSignalStatus = signal;
    std::cout << "SIGINT called\n";
    if(app)
        app->quit();
    if(ros::ok())
        ros::shutdown();
    //  return 0;
}
#endif

int main(int argc, char *argv[])
{
    ros::init(argc,argv,"fukuro_hw_controller");
    QCoreApplication app(argc,argv);
    ros::NodeHandle node;
    fukuro::HWController hw_controller(node);

    bool autoconnect = false;
    bool autosave = false;
    std::string stm_device;
    std::string arduino_device;

    node.getParam("autoconnect",autoconnect);
    node.getParam("autosave",autosave);
    node.getParam("stm_device",stm_device);
    node.getParam("arduino_device",arduino_device);

    if(autoconnect)
    {
      hw_controller.connect(stm_device);
      hw_controller.connectArduino(arduino_device);
    }

    ROS_WARN("parameter : %d, %s, %s", autoconnect, stm_device.c_str(), arduino_device.c_str());

    std::string fukuro_path;
    ros::get_environment_variable(fukuro_path, "FUKURO_WS");
    if(fukuro_path.empty())
    {
      std::stringstream ss;
      ss << "/home/" << std::getenv("USER")
         << "/fukuro_ws/";
      fukuro_path = ss.str();
      ROS_WARN("env variable FUKURO_WS not specified, set to default : %s", fukuro_path.c_str());
    }
    std::stringstream ss;
    ss << "src/fukuro_hw_controller/settings/"
       << std::getenv("FUKURO") << ".ini";
    hw_controller.loadSettings(fukuro_path + ss.str());

    auto wm_topic = hw_controller.robotName() + "/world_model";
    auto teammates_topic = hw_controller.robotName() + "/teammates";
    auto topic = hw_controller.robotName() + "/hwcontrol_command";
    auto topic_man = hw_controller.robotName() + "/vel_cmd_manual";
    auto service_name = hw_controller.robotName() + "/hw_service";
    auto param_srv_name = hw_controller.robotName() + "/hw_param_service";
    ros::Subscriber hwctrl_command = node.subscribe(topic, 3, &fukuro::HWController::updateHWControlCmd, &hw_controller);
    ros::Subscriber hwctrl_command_manual = node.subscribe(topic_man, 3, &fukuro::HWController::updateHWControlManual, &hw_controller);
    ros::Subscriber hwctrl_teammates = node.subscribe(teammates_topic, 3, &fukuro::HWController::updateTeammates, &hw_controller);
    ros::ServiceServer hw_server = node.advertiseService(service_name, &fukuro::HWController::hwctrlService, &hw_controller);
    ros::ServiceServer hw_param_server = node.advertiseService(param_srv_name, &fukuro::HWController::controlParamService, &hw_controller);
    ros::ServiceServer kick_service_server = node.advertiseService( hw_controller.robotName()+("/kick_service"),&fukuro::HWController::kickService, &hw_controller);
    ros::Subscriber world_model_callback = node.subscribe(wm_topic, 3, &fukuro::HWController::updateKickDel, &hw_controller);

                                               hw_controller.start();

    //    ros::AsyncSpinner spinner(2);
    //    spinner.start();
    //    ros::waitForShutdown();
    auto timer = node.createTimer(ros::Duration(0.001),[&](const ros::TimerEvent& event)
    {
      app.processEvents();
      if(!ros::ok())
        app.quit();
    });
    timer.start();

    ros::spin();
    hw_controller.stop();

    //autosave
    if(autosave)
      hw_controller.saveSettings(fukuro_path + ss.str());
    return 0;
#if 0
    QApplication a(argc, argv);
    char* env_robot_name = std::getenv("FUKURO");
    app = &a;
    bool min_gui = false;
    bool use_gui = false;
    bool tuning = false;
    std::string robot_name("fukuro1");
    std::string serial_port("/dev/ttyACM0");
    std::string serial_port_arduino("/dev/ttyUSB0");
    if(argc>1)
        for(int i=1; i<argc; i++)
        {
            std::string arg(argv[i]);
            std::cout << arg << '\n';
            if(arg==std::string("minimum_gui"))
                min_gui = true;
            else if(arg.find(std::string("fukuro"))!=std::string::npos)
                robot_name = arg;
            else if(arg.find(std::string("/dev/tty"))!=std::string::npos)
                serial_port = arg;
            else if(arg.find(std::string("tuning"))!=std::string::npos)
                tuning = true;
        }
    tuning = true;
    if(env_robot_name==NULL)
    {
        ROS_ERROR("robot name is NULL. export FUKURO=fukuro1!!!");
        exit(-1);
    }
    robot_name = std::string(env_robot_name);

    //ROS
    ros::NodeHandle node;
    ros::Publisher odometry_pub = node.advertise<fukuro_common::OdometryInfo>(robot_name+std::string("/odometry_info"),10);
    ros::Publisher compass_pub = node.advertise<fukuro_common::Compass>(robot_name+std::string("/compass"),10);
    ros::Publisher pwm_pub = node.advertise<fukuro_common::PWM>(robot_name+std::string("/pwm"),10);
    fukuro_common::OdometryInfo odom;


    //MainWIndow
    QSettings settings(QString("/home/")+QString(std::getenv("USER"))+QString("/fukuro_ws/src/fukuro_hw_controller/settings/")+QString::fromStdString(robot_name)+QString(".ini"),
                       QSettings::IniFormat);
    //QSettings settings(QSettings::IniFormat,QSettings::UserScope,QString("fukuro"),QString("hw_controller"));
    settings.beginGroup(QString::fromStdString(robot_name));

    fukuro::HWController controller(40.0);

    FukuroWorldModelCallback wm_callback = [&controller](const fukuro_common::WorldModel::ConstPtr& wm)
    {
        controller.getGoalDistance(wm->pose.theta,wm->pose.x);
    };

    std::vector<double> cw, ccw, cw_step, ccw_step;
    std::vector<double> kp, kd;

    QVector<QString> motor_param_list;
    for(int i=0; i<4; i++)
    {
        motor_param_list.append(QString("motor%1_cw").arg(i+1));
        motor_param_list.append(QString("motor%1_ccw").arg(i+1));
        motor_param_list.append(QString("motor%1_cw_025").arg(i+1));
        motor_param_list.append(QString("motor%1_ccw_025").arg(i+1));
        motor_param_list.append(QString("motor%1_cw_050").arg(i+1));
        motor_param_list.append(QString("motor%1_ccw_050").arg(i+1));
        motor_param_list.append(QString("motor%1_cw_075").arg(i+1));
        motor_param_list.append(QString("motor%1_ccw_075").arg(i+1));
        motor_param_list.append(QString("motor%1_cw_125").arg(i+1));
        motor_param_list.append(QString("motor%1_ccw_125").arg(i+1));
        motor_param_list.append(QString("motor%1_cw_150").arg(i+1));
        motor_param_list.append(QString("motor%1_ccw_150").arg(i+1));
    }
    for(const QString& str : motor_param_list)
    {
        if(settings.contains(str))
        {
            auto val = settings.value(str).toDouble();
            auto idx = 1;
            auto cw = str.contains("_cw");
            if(str.contains("2_"))
                idx = 2;
            else if(str.contains("3_"))
                idx = 3;
            else if(str.contains("4_"))
                idx = 4;
            fukuro::HWController::speed_t speed;
            if(str.contains(QString("025")))
                speed = fukuro::HWController::Speed025;
            else if(str.contains(QString("050")))
                speed = fukuro::HWController::Speed050;
            else if(str.contains(QString("075")))
                speed = fukuro::HWController::Speed075;
            else if(str.contains(QString("125")))
                speed = fukuro::HWController::Speed125;
            else if(str.contains(QString("150")))
                speed = fukuro::HWController::Speed150;
            else
                speed = fukuro::HWController::Speed100;
            controller.setMotorParameter(idx,val,15,cw,speed);
        }
    }

    MainWindow w(min_gui);
    MainWindowWrapper wrapper;

    TeammatesCallback tm_callback = [&](const fukuro_common::Teammates::ConstPtr &tm){
        isManualPositioning = tm->isManualPositioning;
    };

    ros::Subscriber tm_sub = node.subscribe(robot_name+std::string("/teammates"),50,tm_callback);

    HWServiceCallback hw_service_callback = [&](fukuro_common::HWControllerService::Request &req,
            fukuro_common::HWControllerService::Response &res){

        if(req.refresh){
            portlist.clear();
            manufacturer.clear();
            w.updateSerialInfo();
            portlist = w.getportlist();
            manufacturer = w.getmanufacturer();
            for(auto s : portlist){
                res.port_list.push_back(s);
            }
            for(auto m : manufacturer){
                res.manufacturer_list.push_back(m);
            }
        }
        if(portlist.size()!=0){
            if(req.isArduino){
                w.connectArduino(req.ArduinoConnect);
                res.ArduinoSuccess = w.getArduinoConnection();
            }

            if(req.isSTM){
                w.connectSerial(req.STMConnect);
                res.STMSuccess = w.getSTMConnection();
            }
        }

        if(req.Compass){
            w.updateOffsetCmps();
            res.Compassuccess = w.getisCompass();
        }
        return true;
    };

    ros::ServiceServer hw_service_server = node.advertiseService(robot_name+std::string("/hw_service"),hw_service_callback);

    w.setReadParamCallback([&controller](int index)
    {
        return controller.motorParam(static_cast<fukuro::HWController::speed_t>(index));
    });

    if(tuning)
    {
        wrapper.connect(&wrapper,SIGNAL(updateRawValue(int,int,int,int,int,bool,double))
                        ,&w,SLOT(updateRawValue(int,int,int,int,int,bool,double)));

        wrapper.connect(&wrapper,SIGNAL(updateValue(double,double,double,double,double,bool,double))
                        ,&w,SLOT(updateValue(double,double,double,double,double,bool,double)));

        wrapper.connect(&wrapper,SIGNAL(updateVelocity(double,double,double))
                        ,&w,SLOT(updateRobotVel(double,double,double)));

        wrapper.connect(&wrapper,SIGNAL(updateOdometry(double,double,double,double,double,double))
                        ,&w,SLOT(updateOdometry(double,double,double,double,double,double)));

        wrapper.connect(&wrapper,SIGNAL(updatePWM(int,int,int,int,int,int)),
                        &w,SLOT(updatePWMValue(int,int,int,int,int,int)));

        wrapper.connect(&wrapper,SIGNAL(updateControl(int,int,int,int,int,int))
                        ,&w,SLOT(updateControl(int,int,int,int,int,int)));
    }

    w.readSettings();

    OdometryServiceCallback odom_service_callback = [&controller](fukuro_common::OdometryService::Request& req, fukuro_common::OdometryService::Response& res)
    {
        controller.resetOdometry(req.x,req.y,req.w);
        res.ok = 1;
#ifdef DEBUG_ODOMETRY
        std::cout << "[OdometryCallback] reset odometry : " << odom.x << "," << odom.y << "," << odom.theta << '\n';
#endif
        return true;
    };

    KickServiceCallback kick_service_callback = [&controller](fukuro_common::Shoot::Request& req, fukuro_common::Shoot::Response& res)
    {
        std::cout << "[KickServiceCallback] request kicker\n";
        if(req.kick_request==3)
            controller.requestKick(3);
        else if(req.kick_request==2)
            controller.requestKick(2);
        res.ShootIsDone = 1;
        return true;
    };

    FukuroVelCmdCallback vel_cmd_callback = [&controller](const fukuro_common::VelCmd::ConstPtr& vel)
    {
        if(!isManualPositioning)
            controller.setTargetRobotVel(vel->Vx,vel->Vy,vel->w);
    };

    FukuroVelCmdManualCallback vel_cmd_manual_callback = [&controller](const fukuro_common::HWControllerManual::ConstPtr& vel)
    {
        if(isManualPositioning)
            controller.setTargetRobotVel(vel->Vx,vel->Vy,vel->w);
    };

    DribblerCmdCallback dribbler_cmd_callback = [&controller](const fukuro_common::DribblerControl::ConstPtr& dribbler)
    {
        if(dribbler->speed>0)
            controller.setDribbler(true,dribbler->dir_in?true:false,dribbler->speed);
        else
            controller.setDribbler(false);
    };

    ros::ServiceClient keypad_service_client = node.serviceClient<fukuro_common::KeypadService>(robot_name+std::string("/keypad_service"));
    ros::ServiceServer kick_service_server = node.advertiseService(robot_name+std::string("/kick_service"),kick_service_callback);
    ros::ServiceServer odometry_service_server = node.advertiseService(robot_name+std::string("/odometry_service"),odom_service_callback);
    ros::Subscriber vel_cmd_sub = node.subscribe(robot_name+std::string("/vel_cmd"),10,vel_cmd_callback);
    ros::Subscriber vel_cmd_manual_sub = node.subscribe(robot_name+std::string("/vel_cmd_manual"),10,vel_cmd_manual_callback);
    ros::Subscriber dribbler_cmd_sub = node.subscribe(robot_name+std::string("/dribbler"),10,dribbler_cmd_callback);
    ros::Subscriber worldmodel = node.subscribe(robot_name+std::string("/world_model"),50,wm_callback);
    ros::AsyncSpinner spinner(2);

    //settings
    w.setSaveCallback([&settings,&controller]
                      (std::vector<double>cw,
                      std::vector<double> cw_step,
                      std::vector<double> ccw,
                      std::vector<double> ccw_step,
                      std::vector<double> kp,
                      std::vector<double> kd)
    {
        if(cw_step.size()==4)
        {
            settings.setValue(QString("motor1_cw_step"),cw_step[0]);
            settings.setValue(QString("motor2_cw_step"),cw_step[1]);
            settings.setValue(QString("motor3_cw_step"),cw_step[2]);
            settings.setValue(QString("motor4_cw_step"),cw_step[3]);
        }
        if(ccw_step.size()==4)
        {
            settings.setValue(QString("motor1_ccw_step"),ccw_step[0]);
            settings.setValue(QString("motor2_ccw_step"),ccw_step[1]);
            settings.setValue(QString("motor3_ccw_step"),ccw_step[2]);
            settings.setValue(QString("motor4_ccw_step"),ccw_step[3]);
        }
        for(int i=0; i<6; i++)
        {
            auto speed = static_cast<fukuro::HWController::speed_t>(i);
            auto params = controller.motorParam(speed);
            auto str_ = QString("cw");
            using namespace fukuro;
            switch(speed)
            {
            case HWController::Speed025:
                str_ = QString("cw_025");
                break;
            case HWController::Speed050:
                str_ = QString("cw_050");
                break;
            case HWController::Speed075:
                str_ = QString("cw_075");
                break;
            case HWController::Speed125:
                str_ = QString("cw_125");
                break;
            case HWController::Speed150:
                str_ = QString("cw_150");
                break;
            }
            for(int i=0; i<8; i++)
            {
                QString str;
                if(i<4)
                    str = QString("motor%1_").arg(i+1)+str_;
                else
                    str = QString("motor%1_c").arg(i-3)+str_;
                settings.setValue(str,params.at(i));
            }
        }
#if 0
        if(cw.size()==4)
        {
            settings.setValue(QString("motor1_cw"),cw[0]);
            settings.setValue(QString("motor2_cw"),cw[1]);
            settings.setValue(QString("motor3_cw"),cw[2]);
            settings.setValue(QString("motor4_cw"),cw[3]);
        }
        if(ccw.size()==4)
        {
            settings.setValue(QString("motor1_ccw"),ccw[0]);
            settings.setValue(QString("motor2_ccw"),ccw[1]);
            settings.setValue(QString("motor3_ccw"),ccw[2]);
            settings.setValue(QString("motor4_ccw"),ccw[3]);
        }
#endif
        if(kp.size()==4)
        {
            settings.setValue(QString("kp_motor1"),kp[0]);
            settings.setValue(QString("kp_motor2"),kp[1]);
            settings.setValue(QString("kp_motor3"),kp[2]);
            settings.setValue(QString("kp_motor4"),kp[3]);
        }
        if(kd.size()==4)
        {
            settings.setValue(QString("kd_motor1"),kd[0]);
            settings.setValue(QString("kd_motor2"),kd[1]);
            settings.setValue(QString("kd_motor3"),kd[2]);
            settings.setValue(QString("kd_motor4"),kd[3]);
        }
    });

#if 0
    if(settings.contains(QString("motor1_cw")) && settings.contains(QString("motor1_cw_step")))
    {
        controller.setMotorParameter(1,settings.value(QString("motor1_cw")).toDouble(),settings.value(QString("motor1_cw_step")).toDouble(),true);
        cw.push_back(settings.value(QString("motor1_cw")).toDouble());
        cw_step.push_back(settings.value(QString("motor1_cw_step")).toDouble());
    }
    if(settings.contains(QString("motor2_cw")) && settings.contains(QString("motor2_cw_step")))
    {
        controller.setMotorParameter(2,settings.value(QString("motor2_cw")).toDouble(),settings.value(QString("motor2_cw_step")).toDouble(),true);
        cw.push_back(settings.value(QString("motor2_cw")).toDouble());
        cw_step.push_back(settings.value(QString("motor2_cw_step")).toDouble());
    }
    if(settings.contains(QString("motor3_cw")) && settings.contains(QString("motor3_cw_step")))
    {
        controller.setMotorParameter(3,settings.value(QString("motor3_cw")).toDouble(),settings.value(QString("motor3_cw_step")).toDouble(),true);
        cw.push_back(settings.value(QString("motor3_cw")).toDouble());
        cw_step.push_back(settings.value(QString("motor3_cw_step")).toDouble());
    }
    if(settings.contains(QString("motor4_cw")) && settings.contains(QString("motor4_cw_step")))
    {
        controller.setMotorParameter(4,settings.value(QString("motor4_cw")).toDouble(),settings.value(QString("motor4_cw_step")).toDouble(),true);
        cw.push_back(settings.value(QString("motor4_cw")).toDouble());
        cw_step.push_back(settings.value(QString("motor4_cw_step")).toDouble());
    }
    if(settings.contains(QString("motor1_ccw")) && settings.contains(QString("motor1_ccw_step")))
    {
        controller.setMotorParameter(1,settings.value(QString("motor1_ccw")).toDouble(),settings.value(QString("motor1_ccw_step")).toDouble(),false);
        ccw.push_back(settings.value(QString("motor1_ccw")).toDouble());
        ccw_step.push_back(settings.value(QString("motor1_ccw_step")).toDouble());
    }
    if(settings.contains(QString("motor2_ccw")) && settings.contains(QString("motor2_ccw_step")))
    {
        controller.setMotorParameter(2,settings.value(QString("motor2_ccw")).toDouble(),settings.value(QString("motor2_ccw_step")).toDouble(),false);
        ccw.push_back(settings.value(QString("motor2_ccw")).toDouble());
        ccw_step.push_back(settings.value(QString("motor2_ccw_step")).toDouble());
    }
    if(settings.contains(QString("motor3_ccw")) && settings.contains(QString("motor3_ccw_step")))
    {
        controller.setMotorParameter(3,settings.value(QString("motor3_ccw")).toDouble(),settings.value(QString("motor3_ccw_step")).toDouble(),false);
        ccw.push_back(settings.value(QString("motor3_ccw")).toDouble());
        ccw_step.push_back(settings.value(QString("motor3_ccw_step")).toDouble());
    }
    if(settings.contains(QString("motor4_ccw")) && settings.contains(QString("motor4_ccw_step")))
    {
        controller.setMotorParameter(4,settings.value(QString("motor4_ccw")).toDouble(),settings.value(QString("motor4_ccw_step")).toDouble(),false);
        ccw.push_back(settings.value(QString("motor4_ccw")).toDouble());
        ccw_step.push_back(settings.value(QString("motor4_ccw_step")).toDouble());
    }
#endif

    if(settings.contains(QString("kp_motor1")) && settings.contains(QString("kd_motor1")) &&
            settings.contains(QString("kp_motor2")) && settings.contains(QString("kd_motor2")) &&
            settings.contains(QString("kp_motor3")) && settings.contains(QString("kd_motor3")) &&
            settings.contains(QString("kp_motor4")) && settings.contains(QString("kd_motor4")))
    {
        kp.push_back(settings.value(QString("kp_motor1")).toDouble());
        kd.push_back(settings.value(QString("kd_motor1")).toDouble());
        kp.push_back(settings.value(QString("kp_motor2")).toDouble());
        kd.push_back(settings.value(QString("kd_motor2")).toDouble());
        kp.push_back(settings.value(QString("kp_motor3")).toDouble());
        kd.push_back(settings.value(QString("kd_motor3")).toDouble());
        kp.push_back(settings.value(QString("kp_motor4")).toDouble());
        kd.push_back(settings.value(QString("kd_motor4")).toDouble());
        controller.setPID(kp,kd);
        w.setPID(kp[0],kd[0],kp[1],kd[1],kp[2],kd[2],kp[3],kd[3]);
    }

    //controller callback
    w.setPIDCallback([&controller](double kpm1, double kdm1, double kpm2, double kdm2, double kpm3, double kdm3, double kpm4, double kdm4)
    {
        controller.setPID({kpm1,kpm2,kpm3,kpm4},
        {kdm1,kdm2,kdm3,kdm4});
    });

    w.setParam(cw,cw_step,ccw,ccw_step);

    w.setPWMCallback([&controller](int ff1, int ff2, int ff3, int ff4, int ff5, int kick)
    {
        controller.setFF(ff1,ff2,ff3,ff4,ff5,kick);
    });

    w.setUpdateParamCallback([&controller](int i, double param, double param2, bool cw, int speed)
    {
        controller.setMotorParameter(i,param,param2,cw,static_cast<fukuro::HWController::speed_t>(speed));
    });

    w.setRobotVelCallback([&controller](double vx, double vy, double w)
    {
        controller.setTargetRobotVel(vx,vy,w);
    });

    w.setDribblerCallback([&controller](bool d, bool o)
    {
        controller.setDribbler(d,o);
    });

    w.setConnectCallback([&controller](std::string port)
    {
        return controller.connect(port);
    });

    w.setConnectArduinoCallback([&controller](std::string port)
    {
        return controller.connectArduino(port);
    });

    w.setDisconectCallback([&controller]()
    {
        return controller.disconnect();
    });

    w.setDisconectArduinoCallback([&controller]()
    {
        return controller.disconnectArduino();
    });

    w.setKickCallback([&controller](int codename)
    {
        controller.requestKick(codename);
    });
    w.setResetSTMCallback([&controller]()
    {
        controller.resetSTM();
    });

    w.setOffsetCmpsCallback([&controller]()
    {
        controller.setOffsetCmps();
    });

    //controller callback
    controller.setPWMUpdateCallback([&wrapper, &pwm_pub](int f1, int f2, int f3, int f4, int f5, int kick)
    {
        wrapper.updatePWM(f1,f2,f3,f4,f5,kick);
        fukuro_common::PWM msg;
        msg.motor.push_back((int32_t)f1);
        msg.motor.push_back((int32_t)f2);
        msg.motor.push_back((int32_t)f3);
        msg.motor.push_back((int32_t)f4);
        msg.motor.push_back((int32_t)f5);
        pwm_pub.publish(msg);
#if 1
        std::cout << "[Main] pwm updated : "
                  << f1 << ", "
                  << f2 << ", "
                  << f3 << ", "
                  << f4 << ", "
                  << f5 << "\n";
#endif
    });

    controller.setCallback([&wrapper](double ff1, double ff2, double ff3, double ff4, double ff5, bool ir, double dt)
    {
        wrapper.updateValue(ff1,ff2,ff3,ff4,ff5,ir,dt);
    });

    controller.setRawDataCallback([&wrapper](int ff1, int ff2, int ff3, int ff4, int ff5, bool ir, double dt)
    {
        wrapper.updateRawValue(ff1,ff2,ff3,ff4,ff5,ir,dt);
    });

    controller.setRobotVelCallback([&wrapper](double vx, double vy, double w)
    {
        wrapper.updateVelocity(vx,vy,w);
    });

    controller.setOdometryCallback([&wrapper,&odometry_pub,&odom,tuning](double px, double py, double theta, double vx, double vy, double w)
    {
        if(tuning)
            wrapper.updateOdometry(px,py,theta,vx,vy,w);
        odom.x = px;
        odom.y = py;
        odom.theta = theta;
        odom.Vx = vx;
        odom.Vy = vy;
        odom.w = w;
#ifdef DEBUG_ODOMETRY
        std::cout << "[controller] publish odometry\n";
#endif
        odometry_pub.publish(odom);
    });

    controller.setControlCallback([&wrapper](std::vector<int> ve_setpoint, std::vector<int> ve)
    {
        if((ve_setpoint.size()==3) && (ve.size()==3))
            wrapper.updateControl(ve_setpoint[0],ve_setpoint[1],ve_setpoint[2],ve[0],ve[1],ve[2]);
    });

    controller.setArduinoCallback([&keypad_service_client](int data)
    {
        std::cout << "[ArduinoCallback] call keypad service client : data -> " << data << " ";
        fukuro_common::KeypadService key;
        key.request.key = data;
        if(keypad_service_client.call(key))
            std::cout << "OK, response -> " << key.response << '\n';
        else
            std::cout << "failed, response -> " << key.response << '\n';
    });

    controller.setArduinoCMPSCallback([&compass_pub,&w](double cmps)
    {
        fukuro_common::Compass msg;
        msg.cmps = cmps;
        compass_pub.publish(msg);
        w.setCompass(cmps);
    });

    int ret = 0;
    std::cout << "[Main] start controller\n";
    controller.start();
    std::cout << "[Main] start spinner\n";
    spinner.start();

    std::cout << "[Main] show main window\n";
    w.showMaximized();

    ret = a.exec();

    return ret;
#endif
}
