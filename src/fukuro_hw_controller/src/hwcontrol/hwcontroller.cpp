#include "hwcontroller.h"
#include "fukuro_common/DribblerControl.h"
#include "fukuro_common/VelCmd.h"
#include "fukuro_common/Encoder.h"
#include "fukuro_common/PWM.h"
#include <iostream>
#include <cmath>
#include <QDebug>
#include <QSettings>
#include <QSerialPortInfo>

/*Robot A                              Robot B (keeper)
      ________                         _________
   1 /        \ 2                   1 / //   \\ \ 2
    / //    \\ \                     /           \    theta=30
    \          /                     \           /
     \   __   /                     4 \ \\   // / 3
      \  --  /                         ---------
       ------     4:Drib                      5:Drib
          3


*/

#define BASE_TYPE_A
#define MOTOR1_INDEX        0
#define MOTOR2_INDEX        1
#define MOTOR3_INDEX        2
#define MOTOR4_INDEX        3
#define DRIB_INDEX          4  // A=3 B=4
#define DRIB_INDEX_A        3  // A=3 B=4
#define DRIB_INDEX_B        4  // A=3 B=4
#define MOTOR1_RATIO_CW     100
#define MOTOR2_RATIO_CW     100
#define MOTOR3_RATIO_CW     100
#define MOTOR4_RATIO_CW     100
#define MOTOR1_RATIO_CCW    100
#define MOTOR2_RATIO_CCW    100
#define MOTOR3_RATIO_CCW    100
#define MOTOR4_RATIO_CCW    100
#define MOTOR1_STEP_CW      11
#define MOTOR2_STEP_CW      11
#define MOTOR3_STEP_CW      11
#define MOTOR4_STEP_CW      11
#define MOTOR1_STEP_CCW     11
#define MOTOR2_STEP_CCW     11
#define MOTOR3_STEP_CCW     11
#define MOTOR4_STEP_CCW     11
#define ERROR_RANGE         30
#define ENCODER_RATIO       0.00781818
#define WHEEL_TO_VE_RATIO   127.388535
#define DEFAULT_PPR         1024
//#define DEBUG_SERIAL_ARDUINO
#define DEBUG_CMPS
//#define YAYABDR_THEORM

namespace fukuro {

constexpr
std::array<HWController::rotation_speed_t, N_ROTATION_SPEED> HWController::rotation_speeds;
constexpr
std::array<HWController::speed_t, N_LINEAR_SPEED> HWController::linear_speeds;

//@TODO : remove
HWController::HWController(double loop_rate, bool ctrl) :
  //    running(false),
  //    rx_state(start_U),
  //    rx_state_arduino(Arduino_start_U),
  //    data_cb(NULL),
  //    arduino_cb(NULL),
  //    arduino_cmps_cb(NULL),
  //    raw_data_cb(NULL),
  //    robot_vel_cb(NULL),
  //    control_cb(NULL),
  //    odometry_cb(NULL),
  //    pwm_update_cb(NULL),
  //    serial_connected(false),
  //    writing_serial(false),
  //    data_updated(false),
  //    kick_request(false),
  enable_control(ctrl),
  //    robot_vx(0.0),
  //    robot_vy(0.0),
  //    robot_w(0.0),
  //    target_robot_vx(0.0),
  //    target_robot_vy(0.0),
  //    target_robot_w(0.0),
  dt(0.0),
  offsetCmps(0.0),
  goal_dist_state(true),
  codename(0)
{
  wait_ms = Duration(1000.0/loop_rate);
  init();
}

HWController::HWController(ros::NodeHandle &node) :
  enable_control(false),
  dt(0.0),
  offsetCmps(0.0),
  goal_dist_state(true),
  codename(0)
{
  ros::get_environment_variable(robot_name, "FUKURO");
  ros::get_environment_variable(base_type, "BASE_TYPE");

  if(base_type.empty())
  {
    ROS_ERROR("Base Type is not supported! export BASE_TYPE=BASE_TYPE_A (or B)!!!");
    exit(-1);
  }
  else if(base_type == std::string("BASE_TYPE_A"))
    this->base = BaseTypeA;
  else if(base_type == std::string("BASE_TYPE_B"))
    this->base = BaseTypeB;
  else
  {
    ROS_ERROR("Base Type is not supported! export BASE_TYPE=BASE_TYPE_A (or B)!!!");
    exit(-1);
  }

  auto topic = robot_name + "/hwcontrol_info";
  auto encoder_topic = robot_name + "/encoder";
  auto compass_topic = robot_name + "/compass";
  hwctrl_pub = node.advertise<fukuro_common::HWController>(topic, 3);
  encoder_pub = node.advertise<fukuro_common::Encoder>(encoder_topic, 3);
  compass_pub = node.advertise<fukuro_common::Compass>(compass_topic, 3);
  init();
}

HWController::~HWController()
{
  {
    running = false;
    if(serial_connected)
      serial->close();
    if(serial_arduino_connected)
      serial_arduino->close();
    if(thread.joinable())
      thread.join();
  }
}

void HWController::setDribbler(bool d, bool in, int speed)
{
  auto idx = (this->base == BaseTypeA ? DRIB_INDEX_A : DRIB_INDEX_B);
  if(d)
    pwm[idx] = in ? speed : -speed;
  else
    pwm[idx] = 0;
}

void HWController::getGoalDistance(double theta, double x)
{
#ifdef YAYABDR_THEORM
  double theta_loc =theta;
  double x_loc =4.5-x;
  double x_loc_sq=x_loc*x_loc;
  double tan_theta_sq=std::pow(tan(theta_loc),2);
  goal_dist= sqrt(x_loc_sq+(tan_theta_sq/x_loc_sq));
  std::cout<<"[GOAL DISTANCE] "<<goal_dist<<std::endl;

  if(goal_dist<5.5 && goal_dist>0)
  {
    kick_del=-2.641695355*goal_dist*goal_dist*goal_dist
        +28.91649552*goal_dist*goal_dist
        -90.62461431*goal_dist
        +109.0564564;
    std::cout<<"[KICK DEL] "<<kick_del<<std::endl;
  }

    double theta_loc =theta;
    double x_loc =6-x;
    double x_loc_sq=x_loc*x_loc;
    double tan_theta_sq=std::pow(tan(theta_loc),2);
    goal_dist= sqrt(x_loc_sq+(tan_theta_sq/x_loc_sq));
    std::cout<<"[GOAL DISTANCE] "<<goal_dist<<std::endl;

    if(goal_dist>4.7) kick_del = 55;
    else if(goal_dist>4) kick_del = 38;
    else if(goal_dist>3) kick_del = 33;
    else if(goal_dist>2) kick_del = 35;
    else kick_del = 45;
#endif
kick_del=33;

}

void HWController::setFF(int ff1, int ff2, int ff3, int ff4, int ff5,int kick)
{
  (ff1|ff2|ff3)? pwm_test = true : pwm_test = false;

  mutex.lock();
  pwm[0] = ff1;
  pwm[1] = ff2;
  pwm[2] = ff3;
  pwm[3] = ff4;
  pwm[4] = ff5;
  (pwm_test)? goal_dist_state = false : goal_dist_state = true;

  if(!goal_dist_state)
    kick_del = kick;
  mutex.unlock();
}

bool HWController::connect(std::string port, int baud_rate)
{
  ROS_WARN("connect to %s, baud rate : %d", port.c_str(), baud_rate);
  mutex.lock();
  serial->setPortName(QString::fromStdString(port));
  serial->setBaudRate((qint32)baud_rate);
  serial->setDataBits(QSerialPort::Data8);
  serial->setStopBits(QSerialPort::OneStop);
  serial->setParity(QSerialPort::NoParity);
  serial_connected = serial->open(QIODevice::ReadWrite);
  if(!serial_connected)
    ROS_ERROR("%s", serial->errorString().toStdString().c_str());
  mutex.unlock();
  return serial_connected;
}

bool HWController::connectArduino(std::string port, int baud_rate)
{
  ROS_WARN("connect to Arduino : %s, baud_rate : %d", port.c_str(), baud_rate);
  mutex.lock();
  serial_arduino->setPortName(QString::fromStdString(port));
  serial_arduino->setBaudRate((qint32)baud_rate);
  serial_arduino->setDataBits(QSerialPort::Data8);
  serial_arduino->setParity(QSerialPort::NoParity);
  serial_arduino_connected = serial_arduino->open(QIODevice::ReadWrite);
  if(!serial_arduino_connected)
    ROS_ERROR("%s", serial_arduino->errorString().toStdString().c_str());
  mutex.unlock();
  return serial_arduino_connected;
}

bool HWController::disconnect()
{
  ROS_WARN("disconnect Nucleo");
  mutex.lock();
  serial->close();
  mutex.unlock();
  serial_connected = !serial->isOpen();
  return serial_connected;
}

bool HWController::disconnectArduino()
{
  ROS_WARN("disconnect Arduino");
  mutex.lock();
  serial_arduino->close();
  mutex.unlock();
  serial_arduino_connected = !serial_arduino->isOpen();
  return serial_arduino_connected;
}

void HWController::setTargetRobotVel(double vx, double vy, double w)
{
  target_robot_vx = vx;
  target_robot_vy = vy;
  target_robot_w = w;
  updateMotorVelocity();
}

void HWController::resetSTM()
{
  ROS_WARN("reset STM");
  QByteArray data;
  data.push_back('r');
  if(serial_arduino->isOpen())
    serial_arduino->write(data);
}

std::vector<double> HWController::motorParam(HWController::speed_t speed)
{
  std::vector<double> ret;
  double *cw, *ccw;
  switch (speed) {
  case Speed025:
    cw = motor_ratio_cw_025;
    ccw = motor_ratio_ccw_025;
    break;
  case Speed050:
    cw = motor_ratio_cw_050;
    ccw = motor_ratio_ccw_050;
    break;
  case Speed075:
    cw = motor_ratio_cw_075;
    ccw = motor_ratio_ccw_075;
    break;
  case Speed100:
    cw = motor_ratio_cw_100;
    ccw = motor_ratio_ccw_100;
    break;
  case Speed125:
    cw = motor_ratio_cw_125;
    ccw = motor_ratio_ccw_125;
    break;
  case Speed150:
    cw = motor_ratio_cw_150;
    ccw = motor_ratio_ccw_150;
    break;
  default:
    break;
  }
  for(int i=0; i<4; i++)
    ret.push_back(cw[i]);
  for(int i=0; i<4; i++)
    ret.push_back(ccw[i]);
  return ret;
}

void HWController::loop()
{
  ROS_WARN("init loop");
  ros::Rate rate(60.0);
  while(ros::ok())
  {
    motorControl();
    if(serial->isOpen())
        sendData();
    rate.sleep();
  }
  ROS_WARN("loop exiting");
}

void HWController::updateKickDel(const fukuro_common::WorldModelConstPtr &wm)
{
  getGoalDistance(wm->pose.theta, wm->pose.x);
}

void HWController::updateSerialInfo()
{
  portlist.clear();
  manufacturer.clear();
  for(const auto& s : QSerialPortInfo::availablePorts())
  {
    auto port = s.portName().toStdString();
    auto factory = s.manufacturer().toStdString();
    portlist.push_back(std::string("/dev/")+port);
    manufacturer.push_back(factory);
  }
}

//void HWController::loop()
//{
//#ifdef DEBUG_HWCONTROLLER
//  std::cout << "[HWController] loop\n";
//#endif
//  update_data_time_point = Clock::now();
//  auto t_init = Clock::now();
//  Duration loop_wait = wait_ms;
//  while(running)
//  {
//    auto t0 = Clock::now();
//    if(serial_connected)
//    {
//      Duration update_dt = t0-update_data_time_point;
//      if(update_dt>Duration(500.0))
//        resetSTM();

//      if(!writing_serial)
//        emit sendSerialData();
//      if(data_updated)
//      {
//        auto t_now = Clock::now();
//        Duration dt_update_data = t_now - update_data_time_point;
//        dt = dt_update_data.count();

//        updateRobotVel();
//        updateOdometry();

//#if 0
//        std::cout <<"VE set"<< ve_setpoint[0]<<" , "<<ve_setpoint[1]<<" , "<<ve_setpoint[2]<<"\n"
//                 << "VE raw"<<ve[0]<<" , "<<ve[1]<<" , "<<ve[2]<<"\n";
//#endif

//        if(data_cb)
//          data_cb(ve_rad_s[0],ve_rad_s[1],ve_rad_s[2],ve_rad_s[3],ve_rad_s[4],ir,dt_update_data.count());
//        if(raw_data_cb)
//          raw_data_cb(ve[0],ve[1],ve[2],ve[3],ve[4],ir,dt_update_data.count());
//        if(robot_vel_cb)
//          robot_vel_cb(robot_vx,robot_vy,robot_w);
//        update_data_time_point = t_now;
//        data_updated = false;
//      }
//    }
//    motorControl();
//    auto t1 = Clock::now();
//    Duration dt = t1-t0;
//    if(dt<loop_wait)
//      std::this_thread::sleep_for(loop_wait-dt);
//  }
//#ifdef DEBUG_HWCONTROLLER
//  std::cout << "[HWController] exit loop\n";
//#endif
//}

void HWController::sendData()
{
  writing_serial = true;
  uchar data[12];
  uchar direction_data = 0x00;

  mutex.lock();
  if(pwm[MOTOR1_INDEX]<0) direction_data |= 0x01;
  if(pwm[MOTOR2_INDEX]<0) direction_data |= 0x02;
  if(pwm[MOTOR3_INDEX]<0) direction_data |= 0x04;
  if(pwm[3]<0) direction_data |= 0x08;
  if(pwm[4]<0) direction_data |= 0x10;

  data[0] = 'U';
  data[1] = 'G';
  data[2] = 'M';
  data[3] = (uchar)(abs(pwm[MOTOR1_INDEX])&0xFF); //m1
  data[4] = (uchar)(abs(pwm[MOTOR2_INDEX])&0xFF); //m2
  data[5] = (uchar)(abs(pwm[MOTOR3_INDEX])&0xFF); //m3
  data[6] = (uchar)(abs(pwm[3])&0xFF); //m4
  data[7] = (uchar)(abs(pwm[4])&0xFF); //drib

  mutex.unlock();
  data[8] = direction_data;

  if(kick_request)
  {
    if(codename==2)
      kick_del = 23;
    data[9] = kick_del;
    kick_request = false;
  }
  else data[9] = 0;

  if(extensi_request)
  {
    data[10] = 1;
    extensi_request=false;
  }
  else data[10] = 0;

  int sum = 0;

  std::stringstream ss;
  for(size_t i=3; i<11; ++i) {
    sum += (uint8_t)data[i];
    ss << (int)data[i] << ", ";
  }

  data[11]=sum&0xFF;

  if(serial->write((char*)data,12)!=12)
    ROS_ERROR("write serial error %s", ss.str().c_str());
  else
    ROS_INFO("SENT DATA %s", ss.str().c_str());
  serial->waitForBytesWritten(1);
  writing_serial = false;
}

void HWController::start()
{
  ROS_WARN("starting thread");
  running = true;
  thread = std::thread(&HWController::loop,this);
  ROS_WARN("thread launched");
}

void HWController::publish()
{
  ROS_INFO("publishing message");
  fukuro_common::HWController msg;
  fukuro_common::Encoder encoder_msg;
  geometry_msgs::Pose2D vel;
  this->getRobotVel(&vel.x, &vel.y, &vel.theta);
  this->encoder(ve);
  for(size_t i=0; i<5; i++)
    msg.pwm.motor.push_back(pwm[i]);
  for(size_t i=0; i<encoder.size; i++)
  {
    //    msg.encoder.current_tick.push_back(encoder.current_tick[i]);
    //    msg.encoder.total_tick.push_back(encoder.total_tick[i]);
    encoder_msg.current_tick.push_back(encoder.current_tick[i]);
    encoder_msg.total_tick.push_back(encoder.total_tick[i]);
  }
  //  msg.compass.cmps = this->cmps;
  msg.base_type = this->base_type;
  msg.vel = vel;
  hwctrl_pub.publish(msg);
  encoder_pub.publish(encoder_msg);
  ROS_INFO("done");
}

void HWController::publishCompass()
{
  ROS_INFO("publishing compass");
  fukuro_common::Compass compass_msg;
  compass_msg.cmps = this->cmps+ this->offsetCmps;
  compass_pub.publish(compass_msg);
  ROS_INFO("publishing compass done");
}

void HWController::getRobotVel(double *vx, double *vy, double *w)
{
  if(!vx || !vy || !w)
    return;

  double encoder_ratio = ENCODER_RATIO;
  double vm_1 = ve[MOTOR1_INDEX]*encoder_ratio;
  double vm_2 = ve[MOTOR2_INDEX]*encoder_ratio;
  double vm_3 = ve[MOTOR3_INDEX]*encoder_ratio;
  double vm_4 = ve[MOTOR4_INDEX]*encoder_ratio;

  switch (this->base) {
  case BaseTypeA :
    *vx =-0.5773472*  vm_1 + 0.5773472*vm_2  + 0*        vm_3;
    *vy = 0.3333333*  vm_1 + 0.3333333*vm_2  - 0.6666667*vm_3;
    *w  = 1.6835017*  vm_1 + 1.6835017*vm_2  + 1.6835017*vm_3;
    break;
  case BaseTypeB :
    *vx = -0.124999 *vm_1 + 0.124999 *vm_2 + 0.124999 *vm_3 - 0.124999 *vm_4;
    *vy =  0.2165058*vm_1 - 0.2165058*vm_2 - 0.2165058*vm_3 + 0.2165058*vm_4;
    *w  =  1.2626263*vm_1 + 1.2626263*vm_2 + 1.2626263*vm_3 + 1.2626263*vm_4;
    break;
  default:
    break;
  }
}

void HWController::loadSettings(const std::string &filename)
{
  ROS_WARN("loading..");
  QSettings settings(QString::fromStdString(filename), QSettings::IniFormat);
  settings.beginGroup(robot_name.c_str());

  for(const QString& str : motor_param_list)
  {
    if(settings.contains(str))
    {
      auto val = settings.value(str).toDouble();
      auto idx = 1;
      auto cw = str.contains("_cw");
      if(str.contains("2_"))
        idx = 2;
      else if(str.contains("3_"))
        idx = 3;
      else if(str.contains("4_"))
        idx = 4;
      fukuro::HWController::speed_t speed;
      if(str.contains(QString("025")))
        speed = fukuro::HWController::Speed025;
      else if(str.contains(QString("050")))
        speed = fukuro::HWController::Speed050;
      else if(str.contains(QString("075")))
        speed = fukuro::HWController::Speed075;
      else if(str.contains(QString("125")))
        speed = fukuro::HWController::Speed125;
      else if(str.contains(QString("150")))
        speed = fukuro::HWController::Speed150;
      else
        speed = fukuro::HWController::Speed100;
      setMotorParameter(idx,val,15,cw,speed);
    }
  }
  settings.endGroup();

  settings.beginGroup("encoder");
  this->encoder_ppr = settings.value("ppr",QVariant(DEFAULT_PPR)).toDouble();
  settings.endGroup();
  ROS_WARN("done");
}

void HWController::saveSettings(const std::string &filename)
{
  ROS_WARN("saving..");
  std::cout << "saving.." << std::endl;
  QSettings settings(QString::fromStdString(filename), QSettings::IniFormat);
  settings.beginGroup(robot_name.c_str());
  for(int i=0; i<6; i++)
  {
    auto speed = static_cast<fukuro::HWController::speed_t>(i);
    auto params = motorParam(speed);
    auto str_ = QString("cw");
    using namespace fukuro;
    switch(speed)
    {
    case HWController::Speed025:
      str_ = QString("cw_025");
      break;
    case HWController::Speed050:
      str_ = QString("cw_050");
      break;
    case HWController::Speed075:
      str_ = QString("cw_075");
      break;
    case HWController::Speed125:
      str_ = QString("cw_125");
      break;
    case HWController::Speed150:
      str_ = QString("cw_150");
      break;
    }
    for(int i=0; i<8; i++)
    {
      QString str;
      if(i<4)
        str = QString("motor%1_").arg(i+1)+str_;
      else
        str = QString("motor%1_c").arg(i-3)+str_;
      settings.setValue(str,params.at(i));
    }
  }
  settings.endGroup();

  settings.beginGroup("encoder");
  settings.setValue("ppr", this->encoder_ppr);
  settings.endGroup();

  ROS_WARN("done");
  std::cout << "done.." << std::endl;
}

void HWController::updateHWControlCmd(const fukuro_common::HWControllerCommandConstPtr &msg)
{
  if(!is_manual_positioning) {
    ROS_INFO("updating cmd");
    auto vx = msg->vel.Vx;
    auto vy = msg->vel.Vy;
    auto w = msg->vel.w;
    auto dribble = msg->dribbler.speed;
    auto dribble_dir = msg->dribbler.dir_in;
    this->setTargetRobotVel(vx, vy, w);
    this->setDribbler(true, dribble_dir, dribble);
  }
}

void HWController::updateHWControlManual(const fukuro_common::HWControllerManualConstPtr &msg)
{
  if(is_manual_positioning) {
    auto vx = msg->Vx;
    auto vy = msg->Vy;
    auto w = msg->w;
    this->setTargetRobotVel(vx, vy, w);
  }
}

void HWController::updateTeammates(const fukuro_common::TeammatesConstPtr &msg)
{
  this->is_manual_positioning = msg->isManualPositioning;
}

bool HWController::hwctrlService(fukuro_common::HWControllerServiceRequest &req, fukuro_common::HWControllerServiceResponse &res)
{
  ROS_WARN("execute service");
  if(req.refresh) {
    this->updateSerialInfo();
    for(const auto& p : portlist)
      res.port_list.push_back(p);
    for(const auto& m : manufacturer)
      res.manufacturer_list.push_back(m);
  }
  if(portlist.size()) {
    if(req.isArduino)
    {
      auto port = this->portlist.at(req.ArduinoConnect);
      this->connectArduino(port);
      res.ArduinoSuccess = this->serial_arduino->isOpen();
    }
    if(req.isSTM)
    {
      auto port = this->portlist.at(req.STMConnect);
      this->connect(port);
      res.STMSuccess = this->serial->isOpen();
    }
    if(req.Compass)
    {
      this->setOffsetCmps();
      res.Compassuccess=true;
    }
  }
  ROS_WARN("execute service done!");
  return true;
}

bool HWController::controlParamService(fukuro_common::HWControllerParamServiceRequest &req, fukuro_common::HWControllerParamServiceResponse &res)
{
//  this->setMotorParameter(req.i, req.ratio, 10, req.cw, static_cast<speed_t>(req.speed));

  {
    motor_ratio_cw_025[0] = req.cw.motor1.at(0);
    motor_ratio_cw_025[1] = req.cw.motor2.at(0);
    motor_ratio_cw_025[2] = req.cw.motor3.at(0);
    motor_ratio_cw_025[3] = req.cw.motor4.at(0);

    motor_ratio_cw_050[0] = req.cw.motor1.at(1);
    motor_ratio_cw_050[1] = req.cw.motor2.at(1);
    motor_ratio_cw_050[2] = req.cw.motor3.at(1);
    motor_ratio_cw_050[3] = req.cw.motor4.at(1);

    motor_ratio_cw_075[0] = req.cw.motor1.at(2);
    motor_ratio_cw_075[1] = req.cw.motor2.at(2);
    motor_ratio_cw_075[2] = req.cw.motor3.at(2);
    motor_ratio_cw_075[3] = req.cw.motor4.at(2);

    motor_ratio_cw_100[0] = req.cw.motor1.at(3);
    motor_ratio_cw_100[1] = req.cw.motor2.at(3);
    motor_ratio_cw_100[2] = req.cw.motor3.at(3);
    motor_ratio_cw_100[3] = req.cw.motor4.at(3);

    motor_ratio_cw_125[0] = req.cw.motor1.at(4);
    motor_ratio_cw_125[1] = req.cw.motor2.at(4);
    motor_ratio_cw_125[2] = req.cw.motor3.at(4);
    motor_ratio_cw_125[3] = req.cw.motor4.at(4);

    motor_ratio_cw_150[0] = req.cw.motor1.at(5);
    motor_ratio_cw_150[1] = req.cw.motor2.at(5);
    motor_ratio_cw_150[2] = req.cw.motor3.at(5);
    motor_ratio_cw_150[3] = req.cw.motor4.at(5);

    motor_ratio_ccw_025[0] = req.ccw.motor1.at(0);
    motor_ratio_ccw_025[1] = req.ccw.motor2.at(0);
    motor_ratio_ccw_025[2] = req.ccw.motor3.at(0);
    motor_ratio_ccw_025[3] = req.ccw.motor4.at(0);

    motor_ratio_ccw_050[0] = req.ccw.motor1.at(1);
    motor_ratio_ccw_050[1] = req.ccw.motor2.at(1);
    motor_ratio_ccw_050[2] = req.ccw.motor3.at(1);
    motor_ratio_ccw_050[3] = req.ccw.motor4.at(1);

    motor_ratio_ccw_075[0] = req.ccw.motor1.at(2);
    motor_ratio_ccw_075[1] = req.ccw.motor2.at(2);
    motor_ratio_ccw_075[2] = req.ccw.motor3.at(2);
    motor_ratio_ccw_075[3] = req.ccw.motor4.at(2);

    motor_ratio_ccw_100[0] = req.ccw.motor1.at(3);
    motor_ratio_ccw_100[1] = req.ccw.motor2.at(3);
    motor_ratio_ccw_100[2] = req.ccw.motor3.at(3);
    motor_ratio_ccw_100[3] = req.ccw.motor4.at(3);

    motor_ratio_ccw_125[0] = req.ccw.motor1.at(4);
    motor_ratio_ccw_125[1] = req.ccw.motor2.at(4);
    motor_ratio_ccw_125[2] = req.ccw.motor3.at(4);
    motor_ratio_ccw_125[3] = req.ccw.motor4.at(4);

    motor_ratio_ccw_150[0] = req.ccw.motor1.at(5);
    motor_ratio_ccw_150[1] = req.ccw.motor2.at(5);
    motor_ratio_ccw_150[2] = req.ccw.motor3.at(5);
    motor_ratio_ccw_150[3] = req.ccw.motor4.at(5);
  }

  //OMG look at this
  {
    res.cw.motor1.push_back(motor_ratio_cw_025[0]);
    res.cw.motor2.push_back(motor_ratio_cw_025[1]);
    res.cw.motor3.push_back(motor_ratio_cw_025[2]);
    res.cw.motor4.push_back(motor_ratio_cw_025[3]);

    res.cw.motor1.push_back(motor_ratio_cw_050[0]);
    res.cw.motor2.push_back(motor_ratio_cw_050[1]);
    res.cw.motor3.push_back(motor_ratio_cw_050[2]);
    res.cw.motor4.push_back(motor_ratio_cw_050[3]);

    res.cw.motor1.push_back(motor_ratio_cw_075[0]);
    res.cw.motor2.push_back(motor_ratio_cw_075[1]);
    res.cw.motor3.push_back(motor_ratio_cw_075[2]);
    res.cw.motor4.push_back(motor_ratio_cw_075[3]);

    res.cw.motor1.push_back(motor_ratio_cw_100[0]);
    res.cw.motor2.push_back(motor_ratio_cw_100[1]);
    res.cw.motor3.push_back(motor_ratio_cw_100[2]);
    res.cw.motor4.push_back(motor_ratio_cw_100[3]);

    res.cw.motor1.push_back(motor_ratio_cw_125[0]);
    res.cw.motor2.push_back(motor_ratio_cw_125[1]);
    res.cw.motor3.push_back(motor_ratio_cw_125[2]);
    res.cw.motor4.push_back(motor_ratio_cw_125[3]);

    res.cw.motor1.push_back(motor_ratio_cw_150[0]);
    res.cw.motor2.push_back(motor_ratio_cw_150[1]);
    res.cw.motor3.push_back(motor_ratio_cw_150[2]);
    res.cw.motor4.push_back(motor_ratio_cw_150[3]);

    res.ccw.motor1.push_back(motor_ratio_ccw_025[0]);
    res.ccw.motor2.push_back(motor_ratio_ccw_025[1]);
    res.ccw.motor3.push_back(motor_ratio_ccw_025[2]);
    res.ccw.motor4.push_back(motor_ratio_ccw_025[3]);

    res.ccw.motor1.push_back(motor_ratio_ccw_050[0]);
    res.ccw.motor2.push_back(motor_ratio_ccw_050[1]);
    res.ccw.motor3.push_back(motor_ratio_ccw_050[2]);
    res.ccw.motor4.push_back(motor_ratio_ccw_050[3]);

    res.ccw.motor1.push_back(motor_ratio_ccw_075[0]);
    res.ccw.motor2.push_back(motor_ratio_ccw_075[1]);
    res.ccw.motor3.push_back(motor_ratio_ccw_075[2]);
    res.ccw.motor4.push_back(motor_ratio_ccw_075[3]);

    res.ccw.motor1.push_back(motor_ratio_ccw_100[0]);
    res.ccw.motor2.push_back(motor_ratio_ccw_100[1]);
    res.ccw.motor3.push_back(motor_ratio_ccw_100[2]);
    res.ccw.motor4.push_back(motor_ratio_ccw_100[3]);

    res.ccw.motor1.push_back(motor_ratio_ccw_125[0]);
    res.ccw.motor2.push_back(motor_ratio_ccw_125[1]);
    res.ccw.motor3.push_back(motor_ratio_ccw_125[2]);
    res.ccw.motor4.push_back(motor_ratio_ccw_125[3]);

    res.ccw.motor1.push_back(motor_ratio_ccw_150[0]);
    res.ccw.motor2.push_back(motor_ratio_ccw_150[1]);
    res.ccw.motor3.push_back(motor_ratio_ccw_150[2]);
    res.ccw.motor4.push_back(motor_ratio_ccw_150[3]);
  }

  res.cw.n_speed = N_LINEAR_SPEED;
  res.ccw.n_speed = N_LINEAR_SPEED;
  res.ok = true;
  return true;
}

std::string HWController::robotName()
{
  return this->robot_name;
}

void HWController::readSerial()
{
//#ifdef DEBUG_SERIAL_NUCLEO
  ROS_INFO("[HWController] readserial\n");
//#endif
  auto rx_data = serial->readAll();
  auto packet_ok = false;
  for(const auto &d : rx_data)
    packet_ok = (updateState((uchar)d) ? true : packet_ok);

  if(packet_ok)
    this->publish();
}

void HWController::readSerialArduino()
{
  auto serial_data = serial_arduino->readAll();
  auto packet_done = false;
  for(auto d : serial_data)
    packet_done = (updateStateArduino(d) ? true : packet_done);
#ifdef DEBUG_SERIAL_ARDUINO
  qDebug() << "[HWController] read Arduino :" << serial_data;
#endif
  if(packet_done)
    this->publishCompass();
}

//@TODO : remove callback
inline
bool HWController::updateStateArduino(uchar data)
{
  auto done = false;
  switch (rx_state_arduino) {
  case Arduino_start_U:
    if(data=='U')
      rx_state_arduino=Arduino_start_G;
    break;
  case Arduino_start_G:
    if(data=='G')
      rx_state_arduino=Arduino_start_M;
    else
      rx_state_arduino=Arduino_start_U;
    break;
  case Arduino_start_M:
    if(data=='M')
#ifdef USE_CMPS11
      rx_state_arduino=state_cmps_high_byte;
#else
      rx_state_arduino = state_key;
#endif
    else
      rx_state_arduino=Arduino_start_U;
    break;
#ifdef USE_CMPS11
  case state_cmps_high_byte:
    cmps = (data<<8);
    rx_state_arduino=state_cmps_low_byte;
    break;
  case state_cmps_low_byte:
    cmps += data;
    cmps /= 10;
    rx_state_arduino=state_key;
    if(arduino_cmps_cb){
      this->finalCmps = cmps+offsetCmps;
      if(finalCmps > 360)
        finalCmps -= 360;
      else if(finalCmps <0)
        finalCmps += 360;
      arduino_cmps_cb(finalCmps);
    }
    break;
#endif
  case state_key:
    if(data>0 && data<40)
    {
      if(arduino_cb)
        arduino_cb(data);
    }
    done = true;
    rx_state_arduino=Arduino_start_U;
    break;
  default:
    break;
  }
  return done;
}

void HWController::motor_multiplier(double speed, double wheel[], double *ratio)
{
  // y -> ratio
  // x -> speed
  // cw or ccw from wheel[i] sign
  if(speed<=0.25)
  {
    for(int i=0; i<4; i++)
    {
      const double multiplier = wheel[i] > 0 ? motor_ratio_cw_025[i] : motor_ratio_ccw_025[i];
      ratio[i] = linear(0.0,0.25,0.0,multiplier,speed);
    }
  }
  else if(speed<=0.5)
  {
    for(int i=0; i<4; i++)
    {
      const double multiplier = wheel[i] > 0 ? motor_ratio_cw_050[i] : motor_ratio_ccw_050[i];
      const double mult0 = wheel[i] > 0 ? motor_ratio_cw_025[i] : motor_ratio_ccw_025[i];
      ratio[i] = linear(0.25,0.5,mult0,multiplier,speed);
    }
  }
  else if(speed<=0.75)
  {
    for(int i=0; i<4; i++)
    {
      const double multiplier = wheel[i] > 0 ? motor_ratio_cw_075[i] : motor_ratio_ccw_075[i];
      const double mult0 = wheel[i] > 0 ? motor_ratio_cw_050[i] : motor_ratio_ccw_050[i];
      ratio[i] = linear(0.5,0.75,mult0,multiplier,speed);
    }
  }
  else if(speed<=1.0)
  {
    for(int i=0; i<4; i++)
    {
      const double multiplier = wheel[i] > 0 ? motor_ratio_cw_100[i] : motor_ratio_ccw_100[i];
      const double mult0 = wheel[i] > 0 ? motor_ratio_cw_075[i] : motor_ratio_ccw_075[i];
      ratio[i] = linear(0.75,1.0,mult0,multiplier,speed);
    }
  }
  else if(speed<=1.25)
  {
    for(int i=0; i<4; i++)
    {
      const double multiplier = wheel[i] > 0 ? motor_ratio_cw_125[i] : motor_ratio_ccw_125[i];
      const double mult0 = wheel[i] > 0 ? motor_ratio_cw_100[i] : motor_ratio_ccw_100[i];
      ratio[i] = linear(1.0,1.25,mult0,multiplier,speed);
    }
  }
  else
  {
    for(int i=0; i<4; i++)
    {
      const double multiplier = wheel[i] > 0 ? motor_ratio_cw_150[i] : motor_ratio_ccw_150[i];
      const double mult0 = wheel[i] > 0 ? motor_ratio_cw_125[i] : motor_ratio_ccw_125[i];
      ratio[i] = linear(1.25,1.5,mult0,multiplier,std::min(1.5,speed));
    }
  }
}

double HWController::linear(double x0, double x1, double y0, double y1, double xi)
{
  auto m = (y1-y0)/(x1-x0);
  auto yi = m*(xi-x0) + y0;
  return yi;
}

inline
bool HWController::updateState(uchar data)
{
  bool packet_ok(false);
  switch(rx_state)
  {
  case start_U:
    if(data=='U')
    {
      rx_state = start_G;
    }
    break;
  case start_G:
    if(data=='G')
      rx_state = start_M;
    else
      rx_state = start_U;
    break;
  case start_M:
    if(data=='M')
      rx_state = state_VE1A;
    else rx_state = start_U;
    break;

  case state_VE1A:
    buffer[0] = data;
    rx_state = state_VE1B;
    break;

  case state_VE1B:
    buffer[1] = data;
    rx_state = state_VE2A;
    break;

  case state_VE2A:
    buffer[2] = data;
    rx_state = state_VE2B;
    break;

  case state_VE2B:
    buffer[3] = data;
    rx_state = state_VE3A;
    break;

  case state_VE3A:
    buffer[4] = data;
    rx_state = state_VE3B;
    break;

  case state_VE3B:
    buffer[5] = data;
    rx_state = state_VE4A;
    break;

  case state_VE4A:
    buffer[6] = data;
    rx_state = state_VE4B;
    break;

  case state_VE4B:
    buffer[7] = data;
    rx_state = state_direction;
    break;

  case state_direction:
    buffer[8] = data;
    rx_state = state_adc_charge;
    break;
  case state_adc_charge:
    buffer[9] = data;
    rx_state = checksum;
    break;
  case checksum:
  {
    uint16_t sum = 0;
    for(int i=0; i<10; i++)
      sum += (uint8_t) (buffer[i]&0xFF);
    if((sum&0xFF) == data)
    {
      updateData();
      packet_ok = true;
    }
    else
    {
      update_data_time_point = Clock::now();
      std::cout << "checksum failed : ";
      std::cout << (uint16_t)sum << ',';
      std::cout << (uint16_t)data << ',';
      for(int i=0; i<11; ++i)
        std::cout << ((uint8_t) buffer[i]&0xFF)<<',';
      std::cout << '\n';
    }
    rx_state = start_U;
    break;
  }
  }
  return packet_ok;
}

inline
void HWController::updateData()
{
  ve[0] = (uint16_t)((buffer[0]&0xFF) | ((buffer[1]&0xFF)<<8));
  ve[1] = -((uint16_t)((buffer[2]&0xFF) | ((buffer[3]&0xFF)<<8)));
  ve[2] = (uint16_t)((buffer[4]&0xFF) | ((buffer[5]&0xFF)<<8));
  ve[3] = (uint16_t)((buffer[6]&0xFF) | ((buffer[7]&0xFF)<<8));

  if(buffer[8]&0x01)
    ve[0] = -ve[0];
  if(buffer[8]&0x02)
    ve[1] = -ve[1];
  if(buffer[8]&0x04)
    ve[2] = -ve[2];
  if(buffer[8]&0x08)
    ve[3] = -ve[3];

  ir = false;
  if(buffer[11]==1)
    ir = true;
}

inline
void HWController::updateMotorVelocity()
{
#if 1
  switch (this->base) {
  case BaseTypeA:
    wheel[0] = -0.86603*target_robot_vx + 0.5*target_robot_vy + 0.198*target_robot_w;
    wheel[1] =  0.86603*target_robot_vx + 0.5*target_robot_vy + 0.198*target_robot_w;
    wheel[2] =        0*target_robot_vx - 1.0*target_robot_vy + 0.198*target_robot_w;
    break;
  case BaseTypeB:
    wheel[0] = -0.5*target_robot_vx + 0.86603*target_robot_vy + 0.198*target_robot_w;
    wheel[1] =  0.5*target_robot_vx + 0.86603*target_robot_vy + 0.198*target_robot_w;
    wheel[2] =  0.5*target_robot_vx - 0.86603*target_robot_vy + 0.198*target_robot_w;
    wheel[3] = -0.5*target_robot_vx - 0.86603*target_robot_vy + 0.198*target_robot_w;
    break;
  default:
    break;
  }
#else
#ifdef BASE_TYPE_A
  wheel[0] = -0.86603*target_robot_vx + 0.5*target_robot_vy + 0.198*target_robot_w;
  wheel[1] =  0.86603*target_robot_vx + 0.5*target_robot_vy + 0.198*target_robot_w;
  wheel[2] =        0*target_robot_vx - 1.0*target_robot_vy + 0.198*target_robot_w;

  mutex.lock();
  ve_setpoint[0] = WHEEL_TO_VE_RATIO*wheel[0];
  ve_setpoint[1] = WHEEL_TO_VE_RATIO*wheel[1];
  ve_setpoint[2] = WHEEL_TO_VE_RATIO*wheel[2];

  ff_counter[0] = abs((wheel[0]>0 ? abs(ceil(wheel[0]*motor_ratio_cw_100[0]-ff[0])/motor_step_cw[0]) :
    abs(ceil(wheel[0]*motor_ratio_ccw_100[0]-ff[0])/motor_step_ccw[0])));
  ff_counter[1] = abs((wheel[1]>0 ? abs(ceil(wheel[1]*motor_ratio_cw_100[1]-ff[1])/motor_step_cw[1]) :
    abs(ceil(wheel[1]*motor_ratio_ccw_100[1]-ff[1])/motor_step_ccw[1])));
  ff_counter[2] = abs((wheel[2]>0 ? abs(ceil(wheel[2]*motor_ratio_cw_100[2]-ff[2])/motor_step_cw[2]) :
    abs(ceil(wheel[2]*motor_ratio_ccw_100[2]-ff[2])/motor_step_ccw[2])));
  mutex.unlock();

#else
  wheel[0] = -0.5*target_robot_vx + 0.86603*target_robot_vy + 0.198*target_robot_w;
  wheel[1] =  0.5*target_robot_vx + 0.86603*target_robot_vy + 0.198*target_robot_w;
  wheel[2] =  0.5*target_robot_vx - 0.86603*target_robot_vy + 0.198*target_robot_w;
  wheel[3] = -0.5*target_robot_vx - 0.86603*target_robot_vy + 0.198*target_robot_w;

  mutex.lock();
  ve_setpoint[0] = WHEEL_TO_VE_RATIO*wheel[0];
  ve_setpoint[1] = WHEEL_TO_VE_RATIO*wheel[1];
  ve_setpoint[2] = WHEEL_TO_VE_RATIO*wheel[2];
  ve_setpoint[3] = WHEEL_TO_VE_RATIO*wheel[3];

  ff_counter[MOTOR1_INDEX] = abs((wheel[0]>0 ? abs(ceil(wheel[0]*motor_ratio_cw[MOTOR1_INDEX]-ff[MOTOR1_INDEX])/motor_step_cw[MOTOR1_INDEX]) :
      abs(ceil(wheel[0]*motor_ratio_ccw[MOTOR1_INDEX]-ff[MOTOR1_INDEX])/motor_step_ccw[MOTOR1_INDEX])));

  ff_counter[MOTOR2_INDEX] = abs((wheel[1]>0 ? abs(ceil(wheel[1]*motor_ratio_cw[MOTOR2_INDEX]-ff[MOTOR2_INDEX])/motor_step_cw[MOTOR2_INDEX]) :
      abs(ceil(wheel[1]*motor_ratio_ccw[MOTOR2_INDEX]-ff[MOTOR2_INDEX])/motor_step_ccw[MOTOR2_INDEX])));

  ff_counter[MOTOR3_INDEX] = abs((wheel[2]>0 ? abs(wheel[2]*motor_ratio_cw[MOTOR3_INDEX]-ff[MOTOR3_INDEX])/motor_step_cw[MOTOR3_INDEX] :
      abs(ceil(wheel[2]*motor_ratio_ccw[MOTOR3_INDEX]-ff[MOTOR3_INDEX])/motor_step_ccw[MOTOR3_INDEX])));

  ff_counter[MOTOR4_INDEX] = abs((wheel[3]>0 ? abs(wheel[3]*motor_ratio_cw[MOTOR4_INDEX]-ff[MOTOR4_INDEX])/motor_step_cw[MOTOR4_INDEX] :
      abs(ceil(wheel[3]*motor_ratio_ccw[MOTOR4_INDEX]-ff[MOTOR4_INDEX])/motor_step_ccw[MOTOR4_INDEX])));

  mutex.unlock();
#endif
#endif
}

inline
void HWController::setMotorParameter(int i, double ratio, double step, bool cw, speed_t speed)
{
#if 1
  double *motor_ratio = nullptr;
  auto str = std::string(speed == Speed025 ? "Speed025" : (speed == Speed050 ? "Speed050" : (speed == Speed075 ? "Speed075" : (speed == Speed100 ? "Speed100" : (speed == Speed125 ? "Speed125" : (speed == Speed150 ? "Speed150" : ""))))));
  std::stringstream ss;
  ss <<  str
     << (cw ? " cw" : " ccw") << "[" << i << "] : "
     << ratio;
  ROS_INFO("%s", ss.str().c_str());

  switch (speed) {
  case Speed025:
    motor_ratio = cw ? &motor_ratio_cw_025[i-1] : &motor_ratio_ccw_025[i-1];
    break;
  case Speed050:
    motor_ratio = cw ? &motor_ratio_cw_050[i-1] : &motor_ratio_ccw_050[i-1];
    break;
  case Speed075:
    motor_ratio = cw ? &motor_ratio_cw_075[i-1] : &motor_ratio_ccw_075[i-1];
    break;
  case Speed100:
    motor_ratio = cw ? &motor_ratio_cw_100[i-1] : &motor_ratio_ccw_100[i-1];
    break;
  case Speed125:
    motor_ratio = cw ? &motor_ratio_cw_125[i-1] : &motor_ratio_ccw_125[i-1];
    break;
  case Speed150:
    motor_ratio = cw ? &motor_ratio_cw_150[i-1] : &motor_ratio_ccw_150[i-1];
    break;
  default:
    break;
  }

  if(motor_ratio)
    (*motor_ratio) = ratio;
#else
  switch(i)
  {
  case 1 :
    if(cw)
    {
      motor_ratio_cw_100[MOTOR1_INDEX] = ratio;
      motor_step_cw[MOTOR1_INDEX] = MOTOR1_STEP_CW;
    }
    else
    {
      motor_ratio_ccw_100[MOTOR1_INDEX] = ratio;
      motor_step_ccw[MOTOR1_INDEX] = MOTOR1_STEP_CCW;
    }
    break;

  case 2:
    if(cw)
    {
      motor_ratio_cw_100[MOTOR2_INDEX] = ratio;
      motor_step_cw[MOTOR2_INDEX] = MOTOR2_STEP_CW;
    }
    else
    {
      motor_ratio_ccw_100[MOTOR2_INDEX] = ratio;
      motor_step_ccw[MOTOR2_INDEX] = MOTOR2_STEP_CCW;
    }
    break;

  case 3:
    if(cw)
    {
      motor_ratio_cw_100[MOTOR3_INDEX] = ratio;
      motor_step_cw[MOTOR3_INDEX] = MOTOR3_STEP_CW;
    }
    else
    {
      motor_ratio_ccw_100[MOTOR3_INDEX] = ratio;
      motor_step_ccw[MOTOR3_INDEX] = MOTOR3_STEP_CCW;
    }
    break;

  case 4:
    if(cw)
    {
      motor_ratio_cw_100[MOTOR4_INDEX] = ratio;
      motor_step_cw[MOTOR4_INDEX] = MOTOR4_STEP_CW;
    }
    else
    {
      motor_ratio_ccw_100[MOTOR4_INDEX] = ratio;
      motor_step_ccw[MOTOR4_INDEX] = MOTOR4_STEP_CCW;
    }
    break;

  default:
    break;
  }
#endif
}
//@TODO : remove
//@Note : use getRobotVel instead
inline
void HWController::updateRobotVel()
{
#ifdef BASE_TYPE_A
  double vm_1 = ve[MOTOR1_INDEX]*ENCODER_RATIO;
  double vm_2 = ve[MOTOR2_INDEX]*ENCODER_RATIO;
  double vm_3 = ve[MOTOR3_INDEX]*ENCODER_RATIO;

  robot_vx =-0.5773472*  vm_1 + 0.5773472*vm_2  + 0*        vm_3;
  robot_vy = 0.3333333*  vm_1 + 0.3333333*vm_2  - 0.6666667*vm_3;
  robot_w  = 1.6835017*  vm_1 + 1.6835017*vm_2  + 1.6835017*vm_3;
#else
  double vm_1 = ve[MOTOR1_INDEX]*ENCODER_RATIO;
  double vm_2 = ve[MOTOR2_INDEX]*ENCODER_RATIO;
  double vm_3 = ve[MOTOR3_INDEX]*ENCODER_RATIO;
  double vm_4 = ve[MOTOR4_INDEX]*ENCODER_RATIO;

  robot_vx = -0.124999 *vm_1 + 0.124999 *vm_2 + 0.124999 *vm_3 - 0.124999 *vm_4;
  robot_vy =  0.2165058*vm_1 - 0.2165058*vm_2 - 0.2165058*vm_3 + 0.2165058*vm_4;
  robot_w  =  1.2626263*vm_1 + 1.2626263*vm_2 + 1.2626263*vm_3 + 1.2626263*vm_4;
#endif
}

//@TODO : remove
inline
void HWController::updateOdometry()
{
  auto c = cos(robot_theta);
  auto s = sin(robot_theta);
  auto g_vx = c*robot_vx-s*robot_vy;
  auto g_vy = s*robot_vx+c*robot_vy;
  auto g_w = robot_w;
  robot_x += g_vx/45;
  robot_y += g_vy/45;
  robot_theta += g_w/45;
}

inline
void HWController::motorControl()
{
  if(!pwm_test)
  {
#if 0
    if(!serial_connected)
      return;
#endif
    mutex.lock();
#if 0
    double m1_ratio = wheel[0] > 0 ? motor_ratio_cw_100[MOTOR1_INDEX] : motor_ratio_ccw_100[MOTOR1_INDEX];
    double m2_ratio = wheel[1] > 0 ? motor_ratio_cw_100[MOTOR2_INDEX] : motor_ratio_ccw_100[MOTOR2_INDEX];
    double m3_ratio = wheel[2] > 0 ? motor_ratio_cw_100[MOTOR3_INDEX] : motor_ratio_ccw_100[MOTOR3_INDEX];
    double m4_ratio = wheel[3] > 0 ? motor_ratio_cw_100[MOTOR4_INDEX] : motor_ratio_ccw_100[MOTOR4_INDEX];
#else
    auto speed = std::hypot(target_robot_vx,target_robot_vy);
    double ratio[4];
    motor_multiplier(speed,wheel,ratio);
    if(speed<0.05){
      if(target_robot_w){
        for(int i=0; i<4; i++)
          ratio[i] = 200;
      }
    }
    double m1_ratio = ratio[0];
    double m2_ratio = ratio[1];
    double m3_ratio = ratio[2];
    double m4_ratio = ratio[3];
#endif

    double m1_step = (wheel[0] > 0 ? motor_step_cw[MOTOR1_INDEX] : motor_step_ccw[MOTOR1_INDEX]);
    double m2_step = (wheel[1] > 0 ? motor_step_cw[MOTOR2_INDEX] : motor_step_ccw[MOTOR2_INDEX]);
    double m3_step = (wheel[2] > 0 ? motor_step_cw[MOTOR3_INDEX] : motor_step_ccw[MOTOR3_INDEX]);
    double m4_step = (wheel[3] > 0 ? motor_step_cw[MOTOR4_INDEX] : motor_step_ccw[MOTOR4_INDEX]);

    double error0 = (ve_setpoint[MOTOR1_INDEX] - ve[MOTOR1_INDEX]);
    double error1 = (ve_setpoint[MOTOR2_INDEX] - ve[MOTOR2_INDEX]);
    double error2 = (ve_setpoint[MOTOR3_INDEX] - ve[MOTOR3_INDEX]);
    double error3 = (ve_setpoint[MOTOR4_INDEX] - ve[MOTOR4_INDEX]);

    double target_ff1 = wheel[MOTOR1_INDEX]*m1_ratio;
    double target_ff2 = wheel[MOTOR2_INDEX]*m2_ratio;
    double target_ff3 = wheel[MOTOR3_INDEX]*m3_ratio;
    double target_ff4 = wheel[MOTOR4_INDEX]*m4_ratio;

#if 1
    if(abs(error0)>ERROR_RANGE)error0=0;
    if(abs(error1)>ERROR_RANGE)error1=0;
    if(abs(error2)>ERROR_RANGE)error2=0;
    if(abs(error3)>ERROR_RANGE)error3=0;

    d_error[MOTOR1_INDEX] = error0 - error[MOTOR1_INDEX];
    d_error[MOTOR2_INDEX] = error1 - error[MOTOR2_INDEX];
    d_error[MOTOR3_INDEX] = error2 - error[MOTOR3_INDEX] ;
    d_error[MOTOR4_INDEX] = error3 - error[MOTOR4_INDEX] ;
    error[MOTOR1_INDEX] = error0;
    error[MOTOR2_INDEX] = error1;
    error[MOTOR3_INDEX] = error2;
    error[MOTOR4_INDEX] = error3;

    if(ff[MOTOR1_INDEX]!=(int)(wheel[MOTOR1_INDEX]*m1_ratio))
    {
      if(abs(ff[MOTOR1_INDEX])<=255)
      {
        if(abs(ff[MOTOR1_INDEX]-target_ff1)>=m1_step){
          ff[MOTOR1_INDEX] += ff[MOTOR1_INDEX] > target_ff1 ? -m1_step : m1_step;
          pwm[MOTOR1_INDEX]=ff[MOTOR1_INDEX];}

      }
      else ff[MOTOR1_INDEX] = ff[MOTOR1_INDEX] < 0 ? -255 : 255;
    }


    if(ff[MOTOR2_INDEX]!=(int)(wheel[MOTOR2_INDEX]*m2_ratio))
    {
      if(abs(ff[MOTOR2_INDEX])<=255)
      {
        if(abs(ff[MOTOR2_INDEX]-target_ff2)>=m2_step){
          ff[MOTOR2_INDEX] += ff[MOTOR2_INDEX] > target_ff2 ? -m2_step : m2_step;
          pwm[MOTOR2_INDEX]=ff[MOTOR2_INDEX];}
      }
      else ff[MOTOR2_INDEX] = ff[MOTOR2_INDEX] < 0 ? -255 : 255;
    }


    if(ff[MOTOR3_INDEX]!=(int)(wheel[MOTOR3_INDEX]*m3_ratio))
    {
      if(abs(ff[MOTOR3_INDEX])<=255)
      {
        if(abs(ff[MOTOR3_INDEX]-target_ff3)>=m3_step){
          ff[MOTOR3_INDEX] += ff[MOTOR3_INDEX] > target_ff3 ? -m3_step : m3_step;
          pwm[MOTOR3_INDEX]=ff[MOTOR3_INDEX];}
      }
      else ff[MOTOR3_INDEX] = ff[MOTOR3_INDEX] < 0 ? -255 : 255;
    }

//    pwm[MOTOR1_INDEX]= pwm[MOTOR1_INDEX] + (error0 * kp[MOTOR1_INDEX] + d_error[MOTOR1_INDEX] * kd[MOTOR1_INDEX] );
//    pwm[MOTOR2_INDEX]= pwm[MOTOR2_INDEX] + (error1 * kp[MOTOR2_INDEX] + d_error[MOTOR2_INDEX] * kd[MOTOR2_INDEX] );
//    pwm[MOTOR3_INDEX]= pwm[MOTOR3_INDEX] + (error2 * kp[MOTOR3_INDEX] + d_error[MOTOR3_INDEX] * kd[MOTOR3_INDEX] );
#endif
#if 0

    pwm[MOTOR1_INDEX]= target_ff1;
    pwm[MOTOR2_INDEX]= target_ff2;
    pwm[MOTOR3_INDEX]= target_ff3;

#endif

//#ifndef BASE_TYPE_A
    if(this->base == BaseTypeB)
    {
      if(ff[MOTOR4_INDEX]!=(int)(wheel[3]*m4_ratio))
      {
        if(abs(ff[MOTOR4_INDEX])<255)
        {
          if((abs(ff[MOTOR4_INDEX])-std::abs(target_ff4)>m4_step))
            ff[MOTOR4_INDEX] += ff[MOTOR4_INDEX] > wheel[3]*m4_ratio ? -m4_step : m4_step;
        }else    ff[MOTOR4_INDEX] = ff[MOTOR4_INDEX] < 0 ? -255 : 255;
      }

      pwm[3]= ff[MOTOR4_INDEX] + (error3 * kp[MOTOR4_INDEX] + d_error[MOTOR4_INDEX] * kd[MOTOR4_INDEX] );
    }

//#endif
    for(int i=0; i<4; i++)
    {
      if(abs(pwm[i])>255)
      {
        pwm[i] = ff[i]<0 ? -255 : 255;
      }
    }
    mutex.unlock();
  }
  if(pwm_update_cb)
    pwm_update_cb(pwm[0],pwm[1],pwm[2],pwm[3],pwm[4],kick_del);
  if(control_cb)
    control_cb({ve_setpoint[0],ve_setpoint[1],ve_setpoint[2],ve_setpoint[3]},
    {ve[0],ve[1],ve[2],ve[3]});
}

void HWController::setOffsetCmps()
{
  this->offsetCmps = 0 - this->cmps;
}

void HWController::init()
{
  serial = new QSerialPort(this);
  serial_arduino = new QSerialPort(this);

  motor_ratio_cw_100[MOTOR1_INDEX] = MOTOR1_RATIO_CW;
  motor_ratio_cw_100[MOTOR2_INDEX] = MOTOR2_RATIO_CW;
  motor_ratio_cw_100[MOTOR3_INDEX] = MOTOR3_RATIO_CW;
  motor_ratio_cw_100[MOTOR4_INDEX] = MOTOR4_RATIO_CW;
  motor_ratio_ccw_100[MOTOR1_INDEX] = MOTOR1_RATIO_CCW;
  motor_ratio_ccw_100[MOTOR2_INDEX] = MOTOR2_RATIO_CCW;
  motor_ratio_ccw_100[MOTOR3_INDEX] = MOTOR3_RATIO_CCW;
  motor_ratio_ccw_100[MOTOR4_INDEX] = MOTOR4_RATIO_CCW;
  motor_step_cw[MOTOR1_INDEX] = MOTOR1_STEP_CW;
  motor_step_cw[MOTOR2_INDEX] = MOTOR2_STEP_CW;
  motor_step_cw[MOTOR3_INDEX] = MOTOR3_STEP_CW;
  motor_step_cw[MOTOR4_INDEX] = MOTOR4_STEP_CW;
  motor_step_ccw[MOTOR1_INDEX] = MOTOR1_STEP_CCW;
  motor_step_ccw[MOTOR2_INDEX] = MOTOR2_STEP_CCW;
  motor_step_ccw[MOTOR3_INDEX] = MOTOR3_STEP_CCW;
  motor_step_ccw[MOTOR4_INDEX] = MOTOR4_STEP_CCW;

  motor_param_list.clear();
  for(int i=0; i<4; i++)
  {
    motor_param_list.append(QString("motor%1_cw").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_025").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_025").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_050").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_050").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_075").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_075").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_125").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_125").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_150").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_150").arg(i+1));
  }

  QObject::connect(serial,SIGNAL(readyRead()),this,SLOT(readSerial()));
  QObject::connect(serial_arduino,SIGNAL(readyRead()),this,SLOT(readSerialArduino()));
  QObject::connect(this,SIGNAL(sendSerialData()),this,SLOT(sendData()));
}

HWController::MotorParameter::MotorParameter()
{
  for(auto s : linear_speeds)
  {
    linear_map_cw.insert(std::make_pair(s,0.0));
    linear_map_ccw.insert(std::make_pair(s,0.0));
  }
  for(auto s : rotation_speeds)
  {
    rotation_map_cw.insert(std::make_pair(s,0.0));
    rotation_map_ccw.insert(std::make_pair(s,0.0));
  }
}

bool fukuro::HWController::kickService(fukuro_common::Shoot::Request &req, fukuro_common::Shoot::Response &res)
{
  ROS_WARN("kick request : %d", req.kick_request);
    if(req.kick_request==3)
        requestKick(3);
    else if(req.kick_request==2)
        requestKick(2);
    res.ShootIsDone = 1;
    return true;
}

void fukuro::HWController::requestKick(int codename)
{
    std::cout << "[HWController] request kick" << std::endl;
    kick_request = true;
    this->codename = codename;
}

}
