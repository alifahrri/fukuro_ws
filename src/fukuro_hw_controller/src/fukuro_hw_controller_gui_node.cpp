#include <ros/ros.h>
#include <QApplication>
#include <QTimer>
#include "mainwindow.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "fukuro_hw_controller_gui_node");
  ros::NodeHandle nh;

  QApplication app(argc, argv);
  MainWindow w(nh);
  w.showMaximized();

  std::string robot_name;
  ros::get_environment_variable(robot_name,"FUKURO");
  ros::Subscriber hwctrl_sub = nh.subscribe(robot_name+"/hwcontrol_info",1,&MainWindow::hwControlInfo,&w);
  ros::Subscriber encoder_sub = nh.subscribe(robot_name+"/encoder",1,&MainWindow::hwEncoderInfo,&w);
  ros::Subscriber compass_sub = nh.subscribe(robot_name+"/compass",1,&MainWindow::hwCompassInfo,&w);
  ros::AsyncSpinner spinner(2);
  spinner.start();

  ROS_INFO("Hello world!");
  QTimer timer;
  timer.connect(&timer,&QTimer::timeout,[&]{
      if(!ros::ok())
        app.quit();
    });
  timer.start(1000.0);
  return app.exec();
}
