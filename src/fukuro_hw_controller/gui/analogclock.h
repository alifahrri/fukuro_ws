#ifndef ANALOGCLOCK_H
#define ANALOGCLOCK_H

#include <QWidget>

//#define HOUR
#define MINUTE

class AnalogClock : public QWidget
{
    Q_OBJECT

public:
    AnalogClock(QWidget *parent = 0);
    void setArrow(double arrow){south = arrow;}

private:
    double north;
    double south;

protected:
    void paintEvent(QPaintEvent *event) override;
};

#endif
