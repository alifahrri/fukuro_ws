#include "fieldwidget.h"
#include "ui_fieldwidget.h"

#include "fielditem.h"
#include "ballitem.h"
#include "robotitem.h"
#include "obstacleitem.h"
#include "pathitem.h"

#include <QGraphicsScene>

FieldWidget::FieldWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::FieldWidget),
  scene(new QGraphicsScene(this)),
  field(new FieldItem),
  balls(new BallItem),
  obstacles(new ObstacleItem),
  robot(new RobotItem),
  path(new PathItem)
{
  setWindowTitle(QString("Field Monitor"));
  ui->setupUi(this);
  scene->addItem(field);
  scene->addItem(obstacles);
  scene->addItem(path);
  scene->addItem(robot);
  scene->addItem(balls);
  scene->setBackgroundBrush(Qt::green);
  ui->graphicsView->setScene(scene);
  ui->graphicsView->scale(0.8,0.8);
  robot->drawText(false);
  robot->drawArrow(false);
  ui->graphicsView->setRenderHints(QPainter::Antialiasing |
                                   QPainter::HighQualityAntialiasing |
                                   QPainter::SmoothPixmapTransform);
}

FieldWidget::~FieldWidget()
{
  delete ui;
}

#ifdef USE_ROS

void FieldWidget::updateField(fukuro_common::WorldModel wm)
{
  robot->setState(wm.pose.x,wm.pose.y,wm.pose.theta,
                  wm.velocity.x,wm.velocity.y,wm.velocity.theta);
  ObstacleItem::Pos2DList obs;
  BallItem::Pos2DList ball;
  PathItem::Pos2DList waypoints;
  for(auto o : wm.obstacles)
  {
    obs.push_back(std::make_pair(o.x,o.y));
  }
  for(auto b : wm.balls)
  {
    ball.push_back(std::make_pair(b.x,b.y));
  }
  waypoints.push_back(std::make_pair(wm.pose.x,wm.pose.y));
  for(size_t i=1; i<wm.waypoints.size(); i++)
  {
    waypoints.push_back(std::make_pair(wm.waypoints[i].x,wm.waypoints[i].y));
  }
  obstacles->setObstacles(obs);
  balls->setBalls(ball);
  path->setPath(waypoints);
  scene->update();
}
#else

void FieldWidget::updateField(double px, double py, double theta)
{
    robot->setPos(px,py,theta);
}

#endif
