#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <functional>
#include <qcustomplot.h>
#include <QTimer>
#include <QElapsedTimer>
#include <QtGamepad/QGamepad>
#include <QtGamepad/QGamepadManager>
#include "fieldwidget.h"
#include "robotmonitordialog.h"
#include "graphicsview.h"
#include "analogclock.h"
#include <string>

#include "fukuro_common/Compass.h"
#include "fukuro_common/Encoder.h"
#include <fukuro_common/HWController.h>
#include <fukuro_common/HWControllerCommand.h>
#include <fukuro_common/HWControllerParamService.h>
#include <fukuro_common/HWControllerService.h>
#include <ros/ros.h>
#include "motor.hpp"

//TODO remove
typedef std::function<bool(std::string)> ConnectCallback;
typedef std::function<bool(void)> DisconnectCallback;
typedef std::function<void(void)> SetOffsetCMPS;
typedef std::function<void(int,int,int,int,int,int)> SetPWMCallback;
typedef std::function<void(double,double,double)> RobotVelCallback;
typedef std::function<void(int,double,double,bool,int)> UpdateMotorParamCallback;
typedef std::function<std::vector<double>(int)> ReadMotorParamCallback;
typedef std::function<void(bool,bool)> DribblerCallback;
typedef std::function<void(std::vector<double>,
                           std::vector<double>,
                           std::vector<double>,
                           std::vector<double>,
                           std::vector<double>,
                           std::vector<double>)> SaveCallback;
typedef std::function<void(double,double,double,double,double,double,double,double)> PIDCallback;
typedef std::function<void(int)> KickCallback;
typedef std::function<void()> ResetSTMCallback;

namespace Ui {
class MainWindow;
}

class SpeedItem;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
  explicit MainWindow(ros::NodeHandle &node, QWidget *parent = nullptr);
  void hwControl(fukuro_common::HWControllerServiceRequest &req, fukuro_common::HWControllerServiceResponse &res);
  /**
   * @brief hwControlInfo : update hwcontrol info from the subscribed topic of HWControlInfo message
   * @param msg : uhm, the received message obviously
   * @note for current implementation, topic subscibtion is done from main
   */
  void hwControlInfo(const fukuro_common::HWController::ConstPtr &msg);
  void hwEncoderInfo(const fukuro_common::EncoderConstPtr &msg);
  void hwCompassInfo(const fukuro_common::CompassConstPtr &msg);
  void hwControlParam(fukuro_common::HWControllerParamServiceRequest &req, fukuro_common::HWControllerParamServiceResponse &res);
private:
  /**
   * @brief publish, read private variable joy_vel and actually publish hwcontrol command message via ros
   */
  void publish();
  /**
   * @brief processSpeed, set private variable joy_vel; automatically published when call publish()
   * @param vx
   * @param vy
   * @param w
   */
  void processSpeed(double vx, double vy, double w);
  /**
   * @brief loadSettings from .ini file
   * @param filename : path to .ini file
   */
  void loadSettings(QString filename);
  /**
   * @brief setMotorParameter : setting motor parameter given parameter
   * @param i : motor index
   * @param ratio : parameter value (feedforward value)
   * @param step : currently unused
   * @param cw : is this value for cw or ccw?
   * @param speed : specifies motor speed
   */
  void setMotorParameter(int i, double ratio, double step, bool cw=true, fukuro::speed_t speed=fukuro::Speed100);
private slots:
  /**
   * @brief hwControlRefresh : call hwcontrol service client to refrest list of serial ports
   */
  void hwControlRefresh();
  /**
   * @brief hwControlConnect : call hwcontrol service client to connect to stm32
   */
  void hwControlConnect();
  /**
   * @brief hwControlArduino : call hwcontrol service client to connect to arduino
   */
  void hwControlArduino();
  void processJoystick();
  /**
   * @brief hwControlParam : call hwcontrol parameter service client to update motor parameter
   */
  void hwControlParam();
  /**
   * @brief updateParamGui, read motor parameter and update spin boxes
   */
  void updateParamGui();
private:
  std::atomic_bool param_blocker{false};
  std::string robot_name;
  ros::Publisher hwctrl_pub;
  ros::ServiceClient hw_client;
  ros::ServiceClient param_client;
  fukuro::MotorParameter motor_param;
  struct JoystickVelocity
  {
    JoystickVelocity() {}
    double vx;
    double vy;
    double w;
  } joy_vel;
  struct Dribble {
    int dir;
    float speed;
  } dribble;
private:
  QVector<QDoubleSpinBox*> cw_sboxes;
  QVector<QDoubleSpinBox*> ccw_sboxes;
  QVector<QLineEdit*> encoder_line;
  QVector<QLineEdit*> encoder_sum_line;
  QVector<QLineEdit*> pwm_line;
  QVector<QString> motor_param_list;
  SpeedItem *speed_item;

public:
    enum plot_mode_t {
        plot_ve_time,
        plot_ve_rad_time,
        plot_pwm_ve_time,
        plot_control,
        plot_ff_ve
    };

    //TODO remove
    explicit MainWindow(bool tuning=false, bool min_gui=false, QWidget *parent = 0);
    void setConnectCallback(ConnectCallback cb){ connect_cb = cb; }
    void setConnectArduinoCallback(ConnectCallback cb) { connect_arduino_cb = cb; }
    void setDisconectCallback(DisconnectCallback cb) { disconnect_cb = cb; }
    void setDisconectArduinoCallback(DisconnectCallback cb) { disconnect_arduino_cb = cb; }
    void setPWMCallback(SetPWMCallback cb) { pwm_cb = cb; }
    void setRobotVelCallback(RobotVelCallback cb);
    void setUpdateParamCallback(UpdateMotorParamCallback cb) { param_cb = cb; }
    void setReadParamCallback(ReadMotorParamCallback cb) { read_param_cb = cb; }
    void setDribblerCallback(DribblerCallback cb) { dribbler_cb = cb; }
    void setSaveCallback(SaveCallback cb) { save_cb = cb; }
    void setResetSTMCallback(ResetSTMCallback cb) { reset_stm_cb = cb; }
    void setParam(std::vector<double> cw, std::vector<double> cw_step,
                  std::vector<double> ccw, std::vector<double> ccw_step);
    void setPID(double kp1, double kd1, double kp2, double kd2, double kp3, double kd3, double kp4, double kd4);
    void setPIDCallback(PIDCallback cb) { pid_cb = cb; }
    void setKickCallback(KickCallback cb) { kick_cb = cb; }
    void setCompass(double cmps);
    bool connectSerial(std::string port_name, int baud_rate=38400);
    void readSettings();
    void setOffsetCmpsCallback(SetOffsetCMPS cb){offset_cmps_cb = cb;}
    std::vector<std::string> getportlist(){
        std::vector<std::string> ret;
        for(auto s : serial_port_list){
            ret.push_back(s.toStdString());
        }
        return ret;
    }
    std::vector<std::string> getmanufacturer(){
        std::vector<std::string> ret;
        for(auto m : manufacturer_list){
            ret.push_back(m.toStdString());
        }
        return ret;
    }
    bool getSTMConnection(){
        return STMConnection;
    }
    bool getArduinoConnection(){
        return ArduinoConnection;
    }
    bool getisCompass(){
        return isCompass;
    }
    ~MainWindow();

public slots:
    void updateRobotVel(double vx, double vy, double w);
    void updateRawValue(int ve1, int ve2, int ve3, int ve4, int ve5, bool ir, double dt);
    void updateValue(double ve1, double ve2, double ve3, double ve4, double ve5, bool ir, double dt);
    void updateOdometry(double px, double py, double theta, double vx, double vy, double w);
    void updatePWMValue(int f1, int f2, int f3, int f4, int f5, int kick);
    void updateControl(int ve_set1, int ve_set2, int ve_set3, int ve1, int ve2, int ve3);
    void updateOffsetCmps();

private:
    Ui::MainWindow *ui;
    ConnectCallback connect_cb;
    ConnectCallback connect_arduino_cb;
    DisconnectCallback disconnect_cb;
    DisconnectCallback disconnect_arduino_cb;
    SetPWMCallback pwm_cb;
    RobotVelCallback robot_vel_cb;
    UpdateMotorParamCallback param_cb;
    ReadMotorParamCallback read_param_cb;
    DribblerCallback dribbler_cb;
    SaveCallback save_cb;
    PIDCallback pid_cb;
    KickCallback kick_cb;
    SetOffsetCMPS offset_cmps_cb;
    ResetSTMCallback reset_stm_cb;
    plot_mode_t plot_mode;
    QTimer *timer;
    RobotMonitorDialog *robot_monitor;
    FieldWidget *field_widget;
    QGraphicsProxyWidget *proxy_field;
    QGraphicsScene *field_scene;
    QGraphicsScene *robot_vel_scene;
    QElapsedTimer *elapsed_timer;
    QVector<QString> serial_port_list;
    QVector<QString> manufacturer_list;
    QGamepad *gamepad;
    QCPGraph* ve1_time_graph;
    QCPGraph* ve2_time_graph;
    QCPGraph* ve3_time_graph;
    QCPGraph* ve4_time_graph;
    QCPGraph* ve5_time_graph;
    QCPGraph* ve1_rad_time_graph;
    QCPGraph* ve2_rad_time_graph;
    QCPGraph* ve3_rad_time_graph;
    QCPGraph* ve4_rad_time_graph;
    QCPGraph* ve5_rad_time_graph;
    QCPGraph* ve1_setpoint_graph;
    QCPGraph* ve2_setpoint_graph;
    QCPGraph* ve3_setpoint_graph;
    QCPGraph* pwm1_graph;
    QCPGraph* pwm2_graph;
    QCPGraph* pwm3_graph;
    QCPGraph* pwm4_graph;
    QCPGraph* pwm5_graph;
    bool d;
    bool isCompass;
    bool STMConnection;
    bool ArduinoConnection;
public slots:
    void updateSerialInfo();

private slots:
    void setRobotVelU();
    void setRobotVelD();
    void setRobotVelL();
    void setRobotVelR();
    void setRobotVelCW();
    void setRobotVelCCW();
    void setPID();
    void saveParam();
    void setDribblerIn(bool d);
    void setDribblerOut(bool d);
    void resetRobotVel();
    void processGamepad();
    void processGamepadButton();
    void processGamepadRotate();
    void updateMotorParam();
    void updateMotorParamCCW();
    void toggleDribbler();
    void toggleDribblerOut();
    void kick();
    void resetSTM();
    void updateParam();
    void connectSerial();
    void connectArduino();
    void updatePWM();
    void updatePlot();
    void resetPWM();

public:
    void connectSerial(int index);
    void connectArduino(int index);
};

#endif // MAINWINDOW_H