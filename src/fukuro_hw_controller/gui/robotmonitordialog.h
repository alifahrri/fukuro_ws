#ifndef ROBOTMONITORDIALOG_H
#define ROBOTMONITORDIALOG_H

#include <QDialog>
#include "robotitem.h"

namespace Ui {
class RobotMonitorDialog;
}

class RobotMonitorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RobotMonitorDialog(QWidget *parent = 0);
    ~RobotMonitorDialog();
    void setBattery(double b1=24.0, double b2=12.0);
    void setCompass(double cmps);
    void setVelocity(double vx, double vy, double vw_);

private:
    Ui::RobotMonitorDialog *ui;
    RobotItem* robot_item;
    QGraphicsScene* scene;
    QGraphicsTextItem* vt;
    QGraphicsTextItem* vw;
};

#endif // ROBOTMONITORDIALOG_H
