#include "pathitem.h"
#include <QPainter>

PathItem::PathItem()
{

}

void PathItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(QPen(Qt::black,1.5));
  painter->setBrush(Qt::gray);
  QVector<QRectF> ellipse;
  QVector<QLineF> lines;
  for(size_t i=1; i<path.size(); i++)
  {
    ellipse.push_back((QRectF(path[i-1].first-5.0,-(path[i-1].second-5.0),5.0,5.0)));
//    painter->drawEllipse(QRectF(path[i-1].first*100.0-5.0,-(path[i-1].second*100.0-5.0),5.0,5.0));
    lines.push_back(QLineF(QPointF(path[i-1].first,-path[i-1].second),QPointF(path[i].first,-path[i].second)));
//    painter->drawLine(QPointF(path[i-1].first*100.0,-path[i-1].second),QPointF(path[i].first*100.0,-path[i].second*100.0));
  }
  if(path.size()>=1)
  {
    ellipse.push_back(QRectF(path[path.size()-1].first-5.0,-(path[path.size()-1].second-5.0),5.0,5.0));
//    painter->drawEllipse(QRectF(path[path.size()-1].first*100.0-5.0,-(path[path.size()-1].second*100.0-5.0),5.0,5.0));
  }
  painter->setPen(Qt::NoPen);
  for(auto e : ellipse)
    painter->drawEllipse(e);
  painter->setPen(QPen(Qt::black,1.5));
  painter->drawLines(lines);
}

QRectF PathItem::boundingRect() const
{

}
