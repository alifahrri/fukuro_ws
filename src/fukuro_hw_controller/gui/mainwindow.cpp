#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "fukuro_common/MotorParameter.h"
#include "speeditem.h"
#include <ros/ros.h>
#include <QSerialPortInfo>
#include <QGamepadManager>
#include <iostream>

#define DEFAULT_PPR 1024

MainWindow::MainWindow(ros::NodeHandle &node, QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  robot_monitor(new RobotMonitorDialog(this)),
  field_widget(new FieldWidget),
  proxy_field(new QGraphicsProxyWidget(0,Qt::Window)),
  field_scene(new QGraphicsScene(this)),
  robot_vel_scene(new QGraphicsScene(this)),
  speed_item(new SpeedItem)
{
  ui->setupUi(this);

  param_blocker = false;
  std::string fukuro_path;
  ros::get_environment_variable(robot_name, "FUKURO");
  ros::get_environment_variable(fukuro_path, "FUKURO_WS");
  if(fukuro_path.empty())
  {
    std::stringstream ss;
    ss << "/home/" << std::getenv("USER")
       << "/fukuro_ws/src/fukuro_hw_controller/settings/";
    fukuro_path = ss.str();
  }

  QGraphicsProxyWidget *proxy_robot = new QGraphicsProxyWidget;
  field_scene->addPixmap(QPixmap(":/fukuro.png").scaledToHeight(500,Qt::SmoothTransformation));
  proxy_robot->setWidget(robot_monitor);
  proxy_field->setWidget(field_widget);
  ui->graphicsView->setScene(new QGraphicsScene(this));
  ui->graphicsView->scene()->addItem(proxy_robot);
  ui->graphicsView->scale(0.8,0.8);
  ui->graphicsView_2->setScene(field_scene);
  ui->graphicsView_2->scene()->setBackgroundBrush(Qt::black);
  ui->graphicsView_2->scene()->addItem(proxy_field);
  ui->graphicsView_2->scale(0.7,0.7);

  //ROS Connection
  auto srv_param_name = robot_name + "/hw_param_service";
  auto srv_hw_name = robot_name + "/hw_service";
  auto hwcmd_topic_name = robot_name + "/hwcontrol_command";
  this->param_client = node.serviceClient<fukuro_common::HWControllerParamService>(srv_param_name);
  this->hw_client = node.serviceClient<fukuro_common::HWControllerService>(srv_hw_name);
  this->hwctrl_pub = node.advertise<fukuro_common::HWControllerCommand>(hwcmd_topic_name,1);

  ui->speed_combo_box->addItem(QString("0.25"));
  ui->speed_combo_box->addItem(QString("0.5"));
  ui->speed_combo_box->addItem(QString("0.75"));
  ui->speed_combo_box->addItem(QString("1.0"));
  ui->speed_combo_box->addItem(QString("1.25"));
  ui->speed_combo_box->addItem(QString("1.5"));

  cw_sboxes.push_back(ui->motor1_spinbox);
  cw_sboxes.push_back(ui->motor2_spinbox);
  cw_sboxes.push_back(ui->motor3_spinbox);
  cw_sboxes.push_back(ui->motor4_spinbox);

  ccw_sboxes.push_back(ui->motor1_ccw_spinbox);
  ccw_sboxes.push_back(ui->motor2_ccw_spinbox);
  ccw_sboxes.push_back(ui->motor3_ccw_spinbox);
  ccw_sboxes.push_back(ui->motor4_ccw_spinbox);

  encoder_line.push_back(ui->raw_ve1_line);
  encoder_line.push_back(ui->raw_ve2_line);
  encoder_line.push_back(ui->raw_ve3_line);
  encoder_line.push_back(ui->raw_ve4_line);
  encoder_line.push_back(ui->raw_ve5_line);

  encoder_sum_line.push_back(ui->ve1_line);
  encoder_sum_line.push_back(ui->ve2_line);
  encoder_sum_line.push_back(ui->ve3_line);
  encoder_sum_line.push_back(ui->ve4_line);
  encoder_sum_line.push_back(ui->ve5_line);

  pwm_line.push_back(ui->pwm1_line_edit);
  pwm_line.push_back(ui->pwm2_line_edit);
  pwm_line.push_back(ui->pwm3_line_edit);
  pwm_line.push_back(ui->pwm4_line_edit);
  pwm_line.push_back(ui->pwm5_line_edit);

  //create parameter list for reading .ini file
  motor_param_list.clear();
  for(int i=0; i<4; i++)
  {
    //connect sbox signal to reading and param service
    connect(cw_sboxes[i],SIGNAL(valueChanged(double)),
            this,SLOT(hwControlParam()));
    connect(ccw_sboxes[i],SIGNAL(valueChanged(double)),
            this,SLOT(hwControlParam()));
    motor_param_list.append(QString("motor%1_cw").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_025").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_025").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_050").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_050").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_075").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_075").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_125").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_125").arg(i+1));
    motor_param_list.append(QString("motor%1_cw_150").arg(i+1));
    motor_param_list.append(QString("motor%1_ccw_150").arg(i+1));
  }

  //.ini file
  auto file = QString::fromStdString(fukuro_path + robot_name + ".ini");

  connect(ui->speed_combo_box,
          SIGNAL(currentIndexChanged(int)),
          this, SLOT(updateParamGui()));

  //actually load settings
  this->loadSettings(file);

  ui->speed_combo_box->setCurrentIndex(3);

  //wait for hw_controller_node
  hw_client.waitForExistence();
  param_client.waitForExistence();

  //speed ui
  ui->speedGraphicsView->setRenderHint(QPainter::Antialiasing);
  ui->speedGraphicsView->setScene(new QGraphicsScene(-320,-240,640,480,this));
  ui->speedGraphicsView->scene()->addItem(speed_item);

  //init gamepad
  auto list = QGamepadManager::instance();
  int idx = 0;
  if(list->connectedGamepads().size())
    idx = list->connectedGamepads().front();
  gamepad = new QGamepad(idx,this);

  connect(ui->refresh_button,&QPushButton::clicked,[=]{
      this->hwControlRefresh();
  });
  connect(ui->connect_button,&QPushButton::clicked,[=]{
      this->hwControlConnect();
  });
  connect(ui->connect_arduino_button,&QPushButton::clicked,[=]{
      this->hwControlArduino();
  });

  //button connection
  connect(ui->u_button,&QPushButton::pressed,[=]{
      auto v = ui->testVelSpinBox->value();
      processSpeed(v,0.0,0.0);
      publish();
  });
  connect(ui->u_button,&QPushButton::released,[=]{
    processSpeed(0.0,0.0,0.0);
    publish();
  });
  connect(ui->d_button,&QPushButton::pressed,[=]{
      auto v = ui->testVelSpinBox->value();
      processSpeed(-v,0.0,0.0);
      publish();
  });
  connect(ui->d_button,&QPushButton::released,[=]{
    processSpeed(0.0,0.0,0.0);
    publish();
  });
  connect(ui->l_button,&QPushButton::pressed,[=]{
      auto v = ui->testVelSpinBox->value();
      processSpeed(0.0,v,0.0);
      publish();
  });
  connect(ui->l_button,&QPushButton::released,[=]{
    processSpeed(0.0,0.0,0.0);
    publish();
  });
  connect(ui->r_button,&QPushButton::pressed,[=]{
      auto v = ui->testVelSpinBox->value();
      processSpeed(0.0,-v,0.0);
      publish();
  });
  connect(ui->r_button,&QPushButton::released,[=]{
    processSpeed(0.0,0.0,0.0);
    publish();
  });
  connect(ui->ccw_button,&QPushButton::clicked,[=]{
      processSpeed(0.0,0.0,-0.5);
      publish();
  });
  connect(ui->cw_button,&QPushButton::clicked,[=]{
      processSpeed(0.0,0.0,0.5);
      publish();
  });
  connect(ui->ff5_scroll,&QScrollBar::valueChanged,[=]{
    dribble.speed = abs(ui->ff5_scroll->value());
    if(ui->ff5_scroll->value()>0)
      dribble.dir = 1;
    else
      dribble.dir = 0;
    publish();
  });
  connect(gamepad,SIGNAL(axisLeftXChanged(double)),this,SLOT(processJoystick()));
  connect(gamepad,SIGNAL(axisLeftYChanged(double)),this,SLOT(processJoystick()));
  connect(gamepad,SIGNAL(buttonUpChanged(bool)),this,SLOT(processGamepadButton()));
  connect(gamepad,SIGNAL(buttonDownChanged(bool)),this,SLOT(processGamepadButton()));
  connect(gamepad,SIGNAL(buttonLeftChanged(bool)),this,SLOT(processGamepadButton()));
  connect(gamepad,SIGNAL(buttonRightChanged(bool)),this,SLOT(processGamepadButton()));

  speed_item->setMotor(this->motor_param);
  speed_item->callback = [&](const fukuro::MotorParameter& motor)
  {
      this->motor_param = motor;
      this->updateParamGui();
      this->hwControlParam();
  };

  ROS_WARN("READY");
}

inline
void MainWindow::hwControl(fukuro_common::HWControllerServiceRequest &req, fukuro_common::HWControllerServiceResponse &res)
{
  this->hw_client.call(req,res);
}

void MainWindow::hwControlInfo(const fukuro_common::HWController::ConstPtr &msg)
{
  ROS_INFO("receive msg :");

  std::stringstream ss;
  //  ss << "encoder (current tick) : ";
  //  for(const auto& e : msg->encoder.current_tick)
  //    ss << e << "; ";
  //  ss << std::endl;
  //  ss << "encoder (total tick) : ";
  //  for(const auto& e : msg->encoder.total_tick)
  //    ss << e << "; ";
  //  ss << std::endl;

  ss << "pwm : ";
  auto pwm_line_it = pwm_line.begin();
  for(const auto &p : msg->pwm.motor)
  {
    ss << p << "; ";
    if(pwm_line_it != pwm_line.end())
    {
      (*pwm_line_it)->setText(QString::number(p));
      ++pwm_line_it;
    }
  }
  ss << std::endl;

  //  ss << "compass : " << msg->compass.cmps << ";\n";
  ss << "ir : " << msg->ir << ";\n";
  ss << "base_type : " << msg->base_type << ";\n";
  ss << "velocity : "
     << msg->vel.x << "; "
     << msg->vel.y << "; "
     << msg->vel.theta << ";\n";

  std::cout << ss.str() << std::flush;
}

void MainWindow::hwEncoderInfo(const fukuro_common::EncoderConstPtr &msg)
{
  ROS_INFO("receive encoder msg");
  auto raw_it = encoder_line.begin();
  auto sum_it = encoder_sum_line.begin();
  for(auto e : msg->current_tick)
    if(raw_it != encoder_line.end())
    {
      (*raw_it)->setText(QString::number(e));
      raw_it++;
    }
  for(auto e : msg->total_tick)
    if(sum_it != encoder_sum_line.end())
    {
      (*sum_it)->setText(QString::number(e));
      sum_it++;
    }
}

void MainWindow::hwCompassInfo(const fukuro_common::CompassConstPtr &msg)
{
  ROS_INFO("receive compass msg");
  this->ui->CompassArrow->setArrow(msg->cmps);
}

void MainWindow::hwControlParam(fukuro_common::HWControllerParamServiceRequest &req, fukuro_common::HWControllerParamServiceResponse &res)
{
  if(!param_client.call(req, res))
      ROS_ERROR("call service failed");
}

void MainWindow::hwControlParam()
{
  if(param_blocker)
    return;
  param_blocker = true;
  ROS_WARN("control param request");

  //update parameter
  auto j = ui->speed_combo_box->currentIndex();
  auto speed = static_cast<fukuro::speed_t>(j);
  for(size_t i=0; i<N_MAX_MOTOR; i++)
  {
    motor_param.linear_map_cw.at(i).at(speed) = cw_sboxes.at(i)->value();
    motor_param.linear_map_ccw.at(i).at(speed) = ccw_sboxes.at(i)->value();
  }

  //call service
  fukuro_common::HWControllerParamServiceRequest req;
  fukuro_common::HWControllerParamServiceResponse res;

  req.cw.n_speed = N_LINEAR_SPEED;
  req.ccw.n_speed = N_LINEAR_SPEED;
  {
    auto cw  = &motor_param.linear_map_cw;
    auto ccw = &motor_param.linear_map_ccw;
    for(size_t i=0; i<N_MAX_MOTOR; i++)
    {
      fukuro_common::MotorParameter::_motor1_type *cw_motor   = nullptr;
      fukuro_common::MotorParameter::_motor1_type *ccw_motor  = nullptr;
      switch(i)
      {
      case 0 :
        cw_motor = &req.cw.motor1;
        ccw_motor = &req.ccw.motor1;
        break;
      case 1 :
        cw_motor = &req.cw.motor2;
        ccw_motor = &req.ccw.motor2;
        break;
      case 2 :
        cw_motor = &req.cw.motor3;
        ccw_motor = &req.ccw.motor3;
        break;
      case 3 :
        cw_motor = &req.cw.motor4;
        ccw_motor = &req.ccw.motor4;
        break;
      }
      for(size_t k=0; k<N_LINEAR_SPEED; k++)
      {
        auto speed = static_cast<fukuro::speed_t>(k);
        cw_motor->push_back(cw->at(i).at(speed));
        ccw_motor->push_back(ccw->at(i).at(speed));
      }
    }
  }

  //call the client
  hwControlParam(req,res);

  //debugging
  std::stringstream ss;

  {
    ss << "REQUEST\n";
    ss << "cw : \n";
    for(size_t i=0; i<req.cw.n_speed; i++)
    {
      ss << "speed[" << i << "] [m1,..,m4] : [";
      ss << req.cw.motor1.at(i) << ", "
         << req.cw.motor2.at(i) << ", "
         << req.cw.motor3.at(i) << ", "
         << req.cw.motor4.at(i) << "]\n";
    }
    ss << "ccw : \n";
    for(size_t i=0; i<req.ccw.n_speed; i++)
    {
      ss << "speed[" << i << "] [m1,..,m4] : [";
      ss << req.ccw.motor1.at(i) << ", "
         << req.ccw.motor2.at(i) << ", "
         << req.ccw.motor3.at(i) << ", "
         << req.ccw.motor4.at(i) << "]\n";
    }
  }

  {
    ss << "RESPONSE\n";
    ss << "cw : \n";
    for(size_t i=0; i<res.cw.n_speed; i++)
    {
      ss << "speed[" << i << "] [m1,..,m4] : [";
      ss << res.cw.motor1.at(i) << ", "
         << res.cw.motor2.at(i) << ", "
         << res.cw.motor3.at(i) << ", "
         << res.cw.motor4.at(i) << "]\n";
    }
    ss << "ccw : \n";
    for(size_t i=0; i<res.ccw.n_speed; i++)
    {
      ss << "speed[" << i << "] [m1,..,m4] : [";
      ss << res.ccw.motor1.at(i) << ", "
         << res.ccw.motor2.at(i) << ", "
         << res.ccw.motor3.at(i) << ", "
         << res.ccw.motor4.at(i) << "]\n";
    }
  }

  ROS_WARN("service result :\n %s", ss.str().c_str());
  param_blocker = false;
}

void MainWindow::loadSettings(QString filename)
{
  ROS_WARN("loading %s", filename.toStdString().c_str());
  QSettings settings(filename, QSettings::IniFormat);
  settings.beginGroup(robot_name.c_str());

  for(const QString& str : motor_param_list)
  {
    if(settings.contains(str))
    {
      auto val = settings.value(str).toDouble();
      auto cw = str.contains("_cw");
      auto idx = 1;
      if(str.contains("2_"))
        idx = 2;
      else if(str.contains("3_"))
        idx = 3;
      else if(str.contains("4_"))
        idx = 4;

      auto speed = fukuro::Speed100;
      if(str.contains(QString("025")))
        speed = fukuro::Speed025;
      else if(str.contains(QString("050")))
        speed = fukuro::Speed050;
      else if(str.contains(QString("075")))
        speed = fukuro::Speed075;
      else if(str.contains(QString("125")))
        speed = fukuro::Speed125;
      else if(str.contains(QString("150")))
        speed = fukuro::Speed150;

      setMotorParameter(idx-1,val,15,cw,speed);
    }
  }
  settings.endGroup();

  ROS_WARN("done");
}

void MainWindow::publish()
{
  fukuro_common::HWControllerCommand cmd;
  cmd.vel.Vx = joy_vel.vx;
  cmd.vel.Vy = joy_vel.vy;
  cmd.vel.w = joy_vel.w;
  cmd.dribbler.dir_in = dribble.dir;
  cmd.dribbler.speed = dribble.speed;
  hwctrl_pub.publish(cmd);
}

void MainWindow::processSpeed(double vx, double vy, double w)
{
  joy_vel.vx = vx;
  joy_vel.vy = vy;
  joy_vel.w = w;
}

void MainWindow::setMotorParameter(int i, double ratio, double step, bool cw, fukuro::speed_t speed)
{
  auto maps = (cw? &motor_param.linear_map_cw : &motor_param.linear_map_ccw);
  maps->at(i)[speed] = ratio;
}

void MainWindow::hwControlRefresh()
{
  fukuro_common::HWControllerServiceRequest request;
  fukuro_common::HWControllerServiceResponse response;
  request.refresh = 1;
  hwControl(request,response);
  ui->comboBox->clear();
  ui->comboBox_2->clear();
  for(const auto& port : response.port_list)
  {
    ui->comboBox->addItem(port.c_str());
    ui->comboBox_2->addItem(port.c_str());
  }
}

void MainWindow::hwControlConnect()
{
  fukuro_common::HWControllerServiceRequest request;
  fukuro_common::HWControllerServiceResponse response;
  request.STMConnect = ui->comboBox->currentIndex();
  request.isSTM = 1;
  hwControl(request,response);
  if(response.STMSuccess)
    ROS_WARN("STM connection successful!!!");
  else
    ROS_ERROR("STM connection failed!!!");
}

void MainWindow::hwControlArduino()
{
  fukuro_common::HWControllerServiceRequest request;
  fukuro_common::HWControllerServiceResponse response;
  request.ArduinoConnect = ui->comboBox_2->currentIndex();
  request.isArduino = 1;
  hwControl(request,response);
  if(response.ArduinoSuccess)
    ROS_WARN("Arduino connection successful!!!");
  else
    ROS_ERROR("Arduino connection failed");
}

void MainWindow::processJoystick()
{
  processSpeed(-gamepad->axisLeftY(),-gamepad->axisLeftX(),0.0);
  publish();
}

void MainWindow::updateParamGui()
{
  if(param_blocker)
    return;
  ROS_WARN("read sboxes");
  param_blocker = true;
  auto idx = ui->speed_combo_box->currentIndex();
  if(idx < 0)
    return;
  auto sidx = static_cast<fukuro::speed_t>(idx);
  auto cw = &(motor_param.linear_map_cw);
  auto ccw = &(motor_param.linear_map_ccw);
  for(size_t i=0; i<ccw_sboxes.size(); i++)
  {
    auto ccw_val = ccw->at(i).at(sidx);
    auto cw_val = cw->at(i).at(sidx);
    ccw_sboxes.at(i)->setValue(ccw_val);
    cw_sboxes.at(i)->setValue(cw_val);
  }
  this->update();
  param_blocker = false;
}

MainWindow::MainWindow(bool tuning, bool min_gui, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    connect_cb(NULL),
    connect_arduino_cb(NULL),
    disconnect_cb(NULL),
    disconnect_arduino_cb(NULL),
    pwm_cb(NULL),
    robot_vel_cb(NULL),
    param_cb(NULL),
    dribbler_cb(NULL),
    kick_cb(NULL),
    reset_stm_cb(NULL),
    read_param_cb(nullptr),
    timer(new QTimer(this)),
    elapsed_timer(new QElapsedTimer),
    plot_mode(plot_ve_time),
    robot_monitor(new RobotMonitorDialog(this)),
    field_widget(new FieldWidget),
    proxy_field(new QGraphicsProxyWidget(0,Qt::Window)),
    field_scene(new QGraphicsScene(this)),
    robot_vel_scene(new QGraphicsScene(this)),
    d(false),
    offset_cmps_cb(NULL),
    STMConnection(true),
    ArduinoConnection(true)
{
    ui->setupUi(this);
    ui->plot_combo_box->addItem(QString("VE(Raw)"));
    ui->plot_combo_box->addItem(QString("VE(rad/s)"));
    ui->plot_combo_box->addItem(QString("VE x PWM"));
    ui->plot_combo_box->addItem(QString("Controller"));
    updateSerialInfo();

    this->setWindowTitle("Serial Communication");

    ve1_time_graph = ui->plot_widget->addGraph();
    ve2_time_graph = ui->plot_widget->addGraph();
    ve3_time_graph = ui->plot_widget->addGraph();
    ve4_time_graph = ui->plot_widget->addGraph();
    ve5_time_graph = ui->plot_widget->addGraph();

    ve1_time_graph->setPen(QPen(Qt::red));
    ve2_time_graph->setPen(QPen(Qt::blue));
    ve3_time_graph->setPen(QPen(Qt::green));
    ve4_time_graph->setPen(QPen(Qt::gray));
    ve5_time_graph->setPen(QPen(Qt::black));

    ve1_rad_time_graph = ui->plot_widget->addGraph();
    ve2_rad_time_graph = ui->plot_widget->addGraph();
    ve3_rad_time_graph = ui->plot_widget->addGraph();
    ve4_rad_time_graph = ui->plot_widget->addGraph();
    ve5_rad_time_graph = ui->plot_widget->addGraph();

    ve1_rad_time_graph->setPen(QPen(Qt::red));
    ve2_rad_time_graph->setPen(QPen(Qt::blue));
    ve3_rad_time_graph->setPen(QPen(Qt::green));
    ve4_rad_time_graph->setPen(QPen(Qt::gray));
    ve5_rad_time_graph->setPen(QPen(Qt::black));

    pwm1_graph = ui->plot_widget->addGraph();
    pwm2_graph = ui->plot_widget->addGraph();
    pwm3_graph = ui->plot_widget->addGraph();
    pwm4_graph = ui->plot_widget->addGraph();
    pwm5_graph = ui->plot_widget->addGraph();

    pwm1_graph->setPen(QPen(Qt::red,1.0,Qt::DotLine));
    pwm2_graph->setPen(QPen(Qt::blue,1.0,Qt::DotLine));
    pwm3_graph->setPen(QPen(Qt::green,1.0,Qt::DotLine));
    pwm4_graph->setPen(QPen(Qt::gray,1.0,Qt::DotLine));
    pwm5_graph->setPen(QPen(Qt::black,1.0,Qt::DotLine));

    ve1_setpoint_graph = ui->plot_widget->addGraph();
    ve2_setpoint_graph = ui->plot_widget->addGraph();
    ve3_setpoint_graph = ui->plot_widget->addGraph();

    ve1_setpoint_graph->setPen(QPen(Qt::red,1.0,Qt::DashDotLine));
    ve2_setpoint_graph->setPen(QPen(Qt::blue,1.0,Qt::DashDotLine));
    ve3_setpoint_graph->setPen(QPen(Qt::green,1.0,Qt::DashDotLine));

    auto list = QGamepadManager::instance();
    int idx = 0;
    if(list->connectedGamepads().size())
        idx = list->connectedGamepads().at(0);

    gamepad = new QGamepad(idx,this);
    connect(gamepad,SIGNAL(axisLeftXChanged(double)),this,SLOT(processGamepad()));
    connect(gamepad,SIGNAL(axisLeftYChanged(double)),this,SLOT(processGamepad()));
    connect(gamepad,SIGNAL(axisRightYChanged(double)),this,SLOT(processGamepadRotate()));
    connect(gamepad,SIGNAL(buttonAChanged(bool)),this,SLOT(kick()));
    connect(gamepad,SIGNAL(buttonBChanged(bool)),this,SLOT(processGamepadButton()));
    connect(gamepad,SIGNAL(buttonUpChanged(bool)),this,SLOT(processGamepadButton()));
    connect(gamepad,SIGNAL(buttonLeftChanged(bool)),this,SLOT(processGamepadButton()));
    connect(gamepad,SIGNAL(buttonDownChanged(bool)),this,SLOT(processGamepadButton()));
    connect(gamepad,SIGNAL(buttonRightChanged(bool)),this,SLOT(processGamepadButton()));
    connect(gamepad,SIGNAL(buttonSelectChanged(bool)),this,SLOT(toggleDribbler()));
    connect(gamepad,SIGNAL(buttonStartChanged(bool)),this,SLOT(toggleDribblerOut()));


    connect(ui->refresh_button,SIGNAL(clicked(bool)),this,SLOT(updateSerialInfo()));
    connect(ui->connect_button,SIGNAL(clicked(bool)),this,SLOT(connectSerial()));
    connect(ui->connect_arduino_button,SIGNAL(clicked(bool)),this,SLOT(connectArduino()));
    connect(ui->reset_button,SIGNAL(clicked(bool)),this,SLOT(resetPWM()));
    connect(ui->in_button,SIGNAL(toggled(bool)),this,SLOT(setDribblerIn(bool)));
    connect(ui->out_button,SIGNAL(toggled(bool)),this,SLOT(setDribblerOut(bool)));

    connect(ui->ff1_scroll,SIGNAL(valueChanged(int)),this,SLOT(updatePWM()));
    connect(ui->ff2_scroll,SIGNAL(valueChanged(int)),this,SLOT(updatePWM()));
    connect(ui->ff3_scroll,SIGNAL(valueChanged(int)),this,SLOT(updatePWM()));
    connect(ui->ff4_scroll,SIGNAL(valueChanged(int)),this,SLOT(updatePWM()));
    connect(ui->ff5_scroll,SIGNAL(valueChanged(int)),this,SLOT(updatePWM()));
    connect(ui->kick_scroll,SIGNAL(valueChanged(int)),this,SLOT(updatePWM()));

    connect(ui->reset_stm_button,&QPushButton::clicked,[&]{
        std::cout << "[MainWindow] resetSTM()\n";
        if(reset_stm_cb)
            reset_stm_cb();
    });

    connect(ui->u_button,SIGNAL(pressed()),this,SLOT(setRobotVelU()));
    connect(ui->d_button,SIGNAL(pressed()),this,SLOT(setRobotVelD()));
    connect(ui->l_button,SIGNAL(pressed()),this,SLOT(setRobotVelL()));
    connect(ui->r_button,SIGNAL(pressed()),this,SLOT(setRobotVelR()));
    connect(ui->cw_button,SIGNAL(pressed()),this,SLOT(setRobotVelCW()));
    connect(ui->ccw_button,SIGNAL(pressed()),this,SLOT(setRobotVelCCW()));
    connect(ui->u_button,SIGNAL(released()),this,SLOT(resetRobotVel()));
    connect(ui->d_button,SIGNAL(released()),this,SLOT(resetRobotVel()));
    connect(ui->l_button,SIGNAL(released()),this,SLOT(resetRobotVel()));
    connect(ui->r_button,SIGNAL(released()),this,SLOT(resetRobotVel()));
    connect(ui->cw_button,SIGNAL(released()),this,SLOT(resetRobotVel()));
    connect(ui->ccw_button,SIGNAL(released()),this,SLOT(resetRobotVel()));
    connect(ui->kick_button,SIGNAL(clicked(bool)),this,SLOT(kick()));
    connect(ui->motor1_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParam()));
    connect(ui->motor2_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParam()));
    connect(ui->motor3_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParam()));
    connect(ui->motor4_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParam()));
    connect(ui->motor1_step_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParam()));
    connect(ui->motor2_step_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParam()));
    connect(ui->motor3_step_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParam()));
    connect(ui->motor4_step_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParam()));
    connect(ui->motor1_ccw_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParamCCW()));
    connect(ui->motor2_ccw_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParamCCW()));
    connect(ui->motor3_ccw_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParamCCW()));
    connect(ui->motor4_ccw_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParamCCW()));
    connect(ui->motor1_ccw_step_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParamCCW()));
    connect(ui->motor2_ccw_step_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParamCCW()));
    connect(ui->motor3_ccw_step_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParamCCW()));
    connect(ui->motor4_ccw_step_spinbox,SIGNAL(valueChanged(double)),this,SLOT(updateMotorParamCCW()));
    connect(ui->action_Save_Settings,SIGNAL(triggered(bool)),this,SLOT(saveParam()));
    connect(ui->kp_motor1,SIGNAL(valueChanged(double)),this,SLOT(setPID()));
    connect(ui->kd_motor1,SIGNAL(valueChanged(double)),this,SLOT(setPID()));
    connect(ui->kp_motor2,SIGNAL(valueChanged(double)),this,SLOT(setPID()));
    connect(ui->kd_motor2,SIGNAL(valueChanged(double)),this,SLOT(setPID()));
    connect(ui->kp_motor3,SIGNAL(valueChanged(double)),this,SLOT(setPID()));
    connect(ui->kd_motor3,SIGNAL(valueChanged(double)),this,SLOT(setPID()));
    connect(ui->kp_motor4,SIGNAL(valueChanged(double)),this,SLOT(setPID()));
    connect(ui->kd_motor4,SIGNAL(valueChanged(double)),this,SLOT(setPID()));

    connect(ui->ff1_scroll,&QScrollBar::valueChanged,this,[=](int value)
    {
        ui->ff1_label->setText(QString("PWM 1 : %1").arg(value));
    });

    connect(ui->ff2_scroll,&QScrollBar::valueChanged,this,[=](int value)
    {
        ui->ff2_label->setText(QString("PWM 2 : %1").arg(value));
    });

    connect(ui->ff3_scroll,&QScrollBar::valueChanged,this,[=](int value)
    {
        ui->ff3_label->setText(QString("PWM 3 : %1").arg(value));
    });

    connect(ui->ff4_scroll,&QScrollBar::valueChanged,this,[=](int value)
    {
        ui->ff4_label->setText(QString("PWM 4 : %1").arg(value));
    });

    connect(ui->ff5_scroll,&QScrollBar::valueChanged,this,[=](int value)
    {
        ui->ff5_label->setText(QString("PWM DRIB : %1").arg(value));
    });

    connect(ui->kick_scroll,&QScrollBar::valueChanged,this,[=](int value)
    {
        ui->kick_label->setText(QString("KICK DEL: %1").arg(value));
    });

    connect(timer,SIGNAL(timeout()),this,SLOT(updatePlot()));

    QGraphicsProxyWidget *proxy_robot = new QGraphicsProxyWidget;
    field_scene->addPixmap(QPixmap("/home/fahri/Downloads/fukuro.jpg").scaledToHeight(500,Qt::SmoothTransformation));
    proxy_robot->setWidget(robot_monitor);
    proxy_field->setWidget(field_widget);
    elapsed_timer->start();
    if(!min_gui)
    {
        ui->graphicsView->setScene(new QGraphicsScene);
        ui->graphicsView->scene()->addItem(proxy_robot);
        ui->graphicsView->scale(0.8,0.8);
        ui->graphicsView_2->setScene(field_scene);
        ui->graphicsView_2->scene()->setBackgroundBrush(Qt::black);
        ui->graphicsView_2->scene()->addItem(proxy_field);
        ui->graphicsView_2->scale(0.7,0.7);
    }
    robot_vel_scene->addEllipse(QRectF(QPointF(-15.0,-15.0),QPointF(15.0,15.0)));
    robot_vel_scene->addEllipse(QRectF(QPointF(-30.0,-30.0),QPointF(30.0,30.0)));
    robot_vel_scene->addEllipse(QRectF(QPointF(-45.0,-45.0),QPointF(45.0,45.0)));
    robot_vel_scene->addEllipse(QRectF(QPointF(-60.0,-60.0),QPointF(60.0,60.0)));
    robot_vel_scene->addEllipse(QRectF(QPointF(-75.0,-75.0),QPointF(75.0,75.0)));
    robot_vel_scene->addEllipse(QRectF(QPointF(-100.0,-100.0),QPointF(100.0,100.0)));
    robot_vel_scene->addEllipse(QRectF(QPointF(-115.0,-115.0),QPointF(115.0,115.0)));

    //  ui->motor1_spinbox->setValue(50.0);
    //  ui->motor2_spinbox->setValue(50.0);
    //  ui->motor3_spinbox->setValue(50.0);

    ui->speed_combo_box->addItem(QString("0.25"));
    ui->speed_combo_box->addItem(QString("0.5"));
    ui->speed_combo_box->addItem(QString("0.75"));
    ui->speed_combo_box->addItem(QString("1.0"));
    ui->speed_combo_box->addItem(QString("1.25"));
    ui->speed_combo_box->addItem(QString("1.5"));

    connect(ui->speed_combo_box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateParam()));

    connect(ui->btnCmps,SIGNAL(clicked(bool)),this,SLOT(updateOffsetCmps()));

    ui->speed_combo_box->setCurrentIndex(3);
    isCompass = false;
    timer->start(100);
}

void MainWindow::setCompass(double cmps)
{
    robot_monitor->setCompass(cmps);
    ui->CompassRead->display(cmps);
    ui->CompassArrow->setArrow(cmps);
    ui->CompassArrow->update();
}

void MainWindow::kick()
{
    std::cout << "[MainWindow] kick()\n";
    if(kick_cb)
    {
        kick_cb(3);
    }
}
void MainWindow::resetSTM()
{
    std::cout << "[MainWindow] resetSTM()\n";
    if(reset_stm_cb)
        reset_stm_cb();
}

void MainWindow::updateParam()
{
    if(read_param_cb)
    {
        auto index = ui->speed_combo_box->currentIndex();
        auto param = read_param_cb(index);
        ui->motor1_spinbox->setValue(param[0]);
        ui->motor2_spinbox->setValue(param[1]);
        ui->motor3_spinbox->setValue(param[2]);
        ui->motor4_spinbox->setValue(param[3]);

        ui->motor1_ccw_spinbox->setValue(param[4]);
        ui->motor2_ccw_spinbox->setValue(param[5]);
        ui->motor3_ccw_spinbox->setValue(param[6]);
        ui->motor4_ccw_spinbox->setValue(param[7]);
    }
}

bool MainWindow::connectSerial(std::string port_name, int baud_rate)
{
    bool ret = false;
    if(connect_cb)
        ret = connect_cb(port_name);
    if(ret)
    {
        ui->connect_button->setText(QString("Disconnect"));
        STMConnection = true;
    }
    else{
        STMConnection = false;
    }
    return ret;
}

void MainWindow::readSettings()
{
    updateParam();
}

void MainWindow::toggleDribbler()
{
    qDebug() << "toggle dribbler";
    if(dribbler_cb)
    {
        if(d)
        {
            dribbler_cb(d,true);
            d = !d;
        }
        else
        {
            dribbler_cb(d,true);
            d = !d;
        }
    }
}

void MainWindow::toggleDribblerOut()
{
    qDebug() << "toggle dribbler out";
    if(dribbler_cb)
    {
        if(d)
        {
            dribbler_cb(d,false);
            d = !d;
        }
        else
        {
            dribbler_cb(d,false);
            d = !d;
        }
    }
}

void MainWindow::updateMotorParam()
{
    if(param_cb)
    {
        auto idx = ui->speed_combo_box->currentIndex();
        param_cb(1,ui->motor1_spinbox->value(),ui->motor1_step_spinbox->value(),true,idx);
        param_cb(2,ui->motor2_spinbox->value(),ui->motor2_step_spinbox->value(),true,idx);
        param_cb(3,ui->motor3_spinbox->value(),ui->motor3_step_spinbox->value(),true,idx);
        param_cb(4,ui->motor4_spinbox->value(),ui->motor4_step_spinbox->value(),true,idx);
    }
}

void MainWindow::updateMotorParamCCW()
{
    if(param_cb)
    {
        auto idx = ui->speed_combo_box->currentIndex();
        param_cb(1,ui->motor1_ccw_spinbox->value(),ui->motor1_ccw_step_spinbox->value(),false,idx);
        param_cb(2,ui->motor2_ccw_spinbox->value(),ui->motor2_ccw_step_spinbox->value(),false,idx);
        param_cb(3,ui->motor3_ccw_spinbox->value(),ui->motor3_ccw_step_spinbox->value(),false,idx);
        param_cb(4,ui->motor4_ccw_spinbox->value(),ui->motor4_ccw_step_spinbox->value(),false,idx);
    }
}

void MainWindow::setPID()
{
    if(pid_cb)
    {
        pid_cb(ui->kp_motor1->value(),ui->kd_motor1->value(),
               ui->kp_motor2->value(),ui->kd_motor2->value(),
               ui->kp_motor3->value(),ui->kd_motor3->value(),
               ui->kp_motor4->value(),ui->kd_motor4->value());
    }
}

void MainWindow::setPID(double kp1, double kd1, double kp2, double kd2, double kp3, double kd3, double kp4, double kd4)
{
    ui->kp_motor1->setValue(kp1);
    ui->kd_motor1->setValue(kd1);
    ui->kp_motor2->setValue(kp2);
    ui->kd_motor2->setValue(kd2);
    ui->kp_motor3->setValue(kp3);
    ui->kd_motor3->setValue(kd3);
    ui->kp_motor4->setValue(kp4);
    ui->kd_motor4->setValue(kd4);
}

void MainWindow::processGamepad()
{
    //    qDebug() << "process gamepad";
    if(robot_vel_cb)
    {
        robot_vel_cb(-gamepad->axisLeftY(),-gamepad->axisLeftX(),0.0);
    }
}

void MainWindow::processGamepadRotate()
{
    if(robot_vel_cb)
    {
        robot_vel_cb(0.0,0.0,(gamepad-> axisRightY())*3.14);
    }
}

void MainWindow::processGamepadButton()
{
#if 1
  auto speed = (ui->speed_combo_box->currentIndex()+1)*0.25;
  if(gamepad->buttonUp())
    processSpeed(speed,0.0,0.0);
  else if(gamepad->buttonDown())
    processSpeed(-speed,0.0,0.0);
  else if(gamepad->buttonLeft())
    processSpeed(0.0,speed,0.0);
  else if(gamepad->buttonRight())
    processSpeed(0.0,-speed,0.0);
  else
    processSpeed(0.0,0.0,0.0);
  publish();
#else
    if(robot_vel_cb)
    {
        auto speed = (ui->speed_combo_box->currentIndex()+1)*0.25;
        if(gamepad->buttonUp())
            robot_vel_cb(speed,0.0,0.0);
        else if(gamepad->buttonDown())
            robot_vel_cb(-speed,0.0,0.0);
        else if(gamepad->buttonLeft())
            robot_vel_cb(0.0,speed,0.0);
        else if(gamepad->buttonRight())
            robot_vel_cb(0.0,-speed,0.0);
        else
            robot_vel_cb(0.0,0.0,0.0);
#if 0
        if(gamepad->buttonUp())
            robot_vel_cb((ui->testVelSpinBox->value()),0.0,0.0);
        else if(gamepad->buttonDown())
            robot_vel_cb(-(ui->testVelSpinBox->value()),0.0,0.0);
        else if(gamepad->buttonLeft())
            robot_vel_cb(0.0,(ui->testVelSpinBox->value()),0.0);
        else if(gamepad->buttonRight())
            robot_vel_cb(0.0,-(ui->testVelSpinBox->value()),0.0);
        //else if(gamepad->buttonA())
        // robot_vel_cb(0.0,(ui->testVelSpinBox->value()),0.0);
        // else if(gamepad->buttonB())
        //robot_vel_cb(0.0,0.0,3.14);
        else
            robot_vel_cb(0.0,0.0,0.0);
#endif
    }
#endif
}

void MainWindow::setRobotVelCallback(RobotVelCallback cb)
{
    robot_vel_cb = cb;
}

void MainWindow::updateRobotVel(double vx, double vy, double w)
{
    robot_monitor->setVelocity(vx,vy,w);
    //    qDebug() << "update vel" << vx << vy << w;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setRobotVelU()
{
    if(robot_vel_cb)
        robot_vel_cb((ui->testVelSpinBox->value()),0.0,0.0);
}

void MainWindow::setRobotVelD()
{
    if(robot_vel_cb)
        robot_vel_cb(-(ui->testVelSpinBox->value()),0.0,0.0);
}

void MainWindow::setRobotVelL()
{
    if(robot_vel_cb)
        robot_vel_cb(0.0,(ui->testVelSpinBox->value()),0.0);
}

void MainWindow::setRobotVelR()
{
    if(robot_vel_cb)
        robot_vel_cb(0.0,-(ui->testVelSpinBox->value()),0.0);
}

void MainWindow::setRobotVelCW()
{
    if(robot_vel_cb)
        robot_vel_cb(0.0,0.0,(ui->testVelSpinBox->value()));
}

void MainWindow::setRobotVelCCW()
{
    if(robot_vel_cb)
        robot_vel_cb(0.0,0.0,-(ui->testVelSpinBox->value()));
}

void MainWindow::resetRobotVel()
{
    if(robot_vel_cb)
        robot_vel_cb(0.0,0.0,0.0);
}

void MainWindow::resetPWM()
{
    ui->ff1_scroll->setValue(0);
    ui->ff2_scroll->setValue(0);
    ui->ff3_scroll->setValue(0);
    ui->ff4_scroll->setValue(0);
    ui->ff5_scroll->setValue(0);
}

void MainWindow::updatePlot()
{
    double t_now = elapsed_timer->elapsed()/1000.0;
    switch(plot_mode)
    {
    case plot_ve_time:
        //        qDebug() << "[MainWindow] update plot vt time\n";

        ve1_rad_time_graph->setVisible(false);
        ve2_rad_time_graph->setVisible(false);
        ve3_rad_time_graph->setVisible(false);
        ve4_rad_time_graph->setVisible(false);
        ve5_rad_time_graph->setVisible(false);

        ve1_time_graph->setVisible(true);
        ve2_time_graph->setVisible(true);
        ve3_time_graph->setVisible(true);
        ve4_time_graph->setVisible(true);
        ve5_time_graph->setVisible(true);

        pwm1_graph->setVisible(false);
        pwm2_graph->setVisible(false);
        pwm3_graph->setVisible(false);
        pwm4_graph->setVisible(false);
        pwm5_graph->setVisible(false);

        ve1_setpoint_graph->setVisible(false);
        ve2_setpoint_graph->setVisible(false);
        ve3_setpoint_graph->setVisible(false);

        if(ui->plot_combo_box->currentText()==QString("VE(rad/s)"))
        {
            plot_mode = plot_ve_rad_time;
        }
        else if(ui->plot_combo_box->currentText()==QString("VE x PWM"))
        {
            plot_mode = plot_pwm_ve_time;
        }
        else if(ui->plot_combo_box->currentText()==QString("Controller"))
        {
            plot_mode = plot_control;
        }
        break;
    case plot_ve_rad_time:

        ve1_time_graph->setVisible(false);
        ve2_time_graph->setVisible(false);
        ve3_time_graph->setVisible(false);
        ve4_time_graph->setVisible(false);
        ve5_time_graph->setVisible(false);

        ve1_rad_time_graph->setVisible(true);
        ve2_rad_time_graph->setVisible(true);
        ve3_rad_time_graph->setVisible(true);
        ve4_rad_time_graph->setVisible(true);
        ve5_rad_time_graph->setVisible(true);

        pwm1_graph->setVisible(false);
        pwm2_graph->setVisible(false);
        pwm3_graph->setVisible(false);
        pwm4_graph->setVisible(false);
        pwm5_graph->setVisible(false);

        ve1_setpoint_graph->setVisible(false);
        ve2_setpoint_graph->setVisible(false);
        ve3_setpoint_graph->setVisible(false);

        if(ui->plot_combo_box->currentText()==QString("VE(Raw)"))
        {
            plot_mode = plot_ve_time;
        }
        else if(ui->plot_combo_box->currentText()==QString("VE x PWM"))
        {
            plot_mode = plot_pwm_ve_time;
        }
        else if(ui->plot_combo_box->currentText()==QString("Controller"))
        {
            plot_mode = plot_control;
        }
        break;
    case plot_pwm_ve_time:
        pwm1_graph->setVisible(true);
        pwm2_graph->setVisible(true);
        pwm3_graph->setVisible(true);
        pwm4_graph->setVisible(true);
        pwm5_graph->setVisible(true);

        ve1_rad_time_graph->setVisible(true);
        ve2_rad_time_graph->setVisible(true);
        ve3_rad_time_graph->setVisible(true);
        ve4_rad_time_graph->setVisible(true);
        ve5_rad_time_graph->setVisible(true);

        ve1_time_graph->setVisible(false);
        ve2_time_graph->setVisible(false);
        ve3_time_graph->setVisible(false);
        ve4_time_graph->setVisible(false);
        ve5_time_graph->setVisible(false);

        ve1_setpoint_graph->setVisible(false);
        ve2_setpoint_graph->setVisible(false);
        ve3_setpoint_graph->setVisible(false);

        if(ui->plot_combo_box->currentText()==QString("VE(Raw)"))
        {
            plot_mode = plot_ve_time;
        }
        else if(ui->plot_combo_box->currentText()==QString("VE(rad/s)"))
        {
            plot_mode = plot_ve_rad_time;
        }
        else if(ui->plot_combo_box->currentText()==QString("Controller"))
        {
            plot_mode = plot_control;
        }
    case plot_control:
        pwm1_graph->setVisible(false);
        pwm2_graph->setVisible(false);
        pwm3_graph->setVisible(false);
        pwm4_graph->setVisible(false);
        pwm5_graph->setVisible(false);

        ve1_rad_time_graph->setVisible(false);
        ve2_rad_time_graph->setVisible(false);
        ve3_rad_time_graph->setVisible(false);
        ve4_rad_time_graph->setVisible(false);
        ve5_rad_time_graph->setVisible(false);

        ve1_time_graph->setVisible(true);
        ve2_time_graph->setVisible(true);
        ve3_time_graph->setVisible(true);
        ve4_time_graph->setVisible(true);
        ve5_time_graph->setVisible(true);

        ve1_setpoint_graph->setVisible(true);
        ve2_setpoint_graph->setVisible(true);
        ve3_setpoint_graph->setVisible(true);

        if(ui->plot_combo_box->currentText()==QString("VE(Raw)"))
        {
            plot_mode = plot_ve_time;
        }
        else if(ui->plot_combo_box->currentText()==QString("VE(rad/s)"))
        {
            plot_mode = plot_ve_rad_time;
        }
        else if(ui->plot_combo_box->currentText()==QString("VE x PWM"))
        {
            plot_mode = plot_pwm_ve_time;
        }
    default:
        break;
    }

    ve1_time_graph->removeDataBefore(t_now-5.0);
    ve2_time_graph->removeDataBefore(t_now-5.0);
    ve3_time_graph->removeDataBefore(t_now-5.0);
    ve4_time_graph->removeDataBefore(t_now-5.0);
    ve5_time_graph->removeDataBefore(t_now-5.0);
    ve1_rad_time_graph->removeDataBefore(t_now-5.0);
    ve2_rad_time_graph->removeDataBefore(t_now-5.0);
    ve3_rad_time_graph->removeDataBefore(t_now-5.0);
    ve4_rad_time_graph->removeDataBefore(t_now-5.0);
    ve5_rad_time_graph->removeDataBefore(t_now-5.0);
    pwm1_graph->removeDataBefore(t_now-5.0);
    pwm2_graph->removeDataBefore(t_now-5.0);
    pwm3_graph->removeDataBefore(t_now-5.0);
    pwm4_graph->removeDataBefore(t_now-5.0);
    pwm5_graph->removeDataBefore(t_now-5.0);
    ve1_setpoint_graph->removeDataBefore(t_now-5.0);
    ve2_setpoint_graph->removeDataBefore(t_now-5.0);
    ve3_setpoint_graph->removeDataBefore(t_now-5.0);
    ui->plot_widget->rescaleAxes(true);
    ui->plot_widget->replot();
    ui->plot_widget->update();

    if(!ros::ok())
        this->close();
}

void MainWindow::updateRawValue(int ve1, int ve2, int ve3, int ve4, int ve5, bool ir, double dt)
{
    //    std::cout << "[MainWindow] update Raw\n";
    ui->raw_ve1_line->setText(QString::number(ve1));
    ui->raw_ve2_line->setText(QString::number(ve2));
    ui->raw_ve3_line->setText(QString::number(ve3));
    ui->raw_ve4_line->setText(QString::number(ve4));
    ui->raw_ve5_line->setText(QString::number(ve5));
    ui->raw_ir_line->setText(QString::number(ir?1:0));

    auto t_now = elapsed_timer->elapsed()/1000.0;
    ve1_time_graph->addData(t_now,ve1);
    ve2_time_graph->addData(t_now,ve2);
    ve3_time_graph->addData(t_now,ve3);
    ve4_time_graph->addData(t_now,ve4);
    ve5_time_graph->addData(t_now,ve5);

    //    this->update();
}

void MainWindow::updateControl(int ve_set1, int ve_set2, int ve_set3, int ve1, int ve2, int ve3)
{
    auto t_now = elapsed_timer->elapsed()/1000.0;
    ve1_setpoint_graph->addData(t_now,ve_set1);
    ve1_setpoint_graph->addData(t_now,ve_set2);
    ve1_setpoint_graph->addData(t_now,ve_set3);
}

void MainWindow::updateValue(double ve1, double ve2, double ve3, double ve4, double ve5, bool ir, double dt)
{
    //    std::cout << "[MainWindow] update Value\n";
    ui->ve1_line->setText(QString::number(ve1));
    ui->ve2_line->setText(QString::number(ve2));
    ui->ve3_line->setText(QString::number(ve3));
    ui->ve4_line->setText(QString::number(ve4));
    ui->ve5_line->setText(QString::number(ve5));
    ui->raw_ir_line->setText(QString::number((ir?1:0)));
    ui->dt_line->setText(QString::number(dt));

    auto t_now = elapsed_timer->elapsed()/1000.0;
    ve1_rad_time_graph->addData(t_now,ve1);
    ve2_rad_time_graph->addData(t_now,ve2);
    ve3_rad_time_graph->addData(t_now,ve3);
    ve4_rad_time_graph->addData(t_now,ve4);
    ve5_rad_time_graph->addData(t_now,ve5);

    //    this->update();
}

void MainWindow::updatePWMValue(int f1, int f2, int f3, int f4, int f5, int kick)
{
    auto t_now = elapsed_timer->elapsed()/1000.0;
    pwm1_graph->addData(t_now,f1);
    pwm2_graph->addData(t_now,f2);
    pwm3_graph->addData(t_now,f3);
    pwm4_graph->addData(t_now,f4);
    pwm5_graph->addData(t_now,f5);
}

void MainWindow::connectSerial()
{
    QString port_nucleo = serial_port_list.at(ui->comboBox->currentIndex());
    QString port_arduino = serial_port_list.at(ui->comboBox_2->currentIndex());
    std::cout << port_nucleo.toStdString() << ", " << port_arduino.toStdString() << '\n';
    if(ui->connect_button->text()==QString("Connect"))
    {
        if(connect_cb)
        {
            if(connect_cb(port_nucleo.toStdString()))
            {
                ui->connect_button->setText(QString("Disconnect"));
                STMConnection = false;
            }
            else{
                STMConnection = true;
            }
        }
    }
    else
    {
        if(disconnect_cb)
        {
            if(disconnect_cb())
            {
                STMConnection = true;
                ui->connect_button->setText(QString("Connect"));
            }
        }
    }
}

void MainWindow::connectSerial(int index)
{
    QString port_nucleo = serial_port_list.at(index);
    ui->comboBox->setCurrentIndex(index);
    emit ui->connect_button->clicked(true);
    //QString port_arduino = serial_port_list.at(ui->comboBox_2->currentIndex());
    //std::cout << port_nucleo.toStdString() << ", " << port_arduino.toStdString() << '\n';
    /*
    if(ui->connect_button->text()==QString("Connect"))
    {
        if(connect_cb)
        {
            if(connect_cb(port_nucleo.toStdString()))
            {
                ui->connect_button->setText(QString("Disconnect"));
                STMConnection = true;
            }
            else{
                STMConnection = false;
            }
        }
    }
    else
    {
        if(disconnect_cb)
        {
            if(disconnect_cb())
            {
                STMConnection = false;
                ui->connect_button->setText(QString("Connect"));
            }
        }
    }
    */
}

void MainWindow::connectArduino()
{
    QString port_nucleo = serial_port_list.at(ui->comboBox->currentIndex());
    QString port_arduino = serial_port_list.at(ui->comboBox_2->currentIndex());
    std::cout << port_nucleo.toStdString() << ", " << port_arduino.toStdString() << '\n';
    if(ui->connect_arduino_button->text()==QString("Connect Arduino"))
    {
        if(connect_arduino_cb)
        {
            if(connect_arduino_cb(port_arduino.toStdString()))
            {
                ui->connect_arduino_button->setText(QString("Disconnect Arduino"));
                ArduinoConnection = false;
            }
            else{
                ArduinoConnection = true;
            }
        }
    }
    else
    {
        if(disconnect_arduino_cb)
        {
            if(disconnect_arduino_cb())
            {
                ArduinoConnection = true;
                ui->connect_arduino_button->setText(QString("Connect Arduino"));
            }
        }
    }
}

void MainWindow::connectArduino(int index)
{
    //QString port_nucleo = serial_port_list.at(ui->comboBox->currentIndex());
    QString port_arduino = serial_port_list.at(index);
    ui->comboBox_2->setCurrentIndex(index);
    emit ui->connect_arduino_button->clicked(true);
    //std::cout << port_nucleo.toStdString() << ", " << port_arduino.toStdString() << '\n';
    //if(ui->connect_arduino_button->text()==QString("Connect Arduino"))
    //{
        //if(connect_arduino_cb)
        //{
            //if(connect_arduino_cb(port_arduino.toStdString()))
            //{
                //ui->connect_arduino_button->setText(QString("Disconnect Arduino"));
                //ArduinoConnection = true;
            //}
            //else{
                //ArduinoConnection = false;
            //}
        //}
    //}
    //else
    //{
        //if(disconnect_arduino_cb)
        //{
            //if(disconnect_arduino_cb())
            //{
                //ArduinoConnection = false;
                //ui->connect_arduino_button->setText(QString("Connect Arduino"));
            //}
        //}
    //}
}

void MainWindow::updateSerialInfo()
{
    ui->comboBox->clear();
    ui->comboBox_2->clear();
    serial_port_list.clear();
    manufacturer_list.clear();
    for(auto s : QSerialPortInfo::availablePorts())
    {
        ui->comboBox->addItem(s.portName()+QString(" : ")+s.manufacturer());
        ui->comboBox_2->addItem(s.portName()+QString(" : ")+s.manufacturer());
        serial_port_list.push_back(s.portName());
        manufacturer_list.push_back(s.manufacturer());
        std::cout << s.portName().toStdString() << " : "
                  << s.description().toStdString() << '\n'
                  << "manufacturer : "
                  << s.manufacturer().toStdString() << '\n'
                  << "serial number : "
                  << s.serialNumber().toStdString() << '\n';
    }
    std::cout << "Standard Baud Rates : \n";
}

void MainWindow::updatePWM()
{
    pwm_cb((ui->ff1_scroll->value()),
           (ui->ff2_scroll->value()),
           (ui->ff3_scroll->value()),
           (ui->ff4_scroll->value()),
           (ui->ff5_scroll->value()),
           (ui->kick_scroll->value()));
}

void MainWindow::updateOdometry(double px, double py, double theta, double vx, double vy, double w)
{
    static int count = 0;
    count++;
    if(count==10)
    {
        ui->textEdit->append(QString("Odometry : (%1,%2,%3) (%4,%5,%6)")
                             .arg(px)
                             .arg(py)
                             .arg(theta)
                             .arg(vx)
                             .arg(vy)
                             .arg(w));
        count = 0;
    }
    field_widget->updateField(px,py,theta);
    field_widget->update();
}

void MainWindow::setDribblerIn(bool d)
{
    if(d)
    {
        if(dribbler_cb)
        {
            dribbler_cb(true,true);
        }

    }
    else
    {
        if(dribbler_cb)
        {
            dribbler_cb(false,false);
        }
    }
}

void MainWindow::setDribblerOut(bool d)
{
    if(d)
    {
        if(dribbler_cb)
        {
            dribbler_cb(true,false);
        }

    }
    else
    {
        if(dribbler_cb)
        {
            dribbler_cb(false,false);
        }
    }
}

void MainWindow::saveParam()
{
    if(save_cb)
        save_cb({ui->motor1_spinbox->value(),
                 ui->motor2_spinbox->value(),
                 ui->motor3_spinbox->value(),
                 ui->motor4_spinbox->value()},
        {ui->motor1_step_spinbox->value(),
         ui->motor2_step_spinbox->value(),
         ui->motor3_step_spinbox->value(),
         ui->motor4_step_spinbox->value()},
        {ui->motor1_ccw_spinbox->value(),
         ui->motor2_ccw_spinbox->value(),
         ui->motor3_ccw_spinbox->value(),
         ui->motor4_ccw_spinbox->value()},
        {ui->motor1_ccw_step_spinbox->value(),
         ui->motor2_ccw_step_spinbox->value(),
         ui->motor3_ccw_step_spinbox->value(),
         ui->motor4_ccw_step_spinbox->value()},
        {ui->kp_motor1->value(),
         ui->kp_motor2->value(),
         ui->kp_motor3->value(),
         ui->kp_motor4->value()},
        {ui->kd_motor1->value(),
         ui->kd_motor2->value(),
         ui->kd_motor3->value(),
         ui->kd_motor4->value()});
}

void MainWindow::setParam(std::vector<double> cw, std::vector<double> cw_step, std::vector<double> ccw, std::vector<double> ccw_step)
{
    if(cw.size()==4)
    {
        ui->motor1_spinbox->setValue(cw[0]);
        ui->motor2_spinbox->setValue(cw[1]);
        ui->motor3_spinbox->setValue(cw[2]);
        ui->motor4_spinbox->setValue(cw[3]);
    }
    if(cw_step.size()==4)
    {
        ui->motor1_step_spinbox->setValue(cw_step[0]);
        ui->motor2_step_spinbox->setValue(cw_step[1]);
        ui->motor3_step_spinbox->setValue(cw_step[2]);
        ui->motor4_step_spinbox->setValue(cw_step[3]);
    }
    if(ccw.size()==4)
    {
        ui->motor1_ccw_spinbox->setValue(ccw[0]);
        ui->motor2_ccw_spinbox->setValue(ccw[1]);
        ui->motor3_ccw_spinbox->setValue(ccw[2]);
        ui->motor4_ccw_spinbox->setValue(ccw[3]);
    }
    if(ccw_step.size()==4)
    {
        ui->motor1_ccw_step_spinbox->setValue(ccw_step[0]);
        ui->motor2_ccw_step_spinbox->setValue(ccw_step[1]);
        ui->motor3_ccw_step_spinbox->setValue(ccw_step[2]);
        ui->motor4_ccw_step_spinbox->setValue(ccw_step[3]);
    }
}
void MainWindow::updateOffsetCmps()
{
    isCompass = !isCompass;
    if(isCompass){
        ui->btnCmps->setText(QString("Stop"));
        if(offset_cmps_cb)
            offset_cmps_cb();
    }
    else
        ui->btnCmps->setText(QString("Start"));
}