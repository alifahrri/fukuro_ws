#ifndef PATHITEM_H
#define PATHITEM_H

#include <QGraphicsItem>

class PathItem : public QGraphicsItem
{
public:
  typedef std::pair<double,double> Pos2D;
  typedef std::vector<Pos2D> Pos2DList;
  PathItem();
  void setPath(Pos2DList p) { path = p; }
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
  QRectF boundingRect() const Q_DECL_OVERRIDE;

private:
  Pos2DList path;
};

#endif // PATHITEM_H
