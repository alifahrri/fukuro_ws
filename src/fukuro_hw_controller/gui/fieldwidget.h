#ifndef FIELDWIDGET_H
#define FIELDWIDGET_H

//#define USE_ROS

#include <QWidget>
#ifdef USE_ROS
#include <fukuro_common/WorldModel.h>
#endif

namespace Ui {
class FieldWidget;
}

class QGraphicsScene;
class RobotItem;
class FieldItem;
class BallItem;
class ObstacleItem;
class PathItem;

class FieldWidget : public QWidget
{
  Q_OBJECT

public:
  explicit FieldWidget(QWidget *parent = 0);
#ifdef USE_ROS
  void updateField(fukuro_common::WorldModel wm);
#else
    void updateField(double px, double py, double theta);
#endif
  ~FieldWidget();

private:
  Ui::FieldWidget *ui;
  QGraphicsScene *scene;
  FieldItem *field;
  BallItem *balls;
  ObstacleItem *obstacles;
  RobotItem *robot;
  PathItem *path;
};

#endif // FIELDWIDGET_H
