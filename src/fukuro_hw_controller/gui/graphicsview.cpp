#include "graphicsview.h"
#include <QDebug>
#include <QMouseEvent>

GraphicsView::GraphicsView() :
    QGraphicsView::QGraphicsView(),
    mouse_cb(NULL)
{

}

GraphicsView::GraphicsView(QWidget *parent) :
    QGraphicsView::QGraphicsView(parent),
    mouse_cb(NULL)
{

}

void GraphicsView::mousePressEvent(QMouseEvent *event)
{
    qDebug() << "[GraphcisView] mouse press :" << event->pos()
             << "size :" << this->rect().height() << this->rect().width()
             << "center :" << this->rect().center();
    if(mouse_cb)
    {
        QPointF vel = event->pos()-this->rect().center();
        auto length = this->rect().topLeft()-this->rect().center();
        vel/=(sqrt(length.rx()*length.rx()+length.ry()*length.ry()));
        mouse_cb(vel.rx(),vel.ry(),0.0);
    }
}

void GraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    qDebug() << "[GraphicsView] mouse move :" << event->pos()
             << "size :" << this->rect().height() << this->rect().width()
             << "center :" << this->rect().center();
    if(mouse_cb)
    {
        QPointF vel = event->pos()-this->rect().center();
        auto length = this->rect().topLeft()-this->rect().center();
        vel/=(sqrt(length.rx()*length.rx()+length.ry()*length.ry()));
        mouse_cb(vel.rx(),vel.ry(),0.0);
    }
}
