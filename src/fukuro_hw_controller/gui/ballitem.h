#ifndef BALLITEM_H
#define BALLITEM_H

#include <QGraphicsItem>

#define MULTIPLE_BALL

class BallItem : public QGraphicsItem
{
public:
  BallItem();
#ifdef MULTIPLE_BALL
  typedef std::pair<double,double> Pos2D;
  typedef std::vector<Pos2D> Pos2DList;
  void setBalls(Pos2DList b) { balls = b; }
#else
  void setPos(double x, double y) { px = x; py = y; }
#endif
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;
	
private:
#ifdef MULTIPLE_BALL
    Pos2DList balls;
#else
    double px, py;
#endif
};

#endif //BALLITEM_H
