#include "speeditem.h"
#include <sstream>
#include <QDebug>
#include <QKeyEvent>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

#define N_MOTOR 4
#define N_SPEED 6

#define COLORS ("127,64,192,255")

#define SPEED_X_OFFSET 50
#define X_LINE_OFFSET 160

#define NORMAL_RADIUS     5.0
#define SELECTION_RADIUS  6.5

SpeedItem::SpeedItem()
{
  cw_settings.resize(N_MOTOR);
  ccw_settings.resize(N_MOTOR);
  for(auto& cw : cw_settings)
    cw.resize(N_SPEED);
  for(auto &ccw : ccw_settings)
    ccw.resize(N_SPEED);

  std::stringstream ss(COLORS);
  int i;
  std::vector<int> color_vct;
  while(ss >> i)
  {
    color_vct.push_back(i);
    if(ss.peek() == ',')
      ss.ignore();
  }

  auto s = color_vct.size();
  for(size_t i=0; i<N_MOTOR; i++)
    colors.push_back(QColor(color_vct[i%s],color_vct[s-i%s],color_vct[i%s]));

  auto pt_offset = QPointF(0.0,X_LINE_OFFSET);
  for(size_t i=0; i<cw_settings.size(); i++)
    for(size_t j=0; j<cw_settings.at(i).size(); j++)
      cw_settings[i][j] = QPointF((j+1)*SPEED_X_OFFSET,-(100+(i*25.0)))+pt_offset;
  for(size_t i=0; i<ccw_settings.size(); i++)
    for(size_t j=0; j<ccw_settings.at(i).size(); j++)
      ccw_settings[i][j] = QPointF(-int((j+1)*SPEED_X_OFFSET),-(100+(i*25.0)))+pt_offset;

  this->setFlag(QGraphicsItem::ItemIsFocusable);
}

void SpeedItem::setMotor(const fukuro::MotorParameter &motor)
{
  for(size_t i=0; i<motor.linear_map_cw.size(); i++)
    for(size_t j=0; j<motor.linear_map_cw.at(i).size(); j++)
    {
      auto speed = static_cast<fukuro::speed_t>(j);
      auto v = motor.linear_map_cw.at(i).at(speed);
      cw_settings[i][j].setY(-v+X_LINE_OFFSET);
    }
  for(size_t i=0; i<motor.linear_map_ccw.size(); i++)
    for(size_t j=0; j<motor.linear_map_ccw.at(i).size(); j++)
    {
      auto speed = static_cast<fukuro::speed_t>(j);
      auto v = motor.linear_map_ccw.at(i).at(speed);
      ccw_settings[i][j].setY(-v+X_LINE_OFFSET);
    }
}

void SpeedItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->drawLine(QPointF(-320.0,0.0+X_LINE_OFFSET),QPointF(320.0,0.0+X_LINE_OFFSET));
  painter->drawLine(QPointF(0.0,-240.0),QPointF(0.0,240.0));
  painter->setFont(QFont("ubuntu"));
  painter->drawText(QPointF(-320,X_LINE_OFFSET+12),"Counter-Clock Wise");
  painter->drawText(QPointF(320-60,X_LINE_OFFSET+12),"Clock Wise");
  painter->drawText(QPointF(0.0-13,-220),"PWM");
  painter->setPen(Qt::DashLine);
  painter->drawLine(QPointF(-320.0,-255.0+X_LINE_OFFSET),QPointF(320.0,-255.0+X_LINE_OFFSET));
  painter->setPen(Qt::gray);
  painter->drawText(QPointF(0.0-13,-255.0+X_LINE_OFFSET),"255");
  auto i = 0;
  for(const auto& cw : cw_settings)
  {
      auto c = colors[i++%colors.size()];
      painter->setBrush(c);
      painter->setPen(c);
      for(const auto& s : cw)
        painter->drawEllipse(s,NORMAL_RADIUS,NORMAL_RADIUS);
      painter->drawPolyline(cw);
  }
  for(const auto& ccw : ccw_settings)
  {
      auto c = colors[i++%colors.size()];
      painter->setBrush(c);
      painter->setPen(c);
      for(const auto& s : ccw)
        painter->drawEllipse(s,NORMAL_RADIUS,NORMAL_RADIUS);
      painter->drawPolyline(ccw);
  }
  for(size_t i=0; i<N_MOTOR; i++)
  {
    auto c = colors[i];
    auto pt = QPointF(-300.0,-220.0+(int)i*12);
    auto offset = QPointF(10.0,5.0);
    auto width = (i==motor_select ? 2.0 : 1.0);
    auto italic = (i==motor_select ? true : false);
    auto size = (i==motor_select ? 12 : 10);
    auto rad = (i==motor_select ? 3.5 : 2.5);
    painter->setBrush(c);
    painter->setPen(c);
    painter->drawEllipse(pt,rad,rad);
    painter->setFont(QFont("ubuntu",size,width,italic));
    painter->setPen(Qt::black);
    painter->drawText(pt+offset,QString("motor %1").arg(i+1));
  }

  {
    auto cw = cw_settings.at(motor_select);
    auto ccw = ccw_settings.at(motor_select);
    auto c = colors[motor_select%colors.size()];

    painter->setBrush(c);
    painter->setPen(QPen(QBrush(c),3.0));

    for(const auto& s : cw)
      painter->drawEllipse(s,SELECTION_RADIUS,SELECTION_RADIUS);
    for(const auto& s : ccw)
      painter->drawEllipse(s,SELECTION_RADIUS,SELECTION_RADIUS);

    painter->drawPolyline(cw);
    painter->drawPolyline(ccw);
  }
}

QRectF SpeedItem::boundingRect() const
{
  return QRectF(-320.0,-240.0,640.0,480.0);
}

void SpeedItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  qDebug() << "mousepress";
  auto& cw = cw_settings[motor_select];
  auto& ccw = ccw_settings[motor_select];
  for(auto &p : cw)
  {
    auto d = event->pos() - p;
    if(d.manhattanLength() < 2*SELECTION_RADIUS)
    {
      motor_pt_select = &p;
      break;
    }
  }
  if(!motor_pt_select)
    for(auto &p : ccw)
    {
      auto d = event->pos() - p;
      if(d.manhattanLength() < 2*SELECTION_RADIUS)
      {
        motor_pt_select = &p;
        break;
      }
    }
}

void SpeedItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  qDebug() << "mousemove, selection :" << motor_pt_select;
  if(!motor_pt_select)
    return;
  motor_pt_select->setY(event->pos().y());
  this->update();
}

void SpeedItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
  motor_pt_select = nullptr;
  if(callback)
  {
      fukuro::MotorParameter motor;
      for(size_t i=0; i<motor.linear_map_cw.size(); i++)
        for(size_t j=0; j<motor.linear_map_cw.at(i).size(); j++)
        {
          auto speed = static_cast<fukuro::speed_t>(j);
          //          auto v = motor.linear_map_cw.at(i).at(speed);
          //          cw_settings[i][j].setY(-v+X_LINE_OFFSET);
          auto v = -(cw_settings[i][j].y()-X_LINE_OFFSET);
          motor.linear_map_cw.at(i).at(speed) = v;
        }
      for(size_t i=0; i<motor.linear_map_ccw.size(); i++)
        for(size_t j=0; j<motor.linear_map_ccw.at(i).size(); j++)
        {
          auto speed = static_cast<fukuro::speed_t>(j);
          //          auto v = motor.linear_map_ccw.at(i).at(speed);
          //          ccw_settings[i][j].setY(-v+X_LINE_OFFSET);
          auto v = -(ccw_settings[i][j].y()-X_LINE_OFFSET);
          motor.linear_map_ccw.at(i).at(speed) = v;
        }
      callback(motor);
  }
}

void SpeedItem::keyPressEvent(QKeyEvent *event)
{
  //  qDebug() << "keypress:" << event->key();
  switch(event->key())
  {
    case '1':
      motor_select = 0;
      break;
    case '2':
      motor_select = 1;
      break;
    case Qt::Key_3:
      motor_select = 2;
      break;
    case Qt::Key_4:
      motor_select = 3;
      break;
    default:
      break;
  }
  this->update();
}
