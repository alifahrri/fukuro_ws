#ifndef MAINWINDOWWRAPPER_H
#define MAINWINDOWWRAPPER_H

#include <QObject>

class MainWindowWrapper : public QObject
{
    Q_OBJECT
public:
    MainWindowWrapper();
signals:
    void updateVelocity(double,double,double);
    void updateRawValue(int,int,int,int,int,bool,double);
    void updateValue(double,double,double,double,double,bool,double);
    void updateOdometry(double,double,double,double,double,double);
    void updatePWM(int,int,int,int,int,int);
    void updateControl(int,int,int,int,int,int);
};

#endif // MAINWINDOWWRAPPER_H
