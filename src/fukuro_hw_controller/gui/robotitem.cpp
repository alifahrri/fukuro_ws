#include "robotitem.h"
#include <cmath>
#include <QPainter>

//#ifdef USE_ROBOTSTATE
#include "robotstate.h"
//#endif

#define PIXMAP_PATH "/home/fahri/fukuro_ws/src/fukuro_hw_controller/fukuro.png"

RobotItem::RobotItem() :
    draw_text(true),
    draw_arrow(true)
{
  char* user_name = std::getenv("USER");
  //  QString pixmap_path = QString("/home/") + QString(user_name) + QString("/fukuro_ws/src/fukuro_hw_controller/gui/fukuro.png");
  //  robot = QPixmap(pixmap_path)
  //      .scaledToHeight(47,Qt::SmoothTransformation).transformed(QTransform().rotate(-90));
  robot = QPixmap("://fukuro.png")
      .scaledToHeight(47,Qt::SmoothTransformation).transformed(QTransform().rotate(-90));
}

void RobotItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QPolygon v_arrow;
  QPainterPath p;
  QMatrix m0;
#ifdef USE_ROBOT_STATE
  if(!rstate)
    return;
  double xt = fukuro::pos(*rstate).x()*100;
  double yt = fukuro::pos(*rstate).y()*100;
  painter->translate(xt,-yt);
#else
  painter->translate(px*100,-py*100);
#endif
  painter->setPen(Qt::NoPen);
  painter->setBrush(Qt::blue);
  QMatrix m = painter->matrix();
  m0 = painter->matrix();
#ifdef USE_ROBOT_STATE
  if(length(vel(*rstate))>0.0) {
#define ARROW_WIDTH 7
    int arrow_length = 30 * fukuro::length(fukuro::vel(*rstate));
    v_arrow << QPoint(-ARROW_WIDTH/2,0) << QPoint(ARROW_WIDTH/2,0);
    v_arrow << QPoint(ARROW_WIDTH/2,arrow_length)
            << QPoint (ARROW_WIDTH,arrow_length)
            << QPoint(0,arrow_length+ARROW_WIDTH)
            << QPoint(-ARROW_WIDTH,arrow_length)
            << QPoint(-ARROW_WIDTH/2,arrow_length);
    v_arrow << QPoint(-ARROW_WIDTH/2,0);
    using fukuro::vel;
    painter->rotate(-90+atan2(-vel(*rstate).y(),vel(*rstate).x())*180/M_PI);
    painter->drawConvexPolygon(v_arrow);
  }
#else
  auto v_mag = sqrt(vx*vx+vy*vy);
  if(v_mag>0.0) {
#define ARROW_WIDTH 7
    int arrow_length = 30 * v_mag;
    v_arrow << QPoint(-ARROW_WIDTH/2,0) << QPoint(ARROW_WIDTH/2,0);
    v_arrow << QPoint(ARROW_WIDTH/2,arrow_length)
            << QPoint (ARROW_WIDTH,arrow_length)
            << QPoint(0,arrow_length+ARROW_WIDTH)
            << QPoint(-ARROW_WIDTH,arrow_length)
            << QPoint(-ARROW_WIDTH/2,arrow_length);
    v_arrow << QPoint(-ARROW_WIDTH/2,0);
    painter->rotate(-90+atan2(-vy,vx)*180/M_PI);
    if(draw_arrow)
        painter->drawConvexPolygon(v_arrow);
  }
#endif
  painter->setMatrix(m);
#ifdef USE_ROBOT_STATE
  painter->rotate(-fukuro::pos(*rstate).w()*180.0/M_PI);
  if(fabs(vel(*rstate).w())>0.0) {
    double arc_length = vel(*rstate).w()/M_PI_2*90.0;
    double d = 13.0;
    double c = cos(vel(*rstate).w());
    double s = -sin(vel(*rstate).w());
    double c1 = (vel(*rstate).w()>0) ?
          cos(vel(*rstate).w()+0.77) : cos(vel(*rstate).w()-0.77);
    double s1 = (vel(*rstate).w()>0) ?
          -sin(vel(*rstate).w()+0.77) : -sin(vel(*rstate).w()-0.77);
    p.moveTo(0,0);
    p.arcTo(QRectF(-16,-16,32,32),0,arc_length);
    p.closeSubpath();
    p.arcTo(QRectF(-10,-10,20,20),0,arc_length);
    p.closeSubpath();
    p.moveTo(d*c,d*s);
    p.lineTo((d+6)*c,(d+6)*s);
    p.lineTo((d+3)*c1,(d+3)*s1);
    p.lineTo((d-6)*c,(d-6)*s);
    p.closeSubpath();
  }
  auto text = QString("%1m/s\n%2rad/s").arg(QString::number(length(vel(*rstate)),'g',2)).arg(QString::number(vel(*rstate).w(),'g',2));
#else
  painter->rotate(-angle_rad*180.0/M_PI);
  if(fabs(vw)>0.0) {
    double arc_length = vw/M_PI_2*90.0;
    double d = 13.0;
    double c = cos(vw);
    double s = -sin(vw);
    double c1 = (vw>0) ?
          cos(vw+0.77) : cos(vw-0.77);
    double s1 = (vw>0) ?
          -sin(vw+0.77) : -sin(vw-0.77);
    p.moveTo(0,0);
    p.arcTo(QRectF(-16,-16,32,32),0,arc_length);
    p.closeSubpath();
    p.arcTo(QRectF(-10,-10,20,20),0,arc_length);
    p.closeSubpath();
    p.moveTo(d*c,d*s);
    p.lineTo((d+6)*c,(d+6)*s);
    p.lineTo((d+3)*c1,(d+3)*s1);
    p.lineTo((d-6)*c,(d-6)*s);
    p.closeSubpath();
  }
  auto text = QString("%1m/s\n%2rad/s").arg(QString::number(sqrt(vx*vx+vy*vy),'g',2)).arg(QString::number(vw,'g',2));
#endif
  painter->setBrush(Qt::red);
  painter->drawPixmap(-boundingRect().width()/2,-boundingRect().height()/2,robot);
  if(draw_arrow)
      painter->drawPath(p);
  painter->setMatrix(m0);
  painter->setPen(QColor(50,50,50));
  if(draw_text)
      painter->drawText(QRect(-10,17,200,200),text);
}

QRectF RobotItem::boundingRect() const {
  return QRectF(robot.rect());
}
