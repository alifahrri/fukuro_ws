#ifndef SPEEDITEM_H
#define SPEEDITEM_H

#include <QGraphicsItem>
#include <functional>
#include "motor.hpp"

class SpeedItem : public QGraphicsItem
{
  typedef QVector<QPointF> MotorPWM;
  typedef QVector<MotorPWM> MotorPWMs;
public:
  typedef std::function<void(const fukuro::MotorParameter&)> Callback;
public:
  Callback callback = nullptr;
public:
  SpeedItem();
  void setMotor(const fukuro::MotorParameter &motor);
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  QRectF boundingRect() const;
private:
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
  void keyPressEvent(QKeyEvent *event);
private:
  MotorPWMs cw_settings;
  MotorPWMs ccw_settings;
  QVector<QColor> colors;
  int motor_select = 0;
  QPointF *motor_pt_select = nullptr;
};

#endif // SPEEDITEM_H
