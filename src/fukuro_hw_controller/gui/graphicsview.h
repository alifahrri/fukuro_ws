#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>
#include <functional>
#include <cmath>

typedef std::function<void(double,double,double)> MouseCallback;

class GraphicsView : public QGraphicsView
{
public:
    GraphicsView();
    GraphicsView(QWidget *parent = NULL);
    void setMouseCallback(MouseCallback cb) { mouse_cb = cb; }
protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
private:
    MouseCallback mouse_cb;
};

#endif // GRAPHICSVIEW_H
