#include "ballitem.h"

#include <QPainter>

BallItem::BallItem()
{
	
}

void BallItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::NoPen);
  painter->setBrush(Qt::red);
#ifdef MULTIPLE_BALL
  auto m = painter->matrix();
  for(auto b : balls)
  {
    painter->setMatrix(m);
    painter->translate(b.first*100,-b.second*100);
    painter->drawEllipse(QRectF(-13,-13,26,26));
  }
#else
	painter->translate(px*100,py*100);
	painter->drawEllipse(QRectF(-13,-13,26,26));
#endif
}

QRectF BallItem::boundingRect() const 
{
#ifdef MULTIPLE_BALL
  return QRectF(-360,-240,720,480);
#else
	return QRectF(-13,-13,26,26);
#endif
}
