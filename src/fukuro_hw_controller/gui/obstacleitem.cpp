#include "obstacleitem.h"
#include <QPainter>

ObstacleItem::ObstacleItem()
{

}

void ObstacleItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    auto m = painter->matrix();
    painter->setPen(Qt::NoPen);
	painter->setBrush(Qt::gray);
    QRectF obs(-25,-25,50,50);
    for(auto o : obstacles) {
        painter->setMatrix(m);
        painter->translate(o.first*100,-o.second*100);
        painter->drawEllipse(obs);
    }
}

QRectF ObstacleItem::boundingRect() const {
    if(obstacles.size()==0)
        return QRectF();
    auto top_left = obstacles[0];
    // auto top_right = obstacles[0];
    // auto bottom_left = obstacles[0];
    auto bottom_right = obstacles[0];
    for(auto o : obstacles) {
        if(o.first < top_left.first && o.second > top_left.second) {
            top_left = o;
        }
        // else if(o.first > top_left.first && o.second > top_left.second) {
        //     top_right = o;
        // }
        // else if(o.first < top_left.first && o.second < top_left.second) {
        //     bottom_left = o;
        // }
        else if(o.first > top_left.first && o.second < top_left.second) {
            bottom_right = o;
        }
    }
	return QRectF(top_left.first*100,
                  -top_left.second*100,
                  (top_left.first-bottom_right.first)*100,
                  (top_left.second-bottom_right.second)*100);
}
