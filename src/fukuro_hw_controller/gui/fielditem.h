#ifndef FIELDITEM_H
#define FIELDITEM_H

#include <QGraphicsItem>

class FieldItem : public QGraphicsItem
{
public:
	FieldItem();
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;
	
	// /*
private:
	void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    // void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	// */
	
private:
	QPixmap lines;
	std::pair<double,double> click_pos;
};


#endif //FIELDITEM_H