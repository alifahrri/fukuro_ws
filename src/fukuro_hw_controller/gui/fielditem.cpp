#include "fielditem.h"
#include <ros/ros.h>
#include <iostream>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

#define PIXEL_WIDTH 900
#define PIXEL_HEIGHT 600

#define PIXMAP_PATH "/home/fahri/fukuro_ws/src/fukuro_hw_controller/lines.png"

FieldItem::FieldItem()
{
  char* user_name = std::getenv("USER");
//  QString pixmap_path = QString("/home/") + QString(user_name) + QString("/fukuro_ws/src/fukuro_hw_controller/gui/lines.png");
//  lines = QPixmap(pixmap_path).scaledToHeight(PIXEL_HEIGHT);
  lines = QPixmap("://lines.png").scaledToHeight(PIXEL_HEIGHT);
}

void FieldItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	painter->setBrush(QColor(0,182,0));
	painter->setPen(Qt::white);
	painter->drawRect(-PIXEL_WIDTH/2,-PIXEL_HEIGHT/2,PIXEL_WIDTH,PIXEL_HEIGHT);
	painter->drawPixmap(-PIXEL_WIDTH/2,-PIXEL_HEIGHT/2,lines);
}

void FieldItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	std::cout << "mouse pressed : " << event->pos().x() << "," << event->pos().y() << '\n';
	click_pos.first = event->pos().x();
	click_pos.second = event->pos().y();
}

void FieldItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	std::cout << "mouse released : " << event->pos().x() << "," << event->pos().y() << '\n';
}

QRectF FieldItem::boundingRect() const
{
	return QRectF(-PIXEL_WIDTH/2,-PIXEL_HEIGHT/2,PIXEL_WIDTH,PIXEL_HEIGHT);
}
