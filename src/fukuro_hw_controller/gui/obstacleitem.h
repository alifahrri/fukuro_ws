#ifndef OBSTACLEITEM_H
#define OBSTACLEITEM_H

#include <QGraphicsItem>

class ObstacleItem : public QGraphicsItem
{
public:
    typedef std::pair<double,double> Pos2D;
    typedef std::vector<Pos2D> Pos2DList;
    ObstacleItem();
    ~ObstacleItem() {}
    void setObstacles(Pos2DList obs) { obstacles = obs; }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;

private:
    Pos2DList obstacles;
};

#endif