#include "robotmonitordialog.h"
#include "ui_robotmonitordialog.h"
#include <QGraphicsScene>
#include <cmath>

RobotMonitorDialog::RobotMonitorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RobotMonitorDialog),
    scene(new QGraphicsScene(this))
{
    ui->setupUi(this);
    ui->graphicsView->setScene(scene);
//    ui->graphicsView->setBackgroundBrush(Qt::gray);

#ifdef USE_ROBOT_STATE
    robot_item = new RobotItem(1.25);
    robot_item->setRobotState(fukuro::RobotState6(fukuro::RobotState(0.0,0.0,0.0),fukuro::RobotState()));
#else
    robot_item = new RobotItem;
    robot_item->setPos(0.0,0.0,0.0);
#endif
    robot_item->drawText(false);
    scene->addItem(robot_item);
    vt = scene->addText(QString("0 m/s"));
    vw = scene->addText(QString("0 rad/s"));
    vt->setPos(-50,30);
    vw->setPos(10,30);
    scene->setSceneRect(-30,-30,60,60);
}

void RobotMonitorDialog::setBattery(double b1, double b2)
{
    ui->batt1LcdNumber->display(b1);
    ui->batt2LcdNumber->display(b2);
}

void RobotMonitorDialog::setCompass(double cmps)
{
    ui->compassLabel->setText(QString::number(cmps));
}

void RobotMonitorDialog::setVelocity(double vx, double vy, double vw_)
{
    auto vt_ = sqrt(vx*vx+vy*vy);
#ifdef USE_ROBOT_STATE
    robot_item->setRobotState(fukuro::RobotState6(fukuro::RobotState(0.0,0.0,0.0),fukuro::RobotState(vx,vy,vw_)));
#else
    robot_item->setState(0.0,0.0,0.0,vx,vy,vw_);
#endif
    vt->setPlainText(QString::number(vt_,'g',3)+QString(" m/s"));
    vw->setPlainText(QString::number(vw_,'g',3)+QString(" rad/s"));
    ui->graphicsView->update();
}

RobotMonitorDialog::~RobotMonitorDialog()
{
    delete ui;
}
