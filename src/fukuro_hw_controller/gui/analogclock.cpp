#include <QtWidgets>
#include "analogclock.h"

AnalogClock::AnalogClock(QWidget *parent)
    : QWidget(parent)
{
    QTimer *timer = new QTimer(this);
    //connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);

    setWindowTitle(tr("Analog Clock"));
    resize(200, 200);
}

void AnalogClock::paintEvent(QPaintEvent *)
{
#ifdef HOUR
    static const QPoint hourHand[3] = {
        QPoint(7, 8),
        QPoint(-7, 8),
        QPoint(0, -40)
    };
    QColor hourColor(127, 0, 127);
#endif

#ifdef MINUTE
    static const QPoint minuteHand[3] = {
        QPoint(7, 8),
        QPoint(-7, 8),
        QPoint(0, -70)
    };
    QColor minuteColor(0, 127, 127, 191);
#endif


    int side = qMin(width(), height());
    //QTime time = QTime::currentTime();

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(width() / 2, height() / 2);
    painter.scale(side / 200.0, side / 200.0);

#ifdef HOUR
    painter.setPen(Qt::NoPen);
    painter.setBrush(hourColor);

    painter.save();
    painter.rotate(30.0 * ((time.hour() + time.minute() / 60.0)));
    painter.drawConvexPolygon(hourHand, 3);
    painter.restore();

    painter.setPen(hourColor);

    for (int i = 0; i < 12; ++i) {
        painter.drawLine(88, 0, 96, 0);
        painter.rotate(30.0);
    }
#endif

#ifdef MINUTE
    painter.setPen(Qt::NoPen);
    painter.setBrush(minuteColor);

    painter.save();
    //painter.rotate(6.0 * (time.minute() + time.second() / 60.0));
    painter.rotate(-south);
    painter.drawConvexPolygon(minuteHand, 3);
    painter.restore();

    painter.setPen(minuteColor);

    painter.rotate(-90.0);
    for (int j = 0; j < 60; ++j) {
        if((j%15)==0){
            painter.drawLine(80, 0, 99, 0);
        }
        else {
            painter.drawLine(92, 0, 96, 0);
        }
        //if ((j % 5) == 0){
        //auto text = QString("%1°").arg(QString::number(j*6.0));
        //painter.drawText(QRect(60,0,65,25),text);
        //}
        painter.rotate(-6.0);
    }
    //painter.restore();
#endif
}
