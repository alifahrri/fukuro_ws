## fukuro_camera_server
collection of launcher for omnicamera and front camera

### Launcher
- `frontcamera.launch` : launch `usb_cam_node` for front-facing camera as `front_camera` node, optional args : `frontcam_device` -> the name of front camera, `video_device` -> path to device. example : `roslaunch fukuro_camera_server frontcamera.launch video_device:=/dev/video1`  
- `frontvision.launch` : launch `frontcamera.launch`, `fukuro_front_vision_node`, `frontvis3d_classifier_node`, `frontvis3d_regressor_node`.
- `omnicamera.launch` : launch another `usb_cam_node` for omni-camera. optional args : `omnicam_device` and `video_device`.
- `omnivision.launch` : launch `omnicamera.launch` and `omni_vision_node`.