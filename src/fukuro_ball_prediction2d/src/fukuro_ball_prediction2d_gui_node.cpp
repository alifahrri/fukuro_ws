#include <ros/ros.h>
#include <QApplication>
#include "ballpredictionwidget.h"

int main(int argc, char **argv)
{
  ros::init(argc,argv,"fukuro_ball_prediction_gui");

  std::string robot;
  ros::get_environment_variable(robot,"FUKURO");

  ros::NodeHandle node;
  QApplication app(argc,argv);
  BallPredictionWidget widget;

  ros::Subscriber prediction_sub = node.subscribe(robot+"/ball_prediction",3,&BallPredictionWidget::update,&widget);
  ros::AsyncSpinner spinner(2);

  spinner.start();
  widget.show();

  app.exec();
  spinner.stop();

  return 0;
}
