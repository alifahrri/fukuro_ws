#include <ros/ros.h>
#include "ballregression.h"

int main(int argc, char** argv)
{
  ros::init(argc,argv,std::string("fukuro_ball_prediction2d"));
  ros::NodeHandle node;
  BallRegression ball_regression(node);
  ros::Subscriber omni_sub = node.subscribe("/localization",1,&BallRegression::processMCL,&ball_regression);
  ros::Subscriber vision_sub = node.subscribe("/omnivision/OmniVisionInfo",1,&BallRegression::processVision,&ball_regression);
  ros::Rate rate(30.0);
  ros::AsyncSpinner spinner(1);
  spinner.start();
  while(ros::ok())
  {
    ball_regression.process();
    ball_regression.publish();
    rate.sleep();
  }
  return 0;
}
