#include "ballregression.h"
#include "fukuro_common/Point2dStamped.h"

#define TO_RAD (M_PI/180.0)
#define OMNI_ANGLE_SHIFT (-M_PI_2)

#define MAX_ERROR 100.0 //cm

BallRegression::BallRegression(ros::NodeHandle &node)
{
  std::string robot;
  ros::get_environment_variable(robot,"FUKURO");
  pub = node.advertise<fukuro_common::BallPrediction>(robot+"/ball_prediction",3);
}

void BallRegression::processVision(const nubot_common::OminiVisionInfo::ConstPtr &omnivision)
{
  if(!localized)
    return;
  else if(!omnivision->ballinfo.pos_known) {
      if(ball_pos.size())
        ball_pos.erase(ball_pos.begin());
      return;
    }
  auto c = cos(robot_pose.w);
  auto s = sin(robot_pose.w);
  auto angle = omnivision->ballinfo.pos_real.angle;
  auto radius = omnivision->ballinfo.pos_real.radius;
  auto bx = cos(angle+OMNI_ANGLE_SHIFT)*radius;
  auto by = -(sin(angle+OMNI_ANGLE_SHIFT)*radius);
  auto gx = c*bx - s*by + robot_pose.x;
  auto gy = s*bx + c*by + robot_pose.y;
  if(ball_pos.size()>n_buffer)
    ball_pos.erase(ball_pos.begin());
  auto now = ros::Time::now().toSec();
  Pos2DStamped ball({gx,gy,now});
  if(ball_pos.size()<2) {
      ball_pos.push_back(ball);
    }
  else {
      auto abs_error_sum = 0.0;
      auto n = ball_pos.size();
      for(size_t i=0; i<n; i++) {
          const auto& b= ball_pos.at(i);
          auto error = std::hypot(ball.x-b.x, ball.y-b.y);
          abs_error_sum += error;
        }
      auto avg_error = abs_error_sum / n;
      if(avg_error < MAX_ERROR) {
          if(ball_pos.size()>n_buffer)
            ball_pos.erase(ball_pos.begin());
          ball_pos.push_back(ball);
        }
      else {
          ball_pos.erase(ball_pos.begin());
        }
    }
  ROS_INFO("Process Vision : ball(%f,%f,%f)",ball.x,ball.y,ball.time);
#if 0
  for(size_t i=0; i<ball_pos.size(); i++)
  {
    auto ball = ball_pos.at(i);
    ROS_INFO("Process Vision : ball_pos(%f,%f,%f)",ball.x,ball.y,ball.time);
  }
#endif
  last_update = ros::Time::now();
}

void BallRegression::processMCL(const fukuro_common::Localization::ConstPtr &mcl)
{
  if(!localized)
    localized = true;
  robot_pose.x = mcl->belief.x;
  robot_pose.y = mcl->belief.y;
  robot_pose.w = mcl->belief.theta;
  ROS_INFO("Process Localization : robot_pose(%f,%f,%f)",robot_pose.x,robot_pose.y,robot_pose.w);
}

void BallRegression::process()
{
  if(ball_pos.size()<2)
    return;
  double sx = 0.0;
  double sqx = 0.0;
  double sy[2] = {0.0,0.0};
  double sxy[2] = {0.0,0.0};
  auto n = ball_pos.size();
  auto t0 = ball_pos.at(0).time;
  for(size_t i=0; i<ball_pos.size(); i++)
  {
    const auto& t = ball_pos.at(i).time-t0;
    const auto& x = ball_pos.at(i).x;
    const auto& y = ball_pos.at(i).y;
    sx += t;
    sqx += t*t;
    sy[0] += x;
    sy[1] += y;
    sxy[0] += t*x;
    sxy[1] += t*y;
    auto ball = ball_pos.at(i);
    ROS_INFO("Process : ball_pos(%f,%f,%f)",ball.x,ball.y,ball.time);
  }
  auto sxsx = sx*sx;
  auto tmp0 = (n*sqx-sxsx);
  auto tmp1 = (n*sqx-sxsx);
  ball_model.x.a = (sy[0]*sqx - sx*sxy[0])/tmp0;
  ball_model.x.b = (n*sxy[0]-sx*sy[0])/tmp0;
  ball_model.y.a = (sy[1]*sqx - sx*sxy[1])/tmp1;
  ball_model.y.b = (n*sxy[1] - sx*sy[1])/tmp1;

  ROS_INFO("Process : ball_model(%f,%f,%f,%f)",ball_model.x.a,ball_model.x.b,ball_model.y.a,ball_model.y.b);
}

void BallRegression::publish()
{
  fukuro_common::BallPrediction msg;
  if(ball_pos.size()<2)
    goto PUBLISH;
  msg.ax = ball_model.x.a;
  msg.bx = ball_model.x.b;
  msg.ay = ball_model.y.a;
  msg.by = ball_model.y.b;
  {
    auto ti = ball_pos.front().time;
    auto tf = ball_pos.back().time;
    auto p1 = ball_model(tf-ti+1.0);
    auto p2 = ball_model(tf-ti+2.0);
    auto p3 = ball_model(tf-ti+3.0);
    msg.p1s.x = p1.x;
    msg.p1s.y = p1.y;
    msg.p2s.x = p2.x;
    msg.p2s.y = p2.y;
    msg.p3s.x = p3.x;
    msg.p3s.y = p3.y;
  }
PUBLISH:
  for(size_t i=0; i<this->ball_pos.size(); i++)
  {
    Pos2DStamped ball = this->ball_pos.at(i);
    fukuro_common::Point2dStamped p;
    p.x = ball.x;
    p.y = ball.y;
    p.t = ball.time;
    msg.pos.push_back(p);
    ROS_INFO("Publish : ball_pos(%f,%f,%f), msg.pos(%f,%f,%f)",ball.x,ball.y,ball.time,p.x,p.y,p.t);
  }
  msg.last_ball.data = last_update;
  pub.publish(msg);
}

double BallRegression::LinearModel::operator()(double x)
{
  return b*x+a;
}

BallRegression::Pos2DStamped BallRegression::BallModel::operator()(double t)
{
  return Pos2DStamped({x(t),y(t),t});
}
