#ifndef BALLPREDICTIONWIDGET_H
#define BALLPREDICTIONWIDGET_H

#if 1
#include <QWidget>
#include <QGraphicsItem>
#include <QtCore>
#else
#include <qt5/QtWidgets/QWidget>
#include <qt5/QtWidgets/QGraphicsItem>
#include <qt5/QtCore/QtCore>
#endif

#include "fukuro/core/field.hpp"
#include "fukuro_common/BallPrediction.h"
#include "qcustomplot.h"

namespace Ui {
class BallPredictionWidget;
}

class BallPredictionWidget : public QWidget
{
  Q_OBJECT

public:
  explicit BallPredictionWidget(QWidget *parent = 0);
  void update(const fukuro_common::BallPrediction::ConstPtr &msg);
  ~BallPredictionWidget();

private:
  class Field : public QGraphicsItem
  {
  public:
    Field();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

  private:
    QVector<QLineF> lines;
    QVector<QLineF> circle_lines;
    QRectF center_circle;
  };

  class Prediction2D : public QGraphicsItem
  {
  public:
    Prediction2D();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    QVector<QPointF> balls;
    QPointF p0;
    QPointF p1;
  };

private:
  Ui::BallPredictionWidget *ui;
  QCPGraph *xt_predict_graph;
  QCPGraph *yt_predict_graph;
  Prediction2D *ball_model;
  QCPGraph *xt_graph;
  QCPGraph *yt_graph;
  Field *field;
};

#endif // BALLPREDICTIONWIDGET_H
