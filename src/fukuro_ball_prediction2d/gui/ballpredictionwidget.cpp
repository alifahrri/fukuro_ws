#include "ballpredictionwidget.h"
#include "ui_ballpredictionwidget.h"
#include <ros/ros.h>
#include <qt5/QtGui/QPainter>
#include <qt5/QtWidgets/QGraphicsScene>
#include <qt5/QtWidgets/QGraphicsView>
#include <qt5/QtGui/QPen>

#define ANGLE_INCREMENT 3

#define CENTER_RADIUS CENTER_CIRCLE_RADIUS

BallPredictionWidget::BallPredictionWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::BallPredictionWidget),
  ball_model(new Prediction2D),
  field(new Field)
{
  ui->setupUi(this);
  ui->graphicsView->setScene(new QGraphicsScene(-FIELD_WIDTH/2-100,-FIELD_HEIGHT/2-100,FIELD_WIDTH+200,FIELD_HEIGHT+200,this));
  ui->graphicsView->scene()->addItem(field);
  ui->graphicsView->scene()->addItem(ball_model);
  ui->graphicsView->setRenderHints(QPainter::Antialiasing | QPainter::HighQualityAntialiasing);
  ui->graphicsView->scale(0.66,0.65);
  ui->graphicsView->setBackgroundBrush(Qt::green);
  this->setFixedSize(this->size());

  xt_graph = ui->plotter->addGraph();
  yt_graph = ui->plotter->addGraph(ui->plotter->xAxis,ui->plotter->yAxis2);
  xt_predict_graph = ui->plotter->addGraph();
  yt_predict_graph = ui->plotter->addGraph(ui->plotter->xAxis,ui->plotter->yAxis2);
  yt_graph->setPen(QPen(Qt::red));
  yt_predict_graph->setPen(QPen(Qt::red,1.0,Qt::DashLine));
  xt_predict_graph->setPen(QPen(Qt::blue,1.0,Qt::DashLine));
  ui->plotter->yAxis2->setVisible(true);

  this->setWindowTitle("Fukuro Ball Prediction");
}

void BallPredictionWidget::update(const fukuro_common::BallPrediction::ConstPtr &msg)
{
  if(msg->pos.size()<2)
    return;
  xt_graph->clearData();
  yt_graph->clearData();
  auto t0 = msg->pos.front().t;
  ball_model->balls.clear();
  for(size_t i=0; i<msg->pos.size(); i++)
  {
    xt_graph->addData(msg->pos.at(i).t-t0,msg->pos.at(i).x);
    yt_graph->addData(msg->pos.at(i).t-t0,msg->pos.at(i).y);
    ROS_INFO("update : ball(%f,%f,%f)",msg->pos.at(i).t-t0,msg->pos.at(i).x,msg->pos.at(i).y);
    ball_model->balls.push_back(QPointF(msg->pos.at(i).x,msg->pos.at(i).y));
  }
  if(msg->pos.size()>=2)
  {
    auto x0 = msg->ax;
    auto y0 = msg->ay;
    auto dt = msg->pos.back().t - msg->pos.front().t;
    auto xt = x0 + msg->bx * (dt+1.0);
    auto yt = y0 + msg->by * (dt+1.0);
    xt_predict_graph->clearData();
    yt_predict_graph->clearData();
    xt_predict_graph->addData(0.0,x0);
    xt_predict_graph->addData(dt+1.0,xt);
    yt_predict_graph->addData(0.0,y0);
    yt_predict_graph->addData(dt+1.0,yt);
    ball_model->p0.setX(x0);
    ball_model->p0.setY(y0);
    ball_model->p1.setX(xt);
    ball_model->p1.setY(yt);
  }
  ui->plotter->rescaleAxes(true);
  ui->plotter->yAxis2->rescale(true);
  ui->plotter->yAxis->rescale(true);
  ui->plotter->replot();
  this->ui->graphicsView->scene()->update();
}

BallPredictionWidget::~BallPredictionWidget()
{
  delete ui;
}

BallPredictionWidget::Field::Field()
{
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE7,YLINE1)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE6),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE1,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE7,YLINE1),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE4,YLINE1),
                         QPointF(XLINE4,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE2,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE6,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE3,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE5,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE1,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE4),
                         QPointF(XLINE1,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE1,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE5),
                         QPointF(XLINE1,YLINE5)));

  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE7,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE4),
                         QPointF(XLINE7,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE7,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE5),
                         QPointF(XLINE7,YLINE5)));

  center_circle.setLeft(-CENTER_RADIUS);
  center_circle.setTop(-CENTER_RADIUS);
  center_circle.setHeight(2*CENTER_RADIUS);
  center_circle.setWidth(2*CENTER_RADIUS);

  QPointF circle_line0(center_circle.width()/2,0.0);
  QLineF line(QPointF(0.0,0.0),circle_line0);
  circle_lines.push_back(QLineF(circle_line0,line.p2()));
  for(int i=ANGLE_INCREMENT; i<=360; i+=ANGLE_INCREMENT)
  {
      line.setAngle(i);
      QPointF p0 = circle_lines.back().p2();
      circle_lines.push_back(QLineF(p0,line.p2()));
  }
}

void BallPredictionWidget::Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::transparent);
  painter->setBrush(Qt::green);
  painter->drawRect(boundingRect());
  painter->setPen(QPen(Qt::white,3.3));
  painter->setBrush(Qt::transparent);
  painter->drawLines(lines);
  painter->drawEllipse(center_circle);
}

QRectF BallPredictionWidget::Field::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

BallPredictionWidget::Prediction2D::Prediction2D()
{

}

void BallPredictionWidget::Prediction2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  if(!balls.size())
    return;
  painter->setBrush(Qt::red);
  painter->setPen(Qt::black);
  for(const auto& b : balls)
    painter->drawEllipse(QPointF(b),10,10);
  painter->setPen(QPen(Qt::blue,2.0));
  painter->drawLine(p0,p1);
  painter->translate(p0);
  auto pd = p1;
  pd -= p0;
  auto length = hypot(p1.x()-p0.x(),p1.y()-p0.y());
  auto angle = atan2(pd.y(),pd.x());
  painter->rotate((-90.0)+(angle*180/M_PI));
  painter->drawLine(QPointF(0.0,length),QPoint(-5.0,length-5.0));
  painter->drawLine(QPointF(0.0,length),QPoint(5.0,length-5.0));
}

QRectF BallPredictionWidget::Prediction2D::boundingRect() const
{

}
