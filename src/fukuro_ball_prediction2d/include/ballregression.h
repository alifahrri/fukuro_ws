#ifndef BALLREGRESSION_H
#define BALLREGRESSION_H

#include <vector>
#include <ros/ros.h>
#include <fukuro_common/Localization.h>
#include <fukuro_common/BallPrediction.h>
#include <nubot_common/OminiVisionInfo.h>

class BallRegression
{

  struct Pos2DStamped
  {
    double x;
    double y;
    double time;
  };

  struct Pose2D
  {
    double x;
    double y;
    double w;
  };

  struct LinearModel
  {
    double a;
    double b;
    double operator()(double x);
  };

  struct BallModel
  {
    LinearModel x;
    LinearModel y;
    Pos2DStamped operator()(double t);
  };

public:
  BallRegression(ros::NodeHandle &node);
  void processVision(const nubot_common::OminiVisionInfo::ConstPtr& omnivision);
  void processMCL(const fukuro_common::Localization::ConstPtr& mcl);
  void process();
  void publish();

private:
  std::vector<Pos2DStamped> ball_pos;
  ros::Publisher pub;
  BallModel ball_model;
  Pose2D robot_pose;
  size_t n_buffer = 30;
  ros::Time last_update;
  bool localized = false;
};
#endif // BALLREGRESSION_H
