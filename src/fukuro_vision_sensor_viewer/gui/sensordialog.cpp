#include "sensordialog.h"
#include "ui_sensordialog.h"
#include "fukuro_common/Point2d.h"
#include <QGraphicsSceneHoverEvent>

#define SCENE_WIDTH 640
#define SCENE_HEIGHT 480

SensorDialog::SensorDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::SensorDialog),
  robot(new RobotItem),
  whites(new WhitesItem),
  balls(new BallsItem),
  obstacles(new ObstaclesItem)
{
  ui->setupUi(this);
  ui->graphicsView->setScene(new QGraphicsScene(-SCENE_WIDTH/2,-SCENE_HEIGHT/2,SCENE_WIDTH,SCENE_HEIGHT,this));
  ui->graphicsView->scene()->addItem(whites);
  ui->graphicsView->scene()->addItem(balls);
  ui->graphicsView->scene()->addItem(obstacles);
  ui->graphicsView->scene()->addItem(robot);
  ui->graphicsView->setRenderHint(QPainter::Antialiasing);
  setFixedSize(SCENE_WIDTH+50, SCENE_WIDTH+50);
  setWindowTitle("Fukuro Vision Sensor");
}

void SensorDialog::updateWhites(const fukuro_common::Whites::ConstPtr &white)
{
  whites->setWhites(white);
}

void SensorDialog::updateBalls(const fukuro_common::Balls::ConstPtr &ball)
{
  balls->setBalls(ball);
}

void SensorDialog::updateObstacles(const fukuro_common::Obstacles::ConstPtr &obs)
{
  obstacles->setObstacles(obs);
}

SensorDialog::~SensorDialog()
{
  delete ui;
}

SensorDialog::RobotItem::RobotItem()
{
  line_width = 600;
  line_height = 600;
  int line_step = 30;
  for(int w=-line_width/2; w<=line_width; w+=line_step)
  {
    lines.push_back(QLineF(QPointF(w,-line_height/2),QPointF(w,line_height/2)));
  }
  for(int h=-line_height/2; h<=line_height; h+=line_step)
  {
    lines.push_back(QLineF(QPointF(-line_width/2,h),QPointF(line_width/2,h)));
  }
}

void SensorDialog::RobotItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->rotate(-90);
  painter->setPen(Qt::red);
  painter->drawEllipse(QPointF(0.0,0.0),25.0,25.0);
  painter->drawLine(QPointF(0.0,0.0),QPointF(30.0,0.0));
  painter->setPen(Qt::gray);
  painter->drawLines(lines);
}

QRectF SensorDialog::RobotItem::boundingRect() const
{
  return QRectF(-line_width/2,-line_height/2,line_width,line_height);
}

SensorDialog::WhitesItem::WhitesItem()
{
  this->setAcceptHoverEvents(true);
}

void SensorDialog::WhitesItem::setWhites(const fukuro_common::Whites::ConstPtr &white)
{
  whites.clear();
  for(auto w : white->whites)
    whites.push_back(QPointF(w.x,w.y));
  this->update();
}

void SensorDialog::WhitesItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->rotate(-90);
  auto matrix0 = painter->matrix();
  painter->setPen(Qt::green);
  QString text;
  for(auto& w : whites)
  {
    painter->setMatrix(matrix0);
    QPointF offset(w.x()*100,-w.y()*100);
    painter->translate(offset);
    if((offset-hover_text).manhattanLength()<15)
    {
      text = QString("(%1,%2)").arg(w.x(),0,'g',2).arg(w.y(),0,'g',2);
    }
    painter->setPen(Qt::green);
    painter->drawEllipse(QPointF(0.0,0.0),3.0,3.0);
  }
  if(draw_text)
  {
    if(!text.isEmpty())
    {
      painter->setMatrix(matrix0);
      painter->rotate(90);
      painter->setPen(Qt::black);
      painter->translate(hover_text);
      painter->drawText(QPointF(0.0,0.0),text);
    }
  }
}

QRectF SensorDialog::WhitesItem::boundingRect() const
{
  return QRectF(-SCENE_WIDTH/2,-SCENE_WIDTH/2,SCENE_WIDTH,SCENE_WIDTH);
}

void SensorDialog::WhitesItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
  draw_text = true;
  hover_text = event->pos();
}

void SensorDialog::WhitesItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
  draw_text = true;
}

void SensorDialog::WhitesItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
  hover_text = event->pos();
}

SensorDialog::BallsItem::BallsItem()
{

}

void SensorDialog::BallsItem::setBalls(const fukuro_common::Balls::ConstPtr &ball)
{
  balls.clear();
  for(auto& b : ball->balls)
    balls.push_back(QPointF(b.x,-b.y));
  this->update();
}

void SensorDialog::BallsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->rotate(-90);
  auto matrix0 = painter->matrix();
  painter->setPen(Qt::red);
  painter->setBrush(Qt::red);
  for(auto& b : balls)
  {
    painter->setMatrix(matrix0);
    painter->translate(b.x()*100,-b.y()*100);
    painter->drawEllipse(QPointF(0.0,0.0),15.0,15.0);
  }
}

QRectF SensorDialog::BallsItem::boundingRect() const
{
  return QRectF(-SCENE_WIDTH/2,-SCENE_WIDTH/2,SCENE_WIDTH,SCENE_WIDTH);
}

SensorDialog::ObstaclesItem::ObstaclesItem()
{

}

void SensorDialog::ObstaclesItem::setObstacles(const fukuro_common::Obstacles::ConstPtr &obstacle)
{
  obstacles.clear();
  for(auto& o : obstacle->obstacles)
    obstacles.push_back(QPointF(o.x,o.y));
  this->update();
}

void SensorDialog::ObstaclesItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->rotate(-90);
  auto matrix0 = painter->matrix();
  painter->setPen(Qt::black);
  painter->setPen(Qt::darkGray);
  painter->setBrush(Qt::gray);
  for(auto& o : obstacles)
  {
    painter->setMatrix(matrix0);
    painter->translate(o.x()*100,-o.y()*100);
    painter->drawEllipse(QPointF(0.0,0.0),25.0,25.0);
  }
}

QRectF SensorDialog::ObstaclesItem::boundingRect() const
{
  return QRectF(-SCENE_WIDTH/2,-SCENE_WIDTH/2,SCENE_WIDTH,SCENE_WIDTH);
}
