#include <ros/ros.h>
#include <QApplication>
#include "sensordialog.h"

int main(int argc, char** argv)
{
  QApplication app(argc,argv);
  ros::init(argc,argv,"fukuro_vision_sensor_viewer");
  ros::NodeHandle node;
  SensorDialog dialog;
  ros::Subscriber whites_sub = node.subscribe("/omnivision/Whites",1,&SensorDialog::updateWhites,&dialog);
  ros::Subscriber balls_sub = node.subscribe("/omnivision/Balls",1,&SensorDialog::updateBalls,&dialog);
  ros::Subscriber obstacles_sub = node.subscribe("/omnivision/Obstacles",1,&SensorDialog::updateObstacles,&dialog);
  dialog.show();
  ros::AsyncSpinner spinner(3);
  if(spinner.canStart())
    spinner.start();
  app.exec();
  return 0;
}
