#include <iostream>
#include <fstream>
#include <sstream>

//#include <omni_vision/OmniVisionConfig.h>

#include <opencv2/opencv.hpp>
#include "nubot/core/core.hpp"
#include "nubot/omni_vision/transfer.h"
#include "nubot/omni_vision/scanpoints.h"
#include "nubot/omni_vision/whites.h"
//#include "nubot/omni_vision/optimise.h"
//#include "nubot/omni_vision/glocalization.h"
//#include "nubot/omni_vision/odometry.h"
//#include "nubot/omni_vision/localization.h"
#include "nubot/omni_vision/obstacles.h"
#include "nubot/omni_vision/colorsegment.h"
#include "nubot/omni_vision/ballfinder.h"
#include "coordinatetransform.h"

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/String.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <std_msgs/Header.h>
#include <image_transport/image_transport.h>
#include <image_transport/camera_subscriber.h>

#include <fukuro_common/Point2d.h>
#include <fukuro_common/Whites.h>
#include <fukuro_common/Balls.h>
#include <fukuro_common/Obstacles.h>
#include <nubot_common/OdoInfo.h>
#include <nubot_common/RobotInfo.h>
#include <nubot_common/BallInfo.h>
#include <nubot_common/ObstaclesInfo.h>
#include <nubot_common/OminiVisionInfo.h>
#include <omni_vision/OmniVisionConfig.h>
#include <dynamic_reconfigure/server.h>
#include <sched.h>
#define DEFAULT_PRIO    80
#define CALIB_PATH "/home/fahri/fukuro_ws/src/omni_vision/calib_results"

using namespace cv;
using namespace nubot;

namespace encodings=sensor_msgs::image_encodings;
namespace nubot {

struct RobotInformation
{
  DPoint2d realvtrans_;
  DPoint2d worldvtrans_;
  DPoint   visual_location_;
  DPoint   final_location_;
  Angle    angle_;
  double   angular_velocity_;
  bool     isglobal_;
};

class Omni_Vision
{

public:

  OmniImage        * imginfo_;
  Transfer         * tranfer_;
  ScanPoints       * scanpts_;
  Whites           * whites_;
  Obstacles        * obstacles_;
  BallFinder       * ball_finder_;
  ColorSegment     * colorsegment_;
  fukuro::CoordinateTransform * c_transform;
  FieldInformation field_info_;

  RobotInformation robot;


  image_transport::Subscriber img_sub_;
  ros::Subscriber motor_info_sub_;

  ros::Publisher  ballinfo_pub_;
  ros::Publisher  robotinfo_pub_;
  ros::Publisher  obstaclesinfo_pub_;
  ros::Publisher  omin_vision_pub_;
  ros::Publisher  whites_pub_;
  ros::Publisher  balls_pub_;
  ros::Publisher  obstacles_pub_;

  nubot_common::BallInfo        ball_info_;
  nubot_common::RobotInfo       robot_info_;
  nubot_common::ObstaclesInfo   obstacles_info_;
  nubot_common::OminiVisionInfo omin_vision_info_;
  dynamic_reconfigure::Server<omni_vision::OmniVisionConfig> reconfigureServer_;

  bool is_show_ball_;
  bool is_show_whites_;
  bool is_show_obstacles_;
  bool is_show_scan_points;
  bool is_show_result_;

  bool is_restart_;
  bool is_robot_stuck_;
  bool is_power_off_;
  cv::Mat field_image_;

  ros::Time receive_time_;
  int Agent_ID_;
public:

  Omni_Vision(int argc, char **argv)
  {
    char * environment;
    if((environment = getenv("AGENT"))==NULL)
    {
      ROS_ERROR("this agent number is not read by robot");
      return ;
    }
    Agent_ID_ = atoi(environment);
    std::stringstream ss;
    ss<<Agent_ID_;
    std::string calibration_path=std::string("/home/")+
        std::string(std::getenv("USER"))+
        std::string("/fukuro_ws/src/omni_vision/calib_results");

    is_show_ball_=false;
    is_show_obstacles_=false;
    is_show_whites_=false;
    is_show_scan_points=false;
    is_show_result_=false;
    is_robot_stuck_ = false;

    ROS_INFO("initialize the omni_vision  process");
    c_transform  = new fukuro::CoordinateTransform;
    c_transform->load(calibration_path+"/"+ss.str()+"/fukuro_mirror_calibration.ini");
    imginfo_     = new OmniImage(calibration_path+"/"+ss.str()+"/ROI.xml");
    tranfer_     = new Transfer(calibration_path+"/"+ss.str()+"/mirror_calib.xml",*imginfo_);
    scanpts_     = new ScanPoints(*imginfo_);
//    whites_      = new Whites(*scanpts_,*tranfer_);
    whites_      = new Whites(*scanpts_,*c_transform);
//    obstacles_   = new Obstacles(*scanpts_,*tranfer_);
    obstacles_   = new Obstacles(*scanpts_,*c_transform,*tranfer_);
//    ball_finder_ = new BallFinder(*scanpts_,*tranfer_);
    ball_finder_ = new BallFinder(*scanpts_,*c_transform);
    colorsegment_= new ColorSegment(calibration_path+"/"+ss.str()+"/CTable.dat");


    robot.isglobal_=true;
    is_restart_=false;
    is_power_off_=false;
    if(WIDTH_RATIO<1)
      field_image_ = cv::imread(calibration_path+"/"+ss.str()+"/field_mine.bmp");
    else
      field_image_ = cv::imread(calibration_path+"/"+ss.str()+"/field.bmp");

    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);
    ROS_INFO("camera......");
    img_sub_= it.subscribe("/usb_cam/image_raw", 1, &Omni_Vision::imageCallback,this);

    ros::NodeHandle node;
    omin_vision_pub_   = node.advertise<nubot_common::OminiVisionInfo>("/omnivision/OmniVisionInfo",1);
    whites_pub_ = node.advertise<fukuro_common::Whites>("/omnivision/Whites",1);
    balls_pub_ = node.advertise<fukuro_common::Balls>("/omnivision/Balls",1);
    obstacles_pub_ = node.advertise<fukuro_common::Obstacles>("/omnivision/Obstacles",1);
    reconfigureServer_.setCallback(boost::bind(&Omni_Vision::configure, this, _1, _2));
  }
  ~Omni_Vision()
  {

  }

public:

  void
  configure(const omni_vision::OmniVisionConfig & config, uint32_t level)
  {
    ROS_INFO("Reconfigure request received");
    is_show_ball_       = config.ball;
    is_show_whites_     = config.white;
    is_show_obstacles_  = config.obstacle;
    is_show_scan_points = config.scan;
    is_show_result_     = config.show;
    int  obstacle_thres = config.obsthres;
    double obstacle_length_thres = config.obs_length_thres;
    double obstacle_basic_thres = config.obs_basic_thres;
    obstacles_->setObsThres(obstacle_thres,obstacle_length_thres,obstacle_basic_thres);
    whites_->setHHigh(config.h_high);
    whites_->setHLow(config.h_low);
    whites_->setMergeWave(config.merge_wave);
    whites_->setNumPtsLine(config.num_pts_line);
    ROS_INFO("Reconfigure request end");
  }

  void
  publish()
  {
    ball_info_.header.stamp = receive_time_;
    ball_info_.header.seq++;
    ball_info_.pos.x =  ball_finder_->get_ball_global_loc().x_;
    ball_info_.pos.y =  ball_finder_->get_ball_global_loc().y_;
    ball_info_.real_pos.angle  = ball_finder_->get_ball_real_loc().angle_.radian_;
    ball_info_.real_pos.radius = ball_finder_->get_ball_real_loc().radius_;
    ball_info_.pixel_pos.x = ball_finder_->get_pixel_pos().x_;
    ball_info_.pixel_pos.y = ball_finder_->get_pixel_pos().y_;
    ball_info_.pos_real.angle = ball_finder_->get_angle_radius().x_;
    ball_info_.pos_real.radius = ball_finder_->get_angle_radius().y_;

    if(is_show_result_)
    {
      ROS_INFO("ball(%.f, %.f, %.1f, %.1f ,%d ,%d)",ball_info_.pos.x,
               ball_info_.pos.y, ball_info_.velocity.x,
               ball_info_.velocity.y,ball_info_.pos_known, ball_finder_->ball_area_.area_size_);
    }
    obstacles_info_.header.stamp= ros::Time::now();
    obstacles_info_.header.seq++;
    obstacles_info_.pos.clear();
    obstacles_info_.polar_pos.clear();
    int length= obstacles_->real_obstacles_.size();
    nubot_common::Point2d point;
    nubot_common::PPoint  polar_point;
    for(int i = 0 ; i < length ; i++)
    {
      DPoint & pt=obstacles_->world_obstacles_[i];
      PPoint & polar_pt= obstacles_->real_obstacles_[i];
      point.x=pt.x_;
      point.y=pt.y_;
      polar_point.angle=polar_pt.angle_.radian_;
      polar_point.radius=polar_pt.radius_;
      obstacles_info_.pos.push_back(point);
      obstacles_info_.polar_pos.push_back(polar_point);
    }
    if(is_show_result_)
    {
      for(int i = 0 ;i < OBS_VALUABLE_NUMBER ;i++)
      {
        if (i<obstacles_info_.pos.size())
          ROS_INFO("obs_omni(%.f, %.f)",obstacles_info_.pos[i].x,obstacles_info_.pos[i].y);
      }
    }
    omin_vision_info_.header.stamp = robot_info_.header.stamp;
    omin_vision_info_.header.seq++;
    omin_vision_info_.ballinfo=ball_info_;
    omin_vision_info_.obstacleinfo=obstacles_info_;
    omin_vision_pub_.publish(omin_vision_info_);
    fukuro_common::Whites white_msg;
    fukuro_common::Balls balls_msg;
    fukuro_common::Obstacles obstacles_msg;
    for(auto& w : whites_->real_pos_white)
    {
      fukuro_common::Point2d white_point;
      white_point.x = w.x_;
      white_point.y = w.y_;
      white_msg.whites.push_back(white_point);
    }
    for(auto& b : ball_finder_->get_real_balls())
    {
      fukuro_common::Point2d ball;
      ball.x = b.x_;
      ball.y = b.y_;
      balls_msg.balls.push_back(ball);
    }
    for(auto& o : obstacles_->real_obstacles_pos)
    {
      fukuro_common::Point2d obs;
      obs.x = o.x_;
      obs.y = o.y_;
      obstacles_msg.obstacles.push_back(obs);
    }
    whites_pub_.publish(white_msg);
    balls_pub_.publish(balls_msg);
    obstacles_pub_.publish(obstacles_msg);
  }

  void
  process()
  {
    if(!colorsegment_->Segment(imginfo_->yuv_image_))
      return;

    //get the colors of the scan_points
    scanpts_->process();
    scanpts_->findFieldBoundary(colorsegment_->segment_result_);
    //detect the white points
    whites_->process();

    if(whites_->img_white_.size()<=0)
    {
      ROS_WARN("don't detect the white points");
      return ;
    }
    obstacles_->process(colorsegment_->segment_result_,robot.final_location_,robot.angle_);
    ball_info_.pos_known=ball_finder_->Process(colorsegment_->segment_result_,
                                               robot.final_location_,robot.angle_);
    ROS_INFO("Robot Final location : %f %f",robot.final_location_.x_,robot.final_location_.y_);
    if(is_show_whites_ || is_show_obstacles_ || is_show_ball_ )
    {
      if(field_image_.empty())
        ROS_INFO("Field.bmp is empty");
      cv::Mat image  = field_image_.clone();
      cv::Mat orgnal = imginfo_->getBGRImage().clone();
      static double length = 1920;
      static double width  = 1314;
      if(WIDTH_RATIO<1)
      {
        length = 1920;
        width  = 882;
      }
      if(is_show_scan_points)
        scanpts_->showScanPoints();

      if(is_show_obstacles_)
      {
        obstacles_->showObstacles(orgnal);
        if(!field_image_.empty())
          obstacles_->showObstacles(image,robot.final_location_,robot.angle_,length,width);
      }
      if(is_show_whites_)
      {
        whites_->showWhitePoints(orgnal);
        if(!field_image_.empty())
          whites_->showWhitePoints(image,robot.final_location_,robot.angle_,length,width);
      }
      if(is_show_ball_)
      {
        ball_finder_->showBall(orgnal);
        if(!field_image_.empty())
          ball_finder_->showBall(image,robot.final_location_,robot.angle_,length,width);
      }
    }
  }

  void
  imageCallback(const sensor_msgs::ImageConstPtr& _image_msg)
  {
    ROS_INFO("Image Callback");
    ros::Time start = ros::Time::now();
    receive_time_ = _image_msg->header.stamp;
    cv_bridge::CvImageConstPtr cv_ptr;
    cv_ptr=cv_bridge::toCvShare(_image_msg,encodings::BGR8);
    Mat orignalimage=cv_ptr->image;
    bool isthreechannels=imginfo_->setBGRImage(orignalimage);
    if(!isthreechannels)
    {
      ROS_WARN("the image doesn't have three channels");
      return ;
    }
    imginfo_->bgr2yuv();
    process();
    publish();
    //   ROS_INFO("start omni_vision imageCallback");
    static ros::Time time_before = ros::Time::now();
    ros::Duration duration  = ros::Time::now() - time_before;
    ros::Duration duration1 = ros::Time::now() - start;
    time_before = ros::Time::now();
    ROS_INFO("omni_time: %d %d %d",int(1.0/duration.toSec()),
             int(1.0/duration1.toSec()),whites_->img_white_.size());
  }
};


}// end of namespace nubot


int main(int argc, char **argv)
{ 
  struct sched_param schedp;
  memset(&schedp, 0, sizeof(schedp));
  schedp.sched_priority = DEFAULT_PRIO;
  if (sched_setscheduler(0, SCHED_FIFO, &schedp)) {
    printf("set scheduler failed.\n");
    sched_setscheduler(0, SCHED_OTHER, &schedp);
  }
  ros::init(argc,argv,"omni_vision_node");
  ros::Time::init();
  ROS_INFO("start omni_vision process");
  nubot::Omni_Vision vision_process(argc, argv);
  ros::spin();
  return 0;
}
