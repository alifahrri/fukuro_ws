#ifndef __NUBOT_VISION_WHITES_H_
#define __NUBOT_VISION_WHITES_H_

#include <opencv2/opencv.hpp>
#include "nubot/core/core.hpp"
#include "nubot/omni_vision/scanpoints.h"
#include "nubot/omni_vision/transfer.h"
#include "coordinatetransform.h"

namespace nubot
{ 

using std::vector;
using std::string;

const int MAX_NUMBRT_CONST = 10000;
const int MIN_NUMBRT_CONST = -10000;

class Whites
{
public:

  struct peak
  {
    int peak_index;
    int left_hollow;
    int right_hollow;
    int near_hollow;
    int width;
    bool boundaries;
  };

public:

  Whites(ScanPoints & _scanpts, Transfer & _coor_transfer);
  Whites(ScanPoints & _scanpts, fukuro::CoordinateTransform & _trans);

  void process();

  void detectWhitePts(std::vector<DPoint2i> & pts,std::vector<uchar> & _ColorY_Aver,std::vector<DPoint2i> & whites);
  void detectWhitePts(std::vector<DPoint2i> & pts,std::vector<uchar> & _ColorY_Aver,std::vector<DPoint2i> & whites, unsigned int green_bound);
  void detectWhitePts(std::vector<DPoint2i> & pts,std::vector<uchar> & _ColorY_Aver,std::vector<DPoint2i> & whites, std::pair<DPoint2i,DPoint2i> green_bound);

  void detectWave(std::vector<uchar> & colors,std::vector<bool> & wave_hollow,std::vector<bool> & wave_peak);
  void detectWave(std::vector<uchar> & colors,std::vector<bool> & wave_hollow,std::vector<bool> & wave_peak, unsigned int green_bound);

  void findNearHollow(vector<uchar> & colors,vector<bool> & wave_peak,vector<bool> & wave_hollow,vector<peak> & peak_count);
  void findNearHollow(vector<uchar> & colors,vector<bool> & wave_peak,vector<bool> & wave_hollow,vector<peak> & peak_count, unsigned int green_bound);

  bool IsWhitePoint(std::vector<DPoint2i> & pts,double _color_average,vector<uchar> & colors,peak & peak_count );
  void setHLow(int h_low) { h_low_ = h_low; }
  void setHHigh(int h_high) { h_high_ = h_high; }
  void setMergeWave(int merge) { merge_wave_ = merge; }
  void setNumPtsLine(int num) { nums_pts_line_ = num; }

  void calculateWeights();

  void showWhitePoints(cv::Mat & _img);

  void showWhitePoints(cv::Mat &  _img,DPoint _robot_loc, Angle _angle, int filed_length =1920,int filed_width=1314);

  ScanPoints   * scanpts_;
  Transfer     * transfer_;
  fukuro::CoordinateTransform* c_transform;

  vector<DPoint2i> img_white_;
  vector<DPoint>    real_pos_white;
  vector<PPoint>   robot_white_;
  vector<double>   weights_;

private:
  int h_low_;
  int h_high_;
  int nums_pts_line_;
  int filter_width_;
  int merge_wave_;
  float t_new[256];

private:
  void calculateRealPos();
};

}
#endif  //!__NUBOT_VISION_WHITES_H_

