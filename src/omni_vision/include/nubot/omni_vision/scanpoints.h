#ifndef __NUBOT_VISION_SCANPOINTS_H_
#define __NUBOT_VISION_SCANPOINTS_H_


#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

#include "nubot/core/core.hpp"
#include "nubot/omni_vision/omniimage.h"

#define VISION_COLORSEGMENT_YELLOW	0	//Yellow represent for football
#define VISION_COLORSEGMENT_BLACK	1	//green represent for ground
#define VISION_COLORSEGMENT_GREEN	2	//green represent for ground
#define VISION_COLORSEGMENT_UNKNOWCOLOR	3	//Unknown color

namespace nubot
{ 
using std::vector;
using std::pair;

class ScanPoints
{
public:
  ScanPoints(OmniImage & _omni_img);
  vector<DPoint2i>  scanPolarLine(const Angle & ang);
  vector<DPoint2i>  scanVertiLine(const DPoint2i & startPt,const DPoint2i & endPt);
  vector<DPoint2i>  scanHorizLine(const DPoint2i & startPt,const DPoint2i & endPt);
  vector<DPoint2i>  scanLine(const DPoint2i & startPt,const DPoint2i & endPt);

  void showScanPoints();

  void process();
  void filter();
  void findFieldBoundary(cv::Mat& segment_image, unsigned char target_color=VISION_COLORSEGMENT_GREEN);
  OmniImage * omni_img_;           //<-image information

  vector<unsigned int> green_line;
  vector<pair<DPoint2i,DPoint2i>> verti_pts_green_bound;
  vector<pair<DPoint2i,DPoint2i>> horiz_pts_green_bound;
  vector< vector<DPoint2i> > polar_pts_;
  vector< vector<DPoint2i> > verti_pts_;
  vector< vector<DPoint2i> > horiz_pts_;
  vector< vector<uchar> >    polar_pts_y_;
  vector< vector<uchar> >    verti_pts_y_;
  vector< vector<uchar> >    horiz_pts_y_;


  vector< vector<uchar> >    filter_polar_pts_y_;
  vector< vector<uchar> >    filter_verti_pts_y_;
  vector< vector<uchar> >    filter_horiz_pts_y_;


private:
  int    filter_width_;
  int    interval_;
  double ratio_;
};



}
#endif  //!__NUBOT_VISION_SCANPOINTS_H_

