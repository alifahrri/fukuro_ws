#ifndef COORDINATETRANSFORM_H
#define COORDINATETRANSFORM_H

#include <cmath>
#include <map>
#include <cstdlib>

namespace fukuro {

class CoordinateTransform
{
public:
  typedef std::pair<double,double> Parameter;
  typedef std::map<double,Parameter> ParameterList;
  CoordinateTransform();
  CoordinateTransform(ParameterList param_list);
  void add(double key, Parameter param);
  void save(std::string ini_file);
  void load(std::string ini_file);
  void reset();
  bool hasParamAt(double key);
  double value(double angle, double radius);
  Parameter getParam(double key);
  ParameterList getParamList();
private:
  ParameterList parameter_list;
};

}

#endif // COORDINATETRANSFORM_H
