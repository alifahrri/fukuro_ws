#ifndef KALMANFILTER_H
#define KALMANFILTER_H

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

#define KF2D

class KalmanFilter
{
public:
    KalmanFilter();
    void setT(double t);
#ifdef KF2D
    void setState(double x1, double x2);
    void setControl(double u1, double u2);
    void setMeasurement(double z1, double z2);
    void update(double T, double u1, double u2, double z1, double z2);
#else
    void setState(double x1, double x2, double x3);
    void setControl(double u1, double u2, double u3);
    void setMeasurement(double z1, double z2, double z3);
    double getRotation();
    void update(double T, double u1, double u2, double u3, double z1, double z2, double z3);
#endif
    double getX();
    double getY();

private:
#ifdef KF2D
    Eigen::Vector2d x_, x, u, e, z;
    Eigen::Matrix2d A, B, P, P_, Q, R, H, K;
#else
    Eigen::Vector3d x_, x, u, e, z;
    Eigen::Matrix3d A, B, P, P_, Q, R, H, K;
#endif
    double T;
    double rv, qv;
    double X, Y, Rot;
    void estimate();
    void measure();
    bool first_update;
};

#endif // KALMANFILTER_H
