#include <ros/ros.h>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <exception>
#include "kalmanfilter.h"
#include "fukuro_common/WorldModel.h"
#include "fukuro_common/OdometryInfo.h"
#include "fukuro_common/LocalVision.h"
#include "fukuro_common/Localization.h"
#include "fukuro_common/Balls.h"
#include "fukuro_common/Obstacles.h"
#include "nubot_common/OminiVisionInfo.h"
#include "nubot_common/RobotInfo.h"
#include "geometry_msgs/Point.h"

#define ANGLE_SHIFT -M_PI_2

//#define ODOMETRY_AMCL
#define ODOMETRY

typedef boost::function<void(const fukuro_common::OdometryInfo::ConstPtr&)> OdometryCallback;
typedef boost::function<void(const fukuro_common::LocalVision::ConstPtr&)> VisionCallback;
typedef boost::function<void(const nubot_common::OminiVisionInfo::ConstPtr&)> OmniVisionCallback;

namespace fukuro {
class WorldModelPublisher
{
public:
    typedef std::chrono::high_resolution_clock Clock;
    typedef std::chrono::high_resolution_clock::time_point TimePoint;
    typedef std::chrono::duration<double,std::milli> Duration;

    WorldModelPublisher(ros::Publisher &pub, std::string name) : publisher(pub)
    {
        world.balls.clear();
        world.obstacles.clear();
        world.robot_name = name;
        world.velocity.x = 0.0;
        world.velocity.y = 0.0;
        world.velocity.theta = 0.0;
        world.pose.x = 0.0;
        world.pose.y = 0.0;
        world.pose.theta = 0.0;
        world.ball_visible = false;
    }

    ~WorldModelPublisher()
    {
        running = false;
        if(thread.joinable())
            thread.join();
    }

    void start()
    {
        running = true;
        thread = std::thread(&WorldModelPublisher::loop,this);
    }

    void stop() { running = false; }
    void update(const nubot_common::OminiVisionInfo::ConstPtr& omni_vision_info);
    void update(const fukuro_common::LocalVision::ConstPtr& vision_info);
    void update(const fukuro_common::OdometryInfo::ConstPtr& odometry_info);
    void updateLocalization(const fukuro_common::Localization::ConstPtr& localization_info);
    void updateBalls(const fukuro_common::Balls::ConstPtr& local_balls_info);
    void updateObstacles(const fukuro_common::Obstacles::ConstPtr& local_obstacles_info);
private:
    void loop();
private:
    KalmanFilter local_ball_pos_kf;
    ros::Publisher& publisher;
    std::thread thread;
    std::mutex mutex;
    std::atomic_bool running;
    fukuro_common::WorldModel world;
};
}

int main(int argc, char** argv)
{
    ros::init(argc,argv,"fukuro_world_model");
    ros::NodeHandle node;
    std::string robot_name;
    if(!ros::get_environment_variable(robot_name,"FUKURO"))
    {
        ROS_ERROR("Robot name empty! export FUKURO=fukuro1");
        exit(-1);
    }
    std::string topic_name = robot_name + std::string("/world_model");
    std::string vision_topic_name = robot_name + std::string("/vision_info");
    std::string odometry_topic_name = robot_name + std::string("/odometry_info");
    std::string omnivision_topic_name = std::string("/omnivision/OmniVisionInfo");
    ros::Publisher world_model_pub = node.advertise<fukuro_common::WorldModel>(topic_name,50);
    fukuro::WorldModelPublisher world_model(world_model_pub,robot_name);
    VisionCallback vision_cb = [&world_model](const fukuro_common::LocalVision::ConstPtr& vision_info)
    {
        //    world_model.update(vision_info);
    };
    OdometryCallback odometry_cb = [&world_model](const fukuro_common::OdometryInfo::ConstPtr& odometry_info)
    {
        world_model.update(odometry_info);
    };
    OmniVisionCallback omni_vision_cb = [&world_model](const nubot_common::OminiVisionInfo::ConstPtr& omni_info)
    {
        world_model.update(omni_info);
    };

    //  ros::Subscriber vision_sub = node.subscribe(vision_topic_name,10,vision_cb);
    ros::Subscriber odometry_sub = node.subscribe(odometry_topic_name,50,odometry_cb);
    ros::Subscriber omni_vision_sub = node.subscribe(omnivision_topic_name,50,omni_vision_cb);
    //ros::Subscriber local_ball_sub = node.subscribe("/omnivision/Balls",10,&fukuro::WorldModelPublisher::updateBalls,&world_model);
    ros::Subscriber localization_sub = node.subscribe("/localization",50,&fukuro::WorldModelPublisher::updateLocalization,&world_model);
    ros::Subscriber obstacles_sub = node.subscribe("/omnivision/Obstacles",50,&fukuro::WorldModelPublisher::updateObstacles,&world_model);

    world_model.start();
    ros::AsyncSpinner spinner(2);
    if(spinner.canStart())
        spinner.start();
    else
    {
        std::cout << "can't start spinner\n";
        exit(-1);
    }
    ros::waitForShutdown();
    return 0;
}

namespace fukuro {

void WorldModelPublisher::loop()
{
    auto wait_ms = Duration{30.0};
    //ros::Rate loop_rate(10);
    while(running && ros::ok())
    {
        auto t0 = Clock::now();
        mutex.lock();
        try{
            publisher.publish(world);
        }
        catch(std::exception& e){
            std::cout<<"[WORLD MODEL PUBLISHER ERROR] "<<e.what()<<std::endl;
        }
        mutex.unlock();
        auto t1 = Clock::now();
        Duration dt = t1-t0;
        if(dt<wait_ms)
            std::this_thread::sleep_for(wait_ms-dt);
        else
            std::this_thread::sleep_for(wait_ms);

        //loop_rate.sleep();

    }
}

void WorldModelPublisher::update(const fukuro_common::OdometryInfo::ConstPtr &odometry_info)
{
    mutex.lock();
    try{
#ifdef ODOMETRY
        world.pose.x = odometry_info->x;
        world.pose.y = odometry_info->y;
        world.pose.theta = odometry_info->theta;
#endif
        world.velocity.x = odometry_info->Vx;
        world.velocity.y = odometry_info->Vy;
        world.velocity.theta = odometry_info->w;
        //  world.ball_visible = false;
        world.local_balls.clear();
    }
    catch(std::exception& e){
        std::cout<< "[ODOMETRY INFO ERROR] "<<e.what()<<std::endl;
    }
    mutex.unlock();
}

void WorldModelPublisher::update(const fukuro_common::LocalVision::ConstPtr &vision_info)
{
    mutex.lock();
    try{
        world.balls.clear();
        world.obstacles.clear();
        auto c = cos(world.pose.theta);
        auto s = sin(world.pose.theta);
        double rx = world.pose.x;
        double ry = world.pose.y;
        for(size_t i=0; i<vision_info->balls.size(); ++i)
        {
            geometry_msgs::Point ball;
            auto ball_x = c*vision_info->balls[i].x - s*vision_info->balls[i].y;
            auto ball_y = s*vision_info->balls[i].x + c*vision_info->balls[i].y;
            ball.x = ball_x + rx;
            ball.y = ball_y + ry;
            world.balls.push_back(ball);
        }
        for(size_t i=0; i<vision_info->obstacles.size(); ++i)
        {
            geometry_msgs::Point obstacle;
            auto obs_x = c*vision_info->obstacles[i].x - s*vision_info->obstacles[i].y;
            auto obs_y = s*vision_info->obstacles[i].x + c*vision_info->obstacles[i].y;
            obstacle.x = obs_x + rx;
            obstacle.y = obs_y + ry;
            world.obstacles.push_back(obstacle);
        }
    }
    catch(std::exception& e){
        std::cout<< "[VISION INFO ERROR] "<<e.what()<<std::endl;
    }
    mutex.unlock();
}

void WorldModelPublisher::update(const nubot_common::OminiVisionInfo::ConstPtr& omni_vision_info)
{
    static TimePoint t0 = Clock::now();
    static double u1 = 0.0;
    static double u2 = 0.0;
    TimePoint t1 = Clock::now();
    t0 = t1;
    Duration dt = t1-t0;
    mutex.lock();
    try{
        std::system("clear");
        std::cout << "OmniVisionInfo : \n";
        std::cout << "ball info\n";
        std::cout << "\tballinfostate : " << omni_vision_info->ballinfo.ballinfostate << '\n';
        std::cout << "\tpos known : " << omni_vision_info->ballinfo.pos_known << '\n';
        std::cout << "\tpos : " << omni_vision_info->ballinfo.pos.x << ", "
                  << omni_vision_info->ballinfo.pos.y << '\n';
        std::cout << "\treal pos : " << omni_vision_info->ballinfo.real_pos.angle << ", "
                  << omni_vision_info->ballinfo.real_pos.radius << '\n';
        std::cout << "\tvelocity known : " << omni_vision_info->ballinfo.velocity_known << '\n';
        std::cout << "\tvelocity : " << omni_vision_info->ballinfo.velocity.x << ", "
                  << omni_vision_info->ballinfo.velocity.y << '\n';
        std::cout << "\tpixel pos : " << omni_vision_info->ballinfo.pixel_pos.x << ", "
                  << omni_vision_info->ballinfo.pixel_pos.y << '\n';
        std::cout << "\tpos real : " << omni_vision_info->ballinfo.pos_real.angle << ", "
                  << omni_vision_info->ballinfo.pos_real.radius << '\n';
        std::cout << "RobotInfo : " << omni_vision_info->robotinfo.size() << '\n';
        auto c = cos(world.pose.theta);
        auto s = sin(world.pose.theta);
        world.balls.clear();
        world.local_balls.clear();
        world.ball_visible = omni_vision_info->ballinfo.pos_known ? true : false;
        double rx = world.pose.x;
        double ry = world.pose.y;
        geometry_msgs::Point ball;
        geometry_msgs::Point local_ball;
        geometry_msgs::Point local_ball_kf;
        geometry_msgs::Point global_ball_kf;
        auto angle = omni_vision_info->ballinfo.pos_real.angle;
        auto radius = omni_vision_info->ballinfo.pos_real.radius;
        auto b_x = cos(angle+(ANGLE_SHIFT))*radius;
        auto b_y = -(sin(angle+(ANGLE_SHIFT))*radius);
        double last_kf_x = local_ball_pos_kf.getX();
        double last_kf_y = local_ball_pos_kf.getY();
        local_ball.x = b_x;
        local_ball.y = b_y;
        local_ball_pos_kf.update(dt.count()/1000.0,u1,u2,local_ball.x,local_ball.y);
        u1 = local_ball_pos_kf.getX() - last_kf_x;
        u2 = local_ball_pos_kf.getY() - last_kf_y;
        local_ball_kf.x = local_ball_pos_kf.getX();
        local_ball_kf.y = local_ball_pos_kf.getY();
        std::cout << "ball x : " << b_x << '\n';
        std::cout << "ball y : " << b_y << '\n';
        auto ball_x = c*b_x-s*b_y;
        auto ball_y = s*b_x+c*b_y;
        ball.x = ball_x + rx;
        ball.y = ball_y + ry;
        world.local_balls_kf = local_ball_kf;
        global_ball_kf.x = c*local_ball_kf.x - s*local_ball_kf.y + rx;
        global_ball_kf.y = s*local_ball_kf.x + c*local_ball_kf.y + ry;
        world.global_balls_kf = global_ball_kf;
        world.balls.push_back(ball);
        world.local_balls.push_back(local_ball);
    }
    catch(std::exception& e){
        std::cout<< "[OMNIVISION INFO ERROR] "<<e.what()<<std::endl;
    }
    mutex.unlock();
}

void WorldModelPublisher::updateLocalization(const fukuro_common::Localization::ConstPtr& localization_info)
{
    try{
#ifdef ODOMETRY_AMCL
        world.pose.x = localization_info->belief.x/100.0;
        world.pose.y = localization_info->belief.y/100.0;
        double angle_deg = localization_info->belief.theta > 180.0 ? localization_info->belief.theta-360.0 : localization_info->belief.theta;
        double angle_rad = angle_deg*M_PI/180.0;
        world.pose.theta = angle_rad;
#endif
    }
    catch(std::exception& e){
        std::cout<< "[LOCALIZATION INFO ERROR] "<<e.what()<<std::endl;
    }
}

void WorldModelPublisher::updateBalls(const fukuro_common::Balls::ConstPtr& local_balls_info)
{
    static TimePoint last_detect_ball = Clock::now();
    double c = cos(world.pose.theta);
    double s = sin(world.pose.theta);
    double robot_x = world.pose.x;
    double robot_y = world.pose.y;
    double nearest_ball_radius = 100.0;
    int nearest_ball_index = -1;
    for(size_t i=0; i<local_balls_info->balls.size(); i++)
    {
        double local_ball_x = local_balls_info->balls.at(i).x/100.0;
        double local_ball_y = local_balls_info->balls.at(i).y/100.0;
        double radius = sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y);
        if(radius<nearest_ball_radius)
        {
            nearest_ball_radius = radius;
            nearest_ball_index = i;
        }
    }
    TimePoint now = Clock::now();
    if(nearest_ball_index<0)
    {
        Duration dt = now - last_detect_ball;
        if(dt.count()>1500.0)
            world.ball_visible = false;
        else
            world.ball_visible = true;
    }
    else
    {
        world.ball_visible = true;
        last_detect_ball = now;
        if(!world.local_balls.size())
            world.local_balls.resize(1);
        geometry_msgs::Point ball;
        ball.x = local_balls_info->balls.at(nearest_ball_index).x/100.0;
        ball.y = local_balls_info->balls.at(nearest_ball_index).y/100.0;
        world.local_balls.at(0) = ball;
    }
}

void WorldModelPublisher::updateObstacles(const fukuro_common::Obstacles::ConstPtr& local_obstacles_info)
{
    try{
        double c = cos(world.pose.theta);
        double s = sin(world.pose.theta);
        double robot_x = world.pose.x;
        double robot_y = world.pose.y;
        world.obstacles.clear();
        for(size_t i=0; i<local_obstacles_info->obstacles.size(); i++)
        {
            geometry_msgs::Point obstacles;
            //    double obstacle_x = local_obstacles_info->obstacles[i].x/100.0;
            //    double obstacle_y = local_obstacles_info->obstacles[i].y/100.0;
            double obstacle_x = local_obstacles_info->obstacles[i].x;
            double obstacle_y = local_obstacles_info->obstacles[i].y;
            obstacles.x = c*obstacle_x - s*obstacle_y + robot_x;
            obstacles.y = s*obstacle_x + c*obstacle_y + robot_y;
            world.obstacles.push_back(obstacles);
        }
    }
    catch(std::exception& e){
        std::cout<< "[OBSTACLE INFO ERROR] "<<e.what()<<std::endl;
    }
}

}
