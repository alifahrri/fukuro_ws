#include "kalmanfilter.h"
#include <stdio.h>
#include <iostream>

#define QVAL 0.01
#define RVAL 0.01

KalmanFilter::KalmanFilter() :
    T(1)
{
    x(0) = 0;
    x(1) = 0;

    H.setIdentity();
    P.setIdentity();
    P_.setIdentity();
    first_update = true;

#ifdef KF2D
    Q << QVAL*QVAL, QVAL*QVAL/10,
            QVAL*QVAL/10, QVAL*QVAL;

    R << RVAL*RVAL, RVAL*RVAL/10,
            RVAL*RVAL/10, RVAL*RVAL;
#else
    x(2) = 0;
    Q << QVAL*QVAL, QVAL*QVAL/10, QVAL*QVAL/10,
            QVAL*QVAL/10, QVAL*QVAL, QVAL*QVAL/10,
            QVAL*QVAL/10, QVAL*QVAL/10, QVAL*QVAL;

    R << RVAL*RVAL, RVAL*RVAL/10, RVAL*RVAL/10,
            RVAL*RVAL/10, RVAL*RVAL, RVAL*RVAL/10,
            RVAL*RVAL/10, RVAL*RVAL/10, RVAL*RVAL;
#endif
    std::cout << "[KalmanFilter] matrix R :\n"<< R <<std::endl;
    std::cout << "[KalmanFilter] matrix Q :\n"<< Q <<std::endl;
}

double KalmanFilter::getX()
{
    return X;
}

double KalmanFilter::getY()
{
    return Y;
}


#ifdef KF2D
void KalmanFilter::update(double T, double u1, double u2, double z1, double z2)
{
    if(first_update){
        setState(z1, z2);
        first_update = false;
    }
    else {
        //printf("[update filter] T:%f u1:%f u2:%f u3:%f z1:%f z2:%f z3:%f\n",T,u1,u2,u3,z1,z2,z3);
        setT(T);
        setControl(u1, u2);
        setMeasurement(z1, z2);
        estimate();
        measure();
//        std::cout << "X:" << x << std::endl;
    }
    X = x(0);
    Y = x(1);
}

void KalmanFilter::setState(double x1, double x2)
{
    x(0) = x1;
    x(1) = x2;
}

void KalmanFilter::setMeasurement(double z1, double z2)
{
    z(0) = z1;
    z(1) = z2;
}

void KalmanFilter::setControl(double u1, double u2)
{
    u(0) = u1;
    u(1) = u2;
}

#else
void KalmanFilter::update(double T, double u1, double u2, double u3, double z1, double z2, double z3)
{
    if(first_update){
        setState(z1, z2, z3);
//        setControl(0,0,z3);
        first_update = false;
    }
    else {
        //printf("[update filter] T:%f u1:%f u2:%f u3:%f z1:%f z2:%f z3:%f\n",T,u1,u2,u3,z1,z2,z3);
        setT(T);
        setControl(u1, u2, u3);
        setMeasurement(z1, z2, z3);
        estimate();
        measure();
//        std::cout << "X:" << x << std::endl;
    }
    X = x(0);
    Y = x(1);
    Rot = x(2);
}

void KalmanFilter::setState(double x1, double x2, double x3)
{
    x(0) = x1;
    x(1) = x2;
    x(2) = x3;
}

double KalmanFilter::getRotation()
{
    return Rot;
}
void KalmanFilter::setMeasurement(double z1, double z2, double z3)
{
    z(0) = z1;
    z(1) = z2;
    z(2) = z3;
}

void KalmanFilter::setControl(double u1, double u2, double u3)
{
    u(0) = u1;
    u(1) = u2;
    u(2) = u3;
}

#endif

void KalmanFilter::setT(double t)
{
    T = t;
}

void KalmanFilter::estimate()
{
#ifdef KF2D
    B << T, 0,
	 0, T;
#else
    double psi = x(2);
    B << T*cos(psi), -T*sin(psi), 0,
            T*sin(psi), T*cos(psi), 0,
            0, 0, T;

//    std::cout << "Psi : " << psi << "B:\n" << B << std::endl;
#endif
    x_ = x + B*u;
    A.setIdentity();
    P_ = A*P*A.transpose() + Q;
}

void KalmanFilter::measure()
{
    K = P_*H.transpose()*(H*P_*H.transpose()+R).inverse();
    x = x_ + K*(z-H*x_);
    P = (Eigen::Matrix2d::Identity() - K*H)*P_;
#ifdef KF2D
#else
    P = (Eigen::Matrix3d::Identity() - K*H)*P_;
#endif
}

