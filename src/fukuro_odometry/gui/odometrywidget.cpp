#include "odometrywidget.h"
#include "ui_odometrywidget.h"
#include <QPainter>

#define ANGLE_INCREMENT 3

#define CENTER_RADIUS CENTER_CIRCLE_RADIUS

#define TO_DEGREE 180.0/M_PI

OdometryWidget::OdometryWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::OdometryWidget),
  field(new Field),
  robot(new Robot)
{
  ui->setupUi(this);
  ui->graphicsView->setScene(new QGraphicsScene(-FIELD_WIDTH/2,-FIELD_HEIGHT/2,FIELD_WIDTH,FIELD_HEIGHT,this));
  ui->graphicsView->scene()->addItem(field);
  ui->graphicsView->scene()->addItem(robot);
  ui->graphicsView->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
  setWindowTitle("odometry");
}

void OdometryWidget::updateOdometry(const fukuro_common::OdometryInfoConstPtr &msg)
{
  auto pos = QPointF(msg->pos.x,msg->pos.y * (-1));
  this->robot->vel = QPointF(msg->vel.x, msg->vel.y* (-1));
  this->robot->center = pos;
  this->robot->angle = msg->pos.theta;
  this->robot->path.push_back(pos);
  if(robot->path.size() > 300)
    robot->path.pop_front();
  this->ui->graphicsView->scene()->update();
}

OdometryWidget::~OdometryWidget()
{
  delete ui;
}

OdometryWidget::Field::Field()
{
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE7,YLINE1)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE6),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE1,YLINE1),
                         QPointF(XLINE1,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE7,YLINE1),
                         QPointF(XLINE7,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE4,YLINE1),
                         QPointF(XLINE4,YLINE6)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE2,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE6,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE3,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE5,YLINE5)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE3),
                         QPointF(XLINE1,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE2,YLINE4),
                         QPointF(XLINE1,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE2),
                         QPointF(XLINE1,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE3,YLINE5),
                         QPointF(XLINE1,YLINE5)));

  lines.push_back(QLineF(QPointF(XLINE6,YLINE3),
                         QPointF(XLINE7,YLINE3)));
  lines.push_back(QLineF(QPointF(XLINE6,YLINE4),
                         QPointF(XLINE7,YLINE4)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE2),
                         QPointF(XLINE7,YLINE2)));
  lines.push_back(QLineF(QPointF(XLINE5,YLINE5),
                         QPointF(XLINE7,YLINE5)));

  center_circle.setLeft(-CENTER_RADIUS);
  center_circle.setTop(-CENTER_RADIUS);
  center_circle.setHeight(2*CENTER_RADIUS);
  center_circle.setWidth(2*CENTER_RADIUS);

  QPointF circle_line0(center_circle.width()/2,0.0);
  QLineF line(QPointF(0.0,0.0),circle_line0);
  circle_lines.push_back(QLineF(circle_line0,line.p2()));
  for(int i=ANGLE_INCREMENT; i<=360; i+=ANGLE_INCREMENT)
  {
      line.setAngle(i);
      QPointF p0 = circle_lines.back().p2();
      circle_lines.push_back(QLineF(p0,line.p2()));
    }
}

void OdometryWidget::Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setPen(Qt::transparent);
  painter->setBrush(Qt::green);
  painter->drawRect(boundingRect());
  painter->setPen(QPen(Qt::white,3.3));
  painter->setBrush(Qt::transparent);
  painter->drawLines(lines);
  painter->drawEllipse(center_circle);
}

QRectF OdometryWidget::Field::boundingRect() const
{
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}

OdometryWidget::Robot::Robot()
{
  auto px = QPixmap(":/fukuro.png");
  auto mat = QMatrix();
  mat.rotate(-90.0);
  pxmap = new QPixmap(px.scaledToHeight(50,Qt::SmoothTransformation).transformed(mat,Qt::SmoothTransformation));
}

void OdometryWidget::Robot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  auto m0 = painter->matrix();
  painter->translate(center);
  painter->rotate(-angle);
  auto offset = QPointF(pxmap->width()/2,pxmap->height()/2);
  painter->drawPixmap(QPointF(0.0,0.0)-offset,*pxmap);
  //  painter->setPen(Qt::green);
  //  painter->setBrush(Qt::black);
  //  painter->drawEllipse(QPoint(0,0),25,25);
  //  painter->setPen(QPen(Qt::black,2.0));
  //  painter->drawLine(QPoint(0,0),QPoint(50,0));
  auto vel_length = vel.manhattanLength()*3.0;
  auto vel_angle = atan2(vel.y(), vel.x());
  auto vel_arrow1 = QLineF::fromPolar(vel_angle*0.5,-45.0).translated(vel_length,0);
  auto vel_arrow2 = QLineF::fromPolar(vel_angle*0.5,45.0).translated(vel_length,0);
  painter->rotate(-vel_angle);
  painter->setPen(Qt::red);
  auto pen = painter->pen();
  pen.setWidthF(3.0);
  painter->setPen(pen);
  painter->drawLine(QPoint(0,0),QPoint(vel_length,0));
  painter->drawLine(vel_arrow1);
  painter->drawLine(vel_arrow2);
  painter->setPen(Qt::blue);
  painter->setMatrix(m0);
  painter->drawPolyline(path);
}

QRectF OdometryWidget::Robot::boundingRect() const
{
  auto offset = QPointF(50,50);
//  return QRectF(center-offset,center+offset);
  return QRectF(XLINE7-100,YLINE6-100,FIELD_WIDTH+200,FIELD_HEIGHT+200);
}
