#ifndef ODOMETRYWIDGET_H
#define ODOMETRYWIDGET_H

#include <QWidget>
#include <QGraphicsItem>
#include "fukuro_common/OdometryInfo.h"
#include "fukuro/core/field.hpp"

namespace Ui {
  class OdometryWidget;
}

class OdometryWidget : public QWidget
{
  Q_OBJECT

public:
  explicit OdometryWidget(QWidget *parent = 0);
  void updateOdometry(const fukuro_common::OdometryInfoConstPtr &msg);
  ~OdometryWidget();

private:
  class Robot : public QGraphicsItem
  {
  public:
    Robot();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
  public:
    QPointF center;
    QPointF vel;
    qreal angle{0.0};
    QVector<QPointF> path;
  private:
    QPixmap *pxmap;
  };

private:
  class Field : public QGraphicsItem
  {
  public:
    Field();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

  private:
    QVector<QLineF> lines;
    QVector<QLineF> circle_lines;
    QRectF center_circle;
  };

private:
  Ui::OdometryWidget *ui;
  Field *field;
  Robot *robot;
};

#endif // ODOMETRYWIDGET_H
