# Overview  
this package include node for odometry and gui node for odometry visualization

## fukuro_odometry_node  
### Published Topic  
* topic name `/robot_name/odometry` of type `fukuro_common/OdometryInfo`  

### Subscribed Topic
* topic name `/robot_name/hwcontrol_info` of type `fukuro_common/HWController`
* subscribed topic from `fukuro_hw_controller_node`  

## fukuro_odometry_gui_node  
provides visualization for odometry result  
### Published Topic
* None  

### Subscribed Topic  
* `/robot_name/odometry` of type `fukuro_common/OdometryInfo`  

## Unit  
* position in meters  
* velocity in meters/second  
* orientation in radian  
