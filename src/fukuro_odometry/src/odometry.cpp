#include "odometry.h"
#include <fstream>
#include <yaml-cpp/yaml.h>

#define TO_RAD M_PI/180.0

#define D 0.0584

#define C M_PI*D //circumference of wheel
#define PPR 200 //pulse per rotation
#define ENCODER_RATIO (C/PPR)

//#define ENCODER_RATIO       0.00781818

#define MOTOR1_INDEX        0
#define MOTOR2_INDEX        1
#define MOTOR3_INDEX        2
#define MOTOR4_INDEX        3

Odometry::Odometry(ros::NodeHandle &node)
  : robot(State({{0.0,0.0,0.0},{0.0,0.0,0.0}})),
    k(Pose({1.0,1.0,1.0})),
    pos(Pose({0.0,0.0,0.0}))
{
  std::string robot_name;
  ros::get_environment_variable(robot_name,"FUKURO");
  pub = node.advertise<fukuro_common::OdometryInfo>(robot_name+"/odometry",1);
  //ROS_INFO_STREAM << "[ENCODER RATIO] " << ENCODER_RATIO;
}

bool Odometry::loadSettings(std::__cxx11::string file)
{
  ROS_WARN("load settings");

  std::ifstream fin(file);
  if(!fin.is_open())
  {
    ROS_ERROR("failed to load settings, check if file exist!");
    return false;
  }
  else
  {
    YAML::Node settings = YAML::Load(fin);

    k.x = settings["kx"].as<double>();
    k.y = settings["ky"].as<double>();
    k.w = settings["kw"].as<double>();

    ROS_WARN("load settings OK");
    return true;
  }
}

bool Odometry::saveSettings(std::__cxx11::string file)
{
  ROS_WARN("save settings");
  YAML::Node settings /*= YAML::LoadFile(file)*/;
  settings["kx"] = k.x;
  settings["ky"] = k.y;
  settings["kw"] = k.w;

  std::ofstream fout(file);
  if(!fout.is_open())
  {
    ROS_ERROR("failed to save settings, check if directory is exist!");
    return false;
  }
  else
  {
    fout << settings;
    fout.close();
    ROS_WARN("save settings OK");
    return true;
  }
}

void Odometry::updateOdometry(const fukuro_common::HWControllerConstPtr &msg)
{
#if 1
  m.lock();
  auto vx = k.x * msg->vel.x;
  auto vy = k.y * msg->vel.y;
  auto w = k.w * msg->vel.theta;
  auto c = cos(robot.pos.w * TO_RAD);
  auto s = sin(robot.pos.w * TO_RAD);
  auto gvx = c*vx - s*vy;
  auto gvy = s*vx + c*vy;
  robot.vel.x += vx;
  robot.vel.y += vy;
  robot.vel.w += w;
  robot.pos.x += gvx;
  robot.pos.y += gvy;
  robot.pos.w += w;
  normalizeAngle(&robot.pos.w);
  m.unlock();
#else
  m.lock();
  robot.vel.x += k.x * msg->vel.x;
  robot.vel.y += k.y * msg->vel.y;
  robot.vel.w += k.x * msg->vel.theta;
  m.unlock();
#endif
}

void Odometry::calibrate(fukuro_common::OdometryCalibrationServiceRequest &request, fukuro_common::OdometryCalibrationServiceResponse &response)
{
  m.lock();
  k.x = robot.pos.x / request.x;
  k.y = robot.pos.y / request.y;
  k.w = robot.pos.w / request.w;
  robot.pos.x = request.x;
  robot.pos.y = request.y;
  robot.pos.w = request.w;
  m.unlock();
  response.ok = 1;
}

bool Odometry::reset(fukuro_common::OdometryService::Request &request, fukuro_common::OdometryService::Response &response)
{
  m.lock();
  robot.pos.x = request.x;
  robot.pos.y = request.y;
  robot.pos.w = request.w;
  last_pos = robot.pos;
  m.unlock();
  response.ok = 1;
  return true;
}

/*
void Odometry::resetLocalization(fukuro_common::LocalizationService::Request &req, fukuro_common::LocalizationService::Response &res){
    m.lock();
    robot.pos.x = req.x;
    robot.pos.y = req.y;
    robot.pos.w = req.w;
    last_pos = robot.pos;
    m.unlock();
    response.ok = 1;
}
*/

void Odometry::process()
{
#if 0
  robot.vel.x = robot.pos.x - pos.x;
  robot.vel.y = robot.pos.y - pos.y;
  robot.vel.w = robot.pos.w - pos.w;
#endif
}

void Odometry::publish()
{
  fukuro_common::OdometryInfo msg;
  //  auto dw = angleDiff(robot.pos.w, last_pos.w);
  msg.pos.x = robot.pos.x;
  msg.pos.y = robot.pos.y;
  msg.pos.theta = robot.pos.w;
  // robot.vel.x = robot.pos.x - last_pos.x;
  // robot.vel.y = robot.pos.y - last_pos.y;
  // robot.vel.w = robot.pos.w - last_pos.w;
  msg.vel.x = robot.vel.x;
  msg.vel.y = robot.vel.y;
  msg.vel.theta = robot.vel.w;
  robot.vel.x = 0.0;
  robot.vel.y = 0.0;
  robot.vel.w = 0.0;
  last_pos = robot.pos;
  m.unlock();
  pub.publish(msg);
}

double Odometry::angleDiff(double angle0, double angle1)
{
  auto ret = angle0 - angle1;
  if((angle0 >= 0.00) && (angle0 < 90.0)) {
    if((angle1 <= 360.0) && (angle1 > 270.0)) {
      ret = angle0 + 360.0 - angle1;
      ret = -ret;
    }
  }
  return ret;
}

void Odometry::normalizeAngle(double *angle)
{
  while (*angle > 360.0) {
    (*angle) -= 360.0;
  }
  while(*angle < 0.0) {
    (*angle) += 360.0;
  }
}

void Odometry::encoderCallback(const fukuro_common::EncoderConstPtr &msg)
{
    double encoder_ratio = ENCODER_RATIO;
    double vm_1 = msg->current_tick[MOTOR1_INDEX]*encoder_ratio;
    double vm_2 = msg->current_tick[MOTOR2_INDEX]*encoder_ratio;
    double vm_3 = msg->current_tick[MOTOR3_INDEX]*encoder_ratio;
    //    double vm_4 = msg->current_tick[MOTOR4_INDEX]*encoder_ratio;

    // ang_rad = 2.0943951023931953
    // L =
    //
    // Matrix B
    // |      0            -1         L |
    // | -cos(ang_rad) sin(ang_rad)   L |
    // |  cos(ang_rad) sin(ang_rad)   L |

    //Matrix B inverse:
    auto vx =-0.5773472*  vm_1 + 0.5773472*vm_2  + 0*        vm_3;
    auto vy = 0.3333333*  vm_1 + 0.3333333*vm_2  - 0.6666667*vm_3;
    auto w  = 1.6835017*  vm_1 + 1.6835017*vm_2  + 1.6835017*vm_3;
    vx *= k.x;
    vy *= k.y;
    w *= k.w;
    m.lock();
    auto c = cos(robot.pos.w * TO_RAD);
    auto s = sin(robot.pos.w * TO_RAD);
    auto gvx = c*vx - s*vy;
    auto gvy = s*vx + c*vy;
    robot.vel.x += vx;
    robot.vel.y += vy;
    robot.vel.w += w;
    robot.pos.x += gvx;
    robot.pos.y += gvy;
    robot.pos.w += w;
    normalizeAngle(&robot.pos.w);
    m.unlock();
}
