#include <ros/ros.h>
#include <QApplication>
#include <QTimer>
#include "odometrywidget.h"

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_odometry_gui");
  ros::NodeHandle node;

  QApplication app(argc,argv);
  OdometryWidget odom_widget;
  odom_widget.show();

  std::string robot_name;
  ros::get_environment_variable(robot_name,"FUKURO");
  ros::Subscriber odom_sub = node.subscribe(robot_name+"/odometry",1,&OdometryWidget::updateOdometry,&odom_widget);
  ros::AsyncSpinner spinner(2);
  spinner.start();

  QTimer timer;
  timer.connect(&timer,&QTimer::timeout,[&]{
      if(!ros::ok())
        odom_widget.close();
    });
  timer.start(1000);

  app.exec();
  return 0;
}
