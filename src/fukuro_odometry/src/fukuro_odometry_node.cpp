#include <ros/ros.h>
#include "odometry.h"

int main(int argc, char **argv)
{
  ros::init(argc,argv,"fukuro_odometry");
  ros::NodeHandle node;
  Odometry odometry(node);

  std::string home;
  std::string robot_name;
  ros::get_environment_variable(robot_name,"FUKURO");
  ros::get_environment_variable(home,"HOME");
  std::string dir(home+"/fukuro_ws/src/fukuro_odometry/settings");
  std::string file(dir+"/"+robot_name+".yaml");
  odometry.loadSettings(file);
  if(!odometry.saveSettings(file))
    ROS_WARN("try $ mkdir -p %s",dir.c_str());
 
  //ros::ServiceServer localization_server = node.advertiseService("/localization_service",&Odometry::resetLocalization,&odometry);
  ros::ServiceServer odometry_server = node.advertiseService("/odometry_service",&Odometry::reset,&odometry);

#if 1
  ros::Subscriber encoder_sub = node.subscribe(robot_name+"/encoder",1,&Odometry::encoderCallback,&odometry);
#else
  ros::Subscriber hw_sub = node.subscribe(robot_name+"/hwcontrol_info",1,&Odometry::updateOdometry,&odometry);
#endif
  ros::AsyncSpinner spinner(2);
  ros::Rate rate(30);
  spinner.start();
  while(ros::ok())
  {
    odometry.publish();
    rate.sleep();
  }
  return 0;
}
