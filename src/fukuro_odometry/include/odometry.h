#ifndef ODOMETRY_H
#define ODOMETRY_H

#include <vector>
#include <ros/ros.h>
#include <mutex>
#include "fukuro_common/Encoder.h"
#include "fukuro_common/HWController.h"
#include "fukuro_common/OdometryInfo.h"
#include "fukuro_common/OdometryCalibrationService.h"
#include "fukuro_common/OdometryService.h"
//#include "fukuro_common/LocalizationService.h"

class Odometry
{
  struct Pose
  {
    double x;
    double y;
    double w;
  };

  struct State
  {
    Pose pos;
    Pose vel;
  };

public:
  Odometry(ros::NodeHandle &node);
  bool loadSettings(std::string file);
  bool saveSettings(std::string file);
  void encoderCallback(const fukuro_common::EncoderConstPtr &msg);
  void updateOdometry(const fukuro_common::HWControllerConstPtr &msg);
  void calibrate(fukuro_common::OdometryCalibrationServiceRequest &request, fukuro_common::OdometryCalibrationServiceResponse &response);
  bool reset(fukuro_common::OdometryService::Request &request, fukuro_common::OdometryService::Response &response);
  //void resetLocalization(fukuro_common::LocalizationService::Request &req, fukuro_common::LocalizationService::Response &res);
  void process();
  void publish();

private:
  /**
   * @brief angleDiff operation for angle0 - angle1
   * @param angle0
   * @param angle1
   * @return
   */
  double angleDiff(double angle0, double angle1);
  void normalizeAngle(double *angle);

private:
  ros::Publisher pub;
  Pose last_pos;
  std::mutex m;
  State robot;
  Pose pos;
  Pose k;
};

#endif // ODOMETRY_H
