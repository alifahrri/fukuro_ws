#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import rospy
import os
import thread
import time
from dynamic_reconfigure.server import Server
from dynamic_reconfigure.parameter_generator_catkin import *
from fukuro_common.msg import *
from fukuro_common.srv import *
from random import randint
from enum import Enum

class Tactics(Enum):
    OFFENSE = 1
    DEFENSE = 2

robot_tactics = Tactics.DEFENSE

goalie_name = 2

## ros publisher n server
robot_control_pub = None
kicker_service = None
strategy_info_pub = None

## ros message
robot_control_msg = RobotControl()
strategy_info_msg = StrategyInfo()

compass = 0.0

## informasi robot
ball_visible = False
local_ball_x = 0.0
local_ball_y = 0.0
global_ball_x = 0.0
global_ball_y = 0.0
robot_pose = (0.0,0.0,0.0)
role_list = ['defender',
            'defender2',
            'timer_defense',
            'timer_defense2',
            'striker',
            'striker_kick',
            'striker_dribble',
            'goalie',
            'timer_striker',
            'timer_striker2']
robot_role = 'striker_dribble'
last_ball_visible_time = rospy.Time()

## default home n kick
home_pos = (0.0,0.0,0.0)
kick_pos = [(4.0,0.0,0.0),(3.0,0.0,0.0),(3.0,1.0,math.atan2(6.25-3.0,-1.0)),(3.0,2.6,math.atan2(6.25-3.0,-2.6)),(3.0,-1.0,math.atan2(6.25-3.0,1.0)),(3.0,-2.6,math.atan2(6.25-3.0,2.6))]
dribble_to_goal_pos = (6.25,0.0,0.0)
kick_off_pos = [(0.0,1.3,-(math.pi/2.0)),(0.0,-1.3,math.pi/2.0)]
corner_kick_pos = [(-3.0,1.5,0.0),(-3.0,-1.5,0.0)]

## setting cari bola
cari_bola_pos = [(-2.5,1.0,math.pi),(2.5,-1.0,math.pi),(-2.5,-1.0,math.pi),(2.5,1.0,math.pi)]
waktu_cari_bola = 2.0
waypoint_cari_bola = 0
init_cari_bola = True
waktu_mulai_cari_bola = None

## setting dekati bola
radius_dribble_auto_on = 0.7 #meter
angle_dribble_auto_on = 0.2 #radian
radius_mulai_dribble_bola = 0.35 #meter
angle_mulai_dribble_bola = 0.2 #radian

## setting kick
min_error_posisi_kick = 0.2 #meter
min_error_sudut_kick = 0.1 #radian

## setting defender
defense_line = -3.5
defense_line2 = -4.0
timer_defense_init = True
timer_defense_t0 = None
defense_time = rospy.Duration(10)

## setting homing
min_error_posisi_homing = 0.25 #meter
min_error_sudut_homing = 0.1 #radian

#ball_shield_angle = math.pi
ball_shield_angle = 0.0

## strategy awal
strategy_state = 'stop'

## goalie stuff
goalie_intercept_pos = (-5.5, 0.0, 0.0)
goalie_intercept_valid = False

distance = 3.0

timer_t0 = None
timer_init = True
timer_duration = rospy.Duration(6)

error_radius = 0.0
error_angle = 0.0

use_ball_priority = False
use_kick_off = True

kick_index = 0
kick_random_state = True

error_positioning = 0.25
error_sudut_positioning = 0.25

radius_mulai_kick_off = 0.415

skip_timer = False

defense_lines = (defense_line,defense_line2)
obstacles = None
obstacle = (0.0,0.0,2*math.pi)

passing = False

manual_positioning = True
striker_kick_off_init_time = True
striker_kick_off_t0 = None
striker_kick_off_waiting_time = rospy.Duration(6.0)

last_position = ()

kick_off_state = False
kick_at_kick_pos = True
main_timer_strategy = False
robotname = int(os.getenv('AGENT')) - 1
teammates = [(0.0,0.0,0.0,0,False),(0.0,0.0,0.0,1,False),(0.0,0.0,0.0,2,False),(0.0,0.0,0.0,3,False),(0.0,0.0,0.0,4,False),(0.0,0.0,0.0,5,False)]
teammates_dist_to_ball = []
nearest_agent = robotname
distance_priority = [(True,0.0,0),(True,0.0,1),(True,0.0,2),(True,0.0,3),(True,0.0,4),(True,0.0,5)]
behavior_list = ['positioning','gameplay','stop']
robot_behavior = 'stop'
robot_connection = False
robot_state = 'welcome'
state_list = ['welcome',
            'our-kick-off',
            'opp-kick-off',
            'our-free-kick',
            'opp-free-kick',
            'our-goal-kick',
            'opp-goal-kick',
            'our-throw-in',
            'opp-throw-in',
            'our-corner-kick',
            'opp-corner-kick',
            'our-penalty',
            'opp-penalty',
            'our-goal',
            'opp-goal',
            'our-repair',
            'opp-repair',
            'our-red-card',
            'opp-red-card',
            'our-yellow-card',
            'opp-yellow-card',
            'start',
            'stop',
            'drop',
            'park',
            '1st',
            '2nd'
            '3rd'
            '4th']

home_pos_state = {
    'our-corner-kick' : (4.0,2.0,0.0),
    'opp-corner-kick' : (-3.2,2.0,3.14),
    'our-free-kick' : (-3.2,2.0,0.0),
    'opp-free-kick' : (-3.2,2.0,0.0),
    'drop' : (-2.0,1.0,0.0)
}

################################################################################################

## edit strategy disini
#################################################################################################

def stop() :
#    rospy.loginfo('stop')
    global strategy_state
    go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
    if robot_behavior == 'gameplay' :
        strategy_state = 'cari_bola'
    #go_to_pos(home_pos[0],home_pos[1],home_pos[2],local='local',dribbler=False)

def defender() :
    global strategy_state, timer_defense_init, timer_defense_t0, robot_role
    line = defense_line
    if not manual_positioning :
        go_to_pos(line,-1.0,0.0)
    else :
        stop()
    rospy.loginfo('defender()')

def striker() :
    global strategy_state
    strategy_state = 'cari_bola'

def striker_kick() :
    global strategy_state
    strategy_state = 'cari_bola'

def striker_dribble() :
    global strategy_state
    strategy_state = 'cari_bola'

def kick_off() :
    global strategy_state, robot_role, local_ball_x, local_ball_y
    global striker_kick_off_init_time, striker_kick_off_t0, striker_kick_off_waiting_time
    stop = (robot_behavior == 'stop')
    positioning = (robot_behavior == 'positioning')
    if not (manual_positioning or stop or positioning) :
        radius = math.hypot(local_ball_x,local_ball_y)
        angle = math.atan2(local_ball_y,local_ball_x)
        if robot_role == 'defender' :
            go_to_pos(local_ball_x, local_ball_y, angle, local='local')
            if radius <= radius_mulai_dribble_bola and math.fabs(angle) <= angle_mulai_dribble_bola :
                go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
                kick()
                strategy_state = 'defender'
        elif robot_role == 'striker' :
            now = rospy.Time.now()
            if striker_kick_off_init_time :
                striker_kick_off_t0 = now
                striker_kick_off_init_time = False
            else :
                if (now - striker_kick_off_t0) > striker_kick_off_waiting_time :
                    striker_kick_off_init_time = True
                    strategy_state = 'cari_bola'
            if radius <= radius_mulai_dribble_bola and math.fabs(angle) <= angle_mulai_dribble_bola :
                striker_kick_off_init_time = True
                strategy_state = 'cari_bola'
    else :
        go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
    rospy.loginfo('defender()')

def cari_bola() :
    global init_cari_bola, waktu_mulai_cari_bola, waypoint_cari_bola, strategy_state
    global robot_behavior
    stop = (robot_behavior == 'stop')
    positioning = (robot_behavior == 'positioning')
    if not (manual_positioning or stop or positioning) :
        if ball_visible :
            strategy_state = 'dekati_bola'
        if init_cari_bola :
            waktu_mulai_cari_bola = rospy.get_rostime()
            init_cari_bola = False
        pos = cari_bola_pos[waypoint_cari_bola]
        go_to_pos(pos[0],pos[1],pos[2],planning=True)
        durasi_cari_bola = rospy.get_rostime() - waktu_mulai_cari_bola
        if durasi_cari_bola.secs > waktu_cari_bola :
            init_cari_bola = True
            waypoint_cari_bola = waypoint_cari_bola + 1
            if waypoint_cari_bola >= len(cari_bola_pos) :
                waypoint_cari_bola = 0
    else :
        go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
    rospy.loginfo('cari bola')

def dekati_bola() :
    global strategy_state, robot_behavior, robot_connection, local_ball_x, local_ball_y, ball_visible, angle_dribble_auto_on, radius_dribble_auto_on, dribble
    global kick_off_state, angle_mulai_dribble_bola, radius_mulai_dribble_bola, robot_pose, robot_role, teammates_dist_to_ball, last_position
    stop = (robot_behavior == 'stop')
    positioning = (robot_behavior == 'positioning')
    if not (manual_positioning or stop or positioning) :
        radius = math.hypot(local_ball_x,local_ball_y)
        angle = math.atan2(local_ball_y,local_ball_x)
        dribble = False
        if radius < radius_dribble_auto_on and math.fabs(angle) < angle_dribble_auto_on :
            dribble = True
        if radius <= radius_mulai_dribble_bola and math.fabs(angle) <= angle_mulai_dribble_bola :
            dribble = True
            strategy_state = 'dribble_ball'
            last_position = (robot_pose[0], robot_pose[1], robot_pose[2])
        elif not ball_visible :
            strategy_state = 'cari_bola'
        go_to_pos(local_ball_x, local_ball_y, angle, local='local', dribbler=dribble)
    else :
        go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
    rospy.loginfo('dekati bola')

def dribble_ball() :
    global strategy_state, local_ball_x, local_ball_y, kick_pos, ball_shield_angle, robot_pose, min_error_posisi_kick, goalie_name
    global min_error_sudut_kick, radius_mulai_dribble_bola, angle_mulai_dribble_bola, kick_at_kick_pos, dribble_to_goal_pos
    global kick_off_state, robot_behavior, robot_connection, kick_index, kick_random_state, obstacle, teammates, passing, last_position
    
    stop = (robot_behavior == 'stop')
    positioning = (robot_behavior == 'positioning')
    if not (manual_positioning or stop or positioning) :
        go_to_pos(kick_pos[0][0], kick_pos[0][1], kick_pos[0][2],planning=True,dribbler=True)
        # dribble up to 3 meter
        radius_bola = math.hypot(local_ball_x,local_ball_y)
        dw = robot_pose[2] - kick_pos[0][2]
        if radius_bola > radius_mulai_dribble_bola :
    		strategy_state = 'cari_bola'
        elif math.hypot(last_position[0]-robot_pose[0],last_position[1]-robot_pose[1]) > 1.0 :
            # strategy_state = 'kick_ball'
            strategy_state = 'align_goal'
            angle = 0.0
            go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
    else :
        go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
    rospy.loginfo('dribble ball')

def align_goal() :
    global strategy_state, kick_pos, robot_pose, local_ball_x, local_ball_y, radius_mulai_dribble_bola, robot_behavior
    stop = (robot_behavior == 'stop')
    positioning = (robot_behavior == 'positioning')
    if not (manual_positioning or stop or positioning) :
    	radius_bola = math.hypot(local_ball_x,local_ball_y)
        dx = 6.0 - robot_pose[1]
        dy = 0.0 - robot_pose[1]
        dw = robot_pose[2] - kick_pos[0][2]
        angle = math.atan2(dy,dx)
        if radius_bola > radius_mulai_dribble_bola :
    		strategy_state = 'cari_bola'
    	elif math.fabs(angle) < 0.3 :
            strategy_state = 'kick_ball'
        go_to_pos(0.0,0.0,angle,local='local',dribbler=False)
        # go_to_pos(robot_pose[0], robot_pose[1], kick_pos[0][2])
    else :
        go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
    rospy.loginfo('align_goal')

def dribble_to_goal() :
    global strategy_state, local_ball_x, local_ball_y, dribble_to_goal_pos, robot_connection
    global radius_mulai_dribble_bola, angle_mulai_dribble_bola, robot_behavior, ball_shield_angle
    #radius_bola = math.sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y)
    radius_bola = math.hypot(local_ball_x,local_ball_y)
    angle_bola = math.atan2(local_ball_y,local_ball_x)
    if robot_behavior == 'stop' and robot_connection :
        strategy_state = 'timer'
    if radius_bola > radius_mulai_dribble_bola or math.fabs(angle_bola) > angle_mulai_dribble_bola :
        strategy_state = 'dekati_bola'
    go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
    rospy.loginfo('dribble ball to goal')

def kick_ball() :
    global strategy_state, kick_off_state, robot_role, kick_random_state
    kick()
    kick_random_state = True
    #if kick_off_state :
        #kick_off_state = False
    if robot_role == 'timer_defense' or robot_role == 'timer_defense2' or robot_role == 'defender' or robot_role == 'defender2' :
        strategy_state = 'defender'
    if strategy_state != 'defender' :
        strategy_state = 'cari_bola'
        if robot_role == 'goalie' :
            strategy_state = 'goalie'
    rospy.loginfo('kick ball')

def goalie():
    global strategy_state, global_ball_y, robot_state, robot_behavior, robot_pose, distance
    global local_ball_x, local_ball_y, ball_visible
    if ball_visible :
        radius = math.hypot(local_ball_x,local_ball_y)
        if radius < 0.5 :
            strategy_state = 'kick_ball'
            return None
    if robot_behavior == 'stop' :
        dx = -5.62 - robot_pose[0]
        dy = robot_pose[1]
        dw = math.fabs(robot_pose[2])
        go_to_pos(-5.62,0.0,0.0)
        if math.hypot(dx,dy) < 0.2 and dw < 0.2 :
            go_to_pos(0.0,0.0,0.0,local='local')
    else :
    # masukin strategi kiper terbaru disini:
        if goalie_intercept_valid : 
            go_to_pos(goalie_intercept_pos[0]/100.0, goalie_intercept_pos[1]/100.0, goalie_intercept_pos[2]*math.pi/180.0)
        else :
            factor = (math.pow( (global_ball_y/distance), 5))
            if factor > 1 :
                factor = 1
            y_setpoint = global_ball_y * (1 - factor)
            if ball_visible :
                if y_setpoint > 1.0 :
                    predicted_trajcetory_error = global_ball_y/2.0
                    y_setpoint = 1.0 - predicted_trajcetory_error
                elif y_setpoint < -1.0 :
                    predicted_trajcetory_error = global_ball_y/2.0
                    y_setpoint = -1.0 - predicted_trajcetory_error
            else :
                y_setpoint = 0.0
                angle_bola = 0.0
            go_to_pos(-5.65,y_setpoint,0.0)
    rospy.loginfo('goalie')


strategy_list = {
    'stop' : stop,
    'cari_bola' : cari_bola,
    'dekati_bola' : dekati_bola,
    'align_goal' : align_goal,
    'dribble_ball' : dribble_ball,
    'dribble_to_goal' : dribble_to_goal,
    'kick_ball' : kick_ball,
    'defender' : defender,
    'striker' : striker,
    'striker_dribble' : striker_dribble,
    'striker_kick' : striker_kick,
    'goalie' : goalie
}

#################################################################################################

def handle_strategy_request(req) :
    global strategy_state, home_pos, kick_pos, cari_bola_pos, robot_role, strategy_list
    ok = 0
    for s, f in strategy_list.items() :
        if req.strategy_state == s :
            strategy_state = req.strategy_state
            ok = 1
    if '--kick_pos' in req.option or '-k' in req.option :
        kick_pos[0] = (req.kick_pos.x, req.kick_pos.y, req.kick_pos.theta)
    if '--home_pos' in req.option or '-h' in req.option :
        home_pos = (req.home_pos.x, req.home_pos.y, req.home_pos.theta)
    if req.role in role_list :
        robot_role = req.role
    rospy.loginfo('strategy request %s %s %s %s' %(strategy_state,kick_pos,home_pos,cari_bola_pos))
    return StrategyServiceResponse(ok)

def update_state() :
    global strategy_list
    strategy_list[strategy_state]()

def go_to_pos(x,y,w,local='',dribbler=False,planning=False):
    global robot_control_msg
    robot_control_msg.target_pose.x = x
    robot_control_msg.target_pose.y = y
    robot_control_msg.target_pose.theta = w
    robot_control_msg.plan = planning
    if local == 'local':
        robot_control_msg.option.data = 'local'
    else :	
        robot_control_msg.option.data = ''
    if dribbler :
        robot_control_msg.dribbler = 1
    else :
        robot_control_msg.dribbler = 0

def world_model_callback(msg) :
    global ball_visible, local_ball_x, local_ball_y, robot_pose, last_ball_visible_time
    global global_ball_x, global_ball_y, obstacles
    now = rospy.get_rostime()
    if msg.ball_visible :
        ball_visible = True
        last_ball_visible_time = now
    elif now - last_ball_visible_time > rospy.Duration(1) :
        ball_visible = False
    local_ball_x = msg.local_balls_kf.x
    local_ball_y = msg.local_balls_kf.y
    global_ball_x = msg.global_balls_kf.x
    global_ball_y = msg.global_balls_kf.y
    robot_pose = (msg.pose.x, msg.pose.y, msg.pose.theta)
    obstacles = msg.obstacles
#    rospy.loginfo('world model callback : %s %f %f' %(msg.robot_name,local_ball_x,local_ball_y))

def strategy_pos_callback(msg) :
    global ball_shield_angle
    ball_shield_angle = msg.ball_shield_angle

def reconfigure_callback(cfg, level) :
    global distance, radius_dribble_auto_on, angle_dribble_auto_on
    global radius_mulai_dribble_bola, angle_mulai_dribble_bola
    global min_error_posisi_kick, min_error_sudut_kick
    global min_error_posisi_homing, min_error_sudut_homing
    global error_positioning, error_sudut_positioning, radius_mulai_kick_off
    distance = cfg.dist
    radius_dribble_auto_on = cfg.radius_dribble_auto_on
    angle_dribble_auto_on = cfg.angle_dribble_auto_on
    radius_mulai_dribble_bola = cfg.radius_mulai_dribble_bola
    angle_mulai_dribble_bola = cfg.angle_mulai_dribble_bola
    min_error_posisi_kick = cfg.min_error_posisi_kick
    min_error_sudut_kick = cfg.min_error_sudut_kick
    min_error_posisi_homing = cfg.min_error_posisi_homing
    min_error_sudut_homing = cfg.min_error_sudut_homing
    error_positioning = cfg.error_positioning
    error_sudut_positioning = cfg.error_sudut_positioning
    radius_mulai_kick_off = cfg.radius_mulai_kick_off
    return cfg

def strategy_param_callback(msg) :
    global distance, radius_dribble_auto_on, angle_dribble_auto_on
    global radius_mulai_dribble_bola, angle_mulai_dribble_bola
    global min_error_posisi_kick, min_error_sudut_kick
    global min_error_posisi_homing, min_error_sudut_homing
    global error_positioning, error_sudut_positioning, radius_mulai_kick_off
    radius_dribble_auto_on = msg.radius_dribble_auto_on
    angle_dribble_auto_on = msg.angle_dribble_auto_on
    radius_mulai_dribble_bola = msg.radius_mulai_dribble_bola
    angle_mulai_dribble_bola = msg.angle_mulai_dribble_bola
    min_error_posisi_kick = msg.min_error_posisi_kick
    min_error_sudut_kick = msg.min_error_sudut_kick
    min_error_posisi_homing = msg.min_error_posisi_homing
    min_error_sudut_homing = msg.min_error_sudut_homing
    error_positioning = msg.error_positioning
    error_sudut_positioning = msg.error_sudut_positioning
    radius_mulai_kick_off = msg.radius_mulai_kick_off

def robot_control_info_callback(msg) :
    global error_radius, error_angle
    error_radius = msg.error_radius
    error_angle = msg.error_angle

def teammates_callback(msg) :
    global teammates, robotname, robot_behavior, robot_connection, robot_state, state_list, behavior_list
    global home_pos_state, robot_pose, global_ball_x, global_ball_y, teammates_dist_to_ball, manual_positioning
    global strategy_state
    robotname = msg.robotname
    manual_positioning = msg.isManualPositioning
    teammates_dist_to_ball = []
    for i in range(0,len(msg.pose)) :
        pose_tuple = (msg.pose[i].x,msg.pose[i].y,msg.pose[i].theta,i,msg.available[i])
        if pose_tuple[4] == True and pose_tuple[3] != robotname :
            teammates_dist_to_ball.append(math.hypot(global_ball_x-pose_tuple[0],global_ball_y-pose_tuple[1]))
        teammates[i] = pose_tuple
    robot_behavior = behavior_list[msg.behavior]
    robot_connection = msg.isConnected
    robot_state = state_list[msg.state]
    if msg.isManualPositioning :
        if robot_state in home_pos_state :
            home_pos_state[robot_state] = (robot_pose[0],robot_pose[1],robot_pose[2])

def kick():
    try:
        global kicker_service, kick_off_state, passing
        if kick_off_state or passing :
            respl = kicker_service(2)
            kick_off_state = False
            passing = False
        else :
            respl = kicker_service(3)
    except rospy.ServiceException, e:
        rospy.logerr('Kick service call failed: %s' %e)

def publish():
    global robot_control_pub
    global robot_control_msg
    global strategy_info_pub
    global strategy_info_msg
    global strategy_state
    global robot_role
    strategy_info_msg.strategy_state = strategy_state
    strategy_info_msg.role = robot_role
    robot_control_pub.publish(robot_control_msg)
    strategy_info_pub.publish(strategy_info_msg)

def compass_callback(msg) :
    global compass
    compass = msg.cmps
    # rospy.loginfo("compass callback %s")

def strategy():
    robot_name = os.getenv('FUKURO')
    rospy.init_node('fukuro_strategy_nasional')
    print('waiting for service')
    rospy.wait_for_service(robot_name+'/kick_service')
    global kicker_service
    global robot_control_pub
    global strategy_info_pub
    global home_pos
    global strategy_state
    global robot_role
    kicker_service = rospy.ServiceProxy(robot_name+'/kick_service',Shoot)
    robot_control_pub = rospy.Publisher(robot_name+'/robot_control',RobotControl,queue_size=1)
    strategy_info_pub = rospy.Publisher(robot_name+'/strategy_info',StrategyInfo,queue_size=1)
    strategy_server = rospy.Service(robot_name+'/strategy_service',StrategyService,handle_strategy_request)
    rospy.Subscriber(robot_name+'/world_model', WorldModel, world_model_callback)
    rospy.Subscriber('/strategy_pos', StrategyPositioning, strategy_pos_callback)
    rospy.Subscriber(robot_name+'/robot_control_info', RobotControlInfo, robot_control_info_callback)
    rospy.Subscriber(robot_name+'/teammates', Teammates, teammates_callback)
    rospy.Subscriber(robot_name+'/strategy_param', StrategyParam, strategy_param_callback)
    rospy.Subscriber(robot_name+"/compass", Compass, compass_callback)

    #srv = Server(StrategyConfig, reconfigure_callback)
    print 'ready'
    rate = rospy.Rate(30)

    if rospy.has_param("home_x") :
        if rospy.has_param("home_y") :
            if rospy.has_param("home_w") :
                x = rospy.get_param("home_x")
                y = rospy.get_param("home_y")
                w = rospy.get_param("home_w")
                home_pos = (float(x), float(y), float(w))
                rospy.logwarn("setting home_pos : %s", home_pos)

    if rospy.has_param("/strategy_state") :
        strategy_state = rospy.get_param("strategy_state")

    if rospy.has_param("/robot_role") :
        robot_role = rospy.get_param("robot_role")
                
    while not rospy.is_shutdown():
        #rospy.loginfo('strategy loop')
        update_state()
        publish()
        rate.sleep()

if __name__ == "__main__" :
    strategy()
