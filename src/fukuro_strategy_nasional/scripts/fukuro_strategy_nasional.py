#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import rospy
import os
import thread
import time
from dynamic_reconfigure.server import Server
from dynamic_reconfigure.parameter_generator_catkin import *
from fukuro_common.msg import *
from fukuro_common.srv import *
from random import randint
from enum import Enum

class Tactics(Enum):
    OFFENSE = 1
    DEFENSE = 2

robot_tactics = Tactics.DEFENSE

goalie_name = 2

## ros publisher n server
robot_control_pub = None
kicker_service = None
strategy_info_pub = None

## ros message
robot_control_msg = RobotControl()
strategy_info_msg = StrategyInfo()

## informasi robot
ball_visible = False
local_ball_x = 0.0
local_ball_y = 0.0
global_ball_x = 0.0
global_ball_y = 0.0
robot_pose = (0.0,0.0,0.0)
role_list = ['defender',
            'defender2',
            'timer_defense',
            'timer_defense2',
            'striker',
            'striker_kick',
            'striker_dribble',
            'goalie',
            'timer_striker',
            'timer_striker2']
robot_role = 'striker_dribble'
last_ball_visible_time = rospy.Time()

## default home n kick
home_pos = (0.0,0.0,0.0)
kick_pos = [(3.0,0.0,0.0),(3.0,1.0,math.atan2(6.25-3.0,-1.0)),(3.0,2.6,math.atan2(6.25-3.0,-2.6)),(3.0,-1.0,math.atan2(6.25-3.0,1.0)),(3.0,-2.6,math.atan2(6.25-3.0,2.6))]
dribble_to_goal_pos = (6.25,0.0,0.0)
kick_off_pos = [(0.0,1.3,-(math.pi/2.0)),(0.0,-1.3,math.pi/2.0)]
corner_kick_pos = [(-3.0,1.5,0.0),(-3.0,-1.5,0.0)]

## setting cari bola
cari_bola_pos = [(-1.5,0.0,math.pi),(1.5,0.0,math.pi)]
waktu_cari_bola = 2.0
waypoint_cari_bola = 0
init_cari_bola = True
waktu_mulai_cari_bola = None

## setting dekati bola
radius_dribble_auto_on = 0.7 #meter
angle_dribble_auto_on = 0.2 #radian
radius_mulai_dribble_bola = 0.35 #meter
angle_mulai_dribble_bola = 0.2 #radian

## setting kick
min_error_posisi_kick = 0.2 #meter
min_error_sudut_kick = 0.1 #radian

## setting defender
defense_line = -2.5
defense_line2 = -3.5
timer_defense_init = True
timer_defense_t0 = None
defense_time = rospy.Duration(10)

## setting homing
min_error_posisi_homing = 0.25 #meter
min_error_sudut_homing = 0.1 #radian

#ball_shield_angle = math.pi
ball_shield_angle = 0.0

## strategy awal
strategy_state = 'stop'

## goalie stuff
goalie_intercept_pos = (-5.5, 0.0, 0.0)
goalie_intercept_valid = False

distance = 3.0

timer_t0 = None
timer_init = True
timer_duration = rospy.Duration(6)

error_radius = 0.0
error_angle = 0.0

use_ball_priority = False
use_kick_off = True

kick_index = 0
kick_random_state = True

error_positioning = 0.25
error_sudut_positioning = 0.25

radius_mulai_kick_off = 0.415

skip_timer = False

defense_lines = (defense_line,defense_line2)
obstacles = None
obstacle = (0.0,0.0,2*math.pi)

passing = False

last_position = ()

kick_off_state = False
kick_at_kick_pos = True
main_timer_strategy = False
robotname = int(os.getenv('AGENT')) - 1
teammates = [(0.0,0.0,0.0,0,False),(0.0,0.0,0.0,1,False),(0.0,0.0,0.0,2,False),(0.0,0.0,0.0,3,False),(0.0,0.0,0.0,4,False),(0.0,0.0,0.0,5,False)]
teammates_dist_to_ball = []
nearest_agent = robotname
distance_priority = [(True,0.0,0),(True,0.0,1),(True,0.0,2),(True,0.0,3),(True,0.0,4),(True,0.0,5)]
behavior_list = ['positioning','gameplay','stop']
robot_behavior = 'stop'
robot_connection = False
robot_state = 'welcome'
state_list = ['welcome',
            'our-kick-off',
            'opp-kick-off',
            'our-free-kick',
            'opp-free-kick',
            'our-goal-kick',
            'opp-goal-kick',
            'our-throw-in',
            'opp-throw-in',
            'our-corner-kick',
            'opp-corner-kick',
            'our-penalty',
            'opp-penalty',
            'our-goal',
            'opp-goal',
            'our-repair',
            'opp-repair',
            'our-red-card',
            'opp-red-card',
            'our-yellow-card',
            'opp-yellow-card',
            'start',
            'stop',
            'drop',
            'park',
            '1st',
            '2nd'
            '3rd'
            '4th']

home_pos_state = {
    'our-corner-kick' : (4.0,2.0,0.0),
    'opp-corner-kick' : (-3.2,2.0,3.14),
    'our-free-kick' : (-3.2,2.0,0.0),
    'opp-free-kick' : (-3.2,2.0,0.0),
    'drop' : (-2.0,1.0,0.0)#,
    #'our-corner-kick' : (-2.0,0.0,0.0),
    #'opp-corner-kick' : (-2.0,0.0,0.0),
    #'welcome' : welcome,
    #'our-kick-off' : (0.0,0.0,0.0),
    #'opp-kick-off' : (0.0,0.0,0.0),
    #'our-goal-kick' : (0.0,0.0,0.0),
    #'opp-goal-kick' : (0.0,0.0,0.0),
    #'our-throw-in' : (0.0,0.0,0.0),
    #'opp-throw-in' : (0.0,0.0,0.0),
    #'our-penalty' : (0.0,0.0,0.0),
    #'opp-penalty' : (0.0,0.0,0.0),
    #'our-goal' : (0.0,0.0,0.0),
    #'opp-goal' : (0.0,0.0,0.0),
    #'our-repair' : (0.0,0.0,0.0),
    #'opp-repair' : (0.0,0.0,0.0),
    #'our-red-card' : (0.0,0.0,0.0),
    #'opp-red-card' : (0.0,0.0,0.0),
    #'our-yellow-card' : (0.0,0.0,0.0),
    #'opp-yellow-card' : (0.0,0.0,0.0),
    #'start' : start,
    #'stop' : stop,
    #'park' : (0.0,0.0,0.0)#,
    #'1st',
    #'2nd'
    #'3rd'
    #'4th'
}

################################################################################################
def our_kick_off() :
    global kick_off_state, robot_role, home_pos, kick_off_pos, robot_pose, home_pos_state
    kick_off_state = True
    error_timer = 0.0
    error_sudut_timer = 0.0
    dx = 0.0
    dy = 0.0
    dw = 0.0
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' :
        if home_pos[1] > 0 :
            dx = kick_off_pos[0][0] - robot_pose[0]
            dy = kick_off_pos[0][1] - robot_pose[1]
            dw = kick_off_pos[0][2] - robot_pose[2]
            error_timer = math.hypot(dx,dy)
            error_sudut_timer = math.fabs(dw)
            #go_to_pos(kick_off_pos[0][0],kick_off_pos[0][1],kick_off_pos[0][2],planning=True)
            go_to_pos(kick_off_pos[0][0],kick_off_pos[0][1],kick_off_pos[0][2])
        elif home_pos[1] < 0 :
            dx = kick_off_pos[1][0] - robot_pose[0]
            dy = kick_off_pos[1][1] - robot_pose[1]
            dw = kick_off_pos[1][2] - robot_pose[2]
            error_timer = math.hypot(dx,dy)
            error_sudut_timer = math.fabs(dw)
            #go_to_pos(kick_off_pos[1][0],kick_off_pos[1][1],kick_off_pos[1][2],planning=True)
            go_to_pos(kick_off_pos[1][0],kick_off_pos[1][1],kick_off_pos[1][2])
    elif robot_role == 'timer_defense' or robot_role == 'timer_defense2' :
        if home_pos[1] > 0 :
            dx = kick_off_pos[0][0] - robot_pose[0]
            dy = kick_off_pos[0][1] - robot_pose[1]
            dw = kick_off_pos[0][2] - robot_pose[2]
            error_timer = math.hypot(dx,dy)
            error_sudut_timer = math.fabs(dw)
            #go_to_pos(kick_off_pos[0][0],kick_off_pos[0][1],kick_off_pos[0][2],planning=True)
            go_to_pos(kick_off_pos[0][0],kick_off_pos[0][1],kick_off_pos[0][2])
        elif home_pos[1] < 0 :
            dx = kick_off_pos[1][0] - robot_pose[0]
            dy = kick_off_pos[1][1] - robot_pose[1]
            dw = kick_off_pos[1][2] - robot_pose[2]
            error_timer = math.hypot(dx,dy)
            error_sudut_timer = math.fabs(dw)
            #go_to_pos(kick_off_pos[1][0],kick_off_pos[1][1],kick_off_pos[1][2],planning=True)
            go_to_pos(kick_off_pos[1][0],kick_off_pos[1][1],kick_off_pos[1][2])
    #if error_timer < error_positioning and error_sudut_timer <= error_sudut_positioning :
    if error_timer < error_positioning : #anti koreksi sudut sudut club
        angle = dw
        if angle < error_sudut_positioning :
            angle = 0.0
        go_to_pos(0.0,0.0,angle,local='local')
        return None

def opp_kick_off() :
    global kick_off_state, robot_role, home_pos, kick_off_pos, robot_pose, home_pos_state
    kick_off_state = False
    error_timer = 0.0
    error_sudut_timer = 0.0
    dx = 0.0
    dy = 0.0
    dw = 0.0
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' :
        if home_pos[1] > 0 :
            dx = defense_lines[0] - robot_pose[0]
            dy = kick_off_pos[0][1] - robot_pose[1]
            dw = 0.0 - robot_pose[2]
            error_timer = math.hypot(dx,dy)
            error_sudut_timer = math.fabs(dw)
            #go_to_pos(kick_off_pos[0][0],kick_off_pos[0][1],kick_off_pos[0][2],planning=True)
            go_to_pos(defense_lines[0],kick_off_pos[0][1],0.0)
        elif home_pos[1] < 0 :
            dx = defense_lines[0] - robot_pose[0]
            dy = kick_off_pos[1][1] - robot_pose[1]
            dw = 0.0 - robot_pose[2]
            error_timer = math.hypot(dx,dy)
            error_sudut_timer = math.fabs(dw)
            #go_to_pos(kick_off_pos[1][0],kick_off_pos[1][1],kick_off_pos[1][2],planning=True)
            go_to_pos(defense_lines[0],kick_off_pos[1][1],0.0)
    elif robot_role == 'timer_defense' or robot_role == 'timer_defense2' :
        if home_pos[1] > 0 :
            dx = defense_lines[1] - robot_pose[0]
            dy = kick_off_pos[0][1] - robot_pose[1]
            dw = 0.0 - robot_pose[2]
            error_timer = math.hypot(dx,dy)
            error_sudut_timer = math.fabs(dw)
            #go_to_pos(kick_off_pos[0][0],kick_off_pos[0][1],kick_off_pos[0][2],planning=True)
            go_to_pos(defense_lines[1],kick_off_pos[0][1],0.0)
        elif home_pos[1] < 0 :
            dx = defense_lines[1] - robot_pose[0]
            dy = kick_off_pos[1][1] - robot_pose[1]
            dw = 0.0 - robot_pose[2]
            error_timer = math.hypot(dx,dy)
            error_sudut_timer = math.fabs(dw)
            #go_to_pos(kick_off_pos[1][0],kick_off_pos[1][1],kick_off_pos[1][2],planning=True)
            go_to_pos(defense_lines[1],kick_off_pos[1][1],0.0)
    #if error_timer < error_positioning and error_sudut_timer <= error_sudut_positioning :
    if error_timer < error_positioning : #anti koreksi sudut sudut club
        angle = dw
        if angle < error_sudut_positioning :
            angle = 0.0
        go_to_pos(0.0,0.0,angle,local='local')
        return None

def our_free_kick() :
    global robot_state, kick_off_state, robot_role, home_pos, global_ball_x, global_ball_y, robot_pose, home_pos_state
    kick_off_state = False
    error_timer = 0.0
    error_sudut_timer = 0.0
    dx = 0.0
    dy = 0.0
    dw = 0.0
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' :
        dx = global_ball_x - home_pos_state[robot_state][0]
        dy = global_ball_y
        dw = math.atan2(dy,dx)
        dx = home_pos_state[robot_state][0] - robot_pose[0]
        dy = global_ball_y - robot_pose[1]
        dw = dw - robot_pose[2]
        error_timer = math.hypot(dx,dy)
        error_sudut_timer = math.fabs(dw)
        go_to_pos(home_pos_state[robot_state][0], global_ball_y, dw)
    elif robot_role == 'timer_defense' or robot_role == 'timer_defense2' :
        dx = global_ball_x - (home_pos_state[robot_state][0]-1.0)
        if home_pos[1] < 0 :
            dy = global_ball_y + home_pos_state[robot_state][1]
        else :
            dy = global_ball_y - home_pos_state[robot_state][1]
        dw = math.atan2(dy,dx)
        error_x = (home_pos_state[robot_state][0]-1.0) - robot_pose[0]
        if home_pos[1] < 0 :
            error_y = -home_pos_state[robot_state][1] - robot_pose[1]
        else :
            error_y = home_pos_state[robot_state][1] - robot_pose[1]
        error_timer = math.hypot(error_x,error_y)
        if home_pos[1] < 0 :
            go_to_pos((home_pos_state[robot_state][0]-1.0), -home_pos_state[robot_state][1], dw)
        else :
            go_to_pos((home_pos_state[robot_state][0]-1.0), home_pos_state[robot_state][1], dw)
        dw = math.atan2(dy,dx) - robot_pose[2]
        error_sudut_timer = math.fabs(dw)
    #if error_timer < error_positioning and error_sudut_timer <= error_sudut_positioning :
    if error_timer < error_positioning : #anti koreksi sudut sudut club
        angle = dw
        if angle < error_sudut_positioning :
            angle = 0.0
        go_to_pos(0.0,0.0,angle,local='local')
        return None

def opp_free_kick() :
    global robot_state, kick_off_state, robot_role, home_pos, global_ball_x, global_ball_y, robot_pose, home_pos_state
    kick_off_state = False
    error_timer = 0.0
    error_sudut_timer = 0.0
    dx = 0.0
    dy = 0.0
    dw = 0.0
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' :
        dx = global_ball_x - home_pos_state[robot_state][0]
        dy = global_ball_y
        dw = math.atan2(dy,dx)
        dx = home_pos_state[robot_state][0] - robot_pose[0]
        dy = global_ball_y - robot_pose[1]
        dw = dw - robot_pose[2]
        error_timer = math.hypot(dx,dy)
        error_sudut_timer = math.fabs(dw)
        go_to_pos(home_pos_state[robot_state][0], global_ball_y, dw)
    elif robot_role == 'timer_defense' or robot_role == 'timer_defense2' :
        dx = global_ball_x - (home_pos_state[robot_state][0]-1.0)
        if home_pos[1] < 0 :
            dy = global_ball_y + home_pos_state[robot_state][1]
        else :
            dy = global_ball_y - home_pos_state[robot_state][1]
        dw = math.atan2(dy,dx)
        error_x = (home_pos_state[robot_state][0]-1.0) - robot_pose[0]
        if home_pos[1] < 0 :
            error_y = -home_pos_state[robot_state][1] - robot_pose[1]
        else :
            error_y = home_pos_state[robot_state][1] - robot_pose[1]
        error_timer = math.hypot(error_x,error_y)
        if home_pos[1] < 0 :
            go_to_pos((home_pos_state[robot_state][0]-1.0), -home_pos_state[robot_state][1], dw)
        else :
            go_to_pos((home_pos_state[robot_state][0]-1.0), home_pos_state[robot_state][1], dw)
        dw = math.atan2(dy,dx) - robot_pose[2]
        error_sudut_timer = math.fabs(dw)
    #if error_timer < error_positioning and error_sudut_timer <= error_sudut_positioning :
    if error_timer < error_positioning : #anti koreksi sudut sudut club
        angle = dw
        if angle < error_sudut_positioning :
            angle = 0.0
        go_to_pos(0.0,0.0,angle,local='local')
        return None

def our_corner_kick() :
    global robot_state, kick_off_state, robot_role, home_pos, global_ball_x, global_ball_y, robot_pose, home_pos_state
    kick_off_state = False
    error_timer = 0.0
    error_sudut_timer = 0.0
    dx = 0.0
    dy = 0.0
    dw = 0.0
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' :
        dx = global_ball_x - home_pos_state[robot_state][0]
        dy = global_ball_y - home_pos_state[robot_state][1]
        dw = math.atan2(dy,dx)
        dx = home_pos_state[robot_state][0] - robot_pose[0]
        dy = home_pos_state[robot_state][1] - robot_pose[1]
        dw = dw - robot_pose[2]
        error_timer = math.hypot(dx,dy)
        error_sudut_timer = math.fabs(dw)
        go_to_pos(home_pos_state[robot_state][0], home_pos_state[robot_state][0], dw)
    elif robot_role == 'timer_defense' or robot_role == 'timer_defense2' :
        dx = global_ball_x - (home_pos_state[robot_state][0]-1.0)
        if home_pos[1] < 0 :
            dy = global_ball_y + home_pos_state[robot_state][1]
        else :
            dy = global_ball_y - home_pos_state[robot_state][1]
        dw = math.atan2(dy,dx)
        error_x = (home_pos_state[robot_state][0]-1.0) - robot_pose[0]
        if home_pos[1] < 0 :
            error_y = -home_pos_state[robot_state][1] - robot_pose[1]
        else :
            error_y = home_pos_state[robot_state][1] - robot_pose[1]
        error_timer = math.hypot(error_x,error_y)
        if home_pos[1] < 0 :
            go_to_pos((home_pos_state[robot_state][0]-1.0), -home_pos_state[robot_state][1], dw)
        else :
            go_to_pos((home_pos_state[robot_state][0]-1.0), home_pos_state[robot_state][1], dw)
        dw = math.atan2(dy,dx) - robot_pose[2]
        error_sudut_timer = math.fabs(dw)
    #if error_timer < error_positioning and error_sudut_timer <= error_sudut_positioning :
    if error_timer < error_positioning : #anti koreksi sudut sudut club
        angle = dw
        if angle < error_sudut_positioning :
            angle = 0.0
        go_to_pos(0.0,0.0,angle,local='local')
        return None

def opp_corner_kick() :
    global robot_state, kick_off_state, robot_role, home_pos, global_ball_x, global_ball_y, robot_pose, home_pos_state
    kick_off_state = False
    error_timer = 0.0
    error_sudut_timer = 0.0
    dx = 0.0
    dy = 0.0
    dw = 0.0
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' :
        dx = global_ball_x - home_pos_state[robot_state][0]
        dy = global_ball_y - home_pos_state[robot_state][1]
        dw = math.atan2(dy,dx)
        dx = home_pos_state[robot_state][0] - robot_pose[0]
        dy = home_pos_state[robot_state][1] - robot_pose[1]
        dw = dw - robot_pose[2]
        error_timer = math.hypot(dx,dy)
        error_sudut_timer = math.fabs(dw)
        go_to_pos(home_pos_state[robot_state][0], home_pos_state[robot_state][1], dw)
    elif robot_role == 'timer_defense' or robot_role == 'timer_defense2' :
        dx = global_ball_x - (home_pos_state[robot_state][0]-1.0)
        if home_pos[1] < 0 :
            dy = global_ball_y + home_pos_state[robot_state][1]
        else :
            dy = global_ball_y - home_pos_state[robot_state][1]
        dw = math.atan2(dy,dx)
        error_x = (home_pos_state[robot_state][0]-1.0) - robot_pose[0]
        if home_pos[1] < 0 :
            error_y = -home_pos_state[robot_state][1] - robot_pose[1]
        else :
            error_y = home_pos_state[robot_state][1] - robot_pose[1]
        error_timer = math.hypot(error_x,error_y)
        if home_pos[1] < 0 :
            go_to_pos((home_pos_state[robot_state][0]-1.0), -home_pos_state[robot_state][1], dw)
        else :
            go_to_pos((home_pos_state[robot_state][0]-1.0), home_pos_state[robot_state][1], dw)
        dw = math.atan2(dy,dx) - robot_pose[2]
        error_sudut_timer = math.fabs(dw)
    #if error_timer < error_positioning and error_sudut_timer <= error_sudut_positioning :
    if error_timer < error_positioning : #anti koreksi sudut sudut club
        angle = dw
        if angle < error_sudut_positioning :
            angle = 0.0
        go_to_pos(0.0,0.0,angle,local='local')
        return None

def drop() :
    global robot_state, kick_off_state, robot_role, home_pos, global_ball_x, global_ball_y, robot_pose, home_pos_state
    kick_off_state = False
    error_timer = 0.0
    error_sudut_timer = 0.0
    dx = 0.0
    dy = 0.0
    dw = 0.0
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' :
        dx = global_ball_x - home_pos_state[robot_state][0]
        if home_pos[1] < 0 :
            dy = global_ball_y + home_pos_state[robot_state][1]
        else:
            dy = global_ball_y - home_pos_state[robot_state][1]
        dw = math.atan2(dy,dx)
        dx = home_pos_state[robot_state][0] - robot_pose[0]
        if home_pos[1] < 0 :
            dy = -home_pos_state[robot_state][1] - robot_pose[1]
        else:
            dy = home_pos_state[robot_state][1] - robot_pose[1]
        dw = dw - robot_pose[2]
        error_timer = math.hypot(dx,dy)
        error_sudut_timer = math.fabs(dw)
        if home_pos[1] < 0 :
            go_to_pos(home_pos_state[robot_state][0], -home_pos_state[robot_state][1], dw)
        else :
            go_to_pos(home_pos_state[robot_state][0], home_pos_state[robot_state][1], dw)
    elif robot_role == 'timer_defense' or robot_role == 'timer_defense2' :
        dx = global_ball_x - (home_pos_state[robot_state][0]-1.0)
        if home_pos[1] < 0 :
            dy = global_ball_y + home_pos_state[robot_state][1]
        else :
            dy = global_ball_y - home_pos_state[robot_state][1]
        dw = math.atan2(dy,dx)
        error_x = (home_pos_state[robot_state][0]-1.0) - robot_pose[0]
        if home_pos[1] < 0 :
            error_y = -home_pos_state[robot_state][1] - robot_pose[1]
        else :
            error_y = home_pos_state[robot_state][1] - robot_pose[1]
        error_timer = math.hypot(error_x,error_y)
        if home_pos[1] < 0 :
            go_to_pos((home_pos_state[robot_state][0]-1.0), -home_pos_state[robot_state][1], dw)
        else :
            go_to_pos((home_pos_state[robot_state][0]-1.0), home_pos_state[robot_state][1], dw)
        dw = math.atan2(dy,dx) - robot_pose[2]
        error_sudut_timer = math.fabs(dw)
    #if error_timer < error_positioning and error_sudut_timer <= error_sudut_positioning :
    if error_timer < error_positioning : #anti koreksi sudut sudut club
        angle = dw
        if angle < error_sudut_positioning :
            angle = 0.0
        go_to_pos(0.0,0.0,angle,local='local')
        return None

def our_penalty() :
    pass

def opp_penalty() :
    pass

def our_repair() :
    pass

def opp_repair() :
    pass

def our_red_card() :
    pass

def opp_red_card() :
    pass

def our_yellow_card() :
    pass

def opp_yellow_card() :
    pass

def our_goal_kick() :
    pass

def opp_goal_kick() :
    pass

def our_throw_in() :
    pass

def opp_throw_in() :
    pass

def park() :
    pass

robot_states = {
    'our-kick-off' : our_kick_off,
    'opp-kick-off' : opp_kick_off,
    'our-free-kick' : our_free_kick,
    'opp-free-kick' : opp_free_kick,
    'our-corner-kick' : our_corner_kick,
    'opp-corner-kick' : opp_corner_kick,
    'drop' : drop#,
    #'welcome' : welcome,
    #'our-goal-kick' : our_goal_kick,
    #'opp-goal-kick' : opp_goal_kick,
    #'our-throw-in' : our_throw_in,
    #'opp-throw-in' : opp_throw_in ,
    #'our-penalty' : our_penalty,
    #'opp-penalty' : opp_penalty,
    #'our-goal' : our_goal,
    #'opp-goal' : opp_goal,
    #'our-repair' : our_repair,
    #'opp-repair' : opp_repair,
    #'our-red-card' : our_red_card,
    #'opp-red-card' : opp_red_card,
    #'our-yellow-card' : our_yellow_card,
    #'opp-yellow-card' : opp_yellow_card,
    #'start' : start,
    #'stop' : stop,
    #'park' : park#,
    #'1st',
    #'2nd'
    #'3rd'
    #'4th'
}

## edit strategy disini
#################################################################################################

def stop() :
#    rospy.loginfo('stop')
    go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
    #go_to_pos(home_pos[0],home_pos[1],home_pos[2],local='local',dribbler=False)

def go_to_home() :
    global strategy_state, home_pos, robot_role, robot_pose, robot_connection
    rospy.loginfo('goto home')
    dx = home_pos[0]-robot_pose[0]
    dy = home_pos[1]-robot_pose[1]
    dw = home_pos[2]-robot_pose[2]
    error = math.hypot(dx,dy)
    error_sudut = math.fabs(dw)
    if not robot_connection :
        #if error < min_error_posisi_homing and error_sudut < min_error_sudut_homing :
        if error < min_error_posisi_homing :
            if robot_role == 'defender' or robot_role == 'defender2' or robot_role == 'timer_defense2' or robot_role == 'timer_defense' :
                strategy_state = 'defender'
            elif robot_role == 'timer_striker2' or robot_role == 'striker_dribble' :
                strategy_state = 'striker_dribble'
    go_to_pos(home_pos[0], home_pos[1], home_pos[2])

def timer() :
    global timer_t0, timer_init, strategy_state, robot_role, home_pos, robot_behavior, robot_connection, robot_state, kick_off_pos, global_ball_x, main_timer_strategy
    global corner_kick_pos, global_ball_y, kick_off_state, local_ball_y, local_ball_x, angle_mulai_dribble_bola, radius_mulai_dribble_bola, ball_visible, robot_pose
    global error_positioning, error_sudut_positioning, radius_mulai_kick_off, skip_timer, defense_lines, robot_states, robot_tactics
    now = rospy.get_rostime()
    radius = math.hypot(local_ball_x,local_ball_y)
    angle = math.atan2(local_ball_y,local_ball_x)
    robot_tactics = Tactics.OFFENSE
    if timer_init :
        if robot_state == 'our-kick-off' or robot_state == 'our-corner-kick' or robot_state == 'drop' or robot_state == 'our-free-kick' :
            skip_timer = True
        if robot_behavior == 'gameplay' and robot_connection :
            timer_init = False
        timer_t0 = now
    else :
        duration = now - timer_t0
        if robot_state == 'our-kick-off' or robot_state == 'our-corner-kick' or robot_state == 'drop' or robot_state == 'our-free-kick' :
            main_timer_strategy = True
            skip_timer = True
        elif duration.secs > 6 or radius < 2.0 and not skip_timer :
            if ball_visible :
                main_timer_strategy = True
            else :
                main_timer_strategy = False
        elif skip_timer :
            main_timer_strategy = True
        if main_timer_strategy :
            timer_init = True
            main_timer_strategy = False
            if robot_state == 'start' or robot_state == '1st' or robot_state == '2nd' or robot_state == '3rd' or robot_state == '4th' :
                skip_timer = False
                if robot_role == 'defender' or robot_role == 'timer_defense' :
                    if kick_off_state :
                        strategy_state = 'dekati_bola'
                        robot_tactics = Tactics.OFFENSE
                    else :
                        strategy_state = 'defender'
                        robot_tactics = Tactics.DEFENSE
                    return None
                elif robot_role == 'defender2' or robot_role == 'timer_defense2' :
                    if kick_off_state :
                        strategy_state = 'dekati_bola'
                        robot_tactics = Tactics.OFFENSE
                    else :
                        strategy_state = 'defender'
                        robot_tactics = Tactics.DEFENSE
                    return None
                elif robot_role == 'timer_striker' :
                    robot_role = 'timer_striker'
                    if not kick_off_state :
                        strategy_state = 'striker'
                        return None
                elif robot_role == 'timer_striker2' :
                    robot_role = 'timer_striker2'
                    if not kick_off_state :
                        strategy_state = 'striker_dribble'
                        return None
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' :
        if radius < radius_mulai_kick_off :
            kick_off_state = False
        elif (now - timer_t0) > rospy.Duration(6.0) :
            kick_off_state = False
        elif robot_state == 'our-kick-off' :
            robot_tactics = Tactics.DEFENSE
            kick_off_state = True
    if robot_state in robot_states :
        robot_states[robot_state]()
    else :
        robot_states['opp-kick-off']()
    rospy.logwarn('timer : robot_states : %s', robot_state)

def go_to_home_cari_bola() :
    global strategy_state, home_pos, ball_visible, robot_pose, robot_role
    rospy.loginfo('goto home')
    dx = home_pos[0] - robot_pose[0]
    dy = home_pos[1] - robot_pose[1]
    dw = home_pos[2] - robot_pose[2]
    error = math.hypot(dx,dy)
    error_sudut = math.fabs(dw)
    if (error < min_error_posisi_homing and error_sudut < min_error_sudut_homing) or ball_visible :
        strategy_state = robot_role
    go_to_pos(home_pos[0], home_pos[1], home_pos[2],planning=True)

def defender() :
    global strategy_state, timer_defense_init, timer_defense_t0, robot_role, global_ball_x, global_ball_y
    global kick_off_state, robot_behavior, robot_connection, defense_line, defense_line2
    global local_ball_x, local_ball_y, radius_mulai_kick_off, robot_tactics
    line = defense_line
    radius = math.hypot(local_ball_x,local_ball_y)
    if robot_behavior == 'stop' and robot_connection :
        strategy_state = 'timer'
    if ball_visible :
        '''
        if global_ball_x > defense_line :
            y = global_ball_y/global_ball_x * defense_line
            y = math.copysign(y,global_ball_y)
            go_to_pos(defense_line,y,math.atan2(global_ball_y,global_ball_x))
            dx = defense_line - robot_pose[0]
            dy = y - robot_pose[1]
            error = math.hypot(dx,dy)
            if error < 0.3 :
                if radius < radius_mulai_kick_off and robot_tactics == Tactics.OFFENSE :
                    strategy_state = 'dekati_bola'
        '''
        #elif global_ball_x > defense_line2 :
        if global_ball_x > defense_line2 :
            y = global_ball_y/global_ball_x * defense_line2
            y = math.copysign(y,global_ball_y)
            go_to_pos(defense_line2,y,math.atan2(global_ball_y,global_ball_x))
            dx = defense_line2 - robot_pose[0]
            dy = y - robot_pose[1]
            error = math.hypot(dx,dy)
            if error < 0.3 :
                if radius < radius_mulai_kick_off or (robot_tactics == Tactics.OFFENSE and radius < 0.75) :
                    strategy_state = 'dekati_bola'
        else :
            strategy_state = 'dekati_bola'
    else :
        go_to_pos(line,0.0,0.0)

def striker() :
    global strategy_state
    strategy_state = 'cari_bola'

def striker_kick() :
    global strategy_state
    strategy_state = 'cari_bola'

def striker_dribble() :
    global strategy_state
    strategy_state = 'cari_bola'

def cari_bola() :
    global init_cari_bola, waktu_mulai_cari_bola, waypoint_cari_bola, strategy_state, nearest_agent, robotname, robot_behavior, robot_connection, cari_bola_pos, ball_visible
    global robot_pose, local_ball_x, local_ball_y, global_ball_x, global_ball_y, teammates, teammates_dist_to_ball, robot_tactics, defense_line
    radius = math.hypot(local_ball_x,local_ball_y)
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' or robot_role == 'striker' or robot_role == 'striker_dribble' :
        if robot_tactics == Tactics.OFFENSE :
            waypoint_cari_bola = 1
        elif robot_tactics == Tactics.DEFENSE :
            waypoint_cari_bola = 0
    if robot_behavior == 'stop' and robot_connection :
        strategy_state = 'timer'
    if ball_visible :
        if robot_connection :
            if robot_behavior == 'gameplay' :
                if robotname == nearest_agent :
                    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' or robot_role == 'striker' or robot_role == 'striker_dribble' :
                        if (global_ball_x > 0 or radius < 0.95) and robot_tactics == Tactics.OFFENSE :
                            strategy_state = 'dekati_bola'
                        elif robot_tactics == Tactics.DEFENSE and radius < 0.95 :
                            strategy_state = 'dekati_bola'
                    else :
                        strategy_state = 'dekati_bola'
                #else :
                    #strategy_state = 'defender'
        else :
            if robot_role == 'timer_striker' or robot_role == 'timer_striker2' or robot_role == 'striker' or robot_role == 'striker_dribble' :
                if global_ball_x > 0 or radius < 0.95 :
                    strategy_state = 'dekati_bola'
            else :
                strategy_state = 'dekati_bola'
    #if init_cari_bola :
        #waktu_mulai_cari_bola = rospy.get_rostime()
        #init_cari_bola = False
    pos = cari_bola_pos[waypoint_cari_bola]
    angle = pos[2]
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' or robot_role == 'striker' or robot_role == 'striker_dribble' :
         angle = math.atan2(global_ball_y - robot_pose[1],global_ball_x - robot_pose[0])
    #go_to_pos(pos[0],pos[1],angle,planning=True)
    #go_to_pos(pos[0],teammates[0][1],angle,planning=True) #Robot 1 -> Defender, Robot 2 -> Striker
    if waypoint_cari_bola == 1 :
        go_to_pos(pos[0],-global_ball_y,angle,planning=True)
    elif waypoint_cari_bola == 0 :
        # y = global_ball_y/global_ball_x * defense_line
        # y = math.copysign(y,global_ball_y)
        # go_to_pos(pos[0],y,angle,planning=True)
        x = global_ball_x
        if(math.fabs(x) < 1.0) :
            x = math.copysign(1.0, global_ball_x)
        y = global_ball_y/x * defense_line
        y = math.copysign(y,global_ball_y)
        go_to_pos(pos[0],y,angle,planning=True)
    '''
    durasi_cari_bola = rospy.get_rostime() - waktu_mulai_cari_bola
    if durasi_cari_bola.secs > waktu_cari_bola :
        init_cari_bola = True
        dx = pos[0] - robot_pose[0]
        #dy = pos[1] - robot_pose[1]
        dy = teammates[0][1] - robot_pose[1]
        error = math.hypot(dx,dy)
        if error < 0.35 :
            waypoint_cari_bola = waypoint_cari_bola + 1
            if waypoint_cari_bola > 1 :
                waypoint_cari_bola = 0
    '''
    rospy.loginfo('cari bola')

def dekati_bola() :
    global strategy_state, robot_behavior, robot_connection, local_ball_x, local_ball_y, ball_visible, angle_dribble_auto_on, radius_dribble_auto_on, dribble
    global kick_off_state, angle_mulai_dribble_bola, radius_mulai_dribble_bola, robot_pose, robot_role, teammates_dist_to_ball, last_position
    #radius = math.sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y)
    radius = math.hypot(local_ball_x,local_ball_y)
    angle = math.atan2(local_ball_y,local_ball_x)
    dribble = False
    if robot_behavior == 'stop' and robot_connection :
        strategy_state = 'timer'
    if radius < radius_dribble_auto_on and math.fabs(angle) < angle_dribble_auto_on :
        #if not kick_off_state :
        dribble = True
    #batas dekati_bola() begin
    approach = True
    for dist in teammates_dist_to_ball :
        if radius < dist and approach :
            approach = True
        else :
            approach = False
    if robot_role == 'timer_defense' or robot_role == 'timer_defense2' or robot_role == 'defender' or robot_role == 'defender2' :
        #if robot_pose[0] > 1.0 :
         if not approach :
            strategy_state = 'defender'
            return None
    if robot_role == 'timer_striker' or robot_role == 'timer_striker2' or robot_role == 'striker' or robot_role == 'striker_dribble' :
        #if robot_pose[0] < -1.0 :
         if not approach :
            strategy_state = 'striker'
            return None
    #batas dekati_bola() end
    if radius <= radius_mulai_dribble_bola and math.fabs(angle) <= angle_mulai_dribble_bola :
        if robot_role == 'striker' or robot_role == 'striker_kick' or robot_role == 'timer_striker' :
            strategy_state = 'dribble_ball'
            last_position = (robot_pose[0], robot_pose[1], robot_pose[2])
        elif robot_role == 'striker_dribble' or robot_role == 'timer_striker2' :
            strategy_state = 'dribble_to_goal'
        elif robot_role == 'defender' or robot_role == 'timer_defense' or robot_role == 'timer_defense2' or robot_role == 'defender2' :
            strategy_state = 'defender_dribble'
            last_position = (robot_pose[0], robot_pose[1], robot_pose[2])
    if not ball_visible :
        strategy_state = 'cari_bola'
    go_to_pos(local_ball_x, local_ball_y, angle, local='local', dribbler=dribble)
    rospy.loginfo('dekati bola')

def dribble_ball() :
    global strategy_state, local_ball_x, local_ball_y, kick_pos, ball_shield_angle, robot_pose, min_error_posisi_kick, goalie_name
    global min_error_sudut_kick, radius_mulai_dribble_bola, angle_mulai_dribble_bola, kick_at_kick_pos, dribble_to_goal_pos
    global kick_off_state, robot_behavior, robot_connection, kick_index, kick_random_state, obstacle, teammates, passing, last_position
    dx = kick_pos[kick_index][0] - robot_pose[0]
    dy = kick_pos[kick_index][1] - robot_pose[1]
    dw = kick_pos[kick_index][2] - robot_pose[2]
    error = math.hypot(dx,dy)
    error_sudut = math.fabs(dw)
    #radius_bola = math.sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y)
    radius_bola = math.hypot(local_ball_x,local_ball_y)
    angle_bola = math.atan2(local_ball_y,local_ball_x)
    if robot_behavior == 'stop' and robot_connection :
        strategy_state = 'timer'
    #if kick_off_state :
        #kick_off_state = False
        #strategy_state = 'kick_ball'
    #if math.fabs(dx) < 0.3 and math.fabs(dy) < 0.3 :
        #kick_at_kick_pos = True
    #else :
    kick_at_kick_pos = False
    #kick_index = 0
    if robot_pose[0] > 0.9 and robot_pose[1] > 0 :#and not kick_at_kick_pos:
        if kick_random_state :
            kick_index = randint(1,2)
            kick_random_state = False
        #kick_at_kick_pos = True
        #theta = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
        #go_to_pos(robot_pose[0],robot_pose[1],theta,planning=True,dribbler=True)
        #strategy_state = 'kick_ball'
    elif robot_pose[0] > 0.9 and robot_pose[1] < 0 :
        if kick_random_state :
            kick_index = randint(3,4)
            kick_random_state = False
    else :
        if kick_random_state :
            kick_index = 0
            kick_random_state = False

    #dribble up to 3 meter
    if math.hypot(last_position[0]-robot_pose[0],last_position[1]-robot_pose[1]) > 2.5 :
        if math.hypot(obstacle[0],obstacle[1]) < 0.75 and obstacle[2] < 0.35 :
            passing = True
            angle = None
            for mate in teammates :
                if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                    angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
            if angle == None :
                passing = False
                strategy_state = 'kick_ball'
                angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
                #go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
                go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
                return None
            else :
                strategy_state = 'kick_ball'
                return None
        else :
            passing = False
            angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
            go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            strategy_state = 'kick_ball'
            return None

    #goal intercept
    if robot_pose[0] > 3.0 :
        if math.hypot(obstacle[0],obstacle[1]) < 0.75 and obstacle[2] < 0.35 :
            passing = True
            angle = None
            for mate in teammates :
                if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                    angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
            if angle == None :
                passing = False
                #strategy_state = 'kick_ball'
                #angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
                go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
                #go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
                return None
            else :
                strategy_state = 'kick_ball'
                return None
        else :
            passing = False
            angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
            go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            strategy_state = 'kick_ball'
            return None
    elif robot_pose[0] > 4.5 and ( robot_pose[1] > 2.5 or robot_pose[1] < -2.5 ) :
        passing = True
        angle = None
        for mate in teammates :
            if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
        if angle == None :
            passing = False
            #strategy_state = 'kick_ball'
            #angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
            go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
            #go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            return None
        else :
            strategy_state = 'kick_ball'
            return None


    if not kick_at_kick_pos :
        if error < min_error_posisi_kick and error_sudut < min_error_sudut_kick and not kick_at_kick_pos :
            if math.hypot(obstacle[0],obstacle[1]) < 0.75 and obstacle[2] < 0.35 :
                passing = True
                angle = None
                for mate in teammates :
                    if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                        angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
                if angle == None :
                    passing = False
                    strategy_state = 'kick_ball'
                    #go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
                else :
                    go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            else :
                strategy_state = 'kick_ball'
        elif error_sudut < min_error_sudut_kick :
            if math.hypot(obstacle[0],obstacle[1]) < 0.75 and obstacle[2] < 0.35 :
                passing = True
                angle = None
                for mate in teammates :
                    if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                        angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
                if angle == None :
                    passing = False
                    #strategy_state = 'kick_ball'
                    go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
                else :
                    go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            else :
                strategy_state = 'kick_ball'
        if radius_bola > radius_mulai_dribble_bola or math.fabs(angle_bola) > angle_mulai_dribble_bola :
            strategy_state = 'dekati_bola'
        dribble_angle = ball_shield_angle
        if error < 1.26 :
            dribble_angle = kick_pos[kick_index][2]
        go_to_pos(kick_pos[kick_index][0],kick_pos[kick_index][1],dribble_angle,planning=True,dribbler=True)
    rospy.loginfo('dribble ball')

def dribble_to_goal() :
    global strategy_state, local_ball_x, local_ball_y, dribble_to_goal_pos, robot_connection
    global radius_mulai_dribble_bola, angle_mulai_dribble_bola, robot_behavior, ball_shield_angle
    #radius_bola = math.sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y)
    radius_bola = math.hypot(local_ball_x,local_ball_y)
    angle_bola = math.atan2(local_ball_y,local_ball_x)
    if robot_behavior == 'stop' and robot_connection :
        strategy_state = 'timer'
    if radius_bola > radius_mulai_dribble_bola or math.fabs(angle_bola) > angle_mulai_dribble_bola :
        strategy_state = 'dekati_bola'
    go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
    rospy.loginfo('dribble ball to goal')

def defender_dribble() :
    global strategy_state, local_ball_x, local_ball_y, dribble_to_goal_pos, robot_role, robot_pose, kick_off_state
    global robot_behavior, robot_connection, kick_index, ball_shield_angle, kick_random_state, home_pos, obstacle
    global kick_pos, min_error_sudut_kick, min_error_posisi_kick, radius_mulai_dribble_bola, angle_mulai_dribble_bola
    global robot_tactics, last_position
    #radius_bola = math.sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y)
    radius_bola = math.hypot(local_ball_x,local_ball_y)
    angle_bola = math.atan2(local_ball_y,local_ball_x)
    dx = kick_pos[kick_index][0]-robot_pose[0]
    dy = kick_pos[kick_index][1]-robot_pose[1]
    dw = kick_pos[kick_index][2]-robot_pose[2]
    error = math.hypot(dx,dy)
    error_sudut = math.fabs(dw)
    #kick_index = 0
    if robot_behavior == 'stop' and robot_connection :
        strategy_state = 'timer'
    if kick_off_state :
        #angle = math.atan2(local_ball_y,local_ball_x)
        #dw = math.fabs(angle)
        if home_pos[1] > 0 :
            #go_to_pos(local_ball_x, local_ball_y, angle, local='local', dribbler=True)
            go_to_pos(0.0,0.25,-(math.pi/2.0),dribbler=True)
            dx = -robot_pose[0]
            dy = 0.25 - robot_pose[1]
            dw = (-math.pi/2.0) - robot_pose[2]
        elif home_pos[1] < 0 :
            #go_to_pos(local_ball_x, local_ball_y, angle, local='local', dribbler=True)
            go_to_pos(0.0,-0.25,math.pi/2.0,dribbler=True)
            dx = -robot_pose[0]
            dy = -0.25 - robot_pose[1]
            dw = (math.pi/2.0) - robot_pose[2]
        #dx = local_ball_x
        #dy = local_ball_y
        error = math.hypot(dx,dy)
        error_sudut = math.fabs(dw)
        error_def_kick = 0.075
        #if error < radius_mulai_dribble_bola : #and error_sudut < min_error_sudut_kick :
        if error < error_def_kick and error_sudut < error_def_kick :
            go_to_pos(0.0,0.0,0.0,local='local',dribbler=True)
            strategy_state = 'kick_ball'
            #kick_off_state = False
        return None
    if radius_bola > radius_mulai_dribble_bola or angle_bola > angle_mulai_dribble_bola and not kick_off_state :
        strategy_state = 'dekati_bola'

    #dribble up to 3 meter
    if math.hypot(last_position[0]-robot_pose[0],last_position[1]-robot_pose[1]) > 2.5 :
        if math.hypot(obstacle[0],obstacle[1]) < 0.75 and obstacle[2] < 0.35 :
            passing = True
            angle = None
            for mate in teammates :
                if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                    angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
            if angle == None :
                passing = False
                strategy_state = 'kick_ball'
                angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
                #go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
                go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
                return None
            else :
                strategy_state = 'kick_ball'
                return None
        else :
            passing = False
            angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
            go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            strategy_state = 'kick_ball'
            return None

    #goal intercept
    if robot_pose[0] > 3.0 :
        if math.hypot(obstacle[0],obstacle[1]) < 0.75 and obstacle[2] < 0.35 :
            passing = True
            angle = None
            for mate in teammates :
                if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                    angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
            if angle == None :
                passing = False
                #strategy_state = 'kick_ball'
                #angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
                go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
                #go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
                return None
            else :
                strategy_state = 'kick_ball'
                return None
        else :
            passing = False
            angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
            go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            strategy_state = 'kick_ball'
            return None
    elif robot_pose[0] > 4.5 and ( robot_pose[1] > 2.5 or robot_pose[1] < -2.5 ) :
        passing = True
        angle = None
        for mate in teammates :
            if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
        if angle == None :
            passing = False
            #strategy_state = 'kick_ball'
            #angle = math.atan2(dribble_to_goal_pos[1]-robot_pose[1],dribble_to_goal_pos[0]-robot_pose[0])
            go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
            #go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            return None
        else :
            strategy_state = 'kick_ball'
            return None


    if robot_role == 'timer_defense' or robot_role == 'defender' and not kick_off_state :
        if robot_pose[1] > 0 :#and not kick_at_kick_pos :
            if kick_random_state :
                kick_index = randint(1,2)
                kick_random_state = False
        elif robot_pose[1] < 0 :
            if kick_random_state :
                kick_index = randint(3,4)
                kick_random_state = False
        else :
            if kick_random_state :
                kick_index = 0
                kick_random_state = False
        dribble_angle = ball_shield_angle
        #if robot_pose[1] < 0 :
            #dribble_angle = 0.785
        #elif robot_pose[1] > 0 :
            #dribble_angle = -0.785
        if obstacle[1] < robot_pose[1] :
            dribble_angle = 0.785
        else :
            dribble_angle = -0.785
        if error < 0.25 :
            dribble_angle = kick_pos[kick_index][2]
        if error < min_error_posisi_kick and error_sudut < min_error_sudut_kick and robot_tactics == Tactics.OFFENSE :
            #if math.hypot(obstacle[0],obstacle[1]) < 0.4 and obstacle[2] < 0.35 :
            #    go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
            #else :
            #    go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
            #    strategy_state = 'kick_ball'
            if math.hypot(obstacle[0],obstacle[1]) < 0.75 and obstacle[2] < 0.35 :
                passing = True
                angle = None
                for mate in teammates :
                    if mate[4] == True and mate[3] != robotname and mate[3] != goalie_name :
                        angle = math.atan2(mate[1]-robot_pose[1],mate[0]-robot_pose[0])
                if angle == None :
                    passing = False
                    #strategy_state = 'kick_ball'
                    go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
                else :
                    go_to_pos(0.0,0.0,angle,local='local',dribbler=True)
            else :
                strategy_state = 'kick_ball'
        elif error_sudut < min_error_sudut_kick and robot_tactics == Tactics.DEFENSE :
            go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
            strategy_state = 'kick_ball'
        #elif error_sudut < 0.873 :
            #go_to_pos(0.0,0.0,0.0,local='local',dribbler=False)
            #strategy_state = 'kick_ball'
        go_to_pos(kick_pos[kick_index][0],kick_pos[kick_index][1],dribble_angle,planning=True,dribbler=True)
    elif robot_role == 'timer_defense2' or robot_role == 'defender2' and not kick_off_state :
        go_to_pos(dribble_to_goal_pos[0],dribble_to_goal_pos[1],ball_shield_angle,planning=True,dribbler=True)
    rospy.loginfo('dribble ball to goal')

def kick_ball() :
    global strategy_state, kick_off_state, robot_role, kick_random_state
    kick()
    kick_random_state = True
    #if kick_off_state :
        #kick_off_state = False
    if robot_role == 'timer_defense' or robot_role == 'timer_defense2' or robot_role == 'defender' or robot_role == 'defender2' :
        strategy_state = 'defender'
    if strategy_state != 'defender' :
        strategy_state = 'cari_bola'
        if robot_role == 'goalie' :
            strategy_state = 'goalie'
    rospy.loginfo('kick ball')

def goalie():
    global strategy_state, global_ball_y, robot_state, robot_behavior, robot_pose, distance
    global local_ball_x, local_ball_y, ball_visible
    if ball_visible :
        radius = math.hypot(local_ball_x,local_ball_y)
        if radius < 0.5 :
            strategy_state = 'kick_ball'
            return None
    if robot_behavior == 'stop' :
        dx = -5.62 - robot_pose[0]
        dy = robot_pose[1]
        dw = math.fabs(robot_pose[2])
        go_to_pos(-5.62,0.0,0.0)
        if math.hypot(dx,dy) < 0.2 and dw < 0.2 :
            go_to_pos(0.0,0.0,0.0,local='local')
    else :
    # masukin strategi kiper terbaru disini:
        if goalie_intercept_valid : 
            go_to_pos(goalie_intercept_pos[0]/100.0, goalie_intercept_pos[1]/100.0, goalie_intercept_pos[2]*math.pi/180.0)
        else :
            factor = (math.pow( (global_ball_y/distance), 5))
            if factor > 1 :
                factor = 1
            y_setpoint = global_ball_y * (1 - factor)
            if ball_visible :
                if y_setpoint > 1.0 :
                    predicted_trajcetory_error = global_ball_y/2.0
                    y_setpoint = 1.0 - predicted_trajcetory_error
                elif y_setpoint < -1.0 :
                    predicted_trajcetory_error = global_ball_y/2.0
                    y_setpoint = -1.0 - predicted_trajcetory_error
            else :
                y_setpoint = 0.0
                angle_bola = 0.0
            go_to_pos(-5.65,y_setpoint,0.0)
    '''
    dx = local_ball_x
    dy = local_ball_y
    dw = math.atan2(dy,dx)
    radius = math.hypot(dx,dy)
    sudut = math.fabs(dw)
    if radius < radius_mulai_dribble_bola and sudut < angle_mulai_dribble_bola  :
        error_x = -4.12 - robot_pose[0]
        error_y = 0.6 - robot_pose[1]
        error = math.hypot(error_x,error_y)
        if error < min_error_posisi_kick :
            strategy_state = 'kick_ball'
        go_to_pos(-4.12,0.6,math.pi/4,dribbler=True)
    '''
    rospy.loginfo('goalie')


strategy_list = {
    'timer' : timer,
    'stop' : stop,
    'go_to_home' : go_to_home,
    'go_to_home_cari_bola' : go_to_home_cari_bola,
    'cari_bola' : cari_bola,
    'dekati_bola' : dekati_bola,
    'dribble_ball' : dribble_ball,
    'dribble_to_goal' : dribble_to_goal,
    'defender_dribble' : defender_dribble,
    'kick_ball' : kick_ball,
    'defender' : defender,
    'striker' : striker,
    'striker_dribble' : striker_dribble,
    'striker_kick' : striker_kick,
    'goalie' : goalie
}

#################################################################################################

#part of 'get the ball' priority
#nearest_agent = 0
#distance_priority = [(True,0.0,0),(True,0.0,1),(True,0.0,2),(True,0.0,3),(True,0.0,4),(True,0.0,5)]
#for agent in teammates :
#    if(robotname!=agent[3]) :
#        if(agent[4]) :
#            distance_priority = (False,math.hypot(global_ball_x - agent[0],global_ball_y - agent[1]),agent[3])
#        else :
#            distance_priority = (True,math.hypot(global_ball_x - agent[0],global_ball_y - agent[1]),agent[3])
#max_agent = min(distance_priority)
#this_agent = math.hypot(robot_pose[0],robot_pose[1])
#if(max_agent[1]>this_agent) :
#    nearest_agent = max_agent[2]
#else :
#    nearest_agent = robotname
#end part of 'get the ball' priority

#def get_ball_priority(delay) :
def get_ball_priority() :
    global teammates, nearest_agent, distance_priority, global_ball_x, global_ball_y, robotname, robot_connection, use_ball_priority
    #while True :
    for agent in teammates :
        if robotname!=agent[3] :
            dx = global_ball_x - agent[0]
            dy = global_ball_y - agent[1]
            if agent[4] :
                distance_priority[agent[3]] = (False,math.hypot(dx,dy),agent[3])
            else :
                distance_priority[agent[3]]  = (True,999,agent[3])
        else :
            distance_priority[robotname] = (True,999,robotname)
    min_agent_distance = min(distance_priority)
    #print max_agent
    this_agent_distance = math.hypot(robot_pose[0] - global_ball_x,robot_pose[1] - global_ball_y)
    nearest_agent = robotname
    if robot_connection :
        if use_ball_priority :
            if min_agent_distance[1]<this_agent_distance :
                #nearest_agent = robotname
                if not min_agent_distance[0] :
                    nearest_agent = min_agent_distance[2]
                #else :
                    #nearest_agent = robotname
    #time.sleep(delay)

def obstacle_estimation(delay) :
    global obstacles, obstacle, robot_pose
    obstacle = (obstacle[0],obstacle[1],2*math.pi)
    if obstacles is not None :
        for obs in obstacles :
            dx = obs.x - robot_pose[0]
            dy = obs.y - robot_pose[1]
            theta = atan2(dy,dx)
            if math.fabs(theta - robot_pose[2]) < obstacle[2] :
                obstacle = (obs.x,obs.y,theta)
    time.sleep(delay)

def handle_strategy_request(req) :
    global strategy_state, home_pos, kick_pos, cari_bola_pos, robot_role, strategy_list
    ok = 0
    for s, f in strategy_list.items() :
        if req.strategy_state == s :
            strategy_state = req.strategy_state
            ok = 1
    if '--kick_pos' in req.option or '-k' in req.option :
        kick_pos[0] = (req.kick_pos.x, req.kick_pos.y, req.kick_pos.theta)
    if '--home_pos' in req.option or '-h' in req.option :
        home_pos = (req.home_pos.x, req.home_pos.y, req.home_pos.theta)
    if req.role in role_list :
        robot_role = req.role
    rospy.loginfo('strategy request %s %s %s %s' %(strategy_state,kick_pos,home_pos,cari_bola_pos))
    return StrategyServiceResponse(ok)

def update_state() :
    global strategy_list
    strategy_list[strategy_state]()

def go_to_pos(x,y,w,local='',dribbler=False,planning=False):
    global robot_control_msg
    robot_control_msg.target_pose.x = x
    robot_control_msg.target_pose.y = y
    robot_control_msg.target_pose.theta = w
    robot_control_msg.plan = planning
    if local == 'local':
        robot_control_msg.option.data = 'local'
    else :	
        robot_control_msg.option.data = ''
    if dribbler :
        robot_control_msg.dribbler = 1
    else :
        robot_control_msg.dribbler = 0

def world_model_callback(msg) :
    global ball_visible, local_ball_x, local_ball_y, robot_pose, last_ball_visible_time
    global global_ball_x, global_ball_y, obstacles
    now = rospy.get_rostime()
    if msg.ball_visible :
        ball_visible = True
        last_ball_visible_time = now
    elif now - last_ball_visible_time > rospy.Duration(1) :
        ball_visible = False
    local_ball_x = msg.local_balls_kf.x
    local_ball_y = msg.local_balls_kf.y
    global_ball_x = msg.global_balls_kf.x
    global_ball_y = msg.global_balls_kf.y
    robot_pose = (msg.pose.x, msg.pose.y, msg.pose.theta)
    obstacles = msg.obstacles
#    rospy.loginfo('world model callback : %s %f %f' %(msg.robot_name,local_ball_x,local_ball_y))

def strategy_pos_callback(msg) :
    global ball_shield_angle
    ball_shield_angle = msg.ball_shield_angle

def reconfigure_callback(cfg, level) :
    global distance, radius_dribble_auto_on, angle_dribble_auto_on
    global radius_mulai_dribble_bola, angle_mulai_dribble_bola
    global min_error_posisi_kick, min_error_sudut_kick
    global min_error_posisi_homing, min_error_sudut_homing
    global error_positioning, error_sudut_positioning, radius_mulai_kick_off
    distance = cfg.dist
    radius_dribble_auto_on = cfg.radius_dribble_auto_on
    angle_dribble_auto_on = cfg.angle_dribble_auto_on
    radius_mulai_dribble_bola = cfg.radius_mulai_dribble_bola
    angle_mulai_dribble_bola = cfg.angle_mulai_dribble_bola
    min_error_posisi_kick = cfg.min_error_posisi_kick
    min_error_sudut_kick = cfg.min_error_sudut_kick
    min_error_posisi_homing = cfg.min_error_posisi_homing
    min_error_sudut_homing = cfg.min_error_sudut_homing
    error_positioning = cfg.error_positioning
    error_sudut_positioning = cfg.error_sudut_positioning
    radius_mulai_kick_off = cfg.radius_mulai_kick_off
    return cfg

def strategy_param_callback(msg) :
    global distance, radius_dribble_auto_on, angle_dribble_auto_on
    global radius_mulai_dribble_bola, angle_mulai_dribble_bola
    global min_error_posisi_kick, min_error_sudut_kick
    global min_error_posisi_homing, min_error_sudut_homing
    global error_positioning, error_sudut_positioning, radius_mulai_kick_off
    radius_dribble_auto_on = msg.radius_dribble_auto_on
    angle_dribble_auto_on = msg.angle_dribble_auto_on
    radius_mulai_dribble_bola = msg.radius_mulai_dribble_bola
    angle_mulai_dribble_bola = msg.angle_mulai_dribble_bola
    min_error_posisi_kick = msg.min_error_posisi_kick
    min_error_sudut_kick = msg.min_error_sudut_kick
    min_error_posisi_homing = msg.min_error_posisi_homing
    min_error_sudut_homing = msg.min_error_sudut_homing
    error_positioning = msg.error_positioning
    error_sudut_positioning = msg.error_sudut_positioning
    radius_mulai_kick_off = msg.radius_mulai_kick_off

def robot_control_info_callback(msg) :
    global error_radius, error_angle
    error_radius = msg.error_radius
    error_angle = msg.error_angle

def teammates_callback(msg) :
    global teammates, robotname, robot_behavior, robot_connection, robot_state, state_list, behavior_list
    global home_pos_state, robot_pose, global_ball_x, global_ball_y, teammates_dist_to_ball
    robotname = msg.robotname
    teammates_dist_to_ball = []
    for i in range(0,len(msg.pose)) :
        pose_tuple = (msg.pose[i].x,msg.pose[i].y,msg.pose[i].theta,i,msg.available[i])
        if pose_tuple[4] == True and pose_tuple[3] != robotname :
            teammates_dist_to_ball.append(math.hypot(global_ball_x-pose_tuple[0],global_ball_y-pose_tuple[1]))
        teammates[i] = pose_tuple
    robot_behavior = behavior_list[msg.behavior]
    robot_connection = msg.isConnected
    robot_state = state_list[msg.state]
    if msg.isManualPositioning :
        if robot_state in home_pos_state :
            home_pos_state[robot_state] = (robot_pose[0],robot_pose[1],robot_pose[2])

def goalie_intercept_callback(msg) :
    global goalie_intercept_pos, goalie_intercept_valid
    pt = msg.intercept_point
    valid = msg.valid
    goalie_intercept_pos = (pt.x, pt.y, pt.theta)
    goalie_intercept_valid = valid

def kick():
    try:
        global kicker_service, kick_off_state, passing
        if kick_off_state or passing :
            respl = kicker_service(2)
            kick_off_state = False
            passing = False
        else :
            respl = kicker_service(3)
    except rospy.ServiceException, e:
        rospy.logerr('Kick service call failed: %s' %e)

def publish():
    global robot_control_pub
    global robot_control_msg
    global strategy_info_pub
    global strategy_info_msg
    global strategy_state
    global robot_role
    strategy_info_msg.strategy_state = strategy_state
    strategy_info_msg.role = robot_role
    robot_control_pub.publish(robot_control_msg)
    strategy_info_pub.publish(strategy_info_msg)

def strategy():
    robot_name = os.getenv('FUKURO')
    rospy.init_node('fukuro_strategy_nasional')
    print('waiting for service')
    rospy.wait_for_service(robot_name+'/kick_service')
    global kicker_service
    global robot_control_pub
    global strategy_info_pub
    global home_pos
    global strategy_state
    global robot_role
    kicker_service = rospy.ServiceProxy(robot_name+'/kick_service',Shoot)
    robot_control_pub = rospy.Publisher(robot_name+'/robot_control',RobotControl,queue_size=1)
    strategy_info_pub = rospy.Publisher(robot_name+'/strategy_info',StrategyInfo,queue_size=1)
    strategy_server = rospy.Service(robot_name+'/strategy_service',StrategyService,handle_strategy_request)
    rospy.Subscriber(robot_name+'/world_model', WorldModel, world_model_callback)
    rospy.Subscriber('/strategy_pos', StrategyPositioning, strategy_pos_callback)
    rospy.Subscriber(robot_name+'/robot_control_info', RobotControlInfo, robot_control_info_callback)
    rospy.Subscriber(robot_name+'/teammates', Teammates, teammates_callback)
    rospy.Subscriber(robot_name+'/strategy_param', StrategyParam, strategy_param_callback)

    ##TODO : dont subscibe if not goalie
    #rospy.Subscriber(robot_name+'/goalie_intercept', InterceptPoint, goalie_intercept_callback)

    try :
        thread.start_new_thread(obstacle_estimation,(0.1,))
        #thread.start_new_thread(get_ball_priority,(0.1,))
        pass
    except e:
        rospy.logerr('[Threading Error] %s' %e)
    #srv = Server(StrategyConfig, reconfigure_callback)
    print 'ready'
    rate = rospy.Rate(30)

    if rospy.has_param("home_x") :
        if rospy.has_param("home_y") :
            if rospy.has_param("home_w") :
                x = rospy.get_param("home_x")
                y = rospy.get_param("home_y")
                w = rospy.get_param("home_w")
                home_pos = (float(x), float(y), float(w))
                rospy.logwarn("setting home_pos : %s", home_pos)

    if rospy.has_param("strategy_state") :
        strategy_state = rospy.get_param("strategy_state")

    if rospy.has_param("robot_role") :
        robot_role = rospy.get_param("robot_role")

    ##TODO : dont subscibe if not goalie
    if robot_role=='goalie':
        rospy.Subscriber(robot_name+'/goalie_intercept', InterceptPoint, goalie_intercept_callback)
                
    while not rospy.is_shutdown():
        #rospy.loginfo('strategy loop')
        get_ball_priority()
        update_state()
        publish()
        rate.sleep()

if __name__ == "__main__" :
    strategy()
