#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import os
import math
from geometry_msgs.msg import Pose2D
from fukuro_common.srv import StrategyService
from fukuro_common.srv import StrategyServiceResponse
from fukuro_common.srv import LocalizationService
from fukuro_common.srv import LocalizationServiceResponse
from fukuro_common.srv import KeypadService
from fukuro_common.srv import KeypadServiceResponse

localization_client = None
strategy_client = None

robotname = int(os.getenv('AGENT')) - 1
#reset_strategy('timer',home=Pose2D(-3.0,1.0,0.0),kick=Pose2D(3.0,-0.6,0.0),role='timer_striker')
role_list = ('timer_defense','timer_striker','goalie')
kick_pos = ((-3.0,-1.0,0.0),(3.0,-1.0,0.0),(-4.12,0.0,0.0))
home_pos = ((-2.5,1.0,0.0),(-2.5,-1.0,0.0),(-4.12,0.0,0.0))

def handle_request(req):
    global role_list, kick_pos, home_pos, robotname
    print "keypad : %s"%(req.key)
    key = req.key
    if key == 0 :
        reset_strategy('stop')
    elif key == 1 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,0.0,0.0),role='defender2')
    elif key == 2 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,0.0,0.0),role='defender2')
    elif key == 3 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,-1.0,0.0),role='defender')
    elif key == 4 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,1.0,0.0),role='defender')
    elif key == 5 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,-1.0,0.0),role='striker')
    elif key == 6 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,0.0,0.0),role='striker_dribble')
    elif key == 7 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('go_to_home_cari_bola',home=Pose2D(-3.0,0.0,0.0),role='striker')
    elif key == 8 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('go_to_home_cari_bola',home=Pose2D(-3.0,0.0,0.0),role='striker_dribble')
    elif key == 9 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('timer',home=Pose2D(-2.5,-1.0,0.0),role='timer_striker')
    elif key == 10 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('timer',home=Pose2D(-2.5,-1.0,0.0),role='timer_striker2')
    elif key == 11 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,-1.0,0.0),kick=Pose2D(3.0,0.0,0.0),role='striker')
    elif key == 12 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('timer',home=Pose2D(-2.5,-1.0,0.0),kick=Pose2D(3.0,0.0,0.0),role='timer_striker')
    elif key == 13 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,-1.0,0.0),kick=Pose2D(3.0,-0.6,0.0),role='striker')
    elif key == 14 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('timer',home=Pose2D(-2.5,-1.0,0.0),kick=Pose2D(3.0,-0.6,0.0),role='timer_striker')
    elif key == 15 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,1.0,0.0),role='striker')
    elif key == 16 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,0.0,0.0),role='striker_dribble')
    elif key == 17 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('go_to_home_cari_bola',home=Pose2D(-3.0,0.0,0.0),role='striker')
    elif key == 18 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('go_to_home_cari_bola',home=Pose2D(-3.0,0.0,0.0),role='striker_dribble')
    elif key == 19 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('timer',home=Pose2D(-3.0,-1.0,0.0),role='timer_defense2')
    elif key == 20 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('timer',home=Pose2D(-3.0,1.0,0.0),role='timer_defense2')
    elif key == 21 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('timer',home=Pose2D(-3.0,-1.0,0.0),role='timer_defense')
    elif key == 22 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('timer',home=Pose2D(-3.0,1.0,0.0),role='timer_defense')
    elif key == 23 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('timer',home=Pose2D(-3.0,1.0,0.0),role='timer_striker')
    elif key == 24 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('timer',home=Pose2D(-3.0,1.0,0.0),role='timer_striker2')
    elif key == 25 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,0.0,0.0),kick=Pose2D(3.0,0.0,0.0),role='striker')
    elif key == 26 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('timer',home=Pose2D(-3.0,1.0,0.0),kick=Pose2D(3.0,0.0,0.0),role='timer_striker')
    elif key == 27 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('go_to_home',home=Pose2D(-3.0,0.0,0.0),kick=Pose2D(3.0,-0.6,0.0),role='striker')
    elif key == 28 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('timer',home=Pose2D(-3.0,1.0,0.0),kick=Pose2D(3.0,-0.6,0.0),role='timer_striker')
    elif key == 29 :
        reset_localization(-5.62,0.0,0.0)
        reset_strategy('goalie', home=None, kick=None, role='goalie')
    elif key == 30 :
        reset_localization(-6.3,2.65,0.0)
        reset_strategy('timer',home=Pose2D(home_pos[0][0],home_pos[0][1],home_pos[0][2]),kick=Pose2D(kick_pos[robotname][0],kick_pos[robotname][1],kick_pos[robotname][2]),role=role_list[robotname])
        #reset_strategy('timer',home=Pose2D(-3.0,1.0,0.0),kick=Pose2D(3.0,-0.6,0.0),role='timer_striker')
    elif key == 31 :
        reset_localization(-6.3,-2.65,0.0)
        reset_strategy('timer',home=Pose2D(home_pos[1][0],home_pos[1][1],home_pos[1][2]),kick=Pose2D(kick_pos[robotname][0],kick_pos[robotname][1],kick_pos[robotname][2]),role=role_list[robotname])
    elif key == 32 :
        reset_localization(0.0,0.0,0.0)
    elif key == 33 :
        reset_localization(-5.62,0.0,0.0)
        reset_strategy('goalie',home=Pose2D(home_pos[2][0],home_pos[2][1],home_pos[2][2]),kick=Pose2D(kick_pos[robotname][0],kick_pos[robotname][1],kick_pos[robotname][2]),role=role_list[robotname])
    return KeypadServiceResponse(1)

#meter, radians
def reset_localization(x,y,w) :
    global localization_client
    print "reset localization"
    angle = w
    if angle < 0 :
        angle += 2*math.pi
    angle *= 180.0/math.pi
    try :
        loc_res = localization_client(x*100, y*100, w, 1)
        return loc_res.ok
    except rospy.ServiceException, e:
        print "Service call failed: %s" %e

def reset_strategy(state, home=None, kick=None, role='') :
    global strategy_client
    print 'reset strategy %s %s %s %s' %(state, home, kick, role)
    try :
        option = ''
        if home is not None :
            option += '-h '
        if kick is not None :
            option += '-k '
        strategy_res = strategy_client(strategy_state=state, option=option, home_pos=home, kick_pos=kick, role=role)
        return strategy_res.ok
    except rospy.ServiceException, e:
        print "Service call failed: %s" %e

def keypad_server():
    global localization_client, strategy_client
    robot_name = os.getenv('FUKURO')
    rospy.init_node('fukuro_keypad_server')
    localization_client = rospy.ServiceProxy('localization_service', LocalizationService)
    strategy_client = rospy.ServiceProxy(robot_name+'/strategy_service', StrategyService)
    s = rospy.Service(robot_name+'/keypad_service', KeypadService, handle_request)
    print 'keypad server ready.'
    rospy.spin()

if __name__ == "__main__":
    keypad_server()
