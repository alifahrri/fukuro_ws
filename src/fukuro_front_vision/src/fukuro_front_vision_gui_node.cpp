#include <ros/ros.h>
#include <QApplication>
#include "gui/scenemodifier.h"
#include "gui/widget3d.h"
#include "gui/frontviswidget.h"

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_front_vision_gui");
  ros::NodeHandle node;

  QApplication app(argc,argv);
  FrontVisWidget widget;
  widget.showMaximized();

  ros::Subscriber frontvis_sub = node.subscribe("/front_vision/3d",1,&SceneModifier::updateBall,widget.widget3d->modifier);

  ros::AsyncSpinner spinner(2);

  spinner.start();
  return app.exec();
}
