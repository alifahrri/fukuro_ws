#include <ros/ros.h>
#include "frontvis.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "fukuro_front_vision_node");
  ros::NodeHandle nh;
  std::string detection;
  fukuro::FrontVision::detection_t detect = fukuro::FrontVision::COLOR_LUT;
  // if(ros::param::get("detection",detection))
  //   if(detection == std::string("yolo"))
  //     detect = fukuro::FrontVision::YOLO;
  fukuro::FrontVision frontvis(nh, detect);
  ros::Subscriber img_sub = nh.subscribe("/front_camera/image_raw",1,&fukuro::FrontVision::detect,&frontvis);
  ros::spin();

  return 0;
}
