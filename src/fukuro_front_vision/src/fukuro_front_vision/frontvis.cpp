#include "frontvis.h"

fukuro::FrontVision::FrontVision(ros::NodeHandle &node, detection_t detection)
{
  this->detection = detection;
  using std::string;
  if(detection == YOLO)
  {
    std::string cfg_file = node.param(string("cfg"),string());
    std::string data_file = node.param(string("data"),string());
    std::string weight_file = node.param(string("weight"),string());
    std::string label_file = node.param(string("classes"),string());

    ROS_INFO("cfg : %s; data : %s; weight : %s; label : %s", cfg_file.c_str(), data_file.c_str(), weight_file.c_str(), label_file.c_str());

    // darknet = new Darknet(cfg_file, data_file, weight_file, label_file);
  }
  else if(detection == COLOR_LUT)
  {
    string fukuro;
    string user;
    string agent;
    string fukuro_ws;
    ros::get_environment_variable(fukuro,"FUKURO");
    ros::get_environment_variable(user,"USER");
    ros::get_environment_variable(agent,"AGENT");
    ros::get_environment_variable(fukuro_ws,"FUKUROWS");
    string calibration_dir(string("/home/")+user+"/fukuro_ws/src/fukuro_front_vision_calibration/calib_results/"+agent);
    color_segment = new ColorSegment(calibration_dir);
    ball_seeker = new BallSeeker;
    std::ifstream stream(calibration_dir+"/detection.txt",std::ios::in);
    std::string detect_size;
    std::getline(stream,detect_size);

    std::string max_box_diff;
    std::getline(stream,max_box_diff);

    ball_seeker->setMinBox(std::stoi(detect_size));
    ball_seeker->setMaxW2HDiff(std::stof(max_box_diff));
  }

  pub = node.advertise<fukuro_common::BoundingBoxes>("/front_vision",3);
}

void fukuro::FrontVision::detect(const sensor_msgs::ImageConstPtr &msg)
{
  ROS_INFO("image callback");
  cv_bridge::CvImagePtr img_ptr;
  img_ptr = cv_bridge::toCvCopy(msg, "bgr8");
  switch (detection) {
    case YOLO:
    {
//      darknet->detect(&img_ptr->image);
//      auto result = darknet->getResult();
//      this->publish(result);
    }
      break;
    default:
    {
      auto segment_result = color_segment->process(img_ptr->image);
      auto ball_segments = ball_seeker->process(segment_result);
      this->publish(ball_segments);
    }
      break;
  }
}

//void fukuro::FrontVision::publish(const Darknet::BBoxes &detection)
//{
//  fukuro_common::BoundingBoxes bboxes;
//  for(const auto& box : detection)
//  {
//    fukuro_common::BBox bbox;
//    bbox.x = box.x;
//    bbox.y = box.y;
//    bbox.w = box.w;
//    bbox.h = box.h;
//    bbox.label = box.label;
//    bbox.prob = box.confidence;
//    bbox.rgb.push_back(box.rgb[0]);
//    bbox.rgb.push_back(box.rgb[1]);
//    bbox.rgb.push_back(box.rgb[2]);
//    bboxes.bboxes.push_back(bbox);
//  }
//  this->publish(bboxes);
//}

void fukuro::FrontVision::publish(const fukuro::ImageSegments &ball_segments)
{
  fukuro_common::BoundingBoxes bboxes;
  for(const auto& box : ball_segments)
  {
    fukuro_common::BBox bbox;
    bbox.x = box.bbox.x0;
    bbox.y = box.bbox.y0;
    bbox.w = box.bbox.width;
    bbox.h = box.bbox.height;
    bbox.label = "ball";
    bbox.prob = 0.0;
    bbox.rgb.push_back(1.0);
    bbox.rgb.push_back(0.0);
    bbox.rgb.push_back(0.0);
    bboxes.bboxes.push_back(bbox);
  }
  this->publish(bboxes);
}

inline
void fukuro::FrontVision::publish(const fukuro_common::BoundingBoxes &msg)
{
  ROS_INFO("publising bbox");
  pub.publish(msg);
}
