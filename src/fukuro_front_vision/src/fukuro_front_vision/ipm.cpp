#include "ipm.h"
#include <fstream>

#define DEG2RAD M_PI/180.0f

using namespace fukuro;

IPM::IPM()
{

}

IPM::IPM(std::string dir)
{
  this->load(dir);
}

cv::Mat IPM::birdsEye(const cv::Mat &in)
{
  int idx = 0;
  cv::Mat out(map_size, CV_8UC3);
  //  cv::Mat in_gray;
  //  in_gray.create(in.rows,in.cols,CV_8UC1);
  //  cv::cvtColor(in,in_gray,cv::COLOR_BGR2GRAY);
  if(map_table.empty())
    return out;
  auto& out_ptr = out.data;
  const auto& in_ptr = in.data;
  //  auto& in_ptr = in_gray.data;
  for(int j=0; j<map_size.height; ++j)
    for(int i=0; i<map_size.width; ++i) {
        //        int k = in.step[0]*j + in.step[1]*i;
        int k = (i*map_size.height + j)*3;
        int table_idx = map_table[idx]*3;
        //        table_idx *= 3;
        if(table_idx > 0) {
            //            out_ptr[i*map_size.height + j] = in_ptr[table_idx];
            out_ptr[k]    = in_ptr[table_idx];
            out_ptr[k+1]  = in_ptr[table_idx+1];
            out_ptr[k+2]  = in_ptr[table_idx+2];
          }
        else
          {
            out_ptr[k]    = 0;
            out_ptr[k+1]  = 0;
            out_ptr[k+2]  = 0;
            //            out_ptr[i*map_size.height + j] = 0;
          }
        ++idx;
      }
  return out;
}

void IPM::birdsEye(cv::Mat &in, cv::Mat &out)
{
  out = birdsEye(in);
}

void IPM::save(const std::string &dir)
{
  auto settings_file = dir+"/map.xml";
  auto map_file = dir+"/ipm_table.map";
  cv::FileStorage map_settings(settings_file, cv::FileStorage::WRITE);
  map_settings << "map" << "[" << "{"
               << "width" << "["
               << map_size.width << "]"
               << "height" << "["
               << map_size.height << "]"
               << "fov_h" << "["
               << fov_h << "]"
               << "fov_v" << "["
               << fov_v << "]"
               << "cam_z" << "["
               << cam_height << "]"
               << "cam_x" << "["
               << cam_xpos << "]"
               << "vanish_pt_x" << "["
               << vanish_pt.x << "]"
               << "vanish_pt_y" << "["
               << vanish_pt.y << "]"
               << "}" << "]";
  map_settings.release();
  std::ofstream map_writer(map_file, std::ios::binary | std::ios::out);
  map_writer.write((char*)map_table.data(), sizeof(int)*(map_table.size()));
  map_writer.close();
}

void IPM::load(const std::string &dir)
{
  auto settings_file = dir+"/map.xml";
  auto map_file = dir+"/ipm_table.map";
  cv::FileStorage map_settings(settings_file, cv::FileStorage::READ);
  auto map = map_settings["map"];
  //  auto map_it = map.begin();
  //  auto map_end = map.end();
  for(auto m : map) {
      this->map_size.width = m["width"];
      this->map_size.height = m["height"];
      this->fov_h = m["fov_h"];
      this->fov_v = m["fov_v"];
      this->cam_height = m["cam_z"];
      this->cam_xpos = m["cam_x"];
      this->vanish_pt.x = m["vanish_pt_x"];
      this->vanish_pt.y = m["vanish_pt_y"];
    }
  map_settings.release();

  this->map_table.clear();
  this->map_table.resize(map_size.area());
  std::ifstream map_reader(map_file, std::ios::binary | std::ios::in);
  if(map_reader.is_open())
    map_reader.read((char*)map_table.data(),sizeof(int)*(map_table.size()));
  map_reader.close();
}

void IPM::buildMap(const cv::Size &in, const cv::Size &out, int *table)
{
  if(!table)
  {
    map_table.clear();
    map_table.resize(out.area());
    table = this->map_table.data();
  }
  map_size = out;
  auto alpha_h = 0.5f * fov_h * DEG2RAD;
  auto alpha_v = 0.5f * fov_v * DEG2RAD;
  auto gamma = -(double)(vanish_pt.x - (in.width >> 1)) * alpha_h / (in.width >> 1);
  auto theta = -(double)(vanish_pt.y - (in.height >> 1)) * alpha_v / (in.height >> 1);
  auto x_start = out.height >> 1;
  auto x_end = out.height + x_start;
  auto half_w = out.width >> 1;
  for(int y=0; y<out.width; ++y)
    for(int x=x_start; x<x_end; ++x)
    {
      auto idx = y * out.height + (x - x_start);
      auto dx = x_end-x-cam_xpos;
      auto dy = y-half_w;
      if(dy==0)
        table[idx] = table[idx-out.height];
      else
      {
        auto u = (int)((atan(cam_height*sin(atan((float)dy/dx)) / dy) - (theta-alpha_v)) / (2*alpha_v / in.height));
        auto v = (int)((atan((float)dy / dx) - (gamma - alpha_h)) / (2*alpha_h / in.width));
        if(u>=0 && u<in.height && v>=0 && v<in.width)
          table[idx] = in.width * u + v;
        else
          table[idx] = -1;
      }
    }
}
