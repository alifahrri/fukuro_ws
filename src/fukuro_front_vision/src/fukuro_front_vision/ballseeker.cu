#include "ballseeker.h"
#include "colorsegment.h"
#include <ros/ros.h>
#include <cuda_runtime.h>
#include <cuda.h>

#define WARP_SIZE 32

#define MIN_WIDTH 10
#define MAX_BOX 50

#define VISION_COLORSEGMENT_BALL 0
#define COLORSEGMENT_TEST

#define CUDA_CALL(x) { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    exit(EXIT_FAILURE);}}
#define CHECK_LAUNCH_ERROR()                                          \
    do {                                                              \
    /* Check synchronous errors, i.e. pre-launch */                   \
    cudaError_t err = cudaGetLastError();                             \
    if (cudaSuccess != err) {                                         \
    fprintf (stderr, "Cuda error in file '%s' in line %i : %s.\n",    \
    __FILE__, __LINE__, cudaGetErrorString(err) );                    \
    exit(EXIT_FAILURE);                                               \
    }                                                                 \
    /* Check asynchronous errors, i.e. kernel failed (ULF) */         \
    err = cudaThreadSynchronize();                                    \
    if (cudaSuccess != err) {                                         \
    fprintf (stderr, "Cuda error in file '%s' in line %i : %s.\n",    \
    __FILE__, __LINE__, cudaGetErrorString( err) );                   \
    exit(EXIT_FAILURE);                                               \
    }                                                                 \
    } while (0)

/**
 * @brief push_boxes device function for thread safe box insertion
 * @param left : leftmost value for box to be inserted
 * @param right : rightmost value for box to be inserted
 * @param top   : upper value for box to be inserted
 * @param bottom  : lower value for box to be inserted
 * @param box     : Boxes struct storing box result
 */
__device__
void push_boxes(int left, int right, int top, int bottom, fukuro::Boxes box) {
  int idx = *box.N;
  if(idx < box.max) {
      atomicAdd(box.N, 1);
      box.x0[idx] = left;
      box.y0[idx] = top;
      box.x1[idx] = right;
      box.y1[idx] = bottom;
    }
}

/**
 * @brief ccl_init : initial function that should be called before ccl kernel, convert 3-channel image struct to 1-channel image suitable for ccl
 * @param segment : 3-channel image
 * @param image   : 1-channel image that will be filled with column value with corresponding segment image, storing left, right, up & bottom value of boxes
 * @note that initial value for left and right is the column and initial value for top & bottom value is the row
 */
__global__
void ccl_init(fukuro::Image segment, fukuro::CCLRegion image)
{
  int i = blockIdx.y * blockDim.y + threadIdx.y;
  int j = blockIdx.x * blockDim.x + threadIdx.x;
  if(j<image.width && i<image.height) {
      int s_idx = segment.step[0]*i + segment.step[1]*j;
      //      int c_idx = image.step[0]*i + image.step[1]*j;
      int c_idx = image.width * i + j;
      if((segment.image[s_idx]    == 0) &&
         (segment.image[s_idx+1]  == 0) &&
         (segment.image[s_idx+2]  == 255)) {
          //          image.image[c_idx] = j;
          image.left[c_idx]   = j;
          image.right[c_idx]  = j;
          image.top[c_idx]    = i;
          image.bottom[c_idx] = i;
        }
      else {
          image.left[c_idx]   = -1;
          image.right[c_idx]  = -1;
          image.top[c_idx]    = -1;
          image.bottom[c_idx] = -1;
        }
    }
}

#define HNEIGHBOR (3)
#define VNEIGHBOR (3)

/**
 * @brief ccl : performs connected component labeling on gpu
 * @param image : a cclregion struct storing latest region information
 * @param flag : pointer to detect if any changes in image
 */
__global__
void ccl(fukuro::CCLRegion image, int *flag)
{
//  int i = blockIdx.y * blockDim.y + threadIdx.y;
//  int j = blockIdx.x * blockDim.x + threadIdx.x;
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  int j = blockIdx.y * blockDim.y + threadIdx.y;
  if(j < image.width && i < image.height) {
//  if(i < image.width && j < image.height) {
      int idx = image.step[0]*i + image.step[1]*j;
      int mflag = 0;
      for(int j_ = -HNEIGHBOR; j_<=HNEIGHBOR; j_++) {
          for(int i_ = -VNEIGHBOR; i_<=VNEIGHBOR; i_++) {
              int ii = i+i_;
              int jj = j+j_;
              if((ii >= 0) &&
                 (jj >= 0) &&
//                 (ii < image.width) &&
//                 (jj < image.height)) {
                 (jj < image.width) &&
                 (ii < image.height)) {
                  int cidx = image.step[0]*ii + image.step[1]*jj;
                  if(image.left[cidx] >= 0) {
                      if((image.left[cidx] < image.left[idx])) {
                          image.left[idx] = image.left[cidx];
                          mflag = 1;
                        }
                      else if((image.right[cidx] > image.right[idx])) {
                          image.right[idx] = image.right[cidx];
                          mflag = 1;
                        }
                      if((image.top[cidx] < image.top[idx])) {
                          image.top[idx] = image.top[cidx];
                          mflag = 1;
                        }
                      else if((image.bottom[cidx] > image.bottom[idx])){
                          image.bottom[idx] = image.bottom[cidx];
                          mflag = 1;
                        }
                    }
                }
            }
        }
      if(mflag>0)
        atomicAdd(flag,1);
    }
}

/**
 * @brief box : parse bounding box information from ccl image
 * @param image : cclregion struct
 * @param boxes : Boxes struct to store region result
 */
__global__
void box(fukuro::CCLRegion image, fukuro::Boxes boxes)
{
  int i = blockIdx.y * blockDim.y + threadIdx.y;
  int j = blockIdx.x * blockDim.x + threadIdx.x;
  if(j < image.width && i < image.height) {
      int idx = image.step[0]*i + image.step[1]*j;
      int x0 = image.left[idx];
      int y0 = image.top[idx];
      int x1 = image.right[idx];
      int y1 = image.bottom[idx];
      if(x0 >= 0) {
          int ii = i-1;
          int jj = j;
          if(ii<0)
            ii = 0;
          int _idx = image.step[0]*ii + image.step[1]*jj;
          if((x0 == j) && (image.left[_idx] < 0)) {
              int w = (x1-x0);
              int h = (y1-y0);
              if((w >= boxes.min_width) && (h >= boxes.min_width))
                if(fabs(float(w-h)/float(h)) <= boxes.w2h_diff)
                  push_boxes(x0, x1, y0, y1, boxes);
            }
        }
    }
}

fukuro::BallSeeker::BallSeeker()
{
  auto max_box = MAX_BOX;
  auto box_bytes = max_box * sizeof(int);
  boxes.max = host_boxes.max = max_box;
  CUDA_CALL(cudaMalloc(&boxes.x0,box_bytes));
  CUDA_CALL(cudaMalloc(&boxes.y0,box_bytes));
  CUDA_CALL(cudaMalloc(&boxes.x1,box_bytes));
  CUDA_CALL(cudaMalloc(&boxes.y1,box_bytes));
  CUDA_CALL(cudaMalloc(&boxes.N,sizeof(int)));
  host_boxes.x0 = (int*)calloc(max_box,sizeof(int));
  host_boxes.x1 = (int*)calloc(max_box,sizeof(int));
  host_boxes.y0 = (int*)calloc(max_box,sizeof(int));
  host_boxes.y1 = (int*)calloc(max_box,sizeof(int));
  host_boxes.N = (int*)calloc(1,sizeof(int));
}

fukuro::BallSeeker::~BallSeeker()
{
  CUDA_CALL(cudaFree(boxes.x0));
  CUDA_CALL(cudaFree(boxes.x1));
  CUDA_CALL(cudaFree(boxes.y0));
  CUDA_CALL(cudaFree(boxes.y1));
  CUDA_CALL(cudaFree(boxes.N));

  free(host_boxes.x0);
  free(host_boxes.x1);
  free(host_boxes.y0);
  free(host_boxes.y1);
  free(host_boxes.N);

  CUDA_CALL(cudaFree(ccl_region.left));
  CUDA_CALL(cudaFree(ccl_region.right));
  CUDA_CALL(cudaFree(ccl_region.top));
  CUDA_CALL(cudaFree(ccl_region.bottom));
}

bool fukuro::BallSeeker::process(cv::Mat &image, std::vector<fukuro::ImageSegment> &segments, fukuro::ColorSegment *color_table)
{
  auto segment = color_table->process(image);
  return this->process(segment,segments);
}

bool fukuro::BallSeeker::process(cv::Mat &image, std::vector<ImageSegment> &segments)
{
  segments = process(image);
  return true;
}

fukuro::ImageSegments fukuro::BallSeeker::process(Image color_segment)
{
  ROS_INFO("start");
  ImageSegments segments;
  CCLRegion ccl_region;

  auto tx = WARP_SIZE;
  auto ty = WARP_SIZE;
  auto bx = std::ceil((double)color_segment.width/tx);
  auto by = std::ceil((double)color_segment.height/ty);

  dim3 grid(bx,by);
  dim3 block(tx,ty);

  if((ccl_region.width != color_segment.width) ||
     (ccl_region.height != color_segment.height)) {
      ccl_region.width = color_segment.width;
      ccl_region.height = color_segment.height;
      ccl_region.step[0] = 1;
      ccl_region.step[1] = ccl_region.width;

      auto n_byte_region = ccl_region.width * ccl_region.height * sizeof(int);

      //      if(ccl_region.left) {
      //          CUDA_CALL(cudaFree(ccl_region.left));
      //          CUDA_CALL(cudaFree(ccl_region.right));
      //          CUDA_CALL(cudaFree(ccl_region.top));
      //          CUDA_CALL(cudaFree(ccl_region.bottom));
      //        }

      CUDA_CALL(cudaMalloc(&ccl_region.left,n_byte_region));
      CUDA_CALL(cudaMalloc(&ccl_region.right,n_byte_region));
      CUDA_CALL(cudaMalloc(&ccl_region.top,n_byte_region));
      CUDA_CALL(cudaMalloc(&ccl_region.bottom,n_byte_region));
    }

  ROS_INFO("launcing ccl_init kernel w/ (%d,%d,%d),(%d,%d,%d)",grid.x,grid.y,grid.z,block.x,block.y,block.z);
  ccl_init<<<grid,block>>>(color_segment,ccl_region);
  CHECK_LAUNCH_ERROR();

  int *host_flag = (int*)calloc(1,sizeof(int));
  int *flag;
  CUDA_CALL(cudaMalloc(&flag,sizeof(int)));
  do {
    *host_flag = 0;
    CUDA_CALL(cudaMemcpy(flag,host_flag,sizeof(int),cudaMemcpyHostToDevice));
    ROS_INFO("launcing ccl kernel w/ (%d,%d,%d),(%d,%d,%d)",grid.x,grid.y,grid.z,block.x,block.y,block.z);
    ccl<<<grid,block>>>(ccl_region,flag);
    CHECK_LAUNCH_ERROR();
    CUDA_CALL(cudaMemcpy(host_flag,flag,sizeof(int),cudaMemcpyDeviceToHost));
    ROS_INFO("flag : %d",*host_flag);
    }
//  while(0);
  while(*host_flag);

  CUDA_CALL(cudaFree(flag));
  free(host_flag);

  //  Boxes boxes;
  //  Boxes host_boxes;

  boxes.min_width = host_boxes.min_width = this->min_size;
  boxes.w2h_diff = host_boxes.w2h_diff = this->max_w2h_tolerance;

  *host_boxes.N = 0;
  CUDA_CALL(cudaMemcpy(boxes.N,host_boxes.N,sizeof(int),cudaMemcpyHostToDevice));

  ROS_INFO("launcing box kernel w/ (%d,%d,%d),(%d,%d,%d)",grid.x,grid.y,grid.z,block.x,block.y,block.z);
  box<<<grid,block>>>(ccl_region,boxes);
  CHECK_LAUNCH_ERROR();

  auto box_bytes = boxes.max * sizeof(int);

  CUDA_CALL(cudaMemcpy(host_boxes.N,boxes.N,sizeof(int),cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaMemcpy(host_boxes.x0,boxes.x0,box_bytes,cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaMemcpy(host_boxes.x1,boxes.x1,box_bytes,cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaMemcpy(host_boxes.y0,boxes.y0,box_bytes,cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaMemcpy(host_boxes.y1,boxes.y1,box_bytes,cudaMemcpyDeviceToHost));

  for(int i=0; i<*host_boxes.N; i++) {
      ImageSegment segment;
      segment.bbox = {
        .x0=host_boxes.x0[i],
        .y0=host_boxes.y0[i],
        .width=host_boxes.x1[i]-host_boxes.x0[i],
        .height=host_boxes.y1[i]-host_boxes.y0[i],
        .x1=host_boxes.x1[i],
        .y1=host_boxes.y1[i]
      };
      segments.push_back(segment);
    }

  ROS_INFO("done, detected %d boxes", segments.size());
  return segments;
}

fukuro::ImageSegments fukuro::BallSeeker::process(cv::Mat &image)
{
  Image color_segment;
  auto n_byte_segment = image.total() * image.elemSize();

  color_segment.width = image.cols;
  color_segment.height = image.rows;
  color_segment.channels = image.channels();
  color_segment.step[0] = image.step[0];
  color_segment.step[1] = image.step[1];

  CUDA_CALL(cudaMalloc(&color_segment.image,n_byte_segment));
  CUDA_CALL(cudaMemcpy(color_segment.image,image.data,n_byte_segment,cudaMemcpyHostToDevice));

  auto ret = process(color_segment);
  CUDA_CALL(cudaFree(color_segment.image));

  return ret;
  /*
  ROS_INFO("start");
  ImageSegments segments;
  Image color_segment;
  CCLRegion ccl_region;

  auto tx = WARP_SIZE;
  auto ty = WARP_SIZE;
  auto bx = std::ceil((double)image.cols/tx);
  auto by = std::ceil((double)image.rows/ty);

  dim3 grid(bx,by);
  dim3 block(tx,ty);

  auto n_byte_segment = image.total() * image.elemSize();
  auto n_byte_region = image.total() * sizeof(int);

  color_segment.width = ccl_region.width = image.cols;
  color_segment.height = ccl_region.height = image.rows;
  color_segment.channels = image.channels();
  color_segment.step[0] = image.step[0];
  color_segment.step[1] = image.step[1];

  ccl_region.step[0] = 1;
  ccl_region.step[1] = ccl_region.width;

  CUDA_CALL(cudaMalloc(&ccl_region.left,n_byte_region));
  CUDA_CALL(cudaMalloc(&ccl_region.right,n_byte_region));
  CUDA_CALL(cudaMalloc(&ccl_region.top,n_byte_region));
  CUDA_CALL(cudaMalloc(&ccl_region.bottom,n_byte_region));
  CUDA_CALL(cudaMalloc(&color_segment.image,n_byte_segment));
  CUDA_CALL(cudaMemcpy(color_segment.image,image.data,n_byte_segment,cudaMemcpyHostToDevice));

  ROS_INFO("launcing ccl_init kernel w/ (%d,%d,%d),(%d,%d,%d)",grid.x,grid.y,grid.z,block.x,block.y,block.z);
  ccl_init<<<grid,block>>>(color_segment,ccl_region);
  CHECK_LAUNCH_ERROR();

  int *host_flag = (int*)calloc(1,sizeof(int));
  int *flag;
  CUDA_CALL(cudaMalloc(&flag,sizeof(int)));
  do {
    *host_flag = 0;
    CUDA_CALL(cudaMemcpy(flag,host_flag,sizeof(int),cudaMemcpyHostToDevice));
    ROS_INFO("launcing ccl kernel w/ (%d,%d,%d),(%d,%d,%d)",grid.x,grid.y,grid.z,block.x,block.y,block.z);
    ccl<<<grid,block>>>(ccl_region,flag);
    CHECK_LAUNCH_ERROR();
    CUDA_CALL(cudaMemcpy(host_flag,flag,sizeof(int),cudaMemcpyDeviceToHost));
    ROS_INFO("flag : %d",*host_flag);
    }
//  while(0);
  while(*host_flag);

  auto max_box = MAX_BOX;
  auto box_bytes = max_box * sizeof(int);
  Boxes boxes;
  Boxes host_boxes;
  boxes.max = host_boxes.max = max_box;
  boxes.min_width = host_boxes.min_width = this->min_size;
  boxes.w2h_diff = host_boxes.w2h_diff = this->max_w2h_tolerance;
  CUDA_CALL(cudaMalloc(&boxes.x0,box_bytes));
  CUDA_CALL(cudaMalloc(&boxes.y0,box_bytes));
  CUDA_CALL(cudaMalloc(&boxes.x1,box_bytes));
  CUDA_CALL(cudaMalloc(&boxes.y1,box_bytes));
  CUDA_CALL(cudaMalloc(&boxes.N,sizeof(int)));
  host_boxes.x0 = (int*)calloc(max_box,sizeof(int));
  host_boxes.x1 = (int*)calloc(max_box,sizeof(int));
  host_boxes.y0 = (int*)calloc(max_box,sizeof(int));
  host_boxes.y1 = (int*)calloc(max_box,sizeof(int));
  host_boxes.N = (int*)calloc(1,sizeof(int));
  CUDA_CALL(cudaMemcpy(boxes.N,host_boxes.N,sizeof(int),cudaMemcpyHostToDevice));

  ROS_INFO("launcing box kernel w/ (%d,%d,%d),(%d,%d,%d)",grid.x,grid.y,grid.z,block.x,block.y,block.z);
  box<<<grid,block>>>(ccl_region,boxes);
  CHECK_LAUNCH_ERROR();
  CUDA_CALL(cudaMemcpy(host_boxes.N,boxes.N,sizeof(int),cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaMemcpy(host_boxes.x0,boxes.x0,box_bytes,cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaMemcpy(host_boxes.x1,boxes.x1,box_bytes,cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaMemcpy(host_boxes.y0,boxes.y0,box_bytes,cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaMemcpy(host_boxes.y1,boxes.y1,box_bytes,cudaMemcpyDeviceToHost));

  for(int i=0; i<*host_boxes.N; i++) {
      ImageSegment segment;
      segment.bbox = {
        .x0=host_boxes.x0[i],
        .y0=host_boxes.y0[i],
        .width=host_boxes.x1[i]-host_boxes.x0[i],
        .height=host_boxes.y1[i]-host_boxes.y0[i],
        .x1=host_boxes.x1[i],
        .y1=host_boxes.y1[i]
      };
      segments.push_back(segment);
    }
  CUDA_CALL(cudaFree(ccl_region.left));
  CUDA_CALL(cudaFree(ccl_region.right));
  CUDA_CALL(cudaFree(ccl_region.top));
  CUDA_CALL(cudaFree(ccl_region.bottom));
  CUDA_CALL(cudaFree(color_segment.image));
  CUDA_CALL(cudaFree(flag));
  CUDA_CALL(cudaFree(boxes.x0));
  CUDA_CALL(cudaFree(boxes.x1));
  CUDA_CALL(cudaFree(boxes.y0));
  CUDA_CALL(cudaFree(boxes.y1));
  CUDA_CALL(cudaFree(boxes.N));

  free(host_flag);
  free(host_boxes.x0);
  free(host_boxes.x1);
  free(host_boxes.y0);
  free(host_boxes.y1);
  free(host_boxes.N);

  ROS_INFO("done, detected %d boxes", segments.size());
  return segments;
  */
}

fukuro::ImageSegments fukuro::BallSeeker::process(cv::Mat &image, fukuro::ColorSegment *color_table)
{
  auto segment = color_table->process(image);
  return this->process(segment);
}

void fukuro::BallSeeker::setMinBox(size_t size)
{
  min_size = std::max(size,(size_t)2);
}

void fukuro::BallSeeker::setMaxW2HDiff(double tolerance)
{
  max_w2h_tolerance = tolerance;
}

void fukuro::BallSeeker::drawSegments(cv::Mat &image, std::vector<fukuro::ImageSegment> &segments, cv::Scalar color, std::vector<std::string> text)
{
  ROS_INFO("start");
  auto it = text.begin();
  auto it_end = text.end();
  std::stringstream ss;
  for(const ImageSegment &s : segments)
  {
    cv::rectangle(image,
                  cv::Rect(s.bbox.x0,
                           s.bbox.y0,
                           s.bbox.width,
                           s.bbox.height),
                  color,2);
    if((it != it_end))
    {
      ss << (*it) << " ";
      cv::putText(image,(*it).c_str(),
                  cv::Point2i(s.bbox.x0,s.bbox.y0),
                  cv::FONT_HERSHEY_PLAIN,0.5,
                  cv::Scalar(0,255,0));
      ++it;
    }
  }
  ROS_INFO("text : %s", ss.str().c_str());
}
