#include "gui/widget3d.h"
#include "gui/scenemodifier.h"
#include <QtWidgets>

#define N_LIGHT_POINT 16
#define ANGLE_INC (360.0f/N_LIGHT_POINT)
#define RADIUS (1000.0f)
#define DEG2RAD (M_PI/180.0f)

Widget3D::Widget3D()
{
    init();
}

QWidget *Widget3D::getContainer()
{
    return container;
}

inline
void Widget3D::init()
{
    view->defaultFrameGraph()->setClearColor(QColor(Qt::black));
    auto screenSize = view->screen()->size();
    container->setMinimumSize(QSize(200,100));
    view->registerAspect(input);

    cameraEntity->setProjectionType(Qt3DRender::QCameraLens::PerspectiveProjection);
    cameraEntity->lens()->setPerspectiveProjection(45.0f, 16.0f/9.0f, 0.1f,1000.0f);
    cameraEntity->setViewCenter(QVector3D(0.0f, 3.5f, 15.0f));
    cameraEntity->setPosition(QVector3D(0.0, 20.0f, -35.0f));
    cameraEntity->setUpVector(QVector3D(0.0f, 1.0f, 0.0f));

    QVector<QVector3D> light_dir;
    light_dir.push_back(QVector3D(-RADIUS,0.0f,0.0f));
    light_dir.push_back(QVector3D(0.0f,-RADIUS,0.0f));
    light_dir.push_back(QVector3D(0.0f,0.0f,RADIUS));
    light_dir.push_back(QVector3D(RADIUS,0.0f,0.0f));
    light_dir.push_back(QVector3D(0.0f,0.0f,-RADIUS));

    for(int i=0; i<light_dir.size(); i++) {
        light_entities.push_back(new Qt3DCore::QEntity(rootEntity));
        //        lights.push_back(new Qt3DRender::QDirectionalLight(light_entities.at(i)));
        lights.push_back(new Qt3DRender::QPointLight(light_entities.at(i)));
        light_transforms.push_back(new Qt3DCore::QTransform(light_entities.at(i)));

        lights.at(i)->setColor(Qt::white);
        lights.at(i)->setIntensity(1.0f/light_dir.size());
        //        lights.at(i)->setWorldDirection(light_dir.at(i));
        light_transforms.at(i)->setTranslation(light_dir.at(i));
        light_entities.at(i)->addComponent(light_transforms.at(i));
        light_entities.at(i)->addComponent(lights.at(i));
      }

#if 0
    auto angle = 0.0f;
    int i;
    for(i=0; i<N_LIGHT_POINT; i++) {
        light_entities.push_back(new Qt3DCore::QEntity(rootEntity));
        lights.push_back(new Qt3DRender::QDirectionalLight(light_entities.at(i)));
        light_transforms.push_back(new Qt3DCore::QTransform(light_entities.at(i)));

        lights.at(i)->setColor(Qt::white);
        lights.at(i)->setIntensity(1.0f/N_LIGHT_POINT);
        lights.at(i)->setWorldDirection(QVector3D(RADIUS*cos(angle*DEG2RAD),0.0f,RADIUS*sin(angle*DEG2RAD)));
        light_entities.at(i)->addComponent(lights.at(i));
//        light_transforms.at(i)->setTranslation(QVector3D(RADIUS*cos(angle*DEG2RAD),RADIUS,RADIUS*sin(angle*DEG2RAD)));
//        light_entities.at(i)->addComponent(light_transforms.at(i));
        angle += ANGLE_INC;
      }
    light_entities.push_back(new Qt3DCore::QEntity(rootEntity));
    lights.push_back(new Qt3DRender::QDirectionalLight(light_entities.at(i)));
    light_transforms.push_back(new Qt3DCore::QTransform(light_entities.at(i)));

    lights.at(i)->setColor(Qt::white);
    lights.at(i)->setIntensity(1.0f/N_LIGHT_POINT);
    lights.at(i)->setWorldDirection(QVector3D(RADIUS*cos(angle*DEG2RAD),0.0f,RADIUS*sin(angle*DEG2RAD)));
    light_entities.at(i)->addComponent(lights.at(i));
//        light_transforms.at(i)->setTranslation(QVector3D(RADIUS*cos(angle*DEG2RAD),RADIUS,RADIUS*sin(angle*DEG2RAD)));
//        light_entities.at(i)->addComponent(light_transforms.at(i));
    angle += ANGLE_INC;

    lights.at(lights.size()-1)->setColor(Qt::white);
    lights.at(lights.size()-1)->setIntensity(1.0f);
    light_entities.at(light_entities.size()-1)->addComponent(lights.at(lights.size()-1));
    light_transforms.at(light_transforms.size()-1)->setTranslation(QVector3D(0.0f,RADIUS,0.0f));
    light_entities.at(light_entities.size()-1)->addComponent(light_transforms.at(light_transforms.size()-1));
#endif

    orbitalCamController->setCamera(cameraEntity);
    orbitalCamController->setLookSpeed(2000.0);
    orbitalCamController->setLinearSpeed(5.0);

//    camController->setCamera(cameraEntity);

    modifier = new SceneModifier(rootEntity);
    view->setRootEntity(rootEntity);

//    orbitalCamController->camera()->rotate(QQuaternion::fromAxisAndAngle(0.0f,1.0f,0.0f,90.0f));
}
