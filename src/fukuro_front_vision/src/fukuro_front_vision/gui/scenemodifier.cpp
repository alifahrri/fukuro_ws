#include "gui/scenemodifier.h"
#include <ros/ros.h>

#define OBJ_SCALE (0.0215)
#define DISTANCE_SCALE (20.0f)

SceneModifier::SceneModifier(Qt3DCore::QEntity *_rootEntity)
{
  robot = new MeshEntity(_rootEntity,
                         QUrl(QStringLiteral("qrc:/mesh/turtle.obj")),
                         Qt::black, OBJ_SCALE,
                         QUrl(QStringLiteral("qrc:/mesh/robot_normal.webp")),
                         QUrl(QStringLiteral("qrc:/mesh/robot.webp")),
                         QUrl(QStringLiteral("qrc:/mesh/robot_specular.webp")));

  robot->transform->setRotationX(-90.0f);
  plane = new PlaneEntity(_rootEntity);

  plane->mesh()->setHeight(250.0f);
  plane->mesh()->setWidth(250.0f);
  plane->mesh()->setMeshResolution(QSize(30,30));

  for(int i=0; i<ball_buffer; i++)
    ball_history.push_back(new BallEntity(_rootEntity));

  //  ball_history.at(0)->transform->setTranslation(QVector3D(0.0,6.0,0.0));
  //  setBallPos(ball_history.at(0),QVector3D(0.5,0.8,0.0));
  //  setBallPos(ball_history.at(0),QVector3D(1.0,0.0,0.0));

  normal = new Qt3DRender::QTextureImage();
  diffuse = new Qt3DRender::QTextureImage();
  specular = new Qt3DRender::QTextureImage();

  normal->setSource((QUrl(QStringLiteral("qrc:/mesh/carpet_normal.webp"))));
  diffuse->setSource((QUrl(QStringLiteral("qrc:/mesh/carpet.webp"))));
  specular->setSource((QUrl(QStringLiteral("qrc:/mesh/carpet_specular.webp"))));
  plane->setTexture(normal,diffuse,specular);
}

SceneModifier::~SceneModifier() {}

void SceneModifier::updateBall(const fukuro_common::FrontVision3dConstPtr &msg)
{
  ROS_INFO("updating, ballcount : %d",msg->ball.size());
  auto& ball = ball_history.at(ball_idx++%ball_buffer);
  if(msg->ball.size())
    this->setBallPos(ball,msg->ball.at(0).x,
                     msg->ball.at(1).y,
                     msg->ball.at(2).z);
}

void SceneModifier::setBallPos(BallEntity *ball, double x, double y, double z)
{
  setBallPos(ball,QVector3D(y,z,x));
}

void SceneModifier::setBallPos(BallEntity *ball, QVector3D pos)
{
  ball->transform->setTranslation(pos*DISTANCE_SCALE);
}
