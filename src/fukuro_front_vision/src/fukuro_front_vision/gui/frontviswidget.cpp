#include "gui/frontviswidget.h"
#include "gui/widget3d.h"
#include <QLayout>

FrontVisWidget::FrontVisWidget(QWidget *parent) :
  QWidget(parent),
  widget3d(new Widget3D)
{
  auto *layout = new QGridLayout(this);
  layout->addWidget(widget3d->getContainer());
  layout->setMargin(0);
  this->setLayout(layout);
}

FrontVisWidget::~FrontVisWidget()
{

}
