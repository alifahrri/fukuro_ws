#include "colorsegment.h"
#include <cuda_runtime.h>
#include <ros/ros.h>

#define MAX_THREADS 32
#define COLORSEGMENT_TEST
// #define BALL_COLOR      0
// #define OBSTACLE_COLOR  1
// #define FIELD_COLOR     2
// #define UNKNOWN_COLOR   3

#define CUDA_CALL(x) { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    exit(EXIT_FAILURE);}}
#define CHECK_LAUNCH_ERROR()                                          \
    do {                                                              \
    /* Check synchronous errors, i.e. pre-launch */                   \
    cudaError_t err = cudaGetLastError();                             \
    if (cudaSuccess != err) {                                         \
    fprintf (stderr, "Cuda error in file '%s' in line %i : %s.\n",    \
    __FILE__, __LINE__, cudaGetErrorString(err) );                    \
    exit(EXIT_FAILURE);                                               \
    }                                                                 \
    /* Check asynchronous errors, i.e. kernel failed (ULF) */         \
    err = cudaThreadSynchronize();                                    \
    if (cudaSuccess != err) {                                         \
    fprintf (stderr, "Cuda error in file '%s' in line %i : %s.\n",    \
    __FILE__, __LINE__, cudaGetErrorString( err) );                   \
    exit(EXIT_FAILURE);                                               \
    }                                                                 \
    } while (0)

__global__
void createSegmentKernel(fukuro::Image in, fukuro::Image out, fukuro::ColorSegment::Table table)
{
//  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
//  i = i*3; //i=i*in.channels;
  unsigned int i = blockIdx.y * blockDim.y + threadIdx.y;
  unsigned int j = blockIdx.x * blockDim.x + threadIdx.x;
  if(j < in.width && i < in.height)
  {
    int size = table.size;
    int idx = in.step[0]*i + in.step[1]*j;
    int k = in.image[idx]*size*size + in.image[idx+1]*size + in.image[idx+2];
    int color = table.yuv[k];
    switch (color) {
      case BALL_COLOR:
        out.image[idx]    = 0;
        out.image[idx+1]  = 0;
        out.image[idx+2]  = 255;
        break;
      case OBSTACLE_COLOR:
        out.image[idx]    = 32;
        out.image[idx+1]  = 32;
        out.image[idx+2]  = 32;
        break;
      case FIELD_COLOR:
        out.image[idx]    = 0;
        out.image[idx+1]  = 255;
        out.image[idx+2]  = 0;
        break;
      default:
        out.image[idx]    = 255;
        out.image[idx+1]  = 255;
        out.image[idx+2]  = 255;
        break;
      }
  }
}

using namespace fukuro;

void ColorSegment::createSegment(uchar *ptr_image, uchar *ptr_result, cv::MatStep out_step, cv::MatStep in_step, ColorTable *table, int size, int rows, int cols)
{
  for(int i=0; i<rows; i++)
    for(int j=0; j<cols; j++)
    {
      int idx = out_step[0]*i + out_step[1]*j;
      int k = in_step[0]*i + in_step[1]*j;
#ifdef COLORSEGMENT_TEST
      auto color = (*table)[ptr_image[k]*size*size + ptr_image[k+1]*size + ptr_image[k+2]];
      cv::Scalar segment_color;
      switch (color) {
      case BALL_COLOR:
        segment_color = cv::Scalar(0,0,255);
        break;
      case OBSTACLE_COLOR:
        segment_color = cv::Scalar(120,120,120);
        break;
      case FIELD_COLOR:
        segment_color = cv::Scalar(0,255,0);
        break;
      case UNKNOWN_COLOR:
        segment_color = cv::Scalar(255,255,255);
        break;
      default:
        break;
      }
      ptr_result[idx]  = segment_color(0);
      ptr_result[idx+1]  = segment_color(1);
      ptr_result[idx+2]  = segment_color(2);
#else
      ptr_result[idx] = table[ptr_image[k]*64*64 + ptr_image[k+1]*64 + ptr_image[k+2]];
#endif
    }
}

ColorSegment::ColorSegment(ColorTable *ctable)
{
  this->table = ctable;
  mode = CALIBRATION;
}

ColorSegment::ColorSegment(std::__cxx11::string table_dir)
{
  this->table = new ColorTable;
  ROS_INFO("load table : %s",table_dir.c_str());

  auto settings_file = table_dir + "/calibration.xml";
  cv::FileStorage ctable_settings(settings_file, cv::FileStorage::READ);
  auto calib_res = ctable_settings["Color_Calib"];
  auto it = calib_res.begin();
  auto it_end = calib_res.end();
  while (it != it_end) {
    (*it)["size"] >> (int&)this->size;
    ++it;
  }
  ctable_settings.release();

  this->table->resize(size*size*size);
  ROS_INFO("table resized : %d",table->size());

  auto table_file = table_dir + "/CTable.dat";
  std::ifstream cs_table_read(table_file.c_str(), std::ios::binary | std::ios::in);
  cs_table_read.read((char*)table->data(),sizeof(ColorTable::value_type)*table->size());
  cs_table_read.close();

  copyTableToDevice(table,size,cuda_table_device);
  ROS_INFO("table loaded");
}

bool ColorSegment::process(const cv::Mat &in, cv::Mat &out)
{
  out = process(in);
  auto ret = true;
  if(out.empty())
    ret = false;
  return ret;
}

cv::Mat ColorSegment::process(const cv::Mat &in)
{
  cv::Mat out;
  out.create(in.rows,in.cols,CV_8UC3);
  uchar *ptr_result = out.data;
  auto out_device = processGPU(in);
  auto n_bytes = in.total() * in.elemSize();
  CUDA_CALL(cudaMemcpy((void*)ptr_result,(void*)out_device.image,n_bytes,cudaMemcpyDeviceToHost));
  CUDA_CALL(cudaFree(out_device.image));
  return out;
}

inline
void ColorSegment::copyTableToDevice(ColorTable *table, size_t size, Table &table_device)
{
  auto n_bytes = size*size*size*sizeof(unsigned char);
  if(size != table_device.size) {
      table_device.size = size;
      if(!table_device.yuv)
          CUDA_CALL(cudaFree(table_device.yuv));
      uchar *yuv = 0;
      CUDA_CALL(cudaMalloc((void**)&yuv,n_bytes));
      table_device.yuv = yuv;
    }
  CUDA_CALL(cudaMemcpy((void*)table_device.yuv,(void*)table->data(),n_bytes,cudaMemcpyHostToDevice));
}

void ColorSegment::copyTableToDevice()
{
  copyTableToDevice(table,size,cuda_table_device);
}

bool ColorSegment::process(const cv::Mat &in, Image &out)
{
  Image out_dev = processGPU(in);
  auto ret = false;
  if(out_dev.image) {
      out = out_dev;
      ret = true;
    }
  return ret;
}

inline
Image ColorSegment::processGPU(const cv::Mat &in)
{
  ROS_INFO("process image");
  Image output;
  if(!table)
    goto DONE;
  if(table->empty())
    goto DONE;
  if(in.cols==0 || in.rows==0)
    goto DONE;
  //#ifdef COLORSEGMENT_TEST
  //  out.create(in.rows,in.cols,CV_8UC3);
  //#else
  //  out.create(in.rows,in.cols,CV_8UC1);
  //#endif

  if(in.channels()<3)
    goto DONE;

  {
    cv::Mat in_yuv;
    cv::cvtColor(in,in_yuv,CV_BGR2YUV);

//    uchar *ptr_result = out.data;
    uchar *ptr_image = in_yuv.data;

    auto n_pixel = in.cols*in.rows;

    auto tx = MAX_THREADS;
    auto ty = MAX_THREADS;
    auto bx = std::ceil((double)in.cols/tx);
    auto by = std::ceil((double)in.rows/ty);

    dim3 grid(bx,by);
    dim3 blocks(tx,ty);

    Image input;
    output.width = input.width = in.cols;
    output.height = input.height = in.rows;
    output.channels = input.channels = in.channels();
    output.step[0] = input.step[0] = in.step[0];
    output.step[1] = input.step[1] = in.step[1];
    //    auto n_bytes = sizeof(uchar)*n_pixel*in.channels();
    auto n_bytes = in.total() * in.elemSize();
    //    auto n_bytes = in.step[0] * in.rows;
    CUDA_CALL(cudaMalloc((void**)&(input.image),n_bytes));
    CUDA_CALL(cudaMalloc((void**)&(output.image),n_bytes));
    CUDA_CALL(cudaMemcpy((void*)input.image,(void*)ptr_image,n_bytes,cudaMemcpyHostToDevice));
    ROS_INFO("launcing (%d,%d,%d)(%d,%d,%d) kernel",grid.x, grid.y, grid.z, blocks.x, blocks.y, blocks.z);
    createSegmentKernel<<<grid,blocks>>>(input,output,cuda_table_device);
    CHECK_LAUNCH_ERROR();
    //    CUDA_CALL(cudaMemcpy((void*)ptr_result,(void*)output.image,n_bytes,cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaFree(input.image));
    //    CUDA_CALL(cudaFree(output.image));

#if 0 //this is cpu version
    createSegment(ptr_image,ptr_result,out.step,in.step,this->table,this->size,in.rows,in.cols);
#endif

  }
  DONE:
  return output;
}
