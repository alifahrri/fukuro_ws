#include "colorsegment.h"
#include <ros/ros.h>
#include <iostream>
#include <fstream>

#define COLORSEGMENT_TEST
// #define BALL_COLOR      0
// #define OBSTACLE_COLOR  1
// #define FIELD_COLOR     2
// #define UNKNOWN_COLOR   3

using namespace fukuro;

void ColorSegment::createSegment(uchar *ptr_image, uchar *ptr_result, cv::MatStep out_step, cv::MatStep in_step, ColorTable *table, int size, int rows, int cols)
{
  for(int i=0; i<rows; i++)
    for(int j=0; j<cols; j++)
    {
      int idx = out_step[0]*i + out_step[1]*j;
      int k = in_step[0]*i + in_step[1]*j;
#ifdef COLORSEGMENT_TEST
      auto color = (*table)[ptr_image[k]*size*size + ptr_image[k+1]*size + ptr_image[k+2]];
      cv::Scalar segment_color;
      switch (color) {
      case BALL_COLOR:
        segment_color = cv::Scalar(0,0,255);
        break;
      case OBSTACLE_COLOR:
        segment_color = cv::Scalar(120,120,120);
        break;
      case FIELD_COLOR:
        segment_color = cv::Scalar(0,255,0);
        break;
      case UNKNOWN_COLOR:
        segment_color = cv::Scalar(255,255,255);
        break;
      default:
        break;
      }
      ptr_result[idx]  = segment_color(0);
      ptr_result[idx+1]  = segment_color(1);
      ptr_result[idx+2]  = segment_color(2);
#else
      ptr_result[idx] = table[ptr_image[k]*64*64 + ptr_image[k+1]*64 + ptr_image[k+2]];
#endif
    }
}

ColorSegment::ColorSegment(ColorTable *ctable)
{
  this->table = ctable;
  mode = CALIBRATION;
}

ColorSegment::ColorSegment(std::__cxx11::string table_dir)
{
  this->table = new ColorTable;
  ROS_INFO("load table : %s",table_dir.c_str());

  auto settings_file = table_dir + "/calibration.xml";
  cv::FileStorage ctable_settings(settings_file, cv::FileStorage::READ);
  auto calib_res = ctable_settings["Color_Calib"];
  auto it = calib_res.begin();
  auto it_end = calib_res.end();
  while (it != it_end) {
    (*it)["size"] >> (int&)this->size;
    ++it;
  }
  ctable_settings.release();

  this->table->resize(size*size*size);
  ROS_INFO("table resized : %d",table->size());

  auto table_file = table_dir + "/CTable.dat";
  std::ifstream cs_table_read(table_file.c_str(), std::ios::binary | std::ios::in);
  cs_table_read.read((char*)table->data(),sizeof(ColorTable::value_type)*table->size());
  cs_table_read.close();

  ROS_INFO("table loaded");
}

bool ColorSegment::process(const cv::Mat &in, cv::Mat &out)
{
  out = process(in);
  auto ret = true;
  if(out.empty())
    ret = false;
  return ret;
}

cv::Mat ColorSegment::process(const cv::Mat &in)
{
  ROS_INFO("process image");
  cv::Mat out;
  if(!table)
    goto DONE;
  if(table->empty())
    goto DONE;
  if(in.cols==0 || in.rows==0)
    goto DONE;
#ifdef COLORSEGMENT_TEST
  out.create(in.rows,in.cols,CV_8UC3);
#else
  out.create(in.rows,in.cols,CV_8UC1);
#endif

  if(in.channels()<3)
    goto DONE;

  {
    cv::Mat in_yuv;
    cv::cvtColor(in,in_yuv,CV_BGR2YUV);

    uchar *ptr_result = out.data;
    uchar *ptr_image = in_yuv.data;

    createSegment(ptr_image,ptr_result,out.step,in.step,this->table,this->size,in.rows,in.cols);
  }
  DONE:
  return out;
}
