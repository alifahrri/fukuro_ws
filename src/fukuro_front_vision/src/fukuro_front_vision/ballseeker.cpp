#include "ballseeker.h"
#include "colorsegment.h"
#include <random>
#include <ros/ros.h>

#define VISION_COLORSEGMENT_BALL 0
#define COLORSEGMENT_TEST

#define ROWSKIP 10
#define COLSKIP 30

//for random seek
#define N_ROW 80
#define N_COL 110

#define IMWIDTH   640
#define IMHEIGHT  480

fukuro::BallSeeker::BallSeeker()
{
  rows.resize(N_ROW);
  cols.resize(N_COL);
  std::mt19937 engine(0);
  std::uniform_int_distribution<int> row_dist(0,IMHEIGHT);
  std::uniform_int_distribution<int> col_dist(0,IMWIDTH);
  for(int i=0; i<N_ROW; i++)
    rows.push_back(row_dist(engine));
  for(int i=0; i<N_COL; i++)
    cols.push_back(col_dist(engine));
}

fukuro::BallSeeker::~BallSeeker()
{

}

bool fukuro::BallSeeker::process(cv::Mat &image, std::vector<fukuro::ImageSegment> &segments, fukuro::ColorSegment *color_table)
{
  auto segment = color_table->process(image);
  return this->process(segment,segments);
}

bool fukuro::BallSeeker::process(cv::Mat &image, std::vector<ImageSegment> &segments)
{
  segments = process(image);
  return true;
}

fukuro::ImageSegments fukuro::BallSeeker::process(cv::Mat &image)
{
  ImageSegments segments;
#ifndef COLORSEGMENT_TEST
  auto target_color = VISION_COLORSEGMENT_BALL;
#else
  auto target_color = cv::Vec3b(0,0,255);
#endif
  cv::Mat checked = cv::Mat::zeros(image.rows,image.cols,CV_8UC1);

  static const cv::Point2i neighbors[8] =
  {
    cv::Point(-1,-1), cv::Point(0,-1), cv::Point(1,-1),
    cv::Point(-1,0),  cv::Point(1,0),
    cv::Point(-1,1),  cv::Point(0,1),  cv::Point(1,1)
  };

  std::deque<cv::Point> growing_queue;

  ROS_INFO("start RegionGrowing");
  //  for(int i=0; i<image.rows; i+=ROWSKIP) {
  //      for(int j=0; j<image.cols; j+=COLSKIP)
  for(int i=0; i<rows.size(); i++) {
      for(int j=0; j<cols.size(); j++)
      {
        ImageSegment current_segment;
        //        auto current_point = cv::Point(j,i);
        auto current_point = cv::Point(cols[j],rows[i]);
        if(image.at<cv::Vec3b>(current_point)==target_color &&
           !checked.at<unsigned char>(current_point))
        {
          checked.at<unsigned char>(current_point) = 1;
          growing_queue.push_back(current_point);
          while(growing_queue.size())
          {
            current_point = growing_queue.front();
            for(size_t k=0; k<8; k++)
            {
              auto neighbor_pt = current_point+neighbors[k];
              neighbor_pt.x = std::max(neighbor_pt.x,0);
              neighbor_pt.x = std::min(neighbor_pt.x,image.cols-1);
              neighbor_pt.y = std::max(neighbor_pt.y,0);
              neighbor_pt.y = std::min(neighbor_pt.y,image.rows-1);
              if(image.at<cv::Vec3b>(current_point)==target_color)
              {
                if(!checked.at<unsigned char>(neighbor_pt))
                {
                  checked.at<unsigned char>(neighbor_pt) = 1;
                  growing_queue.push_back(neighbor_pt);
                }
              }
            }
            current_segment.points.push_back(current_point);

            int &x0 = current_segment.bbox.x0;
            int &y0 = current_segment.bbox.y0;
            int &x1 = current_segment.bbox.x1;
            int &y1 = current_segment.bbox.y1;
            int &w = current_segment.bbox.width;
            int &h = current_segment.bbox.height;

            switch (x0) {
            case -1:
              x0 = current_point.x;
              x1 = x0+1;
              break;
            default:
              x0 = std::min(x0,current_point.x);
              x1 = std::max(x1,current_point.x);
              break;
            }
            switch (y0) {
            case -1:
              y0 = current_point.y;
              y1 = y0+1;
              break;
            default:
              y0 = std::min(y0,current_point.y);
              y1 = std::max(y1,current_point.y);
              break;
            }

            w = x1 - x0;
            h = y1 - y0;

            growing_queue.pop_front();
          }
        }
        if(current_segment.points.size() && current_segment.bbox.width>=min_size && current_segment.bbox.height>=min_size)
        {
          if(fabs(double(current_segment.bbox.width)/double(current_segment.bbox.height)-1.0)<max_w2h_tolerance)
            segments.push_back(current_segment);
        }
      }
    }
  return segments;
}

fukuro::ImageSegments fukuro::BallSeeker::process(cv::Mat &image, fukuro::ColorSegment *color_table)
{
  auto segment = color_table->process(image);
  return this->process(segment);
}

void fukuro::BallSeeker::setMinBox(size_t size)
{
  min_size = std::max(size,(size_t)2);
}

void fukuro::BallSeeker::setMaxW2HDiff(double tolerance)
{
  max_w2h_tolerance = tolerance;
}

void fukuro::BallSeeker::drawSegments(cv::Mat &image, std::vector<fukuro::ImageSegment> &segments, cv::Scalar color, std::vector<std::string> text)
{
  ROS_INFO("start");
  auto it = text.begin();
  auto it_end = text.end();
  std::stringstream ss;
  for(const ImageSegment &s : segments)
  {
    cv::rectangle(image,
                  cv::Rect(s.bbox.x0,
                           s.bbox.y0,
                           s.bbox.width,
                           s.bbox.height),
                  color,2);
    if((it != it_end))
    {
      ss << (*it) << " ";
      cv::putText(image,(*it).c_str(),
                  cv::Point2i(s.bbox.x0,s.bbox.y0),
                  cv::FONT_HERSHEY_PLAIN,0.5,
                  cv::Scalar(0,255,0));
      ++it;
    }
  }
  ROS_INFO("text : %s", ss.str().c_str());
}
