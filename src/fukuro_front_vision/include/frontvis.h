#ifndef FRONTVIS_H
#define FRONTVIS_H

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <image_transport/camera_subscriber.h>
#include "colorsegment.h"
#include "ballseeker.h"
//#include "yolowrapper.h"
#include "fukuro_common/BoundingBoxes.h"

namespace fukuro {

  class FrontVision
  {
  public:
    enum detection_t {
      YOLO,
      COLOR_LUT
    };
  public:
    FrontVision(ros::NodeHandle &node, detection_t detection = COLOR_LUT);
    void detect(const sensor_msgs::ImageConstPtr &msg);
//    void publish(const Darknet::BBoxes &detection);
    void publish(const fukuro::ImageSegments &ball_segments);
    void publish(const fukuro_common::BoundingBoxes &msg);
  private:
//    Darknet *darknet;
    BallSeeker *ball_seeker;
    ColorSegment *color_segment;
    ros::Publisher pub;
    detection_t detection;
  };
}
#endif // FRONTVIS_H
