#ifndef COLORSEGMENT_H
#define COLORSEGMENT_H

#include <opencv2/opencv.hpp>

#define BALL_COLOR      0
#define OBSTACLE_COLOR  1
#define FIELD_COLOR     2
#define UNKNOWN_COLOR   3

namespace fukuro {

  typedef std::vector<uint8_t> ColorTable;

  struct Image
  {
    uchar *image;
    int width;
    int height;
    int channels;
    int step[2];
  };

  class ColorSegment
  {
  public:

    struct Table
    {
      uchar *yuv = 0;
      int size = 0;
    };

    size_t size = 256;

  private:
    ColorTable *table = nullptr;
#ifdef GPU
    Table cuda_table_host;
    Table cuda_table_device;
#endif

  private:
    void createSegment(uchar *ptr_image, uchar *ptr_result, cv::MatStep out_step, cv::MatStep in_step, ColorTable *table, int size, int rows, int cols);
#ifdef GPU
    void copyTableToDevice(ColorTable *table, size_t size, Table &table_device);
  public:
    void copyTableToDevice();
#endif

  public:
    enum Mode {
      CALIBRATION,
      RUNNING
    } mode = RUNNING;

  public:
    ColorSegment(ColorTable *ctable);
    ColorSegment(std::string table_dir);
    bool process(const cv::Mat &in, cv::Mat &out);
    cv::Mat process(const cv::Mat &in);
    bool process(const cv::Mat &in, Image &out);
#ifdef GPU
    /**
     * @brief processGPU : apply color table and return segmented Image in GPU memory
     * @param in : input cv image (on CPU mem)
     * @return : segmented image (in GPU mem) note that you should manually manage the returned mem
     */
    Image processGPU(const cv::Mat &in);
#endif
  };
}

#endif // COLORSEGMENT_H
