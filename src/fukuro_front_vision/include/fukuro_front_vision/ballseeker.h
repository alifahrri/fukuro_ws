#ifndef BALLSEEKER_H
#define BALLSEEKER_H

#include <opencv2/opencv.hpp>

namespace fukuro {

struct Image;
class ColorSegment;

struct Point
{
  int x;
  int y;
};

struct Box
{
  int x0;
  int y0;
  int width;
  int height;
  int x1;
  int y1;
};

struct ImageSegment
{
  std::vector<cv::Point> points;
  Box bbox = Box({-1,-1,-1,-1,-1,-1});
};

/**
 * @brief The CCLRegion struct convinience wrapper for ccl algorithm, only used by ballseeker.cu
 */
struct CCLRegion
{
  int *left = nullptr;
  int *right = nullptr;
  int *top = nullptr;
  int *bottom = nullptr;
  int width = 0;
  int height = 0;
  int step[2];
};

/**
 * @brief The Boxes struct wrapper for GPU version of boxes to fukuro::BBox, only used by ballseeker.cu
 */
struct Boxes
{
  int *x0 = nullptr;
  int *y0 = nullptr;
  int *x1 = nullptr;
  int *y1 = nullptr;
  int *N = nullptr;
  int max;
  int min_width = 10;
  float w2h_diff = 0.1;
};


typedef std::vector<ImageSegment> ImageSegments;

class BallSeeker{

private:
  size_t min_size = 2;
  double max_w2h_tolerance = 0.1;

  //for cuda version
  CCLRegion ccl_region;
  Boxes boxes;
  Boxes host_boxes;

  //for random seek
  std::vector<int> rows;
  std::vector<int> cols;

private:
  ImageSegments process(Image color_segment);

public:
  BallSeeker();
  ~BallSeeker();
  bool process(cv::Mat &image, std::vector<ImageSegment> &segments, ColorSegment* color_table);
  bool process(cv::Mat &image, std::vector<ImageSegment> &segments);
  ImageSegments process(cv::Mat &image);
  ImageSegments process(cv::Mat &image, ColorSegment* color_table);
  void setMinBox(size_t size);
  void setMaxW2HDiff(double tolerance);
  void drawSegments(cv::Mat &image, std::vector<ImageSegment> &segments, cv::Scalar color = cv::Scalar(255,0,0), std::vector<std::string> text = std::vector<std::string>());
};

}
#endif // BALLSEEKER_H
