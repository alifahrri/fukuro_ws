#ifndef IPM_H
#define IPM_H

#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

namespace fukuro {

  class IPM
  {
  public:
    IPM();
    IPM(std::string dir);
    ~IPM(){}
    /**
     * @brief birdsEye, transform image to birds eye perspective
     * @param in, input image to be transformed
     * @return transformed image with size of map_size
     */
    cv::Mat birdsEye(const cv::Mat &in);
    void birdsEye(cv::Mat &in, cv::Mat &out);
    void save(const std::string &dir);
    void load(const std::string &dir);

    /**
     * @brief birdsEye, create lookup table storing corresponding image coordinate, given world coordinate
     * @param in : input size
     * @param out : output size, should not be empty
     * @param vanish_pt : vanishing point
     * @param table : pointer to store ipm table. if nullptr, points to the owned map_table
     * @ref : "GOLD: A Parallel Real-Time Stereo Vision System for Generic Obstacle and Lane Detection, 1998"
     */
    void buildMap(const cv::Size &in, const cv::Size &out, int* table = nullptr);

  public:
    std::vector<int> map_table;
    double fov_h = 75.0f;  // for blue dot, ps3 eye value from wikipedia
    double fov_v = 50.0f;
    int map_width = 500;  //1 cm / px
    int map_height = 500; //1 cm / px
    double cam_height = 60.0f;
    double cam_xpos = 0.0f;
    cv::Point2i vanish_pt = cv::Point2i(320,240);
    cv::Size map_size = cv::Size(500,500);
  };

}

#endif // IPM_H
