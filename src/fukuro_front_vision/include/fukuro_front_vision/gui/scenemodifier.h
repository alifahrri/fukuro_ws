#ifndef SCENEMODIFIER_H
#define SCENEMODIFIER_H

#include <QtCore/QObject>

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>

#include <Qt3DExtras/QTorusMesh>
#include <Qt3DExtras/QConeMesh>
#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QCuboidMesh>
#include <Qt3DExtras/QPlaneMesh>
#include <Qt3DExtras/QSphereMesh>
#include <Qt3DExtras/QPhongMaterial>

#include "meshentity.hpp"
#include "planeentity.hpp"
#include "ballentity.hpp"

#include "fukuro_common/FrontVision3d.h"

class SceneModifier : public QObject
{
  Q_OBJECT

public:
  typedef std::vector<BallEntity*> BallEntities;

public:
  explicit SceneModifier(Qt3DCore::QEntity *_rootEntity);
  ~SceneModifier();

  void updateBall(const fukuro_common::FrontVision3dConstPtr &msg);

private:
  void setBallPos(BallEntity *ball, double x, double y, double z);
  void setBallPos(BallEntity *ball, QVector3D pos);

private:
  const int ball_buffer = 10;
  int ball_idx = 0;
  MeshEntity *robot;
  PlaneEntity *plane;
  BallEntities ball_history;
  Qt3DRender::QTextureImage *normal;
  Qt3DRender::QTextureImage *diffuse;
  Qt3DRender::QTextureImage *specular;
};

#endif // SCENEMODIFIER_H
