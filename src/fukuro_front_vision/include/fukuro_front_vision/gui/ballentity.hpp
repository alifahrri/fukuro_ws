#ifndef BALLENTITY_HPP
#define BALLENTITY_HPP

#include <QEntity>
#include <Qt3DRender>
#include <Qt3DExtras>

class BallEntity : public Qt3DCore::QEntity
{
public:
    BallEntity(Qt3DCore::QEntity *root, double radius = 1.75) {
        mesh = new Qt3DExtras::QSphereMesh();
        mesh->setRadius(radius);

        transform = new Qt3DCore::QTransform();
        transform->setTranslation(QVector3D(0.0f,0.0f,0.0f));

        material = new Qt3DExtras::QPhongMaterial();
        material->setDiffuse(Qt::red);

        this->addComponent(mesh);
        this->addComponent(material);
        this->addComponent(transform);
        this->setParent(root);
    }
public:
    Qt3DCore::QTransform *transform;
private:
    Qt3DExtras::QSphereMesh *mesh;
    Qt3DExtras::QPhongMaterial *material;
};

#endif // BALLENTITY_HPP
