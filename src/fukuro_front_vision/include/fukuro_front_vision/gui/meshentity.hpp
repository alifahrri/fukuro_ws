#ifndef MESHENTITY_HPP
#define MESHENTITY_HPP

#include <Qt3DRender>
#include <Qt3DExtras>

class MeshEntity : public Qt3DCore::QEntity
{
public:
    MeshEntity(Qt3DCore::QEntity *root, QUrl file, QColor color = QColor(Qt::white), double factor=1.0, QUrl normal=QUrl(), QUrl diffuse=QUrl(), QUrl specular=QUrl())
    {
        transform = new Qt3DCore::QTransform();
        transform->setScale(factor);
        mesh = new Qt3DRender::QMesh();
        mesh->setSource(file);
        mesh->setVerticesPerPatch(10);
        material = new Qt3DExtras::QPhongMaterial();
        material->setDiffuse(color);
        //        material->setSpecular(color);
        material->setAmbient(QColor::fromRgbF(0.2f, 0.2f, 0.2f, 1.0f));
        material->setShininess(0.0f);

        normalDiffuseSpecularMap = new Qt3DExtras::QNormalDiffuseSpecularMapMaterial();
//        normalDiffuseSpecularMap->setTextureScale(10.0f);
//        normalDiffuseSpecularMap->setShininess(80.0f);
//        normalDiffuseSpecularMap->setAmbient(QColor::fromRgbF(0.2f, 0.2f, 0.2f, 1.0f));

        normalTexture = new Qt3DRender::QTextureImage();
        if(!normal.isEmpty()) {
            normalTexture->setSource(normal);
            normalDiffuseSpecularMap->normal()->addTextureImage(normalTexture);
          }
        diffuseTexture = new Qt3DRender::QTextureImage();
        if(!diffuse.isEmpty()) {
            diffuseTexture->setSource(diffuse);
            normalDiffuseSpecularMap->diffuse()->addTextureImage(diffuseTexture);
          }
        specularTexture = new Qt3DRender::QTextureImage();
        if(!specular.isEmpty()) {
            specularTexture->setSource(specular);
            normalDiffuseSpecularMap->specular()->addTextureImage(specularTexture);
          }

        qDebug() << "mesh : "   << mesh->source().toString();
        qDebug() << "normal : " << normalTexture->source().toString() << normalTexture->status();
        qDebug() << "diffuse : "  << diffuseTexture->source().toString() << diffuseTexture->status();
        qDebug() << "specular : " << specularTexture->source().toString() << specularTexture->status();

        this->addComponent(mesh);
        this->addComponent(material);
        this->addComponent(transform);
//        this->addComponent(normalDiffuseSpecularMap);
        this->setParent(root);
    }

public:
    Qt3DCore::QTransform *transform;
private:
    Qt3DRender::QMesh *mesh;
    Qt3DExtras::QPhongMaterial *material;
    Qt3DRender::QTextureImage *normalTexture;
    Qt3DRender::QTextureImage *diffuseTexture;
    Qt3DRender::QTextureImage *specularTexture;
    Qt3DExtras::QNormalDiffuseSpecularMapMaterial *normalDiffuseSpecularMap;
};

#endif // MESHENTITY_HPP

