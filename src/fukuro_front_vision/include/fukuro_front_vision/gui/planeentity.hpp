#ifndef PLANEENTITY_HPP
#define PLANEENTITY_HPP

#include <Qt3DRender>
#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>
#include <Qt3DExtras>

class PlaneEntity : public Qt3DCore::QEntity
{
public:
    PlaneEntity(Qt3DCore::QNode *parent = 0)
      : Qt3DCore::QEntity(new Qt3DCore::QEntity(parent))
      , m_mesh(new Qt3DExtras::QPlaneMesh())
      , m_transform(new Qt3DCore::QTransform())
    {
      addComponent(m_mesh);
      addComponent(m_transform);

      normalDiffuseSpecularMapMaterial = new Qt3DExtras::QNormalDiffuseSpecularMapMaterial();
      normalDiffuseSpecularMapMaterial->setTextureScale(10.0f);
      normalDiffuseSpecularMapMaterial->setShininess(1.0f);
      normalDiffuseSpecularMapMaterial->setAmbient(QColor::fromRgbF(0.2f, 0.2f, 0.2f, 1.0f));
    }

    ~PlaneEntity() {}

    Qt3DExtras::QPlaneMesh *mesh() const
    {
      return m_mesh;
    }

    void setTexture(Qt3DRender::QTextureImage *normal, Qt3DRender::QTextureImage *diffuse, Qt3DRender::QTextureImage *specular)
    {
      normalDiffuseSpecularMapMaterial->normal()->addTextureImage(normal);
      normalDiffuseSpecularMapMaterial->diffuse()->addTextureImage(diffuse);
      normalDiffuseSpecularMapMaterial->specular()->addTextureImage(specular);
      addComponent(normalDiffuseSpecularMapMaterial);
    }

public:
    Qt3DExtras::QNormalDiffuseSpecularMapMaterial *normalDiffuseSpecularMapMaterial;

private:
    Qt3DExtras::QPlaneMesh *m_mesh;
    Qt3DCore::QTransform *m_transform;
};

#endif // PLANEENTITY_HPP
