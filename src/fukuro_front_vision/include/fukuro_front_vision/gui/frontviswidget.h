#ifndef FRONTVISWIDGET_H
#define FRONTVISWIDGET_H

#include <QWidget>

class Widget3D;

class FrontVisWidget : public QWidget
{
  Q_OBJECT

public:
  explicit FrontVisWidget(QWidget *parent = 0);
  ~FrontVisWidget();

public:
  Widget3D *widget3d;
};

#endif // FRONTVISWIDGET_H
