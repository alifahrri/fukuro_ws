#!/usr/bin/env python
import os
import sys
import rospy
import numpy as np
import regressor as reg
import std_msgs.msg as std_msg
import geometry_msgs.msg as geo_msg
import sensor_msgs.msg as sensor_msg
import fukuro_common.msg as fukuro_msg

depth_regression = None

IMG_WIDTH = 640
IMG_HEIGHT = 480

def run_training() :
    depth_regression = reg.Regressor('train','data/train.txt')
    depth_regression.train()

def run_prediction() :
    depth_regression = reg.Regressor('predict')
    depth_regression.predict([[1.0,1.0,1.0,1.0]])

class DepthRegressionNode(object) :
    def __init__(self, *args, **kwargs):
        self.mode = kwargs['mode']
        self.robot_name = os.environ['FUKURO']
        self.model_dir = '/home/'+os.environ['USER']+'/fukuro_ws/src/fukuro_front_vision/scripts/depth_regression/models/'+self.robot_name
        self.train_file = rospy.get_param('train_file','data/train.txt')
        self.regressor = reg.Regressor(self.mode, self.train_file, model_dir=self.model_dir)
        self.pub = rospy.Publisher('/front_vision/3d',fukuro_msg.FrontVision3d,queue_size=31)
        self.sub = rospy.Subscriber('/front_vision', fukuro_msg.BoundingBoxes, self.predict)
        
    def loop(self) :
        rospy.spin()

    def train(self) :
        self.regressor.train()

    def publish(self, msg) :
        self.pub.publish(msg)

    def predict(self, bbox) :
        msg_3d = fukuro_msg.FrontVision3d()
        for b in bbox.bboxes :
            pos = geo_msg.Point()
            input = np.array([[float(b.x)/IMG_WIDTH, float(b.y)/IMG_HEIGHT, float(b.w)/IMG_WIDTH, float(b.h)/IMG_HEIGHT]])
            prediction = self.regressor.predict(input)
            # rospy.logerr('prediction[0][0] : %s'%prediction[0][0])
            pos.x = prediction[0][0][0]
            pos.y = prediction[0][0][1]
            pos.z = prediction[0][0][2]
            msg_3d.ball.append(pos)
        self.publish(msg_3d)

if __name__ == '__main__' :
    rospy.init_node('depth_regression_node',argv=sys.argv)
    mode = rospy.get_param('mode','predict')
    if mode == 'train' :
        rospy.loginfo('training mode')
        regressor_node = DepthRegressionNode(mode='train')
        regressor_node.train()
    else :
        rospy.loginfo('prediction mode')
        regressor_node = DepthRegressionNode(mode='predict')
        rospy.loginfo('ready')
        regressor_node.loop()
    rospy.loginfo('done')
    # run_training()

