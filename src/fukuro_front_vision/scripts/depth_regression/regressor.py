import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import dataset as ds
import sys
import os

BATCH = 1000
EPOCH = 300000
BUFFER = 2000
MIN_LOSS = 0.01
LEARNING_RATE = 0.05

class Regressor(object) :
    def __init__(self, mode, *args, **kwargs) :
        self.layer = []
        self.mode = mode
        self.ok = True

        if self.mode == 'nop' :
            return
        if 'epoch' in kwargs :
            global EPOCH
            EPOCH = kwargs['epoch']
        if 'batch_size' in kwargs :
            global BATCH
            BATCH = kwargs['batch_size']
        self.model_dir = './model/params'
        if 'model_dir' in kwargs :
            self.model_dir = kwargs['model_dir']

        if mode == 'train' :
            if len(args) == 0 :
                print 'training mode needs dataset! exiting'
                sys.exit(-1)
            print args
            if 'test_file' in kwargs:
                self.dataset = ds.Dataset(args[0], test_file=kwargs['test_file'])
            else :
                self.dataset = ds.Dataset(args[0])

        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.2
        self.init_regression_model()
        self.session = tf.Session(config=config)
        self.session.run([tf.global_variables_initializer()])
        self.saver = tf.train.Saver()
                
        if self.mode == 'train' :
            self.session.run([self.iterator.initializer], feed_dict={
                            self.tfx : self.dataset.train_input,
                            self.tfy : self.dataset.train_output
                        })
            if os.path.isfile(self.model_dir+'.index') :
                self.saver.restore(self.session, self.model_dir)
        else :
            self.saver.restore(self.session,self.model_dir)

        self.step = 0

    def trainloop(self) :
        if self.mode == 'train' :
            print 'now training with epoch : %s and batch_size : %s'%(EPOCH, BATCH)
            test_loss = 10.0
            step = 0
            done = False
            while (test_loss > MIN_LOSS) & self.ok & (not done) :
                step, loss, test_loss, prediction, done = self.train()
                if step%5 == 0 :
                    print 'step %i :'%step, '| train loss:', loss, '| test loss', test_loss, '| prediction', prediction
            self.save()
    
    def train(self) :
        try :
            prediction = None
            done = False
            test_loss = None
            _, loss = self.session.run([self.train_op, self.loss])
            if self.step % 5 == 0 :
                if self.test_tfx != None :
                    test_loss, prediction = self.session.run([self.loss, self.output], {
                        self.bx : self.dataset.test_input,
                        self.by : self.dataset.test_output
                    })
                else :
                    test_loss = loss
                if (test_loss < MIN_LOSS) & (loss < MIN_LOSS) :
                    done = True
        except tf.errors.OutOfRangeError :
            done = True
            print 'reached last epoch'
        self.step += 1
        return self.step-1, loss, test_loss, prediction, done
        
    def save(self) :
        print 'training done! now saving in %s'%self.model_dir
        self.saver.save(self.session,self.model_dir,write_meta_graph=True)
    
    def predict(self, x) :
        prediction = self.session.run([self.output],{
            self.tfx : x
        })
        # print 'x(%s):'%x, 'prediction:(%s)'%prediction
        return prediction

    def init_regression_model(self) :
        if self.mode == 'train' :
            self.tfx = tf.placeholder(tf.float32, self.dataset.train_input.shape, name="x")
            self.tfy = tf.placeholder(tf.float32, self.dataset.train_output.shape, name="y")
            self.test_tfx = None
            self.test_tfy = None
            if self.dataset.test_input != None :
                self.test_tfx = tf.placeholder(tf.float32, self.dataset.test_input.shape)
                self.test_tfy = tf.placeholder(tf.float32, self.dataset.test_output.shape)
            self.tf_dataset = tf.data.Dataset.from_tensor_slices((self.tfx, self.tfy))
            self.tf_dataset = self.tf_dataset.shuffle(buffer_size=BUFFER)
            self.tf_dataset = self.tf_dataset.batch(BATCH)
            self.tf_dataset = self.tf_dataset.repeat(EPOCH)
            self.iterator = self.tf_dataset.make_initializable_iterator()  # later we have to initialize this one
            self.bx, self.by = self.iterator.get_next()
        elif self.mode == 'predict' :
            x = np.array([[0.0, 0.0, 0.0, 0.0]])
            y = np.array([[0.0, 0.0, 0.0]])
            self.tfx = tf.placeholder(tf.float32, x.shape, name="x")
            self.tfy = tf.placeholder(tf.float32, y.shape, name="y")

        layers = [24, 24, 24, 24, 24, 24, 24, 24]
        for i in range(len(layers)) :
            if i == 0:
                if self.mode == 'predict' :
                    self.layer.append(tf.layers.dense(self.tfx, layers[i], tf.nn.relu, name="dense%s"%i))
                else :
                    self.layer.append(tf.layers.dense(self.bx, layers[i], tf.nn.relu, name="dense%s"%i))
            else :
                self.layer.append(tf.layers.dense(self.layer[i-1], layers[i], tf.nn.relu, name="dense%s"%i))
        self.output = tf.layers.dense(self.layer[-1], 3, name="reg_output")

        if self.mode == 'train' :
            self.loss = tf.losses.mean_squared_error(self.by, self.output)
            self.optimizer = tf.train.GradientDescentOptimizer(learning_rate = LEARNING_RATE, name="optimizer")
            self.train_op = self.optimizer.minimize(self.loss, name="train_op")