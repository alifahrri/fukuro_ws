import numpy as np
from PyQt5 import QtCore, QtWidgets, QtGui

class Parser(object) :
    def __init__(self) :
        pass
    
    def parse(self, path, separator=None) :
        with open(path, 'r') as f:
            first = f.readline()
            if separator is None :
                sep = [' ', ',', ', ', ';', '; ']
                for s in sep :
                    if s in first :
                        separator = s
            val = first.split(separator)
            xpx, ypx = np.array(float(val[4])), np.array(float(val[5]))
            w, h = np.array(float(val[6])), np.array(float(val[7]))
            x, y, z = np.array(float(val[8])), np.array(float(val[9])), np.array(float(val[10]))
            print first
            for line in f.readlines() :
                print line
                val = line.split(separator)
                xpx, ypx = np.append(xpx, float(val[4])), np.append(ypx, float(val[5]))
                w, h = np.append(w, float(val[6])), np.append(h, float(val[7]))
                x, y, z = np.append(x, float(val[8])), np.append(y, float(val[9])), np.append(z, float(val[10]))
            return np.transpose(np.vstack((xpx, ypx, w, h))), np.transpose(np.vstack((x, y, z)))

class Dataset(object) :
    def __init__(self, path, *args, **kwargs) :
        print 'dataset init : %s'%path
        self.parser = Parser()
        self.train_input, self.train_output = self.parser.parse(path)
        self.test_input = None
        self.test_output  = None
        if 'test_file' in kwargs :
            self.test_input, self.test_output = self.parser.parse(kwargs['test_file'])
        print 'train input :'
        print self.train_input
        print 'train output :'
        print self.train_output