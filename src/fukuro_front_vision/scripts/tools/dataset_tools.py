from PyQt5 import QtCore, QtWidgets, QtGui
import numpy as np
import image_item
import datetime
import form
import cv2
import sys
import os

BALL = '1 0'
NO_BALL = '0 1'

class DatasetTools() :
    def __init__(self, *args, **kwargs):
        self.ui = form.Ui_Form()
        self.widget = QtWidgets.QWidget()
        self.ui.setupUi(self.widget)
        self.ball_image = image_item.BallClassifierImage()
        self.scene = QtWidgets.QGraphicsScene(self.ball_image.boundingRect(),self.widget)
        self.ui.graphicsView.setScene(self.scene)
        self.ui.graphicsView.scale(2.25,2.25)
        self.scene.addItem(self.ball_image)
        self.ui.load_btn.clicked.connect(self.loadDirectory)
        self.ui.next_btn.clicked.connect(self.loadNext)
        self.ui.prev_btn.clicked.connect(self.loadPrev)
        self.ui.save_btn.clicked.connect(self.saveDataset)
        self.ui.save_as_btn.clicked.connect(self.replaceDataset)
        self.ui.inc_bright_btn.clicked.connect(self.increaseBrightness)
        self.ui.dec_bright_btn.clicked.connect(self.decreaseBrightness)
        self.ui.horizontal_btn.clicked.connect(self.applyHorizontalMotionBlur)
        self.ui.vertical_btn.clicked.connect(self.applyVerticalMotionBlur)
        self.ui.inc_contrast_btn.clicked.connect(self.increaseContrast)
        self.ui.dec_contrast_btn.clicked.connect(self.decreaseContrast)
        self.ui.salt_btn.clicked.connect(self.saltPepper)
        self.files = []
        self.labels = []
        self.ui.splitter.setSizes([720,200])

    def loadDirectory(self) :
        self.path = QtWidgets.QFileDialog.getExistingDirectory()
		# self.files = os.listdir(self.path)
        print self.path
        files = os.listdir(self.path)
        del self.files[:]
        del self.labels[:]
        for f in files :
            file, ext = os.path.splitext(f)
            if ext == '.jpg' or ext == '.jpeg' :
                self.files.append(f)
                self.labels.append(file+'.txt')
        self.current_idx = 0
        self.ui.lineEdit.setText(self.path)
        img_files = self.files[self.current_idx:self.current_idx+image_item.MAX_CLASSIFIER_DATA]
        labels = self.labels[self.current_idx:self.current_idx+image_item.MAX_CLASSIFIER_DATA]
        # print(img_files, labels)
        self.setImage(img_files, labels)
        print("read images : %s" % self.path)
        print("files: %s" % len(self.files))
        print self.files

    def setImage(self, files, labels) :
        images = []
        balls = []
        for f in files :
            img = cv2.imread(self.path+'/'+f)
            images.append(img)
        for f in labels :
            filepath = self.path+'/'+f
            # print filepath
            with open(filepath, 'r') as file :
                ball = False
                if file.read() == '1 0' :
                    ball = True
                balls.append(ball)
        self.cv_images = images
        self.ball_labels = balls
        self.ball_image.setImage(images,balls)

    def loadNext(self) :
        self.current_idx = self.current_idx + image_item.MAX_CLASSIFIER_DATA
        max_file = len(self.files)
        start = self.current_idx
        end = min(self.current_idx+image_item.MAX_CLASSIFIER_DATA, max_file)
        img_files = self.files[start:end]
        labels = self.labels[start:end]
        # print(img_files, labels)
        self.setImage(img_files, labels)
        self.scene.update()
    
    def loadPrev(self) :
        self.current_idx = self.current_idx - image_item.MAX_CLASSIFIER_DATA
        max_file = len(self.files)
        start = max(self.current_idx,0)
        end = min(self.current_idx+image_item.MAX_CLASSIFIER_DATA, max_file)
        img_files = self.files[start:end]
        labels = self.labels[start:end]
        # print(img_files, labels)
        self.setImage(img_files, labels)
        self.scene.update()

    def increaseBrightness(self) :
        value = 5
        for i in range(len(self.cv_images)) :
            image = self.cv_images[i]
            hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
            h, s, v = cv2.split(hsv)
            lim = 255 - value
            v[v > lim] = 255
            v[v <= lim] += value
            final_hsv = cv2.merge((h, s, v))
            self.cv_images[i] = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
        self.ball_image.setImage(self.cv_images,self.ball_labels)
        self.scene.update()

    def decreaseBrightness(self) :
        value = 5
        for i in range(len(self.cv_images)) :
            image = self.cv_images[i]
            hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
            h, s, v = cv2.split(hsv)
            lim = 255 - value
            v[v > lim] = 255
            v[v <= lim] -= value
            final_hsv = cv2.merge((h, s, v))
            self.cv_images[i] = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
        self.ball_image.setImage(self.cv_images,self.ball_labels)
        self.scene.update()

    def saltPepper(self) :
        s_vs_p = 0.5
        amount = 0.04
        for i in range(len(self.cv_images)) :
            image = self.cv_images[i]
            row,col,ch = image.shape
            out = np.copy(image)
            # Salt mode
            num_salt = np.ceil(amount * image.size * s_vs_p)
            coords = [np.random.randint(0, v - 1, int(num_salt))
                    for v in image.shape]
            out[coords] = 1

            # Pepper mode
            num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
            coords = [np.random.randint(0, v - 1, int(num_pepper))
                    for v in image.shape]
            out[coords] = 0
            self.cv_images[i] = out
        self.ball_image.setImage(self.cv_images,self.ball_labels)
        self.scene.update()

    def increaseContrast(self) :
        b, c = 0, 5
        for i in range(len(self.cv_images)) :
            image = self.cv_images[i]
            image = cv2.addWeighted(image, 1. + c/127., image, 0, b-c)
            self.cv_images[i] = image
        self.ball_image.setImage(self.cv_images,self.ball_labels)
        self.scene.update()

    def decreaseContrast(self) :
        b, c = 0, -5
        for i in range(len(self.cv_images)) :
            image = self.cv_images[i]
            image = cv2.addWeighted(image, 1. + c/127., image, 0, b-c)
            self.cv_images[i] = image
        self.ball_image.setImage(self.cv_images,self.ball_labels)
        self.scene.update()
    
    def applyVerticalMotionBlur(self) :
        size = 3
        kernel_motion_blur = np.zeros((size, size))
        kernel_motion_blur[ : , int((size-1)/2)] = np.ones(size)
        kernel_motion_blur = kernel_motion_blur / size
        for i in range(len(self.cv_images)) :
            img = self.cv_images[i]
            self.cv_images[i] = cv2.filter2D(img, -1, kernel_motion_blur)
        self.ball_image.setImage(self.cv_images,self.ball_labels)
        self.scene.update()
    
    def applyHorizontalMotionBlur(self) :
        size = 3
        kernel_motion_blur = np.zeros((size, size))
        kernel_motion_blur[int((size-1)/2), :] = np.ones(size)
        kernel_motion_blur = kernel_motion_blur / size
        for i in range(len(self.cv_images)) :
            img = self.cv_images[i]
            self.cv_images[i] = cv2.filter2D(img, -1, kernel_motion_blur)
        self.ball_image.setImage(self.cv_images,self.ball_labels)
        self.scene.update()

    def replaceDataset(self) :
        max_file = len(self.files)
        start = max(self.current_idx,0)
        end = min(self.current_idx+image_item.MAX_CLASSIFIER_DATA, max_file)
        img_files = self.files[start:end]
        labels = self.labels[start:end]
        # for i in range(len(img_files)) :
        #     filename = self.path+'/'+img_files[i]
        #     img = img_files[i]
        #     cv2.imwrite(filename, img)
        balls = self.ball_image.labels
        for i in range(len(labels)) :
            filename = self.path+'/'+labels[i]
            ball = balls[i]
            with open(filename,'w+') as f :
                if ball :
                    f.write(BALL)
                else :
                    f.write(NO_BALL)

    def saveDataset(self) :
        now = datetime.datetime.now()
        i = 0
        for img in self.cv_images :
            date = '%s%s%s%s%s-%s'%(now.month,now.day,now.hour,now.minute,now.second,i)
            filename = '%s/%s-%s'%(self.path,'image',date)
            cv2.imwrite(filename+'.jpg', img)
            ball = self.ball_labels[i]
            with open(filename+'.txt','w+') as f :
                if ball :
                    f.write(BALL)
                else :
                    f.write(NO_BALL)
            i = i+1

if __name__ == '__main__' :
    app = QtWidgets.QApplication(sys.argv)
    tools = DatasetTools()
    tools.widget.showMaximized()
    sys.exit(app.exec_())
