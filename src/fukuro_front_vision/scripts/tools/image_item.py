from PyQt5 import QtCore, QtWidgets, QtGui

CHANNELS                = 3
CLASSIFIER_IM_WIDTH     = 28
CLASSIFIER_IM_HEIGHT    = 28
N_BYTES                 = CLASSIFIER_IM_WIDTH * CHANNELS
N_CAPTCHA_ROW           = 10
N_CAPTCHA_COL           = 14
MAX_CLASSIFIER_DATA     = N_CAPTCHA_ROW * N_CAPTCHA_COL

class BallClassifierImage(QtWidgets.QGraphicsItem) :
    def __init__(self, parent=None) :
        QtWidgets.QGraphicsItem.__init__(self, parent)
        self.imgs = []
        self.labels = []
        self.check_boxes = []
        w = N_CAPTCHA_COL * CLASSIFIER_IM_WIDTH
        h = N_CAPTCHA_ROW * CLASSIFIER_IM_HEIGHT
        topleft = QtCore.QPointF(-w/2,-h/2)
        size = QtCore.QSizeF(CLASSIFIER_IM_WIDTH,CLASSIFIER_IM_HEIGHT)
        for _ in range(MAX_CLASSIFIER_DATA) :
            self.imgs.append(QtGui.QImage())
            self.labels.append(False)
        for i in range(N_CAPTCHA_ROW) :
            for j in range(N_CAPTCHA_COL) :
                dy = i*CLASSIFIER_IM_HEIGHT
                dx = j*CLASSIFIER_IM_WIDTH
                self.check_boxes.append(QtCore.QRectF(topleft,size).translated(dx,dy))
                self.labels.append(False)

    def setImage(self, images, labels) :
        for i in range(MAX_CLASSIFIER_DATA) :
            if i < len(images) :
                self.imgs[i] = QtGui.QImage(images[i],CLASSIFIER_IM_WIDTH,CLASSIFIER_IM_HEIGHT,N_BYTES,QtGui.QImage.Format_RGB888).rgbSwapped()
                self.labels[i] = labels[i]
            else :
                self.imgs[i] = QtGui.QImage()
                self.labels[i] = False

    def paint(self, painter, option, widget) :
        font = painter.font()
        font.setPointSizeF(5.0)
        font.setBold(True)
        painter.setFont(font)
        for i in range(N_CAPTCHA_ROW) :
            for j in range(N_CAPTCHA_COL) :
                idx = i*N_CAPTCHA_COL+j
                im = self.imgs[idx]
                if not im.isNull() :
                    px = QtGui.QPixmap.fromImage(im)
                    painter.drawPixmap(self.check_boxes[idx].topLeft(),px)
                    painter.setPen(QtGui.QColor(0,0,0))
                    painter.drawRect(self.check_boxes[idx])
                    if self.labels[idx] :
                        painter.setPen(QtGui.QColor(0,255,0))
                        painter.drawText(self.check_boxes[idx],"ball")
        painter.setPen(QtGui.QColor(255,0,0))
        painter.drawRect(self.boundingRect())

    def mousePressEvent(self, event) :
        for i in range(MAX_CLASSIFIER_DATA) :
            if self.check_boxes[i].contains(event.pos()) :
                self.labels[i] = not self.labels[i]
                break
        if not (self.scene() is None) :
            self.scene().update()

    def boundingRect(self) :
        w = N_CAPTCHA_COL * CLASSIFIER_IM_WIDTH
        h = N_CAPTCHA_ROW * CLASSIFIER_IM_HEIGHT
        return QtCore.QRectF(-w/2,-h/2,w,h)