#!/usr/bin/env python

import os
import sys
import cv2
import rospy
import cv_bridge
import numpy as np
import tensorflow as tf
import depth_regression.regressor as reg
import ball_classification.classifier as cls
import std_msgs.msg as std_msg
import geometry_msgs.msg as geo_msg
import sensor_msgs.msg as sensor_msg
import fukuro_common.msg as fukuro_msg
 
depth_regression = None

IMG_WIDTH = 640
IMG_HEIGHT = 480

CLS_IMG_WIDTH = 28
CLS_IMG_HEIGHT = 28

frontvis3d = None

def finished() :
    if frontvis3d is None :
        return
    frontvis3d.regressor.ok = False
    frontvis3d.classifier.ok = False

class FrontVis3DNode(object) :
    def __init__(self, *args, **kwargs):
        self.mode = 'train_all'

        if 'mode' in kwargs :
            self.mode = kwargs['mode']

        if self.mode == 'train_all' :
            self.cls_mode = 'train'
            self.reg_mode = 'train'
        elif self.mode == 'train_classifier' :
            self.cls_mode = 'train'
            self.reg_mode = 'nop'
        elif self.mode == 'train_regressor' :
            self.reg_mode = 'train'
            self.cls_mode = 'nop'
        else :
            self.cls_mode = 'predict'
            self.reg_mode = 'predict'

        # config = tf.ConfigProto()
        # config.gpu_options.per_process_gpu_memory_fraction = 0.4
        # self.session = tf.Session(config=config)
        # self.session.run([tf.global_variables_initializer()])

        self.img = None
        depth_train_file = rospy.get_param('train_file','data/train.txt')
        classifier_train_dir = rospy.get_param('classifier_train_dir',os.environ['HOME']+'/ball_dataset/train')
        classifier_test_dir = rospy.get_param('classifier_test_dir',os.environ['HOME']+'/ball_dataset/test')

        self.cls_resize = (CLS_IMG_WIDTH, CLS_IMG_HEIGHT)
        self.robot_name = os.environ['FUKURO']
        self.model_dir = os.environ['HOME']+'/fukuro_ws/src/fukuro_front_vision/models/'+self.robot_name

        self.classifier = cls.Classifier(self.cls_mode, train_dir=classifier_train_dir, test_dir=classifier_test_dir, model_dir=self.model_dir+'/classifier/classifier')
        self.regressor = reg.Regressor(self.reg_mode, depth_train_file, model_dir=self.model_dir+'/regressor/regressor')

        self.bridge = cv_bridge.CvBridge()
        self.imsub = rospy.Subscriber('/front_camera/image_raw', sensor_msg.Image, self.updateImage)
        self.pub = rospy.Publisher('/front_vision/3d',fukuro_msg.FrontVision3d,queue_size=31)
        self.sub = rospy.Subscriber('/front_vision', fukuro_msg.BoundingBoxes, self.predict)

    def updateImage(self, img) :
        rospy.loginfo('image callback')
        self.img = self.bridge.imgmsg_to_cv2(img,'bgr8')

    def loop(self) :
        rospy.spin()

    def train_all(self) :
        self.train_regressor()
        self.train_classifier()
        
    def train_regressor(self) :
        rospy.logwarn('Regression Training : ')
        done = False
        while (not rospy.is_shutdown()) & (not done) :
            step, train_loss, test_loss, prediction, done = self.regressor.train()
            if step % 5 == 0 :
                rospy.loginfo('step:%s, train_loss:%s, test_loss:%s, prediction:%s'%(step, train_loss, test_loss, prediction))
        self.regressor.save()
        rospy.logwarn('Regression Training : DONE')
    
    def train_classifier(self) :
        rospy.logwarn('Classifier Training : ')
        done = False
        while (not rospy.is_shutdown()) & (not done) :
            step, train_loss, test_loss, accuracy, done = self.classifier.train()
            if step % 10 == 0 :
                rospy.loginfo('step:%s, train loss:%s, test loss:%s'%(step,train_loss, test_loss))
        self.classifier.save()
        rospy.logwarn('Classifier Training : DONE')

    def publish(self, msg) :
        self.pub.publish(msg)

    def predict(self, bbox) :
        if self.img is None :
            return
        msg_3d = fukuro_msg.FrontVision3d()
        for b in bbox.bboxes :
            x, y, w, h = b.x, b.y, b.w, b.h
            input = np.array([[float(x)/IMG_WIDTH, float(y)/IMG_HEIGHT, float(w)/IMG_WIDTH, float(h)/IMG_HEIGHT]])
            cropped = self.img[y:y+h, x:x+w]
            resized = cv2.resize(cropped, self.cls_resize)
            is_ball = self.classifier.predict(resized)
            if is_ball[0][0] :
                pos = geo_msg.Point()
                prediction = self.regressor.predict(input)
                pos.x = prediction[0][0][0]
                pos.y = prediction[0][0][1]
                pos.z = prediction[0][0][2]
                msg_3d.ball.append(pos)
                # rospy.logerr('prediction[0][0] : %s'%prediction[0][0])
        self.publish(msg_3d)

if __name__ == '__main__' :
    rospy.init_node('front_vision3D_node', argv=sys.argv)
    mode = rospy.get_param('mode','predict')
    rospy.loginfo('%s mode'%mode)
    # rospy.on_shutdown(finished)    
    if (mode == 'train') | (mode == 'train_all') :
        frontvis3d = FrontVis3DNode(mode='train_all')
        frontvis3d.train_all()
    elif mode == 'train_classifier' :
        frontvis3d = FrontVis3DNode(mode=mode)
        frontvis3d.train_classifier()
    elif mode == 'train_regressor' :
        frontvis3d = FrontVis3DNode(mode=mode)
        frontvis3d.train_regressor()
    else :
        frontvis3d = FrontVis3DNode(mode='predict')
        rospy.logwarn('ready')
        frontvis3d.loop()
    rospy.loginfo('done')
    sys.exit(0)