import tensorflow as tf
import dataset as ds
import numpy as np
import rospy
import sys
import cv2
import os

EPOCH = 20000
BATCH = 100
MIN_LOSS = 0.001
LEARN_RATE = 0.001

class Classifier(object) :
    def __init__(self, mode, *args, **kwargs):
        self.mode = mode

        if self.mode == 'nop' :
            return
        if 'epoch' in kwargs :
            global EPOCH
            EPOCH = kwargs['epoch']
        if 'batch_size' in kwargs :
            global BATCH
            BATCH = kwargs['batch_size']
        self.model_dir = './model/params'

        if 'model_dir' in kwargs :
            self.model_dir = kwargs['model_dir']

        self.ok = True
        
        self.train_data = None
        self.train_labels = None
        self.test_data = None
        self.test_labels = None
        self.dataset = ds.Dataset()
        if self.mode == 'train' :
            dataset_dir = os.environ['HOME']+'/ball_dataset/train'
            if 'train_dir' in kwargs :
                dataset_dir = kwargs['train_dir']
            self.dataset.parse_train(dataset_dir)
            self.train_data = self.dataset.get_train_images()
            self.train_labels = self.dataset.get_train_labels()
            print 'dataset : %s images, %s labels'%(len(self.dataset.train_images),len(self.dataset.train_labels))
            print 'train_images : %s, train_labels : %s' %(self.dataset.get_train_images().shape, self.dataset.get_train_labels().shape)
            if 'test_dir' in kwargs :
                if not (kwargs['test_dir'] is None) :
                    self.dataset.parse_test(kwargs['test_dir'])
                    self.test_data = self.dataset.get_test_images()
                    self.test_labels = self.dataset.get_test_labels()
                    print 'test data : %s images, %s labels'%(len(self.dataset.test_images),len(self.dataset.test_labels))
                    print 'test_images : %s, test_labels : %s' %(self.dataset.get_test_images().shape, self.dataset.get_test_labels().shape)

        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.2

        self.init_cnn_model()
        self.session = tf.Session(config=config)
        self.session.run([tf.global_variables_initializer()])
        self.saver = tf.train.Saver()

        if self.mode == 'train' :
            self.session.run([self.iterator.initializer], feed_dict={
                self.features : self.train_data,
                self.labels : self.train_labels,
            })
            if os.path.isfile(self.model_dir+'.index') :
                self.saver.restore(self.session, self.model_dir)
        else :
            self.saver.restore(self.session, self.model_dir)

        self.step = 0

    def trainloop(self) :
        if not (self.mode == 'train') :
            return
        print 'now training ball classifier with epoch %s and batch size : %s' %(EPOCH, BATCH)
        test_loss = 10.0
        self.step = 0
        done = False
        while (test_loss > MIN_LOSS) & self.ok & (not done):
            step, loss, test_loss, accuracy, done = self.train()
            if step%10 == 0 :
                print 'step %i :' %step, '| train loss:', loss, '| test loss:', test_loss, '| accuracy:', accuracy
        self.save()
    
    def train(self) :
        accuracy = None
        done = False
        test_loss = None
        try : 
            _, loss = self.session.run([self.train_op, self.loss])
            if self.step % 10 == 0 :
                if not (self.test_data is None) :
                    out, test_loss = self.session.run([self.output, self.loss], {
                        self.batch_features : self.test_data,
                        self.batch_labels : self.test_labels
                    })
                else :
                    test_loss = loss
                if (test_loss < MIN_LOSS) & (loss < MIN_LOSS) :
                    done = True
        except tf.errors.OutOfRangeError :
            done = True
            print 'reached last epoch'
        self.step = self.step + 1
        return self.step-1, loss, test_loss, accuracy, done

    def save(self) :
        print 'training ball classifier done! now saving in %s' %self.model_dir
        self.saver.save(self.session, self.model_dir, write_meta_graph=True)

    def predict(self, cv_img) :
        if not (self.mode == 'predict') :
            return None
        prediction = self.session.run([self.output_class], feed_dict={
            self.features: cv_img
        })
        print prediction
        return prediction

    def init_cnn_model(self):
        if self.mode == 'train' :
            self.features = tf.placeholder(tf.float32, self.train_data.shape, name="input")
            self.labels = tf.placeholder(tf.int32, self.train_labels.shape, name="label")
            self.tf_dataset = tf.data.Dataset.from_tensor_slices((self.features, self.labels))
            self.tf_dataset = self.tf_dataset.batch(BATCH)
            self.tf_dataset = self.tf_dataset.repeat(EPOCH)
            self.iterator = self.tf_dataset.make_initializable_iterator()
            self.batch_features, self.batch_labels = self.iterator.get_next()
            self.input_layer = self.batch_features
        else:
            self.features = tf.placeholder(tf.float32, self.dataset.single_image_shape)
            self.labels = tf.placeholder(tf.float32, self.dataset.single_label_shape)
            self.input_layer = self.features

        """Model function for CNN."""
        # Input Layer
        # input_layer = tf.reshape(self.features, [-1, 28, 28, 3])

        # Convolutional Layer #1
        self.conv1 = tf.layers.conv2d(
            inputs=self.input_layer,
            filters=32,
            kernel_size=[5, 5],
            padding="same",
            activation=tf.nn.relu,
            name="conv1")

        # Pooling Layer #1
        self.pool1 = tf.layers.max_pooling2d(inputs=self.conv1, pool_size=[2, 2], strides=2, name="pool1")

        # Convolutional Layer #2
        self.conv2 = tf.layers.conv2d(
            inputs=self.pool1,
            filters=64,
            kernel_size=[5, 5],
            padding="same",
            activation=tf.nn.relu,
            name="conv2")

        # Pooling Layer #2
        self.pool2 = tf.layers.max_pooling2d(inputs=self.conv2, pool_size=[2, 2], strides=2, name="pool2")

        # Flatten tensor into a batch of vectors
        self.pool2_flat = tf.reshape(self.pool2, [-1, 7 * 7 * 64], name="pool2_flat")

        # Dense Layer
        self.dense = tf.layers.dense(inputs=self.pool2_flat, units=1024, activation=tf.nn.relu, name="dense")

        # Add dropout operation; 0.6 probability that element will be kept
        self.dropout = tf.layers.dropout(
            inputs=self.dense, rate=0.4, training=(self.mode == 'train'), name="dropout")

        # Logits layer
        # logits = tf.layers.dense(inputs=dropout, units=2)
        # if self.mode == 'predict' :
        #     self.conv1 = tf.get_collection('conv1')[0]
        #     self.conv2 = tf.get_collection('conv2')[0]
        #     self.pool1 = tf.get_collection('pool1')[0]
        #     self.pool2 = tf.get_collection('pool2')[0]
        #     self.pool2 = tf.get_collection('pool2_flat')[0]
        #     self.dense = tf.get_collection('dense')[0]
        #     self.dropout = tf.get_collection('dropout')[0]
        # else :
        #     tf.add_to_collection("conv1",self.conv1)
        #     tf.add_to_collection("conv2",self.conv2)
        #     tf.add_to_collection("pool1",self.pool1)
        #     tf.add_to_collection("pool2",self.pool2)
        #     tf.add_to_collection("pool2_flat",self.pool2_flat)
        #     tf.add_to_collection("dense",self.dense)
        #     tf.add_to_collection("dropout",self.dropout)

        self.logits = tf.layers.dense(inputs=self.dropout, units=2, name="logits")
        self.output_class = tf.argmax(input=self.logits, axis=1, name="output_class")
        self.output = tf.nn.softmax(self.logits, name="softmax_tensor")

        if self.mode == 'train' :
            # Calculate Loss (for both TRAIN and EVAL modes)
            self.loss = tf.losses.sparse_softmax_cross_entropy(labels=self.batch_labels, logits=self.logits)
            self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=LEARN_RATE)
            self.train_op = self.optimizer.minimize(loss=self.loss)
            self.eval_op = tf.metrics.accuracy(labels=self.batch_labels, predictions=self.output_class)