import tensorflow as tf
import dataset as ds
import numpy as np
import classifier as clas
import rospy
import sys
import cv2

tf.logging.set_verbosity(tf.logging.INFO)

def run_training() :
    ball_classifier = clas.Classifier('train',model_dir='/home/fahri/ball_model')
    ball_classifier.train()

def run_predict() :
    ball_imglist = ['img-624165442-0','img-624165129','img-624165453-0']
    noball_imglist = ['img-624165153','img-62416542-1','img-624165337-0']
    ball_classifier = clas.Classifier('predict',model_dir='/home/fahri/ball_model')
    for f in ball_imglist :
        im = cv2.imread('/home/fahri/ball_dataset/'+f+'.jpg',cv2.IMREAD_COLOR)
        ball_classifier.predict(im)
    for f in noball_imglist :
        im = cv2.imread('/home/fahri/ball_dataset/'+f+'.jpg',cv2.IMREAD_COLOR)
        ball_classifier.predict(im)

if __name__ == '__main__' :
    # ball_classifier = clas.Classifier()
    # tf.app.run(main=ball_classifier.main)
    # run_training()
    run_predict()
    print 'done'