import numpy as np
import cv2
import os

DEFAULT_WIDTH   = 28
DEFAULT_HEIGHT  = 28
DEFAULT_CHANNEL = 3

MAX_PREDICT_BATCH = 30

class Dataset(object) :
    def __init__(self, *args, **kwargs):
        self.train_images = []
        self.train_labels = []
        self.test_images = []
        self.test_labels = []
        self.single_image_shape = (MAX_PREDICT_BATCH, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_CHANNEL)
        self.single_label_shape = (MAX_PREDICT_BATCH, None)
        self.n_data = 0

    def get_filenames(self, dir) :
        filenames = []
        for (path, dirs, files) in os.walk(dir) :
            filenames.extend(files)
            break
        return filenames

    def parse_label(self, label_file) :
        with open(label_file, 'r') as f:
            if f.read() == '1 0' :
                return 1
            else : return 0

    def parse_img(self, cv_file) :
        cv_img = cv2.imread(cv_file, cv2.IMREAD_COLOR)
        return cv_img

    def parse_train(self, dir) :
        print 'parsing %s' %dir
        filenames = self.get_filenames(dir)
        for f in filenames :
            file, ext = os.path.splitext(f)
            if ext == '.jpg' :
                image = self.parse_img(dir+'/'+file+'.jpg')
                label = self.parse_label(dir+'/'+file+'.txt')
                self.train_images.append(image)
                # self.train_images.append(image.reshape(image.size))
                self.train_labels.append(label)
        self.n_data = len(self.train_images)
        print '%s parsed'%dir

    def parse_test(self, dir) :
        print 'parsing test %s' %dir
        filenames = self.get_filenames(dir)
        for f in filenames :
            file, ext = os.path.splitext(f)
            if ext == '.jpg' :
                image = self.parse_img(dir+'/'+file+'.jpg')
                label = self.parse_label(dir+'/'+file+'.txt')
                self.test_images.append(image)
                # self.train_images.append(image.reshape(image.size))
                self.test_labels.append(label)
        self.n_test_data = len(self.test_images)
        print '%s parsed'%dir

    def get_train_images(self) :
        return np.asarray(self.train_images, dtype=np.float32)
    
    def get_train_labels(self) :
        return np.asarray(self.train_labels, dtype=np.int32)

    def get_test_images(self) :
        return np.asarray(self.test_images, dtype=np.float32)
    
    def get_test_labels(self) :
        return np.asarray(self.test_labels, dtype=np.int32)