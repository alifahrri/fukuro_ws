#!/usr/bin/env python

import os
import sys
import cv2
import rospy
import cv_bridge
import numpy as np
import tensorflow as tf
import depth_regression.regressor as reg 
import std_msgs.msg as std_msg
import geometry_msgs.msg as geo_msg
import sensor_msgs.msg as sensor_msg
import fukuro_common.msg as fukuro_msg

IMG_WIDTH = 640
IMG_HEIGHT = 480

CLS_IMG_WIDTH = 28
CLS_IMG_HEIGHT = 28

class DepthRegressorNode() :
    def __init__(self, *args, **kwargs):
        self.mode = 'train'

        if 'mode' in kwargs :
            self.mode = kwargs['mode']

        depth_train_file = rospy.get_param('train_file','data/train.txt')

        self.robot_name = os.environ['FUKURO']
        self.model_dir = os.environ['HOME']+'/fukuro_ws/src/fukuro_front_vision/models/'+self.robot_name

        self.regressor = reg.Regressor(self.mode, depth_train_file, model_dir=self.model_dir+'/regressor/regressor')

        self.sub = rospy.Subscriber('/front_vision/classified', fukuro_msg.BoundingBoxes, self.predict)
        self.pub = rospy.Publisher('/front_vision/3d',fukuro_msg.FrontVision3d,queue_size=31)

    def trainloop(self) :
        rospy.logwarn('Regression Training : ')
        done = False
        while (not rospy.is_shutdown()) & (not done) :
            step, train_loss, test_loss, prediction, done = self.regressor.train()
            if step % 5 == 0 :
                rospy.loginfo('step:%s, train_loss:%s, test_loss:%s, prediction:%s'%(step, train_loss, test_loss, prediction))
        self.regressor.save()
        rospy.logwarn('Regression Training : DONE')

    def predict(self, bbox) :
        msg3d = fukuro_msg.FrontVision3d()
        for b in bbox.bboxes :
            x, y, w, h = b.x, b.y, b.w, b.h
            input = np.array([[float(x)/IMG_WIDTH, float(y)/IMG_HEIGHT, float(w)/IMG_WIDTH, float(h)/IMG_HEIGHT]])
            pos = geo_msg.Point()
            prediction = self.regressor.predict(input)
            pos.x = prediction[0][0][0]
            pos.y = prediction[0][0][1]
            pos.z = prediction[0][0][2]
            msg3d.ball.append(pos)
        self.pub.publish(msg3d)

    def loop(self) :
        rospy.spin()

if __name__ == '__main__' :
    rospy.init_node('frontvis3d_classifier_node', argv=sys.argv)
    mode = rospy.get_param('mode','predict')
    rospy.loginfo('%s mode'%mode)
    if mode=='train' :
        frontvis_regressor = DepthRegressorNode(mode=mode)
        frontvis_regressor.trainloop()
    else :
        frontvis_regressor = DepthRegressorNode(mode=mode)
        frontvis_regressor.loop()
    rospy.logwarn('done')
    sys.exit(0)