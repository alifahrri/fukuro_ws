#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from gui import frontvis_gui
import fukuro_common.msg as fukuro_msg
import sensor_msgs.msg as sensor_msg
from PyQt5 import QtCore, QtWidgets, QtGui
import sys

gui = None

def timeout() :
    rospy.loginfo('timeout')
    if rospy.is_shutdown() :
        gui.widget.close()

if __name__ == '__main__' :
    rospy.init_node('front_vision_gui_node',argv=sys.argv)
    ball_dir = rospy.get_param('ball_dataset_dir','')
    prefix = rospy.get_param('filename_prefix','img')
    app = QtWidgets.QApplication(sys.argv)
    gui = frontvis_gui.FrontVisGUI(ball_dataset_dir=ball_dir, filename_prefix=prefix)
    imsub = rospy.Subscriber('/front_camera/image_raw', sensor_msg.Image, gui.imgCallback)
    bbox_sub = rospy.Subscriber('/front_vision', fukuro_msg.BoundingBoxes, gui.bboxCallback)
    clas_sub = rospy.Subscriber('/front_vision/classified', fukuro_msg.BoundingBoxes, gui.ballBBoxCallback)
    ball_sub = rospy.Subscriber('/front_vision/3d', fukuro_msg.FrontVision3d, gui.ballPosCallback)
    gui.widget.showMaximized()
    timer = QtCore.QTimer()
    timer.timeout.connect(timeout)
    timer.start(1000)
    app.exec_()
