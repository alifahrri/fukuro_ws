# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'frontvis_widget.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_frontvis(object):
    def setupUi(self, frontvis):
        frontvis.setObjectName("frontvis")
        frontvis.resize(640, 480)
        self.gridLayout_2 = QtWidgets.QGridLayout(frontvis)
        self.gridLayout_2.setContentsMargins(3, 3, 3, 3)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.splitter = QtWidgets.QSplitter(frontvis)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.graphicsView = QtWidgets.QGraphicsView(self.splitter)
        self.graphicsView.setObjectName("graphicsView")
        self.groupBox = QtWidgets.QGroupBox(self.splitter)
        self.groupBox.setMaximumSize(QtCore.QSize(400, 16777215))
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.load_btn = QtWidgets.QPushButton(self.groupBox)
        self.load_btn.setObjectName("load_btn")
        self.gridLayout.addWidget(self.load_btn, 0, 0, 1, 1)
        self.save_btn = QtWidgets.QPushButton(self.groupBox)
        self.save_btn.setObjectName("save_btn")
        self.gridLayout.addWidget(self.save_btn, 0, 1, 1, 1)
        self.textEdit = QtWidgets.QTextEdit(self.groupBox)
        self.textEdit.setObjectName("textEdit")
        self.gridLayout.addWidget(self.textEdit, 1, 0, 1, 2)
        self.gridLayout_2.addWidget(self.splitter, 0, 0, 1, 1)

        self.retranslateUi(frontvis)
        QtCore.QMetaObject.connectSlotsByName(frontvis)

    def retranslateUi(self, frontvis):
        _translate = QtCore.QCoreApplication.translate
        frontvis.setWindowTitle(_translate("frontvis", "Form"))
        self.groupBox.setTitle(_translate("frontvis", "Position Dataset :"))
        self.load_btn.setText(_translate("frontvis", "Load"))
        self.save_btn.setText(_translate("frontvis", "Save As"))

