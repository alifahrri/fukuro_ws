from PyQt5 import QtCore, QtWidgets, QtGui
import frontvis_widget
import frontvis_img
import numpy as np
import datetime

import fukuro_common.msg as fukuro_msg
import sensor_msgs.msg
import cv_bridge
import rospy
import cv2
import os

BALL    = '1 0'
NO_BALL = '0 1'

class FrontVisGUI(object) :
    def __init__(self, *args, **kwargs):
        self.ui = frontvis_widget.Ui_frontvis()
        self.widget = QtWidgets.QWidget()
        self.img_item = frontvis_img.FrontVisImg()
        self.ui.setupUi(self.widget)
        self.scene = QtWidgets.QGraphicsScene(-512,-360,1024,720,self.widget)
        self.scene.addItem(self.img_item)
        self.ui.graphicsView.setScene(self.scene)
        self.bridge = cv_bridge.CvBridge()
        self.image = np.zeros((256,256,3),dtype="uint8")
        self.ui.splitter.setSizes([1024, 0])
        self.widget.setWindowTitle('fukuRO front vision detections')
        self.ui.save_btn.clicked.connect(self.saveDataset)
        self.ui.load_btn.clicked.connect(self.loadDataset)
        self.img_item.dataset_callback = self.addDataset
        self.img_item.ball_class_dataset_callback = self.addBallClassDataset
        self.ball_data_dir = None
        self.filename_prefix = ''
        self.file_count = 0
        if 'ball_dataset_dir' in kwargs :
            path = kwargs['ball_dataset_dir']
            if os.path.isdir(path) :
                rospy.loginfo('dataset directory : %s' %path)
                self.ball_data_dir = path
            else :
                rospy.logwarn("directory %s doesn't exist" %path)
        if 'filename_prefix' in kwargs :
            self.filename_prefix = kwargs['filename_prefix']

    def addBallClassDataset(self, cv_imgs, labels) :
        if not (self.ball_data_dir is None) :
            now = datetime.datetime.now()
            rospy.loginfo('adding ball dataset')
            for i in range(len(cv_imgs)) :
                date = '%s%s%s%s%s-%s'%(now.month,now.day,now.hour,now.minute,now.second,i)
                filename = '%s/%s-%s'%(self.ball_data_dir,self.filename_prefix,date)
                rospy.loginfo('saving %s, ball? %s' %(filename, labels[i]))
                cv2.imwrite(filename+'.jpg', cv_imgs[i])
                with open(filename+'.txt','w+') as f :
                    if labels[i] :
                        f.write(BALL)
                    else :
                        f.write(NO_BALL)
    
    def addDataset(self, dataset) :
        rospy.loginfo('dataset : %s'%dataset)
        self.ui.textEdit.append(str(dataset).translate(None,'[],'))

    def saveDataset(self) :
        filename = QtWidgets.QFileDialog.getSaveFileName()[0]
        rospy.loginfo('saving dataset in %s'%filename)
        text_data = self.ui.textEdit.toPlainText()
        with open(filename, 'w+') as f :
            f.write(text_data)
            # for line in text_data.splitlines() :
            #     f.write(line)

    def loadDataset(self) :
        filename = QtWidgets.QFileDialog.getOpenFileName()[0]
        rospy.loginfo('loading dataset in %s'%filename)
        self.ui.textEdit.clear()
        with open(filename, 'r') as f:
            self.ui.textEdit.append(f.read())

    def imgCallback(self, img) :
        rospy.loginfo('image callback')
        self.image = self.bridge.imgmsg_to_cv2(img,"bgr8")
        self.img_item.updateImage(self.image)
        # self.scene.update()
        # cv2.imshow('raw image',self.image)

    def bboxCallback(self, bbox) :
        rospy.loginfo('bbox callback')
        self.img_item.updateDetection(bbox)
        # self.scene.update()

    def ballBBoxCallback(self, bbox) :
        rospy.loginfo('ball bbox callback')
        self.img_item.updateBallDetection(bbox)
        # self.scene.update()

    def ballPosCallback(self, ball) :
        rospy.loginfo('ballpos callback')
        self.img_item.updateBall(ball)
        self.scene.update()