from PyQt5 import QtCore, QtWidgets, QtGui
from fukuro_common.msg import BoundingBoxes
import numpy as np
import cv2
import posdialog
import balldialog

CHANNELS                = 3
CLASSIFIER_IM_WIDTH     = 28
CLASSIFIER_IM_HEIGHT    = 28
N_BYTES                 = CLASSIFIER_IM_WIDTH * CHANNELS
N_CAPTCHA_ROW           = 3
N_CAPTCHA_COL           = 4
MAX_CLASSIFIER_DATA     = N_CAPTCHA_ROW * N_CAPTCHA_COL

class PosDialog(object) :
    def __init__(self, parent=None) :
        self.dialog = QtWidgets.QDialog()
        self.ui = posdialog.Ui_Dialog()
        self.ui.setupUi(self.dialog)

    def data(self) :
        xpx = self.ui.xpx_sbox.value()
        ypx = self.ui.ypx_sbox.value()
        wpx = self.ui.wpx_sbox.value()
        hpx = self.ui.hpx_sbox.value()
        nxpx = self.ui.xpx_dsbox.value()
        nypx = self.ui.ypx_dsbox.value()
        nwpx = self.ui.wpx_dsbox.value()
        nhpx = self.ui.hpx_dsbox.value()
        xreal = self.ui.xreal_sbox.value()
        yreal = self.ui.yreal_sbox.value()
        zreal = self.ui.zreal_sbox.value()
        return [xpx, ypx, wpx, hpx, nxpx, nypx, nwpx, nhpx, xreal, yreal, zreal]

    def show(self, px_data) :
        self.ui.xpx_sbox.setValue(px_data[0])
        self.ui.ypx_sbox.setValue(px_data[1])
        self.ui.wpx_sbox.setValue(px_data[2])
        self.ui.hpx_sbox.setValue(px_data[3])
        self.ui.xpx_dsbox.setValue(px_data[4])
        self.ui.ypx_dsbox.setValue(px_data[5])
        self.ui.wpx_dsbox.setValue(px_data[6])
        self.ui.hpx_dsbox.setValue(px_data[7])
        return self.dialog.show()

class BallClassifierImage(QtWidgets.QGraphicsItem) :
    def __init__(self, parent=None) :
        QtWidgets.QGraphicsItem.__init__(self, parent)
        self.imgs = []
        self.labels = []
        self.check_boxes = []
        w = N_CAPTCHA_COL * CLASSIFIER_IM_WIDTH
        h = N_CAPTCHA_ROW * CLASSIFIER_IM_HEIGHT
        topleft = QtCore.QPointF(-w/2,-h/2)
        size = QtCore.QSizeF(CLASSIFIER_IM_WIDTH,CLASSIFIER_IM_HEIGHT)
        for _ in range(MAX_CLASSIFIER_DATA) :
            self.imgs.append(QtGui.QImage())
        for i in range(N_CAPTCHA_ROW) :
            for j in range(N_CAPTCHA_COL) :
                dy = i*CLASSIFIER_IM_HEIGHT
                dx = j*CLASSIFIER_IM_WIDTH
                self.check_boxes.append(QtCore.QRectF(topleft,size).translated(dx,dy))
                self.labels.append(False)

    def setImage(self, images) :
        for i in range(MAX_CLASSIFIER_DATA) :
            self.labels[i] = False
            if i < len(images) :
                self.imgs[i] = QtGui.QImage(images[i],CLASSIFIER_IM_WIDTH,CLASSIFIER_IM_HEIGHT,N_BYTES,QtGui.QImage.Format_RGB888).rgbSwapped()
            else :
                self.imgs[i] = QtGui.QImage()

    def paint(self, painter, option, widget) :
        font = painter.font()
        font.setPointSizeF(5.0)
        font.setBold(True)
        painter.setFont(font)
        for i in range(N_CAPTCHA_ROW) :
            for j in range(N_CAPTCHA_COL) :
                idx = i*N_CAPTCHA_COL+j
                im = self.imgs[idx]
                if not im.isNull() :
                    px = QtGui.QPixmap.fromImage(im)
                    painter.drawPixmap(self.check_boxes[idx].topLeft(),px)
                    painter.setPen(QtGui.QColor(0,0,0))
                    painter.drawRect(self.check_boxes[idx])
                    if self.labels[idx] :
                        painter.setPen(QtGui.QColor(0,255,0))
                        painter.drawText(self.check_boxes[idx],"ball")
        painter.setPen(QtGui.QColor(255,0,0))
        painter.drawRect(self.boundingRect())

    def mousePressEvent(self, event) :
        for i in range(MAX_CLASSIFIER_DATA) :
            if self.check_boxes[i].contains(event.pos()) :
                self.labels[i] = not self.labels[i]
                break
        if not (self.scene() is None) :
            self.scene().update()

    def boundingRect(self) :
        w = N_CAPTCHA_COL * CLASSIFIER_IM_WIDTH
        h = N_CAPTCHA_ROW * CLASSIFIER_IM_HEIGHT
        return QtCore.QRectF(-w/2,-h/2,w,h)

class BallClassifierDialog(object) :
    def __init__(self, parent=None) :
        self.dialog = QtWidgets.QDialog()
        self.img = BallClassifierImage()
        self.ui = balldialog.Ui_Dialog()
        self.ui.setupUi(self.dialog)
        self.scene = QtWidgets.QGraphicsScene(self.img.boundingRect(),self.dialog)
        self.ui.graphicsView.setScene(self.scene)
        self.ui.graphicsView.scale(2.25,2.25)
        self.scene.addItem(self.img)
        self.dialog.setWindowTitle('Select all squares with balls')
        self.cv_balls = []

    def bbox2cv(self, cv_img, bbox) :
        images = []
        if (cv_img is not None) and (bbox is not None) :
            for box in bbox.bboxes :
                x, y, w, h = box.x, box.y, box.w, box.h
                cropped = cv_img[y:y+h, x:x+w]
                resized = cv2.resize(cropped,(CLASSIFIER_IM_WIDTH,CLASSIFIER_IM_HEIGHT))
                images.append(resized)
        return images

    def isVisible(self) :
        return self.dialog.isVisible()

    def show(self, cv_img, bbox) :
        img = self.bbox2cv(cv_img, bbox)
        self.cv_balls = img
        self.img.setImage(img)
        self.scene.update()
        return self.dialog.show()

class FrontVisImg(QtWidgets.QGraphicsItem) :
    def __init__(self, parent=None):
        QtWidgets.QGraphicsItem.__init__(self,parent)
        self.img = QtGui.QImage()
        self.cv_img = np.empty((256,256,3),dtype='uint8')
        self.img_rect = QtCore.QRectF()
        self.bbox = None
        self.ball_bbox = None
        self.ball_pos = None
        self.setAcceptHoverEvents(True)
        self.hover_pos = QtCore.QPointF()
        self.balldialog = BallClassifierDialog()
        self.posdialog = PosDialog()
        self.posdialog.dialog.accepted.connect(self.addDataset)
        self.balldialog.dialog.accepted.connect(self.addBallClassDataset)
        self.dataset_callback = None
        self.ball_class_dataset_callback = None
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsFocusable)
    
    def updateDetection(self, bbox) :
        self.bbox = bbox

    def updateBallDetection(self, bbox) :
        self.ball_bbox = bbox
    
    def updateImage(self, img) :
        self.cv_img = img

    def updateBall(self, ball) :
        self.ball_pos = ball

    def addDataset(self) :
        dataset = self.posdialog.data()
        if self.dataset_callback != None :
            self.dataset_callback(dataset)

    def addBallClassDataset(self) :
        print 'create ball class dataset'
        if self.ball_class_dataset_callback != None :
            self.ball_class_dataset_callback(self.balldialog.cv_balls,self.balldialog.img.labels)
        
    def paint(self, painter, option, widget) :
        if self.cv_img.size :
            height, width, channels = self.cv_img.shape
            bytes_per_line = 3 * width
            self.img = QtGui.QImage(self.cv_img.data, width, height, bytes_per_line, QtGui.QImage.Format_RGB888).rgbSwapped()
            rect = self.img.rect()
            self.img_rect = QtCore.QRectF(-rect.width()/2,-rect.height()/2,rect.width(),rect.height())
            if not self.img.isNull() :
                pixmap = QtGui.QPixmap.fromImage(self.img)
                painter.drawPixmap(self.img_rect.topLeft(), pixmap)
            if self.bbox is not None :
                font = QtGui.QFont("ubuntu",6.*width/320.)
                painter.setFont(font)
                i = 0
                for box in self.bbox.bboxes :
                    x, y, w, h = box.x, box.y, box.w, box.h
                    c = box.rgb
                    color = (c[0]*64,c[1]*64,c[2]*64)
                    p = QtCore.QPointF(x-width/2,y-height/2)
                    s = QtCore.QSizeF(w,h)
                    r = QtCore.QRectF(p,s)
                    brush = QtGui.QColor(color[0],color[1],color[2])
                    painter.setPen(QtGui.QPen(brush,0.75*width/320.,QtCore.Qt.SolidLine))
                    if r.contains(self.hover_pos) :
                        brush.setAlphaF(0.5)
                        painter.setBrush(brush)
                    else :
                        painter.setBrush(QtGui.QColor(0,0,0,0))
                    painter.drawRect(r)
                    # cv2.rectangle(self.cv_img,(x,y),(x+w,y+h),color,thickness=2)
                    # cv2.putText(self.cv_img,box.label,(x,y),cv2.FONT_HERSHEY_PLAIN,2.0,color,thickness=2)
            if self.ball_bbox is not None :
                font = QtGui.QFont("ubuntu",6.*width/320.)
                painter.setFont(font)
                i = 0
                for box in self.ball_bbox.bboxes :
                    x, y, w, h = box.x, box.y, box.w, box.h
                    c = box.rgb
                    color = (c[0]*255,c[1]*255,c[2]*255)
                    p = QtCore.QPointF(x-width/2,y-height/2)
                    s = QtCore.QSizeF(w,h)
                    r = QtCore.QRectF(p,s)
                    brush = QtGui.QColor(color[0],color[1],color[2])
                    painter.setPen(QtGui.QPen(brush,0.75*width/320.,QtCore.Qt.SolidLine))
                    if r.contains(self.hover_pos) :
                        brush.setAlphaF(0.5)
                        painter.setBrush(brush)
                    else :
                        painter.setBrush(QtGui.QColor(0,0,0,0))
                    painter.drawRect(r)
                    label = box.label
                    if self.ball_pos is not None :
                        if i < len(self.ball_pos.ball) :
                            label = label + ('(%.2f,%.2f,%.2f)')%(self.ball_pos.ball[i].x, self.ball_pos.ball[i].y, self.ball_pos.ball[i].z)
                    painter.drawText(r.x(),r.y(),label)
                    i += 1

    def boundingRect(self) :
        # if self.img == None:
        #     return QtCore.QRectF(-320,-240,640,480)
        # else :
        #     return self.img_rect
        return QtCore.QRectF(-640,-360,1280,720)

    def hoverMoveEvent(self, event) :
        self.hover_pos = event.pos()

    def mousePressEvent(self, event) :
        if not (self.bbox is None) :
            height, width, _ = self.cv_img.shape
            self.test_box = []
            for box in self.bbox.bboxes :
                p = QtCore.QPointF(box.x-width/2, box.y-height/2)
                s = QtCore.QSizeF(box.w, box.h)
                r = QtCore.QRectF(p,s)
                if r.contains(event.pos()) :
                    self.test_box = [box.x, box.y, box.w, box.h]
            
    def mouseReleaseEvent(self, event) :
        if not (self.bbox is None) :
            height, width, _ = self.cv_img.shape
            if len(self.test_box) > 0 :
                x = self.test_box[0]
                y = self.test_box[1]
                w = self.test_box[2]
                h = self.test_box[3]
                rx = float(x)/width
                ry = float(y)/height
                rw = float(w)/width
                rh = float(h)/height
                self.posdialog.show([x,y,w,h,rx,ry,rw,rh])

    def keyPressEvent(self, event) :
        print 'keypress detected %s' %(event)
        if event.key() == QtCore.Qt.Key_Space :
            if not self.balldialog.isVisible() :
                self.balldialog.show(self.cv_img, self.bbox)