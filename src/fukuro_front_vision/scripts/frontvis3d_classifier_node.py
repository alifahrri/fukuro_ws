#!/usr/bin/env python

import os
import sys
import cv2
import rospy
import cv_bridge
import numpy as np
import tensorflow as tf
import ball_classification.classifier as clf
import std_msgs.msg as std_msg
import geometry_msgs.msg as geo_msg
import sensor_msgs.msg as sensor_msg
import fukuro_common.msg as fukuro_msg

IMG_WIDTH = 640
IMG_HEIGHT = 480

CLS_IMG_WIDTH = 28
CLS_IMG_HEIGHT = 28

class BallClassifierNode() :
    def __init__(self, *args, **kwargs):
        self.mode = 'train'

        if 'mode' in kwargs :
            self.mode = kwargs['mode']
        
        train_dir = rospy.get_param('classifier_train_dir', os.environ['HOME']+'/fukuro_ws/data/ball_dataset/train')
        test_dir = rospy.get_param('classifier_test_dir', os.environ['HOME']+'/fukuro_ws/data/ball_dataset/test')

        self.cls_resize = (CLS_IMG_WIDTH, CLS_IMG_HEIGHT)
        self.robot_name = os.environ['FUKURO']
        self.model_dir = os.environ['HOME']+'/fukuro_ws/src/fukuro_front_vision/models/'+self.robot_name

        self.classifier = clf.Classifier(self.mode, train_dir=train_dir, test_dir=test_dir, model_dir=self.model_dir+'/classifier/classifier')

        if self.mode == 'predict' :
            self.bridge = cv_bridge.CvBridge()
            self.imsub = rospy.Subscriber('/front_camera/image_raw', sensor_msg.Image, self.updateImage)
            self.sub = rospy.Subscriber('/front_vision', fukuro_msg.BoundingBoxes, self.predict)
            self.pub = rospy.Publisher('/front_vision/classified', fukuro_msg.BoundingBoxes, queue_size=5)
            self.img = None
            self.prediction_process = False
            self.resized_list = []
            for _ in range(30) :
                self.resized_list.append(np.zeros((28, 28, 3)))
            self.resized_array = np.array(self.resized_list)

    def updateImage(self, img) :
        if self.prediction_process :
            return
        rospy.loginfo('image callback')
        self.img = self.bridge.imgmsg_to_cv2(img, 'bgr8')
    
    def loop(self) :
        rospy.spin()
    
    def trainloop(self) :
        rospy.logwarn('Classifier Training : ')
        done = False
        while (not rospy.is_shutdown()) & (not done) :
            step, train_loss, test_loss, accuracy, done = self.classifier.train()
            if step % 10 == 0 :
                rospy.loginfo('step:%s, train loss:%s, test loss:%s'%(step,train_loss, test_loss))
        self.classifier.save()
        rospy.logwarn('Classifier Training : DONE')

    def predict(self, bbox) :
        if self.img is None :
            return
        self.prediction_process = True
        t0 = rospy.Time.now()
        rospy.loginfo('predictiing %s bbox' %len(bbox.bboxes))
        box_classified = fukuro_msg.BoundingBoxes()
        for i in range(30) :
            if i < len(bbox.bboxes) :
                b = bbox.bboxes[i]
                x, y, w, h = b.x, b.y, b.w, b.h
                cropped = self.img[y:y+h, x:x+w]
                resized = cv2.resize(cropped, self.cls_resize)
                self.resized_array[i] = resized
        self.prediction_process = False
        
        print 'resized array shape :', self.resized_array.shape

        is_ball = self.classifier.predict(self.resized_array)
        for i in range(len(bbox.bboxes)) :
            print is_ball[0][i] 
            if is_ball[0][i] :
                box_classified.bboxes.append(bbox.bboxes[i])
        t1 = rospy.Time.now()
        rospy.loginfo('prediction time : %s' %((t1-t0).to_sec()))
        self.pub.publish(box_classified)

if __name__ == '__main__' :
    rospy.init_node('frontvis3d_classifier_node', argv=sys.argv)
    mode = rospy.get_param('mode','predict')
    rospy.loginfo('%s mode'%mode)
    if mode=='train' :
        frontvis_classifier = BallClassifierNode(mode=mode)
        frontvis_classifier.trainloop()
    else :
        frontvis_classifier = BallClassifierNode(mode=mode)
        frontvis_classifier.loop()
    rospy.logwarn('done')
    sys.exit(0)