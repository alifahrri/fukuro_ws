#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import cv_bridge
import rospy
from sensor_msgs import msg

print 'your opencv version %s'%cv2.__version__

file = rospy.get_param('video_file','')
pub = rospy.Publisher('/front_camera/image',msg.Image,queue_size=3)

if __name__ == '__main__' :
    rospy.init_node('image_publisher',anonymous=True)
    cap = cv2.VideoCapture(file)
    # cap = cv2.VideoCapture(1)
    bridge = cv_bridge.CvBridge()
    rate = rospy.Rate(30.0)
    while not rospy.is_shutdown() :
        success, image = cap.read()
        if not success :
            cap = cv2.VideoCapture(file)
        else :
            try :
                #resized = cv2.resize(image,(image.shape[1]/2,image.shape[0]/2))
                #img_msg = bridge.cv2_to_imgmsg(resized,"bgr8")
                img_msg = bridge.cv2_to_imgmsg(image,"bgr8")
                print 'publishing message'
                pub.publish(img_msg)
            except cv_bridge.CvBridgeError :
                print cv_bridge.CvBridgeError.message
        rate.sleep()

    print 'done'
