#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
  ,timer(new QTimer(this)),
    fukuroDB(new FukuroDB),
    isConnected(DISCONNECTED),
    robotscene(new QGraphicsScene),
    robot(new RobotItem)
{
    ui->setupUi(this);

    ui->RobotCondition->setScene(robotscene);
    ui->RobotCondition->scene()->setBackgroundBrush(Qt::green);
    ui->RobotCondition->scene()->addItem(robot);
    robot->setPos(0.6,-0.75,0.0);

    teammates_pose.reserve(6);
    isAvailable.reserve(6);

    int agent = std::atoi(std::getenv("AGENT"));
    tx_port = 23898;

    switch (agent) {
    case 1:{
        rx_port = 10599;
        ui->rx_port->setValue(10599);
        break;
    }
    case 2:{
        rx_port = 16180;
        ui->rx_port->setValue(16180);
        break;
    }
    case 3:{
        rx_port = 31415;
        ui->rx_port->setValue(31415);
        break;
    }
    case 4:{
        rx_port = 46692;
        ui->rx_port->setValue(46692);
        break;
    }
    case 5:{
        rx_port = 10898;
        ui->rx_port->setValue(10898);
        break;
    }
    case 6:{
        rx_port = 13080;
        ui->rx_port->setValue(13080);
        break;
    }
    }

    address = default_address;
    ui->address->setText(QString(address.c_str())); //default Multicast IPv4 for fukuro
    address = ui->address->text().toStdString();
    ui->tx_port->setValue(23898);

    std::cout<<agent<<' '<<rx_port<<' '<<address<<std::endl;

    setWindowTitle("Fukuro Communication");
    setFixedSize(this->size());

    ui->bat_info_1->setText("12V");
    ui->bat_info_3->setText("24V");
    ui->goal_color_lb->setText("Welcome");

    QObject::connect(timer,&QTimer::timeout,this,&MainWindow::updateInfoGUI);
    timer->start(50);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::walk(){
    addr_cb(address);
    tx_cb(tx_port);
    rx_cb(rx_port);
    QObject::connect(ui->address,&QLineEdit::editingFinished,[&](){
        address = ui->address->text().toStdString();
        addr_cb(address);
    });
    QObject::connect(ui->tx_port,&QSpinBox::editingFinished,[&](){
        tx_port = ui->tx_port->value();
        tx_cb(tx_port);
    });
    QObject::connect(ui->rx_port,&QSpinBox::editingFinished,[&](){
        rx_port = ui->rx_port->value();
        rx_cb(rx_port);
    });

    QObject::connect(ui->connectButton,&QPushButton::clicked,[&](){
        connectButton(address,tx_port,rx_port);
    });
    //auto-connect
#ifdef NO_GUI //redudancy
    isConnected = CONNECTED;
    connectcb(true);
    ui->connectButton->setText("Disconnect");
#endif
}

void MainWindow::connectButton(std::string address, int tx_port, int rx_port){
    if(isConnected == DISCONNECTED){
        connectcb(true);
        ui->connectButton->setText("Disconnect");
        isConnected = CONNECTED;
    }
    else if(isConnected == CONNECTED){
        connectcb(false);
        ui->connectButton->setText("Connect");
        isConnected = DISCONNECTED;
    }
    ui->address->setText(QString(address.c_str()));
    addr_cb(address);
    ui->tx_port->setValue(tx_port);
    tx_cb(tx_port);
    ui->rx_port->setValue(rx_port);
    rx_cb(rx_port);
}

void MainWindow::updateInfoGUI(){

    tex.lock();
    for(int i = 24; i<(AGENT+6)*4; i+=4){
        int idx = (i/4)-6;
        teamate_pose.x = fukuroDB->search(i);
        teamate_pose.y = fukuroDB->search(i+1);
        teamate_pose.theta = fukuroDB->search(i+2);
        teammates_pose.push_back(teamate_pose);
        (fukuroDB->getAvailability(idx))?isAvailable.push_back(true)
                                       :isAvailable.push_back(false);
    }

    teammate_msg.state = (int)fukuroDB->search(20);
    teammate_msg.robotname = (int)fukuroDB->search(0);
    for(int i=0; i<AGENT; i++){
        teammate_msg.pose.push_back(teammates_pose[i]);
        teammate_msg.available.push_back(isAvailable[i]);
    }
    teammate_msg.behavior = (int)fukuroDB->search(16);
    isConnected==CONNECTED? teammate_msg.isConnected = true : teammate_msg.isConnected = false;
    fukuroDB->isManualPositioning()? teammate_msg.isManualPositioning = true : teammate_msg.isManualPositioning = false;

    pub_cb(teammate_msg);

    auto vx = fukuroDB->getManualPositioning(4);
    auto vy = fukuroDB->getManualPositioning(5);
    auto w = fukuroDB->getManualPositioning(6);
    hw_msg.Vx = vx;
    hw_msg.Vy = vy;
    hw_msg.w = w;
    hw_cb(hw_msg);

    for(int i=0; i<AGENT; i++){
        teammate_msg.pose.pop_back();
        teammate_msg.available.pop_back();
        teammates_pose.pop_back();
    }

    //Game State??
    switch ((int)fukuroDB->search(20)) {
    case 0:
        ui->goal_color_lb->setText("Welcome");
        break;
    case 1:
        ui->goal_color_lb->setText("[OUR] K-Off");
        break;
    case 2:
        ui->goal_color_lb->setText("[OPP] K-Off");
        break;
    case 3:
        ui->goal_color_lb->setText("[OUR] F-Kick");
        break;
    case 4:
        ui->goal_color_lb->setText("[OPP] F-Kick");
        break;
    case 5:
        ui->goal_color_lb->setText("[OUR] G-Kick");
        break;
    case 6:
        ui->goal_color_lb->setText("[OPP] G-Kick");
        break;
    case 7:
        ui->goal_color_lb->setText("[OUR] T-In");
        break;
    case 8:
        ui->goal_color_lb->setText("[OPP] T-In");
        break;
    case 9:
        ui->goal_color_lb->setText("[OUR] C-Kick");
        break;
    case 10:
        ui->goal_color_lb->setText("[OPP] C-Kick");
        break;
    case 11:
        ui->goal_color_lb->setText("[OUR] Penalty");
        break;
    case 12:
        ui->goal_color_lb->setText("[OPP] Penalty");
        break;
    case 13:
        ui->goal_color_lb->setText("[OUR] Goal");
        break;
    case 14:
        ui->goal_color_lb->setText("[OPP] Goal");
        break;
    case 15:
        ui->goal_color_lb->setText("[OUR] Repair");
        break;
    case 16:
        ui->goal_color_lb->setText("[OPP] Repair");
        break;
    case 17:
        ui->goal_color_lb->setText("[OUR] RedCard");
        break;
    case 18:
        ui->goal_color_lb->setText("[OPP] RedCard");
        break;
    case 19:
        ui->goal_color_lb->setText("[OUR] YellowCard");
        break;
    case 20:
        ui->goal_color_lb->setText("[OPP] YellowCard");
        break;
    case 21:
        ui->goal_color_lb->setText("Start");
        break;
    case 22:
        ui->goal_color_lb->setText("Stop");
        break;
    case 23:
        ui->goal_color_lb->setText("Drop-Ball");
        break;
    case 24:
        ui->goal_color_lb->setText("Park");
        break;
    case 25:
        ui->goal_color_lb->setText("1st");
        break;
    case 26:
        ui->goal_color_lb->setText("2nd");
        break;
    case 27:
        ui->goal_color_lb->setText("3rd");
        break;
    case 28:
        ui->goal_color_lb->setText("4th");
        break;
    }

    switch ((int)fukuroDB->search(15)) {
    case 0:{
        ui->robot_role_lb->setText("NoBehavior");
    }
        break;
    case 1:{
        ui->robot_role_lb->setText("Defender");
    }
        break;
    case 2:{
        ui->robot_role_lb->setText("Defender 2");
    }
        break;
    case 3:{
        ui->robot_role_lb->setText("Timer Defense");
    }
        break;
    case 4:{
        ui->robot_role_lb->setText("Timer Defense 2");
    }
        break;
    case 5:{
        ui->robot_role_lb->setText("Striker");
    }
        break;
    case 6:{
        ui->robot_role_lb->setText("Striker Kick");
    }
        break;
    case 7:{
        ui->robot_role_lb->setText("Striker Dribble");
    }
        break;
    case 8:{
        ui->robot_role_lb->setText("Goalie");
    }
        break;
    case 9:{
        ui->robot_role_lb->setText("Timer Striker");
    }
        break;
    case 10:{
        ui->robot_role_lb->setText("Timer Striker 2");
    }
        break;
    default:
        break;
    }

    switch ((int)fukuroDB->search(16)) {
    case 0:{
        ui->robot_behavior_lb->setText("Positioning");
        break;
    }
    case 1:{
        ui->robot_behavior_lb->setText("Gameplay");
        break;
    }
    case 2:{
        ui->robot_behavior_lb->setText("Stop");
        break;
    }
    default:
        break;
    }

    switch((int)fukuroDB->search(19)){
    case 0:{
        ui->team_color_lb->setText("Cyan");
        break;
    }
    case 1:{
        ui->team_color_lb->setText("Magenta");
        break;
    }
    }

    if(fukuroDB->search(1) && fukuroDB->getAvailability(fukuroDB->search(0)))
        ui->Robot_Running_lb->setText(QString::number(1));
    else
        ui->Robot_Running_lb->setText(QString::number(0));

    ui->robot_coaching_lb->setText(QString::number(fukuroDB->search(18)));
    ui->robot_visible_lb->setText(QString::number(fukuroDB->search(17)));
    ui->Robot_number_lb->setText(QString::number(fukuroDB->search(0)));
    ui->robot_orientation_lb_deg->setText(QString::number(fukuroDB->search(6)));
    ui->robot_orientation_lb_rad->setText(QString::number(fukuroDB->search(7)));
    ui->Robot_position_lb_x->setText(QString::number(fukuroDB->search(2)));
    ui->Robot_position_lb_y->setText(QString::number(fukuroDB->search(3)));
    ui->Robot_velocity_lb_x->setText(QString::number(fukuroDB->search(4)));
    ui->Robot_velocity_lb_y->setText(QString::number(fukuroDB->search(5)));
    ui->bat_info_2->setText(QString::number(fukuroDB->search(21)));
    ui->bat_info_4->setText(QString::number(fukuroDB->search(22)));
    ui->kicker_c_info->setText(QString::number(fukuroDB->search(23)));
    ui->ball_position_lb_x->setText(QString::number(fukuroDB->search(8)));
    ui->ball_position_lb_y->setText(QString::number(fukuroDB->search(9)));
    ui->ball_velocity_lb_x->setText(QString::number(fukuroDB->search(10)));
    ui->ball_velocity_lb_y->setText(QString::number(fukuroDB->search(11)));
    ui->ball_engaged_lb->setText(QString::number(fukuroDB->search(12)));
    ui->ball_own_lb->setText(QString::number(fukuroDB->search(13)));
    ui->ball_visible_lb->setText(QString::number(fukuroDB->search(14)));

    robot->setPos(0.6,-0.75,fukuroDB->search(7));
    ui->RobotCondition->update();

    tex.unlock();

    std::this_thread::sleep_for(std::chrono::milliseconds(50));
}
