#ifndef ROBOTITEM_H
#define ROBOTITEM_H

#include <QGraphicsItem>
//#include <thread>
//#include <chrono>

//#define USE_ROBOT_STATE

#ifdef USE_ROBOT_STATE
namespace fukuro {
struct RobotState;
typedef std::tuple<RobotState,RobotState,RobotState> RobotState9;
typedef std::pair<RobotState,RobotState> RobotState6;
}
#endif

class RobotItem : public QGraphicsItem
{
public:
    RobotItem();
    ~RobotItem() {}
#ifdef USE_ROBOT_STATE
    void setRobotState(fukuro::RobotState6 *rstate_) { rstate = rstate_; }
#else
    void setState(double x, double y, double w, double _vx, double _vy, double _vw)
    {
        px = x;
        py = y;
        angle_rad = w;
        vx = _vx;
        vy = _vy;
        vw = _vw;
    }
    void setPos(double x, double y, double w) {
        px = x; py = y;
        //std::this_thread::sleep_for (std::chrono::milliseconds(100));
        //px = 0.6; py = -0.75; //normalized
        angle_rad = w;
    }
    double getPos(int idx){
        switch(idx){
        case 0:
            return px;
        case 1:
            return py;
        case 2:
            return angle_rad;
        }
    }
#endif
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    void drawText(bool d) { draw_text = d; }
    void drawArrow(bool a) { draw_arrow = a; }
    QRectF boundingRect() const Q_DECL_OVERRIDE;

private:
    double px, py, angle_rad;
    double vx, vy, vw;
    bool draw_text;
    bool draw_arrow;
    QPixmap robot;
#ifdef USE_ROBOT_STATE
    fukuro::RobotState6* rstate;
#endif
    // double* time;
};

#endif //ROBOTITEM_H
