#ifndef MULTICASTRECEIVER_H
#define MULTICASTRECEIVER_H


#include "fukurodb.h"

class MulticastReceiver : public QObject
{
    Q_OBJECT

public:
    MulticastReceiver();
    ~MulticastReceiver();
    void loop();
    void start(){
        running = true;
        thread = std::thread(&MulticastReceiver::loop,this);
    }
    void stop(){
        running = false;
        if(thread.joinable())
            thread.join();
    }
    void setFukuroDB(FukuroDB *db){fukuroDB=db;}

public slots:
    void setPort(quint16 port){ this->port = port;}
    void setAddress(std::string address){ this->address = address;}
    void setConnection(bool status){editflag = status;}

signals:
    void databaseChanged(int idx);

private:
    std::thread thread;
    std::mutex tex;
    std::atomic_bool running;
    std::atomic_int fukuro;
    std::atomic_bool editflag;
    std::atomic_bool lastEditFlag;
    BasestationSend_RobotStatus_RobotName robotname[6];
    FukuroDB *fukuroDB;
    std::string address;
    quint16 port;
};

#endif // MULTICASTRECEIVER_H
