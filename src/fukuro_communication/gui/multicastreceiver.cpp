#include "multicastreceiver.h"

MulticastReceiver::MulticastReceiver():
    fukuro(0), fukuroDB(new FukuroDB), lastEditFlag(false), editflag(false)
{
    robotname[0] = BasestationSend_RobotStatus_RobotName_FUKURO1;
    robotname[1] = BasestationSend_RobotStatus_RobotName_FUKURO2;
    robotname[2] = BasestationSend_RobotStatus_RobotName_FUKURO3;
    robotname[3] = BasestationSend_RobotStatus_RobotName_FUKURO4;
    robotname[4] = BasestationSend_RobotStatus_RobotName_FUKURO5;
    robotname[5] = BasestationSend_RobotStatus_RobotName_FUKURO6;
}

MulticastReceiver::~MulticastReceiver()
{

}

void MulticastReceiver::loop(){

    QUdpSocket *socket;
    socket = new QUdpSocket;

    BasestationSend read;

    fukuroDB->setManualPositioning(false);

    while(running){
        QByteArray data;

        if(lastEditFlag!=editflag)
            if(editflag){
                socket->bind(QHostAddress::AnyIPv4,port,QUdpSocket::ShareAddress|QUdpSocket::ReuseAddressHint);
                socket->joinMulticastGroup(QHostAddress(QString::fromStdString(address)));
                lastEditFlag = true;
            }
            else{
                if(socket->state()==QUdpSocket::BoundState)
                    socket->leaveMulticastGroup(QHostAddress(QString::fromStdString(address)));
                socket->close();
                lastEditFlag = false;
            }

        fukuro = fukuroDB->search(0);

        for(int i=0; i<6; i++)
            fukuroDB->setAvailability(i,false);

        if(socket->state()==QUdpSocket::BoundState){
            while(socket->hasPendingDatagrams()){
                tex.lock();

                data.resize(socket->pendingDatagramSize());
                socket->readDatagram(data.data(),data.size());

                if(read.ParseFromArray(data.data(),data.size())){
                    //ROS_WARN("DATAGRAM HAS BEEN RECEIVED");

                    if(read.has_robotstatus()){
                        //ROS_WARN("DATAGRAM HAS BEEN RECEIVED");
                        fukuroDB->setAvailability(fukuro,true);
                        auto robotname = read.robotstatus().robotname();
                        if(robotname == this->robotname[fukuro]){
                            //ROS_WARN("DATAGRAM HAS BEEN RECEIVED");
                            this->robotname[fukuro-1]?fukuroDB->setAvailability(fukuro-1,true):fukuroDB->setAvailability(fukuro-1,false);
                            auto behavior = read.robotstatus().behavior();
                            auto role =  read.robotstatus().role();
                            auto teamcolor = read.robotstatus().teamcolor();
                            auto isCoaching = read.robotstatus().iscoaching();
                            auto isrobotVisible = read.robotstatus().isrobotvisible();
                            auto isRunning = read.robotstatus().isrunning();

                            fukuroDB->setLocalization(false);

                            if(read.robotstatus().has_gamestate())
                            {
                                auto gamestate = read.robotstatus().gamestate();
                                fukuroDB->insert(20,gamestate);
                            }

                            if(read.robotstatus().islocalized()){
                                //if(read.robotstatus().has_x_desired()){
                                auto x_desired = read.robotstatus().x_desired();
                                fukuroDB->setManualPositioning(0,x_desired);
                                //fukuroDB->setManualPositioning(true);
                                fukuroDB->setLocalization(true);
                                //}
                                //else{
                                //    fukuroDB->setLocalization(false);
                                //    fukuroDB->setManualPositioning(false);
                                //}

                                //if(read.robotstatus().has_y_desired()){
                                auto y_desired = read.robotstatus().y_desired();
                                fukuroDB->setManualPositioning(1,y_desired);
                                //fukuroDB->setManualPositioning(true);
                                //}

                                //if(read.robotstatus().has_wrad_desired()){
                                auto w_rad_desired = read.robotstatus().wrad_desired();
                                fukuroDB->setManualPositioning(2,w_rad_desired);
                                //fukuroDB->setManualPositioning(true);
                                //}

                                //if(read.robotstatus().has_wdeg_desired()){
                                auto w_deg_desired = read.robotstatus().wdeg_desired();
                                fukuroDB->setManualPositioning(3,w_deg_desired);
                                //fukuroDB->setManualPositioning(true);
                            }
                            else{
                                //fukuroDB->setLocalization(false);
                                //fukuroDB->setManualPositioning(false);
                            }

                            if(read.robotstatus().ismanualcontrol()){
                            //if(read.robotstatus().has_vx_desired()){
                                auto vx_desired = read.robotstatus().vx_desired();
                                fukuroDB->setManualPositioning(4,vx_desired);
                                fukuroDB->setManualPositioning(true);
                            //}
                            //else{
                            //    fukuroDB->setManualPositioning(false);
                            //}

                            //if(read.robotstatus().has_vy_desired()){
                                auto vy_desired = read.robotstatus().vy_desired();
                                fukuroDB->setManualPositioning(5,vy_desired);
                                //fukuroDB->setManualPositioning(true);
                            //}

                            //if(read.robotstatus().has_w_desired()){
                                auto w_desired = read.robotstatus().w_desired();
                                fukuroDB->setManualPositioning(6,w_desired);
                                //fukuroDB->setManualPositioning(true);
                            }
                            else{
                                fukuroDB->setManualPositioning(false);
                            }

                            isRunning?fukuroDB->insert(1,1.0):fukuroDB->insert(1,0.0);
                            isCoaching?fukuroDB->insert(18,1.0):fukuroDB->insert(18,0.0);
                            isrobotVisible?fukuroDB->insert(17,1.0):fukuroDB->insert(17,0.0);

                            if(read.robotstatus().has_fukuro1_x()){
                                auto x = read.robotstatus().fukuro1_x();
                                auto y = read.robotstatus().fukuro1_y();
                                auto wrad = read.robotstatus().fukuro1_wrad();
                                auto wdeg = read.robotstatus().fukuro1_wdeg();
                                fukuroDB->insert(24,x);
                                fukuroDB->insert(25,y);
                                fukuroDB->insert(26,wrad);
                                fukuroDB->insert(27,wdeg);
                                fukuroDB->setAvailability(0,true);
                            }
                            else if(fukuro!=0){
                                fukuroDB->setAvailability(0,false);
                            }
                            if(read.robotstatus().has_fukuro2_x()){
                                auto x = read.robotstatus().fukuro2_x();
                                auto y = read.robotstatus().fukuro2_y();
                                auto wrad = read.robotstatus().fukuro2_wrad();
                                auto wdeg = read.robotstatus().fukuro2_wdeg();
                                fukuroDB->insert(28,x);
                                fukuroDB->insert(29,y);
                                fukuroDB->insert(30,wrad);
                                fukuroDB->insert(31,wdeg);
                                fukuroDB->setAvailability(1,true);
                            }
                            else if(fukuro!=1){
                                fukuroDB->setAvailability(1,false);
                            }
                            if(read.robotstatus().has_fukuro3_x()){
                                auto x = read.robotstatus().fukuro3_x();
                                auto y = read.robotstatus().fukuro3_y();
                                auto wrad = read.robotstatus().fukuro3_wrad();
                                auto wdeg = read.robotstatus().fukuro3_wdeg();
                                fukuroDB->insert(32,x);
                                fukuroDB->insert(33,y);
                                fukuroDB->insert(34,wrad);
                                fukuroDB->insert(35,wdeg);
                                fukuroDB->setAvailability(2,true);
                            }
                            else if(fukuro!=2){
                                fukuroDB->setAvailability(2,false);
                            }
                            if(read.robotstatus().has_fukuro4_x()){
                                auto x = read.robotstatus().fukuro4_x();
                                auto y = read.robotstatus().fukuro4_y();
                                auto wrad = read.robotstatus().fukuro4_wrad();
                                auto wdeg = read.robotstatus().fukuro4_wdeg();
                                fukuroDB->insert(36,x);
                                fukuroDB->insert(37,y);
                                fukuroDB->insert(38,wrad);
                                fukuroDB->insert(39,wdeg);
                                fukuroDB->setAvailability(3,true);
                            }
                            else if(fukuro!=3){
                                fukuroDB->setAvailability(3,false);
                            }
                            if(read.robotstatus().has_fukuro5_x()){
                                auto x = read.robotstatus().fukuro5_x();
                                auto y = read.robotstatus().fukuro5_y();
                                auto wrad = read.robotstatus().fukuro5_wrad();
                                auto wdeg = read.robotstatus().fukuro5_wdeg();
                                fukuroDB->insert(40,x);
                                fukuroDB->insert(41,y);
                                fukuroDB->insert(42,wrad);
                                fukuroDB->insert(43,wdeg);
                                fukuroDB->setAvailability(4,true);
                            }
                            else if(fukuro!=4){
                                fukuroDB->setAvailability(4,false);
                            }
                            if(read.robotstatus().has_fukuro6_x()){
                                auto x = read.robotstatus().fukuro6_x();
                                auto y = read.robotstatus().fukuro6_y();
                                auto wrad = read.robotstatus().fukuro6_wrad();
                                auto wdeg = read.robotstatus().fukuro6_wdeg();
                                fukuroDB->insert(44,x);
                                fukuroDB->insert(45,y);
                                fukuroDB->insert(46,wrad);
                                fukuroDB->insert(47,wdeg);
                                fukuroDB->setAvailability(5,true);
                            }
                            else if(fukuro!=5){
                                fukuroDB->setAvailability(5,false);
                            }


                            switch ((int)behavior) {
                            //Robot Stop Can Receive Manual Command
                            case 0:{

                                fukuroDB->insert(16,0.0);

                                emit databaseChanged(0);

                            }
                                break;
                            case 1:{

                                if((int)fukuroDB->search(16)!=1)
                                    emit databaseChanged(1);
                                fukuroDB->insert(16,1.0);

                            }
                                break;
                            case 2:{

                                emit databaseChanged(2);
                                fukuroDB->insert(16,2.0);
                            }
                                break;
                            default:
                                break;
                            }

                            switch ((int)role) {
                            case 0:{

                                fukuroDB->insert(15,0.0);
                            }
                                break;
                            case 1:{

                                fukuroDB->insert(15,1.0);
                            }
                                break;
                            case 2:{

                                fukuroDB->insert(15,2.0);
                            }
                                break;
                            case 3:{

                                fukuroDB->insert(15,3.0);
                            }
                                break;
                            case 4:{

                                fukuroDB->insert(15,4.0);
                            }
                                break;
                            case 5:{

                                fukuroDB->insert(15,5.0);

                            }
                                break;
                            case 6:{

                                fukuroDB->insert(15,6.0);

                            }
                                break;
                            case 7:{

                                fukuroDB->insert(15,7.0);

                            }
                                break;
                            case 8:{

                                fukuroDB->insert(15,8.0);

                            }
                                break;
                            case 9:{

                                fukuroDB->insert(15,9.0);

                            }
                                break;
                            case 10:{

                                fukuroDB->insert(15,10.0);

                            }
                                break;
                            default:
                                break;
                            }

                            switch((int)teamcolor){
                            case 0:{

                                fukuroDB->insert(19,0.0);
                                break;
                            }
                            case 1:{

                                fukuroDB->insert(19,1.0);
                                break;
                            }
                            }
                        }
                        else{

                            continue;
                        }
                    }

                }
                tex.unlock();
            }
        } //endif boundstate checking

        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    socket->close();
}
