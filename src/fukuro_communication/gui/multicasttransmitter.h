#ifndef MULTICASTTRANSMITTER_H
#define MULTICASTTRANSMITTER_H

#include "fukurodb.h"

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::duration<double,std::milli> Duration;

class MulticastTransmitter : public QObject
{
    Q_OBJECT

public:
    MulticastTransmitter();
    ~MulticastTransmitter();
    void loop();
    void start(){
        running = true;
        thread = std::thread(&MulticastTransmitter::loop,this);
    }
    void stop(){
        running = false;
        if(thread.joinable())
            thread.join();
    }
    void setFukuroDB(FukuroDB *db){fukuroDB=db;}

public slots:
    void setPort(quint16 port){ this->port = port; }
    void setAddress(std::string address){ this->address = address; }
    void setConnection(bool status){ editflag = status; }

private:
    double xrobot,yrobot,wrobot_deg,wrobot_rad,xball,yball,battery12v,battery24v,xvrobot,yvrobot;
    std::atomic_bool isballVisible,iskickerC;
    std::string address;
    quint16 port;
    std::atomic_bool running;
    std::atomic_bool editflag;
    std::atomic_bool lastEditFlag;
    std::thread thread;
    std::atomic_int fukuro;
    std::mutex tex;
    FukuroDB *fukuroDB;
};
#endif // MULTICASTTRANSMITTER_H
