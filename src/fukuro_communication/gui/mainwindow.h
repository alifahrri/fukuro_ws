#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "fukurodb.h"

typedef std::function<void(quint16)> TransmitterPortUpdatedCallback;
typedef std::function<void(quint16)> ReceiverPortUpdatedCallback;
typedef std::function<void(std::string)> AddressUpdatedCallback;
typedef std::function<void(bool)> ConnectCallback;
typedef std::function<void(fukuro_common::Teammates)> PublisherCallback;
#ifdef USE_DEPRECATED
typedef std::function<void(fukuro_common::Communication)> CommunicationCallback;
#endif
typedef std::function<void(fukuro_common::HWControllerManual)> HWControllerCallback;

namespace Ui {
class MainWindow;
}

enum ConnectionStatus{DISCONNECTED,CONNECTED};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setReceiverCallback(ReceiverPortUpdatedCallback cb){rx_cb = cb;}
    void setTransmitterCallback(TransmitterPortUpdatedCallback cb){tx_cb = cb;}
    void setAddressCallback(AddressUpdatedCallback cb){addr_cb = cb;}
    void setConnectCallback(ConnectCallback cb){ connectcb = cb;}
    void setPublisherCallback(PublisherCallback cb){ pub_cb = cb;}
#ifdef USE_DEPRECATED
    void setPublisherCallback(CommunicationCallback cb){ com_cb = cb;}
#endif
    void setPublisherCallback(HWControllerCallback cb){ hw_cb = cb;}
    void walk();
    void setFukuroDB(FukuroDB *db){ fukuroDB = db;}
    void connectButton(std::string address, int tx_port, int rx_port);

public slots:
    void updateInfoGUI();

private:
    ConnectionStatus isConnected;
    Ui::MainWindow *ui;
    TransmitterPortUpdatedCallback tx_cb;
    ReceiverPortUpdatedCallback rx_cb;
    AddressUpdatedCallback addr_cb;
    std::string address;
    quint16 tx_port,rx_port;
    ConnectCallback connectcb;
    FukuroDB *fukuroDB;
    QTimer *timer;
    QGraphicsScene *robotscene;
    RobotItem *robot;
    std::mutex tex;
    PublisherCallback pub_cb;
#ifdef USE_DEPRECATED
    CommunicationCallback com_cb;
#endif
    HWControllerCallback hw_cb;
    std::vector<geometry_msgs::Pose2D> teammates_pose;
    geometry_msgs::Pose2D teamate_pose;
    fukuro_common::Teammates teammate_msg;
    fukuro_common::Communication communication_msg;
    fukuro_common::HWControllerManual hw_msg;
    std::vector<uint8_t> isAvailable;
};

#endif // MAINWINDOW_H
