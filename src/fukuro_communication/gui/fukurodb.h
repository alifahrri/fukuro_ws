#ifndef FUKURODB_H
#define FUKURODB_H

#include <QDialog>
#include <QGraphicsProxyWidget>
#include <QHash>
#include <QHostAddress>
#include <QMainWindow>
#include <QObject>
#include <QtNetwork>
#include <QTimer>
#include <QUdpSocket>

#include <atomic>
#include <bitset>
#include <chrono>
#include <csignal>
#include <functional>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

#include <ros/ros.h>

#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>

#include <fukuro_common/Communication.h>
#include <fukuro_common/CommunicationService.h>
#include <fukuro_common/HWControllerManual.h>
#include <fukuro_common/LocalizationService.h>
#include <fukuro_common/OdometryInfo.h>
#include <fukuro_common/StrategyService.h>
#include <fukuro_common/Teammates.h>
#include <fukuro_common/WorldModel.h>
#include "fukuro_common/OdometryService.h"

#include "message/multicast_message_read.pb.h"
#include "message/multicast_message_send.pb.h"
#include "robotitem.h"

#define AGENT 3

#if 1 // 0 : manual connect, 1 : autoconnect
#define NO_GUI //autoconnect
#else
#define USE_GUI //manual connect
#endif

//#define USE_DEPRECATED

typedef std::unordered_map<int,double> HashTable;
typedef std::bitset<6> Available;
typedef std::vector<std::pair<double,double>> Obstacles;

const std::string default_address = "224.16.32.90";

class FukuroDB
{
public:
    FukuroDB();
    inline void insert(int key, double data){hashtable[key]=data;}
    inline double search(int key){return hashtable[key];}
    inline void setAvailability(int bit,bool condition){ condition?available[bit]=1:available[bit]=0; }
    inline int getAvailability(int bit){return available[bit];}
    inline void setManualPositioning(int idx,double data){ manualpos[idx] = data; }
    inline double getManualPositioning(int idx){ return manualpos[idx]; }
    inline bool isManualPositioning() { return manualpositioning; }
    inline void setManualPositioning(bool status){ manualpositioning = status; }
    inline void setLocalization(bool status){ isLocalization=status; }
    inline bool getLocalization() {return isLocalization;}
    inline void setObstacles(Obstacles obs){obstacles = obs;}
    inline Obstacles getObstacles(){
        auto ret = obstacles;
        obstacles.clear();
        return ret;
    }

private:
    bool isLocalization;
    HashTable hashtable;
    Available available;
    std::vector<double> manualpos;
    bool manualpositioning;
    Obstacles obstacles;

};

#endif // FUKURODB_H
