#include "multicasttransmitter.h"

MulticastTransmitter::MulticastTransmitter():
    fukuro(0), fukuroDB(new FukuroDB), editflag(false), lastEditFlag(false)
{

    xvrobot = yvrobot = xrobot = yrobot = wrobot_deg = wrobot_rad = xball = yball = battery12v = battery24v = 0.0;
    isballVisible = iskickerC = false;

}

//inline
void MulticastTransmitter::loop(){

    BasestationRead send;
    QByteArray data;
    QUdpSocket *sendsocket;

    sendsocket = new QUdpSocket;

    fukuro = fukuroDB->search(0);
    while(running){
        //tex.lock();
        xrobot = fukuroDB->search(2);
        yrobot = fukuroDB->search(3);
        xvrobot = fukuroDB->search(4);
        yvrobot = fukuroDB->search(5);
        wrobot_deg = fukuroDB->search(6);
        wrobot_rad = fukuroDB->search(7);
        xball = fukuroDB->search(8);
        yball = fukuroDB->search(9);
        isballVisible = (int)fukuroDB->search(14)==1?true:false;

        if(lastEditFlag!=editflag)
            if(editflag){
                sendsocket->bind(QHostAddress::AnyIPv4,port,QUdpSocket::ShareAddress|QUdpSocket::ReuseAddressHint);
                sendsocket->joinMulticastGroup(QHostAddress(QString::fromStdString(address)));
                lastEditFlag = true;
            }
            else{
                if(sendsocket->state()==QUdpSocket::BoundState)
                    sendsocket->leaveMulticastGroup(QHostAddress(QString::fromStdString(address)));
                sendsocket->close();
                lastEditFlag = false;
            }

        if(sendsocket->state()==QUdpSocket::BoundState){
            switch(fukuro+1){
            case 1:{
                send.mutable_robotstatus()->set_robotname(BasestationRead_RobotStatus_RobotName_FUKURO1);
                break;
            }
            case 2:{
                send.mutable_robotstatus()->set_robotname(BasestationRead_RobotStatus_RobotName_FUKURO2);
                break;
            }
            case 3:{
                send.mutable_robotstatus()->set_robotname(BasestationRead_RobotStatus_RobotName_FUKURO3);
                break;
            }
            case 4:{
                send.mutable_robotstatus()->set_robotname(BasestationRead_RobotStatus_RobotName_FUKURO4);
                break;
            }
            case 5:{
                send.mutable_robotstatus()->set_robotname(BasestationRead_RobotStatus_RobotName_FUKURO5);
                break;
            }
            case 6:{
                send.mutable_robotstatus()->set_robotname(BasestationRead_RobotStatus_RobotName_FUKURO6);
            }
            }

            //send.mutable_robotstatus()->set_robotname();
            tex.lock();
            send.mutable_robotstatus()->mutable_robotinfo()->set_xpos(xrobot);
            send.mutable_robotstatus()->mutable_robotinfo()->set_ypos(yrobot);
            send.mutable_robotstatus()->mutable_robotinfo()->set_xvel(xvrobot);
            send.mutable_robotstatus()->mutable_robotinfo()->set_yvel(yvrobot);
            send.mutable_robotstatus()->mutable_robotinfo()->set_degree(wrobot_deg);
            send.mutable_robotstatus()->mutable_robotinfo()->set_radian(wrobot_rad);
            send.mutable_robotstatus()->mutable_ballstatus()->set_xpos(xball);
            send.mutable_robotstatus()->mutable_ballstatus()->set_ypos(yball);
            send.mutable_robotstatus()->mutable_ballstatus()->set_isvisible(isballVisible);
            send.mutable_robotstatus()->mutable_batteryinfo()->set_battery12v(battery12v);
            send.mutable_robotstatus()->mutable_batteryinfo()->set_battery24v(battery24v);
            send.mutable_robotstatus()->set_iskickerc(iskickerC);
            /*
            auto obstacles = fukuroDB->getObstacles();
            for(int i = 0; i < obstacles.size(); i++){
                send.mutable_robotstatus()->mutable_obstacleinfo()->set_x(i,obstacles[i].first);
                send.mutable_robotstatus()->mutable_obstacleinfo()->set_y(i,obstacles[i].second);
            }
            */
            tex.unlock();

            data.resize(send.ByteSize());
            send.SerializeToArray(data.data(),data.size());
            sendsocket->writeDatagram(data.data(),data.size(),QHostAddress(QString::fromStdString(address)),port);
            //ROS_WARN("DATAGRAM HAS BEEN SENT");
            //tex.unlock();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    sendsocket->close();
}

MulticastTransmitter::~MulticastTransmitter()
{

}
