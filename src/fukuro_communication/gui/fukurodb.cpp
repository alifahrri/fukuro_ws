#include "fukurodb.h"

FukuroDB::FukuroDB()
{
    //List GUI
    //0: Robot Name
    //1: Running
    //2: Robot Position X
    //3: Robot Position Y
    //4: Robot Velocity X
    //5: Robot Velocity Y
    //6: Robot Orientation (Degree)
    //7: Robot Orientation (Radian)
    //8: Ball Position X
    //9: Ball Position Y
    //10: Ball Velocity X
    //11: Ball Velocity Y
    //12: isEngaged
    //13: isOWn
    //14: isVisible
    //15: Role
    //16: Behavior
    //17: Robot Visible
    //18: Coaching Mode
    //19: Team Color
    //20: Game State
    //21: 12 Volt
    //22: 24 Volt
    //23: Kicker
    //24: Fukuro1 X
    //25: Fukuro1 Y
    //26: Fukuro1 W Radian
    //27: Fukuro1 W Degree
    //28: Fukuro2 X
    //29: Fukuro2 Y
    //30: Fukuro2 W Radian
    //31: Fukuro2 W Degree
    //32: Fukuro3 X
    //33: Fukuro3 Y
    //34: Fukuro3 W Radian
    //35: Fukuro3 W Degree
    //36: Fukuro4 X
    //37: Fukuro4 Y
    //38: Fukuro4 W Radian
    //39: Fukuro4 W Degree
    //40: Fukuro5 X
    //41: Fukuro5 Y
    //42: Fukuro5 W Radian
    //43: Fukuro5 W Degree
    //44: Fukuro6 X
    //45: Fukuro6 Y
    //46: Fukuro6 W Radian
    //47: Fukuro6 W Degree

    manualpositioning = false;
    isLocalization = false;

    manualpos.resize(7,0.0);

    for(int i=0; i<48; i++)
        hashtable[i] = 0.0;
}
