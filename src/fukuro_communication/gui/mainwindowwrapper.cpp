#include "mainwindowwrapper.h"

MainWindowWrapper::MainWindowWrapper()
{
    connect(this,SIGNAL(ConnectStatusChanged(bool)),this,SLOT(updateConnection(bool)));
}

void MainWindowWrapper::updateConnection(bool status){
    connection_status = status;
}
