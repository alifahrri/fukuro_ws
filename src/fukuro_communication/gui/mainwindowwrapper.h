#ifndef MAINWINDOWWRAPPER_H
#define MAINWINDOWWRAPPER_H

#include "fukurodb.h"

class MainWindowWrapper : public QObject
{
    Q_OBJECT
public:
    MainWindowWrapper();
    bool isConnected(){
        return connection_status;
    }

signals:
    void ReceiverPortChanged(quint16 port);
    void AddressChanged(std::string address);
    void TransmitterPortChanged(quint16 port);
    void ConnectStatusChanged(bool status);
    void TeammatesPublished(fukuro_common::Teammates msg);
    void CommunicationPublished(fukuro_common::Communication msg);
    void HWControllerPublished(fukuro_common::HWControllerManual msg);

public slots:
    void updateConnection(bool status);

private:
    bool connection_status;

};

#endif // MAINWINDOWWRAPPER_H
