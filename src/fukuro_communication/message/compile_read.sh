#PATH=$(pwd)
#protoc -I=/home/reshalfahsi/fukuro_ws/src/fukuro_communication/message --cpp_out=/home/reshalfahsi/fukuro_ws/src/fukuro_communication/message /home/reshalfahsi/fukuro_ws/src/fukuro_communication/message/multicast_message_read.proto

#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$(readlink -f "$0")" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$(readlink -f "$0")" )" && pwd )"

protoc -I=${DIR} --cpp_out=${DIR} ${DIR}/multicast_message_read.proto
