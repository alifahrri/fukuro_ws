#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include "fukurodb.h"

class Communication
{
public:
    Communication(ros::NodeHandle &node);
    void setFukuroDB(FukuroDB *db){ fukurodb=db; }
    std::string getAddress(){return address;}
    quint16 getTransmitterPort(){return tx_port;}
    quint16 getReceiverPort(){return rx_port;}
    void process();

private:
    FukuroDB *fukurodb;
    std::string address;
    quint16 tx_port,rx_port;
    fukuro_common::Teammates teammate_msg;
    fukuro_common::Communication communication_msg;
    fukuro_common::HWControllerManual hw_msg;
    ros::Publisher teammates_pub;
    ros::Publisher com_pub;
    ros::Publisher hw_pub;
    geometry_msgs::Pose2D teamate_pose;
    ros::NodeHandle &nh;
    std::string robot_name;
    std::vector<geometry_msgs::Pose2D> teammates_pose;
    std::vector<uint8_t> isAvailable;
};

#endif // COMMUNICATION_H
