//                                   بِسْــــــــــــــــــمِ اللهِ الرَّحْمَنِ الرَّحِيْمِ

#include "multicastreceiver.h"
#include "multicasttransmitter.h"
#include "mainwindowwrapper.h"

#ifdef NO_GUI
#include "communication.h"
#endif

#ifdef USE_GUI
#include "mainwindow.h"
#include <QApplication>
QApplication *app = NULL;
#endif


typedef boost::function<void(const fukuro_common::WorldModel::ConstPtr&)> FukuroWorldModelCallback;
typedef boost::function<void(const fukuro_common::OdometryInfo::ConstPtr&)> OdometryInfoCallback;

#ifdef USE_DEPRECATED
typedef boost::function<bool(fukuro_common::CommunicationService::Request&,fukuro_common::CommunicationService::Response&)> CommunicationServiceCallback;
#endif

int ret = 0;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "fukuro_communication_node");
    FukuroDB *fukuroDB;
    fukuroDB = new FukuroDB;
    ros::NodeHandle nh;

#ifdef USE_GUI
    QApplication a(argc,argv);
    app = &a;
    MainWindow w;
    MainWindowWrapper mw;
    w.setFukuroDB(fukuroDB);
#endif

#ifdef NO_GUI
    Communication com(nh);
    com.setFukuroDB(fukuroDB);
#endif

    MulticastReceiver rx;
    MulticastTransmitter tx;

    tx.setFukuroDB(fukuroDB);
    rx.setFukuroDB(fukuroDB);

#ifdef NO_GUI
    rx.setAddress(com.getAddress());
    rx.setPort(com.getReceiverPort());
    rx.setConnection(true);
    tx.setAddress(com.getAddress());
    tx.setPort(com.getTransmitterPort());
    tx.setConnection(true);
#endif

    std::string robot_name("fukuro1");

    char* env_robot_name = std::getenv("FUKURO");
    if(env_robot_name==NULL)
    {
        ROS_ERROR("robot name is NULL!!!");
        exit(-1);
    }


    robot_name = std::string(env_robot_name);

    std::string robot_name_copy(robot_name);
    std::reverse(robot_name_copy.begin(),robot_name_copy.end());
    fukuroDB->insert(0,std::atoi(robot_name_copy.c_str())-1);

#ifdef USE_GUI

#ifdef USE_DEPRECATED
    CommunicationServiceCallback com_service_callback = [&](fukuro_common::CommunicationService::Request& req,fukuro_common::CommunicationService::Response& res)
    {
        if(req.connect){
            w.connectButton(req.address,req.tx_port,req.rx_port);
            res.success = true;
        }
        else{
            w.connectButton(req.address,req.tx_port,req.rx_port);
            res.success = true;
        }
        return true;
    };

    ros::ServiceServer main_gui_communication = nh.advertiseService(robot_name+std::string("/communication_service"),com_service_callback);
#endif

    ros::Publisher teammates_pub = nh.advertise<fukuro_common::Teammates>(robot_name+std::string("/teammates"),1);
    ros::Publisher communication_pub = nh.advertise<fukuro_common::Communication>(robot_name+std::string("/communication"),1);
    ros::Publisher vel_cmd_manual_pub = nh.advertise<fukuro_common::HWControllerManual>(robot_name+std::string("/vel_cmd_manual"),1);
#endif


    FukuroWorldModelCallback wm_callback = [&](const fukuro_common::WorldModel::ConstPtr& wm){
        fukuroDB->insert(2,wm->pose.x);
        fukuroDB->insert(3,wm->pose.y);
        fukuroDB->insert(6,wm->pose.theta*57.2957795131);
        fukuroDB->insert(7,wm->pose.theta);
        fukuroDB->insert(8,wm->global_balls_kf.x);
        fukuroDB->insert(9,wm->global_balls_kf.y);
        fukuroDB->insert(14,wm->ball_visible?1.0:0.0);
        Obstacles obstacles;
        for(auto obstacle : wm->obstacles)
            obstacles.push_back(std::make_pair(obstacle.x,obstacle.y));
        fukuroDB->setObstacles(obstacles);
    };
    OdometryInfoCallback odominfo_callback = [&](const fukuro_common::OdometryInfo::ConstPtr& odominfo){
        fukuroDB->insert(4,odominfo->Vx);
        fukuroDB->insert(5,odominfo->Vy);
    };


    ros::Subscriber worldmodel = nh.subscribe(robot_name+std::string("/world_model"),50,wm_callback);
    ros::Subscriber odometryinfo = nh.subscribe(robot_name+std::string("/odometry_info"),10,odominfo_callback);

#ifdef USE_GUI
    tx.connect(&mw,SIGNAL(AddressChanged(std::string)),&tx,SLOT(setAddress(std::string)));
    rx.connect(&mw,SIGNAL(AddressChanged(std::string)),&rx,SLOT(setAddress(std::string)));
    w.setAddressCallback([&mw](std::string address){
        return mw.AddressChanged(address);
    });

    rx.connect(&mw,SIGNAL(ReceiverPortChanged(quint16)),&rx,SLOT(setPort(quint16)));
    w.setReceiverCallback([&mw](quint16 port){
        return mw.ReceiverPortChanged(port);
    });

    tx.connect(&mw,SIGNAL(TransmitterPortChanged(quint16)),&tx,SLOT(setPort(quint16)));
    w.setTransmitterCallback([&mw](quint16 port){
        return mw.TransmitterPortChanged(port);
    });

    rx.connect(&mw,SIGNAL(ConnectStatusChanged(bool)),&rx,SLOT(setConnection(bool)));
    tx.connect(&mw,SIGNAL(ConnectStatusChanged(bool)),&tx,SLOT(setConnection(bool)));
    w.setConnectCallback([&mw](bool status){
        return mw.ConnectStatusChanged(status);
    });

    QObject::connect(&mw,&MainWindowWrapper::TeammatesPublished,[&](fukuro_common::Teammates msg){
        teammates_pub.publish(msg);
    });
    w.setPublisherCallback([&mw](fukuro_common::Teammates msg){
        return mw.TeammatesPublished(msg);
    });

#ifdef USE_DEPRECATED
    QObject::connect(&mw,&MainWindowWrapper::CommunicationPublished,[&](fukuro_common::Communication msg){
        communication_pub.publish(msg);
    });
    w.setPublisherCallback([&mw](fukuro_common::Communication msg){
        return mw.CommunicationPublished(msg);
    });
#endif

    QObject::connect(&mw,&MainWindowWrapper::HWControllerPublished,[&](fukuro_common::HWControllerManual msg){
        vel_cmd_manual_pub.publish(msg);
    });
    w.setPublisherCallback([&mw](fukuro_common::HWControllerManual msg){
        return mw.HWControllerPublished(msg);
    });
#endif



    std::string role;
    ros::ServiceClient strategyservice = nh.serviceClient<fukuro_common::StrategyService>(robot_name+std::string("/strategy_service"));
    fukuro_common::StrategyService srv;

    ros::ServiceClient reset_localization = nh.serviceClient<fukuro_common::LocalizationService>("/localization_service");
    ros::ServiceClient reset_odometry = nh.serviceClient<fukuro_common::OdometryService>("/odometry_service");
    fukuro_common::LocalizationService localization;
    fukuro_common::OdometryService odometry;

    double x,y,theta;
    rx.connect(&rx,&MulticastReceiver::databaseChanged,[&](int idx){
#ifdef USE_GUI
        if(mw.isConnected()){
#endif
#ifdef NO_GUI
            if(true){
#endif
                if(idx==0){ /* GAME STATE : POSITIONING */

                    if(fukuroDB->getLocalization()){
                        localization.request.x = x*100;
                        localization.request.y = y*100;
                        localization.request.w = (theta < 0.0 ? (theta+2*M_PI) : theta)*180.0/M_PI;
                        localization.request.initial_pos = 1;

                        if(reset_localization.call(localization))
                            if(localization.response.ok)
                                ROS_INFO("Reset Localization Service was Called!!!");
                            else
                                ROS_WARN("Reset Localization Service Call Failed!!!");

                        odometry.request.x = x;
                        odometry.request.y = y;
                        odometry.request.w = (theta < 0.0 ? (theta+2*M_PI) : theta)*180.0/M_PI;
                        //odometry.request.initial_pos = 1;

                        if(reset_odometry.call(odometry))
                            if(odometry.response.ok)
                                ROS_INFO("Reset Odometry Service was Called!!!");
                            else
                                ROS_WARN("Reset Odometry Service Call Failed!!!");

                    }

                    if(fukuroDB->isManualPositioning()){
                        x = fukuroDB->getManualPositioning(0);
                        y = fukuroDB->getManualPositioning(1);
                        theta = fukuroDB->getManualPositioning(2);
                        /*
                        if(fukuroDB->getLocalization()){
                            localization.request.x = x*100;
                            localization.request.y = y*100;
                            localization.request.w = (theta < 0.0 ? (theta+2*M_PI) : theta)*180.0/M_PI;
                            localization.request.initial_pos = 1;

                            if(reset_localization.call(localization))
                                if(localization.response.ok)
                                    ROS_INFO("Reset Localization Service was Called!!!");
                                else
                                    ROS_WARN("Reset Localization Service Call Failed!!!");
                        }
                    */
                    }
                    else{
                        srv.request.strategy_state = "timer";
                        if(strategyservice.call(srv))
                            if(srv.response.ok)
                                ROS_INFO("Strategy Service was Called!!!");
                            else
                                ROS_WARN("Strategy Service Call Failed!!!");
                    }
                }
                else if(idx==1){ /* GAME STATE : GAMEPLAY */
                    if((int)fukuroDB->search(15)!=0)
                        switch ((int)fukuroDB->search(15)) {
                        //case 0:{
                        //role = "defender";
                        //}
                        //    break;
                        case 1:{
                            role = "defender";
                        }
                            break;
                        case 2:{
                            role = "defender2";
                        }
                            break;
                        case 3:{
                            role = "timer_defense";
                        }
                            break;
                        case 4:{
                            role = "timer_defense2";
                        }
                            break;
                        case 5:{
                            role = "striker";
                        }
                            break;
                        case 6:{
                            role = "striker_kick";
                        }
                            break;
                        case 7:{
                            role = "striker_dribble";
                        }
                            break;
                        case 8:{
                            role = "goalie";
                        }
                            break;
                        case 9:{
                            role = "timer_striker";
                        }
                            break;
                        case 10:{
                            role = "timer_striker2";
                        }
                            break;
                        default:
                            break;
                        }
                    srv.request.role = role;
                    if(strategyservice.call(srv))
                        if(srv.response.ok)
                            ROS_INFO("Strategy Service was Called!!!");
                        else
                            ROS_WARN("Strategy Service Call Failed!!!");
                }
                if(idx==2){ /* GAME STATE : GAMEPLAY */
                    srv.request.strategy_state = "stop";
                    if(strategyservice.call(srv))
                        if(srv.response.ok)
                            ROS_INFO("Strategy Service was Called!!!");
                        else
                            ROS_WARN("Strategy Service Call Failed!!!");

                }
            }
        });

#ifdef USE_GUI
        w.walk(); //signal and slot
#endif

        rx.start();
        tx.start(); //threading
        ros::AsyncSpinner spinner(4);
        if(spinner.canStart())
            spinner.start();

#ifdef NO_GUI
        ros::Rate loop(33);
        while(ros::ok()){
            com.process();
            loop.sleep();
        }
        ros::waitForShutdown();
#endif


#ifdef USE_GUI
        w.show();
        ret = a.exec();
#endif

        rx.stop();
        tx.stop();

        return ret;
    }
