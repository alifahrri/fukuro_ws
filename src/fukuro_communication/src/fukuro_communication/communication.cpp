#include "communication.h"

Communication::Communication(ros::NodeHandle &node) :
    fukurodb(new FukuroDB),
    nh(node)
{
    int agent = std::atoi(std::getenv("AGENT"));
    tx_port = 23898;

    switch (agent) {
    case 1:{
        rx_port = 10599;
        break;
    }
    case 2:{
        rx_port = 16180;
        break;
    }
    case 3:{
        rx_port = 31415;
        break;
    }
    case 4:{
        rx_port = 46692;
        break;
    }
    case 5:{
        rx_port = 10898;
        break;
    }
    case 6:{
        rx_port = 13080;
        break;
    }
    }


    //default Multicast IPv4 for fukuro
    address = default_address;

    if(!ros::get_environment_variable(robot_name,"FUKURO"))
    {
        ROS_ERROR("robot name empty!!! export FUKURO=fukuro1");
        exit(-1);
    }

    teammates_pub= nh.advertise<fukuro_common::Teammates>(robot_name+std::string("/teammates"),1);
    com_pub = nh.advertise<fukuro_common::Communication>(robot_name+std::string("/communication"),1);
    hw_pub = nh.advertise<fukuro_common::HWControllerManual>(robot_name+std::string("/vel_cmd_manual"),1);
}

void Communication::process(){

    for(int i = 24; i<(AGENT+6)*4; i+=4){
        int idx = (i/4)-6;
        teamate_pose.x = fukurodb->search(i);
        teamate_pose.y = fukurodb->search(i+1);
        teamate_pose.theta = fukurodb->search(i+2);
        teammates_pose.push_back(teamate_pose);
        (fukurodb->getAvailability(idx))?isAvailable.push_back(true)
                                       :isAvailable.push_back(false);
    }

    teammate_msg.state = (int)fukurodb->search(20);
    teammate_msg.robotname = (int)fukurodb->search(0);
    for(int i=0; i<AGENT; i++){
        teammate_msg.pose.push_back(teammates_pose[i]);
        teammate_msg.available.push_back(isAvailable[i]);
    }
    teammate_msg.behavior = (int)fukurodb->search(16);
    teammate_msg.isConnected = true;
    fukurodb->isManualPositioning()? teammate_msg.isManualPositioning = true : teammate_msg.isManualPositioning = false;

    teammates_pub.publish(teammate_msg);

    auto vx = fukurodb->getManualPositioning(4);
    auto vy = fukurodb->getManualPositioning(5);
    auto w = fukurodb->getManualPositioning(6);
    hw_msg.Vx = vx;
    hw_msg.Vy = vy;
    hw_msg.w = w;
    hw_pub.publish(hw_msg);

    for(int i=0; i<AGENT; i++){
        teammate_msg.pose.pop_back();
        teammate_msg.available.pop_back();
        teammates_pose.pop_back();
    }

}
