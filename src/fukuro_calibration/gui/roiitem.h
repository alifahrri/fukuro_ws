#ifndef ROIITEM_H
#define ROIITEM_H

#include <QGraphicsItem>

class ROIItem : public QGraphicsItem
{
public:
  ROIItem();
  void setROI(QPoint center_, double radius_);
  void setTestPoint(double angle, double radius);
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  QRectF boundingRect() const;
private:
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
private:
  QPoint center;
  double radius;
  QLineF line;
};

#endif // ROIITEM_H
