#include "roiitem.h"
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

ROIItem::ROIItem() :
  center(QPoint(0,0)),
  radius(0.0),
  line(QLineF(0.0,0.0,0.0,0.0))
{

}

void ROIItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  if(radius<=0.0)
    return;
  painter->setPen(Qt::red);
  painter->setBrush(Qt::NoBrush);
  painter->drawEllipse(center.x()-radius,center.y()-radius,radius*2,radius*2);
  painter->setBrush(Qt::red);
  painter->drawEllipse(center.x()-6.0,center.y()-6.0,6.0,6.0);
  painter->drawLine(line);
}

QRectF ROIItem::boundingRect() const
{
  return QRectF(center.x()-radius,center.y()-radius,radius*2,radius*2);
}

void ROIItem::setROI(QPoint center_, double radius_)
{
  center = center_;
  radius = radius_;
}

void ROIItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  line = QLineF(center,event->pos());
}

void ROIItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  line = QLineF(center,event->pos());
}

void ROIItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
  line = QLineF(center,event->pos());
}
