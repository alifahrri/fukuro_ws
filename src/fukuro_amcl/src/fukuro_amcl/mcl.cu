#include "mcl.h"
#include <fstream>
#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cstdlib>
#include <cstdio>
#include <string>

#define REAL_TIME_CMPS_AVAILABLE

#define MAX_THREADS 1024
#define MAX_BLOCK   1024
#define MAX_SENSOR_DATA_THREADS     32
#define MAX_SENSOR_PARTICLE_BLOCKS  32
#define TO_RAD M_PI/180

#define N_PARTICLE 600
#define CENTER_RADIUS 100
#define CAMERA_ANGLE_OFFSET (-M_PI_2)
#define DEFAULT_CMPS_W (1.0)
#define DEFAULT_A_FAST (2.75)
#define DEFAULT_A_SLOW (0.1)
#define MINIMUM_WEIGHT_ANGLE 0.333

#define RESET_SCALE_X 10.0
#define RESET_SCALE_Y 10.0
#define RESET_SCALE_W 5.0

#define RESET_MIN_X (-450.0)
#define RESET_MIN_Y (-300.0)
#define RESET_MIN_W (0.0)
#define RESET_MAX_X (450.0)
#define RESET_MAX_Y (300.0)
#define RESET_MAX_W (360.0)

#define CAMERA_FACTOR 0.985
#define COMPASS_FACTOR (1.0-CAMERA_FACTOR)
#define GRID_SIZE 10

#define XVAR 0.1
#define YVAR 0.1
#define WVAR 0.25

#define CUDA_CALL(x) { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    exit(EXIT_FAILURE);}}
#define CURAND_CALL(x) { if((x)!=CURAND_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    exit(EXIT_FAILURE);}}
#define CHECK_LAUNCH_ERROR()                                          \
    do {                                                              \
    /* Check synchronous errors, i.e. pre-launch */                   \
    cudaError_t err = cudaGetLastError();                             \
    if (cudaSuccess != err) {                                         \
    fprintf (stderr, "Cuda error in file '%s' in line %i : %s.\n",    \
    __FILE__, __LINE__, cudaGetErrorString(err) );                    \
    exit(EXIT_FAILURE);                                               \
    }                                                                 \
    /* Check asynchronous errors, i.e. kernel failed (ULF) */         \
    err = cudaThreadSynchronize();                                    \
    if (cudaSuccess != err) {                                         \
    fprintf (stderr, "Cuda error in file '%s' in line %i : %s.\n",    \
    __FILE__, __LINE__, cudaGetErrorString( err) );                   \
    exit(EXIT_FAILURE);                                               \
    }                                                                 \
    } while (0)

__global__
void init(unsigned int seed, curandState_t *states, int size)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if(i<size)
        curand_init(seed,i,0,&states[i]);
}

__global__
void randoms(curandState_t *states, float *n, int size)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if(i<size)
        n[i] = curand_normal(&states[i]);
}

__global__
void reduce(float *g_idata, float *g_odata, unsigned int n)
{
    extern __shared__ float sdata[];
    unsigned int tidx = threadIdx.x;
    unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;

    sdata[tidx] = (i<n) ? g_idata[i] : 0.0;

    __syncthreads();

    for(unsigned int s=blockDim.x/2; s>0; s>>=1)
    {
        if(tidx < s)
            sdata[tidx] += sdata[tidx+s];
        __syncthreads();
    }
    if(tidx==0)
        g_odata[blockIdx.x] = sdata[0];
}

__global__
void normalize(float *p, float total, unsigned int size)
{
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if(idx < size)
        p[idx] /= total;
}

__global__
void calculateCompassKernel(ParticlesGPU p,double cmps, curandState_t *states)
{
    int index = blockIdx.x*blockDim.x + threadIdx.x;
    if(index<p.size)
    {
        double err = p.w[index] - cmps;
        //        if(p.w[index]>cmps)
        //        {
        //            if((p.w[index]-cmps)<180.0)
        //                err = (p.w[index]-cmps);
        //            else
        //                err = cmps + (360.0-p.w[index]);
        //        }
        //        else
        //        {
        //            if((cmps-p.w[index])<180.0)
        //                err = cmps-p.w[index];
        //            else
        //                err = p.w[index] + (360.0-cmps);
        //        }
        if(err>360)
            err-=360.0;
        if(err<0)
            err+=360.0;
        if(err>180.0)
          err = 360.0-err;
        p.compass[index] = 1.0/(err);
    }
}

__global__
void augmentCompassKernel(ParticlesGPU p, curandState_t *states)
{
    int index = blockIdx.x*blockDim.x + threadIdx.x;
    if(index<p.size)
    {
        float weight = CAMERA_FACTOR * p.weight[index] + COMPASS_FACTOR * p.compass[index];
        p.weight[index] = weight;
    }
}

__global__
void updateMotionKernel(ParticlesGPU p, MotionGPU m, curandState_t *states)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if(i<p.size)
    {
        float rx = curand_normal(&states[i]);
        float ry = curand_normal(&states[i]);
        float rw = curand_normal(&states[i]);
        float c = cos(p.w[i]*TO_RAD);
        float s = sin(p.w[i]*TO_RAD);
        float dx = c*m.vx - s*m.vy;
        float dy = s*m.vx + c*m.vy;
        p.x[i] += dx+rx*XVAR;
        p.y[i] += dy+ry*YVAR;
        p.w[i] += m.w+rw*WVAR;
        while(p.w[i] > 360.0)
            p.w[i] -= 360.0;
        while(p.w[i] < 0.0)
            p.w[i] += 360.0;
    }
}

__global__
void computeDistanceKernel(ParticlesGPU p, SensorGPU s, WeightMapGPU error_map, float* distance)
{
  uint p_idx = blockIdx.x*blockDim.x + threadIdx.x;
  uint s_idx = blockIdx.y*blockDim.y + threadIdx.y;
  if((p_idx < p.size) && (s_idx < s.size)) {
      uint mem_id = p_idx*s.size + s_idx;
      float rsx = s.x[s_idx];
      float rsy = s.y[s_idx];
      float mpx = p.x[p_idx];
      float mpy = p.y[p_idx];
      float mpw = p.w[p_idx];
      float c = cos(mpw*TO_RAD);
      float s = sin(mpw*TO_RAD);
      float wsx = c*rsx - s*rsy + mpx;
      float wsy = s*rsx + c*rsy + mpy;
      float dis = error_map.width;
      //      float dis = 0.0;

      int idx = ((int)(wsy)-error_map.start_y) * error_map.width + (int)(wsx)-error_map.start_x;
      if((fabs(wsx)<error_map.end_x) && (fabs(wsy)<error_map.end_y)) {
          if(idx < error_map.size)
              dis = error_map.weights[idx];
      }
      float pt_distance = rsx*rsx+rsy*rsy;
      if(pt_distance<1)
        pt_distance=1;
      //distance[mem_id] = dis*dis*dis;
      distance[mem_id] = dis*dis*pt_distance;
    }
}

__global__
void computeWeightKernel(ParticlesGPU p, float *distance, uint memsize, uint n_sensor)
{
  uint p_idx = blockIdx.x*blockDim.x + threadIdx.x;
  if(p_idx < p.size) {
      float distance_sum = 0.0;
      for(uint i=0; i<n_sensor; i++) {
          distance_sum += distance[p_idx+i];
        }
      distance_sum /= n_sensor;
      p.weight[p_idx] = 1.0/(distance_sum);
    }
}

__global__
void updateSensorKernel(ParticlesGPU p, SensorGPU s, WeightMapGPU error_map, float* partial_sum, int partial_sum_size)
{
    unsigned int p_idx = blockIdx.y * blockDim.y + threadIdx.y;
    unsigned int tidx = threadIdx.x;
    unsigned int i = blockIdx.x * blockDim.x + tidx;
    unsigned int data_idx = blockIdx.y * blockDim.x + threadIdx.x;
    unsigned int particle_idx = blockIdx.z * gridDim.x + blockIdx.x;
    extern __shared__ float sdata[];

    float rsx = 0.0;
    float rsy = 0.0;
    float mpx = 0.0;
    float mpy = 0.0;
    float mpw = 0.0;
    if((data_idx < s.size) && (particle_idx < p.size))
    {
        rsx = s.x[data_idx];
        rsy = s.y[data_idx];
        mpx = p.x[particle_idx];
        mpy = p.y[particle_idx];
        mpw = p.w[particle_idx];
    }
    float _c = cos(mpw*TO_RAD);
    float _s = sin(mpw*TO_RAD);
    float wsx = _c*rsx - _s*rsy + mpx;
    float wsy = _s*rsx + _c*rsy + mpy;

    float dis = error_map.width;
    int idx = ((int)(wsy)-error_map.start_y) * error_map.width + (int)(wsx)-error_map.start_x;

    if((abs(wsx)<error_map.end_x) && (abs(wsy)<error_map.end_y)) {
        if(idx < error_map.size)
            dis = error_map.weights[idx];
    }

    sdata[tidx] = dis;
    __syncthreads();
    for(unsigned int s = blockDim.x/2; s>0; s>>=1)
    {
        if(tidx < s)
            sdata[tidx] += sdata[tidx+s];
        __syncthreads();
    }
    int sum_idx = blockIdx.z * gridDim.x * gridDim.y + blockIdx.x * gridDim.y + blockIdx.y;
    if(tidx == 0)
        partial_sum[sum_idx] = sdata[0];
}

__global__
void finalSensorReductionKernel(ParticlesGPU p, float *partial_sum, int partial_sum_size, int n_sensor_data)
{
    unsigned int tidx = threadIdx.x;
    unsigned int particle_idx = blockIdx.z * gridDim.x + blockIdx.x;
    int sum_idx = blockIdx.z * gridDim.x * gridDim.y + blockIdx.x * gridDim.y + blockIdx.y + tidx;

    extern __shared__ float sdata[];
    sdata[tidx] = (sum_idx < partial_sum_size ? partial_sum[sum_idx] : 0.0);
    __syncthreads();
    for(unsigned int s=blockDim.x/2; s>0; s>>=1)
    {
        if(tidx < s)
            sdata[tidx] += sdata[tidx+s];
        __syncthreads();
    }
    if((tidx == 0) && (particle_idx < p.size)) {
        p.weight[particle_idx] = 1.0/sdata[0];
        p.weight[particle_idx] /= n_sensor_data;
      }
}

__global__
void resetParticlesKernel(ParticlesGPU p, ParticleGPU min, ParticleGPU max, curandState_t *states)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if(i<p.size)
    {
        float x = curand_uniform(&states[i]);
        float y = curand_uniform(&states[i]);
        float w = curand_uniform(&states[i]);
        float range_x = max.x - min.x;
        float range_y = max.y - min.y;
        float range_w = max.w - min.w;
        p.x[i] = x*range_x + min.x;
        p.y[i] = y*range_y + min.y;
        p.w[i] = w*range_w + min.w;
        p.weight[i] = 1.0/N_PARTICLE;
    }
}

__global__
void resetParticlesNormalKernel(ParticlesGPU p, ParticleGPU shift, ParticleGPU scale, curandState_t *states)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if(i<p.size)
    {
        curandState_t *gen = &states[i];
        float x = curand_normal(gen)*scale.x + shift.x;
        float y = curand_normal(gen)*scale.y + shift.y;
        float w = curand_normal(gen)*scale.w + shift.w;
        p.x[i] = x;
        p.y[i] = y;
        p.w[i] = w;
    }
}

using namespace fukuro;

std::pair<int,int> MCL::getLaunchConfig(int max_thread, int n_particle)
{
    std::pair<int,int> ret;
    ret.first = (int)std::ceil((double)n_particle/(double)max_thread);
    ret.second = (int)(n_particle > max_thread ? max_thread : n_particle);
    return ret;
}

inline
void MCL::initParticles(ParticlesGPU &particles, size_t n)
{
    mutex.lock();
    CUDA_CALL(cudaMalloc((void**)&particles.x, n*sizeof(float)));
    CUDA_CALL(cudaMalloc((void**)&particles.y, n*sizeof(float)));
    CUDA_CALL(cudaMalloc((void**)&particles.w, n*sizeof(float)));
    CUDA_CALL(cudaMalloc((void**)&particles.weight, n*sizeof(float)));
    CUDA_CALL(cudaMalloc((void**)&particles.compass, n*sizeof(float)));
    //    float *initial_weight = (float*)calloc(n, sizeof(float));
    //    for(size_t i=0; i<n; i++)
    //      initial_weight[i] = 1.0/N_PARTICLE;
    //    CUDA_CALL(cudaMemcpy(particles.weight,initial_weight,n*sizeof(float),cudaMemcpyHostToDevice));
    //    free(initial_weight);
    mutex.unlock();
    particles.size = n;
}

MCL::MCL(ros::NodeHandle &nh)
    : node(nh),
      xvar(10), yvar(10), wvar(15),
      cmps(0),
      w_fast(0.0), w_slow(0.0),
      a_fast(2.75), a_slow(0.1),
      a_sensor(1.0/N_PARTICLE),
      camera_angle_offset(CAMERA_ANGLE_OFFSET),
      compass_weight(1.0)
{
    ROS_INFO("MCL CUDA Init");
    initParticles(particles, N_PARTICLE);
    CUDA_CALL(cudaMalloc((void**)&random_states, N_PARTICLE*sizeof(curandState_t)));
    auto n = N_PARTICLE;
    auto launch = getLaunchConfig(MAX_THREADS, N_PARTICLE);
    auto n_block = launch.first;
    auto n_threads = launch.second;
    ROS_INFO("blocks : %d; threads : %d; particles.size : %d", n_block, n_threads, particles.size);
    init<<<n_block,n_threads>>>(time(0),random_states,N_PARTICLE);
    CHECK_LAUNCH_ERROR();

    ParticlesGPU particles_cpu;
    copyParticleToHost(particles_cpu, particles);

    //ParticlesCompass particle_compass;


    for(size_t i=0; i<particles_cpu.size; i++)
    {
        Particle p(particles_cpu.x[i], particles_cpu.y[i], particles_cpu.w[i], 1.0/particles_cpu.size, 1.0/particles_cpu.size);
        particles_host.push_back(p);
    }


    ParticleGPU p_min = {-450.0, -300.0, 0.0, 0.0, 0.0};
    ParticleGPU p_max = {450.0, 300.0, 360.0, 0.0, 0.0};
    resetParticlesKernel<<<n_block,n_threads>>>(particles, p_min, p_max, random_states);
    CHECK_LAUNCH_ERROR();

    // copyFieldToDevice(this->field_host, this->field);
    auto n_matrix = field_host.x_length * field_host.y_length;
    if(!field.weights)
    {
        std::cout << "allocate field on device" << std::endl;
        CUDA_CALL(cudaMalloc((void**)&field.weights, n_matrix*sizeof(float)));
    }
    float *distance_matrix;

    distance_matrix = (float*)calloc(n_matrix, sizeof(float));
    std::cout << "copying field matrix" << std::endl;
    for(size_t i=0; i<n_matrix; i++)
        distance_matrix[i] = (float)field_host.distance_matrix[i];
    copyDataToDevice(distance_matrix, field.weights, n_matrix);
    std::cout << "copying field matrix done" << std::endl;

    std::cout << "checking data" << std::endl;
    float *distance_matrix_cpu;
    distance_matrix_cpu = (float*)calloc(n_matrix, sizeof(float));
    copyDataToHost(distance_matrix_cpu, field.weights, n_matrix);
    for(size_t i=0; i<n_matrix; i++)
        if(distance_matrix_cpu[i] != (float)field_host.distance_matrix[i]) {
            std::cout << "data corrupt : " << distance_matrix_cpu[i] << " != " << distance_matrix[i] << std::endl;
            exit(-1);
        }
    std::cout << "checking data done" << std::endl;

    field.size = n_matrix;
    field.width = field_host.x_length;
    field.height = field_host.y_length;
    field.start_x = field_host.start_x;
    field.start_y = field_host.start_y;
    field.end_x = field_host.end_x;
    field.end_y = field_host.end_y;
    field.stride = field.width;

    double init_x = 0.0;
    double init_y = 0.0;
    double init_w = 0.0;
    if(ros::param::get("/mcl_init_x",init_x))
      if(ros::param::get("/mcl_init_y",init_y))
        if(ros::param::get("/mcl_init_w",init_w)) {
            ROS_INFO("intial pos known, (%.2f,%.2f,%.2f)", init_x, init_y, init_w);
            resetParticles(true, init_x, init_y, init_w);
          }

    last_publish = ros::Time::now();
    localization_pub = node.advertise<fukuro_common::Localization>("/localization", 1);
    global_whites_pub = node.advertise<fukuro_common::Whites>("/localization/global_whites", 1);
}

inline
void MCL::updateCompass(double compass){
    cmps = compass;
}

inline
void MCL::updateMotion(double vx, double vy, double dw)
{
    auto motion = MotionGPU({(float)vx, (float)vy, (float)dw});
    auto launch = getLaunchConfig(MAX_THREADS, N_PARTICLE);
    auto n_block = launch.first;
    auto n_threads = launch.second;
    mutex.lock();
    updateMotionKernel<<<n_block,n_threads>>>(particles, motion, random_states);
    CHECK_LAUNCH_ERROR();
    mutex.unlock();
}

inline
void MCL::calculateCompass(ParticlesGPU &particles, double compass){
    auto launch = getLaunchConfig(MAX_THREADS, N_PARTICLE);
    auto n_block = launch.first;
    auto n_threads = launch.second;
    mutex.lock();
    calculateCompassKernel<<<n_block,n_threads>>>(particles,compass, random_states);
    CHECK_LAUNCH_ERROR();
    mutex.unlock();

}

inline
void MCL::augmentCompass(ParticlesGPU &particles){
    auto launch = getLaunchConfig(MAX_THREADS, N_PARTICLE);
    auto n_block = launch.first;
    auto n_threads = launch.second;
    mutex.lock();
    augmentCompassKernel<<<n_block,n_threads>>>(particles,random_states);
    CHECK_LAUNCH_ERROR();
#if 0
    static bool dumped = false;
    if(!dumped) {
        std::ofstream dump("/home/fahri/cmps_augmented.txt", std::ios::out);
        float* weight = (float*)calloc(particles.size,sizeof(float));
        float* angles = (float*)calloc(particles.size,sizeof(float));
        copyDataToHost(weight, particles.weight, particles.size);
        copyDataToHost(angles, particles.w, particles.size);
        for(size_t i=0; i<particles.size; i++)
          dump << cmps << " "
               << angles[i] << " "
               << weight[i] << std::endl;
        free(weight);
        dumped = true;
      }
#endif
    mutex.unlock();
}

inline
void MCL::updateSensor(SensorGPU &data)
{
  auto n_thread = 32;
  auto n_particle_block = std::ceil((float)N_PARTICLE/n_thread);
  auto n_sensor_block = std::ceil((float)data.size/n_thread);
  dim3 grid(n_particle_block, n_sensor_block);
  dim3 block(n_thread, n_thread);
  size_t n_mem = N_PARTICLE * data.size;
  float *distance;
  SensorGPU gpu_data;
  gpu_data.size = data.size;
  CUDA_CALL(cudaMalloc((void**) &distance, n_mem*sizeof(float)));
  CUDA_CALL(cudaMalloc((void**) &gpu_data.x, data.size*sizeof(float)));
  CUDA_CALL(cudaMalloc((void**) &gpu_data.y, data.size*sizeof(float)));
  copyDataToDevice(data.x, gpu_data.x, data.size);
  copyDataToDevice(data.y, gpu_data.y, data.size);
  mutex.lock();
  ROS_INFO("launch computeDistanceKernel (%d,%d,%d),(%d,%d,%d), n_mem: %d", grid.x, grid.y, grid.z, block.x, block.y, block.z, n_mem);
  computeDistanceKernel<<<grid,block>>>(particles,gpu_data,this->field,distance);
  CHECK_LAUNCH_ERROR();
  grid = dim3(n_particle_block);
  block = dim3(n_thread);
  ROS_INFO("launch computeWeightKernel (%d,%d,%d),(%d,%d,%d)", grid.x, grid.y, grid.z, block.x, block.y, block.z);
  computeWeightKernel<<<grid,block>>>(particles,distance,n_mem,data.size);
  CHECK_LAUNCH_ERROR();
  mutex.unlock();
#if 0
  static bool dumped = false;
  if(!dumped) {
      float *weight_cpu = (float*)calloc(N_PARTICLE, sizeof(float));
      float *distance_cpu = (float*)calloc(n_mem, sizeof(float));
      CUDA_CALL(cudaMemcpy((void*)distance_cpu,(void*)distance,n_mem*sizeof(float),cudaMemcpyDeviceToHost));
      std::ofstream dump("/home/fahri/dump.txt", std::ios::out);
      std::ofstream weight_dump("/home/fahri/particle_weight.txt", std::ios::out);
      std::ofstream sensor_dump("/home/fahri/sensor_data.txt", std::ios::out);
      copyDataToHost(weight_cpu, particles.weight, N_PARTICLE);
      for(size_t i=0; i<data.size; i++)
        sensor_dump << data.x[i] << " " << data.y[i] << std::endl;
      for(size_t i=0; i<n_mem; i++) {
          if(i%data.size == 0)
            dump << "---------------------" << std::endl;
          dump << distance_cpu[i] << std::endl;
        }
      for(size_t i=0; i<N_PARTICLE; i++)
        weight_dump << weight_cpu[i] << std::endl;
      dump.close();
      dumped = true;
      free(distance_cpu);
      free(weight_cpu);
    }
#endif
  CUDA_CALL(cudaFree(distance));
  CUDA_CALL(cudaFree(gpu_data.x));
  CUDA_CALL(cudaFree(gpu_data.y));
}

inline
void MCL::normalizeWeight(ParticlesGPU &particles)
{
    ROS_INFO(" : ");
    auto n_threads = MAX_SENSOR_DATA_THREADS;
    auto n_blocks = std::ceil((double)N_PARTICLE/n_threads);
    auto shared_mem = sizeof(float) * n_threads;
    dim3 Dg(n_blocks);
    dim3 Db(n_threads);

    float *weight_tmp;
    float *weight_tmp_host;
    CUDA_CALL(cudaMalloc((void**)&weight_tmp, sizeof(float)*n_blocks));
    weight_tmp_host = (float*)calloc(n_blocks, sizeof(float));

    // ROS_INFO("1st stage launch<<<(%d,%d,%d),(%d,%d,%d)>>>", Dg.x, Dg.y, Dg.z, Db.x, Db.y, Db.z);
    reduce<<<Dg,Db,shared_mem>>>(particles.weight, weight_tmp, particles.size);
    CHECK_LAUNCH_ERROR();
    // CUDA_CALL(cudaMemcpy((void*)weight_tmp_host, (void*)weight_tmp, n_blocks*sizeof(float), cudaMemcpyDeviceToHost));
    // for(auto i=0; i<n_blocks; i++)
    //   ROS_INFO("weight_tmp[%d] : %f", i, weight_tmp_host[i]);

    Dg.x = 1;
    Db.x = n_threads;
    while(Db.x < n_blocks)
        Db.x += n_threads;
    // ROS_INFO("final stage launch<<<(%d,%d,%d),(%d,%d,%d)>>>", Dg.x, Dg.y, Dg.z, Db.x, Db.y, Db.z);

    float *final_sum;
    float *final_sum_host;
    final_sum_host = (float*)calloc(1, sizeof(float));
    CUDA_CALL(cudaMalloc((void**)&final_sum, sizeof(float)*1));

    shared_mem = sizeof(float) * Db.x;

    reduce<<<Dg,Db,shared_mem>>>(weight_tmp, final_sum, n_blocks);
    CHECK_LAUNCH_ERROR();
    CUDA_CALL(cudaMemcpy((void*)final_sum_host, (void*)final_sum, sizeof(float)*1, cudaMemcpyDeviceToHost));
    // ROS_INFO("final_sum : %f", *final_sum_host);

    // ROS_INFO("normalize");
    Dg.x = n_blocks;
    Db.x = n_threads;
    // ROS_INFO("normalize launch<<<(%d,%d,%d),(%d,%d,%d)>>>", Dg.x, Dg.y, Dg.z, Db.x, Db.y, Db.z);
    normalize<<<Dg,Db>>>(particles.weight, *final_sum_host, particles.size);
    CHECK_LAUNCH_ERROR();

#if 0
    static bool dumped = false;
    if(!dumped) {
        std::ofstream dump("/home/fahri/weight_normalized.txt", std::ios::out);
        float* cmps_weight = (float*)calloc(particles.size,sizeof(float));
        float* angles = (float*)calloc(particles.size,sizeof(float));
        copyDataToHost(cmps_weight, particles.weight, particles.size);
        copyDataToHost(angles, particles.w, particles.size);
        for(size_t i=0; i<particles.size; i++)
          dump << cmps << " "
               << angles[i] << " "
               << cmps_weight[i] << std::endl;
        free(cmps_weight);
        dumped = true;
      }
#endif
    // ROS_INFO("done");
}

inline
void MCL::normalizeCompass(ParticlesGPU &particles)
{
    ROS_INFO(" : ");
    auto n_threads = MAX_SENSOR_DATA_THREADS;
    auto n_blocks = std::ceil((double)N_PARTICLE/n_threads);
    auto shared_mem = sizeof(float) * n_threads;
    dim3 Dg(n_blocks);
    dim3 Db(n_threads);

    float *compass_tmp;
    float *compass_tmp_host;
    CUDA_CALL(cudaMalloc((void**)&compass_tmp, sizeof(float)*n_blocks));
    compass_tmp_host = (float*)calloc(n_blocks, sizeof(float));

    reduce<<<Dg,Db,shared_mem>>>(particles.compass, compass_tmp, particles.size);
    CHECK_LAUNCH_ERROR();

    Dg.x = 1;
    Db.x = n_threads;
    while(Db.x < n_blocks)
        Db.x += n_threads;

    float *compass_sum;
    float *compass_sum_host;
    compass_sum_host = (float*)calloc(1, sizeof(float));
    CUDA_CALL(cudaMalloc((void**)&compass_sum, sizeof(float)*1));

    shared_mem = sizeof(float) * Db.x;

    reduce<<<Dg,Db,shared_mem>>>(compass_tmp, compass_sum, n_blocks);
    CHECK_LAUNCH_ERROR();
    CUDA_CALL(cudaMemcpy((void*)compass_sum_host, (void*)compass_sum, sizeof(float)*1, cudaMemcpyDeviceToHost));

    Dg.x = n_blocks;
    Db.x = n_threads;

    normalize<<<Dg,Db>>>(particles.compass, (*compass_sum_host), particles.size);
    CHECK_LAUNCH_ERROR();

#if 0
    static bool dumped = false;
    if(!dumped) {
        std::ofstream dump("/home/fahri/cmps.txt", std::ios::out);
        float* cmps_weight = (float*)calloc(particles.size,sizeof(float));
        float* angles = (float*)calloc(particles.size,sizeof(float));
        copyDataToHost(cmps_weight, particles.compass, particles.size);
        copyDataToHost(angles, particles.w, particles.size);
        for(size_t i=0; i<particles.size; i++)
          dump << cmps << " "
               << angles[i] << " "
               << cmps_weight[i] << std::endl;
        free(cmps_weight);
        dumped = true;
      }
#endif

    free(compass_sum_host);
    free(compass_tmp_host);
    CUDA_CALL(cudaFree(compass_tmp));
    CUDA_CALL(cudaFree(compass_sum));
}

inline
void MCL::printParticles(ParticlesGPU &particle, std::stringstream &ss)
{
    for(size_t i=0; i<particle.size; i++)
    {
        // std::stringstream sst;
        ss << "p[" << i << "]->("
           << particle.x[i] << ","
           << particle.y[i] << ","
           << particle.w[i] << ","
           << particle.weight[i] << ","
           << particle.compass[i]<< ")";
        // std::cout << sst.str() << std::endl;
        // ROS_INFO("%s", sst.str().c_str());
    }
}

inline
void MCL::copyDataToHost(float *host, float *device, size_t ndata)
{
    CUDA_CALL(cudaMemcpy((void*)host, (void*)device, ndata*sizeof(float), cudaMemcpyDeviceToHost));
}

inline
void MCL::copyDataToDevice(float *host, float *device, size_t ndata)
{
    CUDA_CALL(cudaMemcpy((void*)device, (void*)host, ndata*sizeof(float), cudaMemcpyHostToDevice));
}

void MCL::updateCompass(const fukuro_common::Compass::ConstPtr &compass)
{
    updateCompass(compass->cmps);
}

void MCL::updateMotion(const fukuro_common::OdometryInfo::ConstPtr &odometry)
{
    auto vx = odometry->vel.x;
    auto vy = odometry->vel.y;
    auto w = odometry->vel.theta;
    ROS_WARN("update motion (%.3f,%.3f,%.3f)",vx,vy,w);
    updateMotion(vx, vy, w);
}

void MCL::updateSensor(const fukuro_common::Whites::ConstPtr &whites)
{
    ROS_WARN("callback sensor, size %d", whites->whites.size());
    SensorGPU cpu_whites;
    auto n = whites->whites.size();
    cpu_whites.x = (float*)calloc(n, sizeof(float));
    cpu_whites.y = (float*)calloc(n, sizeof(float));
    cpu_whites.size = n;
    auto i = 0;
    this->cpu_whites.clear();
    for(auto &w : whites->whites)
    {
        cpu_whites.x[i] = w.x*100.0;
        cpu_whites.y[i] = w.y*100.0;
        ++i;
        this->cpu_whites.push_back(std::make_pair(w.x*100.0,w.y*100.0));
    }

    if(n) {
        updateSensor(cpu_whites);
        normalizeWeight(particles);
      }

#ifdef REAL_TIME_CMPS_AVAILABLE
    calculateCompass(particles,cmps);
    normalizeCompass(particles);
    augmentCompass(particles);
#endif

    resample();

#if 0
    static bool dump_partcile = false;
    if(!dump_partcile) {
        std::ofstream particles_dump("/home/fahri/particles_dump.txt", std::ios::out);
        for(const auto &p : this->particles_host)
          particles_dump << x(p) << " "
                         << y(p) << " "
                         << w(p) << " "
                         << weight(p) << " "
                         << compass(p)
                         << std::endl;
        particles_dump.close();
        dump_partcile = true;
      }
#endif

    free(cpu_whites.x);
    free(cpu_whites.y);
}

void MCL::reconfigure(const fukuro_amcl::FukuroAMCLConfig &config, uint32_t level)
{

}

inline
void MCL::copyFieldToDevice(FieldMatrix &field, WeightMapGPU &field_device)
{
    auto n = field.x_length * field.y_length;
    if(!field_device.weights)
    {
        std::cout << "allocate field on device" << std::endl;
        CUDA_CALL(cudaMalloc((void**)&field_device.weights, n*sizeof(float)));
    }
    float *distance_matrix;

    distance_matrix = (float*)calloc(n, sizeof(float));
    std::cout << "copying field matrix" << std::endl;
    for(size_t i=0; i<n; i++)
        distance_matrix[i] = (float)field.distance_matrix[i];
    copyDataToDevice(distance_matrix, field_device.weights, n);
    std::cout << "copying field matrix done" << std::endl;

    std::cout << "checking data" << std::endl;
    float *distance_matrix_cpu;
    distance_matrix_cpu = (float*)calloc(n, sizeof(float));
    copyDataToHost(distance_matrix_cpu, field_device.weights, n);
    for(size_t i=0; i<n; i++)
        if(distance_matrix_cpu[i] != distance_matrix[i])
            std::cout << "data corrupt : " << distance_matrix_cpu[i] << " != " << distance_matrix[i] << std::endl;
    std::cout << "checking data done" << std::endl;

    field_device.size = n;
    field_device.width = field.x_length;
    field_device.height = field.y_length;
    field_device.start_x = field.start_x;
    field_device.start_y = field.start_y;
    field_device.end_x = field.end_x;
    field_device.end_y = field.end_y;
    field_device.stride = field_device.width;
}

inline
void MCL::copyParticleToHost(ParticlesGPU &host, ParticlesGPU &device)
{
    if(!host.x)
        host.x = (float*)calloc(N_PARTICLE, sizeof(float));
    if(!host.y)
        host.y = (float*)calloc(N_PARTICLE, sizeof(float));
    if(!host.w)
        host.w = (float*)calloc(N_PARTICLE, sizeof(float));
    if(!host.weight)
        host.weight = (float*)calloc(N_PARTICLE, sizeof(float));
    if(!host.compass)
        host.compass = (float*)calloc(N_PARTICLE, sizeof(float));
    host.size = device.size;

    mutex.lock();
    CUDA_CALL(cudaMemcpy((void*)host.x, (void*)device.x, N_PARTICLE*sizeof(float), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy((void*)host.y, (void*)device.y, N_PARTICLE*sizeof(float), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy((void*)host.w, (void*)device.w, N_PARTICLE*sizeof(float), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy((void*)host.weight, (void*)device.weight, N_PARTICLE*sizeof(float), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy((void*)host.compass,(void*)device.compass,N_PARTICLE*sizeof(float), cudaMemcpyDeviceToHost));
    mutex.unlock();
}

inline
void MCL::copyParticleToDevice(ParticlesGPU &host, ParticlesGPU &device)
{
    if(!device.x)
        CUDA_CALL(cudaMalloc((void**)&device.x, N_PARTICLE*sizeof(float)));
    if(!device.y)
        CUDA_CALL(cudaMalloc((void**)&device.y, N_PARTICLE*sizeof(float)));
    if(!device.w)
        CUDA_CALL(cudaMalloc((void**)&device.w, N_PARTICLE*sizeof(float)));
    if(!device.weight)
        CUDA_CALL(cudaMalloc((void**)&device.weight, N_PARTICLE*sizeof(float)));
    if(!device.compass)
        CUDA_CALL(cudaMalloc((void**)&device.compass, N_PARTICLE*sizeof(float)));
    device.size = host.size;

    mutex.lock();
    CUDA_CALL(cudaMemcpy((void*)device.x, (void*)host.x, N_PARTICLE*sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy((void*)device.y, (void*)host.y, N_PARTICLE*sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy((void*)device.w, (void*)host.w, N_PARTICLE*sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy((void*)device.weight, (void*)host.weight, N_PARTICLE*sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy((void*)device.compass, (void*)host.compass, N_PARTICLE*sizeof(float), cudaMemcpyHostToDevice));
    mutex.unlock();
}

inline
void MCL::resetParticles(bool init, double xpos, double ypos, double wpos)
{
    mutex.lock();
    auto launch = getLaunchConfig(MAX_THREADS, N_PARTICLE);
    auto n_block = launch.first;
    auto n_threads = launch.second;
    if(!init)
    {
        auto min = ParticleGPU({RESET_MIN_X, RESET_MIN_Y, RESET_MIN_W});
        auto max = ParticleGPU({RESET_MAX_X, RESET_MAX_Y, RESET_MAX_W});
        resetParticlesKernel<<<n_block,n_threads>>>(particles, min, max, random_states);
    }
    else
    {
        auto shift = ParticleGPU({(float)xpos, (float)ypos, (float)wpos});
        auto scale = ParticleGPU({RESET_SCALE_X, RESET_SCALE_Y, RESET_SCALE_W});
        resetParticlesNormalKernel<<<n_block,n_threads>>>(particles, shift, scale, random_states);
    }
    CHECK_LAUNCH_ERROR();
    mutex.unlock();
}

bool MCL::resetParticles(fukuro_common::LocalizationService::Request &req, fukuro_common::LocalizationService::Response &res)
{
    auto known = (req.initial_pos);
    resetParticles(known, req.x, req.y, req.w);
    res.ok = 1;
    return true;
}

void MCL::publish()
{
    fukuro_common::Localization msg;
    ParticlesGPU particle_host;
    copyParticleToHost(particle_host, particles);
    auto belief = mean_estimate;

    msg.belief.x = x(belief);
    msg.belief.y = y(belief);
    msg.belief.theta = w(belief);
    msg.best_estimation.x = x(best_estimate);
    msg.best_estimation.y = y(best_estimate);
    msg.best_estimation.theta = w(best_estimate);
    msg.best_estimation_weight = weight(best_estimate);
    msg.grid_size = GRID_SIZE;
    msg.belief_discrete.x = x(belief)/GRID_SIZE;
    msg.belief_discrete.y = y(belief)/GRID_SIZE;
    msg.belief_discrete.theta = w(belief);
    for(int i=0; i<N_PARTICLE; i++)
    {
        geometry_msgs::Pose2D p;
        p.x = particle_host.x[i];
        p.y = particle_host.y[i];
        p.theta = particle_host.w[i];
        msg.particles.push_back(p);
        msg.weights.push_back(particle_host.weight[i]);
    }
    last_publish = ros::Time::now();
    localization_pub.publish(msg);
    free(particle_host.x);
    free(particle_host.y);
    free(particle_host.w);
    free(particle_host.weight);
    free(particle_host.compass);
}

void MCL::publishGlobalWhites()
{
    fukuro_common::Whites whites_msg;
    mutex.lock();
    auto belief = mean_estimate;
    for(auto& data : cpu_whites)
    {
        fukuro_common::Point2d p;
        double angle_rad = std::get<2>(belief)*M_PI/180.0;
        double c = cos(angle_rad);
        double s = sin(angle_rad);
        p.x = c*data.first-s*data.second+std::get<0>(belief);
        p.y = s*data.first+c*data.second+std::get<1>(belief);
        whites_msg.whites.push_back(p);
    }
    mutex.unlock();
    global_whites_pub.publish(whites_msg);
}

inline
void MCL::resample()
{
    ROS_WARN("start resample");
    static ParticlesGPU particles_cpu;
    copyParticleToHost(particles_cpu, particles);
    for(size_t i=0; i<particles_cpu.size; i++)
    {
        Particle p(particles_cpu.x[i], particles_cpu.y[i], particles_cpu.w[i], particles_cpu.weight[i], particles_cpu.compass[i]);
        // particles_host.push_back(p);
        particles_host.at(i) = p;
    }

    resample(this->particles_host);

    auto i=0;
    for(const auto &p : particles_host)
    {
        particles_cpu.x[i] = x(p);
        particles_cpu.y[i] = y(p);
        particles_cpu.w[i] = w(p);
        particles_cpu.weight[i] = weight(p);
        particles_cpu.compass[i] = compass(p);
        ++i;
    }

    copyParticleToDevice(particles_cpu, particles);
    ROS_WARN("resample done");
}

inline
void MCL::resample(Particles &particles)
{
    Particles plist;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> rg(0.0,1.0/N_PARTICLE);
    double r = rg(gen);
    double c = weight(particles[0]);
    int idx = 0;
    std::random_device rd1;
    std::mt19937 gen1(rd1());
    std::random_device rd2;
    std::mt19937 gen2(rd2());
    std::uniform_real_distribution<double> xrg(-450,450), yrg(-300,300), wrg(0,360);
    double random_prob = (1.0-fabs(w_fast/w_slow));
    std::bernoulli_distribution random_gen((random_prob<0) ? 0.0 : (random_prob>1 ? 0.9 : random_prob));
    best_estimate = particles[0];
    double mean_x(0.0);
    double mean_y(0.0);
    double mean_w(0.0);
    bool init_mean_w(true);
    std::vector<double> angle_vector;
    std::vector<double> angle_vector_weight;
    double angle_weight_sum(0.0);
    // mean_w = w(particles[0]);
    // if(mean_w > 180.0)
        // mean_w -= 360.0;
    // mean_w *= N_PARTICLE;
    for(int i=0; i<N_PARTICLE; i++)
    {
        if(random_gen(gen1))
        {
            plist.push_back(std::make_tuple(xrg(gen2),yrg(gen2),wrg(gen2),1.0/N_PARTICLE,1.0/N_PARTICLE));
        }
        else
        {
            double u = r+((double)i/N_PARTICLE);
            while (u>c) {
                idx += 1;
                c += weight(particles[idx]);
            }
            plist.push_back(particles[idx]);

            if(weight(particles[idx])>weight(best_estimate))
                best_estimate = particles[idx];
            mean_x += 1.0/N_PARTICLE*x(particles[idx]);
            mean_y += 1.0/N_PARTICLE*y(particles[idx]);

            // auto dw = w(particles[idx]);
            // if(dw>180.0) dw -= 360.0;

            // mean_w += 1.0/N_PARTICLE*dw;

            // while(mean_w > 360.0)
                // mean_w -= 360.0;
            // while(mean_w < 0.0)
                // mean_w += 360.0;
            // if(weight(particles[idx])>MINIMUM_WEIGHT_ANGLE)
            // {
                // angle_vector.push_back(w(particles[idx]));
                // angle_vector_weight.push_back(weight(particles[idx]));
                // angle_weight_sum += weight(particles[idx]);
            // }
        }
    }
    mean_w = w(best_estimate);
    for(int i=0; i<N_PARTICLE; i++){
        auto dw = w(particles[i]) - w(best_estimate);
        while(dw > 180.0)
            dw -= 360.0;
        while(dw < -180.0)
            dw += 360.0;
        dw *= weight(particles[i]);
        mean_w += dw;
    }

    while(mean_w > 360.0)
        mean_w -= 360.0;
    while(mean_w < 0.0)
        mean_w += 360.0;

    x(mean_estimate) = mean_x;
    y(mean_estimate) = mean_y;
    w(mean_estimate) = mean_w;
    //    w(mean_estimate) = w(best_estimate);
    // if(angle_vector.size())
    // {
    //     double angle_mean = w(best_estimate);
    //     double best_w = w(best_estimate);
    //     for(size_t i=0; i<angle_vector.size(); i++)
    //     {
    //         double angle = angle_vector[i]-best_w;
    //         if((angle_vector[i]-best_w)>180.0)
    //             angle = -((360.0 - angle_vector[i]) + best_w);
    //         else if((angle_vector[i]-best_w)<-180.0)
    //             angle = (360.0 - best_w) + angle_vector[i];
    //         angle_mean += 1.0/angle_vector.size()*angle;
    //     }
    //     w(mean_estimate) = angle_mean;
    // }
    //  w(mean_estimate) = mean_w;
    particles = plist;
}

double MCL::cmps_error(double &angle){
    double err;
    if(angle>cmps)
    {
        if((angle-cmps)<180.0)
            err = (angle-cmps);
        else
            err = cmps + (360.0-angle);
    }
    else
    {
        if((cmps-angle)<180.0)
            err = cmps-angle;
        else
            err = angle + (360.0-cmps);
    }
    while(err>360)
        err-=360.0;
    while(err<0)
        err+=360.0;
    return err;
}

MCL::FieldMatrix::FieldMatrix()
{
    xline.push_back(XLINE1);
    xline.push_back(XLINE2);
    xline.push_back(XLINE3);
    xline.push_back(XLINE4);
    xline.push_back(XLINE5);
    xline.push_back(XLINE6);
    xline.push_back(XLINE7);

    yline.push_back(YLINE1);
    yline.push_back(YLINE2);
    yline.push_back(YLINE3);
    yline.push_back(YLINE4);
    yline.push_back(YLINE5);
    yline.push_back(YLINE6);

    start_x = -xline[0]-100;
    end_x = xline[0]+100;
    start_y = -yline[0]-100;
    end_y = yline[0]+100;
    x_length = end_x-start_x+1;
    y_length = end_y-start_y+1;

    std::string distance_path = std::string("/home/") +
            std::getenv("USER") +
            std::string("/fukuro_ws/src/omni_vision/calib_results/errortable.bin");
    distance_matrix = new double[x_length*y_length];

    std::ifstream ErrorTable_Read(distance_path.c_str(),std::ios::binary|std::ios::in);
    ErrorTable_Read.read((char*)distance_matrix,sizeof(double)*x_length*y_length);
    ErrorTable_Read.close();
}
