#include "mcl.h"
#include <fstream>
#include <iostream>
#include "fukuro_common/Point2d.h"

#define N_PARTICLE 300
#define TO_RAD M_PI/180.0

//#define REAL_TIME_CMPS_AVAILABLE
#define COMPASS_RESAMPLING

#define CMPS_ERROR
// #define SQUARED_DISTANCE

#define CENTER_RADIUS 100
#define CAMERA_ANGLE_OFFSET (-M_PI_2)
#define DEFAULT_CMPS_W (1.0)
#define DEFAULT_A_FAST (2.75)
#define DEFAULT_A_SLOW (0.1)
#define MINIMUM_WEIGHT_ANGLE 0.333

#define XVAR 1.5
#define YVAR 1.5
#define WVAR 1.1

#define CAMERA_FACTOR 0.5
#define COMPASS_FACTOR (1.0 - CAMERA_FACTOR)

#define ERROR_CONSTANT 500.0
#define BOUNDED_ERROR_CONSTANT 250.0
#define BATCH_SIZE 50

using namespace fukuro;

MCL::MCL(ros::NodeHandle &nh) :
    node(nh),
    xvar(XVAR), yvar(YVAR), wvar(WVAR),
    cmps(0),
    w_fast(0.0), w_slow(0.0),
    a_fast(2.75), a_slow(0.1),
    a_sensor(1.0/N_PARTICLE),
    camera_angle_offset(CAMERA_ANGLE_OFFSET),
    compass_weight(1.0)
{
    std::random_device x_rd, y_rd, w_rd;
    std::uniform_real_distribution<double> x_rgen(-450,450), y_rgen(-300,300), w_rgen(0,360);
    for(int i=0; i<N_PARTICLE; i++)
    {
        particles.push_back(Particle(x_rgen(x_rd), y_rgen(y_rd), w_rgen(w_rd), 1.0 / N_PARTICLE, 1.0 / N_PARTICLE));
    }
    last_publish = ros::Time::now();
    localization_pub = node.advertise<fukuro_common::Localization>("/localization",1);
    global_whites_pub = node.advertise<fukuro_common::Whites>("/localization/global_whites",1);

    double init_x = 0.0;
    double init_y = 0.0;
    double init_w = 0.0;
    if(ros::param::get("/mcl_init_x",init_x))
        if(ros::param::get("/mcl_init_y",init_y))
            if(ros::param::get("/mcl_init_w",init_w)) {
                ROS_INFO("intial pos known, (%.2f,%.2f,%.2f)", init_x, init_y, init_w);
                resetParticles(true, init_x, init_y, init_w);
            }
    double param_xvar = XVAR;
    double param_yvar = YVAR;
    double param_wvar = WVAR;
    if(ros::param::get("/mcl_xvar",param_xvar))
        if(ros::param::get("/mcl_yvar",param_yvar))
            if(ros::param::get("/mcl_wvar",param_wvar)) {
                xvar = param_xvar;
                yvar = param_yvar;
                wvar = param_wvar;
                ROS_INFO("setting mcl variance (%.2f,%.2f,%.2f)", xvar, yvar, wvar);
            }

    time_analytic.best_motion = 1e6;
    time_analytic.best_sensor = 1e6;
    time_analytic.worst_motion = -1e6;
    time_analytic.worst_sensor = -1e6;
    time_analytic.count_motion = 0;
    time_analytic.count_sensor = 0;
}

inline
void MCL::updateMotion(double vx, double vy, double dw)
{
    utility::timer timer;
    mutex.lock();
    static std::random_device xrd, yrd, wrd;
    static std::normal_distribution<> xgen(0.0,xvar), ygen(0.0,yvar), wgen(0.0,wvar);
    static std::normal_distribution<> xygen(0.0,xyvar_motion), yxgen(0.0,yxvar_motion), wxgen(0.0,wxvar_motion);
    static std::normal_distribution<> xwgen(0.0,xwvar_motion), ywgen(0.0,ywvar_motion), wygen(0.0,wyvar_motion);
    for(auto& p : particles)
    {
        double c = cos(w(p)*TO_RAD);
        double s = sin(w(p)*TO_RAD);
        double dx = c*vx-s*vy;
        double dy = s*vx+c*vy;
        // x(p) += dx+dx*xgen(xrd)+(dw)*xwgen(xrd)+(dy)*xygen(xrd);
        // y(p) += dy+dy*ygen(yrd)+(dw)*ywgen(xrd)+(dx)*yxgen(xrd);
        // w(p) += dw+dw*wgen(wrd)+(dx)*wxgen(wrd)+(dy)*wygen(wrd);
        x(p) += dx+dx*xgen(xrd);
        y(p) += dy+dy*ygen(yrd);
        w(p) += dw+dw*wgen(wrd);
        // x(p) += dx + xgen(xrd);
        // y(p) += dy + ygen(yrd);
        // w(p) += dw + wgen(wrd);
        while(w(p)>360.0)
            w(p)-=360.0;
        while(w(p)<0.0)
            w(p)+=360.0;
    }
    mutex.unlock();
    //  ROS_INFO("update motion : %f ms", timer.elapsed());
    auto elapsed = timer.elapsed();
    //  time_analytic.count_motion++;
    if(elapsed < time_analytic.best_motion)
        time_analytic.best_motion = elapsed;
    else if(elapsed > time_analytic.worst_motion)
        time_analytic.worst_motion = elapsed;
    time_analytic.avg_motion = (((time_analytic.avg_motion*time_analytic.count_motion++)+elapsed)/time_analytic.count_motion);
    ROS_INFO("update motion : %.3f ms, best : %.3f, worst : %.3f, avg : %.3f", elapsed, time_analytic.best_motion, time_analytic.worst_motion, time_analytic.avg_motion);
}

// Crucial Part
inline
void MCL::updateSensor(std::vector<SensorData>& data)
{
    utility::timer timer;
    mutex.lock();
    int n_data = data.size();
    double weight_sum(0.0);
    for(auto& p : particles)
    {
        double err_sum(0.0);
        for(auto d : data)
        {
            double angle_rad = w(p)*TO_RAD;
            double c = cos(angle_rad);
            double s = sin(angle_rad);
            double world_x = c*x(d)-s*y(d)+x(p);
            double world_y = s*x(d)+c*y(d)+y(p);
            double distance = field.distance(world_x,world_y);
#ifdef SQUARED_DISTANCE
            double pt_distance = (x(d)*x(d)+y(d)*y(d));
#else
            double pt_distance = sqrt(x(d)*x(d)+y(d)*y(d));
#endif
            if(pt_distance<0.0)
                pt_distance = 1.0;
            if(distance<0.01)
                distance = 0.01;
            //       err_sum += distance*distance*pt_distance;
            //      err_sum += distance*distance;
            err_sum += distance*distance*distance;
        }
        double p_weight = 1.0;
        if(err_sum>0)
            p_weight = 1.0/(err_sum);
        if(n_data>0)
            p_weight /= n_data;
        weight(p) = p_weight;
        weight_sum += p_weight;
    }
    double w_avg = weight_sum / N_PARTICLE;
    w_slow = w_slow + a_slow*(w_avg-w_slow);
    w_fast = w_fast + a_fast*(w_avg-w_fast);
    if(weight_sum>0)
        for(auto& p : particles)
            weight(p) /= weight_sum;
    mutex.unlock();
#ifdef CMPS_ERROR
#ifdef REAL_TIME_CMPS_AVAILABLE
    updateCompass(cmps);
    augmentCompassWeight();
#endif
#endif
    resample();
    auto elapsed = timer.elapsed();
    //  time_analytic.count_sensor++;
    if(elapsed < time_analytic.best_sensor)
        time_analytic.best_sensor = elapsed;
    else if(elapsed > time_analytic.worst_sensor)
        time_analytic.worst_sensor = elapsed;
    time_analytic.avg_sensor = (((time_analytic.avg_sensor*time_analytic.count_sensor++)+elapsed)/time_analytic.count_sensor);
    ROS_INFO("update sensor : %.3f ms, best : %.3f, worst : %.3f, avg : %.3f, white points : %d", elapsed, time_analytic.best_sensor, time_analytic.worst_sensor, time_analytic.avg_sensor, data.size());
}

inline 
void MCL::augmentCompassWeight()
{
    for(auto &p : particles) {
        double weight = CAMERA_FACTOR * this->weight(p) + COMPASS_FACTOR * compass(p);
        this->weight(p) = weight;
    }
}

inline
void MCL::updateCompass(double compass)
{
    auto err_sum = 0.0;
    mutex.lock();
    for(auto& p : particles) {
        auto err = 1.0/fabs(cmps_error(w(p), compass));
        err_sum += err;
        this->compass(p) = err;
    }
    for(auto& p : particles) {
        this->compass(p) /= err_sum;
    }
#ifndef REAL_TIME_CMPS_AVAILABLE
    resampleAngle();
#endif
    mutex.unlock();
}

/* RProp Part */
double MCL::calculateError(){

    double error = 0;
    double bounded_error = std::pow(BOUNDED_ERROR_CONSTANT,2);

    for(auto data : sensor_data){
        double angle_rad = w(mean_estimate)*TO_RAD;
        double c = cos(angle_rad);
        double s = sin(angle_rad);
        double world_x = c*x(data)-s*y(data)+x(mean_estimate);
        double world_y = s*x(data)+c*y(data)+y(mean_estimate);
        double distance = field.distance(world_x,world_y);

        double err = bounded_error + std::pow(distance,2);

        error+=(1-((double)bounded_error/err));
    }

    return error;

}

void MCL::calculateGradient(){

    double tempgradx(0), tempgrady(0), tempgradw(0);
    double bounded_error = std::pow(BOUNDED_ERROR_CONSTANT,2);

    for(auto data : sensor_data){
        double angle_rad = w(mean_estimate)*TO_RAD;
        double c = cos(angle_rad);
        double s = sin(angle_rad);
        double world_x = c*x(data)-s*y(data)+x(mean_estimate);
        double world_y = s*x(data)+c*y(data)+y(mean_estimate);
        double distance = field.distance(world_x,world_y);
        double err = bounded_error + std::pow(distance,2);
        double derr = (2*bounded_error*distance)/(std::pow(err,2));

        double diffx = field.diff_x(world_x,world_y);
        double diffy = field.diff_y(world_x,world_y);

        tempgradx += derr*diffx;
        tempgrady += derr*diffy;
        tempgradw += derr*(-diffx*(s*x(data)+c*y(data))+diffy*(c*x(data)-s*y(data)));
    }

    gradx = tempgradx;
    grady = tempgradx;
    gradw = tempgradw;

}

MCL::State MCL::mean()
{

    double x_loc = x(mean_estimate);
    double y_loc = y(mean_estimate);
    double w_loc = w(mean_estimate);

    /* @TODO: Add RProp Here */
///*
    double dx(16.0), dy(16.0), dw(8.0);
    double last_grad_x(0), last_grad_y(0), last_grad_w(0);

    for(int i=0; i<BATCH_SIZE; i++){

        calculateGradient();

        if((gradx*last_grad_x)!=0) dx *= gradx*last_grad_x<0?0.5:1.2;
        if((grady*last_grad_y)!=0) dy *= grady*last_grad_y<0?0.5:1.2;
        if((gradw*last_grad_w)!=0) dw *= gradw*last_grad_w<0?0.5:1.2;

        if(gradx!=0) x_loc += gradx<0?dx:-dx;
        if(grady!=0) y_loc += grady<0?dy:-dy;
        if(gradw!=0) w_loc += gradw<0?dw:-dw;

        last_grad_x = gradx;
        last_grad_y = grady;
        last_grad_w = gradw;
    }

    x(mean_estimate) = x_loc;
    y(mean_estimate) = y_loc;
    w(mean_estimate) = w_loc;
//*/

    return std::make_tuple(x_loc,y_loc,w_loc);
}

void MCL::resetParticles(bool init, double xpos, double ypos, double wpos)
{
    std::random_device xrd, yrd, wrd;
    if(init)
    {
        std::normal_distribution<double> xrg(xpos,xvar), yrg(ypos,yvar), wrg(wpos,wvar);
        for(auto& p : particles)
        {
            x(p) = xrg(xrd);
            y(p) = yrg(yrd);
            w(p) = wrg(wrd);
        }
    }
    else
    {
        std::uniform_real_distribution<double> xrg(-450,450), yrg(-300,300), wrg(0,360);
        for(auto& p : particles)
        {
            x(p) = xrg(xrd);
            y(p) = yrg(yrd);
            w(p) = wrg(wrd);
        }
    }
}

void MCL::setRandomParameter(double xv, double yv, double wv)
{
    xvar = xv;
    yvar = yv;
    wvar = wv;
}

void MCL::publish()
{
    fukuro_common::Localization loc_msg;
    auto belief = mean();
    loc_msg.belief.x = std::get<0>(belief);
    loc_msg.belief.y = std::get<1>(belief);
    loc_msg.belief.theta = std::get<2>(belief);
    loc_msg.best_estimation.x = x(best_estimate);
    loc_msg.best_estimation.y = y(best_estimate);
    loc_msg.best_estimation.theta = w(best_estimate);
    loc_msg.best_estimation_weight = weight(best_estimate);
    for(auto& p : particles)
    {
        geometry_msgs::Pose2D pose;
        pose.x = x(p);
        pose.y = y(p);
        pose.theta = w(p);
        loc_msg.particles.push_back(pose);
        loc_msg.weights.push_back(weight(p));
    }
    last_publish = ros::Time::now();
    localization_pub.publish(loc_msg);
}

void MCL::publishGlobalWhites()
{
    fukuro_common::Whites whites_msg;
    mutex.lock();
    auto bel = mean();
    for(auto& data : sensor_data)
    {
        fukuro_common::Point2d p;
        //    double angle_rad = w(best_estimate)*M_PI/180.0;
        double angle_rad = std::get<2>(bel)*M_PI/180.0;
        double c = cos(angle_rad);
        double s = sin(angle_rad);
        //    p.x = c*data.first-s*data.second+x(best_estimate);
        //    p.y = s*data.first+c*data.second+y(best_estimate);
        p.x = c*data.first-s*data.second+std::get<0>(bel);
        p.y = s*data.first+c*data.second+std::get<1>(bel);
        whites_msg.whites.push_back(p);
    }
    mutex.unlock();
    global_whites_pub.publish(whites_msg);
}

inline
void MCL::resample()
{
    Particles plist;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> rg(0.0,1.0/N_PARTICLE);
    double r = rg(gen);
    double c = weight(particles[0]);
    int idx = 0;
    std::random_device rd1;
    std::mt19937 gen1(rd1());
    std::random_device rd2;
    std::mt19937 gen2(rd2());
    std::uniform_real_distribution<double> xrg(-450,-400), yrg(-200,200), wrg(0,360);
    double random_prob = (1.0-fabs(w_fast/w_slow));
    std::bernoulli_distribution random_gen((random_prob<0) ? 0.0 : (random_prob>1 ? 0.9 : random_prob));
    best_estimate = particles[0];
    double mean_x(0.0);
    double mean_y(0.0);
    double mean_w(0.0);
    bool init_mean_w(true);
    std::vector<double> angle_vector;
    std::vector<double> angle_vector_weight;
    double angle_weight_sum(0.0);
    for(int i=0; i<N_PARTICLE; i++)
    {
        if(random_gen(gen1))
        {
            plist.push_back(std::make_tuple(xrg(gen2), yrg(gen2), wrg(gen2), 1.0 / N_PARTICLE, 1.0 / N_PARTICLE));
        }
        else
        {
            double u = r+((double)i/N_PARTICLE);
            while (u>c) {
                idx += 1;
                c += weight(particles[idx]);
            }
            plist.push_back(particles[idx]);

            if(weight(particles[idx])>weight(best_estimate))
                best_estimate = particles[idx];
            //      mean_x += weight(particles[idx])*x(particles[idx]);
            //      mean_y += weight(particles[idx])*y(particles[idx]);
            mean_x += 1.0/N_PARTICLE*x(particles[idx]);
            mean_y += 1.0/N_PARTICLE*y(particles[idx]);
            //      if(weight(particles[idx])>MINIMUM_WEIGHT_ANGLE)
            //      {
            //        angle_vector.push_back(w(particles[idx]));
            //        angle_vector_weight.push_back(weight(particles[idx]));
            //        angle_weight_sum += weight(particles[idx]);
            //      }
        }
    }
    mean_w = w(best_estimate);
    for(int i=0; i<N_PARTICLE; i++){
        auto dw = w(particles[i]) - w(best_estimate);
        while(dw > 180.0)
            dw -= 360.0;
        while(dw < -180.0)
            dw += 360.0;
        dw *= weight(particles[i]);
        mean_w += dw;
    }

    while(mean_w > 360.0)
        mean_w -= 360.0;
    while(mean_w < 0.0)
        mean_w += 360.0;

    x(mean_estimate) = mean_x;
    y(mean_estimate) = mean_y;
    w(mean_estimate) = mean_w;
    //  w(mean_estimate) = w(best_estimate);
    //  if(angle_vector.size())
    //  {
    //    double angle_mean = w(best_estimate);
    //    double best_w = w(best_estimate);
    //    for(size_t i=0; i<angle_vector.size(); i++)
    //    {
    //      double angle = angle_vector[i]-best_w;
    //      if((angle_vector[i]-best_w)>180.0)
    //        angle = -((360.0 - angle_vector[i]) + best_w);
    //      else if((angle_vector[i]-best_w)<-180.0)
    //        angle = (360.0 - best_w) + angle_vector[i];
    //      angle_mean += 1.0/angle_vector.size()*angle;
    //    }
    //    w(mean_estimate) = angle_mean;
    //  }
    //  w(mean_estimate) = mean_w;
    particles = plist;
}

inline
void MCL::resampleAngle()
{
    Particles plist = particles;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> rg(0.0,1.0/N_PARTICLE);
    double r = rg(gen);
    double c = compass(particles[0]);
    int idx = 0;
    for(int i=0; i<N_PARTICLE; i++)
    {
        double u = r+((double)i/N_PARTICLE);
        while (u>c) {
            idx += 1;
            c += compass(particles[idx]);
        }
        w(plist[i]) = w(particles[idx]);
    }
    particles = plist;
}

double MCL::cmps_error(double angle, double compass)
{
    double err = angle - cmps;
    if (err > 360)
        err -= 360.0;
    if (err < 0)
        err += 360.0;
    if (err > 180.0)
        err = 360.0 - err;
    if(err < -180.0)
        err = 360.0 + err;
    return fabs(err);
}

double MCL::cmps_error(double &angle)
{
    double err;
    if(angle>cmps)
    {
        if((angle-cmps)<180.0)
            err = (angle-cmps);
        else
            err = cmps + (360.0-angle);
    }
    else
    {
        if((cmps-angle)<180.0)
            err = cmps-angle;
        else
            err = angle + (360.0-cmps);
    }
    while(err>360)
        err-=360.0;
    while(err<0)
        err+=360.0;
    return err;
}

MCL::FieldMatrix::FieldMatrix()
{
    xline.push_back(XLINE1);
    xline.push_back(XLINE2);
    xline.push_back(XLINE3);
    xline.push_back(XLINE4);
    xline.push_back(XLINE5);
    xline.push_back(XLINE6);
    xline.push_back(XLINE7);

    yline.push_back(YLINE1);
    yline.push_back(YLINE2);
    yline.push_back(YLINE3);
    yline.push_back(YLINE4);
    yline.push_back(YLINE5);
    yline.push_back(YLINE6);

    start_x = -xline[0]-100;
    end_x = xline[0]+100;
    start_y = -yline[0]-100;
    end_y = yline[0]+100;
    x_length = end_x-start_x+1;
    y_length = end_y-start_y+1;

    std::string distance_path = std::string("/home/") +
            std::getenv("USER") +
            std::string("/fukuro_ws/src/omni_vision/calib_results/errortable.bin");
    distance_matrix = new double[x_length*y_length];

    std::ifstream ErrorTable_Read(distance_path.c_str(),std::ios::binary|std::ios::in);
    ErrorTable_Read.read((char*)distance_matrix,sizeof(double)*x_length*y_length);
    ErrorTable_Read.close();

    /* RProp Part */
    std::string diff_x_path = std::string("/home/") +
            std::getenv("USER") +
            std::string("/fukuro_ws/src/omni_vision/calib_results/Diff_X.bin");
    std::string diff_y_path = std::string("/home/") +
            std::getenv("USER") +
            std::string("/fukuro_ws/src/omni_vision/calib_results/Diff_Y.bin");
    diff_x_matrix=new double[x_length*y_length];
    diff_y_matrix=new double[x_length*y_length];



    std::ifstream DiffXTable_Read(diff_x_path.c_str(), std::ios::binary|std::ios::in);
    DiffXTable_Read.read((char *)diff_x_matrix,sizeof(double)*x_length*y_length);
    DiffXTable_Read.close();

    std::ifstream DiffYTable_Read(diff_y_path.c_str(), std::ios::binary|std::ios::in);
    DiffYTable_Read.read((char *)diff_y_matrix,sizeof(double)*x_length*y_length);
    DiffYTable_Read.close();

}

double MCL::FieldMatrix::distance(double x, double y)
{
    if((abs((int)x)<=end_x) &&
            (abs((int)y)<=end_y))
        return distance_matrix[((int)(y)-start_y)*x_length+(int)(x)-start_x];
    else
    {
        return ERROR_CONSTANT;
    }
}

double MCL::FieldMatrix::diff_x(double x, double y)
{
    if((abs((int)x)<=end_x) &&
            (abs((int)y)<=end_y))
        return diff_x_matrix[((int)(y)-start_y)*x_length+(int)(x)-start_x];
    else
    {
        return ERROR_CONSTANT;
    }
}

double MCL::FieldMatrix::diff_y(double x, double y)
{
    if((abs((int)x)<=end_x) &&
            (abs((int)y)<=end_y))
        return diff_y_matrix[((int)(y)-start_y)*x_length+(int)(x)-start_x];
    else
    {
        return ERROR_CONSTANT;
    }
}


void MCL::updateMotion(const fukuro_common::OdometryInfo::ConstPtr &odometry)
{
    auto vx = odometry->vel.x;
    auto vy = odometry->vel.y;
    auto w = odometry->vel.theta;
    ROS_WARN("update motion (%.3f,%.3f,%.3f)",vx,vy,w);
    updateMotion(vx, vy, w);
    // updateMotion(odometry->Vx*2.22,odometry->Vy*1.388,odometry->w/M_PI);
}

void MCL::updateSensor(const fukuro_common::Whites::ConstPtr &whites)
{
    std::vector<MCL::SensorData> data;
    for(auto& w : whites->whites)
        data.push_back(SensorData(w.x*100,w.y*100));
    sensor_data = data;
    updateSensor(data);
}

void MCL::updateCompass(const fukuro_common::Compass::ConstPtr &compass){
    ROS_INFO("compass : %.3f", compass->cmps);
    cmps = compass->cmps;
#ifndef REAL_TIME_CMPS_AVAILABLE
#ifdef COMPASS_RESAMPLING
    updateCompass(compass->cmps);
#endif
#endif
}

void MCL::reconfigure(const fukuro_amcl::FukuroAMCLConfig &config, uint32_t level)
{
    ROS_INFO("Reconfigure");
    a_fast = config.a_fast;
    a_slow = config.a_slow;
    xvar_motion = config.motion_x_var;
    xyvar_motion = config.motion_xy_var;
    xwvar_motion = config.motion_xw_var;
    yvar_motion = config.motion_y_var;
    yxvar_motion = config.motion_yx_var;
    ywvar_motion = config.motion_yw_var;
    wvar_motion = config.motion_w_var;
    wxvar_motion = config.motion_wx_var;
    wyvar_motion = config.motion_wy_var;
    camera_angle_offset = config.camera_angle_offset;
    a_sensor = config.vision_sensor_w;
    cmps = config.compass_offset;
}

bool MCL::resetParticles(fukuro_common::LocalizationService::Request &req, fukuro_common::LocalizationService::Response &res)
{
    if(req.initial_pos)
        resetParticles(true,req.x,req.y,req.w);
    else
        resetParticles(false,0.0,0.0,0.0);
    res.ok = 1;
    return true;
}

