#include <ros/ros.h>
#include "mcl.h"

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_amcl");
  ros::NodeHandle node;
  fukuro::MCL mcl(node);
  char* env;
  env = std::getenv("FUKURO");
  if(env==NULL)
  {
    ROS_ERROR("robot name empty, export FUKURO=fukuro1!!!");
    exit(-1);
  }
  std::string robot_name(env);
  ros::Subscriber odometry_sub = node.subscribe(robot_name+"/odometry",1,&fukuro::MCL::updateMotion,&mcl);
  ros::Subscriber whites_sub = node.subscribe("/omnivision/Whites",1,&fukuro::MCL::updateSensor,&mcl);
  ros::Subscriber compass_sub = node.subscribe(robot_name+std::string("/compass"),1,&fukuro::MCL::updateCompass,&mcl);
  ros::ServiceServer localization_server = node.advertiseService("/localization_service",&fukuro::MCL::resetParticles,&mcl);
  dynamic_reconfigure::Server<fukuro_amcl::FukuroAMCLConfig> reconfigure_server;
  reconfigure_server.setCallback(boost::bind(&fukuro::MCL::reconfigure,&mcl,_1,_2));
  ros::AsyncSpinner spinner(2);
  if(spinner.canStart())
    spinner.start();
  ros::Rate loop(27);
  while(ros::ok())
  {
    mcl.publish();
    // mcl.publishGlobalWhites();
    loop.sleep();
  }
  ros::waitForShutdown();
  return 0;
}
