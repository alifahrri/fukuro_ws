#ifndef MCL_H
#define MCL_H

#include <vector>
#include <random>
#include <tuple>
#include <mutex>
#include "utility.hpp"
#include "ros/ros.h"
#include "fukuro_common/Compass.h"
#include "fukuro_common/Localization.h"
#include "fukuro_common/Whites.h"
#include "fukuro_common/OdometryInfo.h"
#include "fukuro_common/LocalizationService.h"
#include "dynamic_reconfigure/server.h"
#include "fukuro_amcl/FukuroAMCLConfig.h"
#include "fukuro/core/field.hpp"

/* //@TODO: TRY CPU FIRST, IF ROBUST THEN MOVE TO GPU

#ifdef CUDA_FOUND
#define GPU
#endif

*/

#ifdef GPU
#include <cuda.h>
#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>
#include <vector>

struct PointGPU
{
    float x;
    float y;
};

struct ParticleGPU
{
    float x;
    float y;
    float w;
    float weight;
    float compass;
};

struct ParticlesGPU
{
    float *x = 0;
    float *y = 0;
    float *w = 0;
    float *weight = 0;
    float *compass = 0;
    int size;
};

//struct ParticlesCompass{
//    float *compass_error = 0;
//    int size;
//}

struct SensorGPU
{
    float *x = 0;
    float *y = 0;
    int size;
};

struct MotionGPU
{
    float vx;
    float vy;
    float w;
};

struct WeightMapGPU
{
    int width;
    int height;
    int stride;
    int start_y;
    int start_x;
    int end_x;
    int end_y;
    int size;
    float *weights = 0;
};

#endif

namespace fukuro {
class MCL
{
public:
    typedef std::tuple<double,double,double,double,double> Particle;
    typedef std::vector<Particle> Particles;
    typedef std::pair<double,double> SensorData;
    typedef std::tuple<double,double,double> State;
    struct FieldMatrix;

    MCL(ros::NodeHandle& nh);
    void updateMotion(const fukuro_common::OdometryInfo::ConstPtr& odometry);
    void updateSensor(const fukuro_common::Whites::ConstPtr& whites);
    void updateCompass(const fukuro_common::Compass::ConstPtr& compass);
    void reconfigure(const fukuro_amcl::FukuroAMCLConfig& config, uint32_t level);
    bool resetParticles(fukuro_common::LocalizationService::Request& req, fukuro_common::LocalizationService::Response& res);
    Particles getParticles()
    {
        mutex.lock();
        auto ret = particles;
        mutex.unlock();
#ifdef GPU
        return toParticlesCPU(ret);
#else
        return ret;
#endif
    }
    State mean();
    void setRandomParameter(double xv, double yv, double wv);
    FieldMatrix* getField()
    {
#ifdef GPU
        FieldMatrix *mat = new FieldMatrix();
        return mat;
#else
        return &field;
#endif
    }
    void publish();
    void publishGlobalWhites();

    inline
    double& x(MCL::Particle& particle) {
        return std::get<0>(particle);
    }

    inline
    const double& x(const MCL::Particle& particle) const {
        return std::get<0>(particle);
    }

    inline
    double& y(MCL::Particle& particle) {
        return std::get<1>(particle);
    }

    inline
    const double& y(const MCL::Particle& particle) const {
        return std::get<1>(particle);
    }

    inline
    double& w(MCL::Particle& particle) {
        return std::get<2>(particle);
    }

    inline
    const double& w(const MCL::Particle& particle) const {
        return std::get<2>(particle);
    }

    inline
    double& weight(MCL::Particle& particle) {
        return std::get<3>(particle);
    }

    inline
    const double& weight(const MCL::Particle& particle) const {
        return std::get<3>(particle);
    }

    inline
    double &compass(MCL::Particle &particle)
    {
        return std::get<4>(particle);
    }

    inline
    const double& compass(const MCL::Particle& particle) const {
        return std::get<4>(particle);
    }

    inline
    double& x(MCL::SensorData& sensor) {
        return sensor.first;
    }

    inline
    double& y(MCL::SensorData& sensor)
    {
        return sensor.second;
    }

    /* @TODO: AMCL fused with RProp */
public:
    double calculateError();
    void calculateGradient();
private:
    double gradx;
    double grady;
    double gradw;

public:
    struct FieldMatrix
    {
        FieldMatrix();
        std::vector<int> xline;
        std::vector<int> yline;
        int start_x;
        int start_y;
        int end_x;
        int end_y;
        int x_length;
        int y_length;
        double *distance_matrix;
        double distance(double x, double y);

        /*Rprop Part*/
        double *diff_x_matrix;
        double *diff_y_matrix;
        double diff_x(double x, double y);
        double diff_y(double x, double y);
    };

private:
    double cmps_error(double& angle);
    double cmps_error(double angle, double compass);
    void updateMotion(double vx, double vy, double dw);
#ifdef GPU
    void resample();
    void resample(Particles &particles);
    void updateSensor(SensorGPU &data);
#else
    void resample();
    void resampleAngle();
    void updateSensor(std::vector<SensorData>& data);
#endif
    void updateCompass(double compass);
    void resetParticles(bool init, double xpos, double ypos, double wpos);
    void augmentCompassWeight();

private:
    Particle best_estimate;
    Particle mean_estimate;
    std::mutex mutex;
    ros::NodeHandle& node;
    ros::Publisher localization_pub;
    ros::Publisher global_whites_pub;
    ros::Time last_publish;
    double xvar;
    double yvar;
    double wvar;
    double cmps = 0.0;
    double a_sensor;
    double w_fast;
    double w_slow;
    double a_fast;
    double a_slow;
    double xvar_motion;
    double yvar_motion;
    double wvar_motion;
    double xyvar_motion;
    double yxvar_motion;
    double wxvar_motion;
    double xwvar_motion;
    double ywvar_motion;
    double wyvar_motion;
    double camera_angle_offset;
    double compass_weight;

#ifdef GPU
private:
    curandState_t *random_states;
    MotionGPU odometry_info;
    ParticlesGPU particles;
    Particles particles_host; //resample on cpu
    curandGenerator_t gen;
    SensorGPU sensor_data;
    WeightMapGPU field;
    FieldMatrix field_host;
    std::vector<float> partial_sum;
    std::vector<SensorData> cpu_whites;
    std::mutex cpy_mutex;

private:
    std::pair<int,int> getLaunchConfig(int max_thread, int n_particle);
    void copyDataToDevice(float *host, float* device, size_t ndata);
    void copyDataToHost(float *host, float* device, size_t ndata);
    void copyParticleToHost(ParticlesGPU &host, ParticlesGPU &device);
    void copyParticleToDevice(ParticlesGPU &host, ParticlesGPU &device);
    void initParticles(ParticlesGPU &particles, size_t n);
    void printParticles(ParticlesGPU &particles, std::stringstream &ss);
    void copyFieldToDevice(FieldMatrix &field, WeightMapGPU &field_device);
    void normalizeWeight(ParticlesGPU &particles);
    void calculateCompass(ParticlesGPU &particles, double compass);
    void normalizeCompass(ParticlesGPU &particles);
    void augmentCompass(ParticlesGPU &particles);
    Particles toParticlesCPU(ParticlesGPU particles);
    FieldMatrix toFieldMatrixCPU(WeightMapGPU field);
#else
    Particles particles;
    FieldMatrix field;
    std::vector<SensorData> sensor_data;
#endif

    struct TimerAnalytics
    {
        double best_sensor;
        double best_motion;
        double worst_sensor;
        double worst_motion;
        double avg_sensor;
        double avg_motion;
        long count_motion;
        long count_sensor;
    } time_analytic;
};

}

#endif // MCL_H
