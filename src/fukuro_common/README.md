## fukuro_common
common messages and service files definition

### Message Files  
#### Basic Messages
- `BBox.msg` : contains `x``y` origin, width and height (`w`,`h`), `label`, box color (`rgb`), and probability of bounding box  
- `Communication.msg` : contains `address`, `tx_port`, and `rx_port`  
- `Compass.msg` : contains `cmps` value  
- `DribblerControl.msg` : contains dribbler direction and speed  
- `Encoder.msg` : contains information of each encoders, `current_tick` and `total_tick`  
- `FrontVision3d.msg` : contains information of balls in 3d with type of array of `geometry_msgs/Point`  
- `HWControllerManual.msg` : contains velocity commands 
- `InterceptPoint.msg` : contains information about intercept_point, intercept_time, and validity of the interception  
- `Localization.msg` : contains information about `belief` particle (which is the weighted sum of all particles' position), `best_estimation` (particle with the highest weight), `weights` of all particles, pose of all `particles`, weight of the best particle, `grid_size` and `belief_discrete`  
- `LocalVision.msg` : contains positions of obstalces and balls relative to the robot pose  
- `MotorParameter.msg` : settings for each motors  
- `Obstacles.msg` : information about obstacles' position  
- `OdometryInfo.msg` : robot's position and velocity using odometry 
- `PlannerInfo.msg` : information from path-planner, `obstacles` (in grid), `solution` tiles, `start` and `goal` info, `search_time`, and `solved` (or not)  
- `Point2d.msg` : a 2d point  
- `Point2dStamped.msg` : a 2d point in time  
- `Point3d.msg` : a 3d point  
- `Point3dStamped.msg` a 3d point in time  
- `PWM.msg` : pwm value for each `motor`s  
- `RobotControl.msg` : command for position-based robot control, including `target_pose` (type of `geometry_msgs/Pose2d`), `option`, `control`, `dribbler`, and `plan` (use path from planner or not)  
- `StrategyInfo.msg` : strategy_state, role, and option  
- `StrategyPositioning.msg` : `ball_shield_angle`, `kick_angle`, and `kick_target`  
- `Teammates.msg` : `robotname`, `pose`, `available`, `behavior`, `isConnected`, `state`, `isManualPositioning`  
- `VelCmd.msg` : local velocity, `Vx`, `Vy`, and `w` commands
#### Compound Messages
- `BallPrediction.msg` : contains linear regression model, history of position, time of last detected ball, and proposed intercept pos for ball in 2d
- `BallPrediction3d.msg` : contains linear model, quadratic model for ball height, history of position, time of last detected ball, and proposed intercept pos for ball in 3d  
- `Ball.msg` : array of ball (Point2d) 
- `HWController.msg` : contains information about pwm values (type of `PWM.msg`), ir sensor, base_type, and body rate or velocity  
- `HWControllerCommand.msg` : contains commands for velocity (local, of type `VelCmd.msg`) and dribbler command (of type `DribblerControl.msg`)  
- `RGBDVision.msg` : information about balls, bounding boxes, and its raw (depth) value  
- `Whites.msg` : detected `whites`  
- `WorldModel.msg` : world model information  

### Service Files
- `BallHandle.srv` : request `enable` (type of `int64`) and respond `BallIsHolding` (also `int64`)  
- `BallPredictionService.srv` : request `speed` (type of `float64`) and respond `ok` (`int32`)  
- `CommunicationService.srv` : request `connect` (`uint8`), `address` (`string`), `tx_port` and `rx_port` (`int64`) and respond `success` (`uint8`)  
- `HWControllerParamService.srv` : request `cw` and `ccw` (of type `MotorParameter`, see msg files) and respond `cw`, `ccw`, and `ok`  
- `HWControllerService.srv` : request `refresh` (`uint8`), `STMConnect` (`int64`), `isSTM` (`uint8`), `ArduinoConnect` (`int64`), `isArduino` (`uint8`), `Compass` (`uint8`) and respond `port_list` (array of `string`), `manufacturer_list` (array of `string`), `STMSuccess` (`uint8`), `ArduinoSuccess` (`uint8`), `Compassuccess` (`uint8`)  
- `KeypadService.srv` : request `key` (`uint18`) and respond `res` (`int64`)  
- `LocalizationService.srv` : request `x`, `y`, `w` (`float32`), and `initial_pos` (`uint8`) and respond `ok` (`int64`)  
- `OdometryCalibarationService.srv` : request `ground_x`, `ground_y`, `ground_w`, `x`, `y`, `w` (`float32`) and respond `ok`  
- `OdometryService.srv` : request `x`, `y`, `w` (`float32`) and respond `ok`  
- `PlannerInfoService.srv` : request `info` (`uint8`) and respond `width`, `height`, `grid_size` (`float32`)  
- `PlannerService.srv` : request `goal` (`Point2d`, see msg files) and respond `ok`  
- `Shoot.srv` : request `kick_request` (`uint8`) and respond `ShootIsDone` (`int64`)  
- `StrategyService.srv` : request `strategy_state`, `option`, `role` (`string`), `home_pos`, `kick_pose` (`geometry_msgs/Pose2D`) and respond `ok`  

### Usage  
add `<build_depend>fukuro_common</build_depend>` and `<exec_depend>fukuro_common</exec_depend>` tag to your `package.xml` file and add `fukuro_common` to `find_package(catkin REQUIRED COMPONENTS ...)` in your `CMakeLists.txt`  