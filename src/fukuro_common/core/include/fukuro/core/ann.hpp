#ifndef ANN_HPP
#define ANN_HPP

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

#include <vector>
#include <random>
#include <string>
#include <cmath>
#include <iostream>
#include <iosfwd>
#include <fstream>
#include <istream>
#include <sys/types.h>
#include <sys/stat.h>

typedef std::vector<std::vector<double>> Data;

namespace fukuro {

struct Layer{

    int node;
    std::string type;
    Eigen::MatrixXd weight;
    Eigen::MatrixXd input;
    Eigen::MatrixXd output;
    Eigen::MatrixXd output_target;
    Eigen::MatrixXd error;
    std::string activation;

};


class ANN{

public:
    ANN(int ep=500, double lr=0.01, std::string path="./saved_weight/weight.dat"){

        epoch = ep;
        learning_rate = lr;
        weight_path = path;

    }

public:

    void add_layer(int node, std::string type, std::string activation){

        Layer layer;
        layer.node = node;
        layer.type = type;
        layer.activation = activation;


        if (layer.type=="hidden")
        {
            layer.weight.resize(model[model.size()-1].node,layer.node);
            layer.input.resize(1,layer.node);
            layer.output.resize(1,layer.node);
            layer.output_target.resize(1,layer.node);
            layer.error.resize(1,layer.node);
        }
        else if(layer.type=="input")
        {
            layer.weight.resize(1,layer.node);
            layer.output.resize(1,layer.node);
            layer.output_target.resize(1,layer.node);
        }
        else if(layer.type=="output")
        {
            layer.weight.resize(model[model.size()-1].node,layer.node);
            layer.input.resize(1,layer.node);
            layer.output.resize(1,layer.node);
            layer.output_target.resize(1,layer.node);
            layer.error.resize(1,layer.node);
        }

        model.push_back(layer);

    }

    void fit(Data X, Data Y){

        if(X.size()!=Y.size()){
            std::cout << "Size Error!" << '\n';
            return;
        }

        struct stat info;
        if(stat(weight_path.c_str(),&info)==0)
            load_weight();
        else{
            for(int idx = 1; idx<model.size(); idx++){
                randomize_weight(model[idx].weight);
            }
        }


        for(int ep = 0; ep < epoch; ep++){

            for(int data=0; data<X.size(); data++){

                for(int idx=0; idx<X[data].size(); idx++)
                    model[0].output(0,idx) = X[data][idx];


                for(int idx=0; idx<Y[data].size(); idx++)
                    model[model.size()-1].output_target(0,idx) = Y[data][idx];


                for(int l=0; l<model.size()-1; l++){
                    model[l+1].input = model[l].output * model[l+1].weight;
                    if(model[l+1].activation=="relu"){
                        model[l+1].output = activation(model[l+1].input);
                    }
                    else if(model[l+1].activation=="sigmoid"){
                        model[l+1].output = activation(model[l+1].input,"sigmoid");
                    }
                }

                /* Backprop */
                for(int b=model.size()-1; b>0; b--){

                    /* MSE */
                    double sum = 0.0;
                    Eigen::MatrixXd temp_error(model[b].error.rows(),model[b].error.cols());
                    for(int y=0; y<model[b].output_target.cols(); y++){

                        model[b].error(0,y) = std::pow((model[b].output_target(0,y) - model[b].output(0,y)),2);
                        //std::cout << "ERROR PASSED!! " <<'\n';
                        temp_error(0,y) = (model[b].output_target(0,y) - model[b].output(0,y));
                        sum += model[b].error(0,y);

                    }
                    //for(int row=0; row<model[b].error.rows();row++)
                    //    for(int col=0; col<model[b].error.cols();col++)
                    //        model[b].error(row,col) /= sum;

                    for(int row=0; row<model[b].weight.rows(); row++){
                        for(int col=0; col<model[b].weight.cols(); col++){

                            if(model[b].activation=="relu"){
                                //std::cout << "OLD WEIGHT " << row <<' '<<col<<' '<<model[b].weight(row,col)<<'\n';
                                double derr = model[b].output(0,col)>0?1.0:0.0;
                                model[b].weight(row,col) = model[b].weight(row,col) + learning_rate*(2*(temp_error(0,col)/sum))*derr;
                                //std::cout << "NEW WEIGHT " << row <<' '<<col<<' '<<model[b].weight(row,col)<<'\n';
                            }
                            else if(model[b].activation=="sigmoid"){
                                auto output = model[b].output(0,col);
                                auto input = model[b].input(0,col);
                                model[b].weight(row,col) = model[b].weight(row,col) + learning_rate*(temp_error(0,col))*(output)*(1-output)*input;
                            }

                        }

                    }

                    //std::cout << "WEIGHT " <<b<<'('<< '0' <<' '<<'0'<<") "<<model[b].weight(0,0)<<'\n';
                    model[b-1].output_target = model[b].output * pseudoinverse(model[b].weight);

                }

            }
            /* ACCURACY TEST */
            for(int l=0; l<model.size()-1; l++){
                model[l+1].input = model[l].output * model[l+1].weight;
                if(model[l+1].activation=="relu"){
                    model[l+1].output = activation(model[l+1].input);
                }
                else if(model[l+1].activation=="sigmoid"){
                    model[l+1].output = activation(model[l+1].input,"sigmoid");
                }
            }

            //double sum = 0.0;
            for(int y=0; y<model[model.size()-1].output_target.cols(); y++){
                model[model.size()-1].error(0,y) = std::pow((model[model.size()-1].output_target(0,y) - model[model.size()-1].output(0,y)),2);
                //    sum += model[model.size()-1].error(0,y);
            }
            //for(int row=0; row<model[model.size()-1].error.rows();row++)
            //    for(int col=0; col<model[model.size()-1].error.cols();col++)
            //        model[model.size()-1].error(row,col) /= sum;

            std::cout << "Loss: " << sum_error(model[model.size()-1].error) << '\n';

            save_weight();
        }

    }

    std::vector<double> predict(std::vector<double> X){

        std::vector<double> ret;

        assert(X.size()==model[0].output.cols());

        load_weight();

        for(int idx=0; idx<X.size(); idx++)
            model[0].output(0,idx) = X[idx];

        for(int l=0; l<model.size()-1; l++){
            model[l+1].input = model[l].output * model[l+1].weight;
            if(model[l+1].activation=="relu"){
                model[l+1].output = activation(model[l+1].input);
            }
            else if(model[l+1].activation=="sigmoid"){
                model[l+1].output = activation(model[l+1].input,"sigmoid");
            }
        }

        for(int col=0; col<model[model.size()-1].output.cols(); col++){
            auto output = model[model.size()-1].output(0,col);
            ret.push_back(output);
            std::cout << "OUTPUT "<<col<<' '<<output<<'\n';
        }

        return ret;

    }

private:

    inline
    void save_weight(){
        std::ofstream out(weight_path.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
        for(int idx =1; idx<model.size(); idx++){
            auto rows = model[idx].weight.rows();
            auto cols = model[idx].weight.cols();
            out.write((char*) (&rows), sizeof(rows));
            out.write((char*) (&cols), sizeof(cols));
            out.write((char*) model[idx].weight.data(), rows*cols*sizeof(Eigen::MatrixXd::Scalar));
        }
        out.close();
    }

    inline
    void load_weight(){
        /* Must Create a Model*/
        if(model.size()==0){
            std::cout<<"Error! Must Create a Model!"<<'\n';
            return;
        }

        std::ifstream in(weight_path.c_str(), std::ios::in | std::ios::binary);
        for(int idx = 1; idx<model.size(); idx++){
            auto rows = model[idx].weight.rows();
            auto cols = model[idx].weight.cols();
            in.read((char*) (&rows),sizeof(rows));
            in.read((char*) (&cols),sizeof(cols));
            model[idx].weight.resize(rows, cols);
            in.read((char*)model[idx].weight.data(),rows*cols*sizeof(Eigen::MatrixXd::Scalar));
        }
        in.close();
    }

    inline
    void randomize_weight(Eigen::MatrixXd &input){

        std::random_device rand;
        std::uniform_real_distribution<double> randgen(-1.0,1.0);

        for(int row=0; row<input.rows(); row++){
            for(int col=0; col<input.cols(); col++){
                input(row,col) = randgen(rand)*(double)std::sqrt(2.0/input.rows());
            }
        }


    }

    inline double sum_error(Eigen::MatrixXd input){

        double ret = 0.0;

        for(int row=0; row<input.rows();row++)
            for(int col=0; col<input.cols();col++){
                //std::cout << "ERROR " << input(row,col) << '\n';
                ret += input(row,col);
            }

        return ret;

    }

    inline Eigen::MatrixXd activation(Eigen::MatrixXd input,std::string type="relu"){

        if(type=="relu"){
            /* ReLU */
            for(int col=0; col<input.cols(); col++)
                input(0,col) = std::max(0.0,input(0,col));
        }
        else if(type=="sigmoid"){
            /* Sigmoid */
            for(int col=0; col<input.cols(); col++)
                input(0,col) = 1.0/(1.0+std::exp(-input(0,col)));
        }

        return input;

    }

private:

    template <class MatT>
    inline
    Eigen::Matrix<typename MatT::Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime>
    pseudoinverse(const MatT &mat, typename MatT::Scalar tolerance = typename MatT::Scalar{1e-4}) // choose appropriately
    {
        typedef typename MatT::Scalar Scalar;
        auto svd = mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
        const auto &singularValues = svd.singularValues();
        Eigen::Matrix<Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime> singularValuesInv(mat.cols(), mat.rows());
        singularValuesInv.setZero();
        for (unsigned int i = 0; i < singularValues.size(); ++i) {
            if (singularValues(i) > tolerance)
            {
                singularValuesInv(i, i) = Scalar{1} / singularValues(i);
            }
            else
            {
                singularValuesInv(i, i) = Scalar{0};
            }
        }
        return svd.matrixV() * singularValuesInv * svd.matrixU().adjoint();
    }
    /*
    Eigen::MatrixXd
    pseudoinverse(const Eigen::MatrixXd &mat, Eigen::MatrixXd::Scalar tolerance = Eigen::MatrixXd::Scalar{1e-4}){

        typedef typename Eigen::MatrixXd::Scalar Scalar;
        auto svd = mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
        const auto &singularValues = svd.singularValues();
        Eigen::Matrix<Scalar, Eigen::MatrixXd::ColsAtCompileTime, Eigen::MatrixXd::RowsAtCompileTime> singularValuesInv(mat.cols(),mat.rows());
        singularValuesInv.setZero();

        for(unsigned int i = 0; i<singularValues.size(); i++){
            if(singularValues(i)>tolerance){
                singularValuesInv(i,i) = Scalar{1}/singularValues(i);
            }
            else
            {
                singularValuesInv(i,i) = Scalar{0};
            }
        }
        std::cout << "ERROR PASSED!" << '\n';
        std::cout << "SVD V: " << svd.matrixV().rows() <<' ' << svd.matrixV().cols() << '\n';
        std::cout << "SV Inv: " << singularValuesInv.rows() <<' ' << singularValuesInv.cols() << '\n';
        std::cout << "SVD U: " << svd.matrixU().rows() <<' ' << svd.matrixU().cols() << '\n';
        return svd.matrixV()*singularValuesInv*svd.matrixU().adjoint();

    }
*/

private:
    std::vector<Layer> model;
    int epoch;
    double learning_rate;
    std::string weight_path;

};


}



#endif //ANN_HPP
