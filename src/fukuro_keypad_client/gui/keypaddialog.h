#ifndef KEYPADDIALOG_H
#define KEYPADDIALOG_H

#include <QDialog>
#include <ros/ros.h>

namespace Ui {
class KeypadDialog;
}

class KeypadDialog : public QDialog
{
  Q_OBJECT

public:
  explicit KeypadDialog(ros::NodeHandle &node_, QWidget *parent = 0);
  ~KeypadDialog();
  void setKey(int keyIn);
  std::string getRole(int keyIn);

private:
  Ui::KeypadDialog *ui;
  std::string role[30]={"stop", "defender2", "defender2", "defender", "defender", "striker",
                        "striker_dribble", "striker", "striker_dribble", "timer_striker", "timer_striker2", "striker",
                        "timer_striker", "striker", "timer_striker", "striker", "striker_dribble", "striker",
                        "striker_dribble", "timer_defense2", "timer_defense2", "timer_defense", "timer_defense","timer_striker",
                        "timer_striker2", "striker", "timer_striker", "striker", "timer_striker", "goalie"};
};

#endif // KEYPADDIALOG_H
