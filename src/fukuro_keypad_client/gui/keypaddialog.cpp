#include "keypaddialog.h"
#include "ui_keypaddialog.h"
#include "fukuro_common/KeypadService.h"
#include "iostream"
#include <QString>

std::string robot_name;

KeypadDialog::KeypadDialog(ros::NodeHandle &node_, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KeypadDialog)
{
    ui->setupUi(this);
    ROS_INFO("wait keypad server");
    if(!ros::get_environment_variable(robot_name,"FUKURO"))
        exit(-1);
    ros::service::waitForService(robot_name+"/keypad_service");

    connect(ui->pushButton,&QPushButton::clicked,[=]
    {
        setKey(0);
    });

    connect(ui->pushButton_2,&QPushButton::clicked,[=]
    {
        setKey(1);
    });

    connect(ui->pushButton_3,&QPushButton::clicked,[=]
    {
        setKey(2);
    });

    connect(ui->pushButton_4,&QPushButton::clicked,[=]
    {
        setKey(3);
    });

    connect(ui->pushButton_5,&QPushButton::clicked,[=]
    {
        setKey(4);
    });

    connect(ui->pushButton_6,&QPushButton::clicked,[=]
    {
        setKey(5);
    });

    connect(ui->pushButton_7,&QPushButton::clicked,[=]
    {
        setKey(6);
    });

    connect(ui->pushButton_8,&QPushButton::clicked,[=]
    {
        setKey(7);
    });

    connect(ui->pushButton_9,&QPushButton::clicked,[=]
    {
        setKey(8);
    });

    connect(ui->pushButton_10,&QPushButton::clicked,[=]
    {
        setKey(9);
    });

    connect(ui->pushButton_11,&QPushButton::clicked,[=]
    {
        setKey(10);
    });

    connect(ui->pushButton_12,&QPushButton::clicked,[=]
    {
        setKey(11);
    });

    connect(ui->pushButton_13,&QPushButton::clicked,[=]
    {
        setKey(12);
    });

    connect(ui->pushButton_14,&QPushButton::clicked,[=]
    {
        setKey(13);
    });

    connect(ui->pushButton_15,&QPushButton::clicked,[=]
    {
        setKey(14);
    });

    connect(ui->pushButton_16,&QPushButton::clicked,[=]
    {
        setKey(15);
    });

    connect(ui->pushButton_17,&QPushButton::clicked,[=]
    {
        setKey(16);
    });

    connect(ui->pushButton_18,&QPushButton::clicked,[=]
    {
        setKey(17);
    });

    connect(ui->pushButton_19,&QPushButton::clicked,[=]
    {
        setKey(18);
    });

    connect(ui->pushButton_20,&QPushButton::clicked,[=]
    {
        setKey(19);
    });

    connect(ui->pushButton_21,&QPushButton::clicked,[=]
    {
        setKey(20);
    });

    connect(ui->pushButton_22,&QPushButton::clicked,[=]
    {
        setKey(21);
    });

    connect(ui->pushButton_23,&QPushButton::clicked,[=]
    {
        setKey(22);
    });

    connect(ui->pushButton_24,&QPushButton::clicked,[=]
    {
        setKey(23);
    });

    connect(ui->pushButton_25,&QPushButton::clicked,[=]
    {
        setKey(24);
    });

    connect(ui->pushButton_26,&QPushButton::clicked,[=]
    {
        setKey(25);
    });

    connect(ui->pushButton_27,&QPushButton::clicked,[=]
    {
        setKey(26);
    });

    connect(ui->pushButton_28,&QPushButton::clicked,[=]
    {
        setKey(27);
    });

    connect(ui->pushButton_29,&QPushButton::clicked,[=]
    {
        setKey(28);
    });

    connect(ui->pushButton_30,&QPushButton::clicked,[=]
    {
        setKey(29);
    });

    connect(ui->pushButton_31,&QPushButton::clicked,[=]
    {
        setKey(30);
    });

    connect(ui->pushButton_32,&QPushButton::clicked,[=]
    {
        setKey(31);
    });

    connect(ui->pushButton_33,&QPushButton::clicked,[=]
    {
        setKey(32);
    });

    connect(ui->pushButton_34,&QPushButton::clicked,[=]
    {
        setKey(33);
    });

    connect(ui->pushButton_35,&QPushButton::clicked,[=]{

    });

    connect(ui->pushButton_36,&QPushButton::clicked,[=]{

    });

    setWindowTitle("Fukuro Keypad Client");
    this->setFixedSize(this->size());

    ui->text_edit->appendPlainText(QString("Key List:\n---"));
    ui->text_edit->appendPlainText(QString("Key 0: Stop\nComm : Auto\nStart: Auto\n---\nKey 1: Defender 2\nComm : False\nStart: Right\n---\nKey 2: Defender 2"));
    ui->text_edit->appendPlainText(QString("Comm : False\nStart: Left\n---\nKey 3: Defender\nComm : False\nStart: Right\n---\nKey 4: Defender\nComm : False"));
    ui->text_edit->appendPlainText(QString("Start: Left\n---\nKey 5: Striker\nComm : False\nStart: Right\n---\nKey 6: Striker Dribble\nComm : False\nStart: Right\n---"));
    ui->text_edit->appendPlainText(QString("Key 7: Striker\nComm : False\nStart: Right (Fast Mode)\n---\nKey 8: Striker Dribble\nComm : False\nStart: Right (Fast Mode)"));
    ui->text_edit->appendPlainText(QString("---\nKey 9: Timer Striker\nComm : True\nStart: Right\n---\nKey10: Timer Striker 2\nComm : True\nStart: Right\n---\nKey11: Striker"));
    ui->text_edit->appendPlainText(QString("Comm : False\nStart: Right\n---\nKey12: Timer Striker\nComm : True\nStart: Right\n---\nKey13: Striker\nComm : False\nStart: Right"));
    ui->text_edit->appendPlainText(QString("---\nKey14: Timer Striker\nComm : True\nStart: Right\n---\nKey15: Striker\nComm : False\nStart: Left\n---\nKey16: Striker Dribble"));
    ui->text_edit->appendPlainText(QString("Comm : False\nStart: Left\n---\nKey17: Striker\nComm : False\nStart: Left (Fast Mode)\n---\nKey18: Striker Dribble\nComm : False"));
    ui->text_edit->appendPlainText(QString("Start: Left (Fast Mode)\n---\nKey19: Timer Defense 2\nComm : True\nStart: Right\n---\nKey20: Timer Defense 2\nComm : True\nStart: Left"));
    ui->text_edit->appendPlainText(QString("---\nKey21: Timer Defense\nComm : True\nStart: Right\n---\nKey22: Timer Defense 2\nComm : True\nStart: Left\n---\nKey23: Timer Striker"));
    ui->text_edit->appendPlainText(QString("Comm : True\nStart: Left\n---\nKey24: Timer Striker 2\nComm : True\nStart: Left\n---\nKey25: Striker\nComm : False\nStart: Left\n---\n"));
    ui->text_edit->appendPlainText(QString("Key26: Timer Striker\nComm : True\nStart: Left\n---\nKey27: Striker\nComm : False\nStart: Left\n---\nKey28: Timer Striker\nComm : True"));
    ui->text_edit->appendPlainText(QString("Start: Left\n---\nKey29: Goalie\nComm : Auto\nStart: Goalie Post\n---\n"));
}


KeypadDialog::~KeypadDialog()
{
    delete ui;
}

std::string KeypadDialog::getRole(int keyIn){
    if(keyIn <= 29)
        return this->role[keyIn];
    return "auto";
}

void KeypadDialog::setKey(int keyIn){
    fukuro_common::KeypadService key_service;
    key_service.request.key = keyIn;
    std::string rolein = getRole(keyIn);
    QString role = QString::fromStdString(rolein);

    if(ros::service::call(robot_name+"/keypad_service",key_service))
    {
        ui->text_edit->appendPlainText(QString("Request %1 OK\t| Role: %2").arg(keyIn).arg(role));
    }
    else
    {
        ui->text_edit->appendPlainText(QString("Service request failed"));
    }
}

