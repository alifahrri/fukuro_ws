#include <ros/ros.h>
#include <QApplication>
#include "keypaddialog.h"

int main(int argc, char** argv)
{
  QApplication app(argc,argv);
  ros::init(argc,argv,"fukuro_keypad_client");
  ros::NodeHandle nh;
  KeypadDialog dialog(nh);
  dialog.show();
  app.exec();
  return 0;
}
