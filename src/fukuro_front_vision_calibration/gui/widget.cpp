#include "widget.h"
#include "ui_widget.h"

#include "ipmwidget.h"
#include "yuvitem.h"
#include "imageitem.h"
#include "colortablegenerator.h"

#include "util.hpp"

#include <QRgb>
#include <QFileDialog>
#include <QResizeEvent>
#include <QGraphicsPixmapItem>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <QtGlobal>

#define SCALE 0.75

Widget::Widget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::Widget),
  ipm(new fukuro::IPM),
  yuv_item(new YUVItem),
  img_item(new ImageItem),
  ipm_widget(new IPMWidget),
  ctable_gen(new ColorTableGenerator),
  ball_seeker(new fukuro::BallSeeker)
{
  ui->setupUi(this);
  color_segment = new fukuro::ColorSegment(ctable_gen->table);

  auto s = this->size();
  auto w = s.width()-10;
  auto h = s.height()-50;
  ui->graphicsView->setScene(new QGraphicsScene(-w/2,-h/2,w,h,this));
  ui->graphicsView->scale(SCALE,SCALE);

  ui->graphicsView->scene()->addItem(img_item);

  auto ys = ui->yuv_graphicsView->geometry().size();
  auto yw = ys.width();
  auto yh = ys.height();
  ui->yuv_graphicsView->setScene(new QGraphicsScene(-yw/2,-yh/2,yw,yh,this));

  ui->yuv_graphicsView->scene()->addItem(yuv_item);
  lookup = createYUV(64,64,64);  

  setupCallback();
  setupConnection();

  //  ui->yuv_graphicsView->setRenderHint(QPainter::HighQualityAntialiasing);
  //  ui->graphicsView->setRenderHint(QPainter::HighQualityAntialiasing);
  this->setWindowTitle("FUKURO Front Camera Calibration");

  ui->stream_cb->toggled(ui->stream_cb->isChecked());
  ui->overlay_cb->toggled(ui->overlay_cb->isChecked());
  ui->min_width_sbox->valueChanged(ui->min_width_sbox->value());
  ui->w2h_sbox->valueChanged(ui->w2h_sbox->value());

  std::string fukuro, user, agent;
  ros::get_environment_variable(fukuro,"FUKURO");
  ros::get_environment_variable(user,"USER");
  ros::get_environment_variable(agent,"AGENT");
  bool autoload = false;
  if(ros::param::get("autoload",autoload)) {
      auto dir = QString("/home/%1/fukuro_ws/src/fukuro_front_vision_calibration/calib_results/%2").arg(user.c_str()).arg(agent.c_str());
      ui->dir_line_edit->setText(dir);
    }

  ipm_widget->show();
}

void Widget::updateImage(const sensor_msgs::ImageConstPtr &msg)
{
  ROS_INFO("image callback");
  cv_bridge::CvImageConstPtr cv_ptr;
  cv_ptr = cv_bridge::toCvShare(msg,sensor_msgs::image_encodings::BGR8);
  this->mat_ori = cv_ptr->image;
  if(img_item->stream)
  {
    if(img_item->overlay && ctable_gen->table->size())
    {
      ROS_INFO("overlaying");
      auto color = color_segment->process(cv_ptr->image);
      if(color.empty())
        img_item->setImage(cv_ptr->image);
      else
      {
        auto ball_segment = ball_seeker->process(color);
        cv::Mat ball_box;
        img_item->overlayMat(cv_ptr->image,color,ball_box);
        //        ball_seeker->drawSegments(ball_box, ball_segment, cv::Scalar(0,0,255));
        //        img_item->ball_segments = ball_segment;

        QVector<QRectF> segments;
        QPointF half(-img_item->bgr_mat.cols/2,-img_item->bgr_mat.rows/2);
        for(const auto& s : ball_segment)
          segments.push_back(QRectF(QPointF(s.bbox.x0,s.bbox.y0)+half,
                                    QPointF(s.bbox.x1,s.bbox.y1)+half));
        img_item->boxes = segments;
        img_item->setImage(ball_box);
      }
      if(ui->ipm_checkbox->isChecked()) {
          auto transformed = ipm->birdsEye(color);
          auto ori_transformed = ipm->birdsEye(cv_ptr->image);
          cv::Mat cv_ipm;
          ipm_widget->img_item->overlayMat(transformed,ori_transformed,cv_ipm);
          if(!cv_ipm.empty())
            ipm_widget->img_item->setImage(cv_ipm);
        }
      ROS_INFO("overlaying done");
    }
    else {
        img_item->setImage(cv_ptr->image);
        img_item->boxes.clear();
      }
  }
}

Widget::~Widget()
{
  delete ui;
}

void Widget::setupConnection()
{
  connect(ui->build_ipmap_btn, &QPushButton::clicked,[=]
  {
      ipm->fov_h = ui->ipm_fovh_sbox->value();
      ipm->fov_v = ui->ipm_fovv_sbox->value();
      ipm->cam_xpos = ui->ipm_camx_sbox->value();
      ipm->cam_height = ui->ipm_camheight_sbox->value();
      auto vptx = ui->ipm_vanishx_sbox->value();
      auto vpty = ui->ipm_vanishy_sbox->value();
      ipm->vanish_pt = cv::Point2i(vptx, vpty);
      ipm->buildMap(cv::Size(640,480),cv::Size(700,700));
  });

  connect(ui->load_btn,&QPushButton::clicked,[=]
  {
    auto file = QFileDialog::getOpenFileName(this,"Open Image","~");
    if(file.isNull())
      return;
    mat_ori = cv::imread(file.toStdString());
    img_item->setImage(mat_ori);
    ui->graphicsView->scene()->update();
  });

  connect(ui->ctable_btn,&QPushButton::clicked,[=]
  {
    std::stringstream ss;
    auto poly = yuv_item->ball_polygon;
    auto field_poly = yuv_item->field_polygon;
    auto obs_poly = yuv_item->obs_polygon;
    auto y_min = yuv_item->y_min;
    auto y_max = yuv_item->y_max;
    auto field_ymin = yuv_item->field_ymin;
    auto field_ymax = yuv_item->field_ymax;
    auto obs_ymin = yuv_item->obs_ymin;
    auto obs_ymax = yuv_item->obs_ymax;

    for(const auto& p : poly)
      ss << "(" << p.x() << ";" << p.y() << "), ";
    ROS_INFO("poly : %s", ss.str().c_str());

    //    ctable_gen->createYUVTable(256,poly,y_min,y_max,field_poly,field_ymin,field_ymax);
    ctable_gen->createYUVTable(256,poly,y_min,y_max,field_poly,field_ymin,field_ymax,obs_poly,obs_ymin,obs_ymax);
    color_segment->size = ctable_gen->size;
#ifdef GPU
    color_segment->copyTableToDevice();
#endif
  });

  connect(ui->segment_result_btn,&QPushButton::clicked,[=]
  {
    auto segment = ctable_gen->segmentImage(img_item->bgr_mat);
    cv::namedWindow("segment result",CV_WINDOW_NORMAL);
    cv::imshow("segment result",segment);
  });

  connect(ui->stream_cb,&QCheckBox::toggled,[=](bool checked)
  {
    img_item->stream = checked;
  });

  connect(ui->overlay_cb,&QCheckBox::toggled,[=](bool checked)
  {
    img_item->overlay = checked;
  });

  connect(ui->save_btn,&QPushButton::clicked,[=]
  {
    QString dir;
    if(ui->dir_line_edit->text().isEmpty())
      dir = QFileDialog::getExistingDirectory(this,QString("Save Settings"));
    else
      dir = ui->dir_line_edit->text();
    ctable_gen->saveTable(dir.toStdString());
    ipm->save(dir.toStdString());
    std::ofstream detect(dir.toStdString()+"/detection.txt",std::ios::out);
    auto detect_size = QString::number(ui->min_width_sbox->value()).toStdString();
    auto w2h = QString::number(ui->w2h_sbox->value()).toStdString();
    //    detect.write(detect_size.c_str(),sizeof(char)*detect_size.size());
    //    detect.write(w2h.c_str(),sizeof(char)*w2h.size());
    detect << detect_size << std::endl;
    detect << w2h << std::endl;
    detect.close();
  });

  connect(ui->load_table_btn,&QPushButton::clicked,[=]
  {
    QString dir;
    if(ui->dir_line_edit->text().isEmpty()) {
        auto dir = QFileDialog::getExistingDirectory(this,QString("Load Settings"));
        ui->dir_line_edit->setText(dir);
      }
    else
      dir = ui->dir_line_edit->text();
    ctable_gen->loadTable(dir.toStdString());
    color_segment->size = ctable_gen->size;
#ifdef GPU
    color_segment->copyTableToDevice();
#endif
    ipm->load(dir.toStdString());
    this->ui->ipm_fovh_sbox->setValue(ipm->fov_h);
    this->ui->ipm_fovv_sbox->setValue(ipm->fov_v);
    this->ui->ipm_camheight_sbox->setValue(ipm->cam_height);
    this->ui->ipm_camx_sbox->setValue(ipm->cam_xpos);
    this->ui->ipm_vanishx_sbox->setValue(ipm->vanish_pt.x);
    this->ui->ipm_vanishy_sbox->setValue(ipm->vanish_pt.y);
    std::ifstream detect(dir.toStdString()+"/detection.txt",std::ios::in);
    std::string detect_size;
    std::string max_box_diff;
    std::getline(detect,detect_size);
    std::getline(detect,max_box_diff);
    this->ui->min_width_sbox->setValue(std::stoi(detect_size));
    this->ui->w2h_sbox->setValue(QString(max_box_diff.c_str()).toDouble());
    detect.close();

    std::ifstream mapping(dir.toStdString()+"/inverse_mapping.txt",std::ios::in);
  });

  connect(ui->min_width_sbox,SIGNAL(valueChanged(int)),this,SLOT(setMinBox(int)));
  connect(ui->w2h_sbox,SIGNAL(valueChanged(double)),this,SLOT(setMaxW2H(double)));
}

void Widget::setupCallback()
{
  img_item->hover_cb = [&](QPointF uv, qreal y)
  {
    yuv_item->setHighlightPoint(true,y,uv);
    ui->yuv_graphicsView->scene()->update();
  };

  img_item->image_cb = [&]
  {
    yuv_item->readMat(mat_ori);
    ui->yuv_graphicsView->scene()->update();
  };

  ctable_gen->load_table_cb = [&](QPolygonF poly, int y_min, int y_max,
      QPolygonF field, int field_min, int field_max,
      QPolygonF obs, int obs_min, int obs_max, size_t size)
  {
    ROS_INFO("load table callback");
    yuv_item->ball_polygon = poly.translated(QPointF(-(double)size/2,-(double)size/2));
    yuv_item->y_min = y_min;
    yuv_item->y_max = y_max;
    yuv_item->field_polygon = field.translated(QPointF(-(double)size/2,-(double)size/2));
    yuv_item->field_ymin = field_min;
    yuv_item->field_ymax = field_max;
    yuv_item->obs_polygon = obs.translated(QPointF(-(double)size/2,-(double)size/2));
    yuv_item->obs_ymin = obs_min;
    yuv_item->obs_ymax = obs_max;
    std::stringstream ss;
    for(const auto& p : yuv_item->ball_polygon)
      ss << "(" << p.x() << "," << p.y() << ")";
    ROS_INFO("load table callback : %d %s", size, ss.str().c_str());
    ui->yuv_graphicsView->scene()->update();
  };
}

QVector<QImage> Widget::createYUV(int y, int u, int v)
{
  QVector<QImage> ret;
  ret.resize(y);
  for(int z=0; z<y; z++)
  {
    cv::Mat m = cv::Mat::zeros(u,v,CV_8UC3);
    for(int py=0; py<u; py++)
      for(int px=0; px<v; px++)
      {
        int vv = px*255/v;
        int vu = py*255/u;
        int vy = z*255/y;
        uint8_t r, g, b;
        yuv2rgb(vy,vu,vv,&r,&g,&b);
        m.at<cv::Vec3b>(px,py)[0]=b;
        m.at<cv::Vec3b>(px,py)[1]=g;
        m.at<cv::Vec3b>(px,py)[2]=r;
      }
    ret[z] = cvMatToQImage(m);
  }
  return ret;
}

QImage Widget::readImage(const QString &file)
{
  return QImage(file);
}

void Widget::resizeEvent(QResizeEvent *event)
{
  auto s = event->size();
  auto w = s.width()-10;
  auto h = s.height()-50;
  ui->graphicsView->scene()->setSceneRect(-w/2,-h/2,w,h);
}

void Widget::closeEvent(QCloseEvent *event)
{
  if(ipm_widget->isVisible())
    ipm_widget->close();
}

void Widget::setMinBox(int width)
{
  ball_seeker->setMinBox(width);
}

void Widget::setMaxW2H(double value)
{
  ball_seeker->setMaxW2HDiff(value);
}
