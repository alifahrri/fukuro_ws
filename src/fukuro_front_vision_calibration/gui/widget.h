#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include "fukuro_front_vision/ipm.h"
#include "fukuro_front_vision/ballseeker.h"
#include "fukuro_front_vision/colorsegment.h"

namespace Ui {
class Widget;
}

class IPMWidget;
class YUVItem;
class ImageItem;
class ColorTableGenerator;

class Widget : public QWidget
{
  Q_OBJECT

public:
  explicit Widget(QWidget *parent = 0);
  void updateImage(const sensor_msgs::ImageConstPtr &msg);
  ~Widget();

public:
  ImageItem *img_item;
  IPMWidget *ipm_widget;
  fukuro::BallSeeker *ball_seeker;
  fukuro::ColorSegment *color_segment;
  fukuro::IPM *ipm;

private:
  Ui::Widget *ui;
  cv::Mat mat_ori;
  YUVItem *yuv_item;
  QVector<QImage> lookup;
  ColorTableGenerator *ctable_gen;

private:
  void setupConnection();
  void setupCallback();
  QVector<QImage> createYUV(int y, int u, int v);
  QImage readImage(const QString &file);
  void resizeEvent(QResizeEvent *event);
  void closeEvent(QCloseEvent *event);

private slots:
  void setMinBox(int width);
  void setMaxW2H(double value);
};

#endif // WIDGET_H
