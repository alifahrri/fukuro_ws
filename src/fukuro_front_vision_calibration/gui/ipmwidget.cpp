#include "ipmwidget.h"
#include "ui_ipmwidget.h"
#include "graphicsview.h"
#include "imageitem.h"
#include <QGraphicsView>

IPMWidget::IPMWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::IPMWidget),
  img_item(new ImageItem)
{
  ui->setupUi(this);
  auto rect = ui->graphicsView->rect();
  ui->graphicsView->setScene(new QGraphicsScene(rect,this));
  ui->graphicsView->scene()->addItem(img_item);

  setWindowTitle("Inverse Perspective Map");
}

IPMWidget::~IPMWidget()
{
  delete ui;
}
