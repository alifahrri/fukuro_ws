#include "yuvitem.h"
#include "util.hpp"
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsWidget>
#include <QGraphicsSceneMouseEvent>
#include <opencv2/opencv.hpp>
#include <ros/ros.h>

YUVItem::YUVItem(int w, int h)
  : width(w),
    height(h)
{

}

void YUVItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  auto mat = painter->matrix();
  auto px_size = pixmap.size();
  auto w = px_size.width();
  auto h = px_size.height();
  auto bound = QRectF(-w/2,-h/2,w,h);
  painter->drawPixmap(-w/2,-h/2,w,h,pixmap);
  painter->setBrush(Qt::NoBrush);
  painter->setPen(Qt::black);
  painter->drawRect(bound);
  if(ball_polygon.size())
  {
    painter->setPen(Qt::red);
    painter->setBrush(Qt::NoBrush);
    painter->drawPolygon(ball_polygon);
    painter->drawPoints(ball_polygon);
  }
  if(field_polygon.size())
  {
    painter->setPen(Qt::green);
    painter->setBrush(Qt::NoBrush);
    painter->drawPolygon(field_polygon);
    painter->drawPoints(field_polygon);
  }
  if(obs_polygon.size())
  {
    painter->setPen(Qt::darkGray);
    painter->setBrush(Qt::NoBrush);
    painter->drawPolygon(obs_polygon);
    painter->drawPoints(obs_polygon);
  }

  if(draw_highlight)
  {
    painter->setBrush(Qt::blue);
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(highlightpoint_uv-QPoint(w/2,h/2),2,2);
  }

  auto m = mat;
  zoom_x = m.m11();
  zoom_y = m.m22();
  m.scale(std::fabs(1.0/m.m11()),std::fabs(1.0/m.m22()));
  painter->setMatrix(m);

  if(pixmap.size().isEmpty())
    return;
  auto diameter = 5.0;
  auto rect = QRectF(-w/2,h/2-diameter/2,w,diameter);
  painter->setBrush(Qt::blue);
  painter->setPen(Qt::blue);
  painter->drawRect(rect);

  // draw field y point
  {
    auto y_min_pt = QPointF(-w/2+field_ymin*w/255,h/2);
    auto y_max_pt = QPointF(-w/2+field_ymax*w/255,h/2);
    painter->setBrush(Qt::green);
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(y_min_pt,diameter,diameter);
    painter->drawEllipse(y_max_pt,diameter,diameter);
  }

  // draw obstacle y point
  {
    auto y_min_pt = QPointF(-w/2+obs_ymin*w/255,h/2);
    auto y_max_pt = QPointF(-w/2+obs_ymax*w/255,h/2);
    painter->setBrush(Qt::darkGray);
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(y_min_pt,diameter,diameter);
    painter->drawEllipse(y_max_pt,diameter,diameter);
  }

  // draw ball y point
  {
    auto y_min_pt = QPointF(-w/2+y_min*w/255,h/2);
    auto y_max_pt = QPointF(-w/2+y_max*w/255,h/2);
    painter->setBrush(Qt::red);
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(y_min_pt,diameter,diameter);
    painter->drawEllipse(y_max_pt,diameter,diameter);
  }

  if(draw_highlight)
  {
    painter->setBrush(Qt::blue);
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(QPointF(highlightpoint_y-diameter/2-w/2,h/2),diameter,diameter);
  }

  painter->setMatrix(mat);
}

void YUVItem::setHighlightPoint(bool draw, qreal y, QPointF uv)
{
  highlightpoint_uv = uv;
  highlightpoint_y = y;
  draw_highlight = draw;
}

void YUVItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
  auto px_size = pixmap.size();
  auto w = px_size.width();
  auto h = px_size.height();
  auto y_min_pt = QPointF(-w/2+y_min*w/255,h/2);
  auto y_max_pt = QPointF(-w/2+y_max*w/255,h/2);
  auto diameter = 10;
  auto pos = event->pos();
  pos *= zoom_x;
  auto dp_min = pos - y_min_pt;
  auto dp_max = pos - y_max_pt;

  if(dp_min.manhattanLength()<=diameter)
    return;
  else if(dp_max.manhattanLength()<=diameter)
    return;

  if(!ball_polygon.size())
  {
    ball_polygon.push_back(QPoint(-10,-10));
    ball_polygon.push_back(QPoint(-10,10));
    ball_polygon.push_back(QPoint(10,10));
    ball_polygon.push_back(QPoint(10,-10));
  }
  else
  {
    auto dp_min = 100000000;
    auto insert_point = 0;
    for(size_t i=0; i<ball_polygon.size(); i++)
    {
      auto p = ball_polygon.at(i);
      auto dp = event->pos() - p;
      if(dp.manhattanLength() < dp_min)
      {
        insert_point = i;
        dp_min = dp.manhattanLength();
      }
    }
    ball_polygon.insert(insert_point,event->pos());
    active_poly_pt = &ball_polygon[insert_point];
  }
  this->update();
}

void YUVItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  auto px_size = pixmap.size();
  auto w = px_size.width();
  auto h = px_size.height();
  auto y_min_pt = QPointF(-w/2+y_min*w/255,h/2);
  auto y_max_pt = QPointF(-w/2+y_max*w/255,h/2);
  auto field_ymin_pt = QPointF(-w/2+field_ymin*w/255,h/2);
  auto field_ymax_pt = QPointF(-w/2+field_ymax*w/255,h/2);
  auto obs_ymin_pt = QPointF(-w/2+obs_ymin*w/255,h/2);
  auto obs_ymax_pt = QPointF(-w/2+obs_ymax*w/255,h/2);
  auto diameter = 10;
  auto pos = event->pos();
  pos *= zoom_x;
  auto dp_min = pos - y_min_pt;
  auto dp_max = pos - y_max_pt;
  auto field_dp_min = pos - field_ymin_pt;
  auto field_dp_max = pos - field_ymax_pt;
  auto obs_dp_min = pos - obs_ymin_pt;
  auto obs_dp_max = pos - obs_ymax_pt;

  if(dp_min.manhattanLength()<=diameter)
  {
    set_y = 1;
    return;
  }
  else if(dp_max.manhattanLength()<=diameter)
  {
    set_y = 2;
    return;
  }
  else if(field_dp_min.manhattanLength()<=diameter)
  {
    set_y = 3;
    return;
  }
  else if(field_dp_max.manhattanLength()<=diameter)
  {
    set_y = 4;
    return;
  }
  else if(obs_dp_min.manhattanLength()<=diameter)
  {
    set_y = 5;
    return;
  }
  else if(obs_dp_max.manhattanLength()<=diameter)
  {
    set_y = 6;
    return;
  }
  else
    set_y = 0;

  if(!ball_polygon.size())
  {
    ball_polygon.push_back(QPoint(-10,-10));
    ball_polygon.push_back(QPoint(-10,10));
    ball_polygon.push_back(QPoint(10,10));
    ball_polygon.push_back(QPoint(10,-10));

    field_polygon.push_back(QPoint(-15,-15));
    field_polygon.push_back(QPoint(-15,15));
    field_polygon.push_back(QPoint(15,15));
    field_polygon.push_back(QPoint(15,-15));

    obs_polygon.push_back(QPoint(-5,-5));
    obs_polygon.push_back(QPoint(-5,5));
    obs_polygon.push_back(QPoint(5,5));
    obs_polygon.push_back(QPoint(5,-5));
  }
  else
  {
    auto pos = event->pos();
    auto dp_min = 1e6;
    auto idx = 0;
    QPolygonF *poly = nullptr;
    for(size_t i=0; i<ball_polygon.size(); i++)
    {
      auto p = ball_polygon.at(i);
      auto dp = pos - p;
      if(dp.manhattanLength() < dp_min)
      {
        idx = i;
        dp_min = dp.manhattanLength();
        poly = &ball_polygon;
      }
    }
    for(size_t i=0; i<field_polygon.size(); i++)
    {
        auto p = field_polygon.at(i);
        auto dp = pos - p;
        if(dp.manhattanLength() < dp_min)
        {
          idx = i;
          dp_min = dp.manhattanLength();
          poly = &field_polygon;
        }
    }
    for(size_t i=0; i<obs_polygon.size(); i++)
    {
        auto p = obs_polygon.at(i);
        auto dp = pos - p;
        if(dp.manhattanLength() < dp_min)
        {
          idx = i;
          dp_min = dp.manhattanLength();
          poly = &obs_polygon;
        }
    }
    active_poly_pt = &(*poly)[idx];
    (*poly)[idx]= pos;
//    ball_polygon[idx] = pos;
  }
  this->update();
}

void YUVItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  auto px_size = this->pixmap.size();
  auto w = px_size.width();
  auto h = px_size.height();
  auto y_min_pt = QPointF(-w/2+y_min/255*w,h/2);
  auto y_max_pt = QPointF(-w/2+y_max/255*w,h/2);
  auto diameter = 10;
  auto pos = event->pos();
  pos *= zoom_x;
  auto dp_min = pos - y_min_pt;
  auto dp_max = pos - y_max_pt;

  ROS_INFO("pos : (%.1f,%.1f); "
           "event->pos() : (%.1f,%.1f), "
           "y_max_pt : (%.1f,%.1f), "
           "mlength : (%.1f) "
           "w : (%d)",
           pos.x(), pos.y(),
           event->pos().x(), event->pos().y(),
           y_max_pt.x(), y_max_pt.y(),
           dp_max.manhattanLength(),
           w);

  switch (set_y) {
  case 1:
    y_min = ((pos.x()+(double)w/2.0)*255.0/(double)(w-1));
    y_min = std::max(0,y_min);
    y_min = std::min(255,y_min);
    ROS_INFO("y_min : %.1f %.1f",y_min, (pos.x()+(double)w/2.0));
    break;
  case 2:
    y_max = ((pos.x()+(double)w/2.0)*255.0/(double)(w-1));
    y_max = std::max(0,y_max);
    y_max = std::min(255,y_max);
    ROS_INFO("y_max : %.1f %.1f",y_max, (pos.x()+(double)w/2.0));
    break;
  case 3:
    field_ymin = ((pos.x()+(double)w/2.0)*255.0/(double)(w-1));
    field_ymin = std::max(0,field_ymin);
    field_ymin = std::min(255,field_ymin);
    ROS_INFO("y_max : %.1f %.1f",field_ymin, (pos.x()+(double)w/2.0));
    break;
  case 4:
    field_ymax = ((pos.x()+(double)w/2.0)*255.0/(double)(w-1));
    field_ymax = std::max(0,field_ymax);
    field_ymax = std::min(255,field_ymax);
    ROS_INFO("y_max : %.1f %.1f",field_ymin, (pos.x()+(double)w/2.0));
    break;
  case 5:
    obs_ymin = ((pos.x()+(double)w/2.0)*255.0/(double)(w-1));
    obs_ymin = std::max(0,obs_ymin);
    obs_ymin = std::min(255,obs_ymin);
    ROS_INFO("y_max : %.1f %.1f",obs_ymin, (pos.x()+(double)w/2.0));
    break;
  case 6:
    obs_ymax = ((pos.x()+(double)w/2.0)*255.0/(double)(w-1));
    obs_ymax = std::max(0,obs_ymax);
    obs_ymax = std::min(255,obs_ymax);
    ROS_INFO("y_max : %.1f %.1f",obs_ymin, (pos.x()+(double)w/2.0));
    break;
  default:
    if(active_poly_pt)
      *active_poly_pt = event->pos();
    break;
  }

  this->update();
}

void YUVItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
  active_poly_pt = nullptr;
}

QRectF YUVItem::boundingRect() const
{
  if(scene())
    return this->scene()->sceneRect();
  auto px_size = pixmap.size();
  auto w = px_size.width()+50;
  auto h = px_size.height()+50;
  return QRectF(-w/2,-h/2,w,h);
}

void YUVItem::readMat(cv::Mat &mat)
{
  cv::Mat yuv(mat.rows,mat.cols,CV_8UC3);
  cv::Mat yuv_table(256,256,CV_8UC3);
  cv::cvtColor(mat,yuv,CV_BGR2YUV);
  yuv_table.setTo(cv::Scalar(255,255,255));
  for(size_t i=0; i<mat.rows; i++)
    for(size_t j=0; j<mat.cols; j++)
    {
      auto color = mat.at<cv::Vec3b>(i,j);
      auto yuv_idx = yuv.at<cv::Vec3b>(i,j);
      yuv_table.at<cv::Vec3b>(int(yuv_idx[2]),int(255-yuv_idx[1])) = color;
    }
  //  cv::imshow("yuv_table",yuv_table);
  pixmap = QPixmap::fromImage(cvMatToQImage(yuv_table));
}
