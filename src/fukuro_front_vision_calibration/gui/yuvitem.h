#ifndef YUVITEM_H
#define YUVITEM_H

#include <QGraphicsItem>

namespace cv {
class Mat;
}

class YUVItem : public QGraphicsItem
{
public:
  YUVItem(int w=64, int h=64);
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  QRectF boundingRect() const;

public:
  void readMat(cv::Mat &mat);
  void setHighlightPoint(bool draw = false, qreal y = 0.0, QPointF uv = QPointF());

public:
  QPixmap pixmap;
  QPolygonF ball_polygon;
  QPolygonF field_polygon;
  QPolygonF obs_polygon;
  int y_min = 0;
  int y_max = 255;
  int field_ymin = 0;
  int field_ymax = 255;
  int obs_ymin = 0;
  int obs_ymax = 255;

private:
  void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
  int width;
  int height;
  bool draw_highlight = false;
  double zoom_x;
  double zoom_y;
  int set_y = 0;
  QPointF *active_poly_pt = nullptr;
  QPointF highlightpoint_uv;
  qreal highlightpoint_y;
};

#endif // YUVITEM_H
