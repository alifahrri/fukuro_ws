#ifndef IMAGEITEM_H
#define IMAGEITEM_H

#include <QGraphicsItem>
#include <functional>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <opencv2/opencv.hpp>

namespace cv {
  class Mat;
}

//namespace fukuro {
//  struct ImageSegment;
//  typedef std::vector<ImageSegment> ImageSegments;
//}

class ImageItem : public QGraphicsItem
{
public:
  typedef std::function<void(QPointF,qreal)> HoverCallback;
  typedef std::function<void()> ImageCallback;

public:
  ImageItem();
  void updateImage(const sensor_msgs::ImageConstPtr &msg);
  void overlayMat(const cv::Mat &in1, const cv::Mat &in2, cv::Mat &out);
  void setImage(const cv::Mat &mat);
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  QRectF boundingRect() const;

public:
//  fukuro::ImageSegments ball_segments;
  QVector<QRectF> boxes;
  HoverCallback hover_cb = nullptr;
  ImageCallback image_cb = nullptr;
  bool stream = false;
  bool overlay = false;
  cv::Mat yuv_mat;
  cv::Mat bgr_mat;

private:
  void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
  QPointF ruler[2];
  QPixmap img;
  QPixmap overlay_img;
  cv::Mat overlay_mat;
};

#endif // IMAGEITEM_H
