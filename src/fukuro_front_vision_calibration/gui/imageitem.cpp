#include "imageitem.h"
#include "fukuro_front_vision/ballseeker.h"
#include <ros/ros.h>
#include <util.hpp>
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneHoverEvent>
#include <cv_bridge/cv_bridge.h>

ImageItem::ImageItem()
{
  setAcceptHoverEvents(true);
}

void ImageItem::updateImage(const sensor_msgs::ImageConstPtr &msg)
{
  ROS_INFO("image callback");
  if(stream)
  {
    cv_bridge::CvImageConstPtr cv_ptr;
    cv_ptr = cv_bridge::toCvShare(msg,sensor_msgs::image_encodings::BGR8);
    setImage(cv_ptr->image);
  }
}

void ImageItem::setImage(const cv::Mat &mat)
{
  bgr_mat.create(mat.rows,mat.cols,CV_8UC3);
  yuv_mat.create(mat.rows,mat.cols,CV_8UC3);
  mat.copyTo(bgr_mat);
  cv::cvtColor(bgr_mat,yuv_mat,CV_BGR2YUV);
  img = QPixmap::fromImage(cvMatToQImage(bgr_mat));
  if(image_cb)
    image_cb();
  if(this->scene())
    this->scene()->update();
}

void ImageItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  auto imsize = img.size();
  auto w = imsize.width();
  auto h = imsize.height();
  painter->drawPixmap(-w/2,-h/2,w,h,img);
  QLineF line(ruler[0], ruler[1]);
  auto l = line.length();
  if(l > 5) {
      painter->setBrush(Qt::blue);
      painter->setPen(Qt::blue);
      painter->drawLine(line);
      painter->drawEllipse(ruler[0],2,2);
      painter->drawEllipse(ruler[1],2,2);
      line.setLength(l/2);
      painter->setBrush(Qt::black);
      painter->setPen(Qt::black);
      painter->drawText(line.p2(),QString::number(l));
    }

  if(boxes.size()) {
      auto pen = painter->pen();
      pen.setBrush(Qt::red);
      pen.setWidthF(2.0);
      painter->setPen(pen);
      painter->drawRects(boxes);
    }
}

QRectF ImageItem::boundingRect() const
{
  auto imsize = img.size();
  auto w = imsize.width();
  auto h = imsize.height();
  return QRectF(-w/2,-h/2,w,h);
}

void ImageItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
  if(hover_cb)
  {
    auto pos = mapToScene(event->pos());
    pos += QPointF(yuv_mat.cols/2,yuv_mat.rows/2);
    //    ROS_INFO("ImageItem::hoverMoveEvent %f,%f->%f,%f",event->pos().x(),event->pos().y(),pos.x(),pos.y());
    if(pos.x()<yuv_mat.cols
       && pos.y()<yuv_mat.rows
       && pos.x() > 0
       && pos.y() > 0)
    {
      auto yuv = yuv_mat.at<cv::Vec3b>(cv::Point(pos.x(),pos.y()));
      hover_cb(QPointF(255-yuv[1],yuv[2]),yuv[0]);
    }
    }
}

void ImageItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  ruler[0] = event->pos();
}

void ImageItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  ruler[1] = event->pos();
}

void ImageItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
  ruler[1] = event->pos();
}

void ImageItem::overlayMat(const cv::Mat &in1, const cv::Mat &in2, cv::Mat &out)
{
  cv::addWeighted(in1,0.75,in2,0.25,0.0,out,out.type());
}
