#ifndef IPMWIDGET_H
#define IPMWIDGET_H

#include <QWidget>

namespace Ui {
  class IPMWidget;
}

class ImageItem;

class IPMWidget : public QWidget
{
  Q_OBJECT

public:
  explicit IPMWidget(QWidget *parent = 0);
  ~IPMWidget();

public:
  ImageItem *img_item;

private:
  Ui::IPMWidget *ui;
};

#endif // IPMWIDGET_H
