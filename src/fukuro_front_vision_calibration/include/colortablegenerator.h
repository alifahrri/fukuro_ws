#ifndef COLORTABLEGENERATOR_H
#define COLORTABLEGENERATOR_H

#include <QtCore>
#include <opencv2/opencv.hpp>
#include <functional>
#include <util.hpp>

using std::vector;

class ColorTableGenerator
{
public:
  typedef vector<uint8_t> ColorTable;
  typedef std::function<void(QPolygonF,int,int,QPolygonF,int,int,QPolygonF,int,int,size_t)> LoadTableCallback;

public:
  ColorTableGenerator();
  // TODO remove this, use ColorSegment class instead
  cv::Mat segmentImage(cv::Mat &src);
  void saveTable(const std::string &directory);
  void loadTable(const std::string &directory);
  void createYUVTable(int size, QPolygonF vu_ball_poly, int ball_y_min, int ball_y_max, QPolygonF vu_field_poly, int field_y_min, int field_y_max);
  void createYUVTable(int size, QPolygonF vu_ball_poly, int ball_y_min, int ball_y_max, QPolygonF vu_field_poly, int field_y_min, int field_y_max, QPolygonF vu_obs_poly, int obs_y_min, int obs_y_max);

public:
  LoadTableCallback load_table_cb = nullptr;
  ColorTable *table = nullptr;
  QPolygonF ball_polygon;
  QPolygonF field_polygon;
  QPolygonF obs_polygon;
  size_t size = 256;
  int ball_y_min;
  int ball_y_max;
  int field_y_min;
  int field_y_max;
  int obs_y_min;
  int obs_y_max;
};

#endif // COLORTABLEGENERATOR_H
