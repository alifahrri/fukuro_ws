#include "colortablegenerator.h"
#include <ros/ros.h>

#define BALL_COLOR      0
#define OBSTACLE_COLOR  1
#define FIELD_COLOR     2
#define UNKNOWN_COLOR   3

ColorTableGenerator::ColorTableGenerator() :
  table(new ColorTable)
{

}

//TODO remove this function, use ColorSegment class instead
cv::Mat ColorTableGenerator::segmentImage(cv::Mat &src)
{
  ROS_INFO("creating Segment Image");
  cv::Mat yuv;
  cv::Mat result;
  yuv.create(src.rows,src.cols,CV_8UC3);
  result.create(src.rows,src.cols,CV_8UC3);
  cv::cvtColor(src,yuv,CV_BGR2YUV);

  if(!table->size())
  {
    ROS_ERROR("table is empty! create table first");
    return result;
  }

  uchar *ptr_result = result.data;
  uchar *ptr_yuv = yuv.data;
  auto s = this->size;

  for(size_t i=0; i<yuv.rows; i++)
    for(size_t j=0; j<yuv.cols; j++)
    {
      auto idx = result.step[0]*i+result.step[1]*j;
      auto k = yuv.step[0]*i + yuv.step[1]*j;
      auto color = (*table)[ptr_yuv[k]*s*s + ptr_yuv[k+1]*s + ptr_yuv[k+2]];
      if(color == BALL_COLOR)
      {
        ptr_result[idx]   = 0;
        ptr_result[idx+1] = 0;
        ptr_result[idx+2] = 255;
      }
      else
      {
        ptr_result[idx]   = 255;
        ptr_result[idx+1] = 255;
        ptr_result[idx+2] = 255;
      }
    }
  ROS_INFO("creating Segment Image : Done");
  return result;
}

void ColorTableGenerator::saveTable(const std::string &directory)
{
  ROS_INFO("saving table");
  auto calib_setting_path = directory + "/calibration.xml";
  auto table_path = directory + "/CTable.dat";

  cv::FileStorage calibration(calib_setting_path, cv::FileStorage::WRITE);
  calibration << "Color_Calib" << "[";
  calibration << "{";

  calibration << "ball_poly_pts" << "[";
  for(const QPointF& p : ball_polygon)
    calibration << (double)p.x() << (double)p.y();
  calibration << "]";

  calibration << "field_poly_pts" << "[";
  for(const QPointF& p : field_polygon)
    calibration << (double)p.x() << (double)p.y();
  calibration << "]";

  calibration << "obs_poly_pts" << "[";
  for(const QPointF& p : obs_polygon)
    calibration << (double)p.x() << (double)p.y();
  calibration << "]";

  calibration << "ball_y" << "[";
  calibration << this->ball_y_min
              << this->ball_y_max;
  calibration << "]";

  calibration << "field_y" << "[";
  calibration << this->field_y_min
              << this->field_y_max;
  calibration << "]";

  calibration << "obs_y" << "[";
  calibration << this->obs_y_min
              << this->obs_y_max;
  calibration << "]";

  calibration << "size" << "[";
  calibration << (int&)(this->size);
  calibration << "]";

  calibration << "}";
  calibration << "]";
  calibration.release();

  std::ofstream table_writer(table_path, std::ios::binary | std::ios::out);
  table_writer.write((char*)table->data(),sizeof(ColorTable::value_type)*(table->size()));
  table_writer.close();
  ROS_INFO("saving table done");
}

void ColorTableGenerator::loadTable(const std::string &directory)
{
  ROS_INFO("loading table");
  auto calib_setting_path = directory + "/calibration.xml";
  auto table_path = directory + "/CTable.dat";

  cv::FileStorage calibration(calib_setting_path, cv::FileStorage::READ);
  cv::FileNode calib_result = calibration["Color_Calib"];
  cv::FileNodeIterator it = calib_result.begin();
  cv::FileNodeIterator it_end = calib_result.end();
  std::vector<double> poly_pts;
  std::vector<int> y_value;
  for(int i = 0; it != it_end; ++it)
  {
    std::stringstream ss;

    (*it)["ball_poly_pts"] >> poly_pts;
    this->ball_polygon.clear();
    for(size_t i=0; i<poly_pts.size(); i+=2)
      this->ball_polygon.push_back(QPointF(poly_pts[i],poly_pts[i+1]));
    (*it)["ball_y"] >> y_value;
    this->ball_y_min = y_value[0];
    this->ball_y_max = y_value[1];

    (*it)["field_poly_pts"] >> poly_pts;
    this->field_polygon.clear();
    for(size_t i=0; i<poly_pts.size(); i+=2)
      this->field_polygon.push_back(QPointF(poly_pts[i],poly_pts[i+1]));
    (*it)["field_y"] >> y_value;
    this->field_y_min = y_value[0];
    this->field_y_max = y_value[1];

    (*it)["obs_poly_pts"] >> poly_pts;
    this->obs_polygon.clear();
    for(size_t i=0; i<poly_pts.size(); i+=2)
      this->obs_polygon.push_back(QPointF(poly_pts[i],poly_pts[i+1]));
    (*it)["obs_y"] >> y_value;
    this->obs_y_min = y_value[0];
    this->obs_y_max = y_value[1];

    (*it)["size"] >> (int&)this->size;
  }
  calibration.release();

  ROS_INFO("size : %d", size);
  this->table->resize(size*size*size);

  std::stringstream ss;
  for(size_t i=0; i<poly_pts.size(); i+=2)
    ss << "(" << poly_pts[i] << "," << poly_pts[i+1] << ")";
  ROS_INFO("poly : %s", ss.str().c_str());
  ROS_INFO("y_min : %d y_max : %d", ball_y_min, ball_y_max);

  std::fstream table_reader(table_path, std::ios::binary | std::ios::in);
  if(!table_reader.is_open())
    ROS_ERROR("cannot open CTable");
  else
  {
    ROS_INFO("table size : %d", table->size());
    table_reader.read((char*)table->data(), sizeof(ColorTable::value_type)*(table->size()));
  }
  table_reader.close();

  ROS_INFO("loading table done");

  if(load_table_cb)
    load_table_cb(ball_polygon,ball_y_min,ball_y_max,
                  field_polygon,field_y_min,field_y_max,
                  obs_polygon,obs_y_min,obs_y_max,
                  size);
}

void ColorTableGenerator::createYUVTable(int size, QPolygonF vu_ball_poly, int ball_y_min, int ball_y_max, QPolygonF vu_field_poly, int field_y_min, int field_y_max)
{
  ROS_INFO("creating YUV table");
  this->size = size;
  this->ball_y_min = ball_y_min;
  this->ball_y_max = ball_y_max;
  this->field_y_max = field_y_max;
  this->field_y_min = field_y_min;
  auto hs = size/2;
  table->resize(size*size*size);
  ball_polygon = vu_ball_poly.translated(QPointF(hs,hs));
  field_polygon = vu_field_poly.translated(QPointF(hs,hs));
  for(size_t y=0; y<size; y++)
    for(size_t u=0; u<size; u++)
      for(size_t v=0; v<size; v++)
      {
        auto idx = y*size*size+u*size+v;
        auto inside_ball_poly = ball_polygon.containsPoint(QPointF(255-u,v),Qt::OddEvenFill);
        auto inside_field_poly = field_polygon.containsPoint(QPointF(255-u,v),Qt::OddEvenFill);
        if(inside_ball_poly && (y>=ball_y_min && y<=ball_y_max))
          (*table)[idx] = BALL_COLOR;
        else if(inside_field_poly && (y>=field_y_min && y<=field_y_max))
          (*table)[idx] = FIELD_COLOR;
        else
          (*table)[idx] = UNKNOWN_COLOR;
      }
  std::stringstream ss;
  ss << "ball";
  for(const auto& p : ball_polygon)
    ss << "(" << p.x() << "," << p.y() << ")";
  ss << " field";
  for(const auto& p : field_polygon)
    ss << "(" << p.x() << "," << p.y() << ")";
  ROS_INFO("poly : %s", ss.str().c_str());
  ROS_INFO("creating YUV table : DONE");
}

void ColorTableGenerator::createYUVTable(int size, QPolygonF vu_ball_poly, int ball_y_min, int ball_y_max, QPolygonF vu_field_poly, int field_y_min, int field_y_max, QPolygonF vu_obs_poly, int obs_y_min, int obs_y_max)
{
  ROS_INFO("creating YUV table");
  this->size = size;
  this->ball_y_min = ball_y_min;
  this->ball_y_max = ball_y_max;
  this->field_y_max = field_y_max;
  this->field_y_min = field_y_min;
  this->obs_y_max = obs_y_max;
  this->obs_y_min = obs_y_min;
  auto hs = size/2;
  table->resize(size*size*size);
  ball_polygon = vu_ball_poly.translated(QPointF(hs,hs));
  field_polygon = vu_field_poly.translated(QPointF(hs,hs));
  obs_polygon = vu_obs_poly.translated(QPointF(hs,hs));
  for(size_t y=0; y<size; y++)
    for(size_t u=0; u<size; u++)
      for(size_t v=0; v<size; v++)
      {
        auto idx = y*size*size+u*size+v;
        auto inside_ball_poly = ball_polygon.containsPoint(QPointF(255-u,v),Qt::OddEvenFill);
        auto inside_field_poly = field_polygon.containsPoint(QPointF(255-u,v),Qt::OddEvenFill);
        auto inside_obs_poly = obs_polygon.containsPoint(QPointF(255-u,v),Qt::OddEvenFill);
        if(inside_ball_poly && (y>=ball_y_min && y<=ball_y_max))
          (*table)[idx] = BALL_COLOR;
        else if(inside_field_poly && (y>=field_y_min && y<=field_y_max))
          (*table)[idx] = FIELD_COLOR;
        else if(inside_obs_poly && (y>=obs_y_min && y<=obs_y_max))
          (*table)[idx] = OBSTACLE_COLOR;
        else
          (*table)[idx] = UNKNOWN_COLOR;
      }
  std::stringstream ss;
  ss << "ball";
  for(const auto& p : ball_polygon)
    ss << "(" << p.x() << "," << p.y() << ")";
  ss << " field";
  for(const auto& p : field_polygon)
    ss << "(" << p.x() << "," << p.y() << ")";
  ss << " obstacle";
  for(const auto& p : obs_polygon)
    ss << "(" << p.x() << "," << p.y() << ")";
  ROS_INFO("poly : %s", ss.str().c_str());
  ROS_INFO("creating YUV table : DONE");
}
