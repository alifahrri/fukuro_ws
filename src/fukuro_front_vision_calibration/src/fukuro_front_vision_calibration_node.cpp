#include <ros/ros.h>
#include <QApplication>
#include "imageitem.h"
#include "widget.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "fukuro_front_vision_calibration_node");
  ros::NodeHandle nh;

  QApplication app(argc,argv);
  Widget widget;

  ros::Subscriber front_cam_sub = nh.subscribe("/front_camera/image_raw",1,&Widget::updateImage, &widget);
  ros::AsyncSpinner spinner(2);
  spinner.start();

  widget.showMaximized();
  app.exec();
  return 0;
}
