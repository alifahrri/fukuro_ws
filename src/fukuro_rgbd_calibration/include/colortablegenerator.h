#ifndef COLORTABLEGENERATOR_H
#define COLORTABLEGENERATOR_H

#include <QtCore>
#include <opencv2/opencv.hpp>
#include <functional>
#include <util.hpp>

using std::vector;

class ColorTableGenerator
{
public:
  typedef vector<uint8_t> ColorTable;
  typedef std::function<void(QPolygonF,int,int,size_t)> LoadTableCallback;

public:
  ColorTableGenerator();
  cv::Mat segmentImage(cv::Mat &src);
  void saveTable(const std::string &directory);
  void loadTable(const std::string &directory);
  void createYUVTable(int size, QPolygonF vu_poly, int y_min, int y_max);

public:
  LoadTableCallback load_table_cb = nullptr;
  ColorTable table;
  QPolygonF poly;
  size_t size = 256;
  int y_min;
  int y_max;
};

#endif // COLORTABLEGENERATOR_H
