#include "colortablegenerator.h"
#include <ros/ros.h>

#define BALL_COLOR 0
#define UNKNOWN_COLOR 3

ColorTableGenerator::ColorTableGenerator()
{

}

cv::Mat ColorTableGenerator::segmentImage(cv::Mat &src)
{
  ROS_INFO("creating Segment Image");
  cv::Mat yuv;
  cv::Mat result;
  yuv.create(src.rows,src.cols,CV_8UC3);
  result.create(src.rows,src.cols,CV_8UC3);
  cv::cvtColor(src,yuv,CV_BGR2YUV);

  if(!table.size())
  {
    ROS_ERROR("table is empty! create table first");
    return result;
  }

  uchar *ptr_result = result.data;
  uchar *ptr_yuv = yuv.data;
  auto s = this->size;

  for(size_t i=0; i<yuv.rows; i++)
    for(size_t j=0; j<yuv.cols; j++)
    {
      auto idx = result.step[0]*i+result.step[1]*j;
      auto k = yuv.step[0]*i + yuv.step[1]*j;
      auto color = table[ptr_yuv[k]*s*s + ptr_yuv[k+1]*s + ptr_yuv[k+2]];
      if(color == BALL_COLOR)
      {
        ptr_result[idx]   = 0;
        ptr_result[idx+1] = 0;
        ptr_result[idx+2] = 255;
      }
      else
      {
        ptr_result[idx]   = 255;
        ptr_result[idx+1] = 255;
        ptr_result[idx+2] = 255;
      }
    }
  ROS_INFO("creating Segment Image : Done");
  return result;
}

void ColorTableGenerator::saveTable(const std::string &directory)
{
  ROS_INFO("saving table");
  auto calib_setting_path = directory + "/calibration.xml";
  auto table_path = directory + "/CTable.dat";

  cv::FileStorage calibration(calib_setting_path, cv::FileStorage::WRITE);
  calibration << "Color_Calib" << "[";
  calibration << "{";

  calibration << "poly_pts" << "[";
  for(const QPointF& p : poly)
    calibration << (double)p.x() << (double)p.y();
  calibration << "]";

  calibration << "y" << "[";
  calibration << this->y_min
              << this->y_max;
  calibration << "]";

  calibration << "size" << "[";
  calibration << (int&)(this->size);
  calibration << "]";

  calibration << "}";
  calibration << "]";
  calibration.release();

  std::ofstream table_writer(table_path, std::ios::binary | std::ios::out);
  table_writer.write((char*)table.data(),sizeof(ColorTable::value_type)*table.size());
  table_writer.close();
  ROS_INFO("saving table done");
}

void ColorTableGenerator::loadTable(const std::string &directory)
{
  ROS_INFO("loading table");
  auto calib_setting_path = directory + "/calibration.xml";
  auto table_path = directory + "/CTable.dat";

  cv::FileStorage calibration(calib_setting_path, cv::FileStorage::READ);
  cv::FileNode calib_result = calibration["Color_Calib"];
  cv::FileNodeIterator it = calib_result.begin();
  cv::FileNodeIterator it_end = calib_result.end();
  std::vector<double> poly_pts;
  std::vector<int> y_value;
  for(int i = 0; it != it_end; ++it)
  {
    (*it)["poly_pts"] >> poly_pts;
    this->poly.clear();
    std::stringstream ss;
    for(size_t i=0; i<poly_pts.size(); i+=2)
      this->poly.push_back(QPointF(poly_pts[i],poly_pts[i+1]));
    (*it)["y"] >> y_value;
    this->y_min = y_value[0];
    this->y_max = y_value[1];
    (*it)["size"] >> (int&)this->size;
  }
  calibration.release();

  ROS_INFO("size : %d", size);
  this->table.resize(size*size*size);

  std::stringstream ss;
  for(size_t i=0; i<poly_pts.size(); i+=2)
    ss << "(" << poly_pts[i] << "," << poly_pts[i+1] << ")";
  ROS_INFO("poly : %s", ss.str().c_str());
  ROS_INFO("y_min : %d y_max : %d", y_min, y_max);

  std::fstream table_reader(table_path, std::ios::binary | std::ios::in);
  if(!table_reader.is_open())
    ROS_ERROR("cannot open CTable");
  else
  {
    ROS_INFO("table size : %d", table.size());
    table_reader.read((char*)table.data(), sizeof(ColorTable::value_type)*table.size());
  }
  table_reader.close();

  if(load_table_cb)
    load_table_cb(poly,y_min,y_max,size);

  ROS_INFO("loading table done");
}

void ColorTableGenerator::createYUVTable(int size, QPolygonF vu_poly, int y_min, int y_max)
{
  ROS_INFO("creating YUV table");
  this->size = size;
  this->y_min = y_min;
  this->y_max = y_max;
  auto hs = size/2;
  table.resize(size*size*size);
  poly = vu_poly.translated(QPointF(hs,hs));
  for(size_t y=0; y<size; y++)
    for(size_t u=0; u<size; u++)
      for(size_t v=0; v<size; v++)
      {
        auto idx = y*size*size+u*size+v;
        auto inside_poly = poly.containsPoint(QPointF(255-u,v),Qt::OddEvenFill);
        if(inside_poly && (y>=y_min && y<=y_max))
          table[idx] = BALL_COLOR;
        else
          table[idx] = UNKNOWN_COLOR;
      }
  std::stringstream ss;
  for(const auto& p : poly)
    ss << "(" << p.x() << "," << p.y() << ")";
  ROS_INFO("poly : %s", ss.str().c_str());
  ROS_INFO("creating YUV table : DONE");
}
