#include <ros/ros.h>
#include <QApplication>
#include "widget.h"
#include "imageitem.h"

int main(int argc, char **argv)
{
  ros::init(argc,argv,"fukuro_rgbd_calibration");
  QApplication app(argc,argv);
  ros::NodeHandle node;

  Widget widget;
  ros::Subscriber color_sub = node.subscribe("/camera/color/image_raw",1,&ImageItem::updateImage,widget.img_item);
  ros::Subscriber depth_sub = node.subscribe("/camera/depth/image_raw",1,&ImageItem::updateDepth,widget.img_item);
  ros::Subscriber depth_info_sub = node.subscribe("/camera/depth/camera_info",1,&ImageItem::updateDepthMatrix,widget.img_item);
  ros::AsyncSpinner spinner(3);

  spinner.start();
  widget.showMaximized();

  app.exec();
  return 0;
}
