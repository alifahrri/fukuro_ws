#include "widget.h"
#include "ui_widget.h"

#include "yuvitem.h"
#include "warpitem.h"
#include "imageitem.h"
#include "colortablegenerator.h"

#include "util.hpp"

#include <QRgb>
#include <QFileDialog>
#include <QResizeEvent>
#include <QGraphicsPixmapItem>

#include <ros/ros.h>

#define SCALE 0.75

Widget::Widget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::Widget),
  yuv_item(new YUVItem),
  warp_item(new WarpItem),
  img_item(new ImageItem),
  ctable_gen(new ColorTableGenerator)
{
  ui->setupUi(this);

  auto s = this->size();
  auto w = s.width()-10;
  auto h = s.height()-50;
  ui->graphicsView->setScene(new QGraphicsScene(-w/2,-h/2,w,h,this));
  ui->graphicsView->scale(SCALE,SCALE);

  ui->graphicsView->scene()->addItem(img_item);

  auto ys = ui->yuv_graphicsView->geometry().size();
  auto yw = ys.width();
  auto yh = ys.height();
  ui->yuv_graphicsView->setScene(new QGraphicsScene(-yw/2,-yh/2,yw,yh,this));

  ui->yuv_graphicsView->scene()->addItem(yuv_item);
  lookup = createYUV(64,64,64);

  ui->warp_graphicsView->setScene(new QGraphicsScene(warp_item->boundingRect(),this));
  ui->warp_graphicsView->scene()->addItem(warp_item);
  //  ui->warp_graphicsView->scene()->update();

  img_item->hover_cb = [&](QPointF uv, qreal y)
  {
    //    ROS_INFO("img_item hover callback %f,%f,%f",uv.x(),uv.y(),y);
    yuv_item->setHighlightPoint(true,y,uv);
    ui->yuv_graphicsView->scene()->update();
  };

  connect(ui->load_btn,&QPushButton::clicked,[=]
  {
    auto file = QFileDialog::getOpenFileName(this,"Open Image","~");
    if(file.isNull())
      return;
    mat_ori = cv::imread(file.toStdString());
    img_item->setImage(mat_ori);
    ui->graphicsView->scene()->update();
  });

  warp_item->warp_cb = [&](QVector<QPointF> ori, QVector<QPointF> warped)
  {
    img_item->setWarpMatrix(ori,warped);
  };

  img_item->image_cb = [&]
  {
    yuv_item->readMat(img_item->bgr_mat);
    ui->yuv_graphicsView->scene()->update();
  };

  ctable_gen->load_table_cb = [&](QPolygonF poly, int y_min, int y_max, size_t size)
  {
    ROS_INFO("load table callback");
    yuv_item->polygon = poly.translated(QPointF(-(double)size/2,-(double)size/2));
    yuv_item->y_min = y_min;
    yuv_item->y_max = y_max;
    std::stringstream ss;
    for(const auto& p : yuv_item->polygon)
      ss << "(" << p.x() << "," << p.y() << ")";
    ROS_INFO("load table callback : %d %s", size, ss.str().c_str());
    ui->yuv_graphicsView->scene()->update();
  };

  connect(ui->save_warp_btn,&QPushButton::clicked,[=]
  {
    auto dir = QFileDialog::getExistingDirectory(this,"Save Warp");
    this->warp_item->save(dir.toStdString());
  });

  connect(ui->load_warp_btn,&QPushButton::clicked,[=]
  {
    auto dir = QFileDialog::getExistingDirectory(this,"Save Warp");
    this->warp_item->load(dir.toStdString());
  });

  connect(ui->ctable_btn,&QPushButton::clicked,[=]
  {
    std::stringstream ss;
    auto poly = yuv_item->polygon;
    auto y_min = yuv_item->y_min;
    auto y_max = yuv_item->y_max;

    for(const auto& p : poly)
      ss << "(" << p.x() << ";" << p.y() << "), ";
    ROS_INFO("poly : %s", ss.str().c_str());

    ctable_gen->createYUVTable(256,poly,y_min,y_max);
  });

  connect(ui->segment_result_btn,&QPushButton::clicked,[=]
  {
    auto segment = ctable_gen->segmentImage(img_item->bgr_mat);
    cv::namedWindow("segment result",CV_WINDOW_NORMAL);
    cv::imshow("segment result",segment);
  });

  connect(ui->stream_cb,&QCheckBox::toggled,[=](bool checked)
  {
    img_item->stream = checked;
  });

  connect(ui->overlay_cb,&QCheckBox::toggled,[=](bool checked)
  {
    img_item->overlay = checked;
  });

  connect(ui->save_btn,&QPushButton::clicked,[=]
  {
    auto dir = QFileDialog::getExistingDirectory(this,QString("Save Settings"));
    ctable_gen->saveTable(dir.toStdString());
  });

  connect(ui->load_table_btn,&QPushButton::clicked,[=]
  {
    auto dir = QFileDialog::getExistingDirectory(this,QString("Load Settings"));
    ctable_gen->loadTable(dir.toStdString());
  });

  connect(ui->cmatrix_m11,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));
  connect(ui->cmatrix_m12,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));
  connect(ui->cmatrix_m13,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));
  connect(ui->cmatrix_m21,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));
  connect(ui->cmatrix_m22,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));
  connect(ui->cmatrix_m23,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));
  connect(ui->cmatrix_m31,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));
  connect(ui->cmatrix_m32,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));
  connect(ui->cmatrix_m33,SIGNAL(valueChanged(double)),this,SLOT(updateCameraMatrix()));

  //  ui->yuv_graphicsView->setRenderHint(QPainter::HighQualityAntialiasing);
  //  ui->graphicsView->setRenderHint(QPainter::HighQualityAntialiasing);
  this->setWindowTitle("FUKURO RGB-D Camera Calibration");
}

Widget::~Widget()
{
  delete ui;
}

void Widget::updateCameraMatrix()
{
  std::vector<double> cm;
  cm.resize(9,0.0);
  cm[0] = ui->cmatrix_m11->value();
  cm[1] = ui->cmatrix_m12->value();
  cm[2] = ui->cmatrix_m13->value();
  cm[3] = ui->cmatrix_m21->value();
  cm[4] = ui->cmatrix_m22->value();
  cm[5] = ui->cmatrix_m23->value();
  cm[6] = ui->cmatrix_m31->value();
  cm[7] = ui->cmatrix_m32->value();
  cm[8] = ui->cmatrix_m33->value();
  img_item->setCameraMatrix(cm);
}

QVector<QImage> Widget::createYUV(int y, int u, int v)
{
  QVector<QImage> ret;
  ret.resize(y);
  for(int z=0; z<y; z++)
  {
    cv::Mat m = cv::Mat::zeros(u,v,CV_8UC3);
    for(int py=0; py<u; py++)
      for(int px=0; px<v; px++)
      {
        int vv = px*255/v;
        int vu = py*255/u;
        int vy = z*255/y;
        uint8_t r, g, b;
        yuv2rgb(vy,vu,vv,&r,&g,&b);
        m.at<cv::Vec3b>(px,py)[0]=b;
        m.at<cv::Vec3b>(px,py)[1]=g;
        m.at<cv::Vec3b>(px,py)[2]=r;
      }
    ret[z] = cvMatToQImage(m);
  }
  return ret;
}

QImage Widget::readImage(const QString &file)
{
  return QImage(file);
}

void Widget::resizeEvent(QResizeEvent *event)
{
  auto s = event->size();
  auto w = s.width()-10;
  auto h = s.height()-50;
  ui->graphicsView->scene()->setSceneRect(-w/2,-h/2,w,h);
}
