#ifndef IMAGEITEM_H
#define IMAGEITEM_H

#include <QGraphicsItem>
#include <functional>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <opencv2/opencv.hpp>

namespace cv {
class Mat;
}

class ImageItem : public QGraphicsItem
{
public:
  typedef std::function<void(QPointF,qreal)> HoverCallback;
  typedef std::function<void()> ImageCallback;

public:
  ImageItem();
  void updateImage(const sensor_msgs::ImageConstPtr &msg);
  void updateDepth(const sensor_msgs::ImageConstPtr &msg);
  void updateDepthMatrix(const sensor_msgs::CameraInfoConstPtr &msg);
  void setImage(const cv::Mat &mat);
  void setDepth(const cv::Mat &mat);
  void setWarpMatrix(QVector<QPointF> ori_points, QVector<QPointF> warped_points);
  void setCameraMatrix(std::vector<double> cm);
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  QRectF boundingRect() const;

public:
  HoverCallback hover_cb = nullptr;
  ImageCallback image_cb = nullptr;
  bool stream = false;
  bool overlay = false;
  cv::Mat yuv_mat;
  cv::Mat bgr_mat;
  cv::Mat depth_mat;

private:
  void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
  void applyWarp(cv::Mat &out);
  void resizeDepth(cv::Mat &depth_warped, cv::Mat &depth);
  void overlayMat(const cv::Mat &in1, const cv::Mat &in2, cv::Mat &out);

private:
  QPixmap img;
  QPixmap overlay_img;
  cv::Mat overlay_mat;
  cv::Mat camera_matrix;
  cv::Mat warp_matrix;
  cv::Vec<double, 5> distort_param;
  //  cv::Mat distort_param;
};

#endif // IMAGEITEM_H
