#include "yuvitem.h"
#include "util.hpp"
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsWidget>
#include <QGraphicsSceneMouseEvent>
#include <opencv2/opencv.hpp>
#include <ros/ros.h>

YUVItem::YUVItem(int w, int h)
  : width(w),
    height(h)
{

}

void YUVItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  auto mat = painter->matrix();
  auto px_size = pixmap.size();
  auto w = px_size.width();
  auto h = px_size.height();
  auto bound = QRectF(-w/2,-h/2,w,h);
  painter->drawPixmap(-w/2,-h/2,w,h,pixmap);
  painter->setBrush(Qt::NoBrush);
  painter->setPen(Qt::black);
  painter->drawRect(bound);
  if(polygon.size())
  {
    painter->setPen(Qt::blue);
    painter->setBrush(Qt::NoBrush);
    painter->drawPolygon(polygon);
  }

  if(draw_highlight)
  {
    painter->setBrush(Qt::red);
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(highlightpoint_uv-QPoint(w/2,h/2),2,2);
  }

  auto m = mat;
  zoom_x = m.m11();
  zoom_y = m.m22();
  m.scale(std::fabs(1.0/m.m11()),std::fabs(1.0/m.m22()));
  painter->setMatrix(m);

  if(pixmap.size().isEmpty())
    return;
  auto diameter = 5.0;
  auto rect = QRectF(-w/2,h/2-diameter/2,w,diameter);
  painter->setBrush(Qt::blue);
  painter->setPen(Qt::blue);
  painter->drawRect(rect);

  auto y_min_pt = QPointF(-w/2+y_min*w/255,h/2);
  auto y_max_pt = QPointF(-w/2+y_max*w/255,h/2);
  painter->setBrush(Qt::green);
  painter->setPen(Qt::NoPen);
  painter->drawEllipse(y_min_pt,diameter,diameter);
  painter->drawEllipse(y_max_pt,diameter,diameter);
  if(draw_highlight)
  {
    painter->setBrush(Qt::red);
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(QPointF(highlightpoint_y-diameter/2-w/2,h/2),diameter,diameter);
  }

  painter->setMatrix(mat);
}

void YUVItem::setHighlightPoint(bool draw, qreal y, QPointF uv)
{
  highlightpoint_uv = uv;
  highlightpoint_y = y;
  draw_highlight = draw;
}

void YUVItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
  auto px_size = pixmap.size();
  auto w = px_size.width();
  auto h = px_size.height();
  auto y_min_pt = QPointF(-w/2+y_min*w/255,h/2);
  auto y_max_pt = QPointF(-w/2+y_max*w/255,h/2);
  auto diameter = 10;
  auto pos = event->pos();
  pos *= zoom_x;
  auto dp_min = pos - y_min_pt;
  auto dp_max = pos - y_max_pt;

  if(dp_min.manhattanLength()<=diameter)
    return;
  else if(dp_max.manhattanLength()<=diameter)
    return;

  if(!polygon.size())
  {
    polygon.push_back(QPoint(-10,-10));
    polygon.push_back(QPoint(-10,10));
    polygon.push_back(QPoint(10,10));
    polygon.push_back(QPoint(10,-10));
  }
  else
  {
    auto dp_min = 100000000;
    auto insert_point = 0;
    for(size_t i=0; i<polygon.size(); i++)
    {
      auto p = polygon.at(i);
      auto dp = event->pos() - p;
      if(dp.manhattanLength() < dp_min)
      {
        insert_point = i;
        dp_min = dp.manhattanLength();
      }
    }
    polygon.insert(insert_point,event->pos());
    active_poly_pt = &polygon[insert_point];
  }
  this->update();
}

void YUVItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  auto px_size = pixmap.size();
  auto w = px_size.width();
  auto h = px_size.height();
  auto y_min_pt = QPointF(-w/2+y_min*w/255,h/2);
  auto y_max_pt = QPointF(-w/2+y_max*w/255,h/2);
  auto diameter = 10;
  auto pos = event->pos();
  pos *= zoom_x;
  auto dp_min = pos - y_min_pt;
  auto dp_max = pos - y_max_pt;

  if(dp_min.manhattanLength()<=diameter)
  {
    set_y = 1;
    return;
  }
  else if(dp_max.manhattanLength()<=diameter)
  {
    set_y = 2;
    return;
  }
  else
    set_y = 0;

  if(!polygon.size())
  {
    polygon.push_back(QPoint(-10,-10));
    polygon.push_back(QPoint(-10,10));
    polygon.push_back(QPoint(10,10));
    polygon.push_back(QPoint(10,-10));
  }
  else
  {
    auto pos = event->pos();
    auto dp_min = 1000000000;
    auto idx = 0;
    for(size_t i=0; i<polygon.size(); i++)
    {
      auto p = polygon.at(i);
      auto dp = pos - p;
      if(dp.manhattanLength() < dp_min)
      {
        idx = i;
        dp_min = dp.manhattanLength();
      }
    }
    active_poly_pt = &polygon[idx];
    polygon[idx] = pos;
  }
  this->update();
}

void YUVItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  auto px_size = this->pixmap.size();
  auto w = px_size.width();
  auto h = px_size.height();
  auto y_min_pt = QPointF(-w/2+y_min/255*w,h/2);
  auto y_max_pt = QPointF(-w/2+y_max/255*w,h/2);
  auto diameter = 10;
  auto pos = event->pos();
  pos *= zoom_x;
  auto dp_min = pos - y_min_pt;
  auto dp_max = pos - y_max_pt;

  ROS_INFO("pos : (%.1f,%.1f); "
           "event->pos() : (%.1f,%.1f), "
           "y_max_pt : (%.1f,%.1f), "
           "mlength : (%.1f) "
           "w : (%d)",
           pos.x(), pos.y(),
           event->pos().x(), event->pos().y(),
           y_max_pt.x(), y_max_pt.y(),
           dp_max.manhattanLength(),
           w);

  switch (set_y) {
  case 1:
    y_min = ((pos.x()+(double)w/2.0)*255.0/(double)(w-1));
    y_min = std::max(0,y_min);
    y_min = std::min(255,y_min);
    ROS_INFO("y_min : %.1f %.1f",y_min, (pos.x()+(double)w/2.0));
    break;
  case 2:
    y_max = ((pos.x()+(double)w/2.0)*255.0/(double)(w-1));
    y_max = std::max(0,y_max);
    y_max = std::min(255,y_max);
    ROS_INFO("y_max : %.1f %.1f",y_max, (pos.x()+(double)w/2.0));
    break;
  default:
    if(!active_poly_pt)
      return;
    *active_poly_pt = event->pos();
    break;
  }

  this->update();
}

void YUVItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
  active_poly_pt = nullptr;
}

QRectF YUVItem::boundingRect() const
{
  if(scene())
    return this->scene()->sceneRect();
  auto px_size = pixmap.size();
  auto w = px_size.width()+50;
  auto h = px_size.height()+50;
  return QRectF(-w/2,-h/2,w,h);
}

void YUVItem::readMat(cv::Mat &mat)
{
  cv::Mat yuv(mat.rows,mat.cols,CV_8UC3);
  cv::Mat yuv_table(256,256,CV_8UC3);
  cv::cvtColor(mat,yuv,CV_BGR2YUV);
  yuv_table.setTo(cv::Scalar(255,255,255));
  for(size_t i=0; i<mat.rows; i++)
    for(size_t j=0; j<mat.cols; j++)
    {
      auto color = mat.at<cv::Vec3b>(i,j);
      auto yuv_idx = yuv.at<cv::Vec3b>(i,j);
      yuv_table.at<cv::Vec3b>(int(yuv_idx[2]),int(255-yuv_idx[1])) = color;
    }
  //  cv::imshow("yuv_table",yuv_table);
  pixmap = QPixmap::fromImage(cvMatToQImage(yuv_table));
}
