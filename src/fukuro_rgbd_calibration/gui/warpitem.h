#ifndef WARPITEM_H
#define WARPITEM_H

#include <QGraphicsItem>
#include <functional>

class WarpItem : public QGraphicsItem
{
public:
  typedef std::function<void(QVector<QPointF>,QVector<QPointF>)> WarpCallback;
public:
  WarpItem();
  void save(const std::string &dir);
  void load(const std::string &dir);
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  QRectF boundingRect() const;

private:
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

public:
  WarpCallback warp_cb = nullptr;
  QVector<QPointF> warp_points;

private:
  QVector<QLineF> ori_lines;
  QVector<QPointF> ori_points;
  QVector<QLineF> warped_lines;
  int select_point;
};

#endif // WARPITEM_H
