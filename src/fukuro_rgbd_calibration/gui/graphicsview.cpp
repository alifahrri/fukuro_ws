#include "graphicsview.h"
#include <QMouseEvent>
#include <QWheelEvent>
#include <QScrollBar>

GraphicsView::GraphicsView(QWidget *parent)
  : QGraphicsView(parent)
{
  this->horizontalScrollBar()->setEnabled(false);
  this->verticalScrollBar()->setEnabled(false);
  this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  this->setRenderHint(QPainter::HighQualityAntialiasing);
}

void GraphicsView::mouseMoveEvent(QMouseEvent *event)
{
  QGraphicsView::mouseMoveEvent(event);
  event->accept();
}

void GraphicsView::wheelEvent(QWheelEvent *event)
{
  if(event->modifiers() != Qt::ControlModifier)
    goto DONE;
  if(std::abs(event->angleDelta().y()) < 2)
    goto DONE;
  if(event->angleDelta().y() < 0)
  {
    this->scale(0.9,0.9);
    zoom_level *= 0.9;
  }
  else
  {
    this->scale(1.1,1.1);
    zoom_level *= 1.1;
  }
DONE:
  event->accept();
  if(!scene())
    return;
  auto r = sceneRect();
}

void GraphicsView::resizeEvent(QResizeEvent *event)
{
  if(this->scene())
  {
    auto s = event->size();
    auto w = s.width();
    auto h = s.height();
    scene()->setSceneRect(-w/2,-h/2,w,h);
    scene()->update();
    auto r = sceneRect();
  }
}
