#include "imageitem.h"
#include <ros/ros.h>
#include <util.hpp>
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneHoverEvent>
#include <cv_bridge/cv_bridge.h>

ImageItem::ImageItem()
{
  setAcceptHoverEvents(true);
  //  camera_matrix = cv::Mat(3,3,CV_32FC1);
  camera_matrix = cv::Mat::eye(3,3,CV_64FC1);
  warp_matrix = cv::Mat::eye(3,3,CV_64FC1);
  //  distort_param = cv::Mat::zeros(5,0,CV_32FC1);
  for(size_t i=0; i<5; i++)
    distort_param[i] = 0.0;
}

void ImageItem::updateImage(const sensor_msgs::ImageConstPtr &msg)
{
  ROS_INFO("image callback");
  if(stream)
  {
    cv_bridge::CvImageConstPtr cv_ptr;
    cv_ptr = cv_bridge::toCvShare(msg,sensor_msgs::image_encodings::BGR8);
    setImage(cv_ptr->image);
  }
}

void ImageItem::updateDepth(const sensor_msgs::ImageConstPtr &msg)
{
  ROS_INFO("depth callback");
  if(stream)
  {
    cv_bridge::CvImageConstPtr cv_ptr;
    cv_ptr = cv_bridge::toCvShare(msg,sensor_msgs::image_encodings::TYPE_16UC1);
    setDepth(cv_ptr->image);
    if(overlay)
    {
      ROS_INFO("overlay process : bgr(%d;%d) depth(%d;%d)", bgr_mat.cols, bgr_mat.rows, depth_mat.cols, depth_mat.rows);
      cv::Mat depth, depth_warped;
      applyWarp(depth_warped);
      resizeDepth(depth_warped,depth);
      overlayMat(bgr_mat,depth,overlay_mat);
      overlay_img = QPixmap::fromImage(cvMatToQImage(overlay_mat));
    }
  }
}

void ImageItem::updateDepthMatrix(const sensor_msgs::CameraInfoConstPtr &msg)
{
  ROS_INFO("depth camera info callback");
  for(size_t i=0; i<9; i++)
  {
    camera_matrix.at<double>(i) = msg->K.at(i)/(i<8?1.0:1.0);
    if(i<5)
      distort_param[i] = msg->D.at(i);
  }
}

void ImageItem::setImage(const cv::Mat &mat)
{
  bgr_mat.create(mat.rows,mat.cols,CV_8UC3);
  yuv_mat.create(mat.rows,mat.cols,CV_8UC3);
  mat.copyTo(bgr_mat);
  cv::cvtColor(bgr_mat,yuv_mat,CV_BGR2YUV);
  img = QPixmap::fromImage(cvMatToQImage(bgr_mat));
  if(image_cb)
    image_cb();
  //  this->update();
  if(this->scene())
    this->scene()->update();
}

void ImageItem::setDepth(const cv::Mat &mat)
{
  depth_mat.create(mat.rows,mat.cols,mat.type());
  cv::undistort(mat,depth_mat,camera_matrix,distort_param);
}

void ImageItem::setWarpMatrix(QVector<QPointF> ori_points, QVector<QPointF> warped_points)
{
  std::vector<cv::Point2f> src, dst;
  for(const auto& p : ori_points)
    src.push_back(cv::Point2f(p.x()+320,p.y()+240));
  for(const auto& p : warped_points)
    dst.push_back(cv::Point2f(p.x()+320,p.y()+240));
  warp_matrix = cv::getPerspectiveTransform(src,dst);
  if(!stream && overlay)
  {
    cv::Mat depth, warped;
    applyWarp(warped);
    resizeDepth(warped,depth);
    overlayMat(bgr_mat,depth,overlay_mat);
    overlay_img = QPixmap::fromImage(cvMatToQImage(overlay_mat));
    this->scene() ? this->scene()->update() : this->update();
  }
}

void ImageItem::setCameraMatrix(std::vector<double> cm)
{
  std::stringstream ss;
  for(size_t i=0; i<cm.size(); i++)
  {
    camera_matrix.at<double>(i) = cm[i];
    ss << cm[i] << (i==cm.size()-1?"":",");
  }
  ROS_INFO("camera mat : %s", ss.str().c_str());
}

void ImageItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  if(stream || overlay)
  {
    auto imsize = overlay_img.size();
    auto w = imsize.width();
    auto h = imsize.height();
    painter->drawPixmap(-w/2,-h/2,w,h,overlay_img);
  }
  else
  {
    auto imsize = img.size();
    auto w = imsize.width();
    auto h = imsize.height();
    painter->drawPixmap(-w/2,-h/2,w,h,img);
  }
}

QRectF ImageItem::boundingRect() const
{
  auto imsize = img.size();
  auto w = imsize.width();
  auto h = imsize.height();
  return QRectF(-w/2,-h/2,w,h);
}

void ImageItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
  if(hover_cb)
  {
    auto pos = mapToScene(event->pos());
    pos += QPointF(yuv_mat.cols/2,yuv_mat.rows/2);
    //    ROS_INFO("ImageItem::hoverMoveEvent %f,%f->%f,%f",event->pos().x(),event->pos().y(),pos.x(),pos.y());
    if(pos.x()<yuv_mat.cols
       && pos.y()<yuv_mat.rows
       && pos.x() > 0
       && pos.y() > 0)
    {
      auto yuv = yuv_mat.at<cv::Vec3b>(cv::Point(pos.x(),pos.y()));
      hover_cb(QPointF(255-yuv[1],yuv[2]),yuv[0]);
    }
    }
}

void ImageItem::applyWarp(cv::Mat &out)
{
  out.create(depth_mat.rows,depth_mat.cols,CV_8UC3);
  auto depth_mat_warped = cv::Mat(depth_mat.rows,depth_mat.cols,depth_mat.type());
  cv::warpPerspective(depth_mat,depth_mat_warped,warp_matrix,depth_mat_warped.size());
  out = depth_mat_warped;
}

void ImageItem::resizeDepth(cv::Mat &depth_warped, cv::Mat &depth)
{
  cv::cvtColor(depth_warped,depth_warped,CV_GRAY2BGR);
  auto depth_width = 1440; /*bgr_mat.cols;*/
  auto depth_height = bgr_mat.rows;
  depth.create(depth_height,depth_width,CV_8UC3);
  cv::resize(depth_warped,depth,depth.size());
}

void ImageItem::overlayMat(const cv::Mat &in1, const cv::Mat &in2, cv::Mat &out)
{
  //  auto tmp = cv::Mat::zeros(in1.rows,in1.cols,CV_8UC3);
  cv::Mat tmp(in1.rows,in1.cols,CV_8UC3);
  in2.copyTo(tmp(cv::Rect(240,0,1440,1080)));
  cv::addWeighted(in1,0.75,tmp,0.25,0.0,out,out.type());
}
