#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <opencv2/opencv.hpp>

namespace Ui {
class Widget;
}

class YUVItem;
class WarpItem;
class ImageItem;
class ColorTableGenerator;

class Widget : public QWidget
{
  Q_OBJECT

public:
  explicit Widget(QWidget *parent = 0);
  ~Widget();

public:
  ImageItem *img_item;

private slots:
  void updateCameraMatrix();

private:
  Ui::Widget *ui;
  cv::Mat mat_ori;
  YUVItem *yuv_item;
  WarpItem *warp_item;
  QVector<QImage> lookup;
  ColorTableGenerator *ctable_gen;

private:
  QVector<QImage> createYUV(int y, int u, int v);
  QImage readImage(const QString &file);
  void resizeEvent(QResizeEvent *event);
};

#endif // WIDGET_H
