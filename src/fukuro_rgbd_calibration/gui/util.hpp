#ifndef UTIL_HPP
#define UTIL_HPP

#include <cstdint>
#include <algorithm>
#include <QImage>
#include <opencv2/opencv.hpp>

inline
int clamp(int value, int min, int max)
{
  return (std::min(max,std::max(min,value)));
}

inline
void yuv2rgb(uint8_t yValue, uint8_t uValue, uint8_t vValue, uint8_t *r, uint8_t *g, uint8_t *b)
{
  int rTmp = yValue + (1.370705 * (vValue-128));
  int gTmp = yValue - (0.698001 * (vValue-128)) - (0.337633 * (uValue-128));
  int bTmp = yValue + (1.732446 * (uValue-128));
  *r = clamp(rTmp, 0, 255);
  *g = clamp(gTmp, 0, 255);
  *b = clamp(bTmp, 0, 255);
}

inline
void rgb2yuv(uint8_t r, uint8_t g, uint8_t b, uint8_t *y, uint8_t* u, uint8_t *v)
{

}

inline
QImage cvMatToQImage(cv::Mat &mat)
{
  return QImage((const uchar*)mat.data,
                mat.cols,
                mat.rows,
                mat.cols*mat.channels(),
                QImage::Format_RGB888).rgbSwapped();
}

#endif // UTIL_HPP
