#include "warpitem.h"
#include <opencv2/opencv.hpp>
#include <QDebug>
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

#define DEFAULT_WIDTH   640
#define DEFAULT_HEIGHT  480
#define DEFAULT_STEP    40

WarpItem::WarpItem()
{
  for(int i=-DEFAULT_WIDTH/2; i<=DEFAULT_WIDTH/2; i+=DEFAULT_STEP)
  {
    QPointF pt1(i,-DEFAULT_HEIGHT/2);
    QPointF pt2(i,DEFAULT_HEIGHT/2);
    ori_lines.push_back(QLineF(pt1,pt2));
  }

  for(int j=-DEFAULT_HEIGHT/2; j<=DEFAULT_HEIGHT/2; j+=DEFAULT_STEP)
  {
    QPointF pt1(-DEFAULT_WIDTH/2,j);
    QPointF pt2(DEFAULT_WIDTH/2,j);
    ori_lines.push_back(QLineF(pt1,pt2));
  }

  warp_points.resize(4);
  warp_points[0] = QPointF(-DEFAULT_WIDTH/2,-DEFAULT_HEIGHT/2);
  warp_points[1] = QPointF(DEFAULT_WIDTH/2,-DEFAULT_HEIGHT/2);
  warp_points[2] = QPointF(DEFAULT_WIDTH/2,DEFAULT_HEIGHT/2);
  warp_points[3] = QPointF(-DEFAULT_WIDTH/2,DEFAULT_HEIGHT/2);

  ori_points = warp_points;
  warped_lines = ori_lines;
}

void WarpItem::save(const std::string &dir)
{
  cv::FileStorage warp_settings(dir+"/warp.xml",cv::FileStorage::WRITE);
  warp_settings << "WarpSettings"
                << "[" << "{";
  warp_settings << "origin" << "[";
  for(const auto &p : ori_points)
    warp_settings << (double)p.x() << (double)p.y();
  warp_settings << "]";

  warp_settings << "warped" << "[";
  for(const auto &p : warp_points)
    warp_settings << (double)p.x() << (double)p.y();
  warp_settings << "]";

  warp_settings << "}" << "]";
  warp_settings.release();
}

void WarpItem::load(const std::string &dir)
{
  cv::FileStorage warp_settings(dir+"/warp.xml", cv::FileStorage::READ);
  cv::FileNode warp_node = warp_settings["WarpSettings"];
  cv::FileNodeIterator it = warp_node.begin();
  cv::FileNodeIterator it_end = warp_node.end();
  std::vector<double> ori;
  std::vector<double> warped;
  while(it != it_end)
  {
    (*it)["origin"] >> ori;
    (*it)["warped"] >> warped;
    ++it;
  }
  ori_points.clear();
  for(size_t i=0; i<ori.size(); i+=2)
    ori_points.push_back(QPointF(ori[i],ori[i+1]));
  warp_points.clear();
  for(size_t i=0; i<warped.size(); i+=2)
    warp_points.push_back(QPointF(warped[i],warped[i+1]));

  this->scene() ? this->scene()->update() : this->update();
}

void WarpItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->setBrush(Qt::NoBrush);
  painter->setPen(QPen(Qt::blue,0.5,Qt::DashLine));
  painter->drawLines(ori_lines);
  //  painter->setPen(QPen(Qt::green,0.5,Qt::DashLine));
  //  painter->drawLines(warped_lines);
  painter->setBrush(Qt::blue);
  for(const auto &p : ori_points)
    painter->drawEllipse(p,5.0,5.0);
  painter->setPen(QPen(Qt::green,2.0));
  painter->setBrush(Qt::green);
  for(const auto &p : warp_points)
    painter->drawEllipse(p,5.0,5.0);
  painter->setBrush(Qt::NoBrush);
  painter->drawPolygon(QPolygonF(warp_points));
  //  qDebug() << "warpItem paint :" << ori_lines << warp_points;
  //  painter->drawText(0,0,"WarpItem");
}

QRectF WarpItem::boundingRect() const
{
  return QRectF(-DEFAULT_WIDTH,-DEFAULT_HEIGHT,DEFAULT_WIDTH*2,DEFAULT_HEIGHT*2);
}

void WarpItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  auto p = event->pos();
  select_point = -1;
  for(size_t i=0; i<4; i++)
  {
    auto d = p-warp_points.at(i);
    if(d.manhattanLength()<10)
      select_point = i;
  }
}

void WarpItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  if(select_point < 0)
    return;
  auto p = event->pos();
  warp_points[select_point] = p;
  this->scene() ? this->scene()->update() : this->update();
  if(warp_cb)
    warp_cb(ori_points,warp_points);
}
