#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>

class GraphicsView : public QGraphicsView
{
  Q_OBJECT
public:
  GraphicsView(QWidget* parent = nullptr);

private:
  void mouseMoveEvent(QMouseEvent *event);
  void wheelEvent(QWheelEvent *event);
  void resizeEvent(QResizeEvent *event);

private:
  qreal zoom_level = 1.0;
};

#endif // GRAPHICSVIEW_H
