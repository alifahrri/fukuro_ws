# The fukuRŌ RGB-D Camera Calibration Package

This package does the followings :

+ Load saved Images  
+ Stream from camera by subscribing /camera/color/image_raw and /camera/depth/image_raw  
+ Read camera matrix from /camera/depth/camera_info  
+ Create YUV color table using interactive GUI  
+ Save and load the created color table  
+ Preview image segmentation result  
+ Setting warp perspective transfrom to align depth images with rgb images using interactive GUI  
+ Save and load the warp settings  