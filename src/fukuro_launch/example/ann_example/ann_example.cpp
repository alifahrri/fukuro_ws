#include <iostream>

/* TESTING */
#include <fukuro/core/ann.hpp>
#include <fukuro/core/csvreader.hpp>
#include <ros/ros.h>

void coba_ann(bool isTrain=true){

    if(isTrain){
        char* user_name = std::getenv("USER");
        std::string csv_path = std::string("/home/") + std::string(user_name) + std::string("/fukuro_ws/src/fukuro_launch/example/ann_example/mnist_train_100.csv");
        std::string weight_path = std::string("/home/") + std::string(user_name) + std::string("/fukuro_ws/src/fukuro_launch/example/ann_example/saved_weight/weight.dat");

        Data X,Y;
        CSVReader train_data(csv_path);
        auto datalist = train_data.getData();

        for(auto vec : datalist)
        {
            int idx = 0;
            std::vector<double> data_temp;
            for(auto data : vec)
            {
                if(idx==0){
                    if(data=="0"){
                        Y.push_back({1,0,0,0,0,0,0,0,0,0});
                    }
                    else if(data=="1"){
                        Y.push_back({0,1,0,0,0,0,0,0,0,0});
                    }
                    else if(data=="2"){
                        Y.push_back({0,0,1,0,0,0,0,0,0,0});
                    }
                    else if(data=="3"){
                        Y.push_back({0,0,0,1,0,0,0,0,0,0});
                    }
                    else if(data=="4"){
                        Y.push_back({0,0,0,0,1,0,0,0,0,0});
                    }
                    else if(data=="5"){
                        Y.push_back({0,0,0,0,0,1,0,0,0,0});
                    }
                    else if(data=="6"){
                        Y.push_back({0,0,0,0,0,0,1,0,0,0});
                    }
                    else if(data=="7"){
                        Y.push_back({0,0,0,0,0,0,0,1,0,0});
                    }
                    else if(data=="8"){
                        Y.push_back({0,0,0,0,0,0,0,0,1,0});
                    }
                    else if(data=="9"){
                        Y.push_back({0,0,0,0,0,0,0,0,0,1});
                    }
                }
                else{
                    double data_norm = std::atof(data.c_str());
                    data_temp.push_back(data_norm/255.0);
                }
                idx++;
            }
            X.push_back(data_temp);
            data_temp.clear();
        }

        fukuro::ANN myNetwork(10,0.001,weight_path);

        myNetwork.add_layer(784,"input","relu");
        myNetwork.add_layer(100,"hidden","relu");
        myNetwork.add_layer(100,"hidden","relu");
        myNetwork.add_layer(10,"output","sigmoid");

        //std::cout << "ERROR PASSED!!" << '\n';

        myNetwork.fit(X,Y);

        std::random_device rd;
        std::uniform_int_distribution<int> rgen(0,99);
        int idx = rgen(rd);

        std::vector<double> x = X[idx],y_test = Y[idx];

        auto y = myNetwork.predict(x);
        int pred = 0;
        double biggest = y[pred];
        for(int i=0; i<y.size(); i++){
            if(biggest<y[i]){
                biggest = y[i];
                pred = i;
            }
        }
        int test = 0;
        biggest = y_test[test];
        for(int i=0; i<y_test.size(); i++){
            if(biggest<y_test[i]){
                biggest = y_test[i];
                test = i;
            }
        }
        std::cout << "Predicted : " << pred << '\n';
        std::cout << "True Value : " << test << '\n';
    }
    else{

    }

}

//*/
int main(int argc, char** argv){

    std::cout << "Coba ANN" << '\n';

    bool isTrain;

    if(ros::param::get("\train",isTrain))
        coba_ann(isTrain);
    else
        coba_ann();


}

