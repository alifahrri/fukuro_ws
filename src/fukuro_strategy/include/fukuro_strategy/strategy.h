#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <fukuro_common/WorldModel.h>
#include <fukuro_common/KeypadService.h>
#include <fukuro_common/RobotControl.h>
#include <fukuro_common/RobotControlInfo.h>
#include <fukuro_common/StrategyInfo.h>
#include <fukuro_common/StrategyService.h>
#include <fukuro_common/Shoot.h>
#include <fukuro_common/StrategyPositioning.h>
#include <fukuro_common/Teammates.h>
#include <fukuro_common/InterceptPoint.h>

#ifndef FIRST_BUILD
//#include <fukuro_strategy/FukuroStrategyConfig.h>
#endif

#include <chrono>
#include <mutex>
#include <thread>
#include <vector>
#include <random>
#include <tuple>
#include <queue>
#include <cmath>

#include "fukuro/core/field.hpp"

namespace fukuro {

enum GameState {POSITIONING,GAMEPLAY,HALT};

enum RefereeState {WELCOME,OUR_KICKOFF,OPP_KICKOFF,OUR_FREEKICK,OPP_FREEKICK,
                   OUR_GOALKICK, OPP_GOALKICK, OUR_THROWIN, OPP_THROWIN,
                   OUR_CORNERKICK, OPP_CORNERKICK, OUR_PENALTY, OPP_PENALTY,
                   OUR_GOAL, OPP_GOAL, OUR_REPAIR, OPP_REPAIR, OUR_REDCARD, OPP_REDCARD,
                   OUR_YELLOWCARD, OPP_YELLOWCARD, START, STOP, DROP, _1ST, _2ND, _3RD, _4TH
                  };

enum Role {GOALIE,DEFENDER,STRIKER};

enum State {TIMER, KICKOFF, IDLE, HOMING,
            BALL_SEARCHING, CORNERKICK, FREEKICK,
            DRIBBLE_BALL, KICK_BALL, GK, PENALTY};

enum PlayArea {OUR_LEFT,OUR_RIGHT,OPP_LEFT,OPP_RIGHT};


typedef std::tuple<double,double,int> Position;
typedef std::priority_queue<Position,std::vector<Position>,std::greater<Position>> Neighbor;

struct RobotInfo{

    int robot_name;

    double x;
    double y;
    double theta;

    double last_x;
    double last_y;

    double kick_x;
    double kick_y;
    double kick_angle;

    double ball_shield_angle;

    double error_angle;
    double error_radius;

    double home_x;
    double home_y;
    double home_theta;

    double intercept_x;
    double intercept_y;
    double intercept_theta;
    bool isInterceptValid;

    bool isNearestoBall;
    bool isBallEngaged;
    bool CheckisBallEngaged(double x,double y);

    PlayArea playarea;
    PlayArea checkPlayArea(double x, double y);

    GameState gstate;
    RefereeState rstate;
    Role role;
    State state,last_state;

    Neighbor neighbor, neighbor_ball;
    double neighbor_x;
    double neighbor_y;

    int penalty_counter;

};

struct BallInfo{

    double local_x;
    double local_y;
    double global_x;
    double global_y;

    bool isVisible;
    ros::Time last_seen;

};

class Strategy
{
public:
    typedef std::chrono::high_resolution_clock Clock;
    typedef std::chrono::duration<double,std::milli> Duration;
    Strategy(ros::NodeHandle& nh);
    //struct fukuro::RobotInfo;

    void process();
    Role roleChecking(std::string str);
    std::string getRole(Role role);
    RefereeState rstateChecking(int state);
    int getRstate(RefereeState rstate);
    State checkState(std::string str);
    std::string getState(State state);
    void setStraightLine(double x1, double x2, double y1, double y2);
    double getYStraightLine(double X);

private:
    double x1;
    double x2;
    double y1;
    double y2;

public:
    void updateTeammatesInfo(const fukuro_common::Teammates::ConstPtr& teammates);
    void updateWorldModelInfo(const fukuro_common::WorldModel::ConstPtr& wm);
    void updateStrategyPositioningInfo(const fukuro_common::StrategyPositioning::ConstPtr& strpos);
    void updateRobotControlInfo(const fukuro_common::RobotControlInfo::ConstPtr& robotinfo);
#ifndef FIRST_BUILD
    //void reconfigure(const fukuro_strategy::FukuroStrategyConfig& config, uint32_t level);
#endif
    bool StrategyRequestHandler(fukuro_common::StrategyService::Request &request, fukuro_common::StrategyService::Response &response);
    void GoalieIntercept(const fukuro_common::InterceptPoint::ConstPtr &point);

private:
    inline void penalty();
    inline void free_kick();
    inline void corner_kick();
    inline void goalie();
    inline void timer();
    inline void dribble_ball();
    inline void stop();
    inline void positioning();
    inline void kick_ball();
    inline void ball_search();
    inline void kick_off();
    inline void go_to_pos(double x, double y, double theta, std::string local = "", bool dribbler = false, bool planning = false);

private:
    fukuro::RobotInfo robot;
    fukuro::BallInfo ball;
    ros::NodeHandle& node;
    ros::Publisher robotcontrol_pub;
    ros::Publisher strinfo_pub;
    fukuro_common::Shoot kicker_service;
    std::string robot_name;
    fukuro_common::RobotControl rcontrolmsg;
    ros::Time last_time;

private:
    double angle_dribble_auto_on;
    double angle_mulai_dribble_bola;
    double error_positioning;
    double error_sudut_positioning;
    double min_error_posisi_homing;
    double min_error_posisi_kick;
    double min_error_sudut_homing;
    double min_error_sudut_kick;
    double radius_dribble_auto_on;
    double radius_mulai_dribble_bola;
    double radius_mulai_kick_off;


};

}
