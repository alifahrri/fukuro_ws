#include "strategy.h"

using namespace fukuro;


Strategy::Strategy(ros::NodeHandle &nh):
    node(nh), angle_dribble_auto_on(0.0), angle_mulai_dribble_bola(0.0), error_positioning(0.0),
    error_sudut_positioning(0.0), min_error_posisi_homing(0.0), min_error_posisi_kick(0.0),
    min_error_sudut_homing(0.0), min_error_sudut_kick(0.0), radius_dribble_auto_on(0.0),
    radius_mulai_dribble_bola(0.0), radius_mulai_kick_off(0.0)
{


    std::string state;
    if(ros::param::get("/strategy_state",state)){
        robot.state = checkState(state);
    }

    robot.penalty_counter = 0;

    if(!ros::get_environment_variable(robot_name,"FUKURO"))
    {
        ROS_ERROR("robot name empty!!! export FUKURO=fukuro1");
        exit(-1);
    }

    robotcontrol_pub = node.advertise<fukuro_common::RobotControl>(robot_name+std::string("/robot_control"),1);
    strinfo_pub = node.advertise<fukuro_common::StrategyInfo>(robot_name+std::string("/strategy_info"),1);

    std::string role;
    if(ros::param::get("/robot_role",role)){
        robot.role = roleChecking(role);
    }
    switch(robot.role){
    case GOALIE:{
        robot.home_x = GOALIE_HOME_X;
        robot.home_y = GOALIE_HOME_Y;
        robot.home_theta = 0.0;
        return;
    }
    case DEFENDER:{
        robot.home_x = DEFENDER_HOME_X;
        robot.home_y = DEFENDER_HOME_Y;
        robot.home_theta = 0.0;
        break;
    }
    case STRIKER:{
        robot.home_x = STRIKER_HOME_X;
        robot.home_y = STRIKER_HOME_Y;
        robot.home_theta = 0.0;
        break;
    }
    }

    double home_x = 0.0;
    double home_y = 0.0;
    double home_w = 0.0;
    if(ros::param::get("/home_x",home_x))
        if(ros::param::get("/home_y",home_y))
            if(ros::param::get("/home_w",home_w)) {
                robot.home_x = home_x;
                robot.home_y = home_y;
                robot.home_theta = home_w;
            }

}


Role Strategy::roleChecking(std::string str){

    Role ret;

    if(str.find("def")!=std::string::npos){
        ret = DEFENDER;
    }
    else if(str.find("str")!=std::string::npos){
        ret = STRIKER;
    }
    else if(str.find("goal")!=std::string::npos){
        ret = GOALIE;
    }

    return ret;
}

std::string Strategy::getRole(Role role){

    std::string ret;

    switch(role){
    case DEFENDER:{
        ret = "defender";
        break;
    }
    case STRIKER:{
        ret = "striker";
        break;
    }
    case GOALIE:{
        ret = "goalie";
        break;
    }
    };

    return ret;
}

/*
RefereeState Strategy::rstateChecking(int state){

    RefereeState ret;

    switch (state) {

    };

    return ret;

}

int Strategy::getRstate(RefereeState rstate){

    int ret;

    switch (rstate) {

    };

    return ret;
}
*/

State Strategy::checkState(std::string str){

    State ret;

    if(str.find("timer")!=std::string::npos){
        ret = TIMER;
    }
    else if(str.find("stop")!=std::string::npos){
        ret = IDLE;
    }
    else{
        ret = robot.state;
    }

    return ret;

}

std::string Strategy::getState(State state){

    std::string ret;

    switch(state){
    case TIMER:{
        ret = "timer";
        break;
    }

    case KICKOFF:{
        ret = "kick-off";
        break;
    }

    case IDLE:{
        ret = "stop";
        break;
    }

    case HOMING:{
        ret = "positioning";
        break;
    }

    case BALL_SEARCHING:{
        ret = "ball-searching";
        break;
    }

    case DRIBBLE_BALL:{
        ret = "dribble-ball";
        break;
    }

    case KICK_BALL:{
        ret = "kick-ball";
        break;
    }
    case CORNERKICK:{
        ret = "corner-kick";
        break;
    }
    case GK:{
        ret = "goalie";
        break;
    }
    case FREEKICK:{
        ret = "free-kick";
        break;
    }
    }

    return ret;
}

void Strategy::go_to_pos(double x, double y, double theta, std::string local, bool dribbler, bool planning){

    rcontrolmsg.target_pose.x = x;
    rcontrolmsg.target_pose.y = y;
    rcontrolmsg.target_pose.theta = theta;
    rcontrolmsg.plan = planning;
    rcontrolmsg.option.data = local=="local"?"local":"";
    rcontrolmsg.dribbler = dribbler?1:0;

}

void Strategy::penalty(){

    static bool isFirstCall = true;

    if(isFirstCall){
        robot.penalty_counter++;
        isFirstCall = false;
    }
    std::random_device y_rd;
    std::uniform_real_distribution<double> y_rgen(-1.0,1.0);


    switch (robot.role) {
    case GOALIE:{
        /*
        if((robot.penalty_counter%3)==0){

        }
        */
        robot.state = GK;
        break;
    }
    case DEFENDER:{
        static auto py = y_rgen(y_rd);
        auto angle = std::atan2((py-ball.global_y),(GOAL_POS_X-ball.global_x));
        if(robot.gstate==GAMEPLAY){
            if((robot.penalty_counter%2)==1){
                go_to_pos(ball.global_x,ball.global_y,angle);
                //auto error_radius = std::hypot(std::fabs(robot.x-ball.global_x),std::fabs(robot.y-ball.global_y));
                //auto error_angle = std::atan2(std::fabs(robot.y-ball.global_y),std::fabs(robot.x-ball.global_x));
                if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                        //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                        ){
                    robot.state = KICK_BALL;
                    isFirstCall = true;
                    py = y_rgen(y_rd);
                    return;
                }

            }
        }
        go_to_pos(0.0,1.3,0.0);
        break;
    }
    case STRIKER:{
        static auto py = y_rgen(y_rd);
        auto angle = std::atan2((py-ball.global_y),(GOAL_POS_X-ball.global_x));
        if(robot.gstate==GAMEPLAY){
            if((robot.penalty_counter%2)==0){
                go_to_pos(ball.global_x,ball.global_y,angle);
                //auto error_radius = std::hypot(std::fabs(robot.x-ball.global_x),std::fabs(robot.y-ball.global_y));
                //auto error_angle = std::atan2(std::fabs(robot.y-ball.global_y),std::fabs(robot.x-ball.global_x));
                if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                        //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                        ){
                    robot.state = KICK_BALL;
                    isFirstCall = true;
                    py = y_rgen(y_rd);
                    return;
                }

            }
        }
        go_to_pos(0.0,0.0,0.0);
        break;
    }
    }

}

void Strategy::goalie(){

    if(robot.gstate==POSITIONING || robot.gstate==HALT){

        //auto error_radius = std::hypot(std::fabs(robot.x-GOALIE_HOME_X),std::fabs(robot.y-GOALIE_HOME_Y));
        //auto error_angle = std::atan2(std::fabs(robot.y-GOALIE_HOME_Y),std::fabs(robot.x-GOALIE_HOME_X));
        go_to_pos(GOALIE_HOME_X,GOALIE_HOME_Y,0.0,"",false,false);
        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            stop();
        }
        return;

    }

    if(ball.isVisible){
        auto radius = std::hypot(ball.local_x,ball.local_y);
        if(radius<0.5){
            robot.state = KICK_BALL;
            return;
        }
    }

    if(robot.isInterceptValid){
        go_to_pos(robot.intercept_x/100.0,robot.intercept_y/100.0,robot.intercept_theta*M_PI/180.0);
    }
    else{
        auto factor = std::pow((ball.global_y/MAX_FIELD_Y),5);
        factor = factor>=1.0?1.0:factor;
        auto py = ball.global_y*(1-factor);
        if(ball.isVisible){
            if(py>=1.0){
                auto predicty_error = ball.global_y/2.0;
                py = 1.0 - predicty_error;
            }
            else if(py<=-1.0){
                auto predicty_error = ball.global_y/2.0;
                py = -1.0 - predicty_error;
            }
        }
        else{
            py = 0.0;
        }
        go_to_pos(GOALIE_HOME_X,py,0.0);

    }

}

void Strategy::setStraightLine(double x1, double x2, double y1, double y2){

    this->x1 = x1;
    this->x2 = x2;
    this->y1 = y1;
    this->y2 = y2;

}

double Strategy::getYStraightLine(double X){

    auto y2y1 = y2 - y1;

    return (X*(y2y1))-(x1*(y2y1))+y1;
}

void Strategy::free_kick(){

    switch(robot.role){
    case GOALIE:{
        robot.state = GK;
        return;
    }
    case DEFENDER:{

        if(robot.gstate==GAMEPLAY){
            robot.state = HOMING;
            return;
        }

        auto targetx = GOAL_POS_X - ball.global_x;
        auto targety = GOAL_POS_Y + ball.global_y;
        auto targetw = std::atan2(-targety,targetx);

        setStraightLine(GOAL_POS_X,ball.global_x,GOAL_POS_Y,-ball.global_y);

        auto px = ball.global_x - 1.0;
        auto py = getYStraightLine(px);

        if(px>=MAX_FIELD_X) px = MAX_FIELD_X;
        else if(px<=(-MAX_FIELD_X)) px = -MAX_FIELD_X;

        if(py>=MAX_FIELD_Y) py = MAX_FIELD_Y;
        else if(py<=(-MAX_FIELD_Y)) py = -MAX_FIELD_Y;

        //auto error_radius = std::hypot(std::fabs(robot.x-px),std::fabs(robot.y-py));
        //auto error_angle = std::atan2(std::fabs(robot.y-py),std::fabs(robot.x-px));

        go_to_pos(px,py,targetw,"",false,false);

        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            stop();
        }

        break;
    }
    case STRIKER:{

        if(robot.gstate==GAMEPLAY){

            //auto error_radius = std::hypot(std::fabs(robot.x-ball.global_x),std::fabs(robot.y-ball.global_y));
            //auto error_angle = std::atan2(std::fabs(robot.y-ball.global_y),std::fabs(robot.x-ball.global_x));

            auto angle = std::atan2(ball.global_y,ball.global_x);

            go_to_pos(ball.global_x,ball.global_y,angle,"",true,false);
            if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE && robot.isBallEngaged
                    //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                    ){
                robot.state = KICK_BALL;
                return;
            }

            return;
        }

        auto targetx = GOAL_POS_X - ball.global_x;
        auto targety = GOAL_POS_Y - ball.global_y;
        auto targetw = std::atan2(targety,targetx);

        setStraightLine(GOAL_POS_X,ball.global_x,GOAL_POS_Y,ball.global_y);

        auto px = ball.global_x - 1.0;
        auto py = getYStraightLine(px);

        if(px>=MAX_FIELD_X) px = MAX_FIELD_X;
        else if(px<=(-MAX_FIELD_X)) px = -MAX_FIELD_X;

        if(py>=MAX_FIELD_Y) py = MAX_FIELD_Y;
        else if(py<=(-MAX_FIELD_Y)) py = -MAX_FIELD_Y;

        //auto error_radius = std::hypot(std::fabs(robot.x-px),std::fabs(robot.y-py));
        //auto error_angle = std::atan2(std::fabs(robot.y-py),std::fabs(robot.x-px));

        go_to_pos(px,py,targetw,"",false,false);

        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            stop();
        }

        break;
    }
    }

}

void Strategy::corner_kick(){

    switch(robot.role){
    case GOALIE:{
        robot.state = GK;
        return;
    }
    case STRIKER:{

        std::random_device x_rd, y_rd;
        std::uniform_real_distribution<double> x_rgen(2.5,4.8), y_rgen(-(std::fabs(ball.global_y)),std::fabs(ball.global_y));

        static auto px = x_rgen(x_rd);
        static auto py = y_rgen(y_rd);
        auto pw = std::atan2((ball.global_y-px),(ball.global_x-px));

        if(robot.gstate==GAMEPLAY){

            auto dist = std::fabs(ball.global_y - robot.y);

            go_to_pos(ball.global_x,ball.global_y,robot.theta,"",false,false);
            if(dist <= 1.0 || ball.global_x<=robot.x || robot.isBallEngaged){
                robot.state = BALL_SEARCHING;
                px = x_rgen(x_rd);
                py = y_rgen(y_rd);
                return;
            }
            return;
        }

        //auto error_radius = std::hypot(std::fabs(robot.x-px),std::fabs(robot.y-py));
        //auto error_angle = std::atan2(std::fabs(robot.y-py),std::fabs(robot.x-px));
        go_to_pos(px,py,pw,"",false,false);
        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            stop();
        }
        break;
    }
    case DEFENDER:{

        if(robot.gstate==GAMEPLAY){

            auto angle = std::hypot((robot.neighbor_y-ball.global_y),(robot.neighbor_x-ball.global_x));
            go_to_pos(ball.global_x,ball.global_y,angle,"",true,false);

            if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                    //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                    ){
                robot.state = KICK_BALL;
                return;
            }
            return;
        }

        auto px = ball.global_x-1.0;
        auto py = ball.global_y;
        //auto error_radius = std::hypot(std::fabs(robot.x-px),std::fabs(robot.y-py));
        //auto error_angle = std::atan2(std::fabs(robot.y-py),std::fabs(robot.x-px));
        go_to_pos(px,py,0.0,"",false,false);
        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            stop();
        }

        break;
    }
    }
}

void Strategy::stop(){

    go_to_pos(0.0,0.0,0.0,"local",false,false);

}

void Strategy::dribble_ball(){

    auto error_radius = std::hypot(std::fabs(robot.x-robot.kick_x),std::fabs(robot.y-robot.kick_y));
    auto error_angle = std::atan2(std::fabs(robot.y-robot.kick_y),std::fabs(robot.x-robot.kick_x));

    auto angle = std::atan2((GOAL_POS_Y-robot.y),(GOAL_POS_X-robot.x));

    if(robot.gstate==HALT){
        stop();
        robot.state=TIMER;
        return;
    }

    switch (robot.role) {
    case GOALIE:
    {
        robot.state = GK;
        break;
    }
    case DEFENDER:{

        robot.state = robot.isBallEngaged?robot.state:BALL_SEARCHING;


        if(std::hypot((robot.x-robot.last_x),(robot.y-robot.last_y))<MAXIMUM_DRIBBLE_DISTANCE){

            go_to_pos(robot.kick_x,robot.kick_y,robot.kick_angle,"",true,true);

            if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                    //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                    ){
                robot.state = KICK_BALL;
                return;
            }
        }
        else{

            go_to_pos(0.0,0.0,angle,"local",true,false);

            if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                    //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                    ){
                robot.state = KICK_BALL;
                return;
            }

        }

        break;
    }
    case STRIKER:{

        robot.state = robot.isBallEngaged?robot.state:BALL_SEARCHING;

        if(std::hypot((robot.x-robot.last_x),(robot.y-robot.last_y))<MAXIMUM_DRIBBLE_DISTANCE){

            go_to_pos(robot.kick_x,robot.kick_y,robot.kick_angle,"",true,true);

            if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                    //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                    ){
                robot.state = KICK_BALL;
                return;
            }
        }
        else{

            go_to_pos(0.0,0.0,angle,"local",true,false);

            if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                    //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                    ){
                robot.state = KICK_BALL;
                return;
            }

        }

        break;
    }
    }

}

void Strategy::timer(){

    auto now = ros::Time::now();
    auto radius = std::hypot(ball.local_x,ball.local_y);
    //auto angle = std::atan2(ball.local_y,ball.local_x);

    //auto error_radius = std::hypot(std::fabs(robot.x-robot.home_x),std::fabs(robot.y-robot.home_y));
    //auto error_angle = std::atan2(std::fabs(robot.y-robot.home_y),std::fabs(robot.x-robot.home_x));

    static bool isKickOff = true;
    static bool isCornerKick = true;
    static bool isFreekick = true;
    static bool isPenalty = true;

    switch(robot.role){
    case GOALIE:{
        robot.state = GK;
        return;
    }
    case DEFENDER:{
        break;
    }
    case STRIKER:{
        break;
    }
    }

    if(robot.rstate==OUR_KICKOFF){
        robot.state = KICKOFF;
        isKickOff = true;
        return;
    }
    else if(robot.rstate==OPP_KICKOFF){
        isKickOff = false;
    }

    if(robot.rstate==OUR_FREEKICK){
        robot.state = FREEKICK;
        isFreekick = true;
        return;
    }
    else if(robot.rstate==OPP_FREEKICK){
        isFreekick = false;
    }

    if(robot.rstate==OUR_CORNERKICK){
        robot.state = CORNERKICK;
        isCornerKick = true;
        return;
    }
    else if(robot.rstate==OPP_CORNERKICK){
        isCornerKick = false;
    }

    if(robot.rstate==OUR_PENALTY){
        robot.state = PENALTY;
        isPenalty = true;
        return;
    }
    else if(robot.rstate==OPP_PENALTY){
        isPenalty = false;
    }

    if(robot.gstate==POSITIONING){
        last_time = now;
        go_to_pos(robot.home_x,robot.home_y,robot.home_theta,"",false,false);
        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            stop();
        }
    }
    else if(robot.gstate==GAMEPLAY){
        if(!isPenalty){
            return;
        }
        if(!isKickOff || !isFreekick|| !isCornerKick){
            if((now-last_time)>ros::Duration(6.0)){
                isKickOff = true;
                isCornerKick = true;
                isCornerKick = true;
                robot.state = HOMING;
                return;
            }
            if(radius<=1.0){
                isKickOff = true;
                isCornerKick = true;
                isCornerKick = true;
                robot.state = HOMING;
                return;
            }
        }
        else{
            robot.state = HOMING;
            return;
        }
    }
    else if(robot.gstate==HALT){
        stop();
    }

}

void Strategy::positioning(){

    auto angle = atan2(ball.local_y,ball.local_x);
    std::random_device x_rd;
    std::uniform_real_distribution<double> x_rgen(0.0,4.8);
    static double random_x = 0.0;

    if(robot.gstate==HALT){
        stop();
        robot.state=TIMER;
        return;
    }

    switch (robot.role) {
    case GOALIE:
    {
        robot.state = GK;
        break;
    }
    case DEFENDER:{

        if(ball.global_x>=-1.0){
            go_to_pos(DEFENSE_LINE_1,ball.global_y,angle,"",false,false);
        }
        else{
            go_to_pos(DEFENSE_LINE_2,ball.global_y,angle,"",false,false);
        }

        if(robot.isNearestoBall || ball.local_y<=RADIUS_BALL_DRIBBLE){
            robot.state = BALL_SEARCHING;
            return;
        }

        break;
    }
    case STRIKER:{

        //auto error_radius = std::hypot(std::fabs(robot.x-random_x),std::fabs(robot.y-ball.global_y));
        //auto error_angle = std::atan2(std::fabs(robot.y-ball.global_y),std::fabs(robot.x-random_x));

        go_to_pos(random_x,ball.global_y,angle);

        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            random_x = x_rgen(x_rd);
        }


        if(robot.isNearestoBall || ball.local_y<=RADIUS_BALL_DRIBBLE){
            robot.state = BALL_SEARCHING;
            return;
        }

        break;
    }
    }

}

void Strategy::kick_off(){

    static bool isRight;
    //auto error_radius = std::hypot(std::fabs(robot.x-KICK_OFF_X),std::fabs(robot.y-KICK_OFF_Y));
    //auto error_angle = std::atan2(std::fabs(robot.y-KICK_OFF_Y),std::fabs(robot.x-KICK_OFF_X));

    if(robot.gstate==GAMEPLAY){
        if(isRight){
            go_to_pos(0.0,-0.1,-(KICK_OFF_ANGLE),"",true,false);
            if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                    //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                    ){
                robot.state = KICK_BALL;
                return;
            }
        }
        else{
            go_to_pos(ball.global_x,robot.y,robot.theta,"",true,false);
            if(ball.global_y>=robot.y || robot.isBallEngaged){
                robot.state = BALL_SEARCHING;
                return;
            }
        }
        return;
    }

    if(robot.playarea==OUR_RIGHT || robot.playarea==OPP_LEFT){
        go_to_pos(KICK_OFF_X,-KICK_OFF_Y,-(KICK_OFF_ANGLE),"",false,true);
        isRight = true;
        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            stop();
        }
    }
    else if(robot.playarea==OUR_LEFT || robot.playarea==OPP_RIGHT){
        go_to_pos(KICK_OFF_X,KICK_OFF_Y,(KICK_OFF_ANGLE),"",false,true);
        isRight = false;
        if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                ){
            stop();
        }
    }

}

void Strategy::ball_search(){

    static bool isDribble;
    static double random_x = 1.75;
    static double random_y = 0.0;
    std::random_device x_rd, y_rd;
    std::uniform_real_distribution<double> x_rgen(-2.5,2.5), y_rgen(-2.0,2.0);

    auto radius = std::hypot(ball.local_x,ball.local_y);
    auto angle = std::atan2(ball.local_y,ball.local_x);

    if(robot.gstate==HALT) {
        stop();
        robot.state=TIMER;
        return;
    }

    if(ball.isVisible){

        robot.state = robot.isBallEngaged? DRIBBLE_BALL:robot.state;
        if(robot.state==DRIBBLE_BALL) {
            robot.last_x = robot.x;
            robot.last_y = robot.y;
            return;
        }
        robot.state = (!robot.isNearestoBall)? HOMING:robot.state;
        if(robot.state==HOMING) return;

        isDribble = radius<RADIUS_BALL_DRIBBLE? true:false;
        isDribble = angle<ANGLE_BALL_DRIBBLE? true:false;

        go_to_pos(ball.local_x,ball.local_y,angle,"local",isDribble,false);

    }
    else{

        switch(robot.role){
        case STRIKER:{
            //auto error_radius = std::hypot(std::fabs(robot.x-random_x),std::fabs(robot.y-random_y));
            //auto error_angle = std::atan2(std::fabs(robot.y-random_y),std::fabs(robot.x-random_x));

            go_to_pos(random_x,random_y,0.0,"",false,false);

            if(robot.error_radius<=ERROR_RADIUS && robot.error_angle<=ERROR_ANGLE
                    //error_radius<=ERROR_RADIUS && error_angle<=ERROR_ANGLE
                    ){
                random_x = x_rgen(x_rd);
                random_y = y_rgen(y_rd);
            }
            break;
        }
        case DEFENDER:{
            robot.state = HOMING;
            return;
        }
        }

    }

}

void Strategy::kick_ball(){

    int kick_mode;

    kick_mode = robot.last_state==KICKOFF?2:robot.last_state==CORNERKICK?2:3;

    kicker_service.request.kick_request = kick_mode;

    if(ros::service::call(robot_name+std::string("/kick_service"),kicker_service)){
        ROS_INFO("KICK SUCCESS!!");
    }
    else{
        ROS_INFO("KICK FAILED!!");
    }

    robot.state = robot.role==GOALIE? GK : robot.last_state==PENALTY? TIMER:HOMING;

}

void Strategy::GoalieIntercept(const fukuro_common::InterceptPoint::ConstPtr &point){

    robot.intercept_x = point->intercept_point.x;
    robot.intercept_y = point->intercept_point.y;
    robot.intercept_theta = point->intercept_point.theta;
    robot.isInterceptValid = point->valid;

}

void Strategy::updateWorldModelInfo(const fukuro_common::WorldModel::ConstPtr &wm){

    robot.x = wm->pose.x;
    robot.y = wm->pose.y;
    robot.theta = wm->pose.theta;

    ball.local_x = wm->local_balls_kf.x;
    ball.local_y = wm->local_balls_kf.y;
    ball.global_x = wm->global_balls_kf.x;
    ball.global_x = wm->global_balls_kf.x;

    auto now = ros::Time::now();
    if(wm->ball_visible){
        ball.isVisible = true;
        ball.last_seen = now;
    }
    else if( (now - ball.last_seen) > ros::Duration(1) ){
        ball.isVisible = false;
    }

}

void Strategy::updateRobotControlInfo(const fukuro_common::RobotControlInfo::ConstPtr &robotinfo){

    robot.error_radius = robotinfo->error_radius;
    robot.error_angle = robotinfo->error_angle;

}

void Strategy::updateTeammatesInfo(const fukuro_common::Teammates::ConstPtr &teammates){

    robot.robot_name = teammates->robotname;

    switch(teammates->behavior){
    case 0:
    {
        robot.gstate = POSITIONING;
        break;
    }
    case 1:
    {
        robot.gstate = GAMEPLAY;
    }
    case 2:
    {
        robot.gstate = HALT;
    }

    }

    robot.rstate = static_cast<RefereeState>(teammates->state);
    //robot.rstate = rstateChecking(teammates->state);

    if(teammates->isManualPositioning){
        robot.home_x = robot.x;
        robot.home_y = robot.y;
        robot.home_theta = robot.theta;
    }

    while(!robot.neighbor_ball.empty()) robot.neighbor_ball.pop();
    while(!robot.neighbor.empty()) robot.neighbor.pop();

    for(int i=0; i<teammates->pose.size();i++){
        if(teammates->available[i]){
            if(i!=robot.robot_name){
                auto dist = std::hypot((ball.global_x-teammates->pose[i].x),(ball.global_y-teammates->pose[i].y));
                auto angle = std::atan2((ball.global_y-teammates->pose[i].y),(ball.global_x-teammates->pose[i].x));
                robot.neighbor_ball.push(std::make_tuple(dist,angle,i));

                dist = std::hypot((teammates->pose[i].x-robot.x),(teammates->pose[i].y-robot.y));
                angle = std::atan2((teammates->pose[i].y-robot.y),(teammates->pose[i].x-robot.x));
                robot.neighbor.push(std::make_tuple(dist,angle,i));
            }
            else{
                auto dist = std::hypot((ball.local_x),(ball.local_y));
                auto angle = std::atan2((ball.local_y),(ball.local_x));
                robot.neighbor_ball.push(std::make_tuple(dist,angle,i));
            }
        }
    }

    auto top = robot.neighbor_ball.top();
    robot.isNearestoBall = std::get<2>(top) == robot.robot_name? true:false;
    top = robot.neighbor.top();
    auto idx = std::get<2>(top);
    robot.neighbor_x = teammates->pose[idx].x;
    robot.neighbor_y = teammates->pose[idx].y;

}

void Strategy::updateStrategyPositioningInfo(const fukuro_common::StrategyPositioning::ConstPtr &strpos){

    robot.kick_x = strpos->kick_target.x;
    robot.kick_x = strpos->kick_target.y;
    robot.kick_angle = strpos->kick_angle;

    robot.ball_shield_angle = strpos->ball_shield_angle;
}

bool Strategy::StrategyRequestHandler(fukuro_common::StrategyService::Request &request, fukuro_common::StrategyService::Response &response){

    robot.role = roleChecking(request.role);
    response.ok = 1;

    std::string kick_req(request.option);
    if(kick_req.find("--kick_pos")!=std::string::npos){
        robot.kick_x = request.kick_pos.x;
        robot.kick_y = request.kick_pos.y;
        robot.kick_angle = request.kick_pos.theta;
    }
    else if(kick_req.find("-k")!=std::string::npos){
        robot.kick_x = request.kick_pos.x;
        robot.kick_y = request.kick_pos.y;
        robot.kick_angle = request.kick_pos.theta;
    }

    std::string home_req(request.option);
    if(home_req.find("--home_pos")!=std::string::npos){
        robot.home_x = request.home_pos.x;
        robot.home_y = request.home_pos.y;
        robot.home_theta = request.home_pos.theta;
    }
    else if(kick_req.find("-h")!=std::string::npos){
        robot.home_x = request.home_pos.x;
        robot.home_y = request.home_pos.y;
        robot.home_theta = request.home_pos.theta;
    }

    std::string strequest (request.strategy_state);
    robot.state = checkState(strequest);


}

#ifndef FIRST_BUILD
/*
void Strategy::reconfigure(const fukuro_strategy::FukuroStrategyConfig &config, uint32_t level){

    / *

    angle_dribble_auto_on = config.angle_dribble_auto_on;
    angle_mulai_dribble_bola = config.angle_mulai_dribble_bola;
    error_positioning = config.error_positioning;
    error_sudut_positioning = config.error_sudut_positioning;
    min_error_posisi_homing = config.min_error_posisi_homing;
    min_error_posisi_kick = config.min_error_posisi_kick;
    min_error_sudut_homing = config.min_error_sudut_homing;
    min_error_sudut_kick = config.min_error_sudut_kick;
    radius_dribble_auto_on = config.radius_dribble_auto_on;
    radius_mulai_dribble_bola = config.radius_mulai_dribble_bola;
    radius_mulai_kick_off = config.radius_mulai_kick_off;

    * /

}
*/
#endif

bool RobotInfo::CheckisBallEngaged(double x, double y){

    bool ret;

    ret = std::hypot(x,y)<=RADIUS_BALL_ENGAGED? true : false;

    return ret;
}

PlayArea RobotInfo::checkPlayArea(double x, double y){

    PlayArea ret;

    if((x>0) && (y>0)){
        ret = OPP_RIGHT;
    }
    else if((x>0) && (y<=0)){
        ret = OPP_LEFT;
    }
    else if((x<=0) && (y>0)){
        ret = OUR_LEFT;
    }
    else if((x<=0) && (y<=0)){
        ret = OUR_RIGHT;
    }

    return ret;

}

void Strategy::process(){

    fukuro_common::StrategyInfo strinfo;
    strinfo.strategy_state = getState(robot.state);
    strinfo.role = getRole(robot.role);

    strinfo_pub.publish(strinfo);
    robotcontrol_pub.publish(rcontrolmsg);

    //robot.playarea = robot.checkPlayArea(robot.x,robot.y);
    robot.isBallEngaged = robot.CheckisBallEngaged(ball.local_x,ball.local_y);

    switch (robot.state) {

    case TIMER:{
        robot.playarea = robot.checkPlayArea(robot.x,robot.y);
        timer();
        robot.last_state = TIMER;
        break;
    }

    case KICKOFF:{
        kick_off();
        robot.last_state = KICKOFF;
        break;
    }

    case IDLE:{
        robot.playarea = robot.checkPlayArea(robot.x,robot.y);
        stop();
        robot.last_state = IDLE;
        break;
    }

    case HOMING:{
        positioning();
        robot.last_state = HOMING;
        break;
    }

    case BALL_SEARCHING:{
        ball_search();
        robot.last_state = BALL_SEARCHING;
        break;
    }

    case DRIBBLE_BALL:{
        dribble_ball();
        robot.last_state = DRIBBLE_BALL;
        break;
    }

    case KICK_BALL:{
        kick_ball();
        robot.last_state = KICK_BALL;
        break;
    }

    case CORNERKICK:{
        corner_kick();
        robot.last_state = CORNERKICK;
        break;
    }

    case GK:{
        goalie();
        robot.last_state = GK;
        break;
    }

    case FREEKICK:{
        free_kick();
        robot.last_state = FREEKICK;
        break;
    }
    case PENALTY:{
        penalty();
        robot.last_state = PENALTY;
        break;
    }

    };

}


