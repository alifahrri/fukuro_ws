//#define FAHRI_ALI_LEGACY

#ifndef FAHRI_ALI_LEGACY

#include "strategy.h"
#include <ros/ros.h>

int main(int argc, char** argv){

    ros::init(argc,argv,"fukuro_strategy");
    ros::NodeHandle node;
    fukuro::Strategy str(node);
    char* env;
    env = std::getenv("FUKURO");
    if(env==NULL)
    {
        ROS_ERROR("robot name empty, export FUKURO=fukuro1!!!");
        exit(-1);
    }
    std::string robot_name(env);

    std::string role;
    if(ros::param::get("/robot_role",role)){
        if(role=="goalie"){
            ros::Subscriber goalintercept = node.subscribe(robot_name+std::string("/goalie_intercept"),1,&fukuro::Strategy::GoalieIntercept,&str);
        }
    }

    ros::ServiceServer strategy_server = node.advertiseService(robot_name+std::string("/strategy_service"),&fukuro::Strategy::StrategyRequestHandler,&str);
    ros::Subscriber worldmodel = node.subscribe(robot_name+std::string("/world_model"),1,&fukuro::Strategy::updateWorldModelInfo,&str);
    ros::Subscriber strpos = node.subscribe(robot_name+std::string("/strategy_pos"),1,&fukuro::Strategy::updateStrategyPositioningInfo,&str);
    ros::Subscriber roboctrlinfo = node.subscribe("/robot_control_info",1,&fukuro::Strategy::updateRobotControlInfo,&str);
    ros::Subscriber teammates = node.subscribe(robot_name+std::string("/teammates"),1,&fukuro::Strategy::updateTeammatesInfo,&str);
#ifndef FIRST_BUILD
    //dynamic_reconfigure::Server<fukuro_strategy::FukuroStrategyConfig> reconfigure_server;
    //reconfigure_server.setCallback(boost::bind(&fukuro::Strategy::reconfigure,&str,_1,_2));
#endif
    ros::AsyncSpinner spinner(2);
    if(spinner.canStart())
        spinner.start();
    ros::Rate loop(27);
    while(ros::ok())
    {
        str.process();
        loop.sleep();
    }
    ros::waitForShutdown();
    return 0;



}

#endif

#ifdef FAHRI_ALI_LEGACY
#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <fukuro_common/WorldModel.h>
#include <fukuro_common/KeypadService.h>
#include <fukuro_common/OdometryService.h>
#include <fukuro_common/VelCmd.h>
#include <fukuro_common/DribblerControl.h>
#include <fukuro_common/Shoot.h>
#include <fukuro_common/Compass.h>
#include <fukuro_strategy/FukuroStrategyConfig.h>
#include <thread>
#include <mutex>
#include <chrono>

#define CMPS_KICK

#define START_POS_1_X -4.8
#define START_POS_1_Y -1.8
#define START_POS_1_W 0.0
#define START_POS_2_X -4.8
#define START_POS_2_Y 1.8
#define START_POS_2_W 0.0

#define HOME1_DEFENDER_X -3.00
#define HOME1_DEFENDER_Y -0.55
#define HOME1_DEFENDER_W 0.0
#define HOME2_DEFENDER_X -3.0
#define HOME2_DEFENDER_Y -0.55
#define HOME2_DEFENDER_W 0.0
#define HOME_STRIKER_X -1.50
#define HOME_STRIKER_Y 0.0
#define HOME_STRIKER_W 0.0

#define DRIBBLE_POS_X 3.0
#define DRIBBLE_POS_Y 0.5
#define DRIBBLE_POS_W 0.1

#define DEFENDER_DRIBBLE_POS_X 1.0
#define DEFENDER_DRIBBLE_POS_Y 0.4
#define DEFENDER_DRIBBLE_POS_W 0.0

#define STRIKER_DRIBBLE_POS_X 3.00
#define STRIKER_DRIBBLE_POS_Y -0.55
#define STRIKER_DRIBBLE_POS_W -M_PI_2

#define ERROR_POS_GOTO_HOME 0.1

typedef boost::function<void(const fukuro_common::WorldModel::ConstPtr&)> WorldModelCallback;
typedef boost::function<bool(fukuro_common::KeypadService::Request&, fukuro_common::KeypadService::Response&)> KeypadServiceCallback;
typedef boost::function<void(const fukuro_common::Compass::ConstPtr&)> CompassCallback;
typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::duration<double,std::milli> Duration;
typedef std::chrono::high_resolution_clock::time_point TimePoint;

enum robot_strategy_t {
    stop,
    cari_bola,
    cari_bola_pasif,
    dekati_bola,
    go_to_home,
    kick_to_goal,
    dribble_ball,
    striker_dribble_exe,
    defend1,
    goalie_exe
};

enum robot_role_t {
    striker,
    striker_dribble,
    defender_attack,
    defender_full_defend,
    goalie
};

enum robot_start_pos_t {
    start_pos_1,
    start_pos_2,
    home
};

enum kick_off_t {
    kick_off_lawan,
    kick_off_teman
};

double robot_x = 0.0;
double robot_y = 0.0;
double robot_theta = 0.0;
double robot_vx = 0.0;
double robot_vy = 0.0;
double robot_w = 0.0;
double ball_x = 0.0;
double ball_y = 0.0;
double local_ball_x = 0.0;
double local_ball_y = 0.0;
double local_ball_x_kf = 0.0;
double local_ball_y_kf = 0.0;
double vm = 1.0;
double wm = 0.75;
double accel = 0.1;
double v_target = 0.0;
double angle = 0.0;
double radius = 0.0;
double angle_kf = 0.0;
double radius_kf = 0.0;
double cmd_vx = 0.0;
double cmd_vy = 0.0;
double cmd_w = 0.0;
double kp_vel = 2.0;
double kd_vel = 1.0;
double kp_w = 1.0;
double kd_w = 1.0;
double kp_dribbler_maju = 0.3;
double kp_dribbler_mundur = 1.0;
double kp_dribbler_vy = 1.0;
double kp_dribbler_w = 1.0;
double dribbler_radius = 0.2;
double cmps = 0.0;
double cyan_goal_cmps = 0.0;
double magenta_goal_cmps = 180.0;
bool our_team_is_cyan = true;
int dribbler_speed = 200;
bool ball_visible = false;
bool strategy_loop_running = false;
bool control_loop_running = false;
double cari_bola_waypoint_x[4] = { 1.0, 2.0,2.0,1.0};
double cari_bola_waypoint_y[4] = {-1.5,-1.5,1.5,1.5};
int cari_bola_waypoint_counter = 0;
double goalie_homing1_waypoint_x[3] = {-3.5,-3.5,-4.4};
double goalie_homing1_waypoint_y[3] = {-1.8,0.0,0.0};
int goalie_homing1_waypoint_counter = 0;
double goalie_homing2_waypoint_x[3] = {-3.5,-3.5,-4.4};
double goalie_homing2_waypoint_y[3] = {1.8,0.0,0.0};
int goalie_homing2_waypoint_counter = 0;
double striker_dribble_waypoint_x[2] = {STRIKER_DRIBBLE_POS_X,4.6};
double striker_dribble_waypoint_y[2] = {STRIKER_DRIBBLE_POS_Y,STRIKER_DRIBBLE_POS_Y};
double striker_dribble_index = 0;
TimePoint kick_off_lawan_t0 = Clock::now();
ros::Publisher vel_pub;
ros::Publisher dribbler_pub;
ros::ServiceClient kick_client;
robot_strategy_t robot_strategy = stop;
robot_role_t robot_role = defender_attack;
robot_start_pos_t robot_start = start_pos_1;
kick_off_t kick_off = kick_off_teman;
Duration wait_kick_off_lawan(5500);

int quad(double& _angle)
{
    if(_angle>=0.0 && _angle<=90.0)
        return 1;
    else if(_angle>90.0 && _angle<=180.0)
        return 2;
    else if(_angle>180.0 && _angle<=270.0)
        return 3;
    else if(_angle>270.0 && _angle<=360.0)
        return 4;
}

void strategy_loop()
{
    strategy_loop_running = true;
    std::cout << "start strategy loop\n";
    Duration wait_ms(33.0);
    while(strategy_loop_running)
    {
        auto t0 = Clock::now();
        auto t1 = Clock::now();
        Duration dt = t1-t0;
        if(dt<wait_ms)
            std::this_thread::sleep_for(wait_ms-dt);
    }
    std::cout << "exit strategy loop\n";
}

void control_loop()
{
    control_loop_running = true;
    std::cout << "start control loop\n";
    Duration wait_ms(33.0);
    Duration ball_visible_dt(0.0);
    TimePoint ball_visible_t0 = Clock::now();
    fukuro_common::VelCmd cmd;
    fukuro_common::DribblerControl dribbler;
    while(control_loop_running)
    {
        auto t0 = Clock::now();
        std::cout << "team : " << (our_team_is_cyan ? std::string("cyan\n") : std::string("magenta\n"));
        switch (robot_strategy) {
        case stop:
            cmd_vx = 0.0;
            cmd_vy = 0.0;
            cmd_w = 0.0;
            dribbler.dir_in = 0;
            dribbler.speed = 0;
            std::cout << "[ControlLoop] stop\n";
            break;
        case dekati_bola:
        {
            ball_visible_dt = t0 - ball_visible_t0;
            if(ball_visible)
                ball_visible_t0 = t0;
            if(ball_visible_dt < Duration(1000.0))
            {
                static double error = 0.0;
                static double last_error = 0.0;
                static double error_angle = 0.0;
                static double last_error_angle = 0.0;
                error = radius_kf;
                error_angle = angle_kf;
                double d_error = error - last_error;
                double d_error_angle = error_angle - last_error_angle;
                last_error = error;
                last_error_angle = error_angle;
                double cmd_vel = kp_vel*error + kd_vel*d_error;
                if(fabs(cmd_vel)>vm)
                    cmd_vel = cmd_vel < 0 ? -vm : vm;
                cmd_w = kp_w*error_angle + kd_w*d_error_angle;
                if(fabs(cmd_w)>wm)
                    cmd_w = cmd_w < 0 ? -wm : wm;
                cmd_vx = cos(angle)*cmd_vel;
                cmd_vy = sin(angle)*cmd_vel;
            }
            else if(robot_role==striker)
            {
                robot_strategy = cari_bola;
            }
            else if((robot_role==defender_attack) || (robot_role==defender_full_defend))
            {
                robot_strategy = go_to_home;
            }
            if((radius_kf<=0.2)&&(fabs(angle_kf)<=0.2))
            {
                dribbler.dir_in = 1;
                dribbler.speed = dribbler_speed;
            }
            if((radius_kf<=0.15)&&(fabs(angle_kf)<=0.045))
            {
                if(robot_role==striker)
                    robot_strategy = dribble_ball;
                else if(robot_role==striker_dribble)
                {
                    robot_strategy = striker_dribble_exe;
                    striker_dribble_index = 0;
                }
            }
            std::cout << "[ControlLoop] dekati bola\n";
        }
            break;
        case cari_bola_pasif:
            dribbler.dir_in = 1;
            dribbler.speed = 0;
            if(ball_visible)
                robot_strategy = dekati_bola;
            std::cout << "[ControlLoop] cari bola\n";
            break;
        case cari_bola:
        {
            dribbler.dir_in = 1;
            dribbler.speed = 0;
            int idx = cari_bola_waypoint_counter;
            double target_x = cari_bola_waypoint_x[idx];
            double target_y = cari_bola_waypoint_y[idx];
            double target_theta = 0.0;
            double dx = (target_x-robot_x);
            double dy = (target_y-robot_y);
            double c = cos(robot_theta);
            double s = sin(robot_theta);
            double local_target_x = c*dx+s*dy;
            double local_target_y = -s*dx+c*dy;
            double local_target_radius = sqrt(dx*dx+dy*dy);
            double local_target_angle = atan2(local_target_y,local_target_x);
            static double error = 0.0;
            static double last_error = 0.0;
            static double error_angle = 0.0;
            static double last_error_angle = 0.0;
            error = local_target_radius;
            error_angle = target_theta - robot_theta;
            double d_error = error - last_error;
            double d_error_angle = error_angle - last_error_angle;
            last_error = error;
            last_error_angle = error_angle;
            double cmd_vel = kp_vel*error + kd_vel*d_error;
            if(fabs(cmd_vel)>vm)
                cmd_vel = cmd_vel < 0 ? -vm : vm;
            cmd_w = kp_w*error_angle + kd_w*d_error_angle;
            if(fabs(cmd_w)>wm)
                cmd_w = cmd_w < 0 ? -wm : wm;
            cmd_vx = cos(local_target_angle)*cmd_vel;
            cmd_vy = sin(local_target_angle)*cmd_vel;
            if(ball_visible)
                robot_strategy = dekati_bola;
            else if(error<0.05)
            {
                cari_bola_waypoint_counter++;
                if(cari_bola_waypoint_counter>3)
                    cari_bola_waypoint_counter = 0;
            }
            std::cout << "[ControlLoop] dribble ball\n";
        }
            break;
        case go_to_home:
        {
            double home_x = 0.0;
            double home_y = 0.0;
            double home_theta = 0.0;
            if((robot_role == striker) || (robot_role==striker_dribble))
            {
                home_x = HOME_STRIKER_X;
                home_y = HOME_STRIKER_Y;
            }
            else if(((robot_role == defender_attack) || (robot_role == defender_full_defend))
                    && (robot_start == start_pos_1))
            {
                home_x = HOME1_DEFENDER_X;
                home_y = HOME1_DEFENDER_Y;
            }
            else if(((robot_role == defender_attack) || (robot_role == defender_full_defend))
                    && (robot_start == start_pos_2))
            {
                home_x = HOME2_DEFENDER_X;
                home_y = HOME2_DEFENDER_Y;
            }
            else if((robot_role == goalie) && (robot_start == start_pos_1))
            {
                int idx = goalie_homing1_waypoint_counter;
                home_x = goalie_homing1_waypoint_x[idx];
                home_y = goalie_homing1_waypoint_y[idx];
            }
            else if((robot_role == goalie) && (robot_start == start_pos_2))
            {
                int idx = goalie_homing2_waypoint_counter;
                home_x = goalie_homing2_waypoint_x[idx];
                home_y = goalie_homing2_waypoint_y[idx];
            }
            double dx = (home_x-robot_x);
            double dy = (home_y-robot_y);
            double c = cos(robot_theta);
            double s = sin(robot_theta);
            double local_target_x = c*dx+s*dy;
            double local_target_y = -s*dx+c*dy;
            double local_target_radius = sqrt(dx*dx+dy*dy);
            double local_target_angle = atan2(local_target_y,local_target_x);
            static double error = 0.0;
            static double last_error = 0.0;
            static double error_angle = 0.0;
            static double last_error_angle = 0.0;
            error = local_target_radius;
            error_angle = home_theta - robot_theta;
            double d_error = error - last_error;
            double d_error_angle = error_angle - last_error_angle;
            last_error = error;
            last_error_angle = error_angle;
            double cmd_vel = kp_vel*error + kd_vel*d_error;
            if(fabs(cmd_vel)>vm)
                cmd_vel = cmd_vel < 0 ? -vm : vm;
            cmd_w = kp_w*error_angle + kd_w*d_error_angle;
            if(fabs(cmd_w)>wm)
                cmd_w = cmd_w < 0 ? -wm : wm;
            cmd_vx = cos(local_target_angle)*cmd_vel;
            cmd_vy = sin(local_target_angle)*cmd_vel;
            if(kick_off == kick_off_lawan)
            {
                TimePoint t_now = Clock::now();
                Duration dt = t_now - kick_off_lawan_t0;
                if(dt>wait_kick_off_lawan)
                    if(error<ERROR_POS_GOTO_HOME)
                    {
                        if(robot_role == striker)
                            robot_strategy = cari_bola;
                        else if(robot_role == defender_attack)
                            robot_strategy = cari_bola_pasif;
                        else if(robot_role == defender_full_defend)
                            robot_strategy == defend1;
                    }
            }
            else if(error<ERROR_POS_GOTO_HOME)
            {
                if(robot_role == striker)
                    robot_strategy = cari_bola;
                else if(robot_role == defender_attack)
                    robot_strategy = cari_bola_pasif;
                else if(robot_role == defender_full_defend)
                    robot_strategy == defend1;
                else if(robot_role==goalie)
                {
                    if(robot_start==start_pos_1)
                    {
                        goalie_homing1_waypoint_counter++;
                        if(goalie_homing1_waypoint_counter>2)
                            robot_strategy = goalie_exe;
                    }
                    else if(robot_start==start_pos_2)
                    {
                        goalie_homing2_waypoint_counter++;
                        if(goalie_homing2_waypoint_counter>2)
                            robot_strategy = goalie_exe;
                    }
                }
            }
            dribbler.dir_in = 1;
            dribbler.speed = 0;
            std::cout << "[ControlLoop] go to home => " << home_x << ", " << home_y << ", "  << home_theta << "\n";
        }
            break;
        case dribble_ball:
        {
            double target_x = DRIBBLE_POS_X;
            double target_y = DRIBBLE_POS_Y;
            double target_theta = DRIBBLE_POS_W;
            if(robot_role==defender_full_defend)
            {
                target_x = DEFENDER_DRIBBLE_POS_X;
                target_y = DEFENDER_DRIBBLE_POS_Y;
                target_theta = DEFENDER_DRIBBLE_POS_W;
            }
            double dx = (target_x-robot_x);
            double dy = (target_y-robot_y);
            double c = cos(robot_theta);
            double s = sin(robot_theta);
            double local_target_x = c*dx+s*dy;
            double local_target_y = -s*dx+c*dy;
            double local_target_radius = sqrt(dx*dx+dy*dy);
            double local_target_angle = atan2(local_target_y,local_target_x);
            static double error = 0.0;
            static double last_error = 0.0;
            static double error_angle = 0.0;
            static double last_error_angle = 0.0;
            error = local_target_radius;
            error_angle = target_theta - robot_theta;
            double d_error = error - last_error;
            double d_error_angle = error_angle - last_error_angle;
            last_error = error;
            last_error_angle = error_angle;
            double cmd_vel = kp_vel*error + kd_vel*d_error;
            if(fabs(cmd_vel)>vm)
                cmd_vel = cmd_vel < 0 ? -vm : vm;
            cmd_w = kp_w*error_angle + kd_w*d_error_angle;
            if(fabs(cmd_w)>wm)
                cmd_w = cmd_w < 0 ? -wm : wm;
            cmd_vx = cos(local_target_angle)*cmd_vel;
            cmd_vy = sin(local_target_angle)*cmd_vel;
            dribbler.dir_in = 1;
            double drib_speed = dribbler_speed*((cmd_vx<0) ? (kp_dribbler_mundur*fabs(cmd_vx)) : (kp_dribbler_maju*cmd_vx)) +
                    dribbler_speed*(fabs(cmd_vy)*kp_dribbler_vy) +
                    dribbler_speed*(fabs(cmd_w)*kp_dribbler_w);
            dribbler.speed = (drib_speed>255.0) ? 255.0 : drib_speed;
            if(error<0.01)
                robot_strategy = kick_to_goal;
            if(radius_kf>0.175)
                robot_strategy = dekati_bola;
            std::cout << "[ControlLoop] dribble ball\n";
        }
            break;
        case kick_to_goal:
        {
#ifdef CMPS_KICK
            double target_theta = our_team_is_cyan ? magenta_goal_cmps : cyan_goal_cmps;
            static double error_angle = 0.0;
            static double last_error_angle = 0.0;
            if(quad(cmps)==1 && quad(target_theta)==4)
                error_angle = cmps + fabs(360.0 - target_theta);
            else if(quad(cmps)==4 && quad(target_theta)==1)
                error_angle = fabs(360.0 - cmps) + target_theta;
            else if(quad(cmps)==2 && quad(target_theta)==4)
            {
                double err_cw = fabs(360.0 - target_theta) + cmps;
                double err_ccw = target_theta - cmps;
                if(err_ccw<err_cw)
                    error_angle = err_ccw;
                else if(err_cw)
                    error_angle = -err_cw;
                else
                    error_angle = err_ccw;
            }
            else if(quad(cmps)==4 && quad(target_theta)==2)
            {
                double err_cw = cmps - target_theta;
                double err_ccw = target_theta + fabs(360.0-cmps);
                if(err_ccw<err_cw)
                    error_angle = err_ccw;
                else if(err_cw)
                    error_angle = -err_cw;
                else
                    error_angle = err_ccw;
            }
            else if(quad(cmps)==1 && quad(target_theta)==3)
            {
                double err_cw = fabs(360.0 - target_theta) + cmps;
                double err_ccw = target_theta - cmps;
                if(err_ccw<err_cw)
                    error_angle = err_ccw;
                else if(err_cw)
                    error_angle = -err_cw;
                else
                    error_angle = err_ccw;
            }
            else if(quad(cmps)==3 && quad(target_theta)==1)
            {
                double err_cw = fabs(360.0-cmps) + target_theta;
                double err_ccw = cmps - target_theta;
                if(err_ccw<err_cw)
                    error_angle = err_ccw;
                else if(err_cw)
                    error_angle = -err_cw;
                else
                    error_angle = err_ccw;
            }
            else
                error_angle = (target_theta - cmps);
            double d_error_angle = error_angle - last_error_angle;
#else
            double target_theta = 0.0;
            static double error_angle = 0.0;
            static double last_error_angle = 0.0;
            error_angle = target_theta - robot_theta;
            double d_error_angle = error_angle - last_error_angle;
            cmd_w = kp_w*error_angle + kd_w*d_error_angle;
            cmd_vx = 0.0;
            cmd_vy = 0.0;
#endif
            double cmdw = kp_w*error_angle + kd_w*d_error_angle;
            //      cmd_w = 0.0;
            cmd_w = cmdw;
            cmd_vx = 0.0;
            cmd_vy = 0.0;
            std::cout << "[ControlLoop] Kicking => cmps : " << cmps << ", target = " << target_theta << ", error = " << error_angle << ", cmd w : " << cmdw <<'\n';
            if(error_angle<0.075)
            {
                fukuro_common::Shoot shoot;
                shoot.request.kick_request = 3;
                if(kick_client.call(shoot))
                    std::cout << "[ControlLoop] Kick OK\n";
                else
                    std::cout << "[ControlLoop] Kick failed\n";
                robot_strategy = cari_bola;
                if(robot_role==defender_full_defend)
                    robot_strategy = go_to_home;
            }
        }
            break;
        case defend1:
        {
            static double error = 0.0;
            static double last_error = 0.0;
            static double error_angle = 0.0;
            static double last_error_angle = 0.0;
            static double target_theta = 0.0;
            static TimePoint first_home_t0 = Clock::now();
            static bool first_home = true;
            double target_y = 0.0;
            if(first_home)
            {
                target_y = (robot_start==start_pos_1) ? HOME1_DEFENDER_Y : HOME2_DEFENDER_Y;
                if(ball_visible)
                {
                    first_home = false;
                    first_home_t0 = Clock::now();
                }
            }
            else
            {
                target_y = (fabs(ball_y)>4.0) ?  (ball_y<0.0 ? -3.75 : 3.75) : ball_y;
                if(ball_visible)
                {
                    first_home_t0 = Clock::now();
                }
                Duration dt = Clock::now() - first_home_t0;
                if(dt < Duration(3.000))
                {
                    first_home = true;
                    last_error = 0.0;
                    last_error_angle = 0.0;
                }
            }
            double target_x = HOME1_DEFENDER_X;
            double dx = (target_x-robot_x);
            double dy = (target_y-robot_y);
            double c = cos(robot_theta);
            double s = sin(robot_theta);
            double local_target_x = c*dx+s*dy;
            double local_target_y = -s*dx+c*dy;
            double local_target_radius = sqrt(dx*dx+dy*dy);
            double local_target_angle = atan2(local_target_y,local_target_x);
            error = local_target_radius;
            error_angle = target_theta - robot_theta;
            double d_error = error - last_error;
            double d_error_angle = error_angle - last_error_angle;
            last_error = error;
            last_error_angle = error_angle;
            double cmd_vel = kp_vel*error + kd_vel*d_error;
            if(fabs(cmd_vel)>vm)
                cmd_vel = cmd_vel < 0 ? -vm : vm;
            cmd_w = kp_w*error_angle + kd_w*d_error_angle;
            if(fabs(cmd_w)>wm)
                cmd_w = cmd_w < 0 ? -wm : wm;
            cmd_vx = cos(local_target_angle)*cmd_vel;
            cmd_vy = sin(local_target_angle)*cmd_vel;
            if(local_ball_x_kf<0.3)
                robot_strategy = dekati_bola;
        }
            break;
        case striker_dribble_exe:
        {
            int i = (striker_dribble_index>1) ? 1 : striker_dribble_index;
            static bool wait = false;
            static TimePoint t0 = Clock::now();
            double target_x = striker_dribble_waypoint_x[i];
            double target_y = striker_dribble_waypoint_x[i];
            double target_theta = STRIKER_DRIBBLE_POS_W;
            double dx = (target_x-robot_x);
            double dy = (target_y-robot_y);
            double c = cos(robot_theta);
            double s = sin(robot_theta);
            double local_target_x = c*dx+s*dy;
            double local_target_y = -s*dx+c*dy;
            double local_target_radius = sqrt(dx*dx+dy*dy);
            double local_target_angle = atan2(local_target_y,local_target_x);
            static double error = 0.0;
            static double last_error = 0.0;
            static double error_angle = 0.0;
            static double last_error_angle = 0.0;
            error = local_target_radius;
            error_angle = target_theta - robot_theta;
            double d_error = error - last_error;
            double d_error_angle = error_angle - last_error_angle;
            last_error = error;
            last_error_angle = error_angle;
            double cmd_vel = kp_vel*error + kd_vel*d_error;
            if(fabs(cmd_vel)>vm)
                cmd_vel = cmd_vel < 0 ? -vm : vm;
            cmd_w = kp_w*error_angle + kd_w*d_error_angle;
            if(fabs(cmd_w)>wm)
                cmd_w = cmd_w < 0 ? -wm : wm;
            cmd_vx = cos(local_target_angle)*cmd_vel;
            cmd_vy = sin(local_target_angle)*cmd_vel;
            dribbler.dir_in = 1;
            double drib_speed = dribbler_speed*((cmd_vx<0) ? (kp_dribbler_mundur*fabs(cmd_vx)) : (kp_dribbler_maju*cmd_vx)) +
                    dribbler_speed*(fabs(cmd_vy)*kp_dribbler_vy) +
                    dribbler_speed*(fabs(cmd_w)*kp_dribbler_w);
            dribbler.speed = (drib_speed>255.0) ? 255.0 : drib_speed;
            if(wait)
            {
                TimePoint t_now = Clock::now();
                Duration dt = t_now - t0;
                if(dt<Duration(1000.0))
                {
                    striker_dribble_index++;
                    if(striker_dribble_index>1)
                        striker_dribble_index = 1;
                    wait = false;
                }
            }
            else if((error<0.1)&&(error_angle<0.1))
            {
                wait = true;
                t0 = Clock::now();
            }
            if(radius_kf>0.175)
                robot_strategy = dekati_bola;
            std::cout << "[ControlLoop] dribble ball\n";
        }
            break;
        case goalie_exe:
        {
            static double error = 0.0;
            static double last_error = 0.0;
            static double error_angle = 0.0;
            static double last_error_angle = 0.0;
            static double last_ball_y = 0.0;
            double target_x = goalie_homing2_waypoint_x[2];
            double target_y = 0.3*last_ball_y+0.7*ball_y;
            last_ball_y = ball_y;
            if(fabs(target_y)>0.9)
                target_y = (target_y<0) ? -0.9: 0.9;
            double target_theta = 0.0;
            double dx = (target_x-robot_x);
            double dy = (target_y-robot_y);
            double c = cos(robot_theta);
            double s = sin(robot_theta);
            double local_target_x = c*dx+s*dy;
            double local_target_y = -s*dx+c*dy;
            double local_target_radius = sqrt(dx*dx+dy*dy);
            double local_target_angle = atan2(local_target_y,local_target_x);
            error = local_target_radius;
            error_angle = target_theta - robot_theta;
            double d_error = error - last_error;
            double d_error_angle = error_angle - last_error_angle;
            last_error = error;
            last_error_angle = error_angle;
            double cmd_vel = kp_vel*error + kd_vel*d_error;
            if(fabs(cmd_vel)>vm)
                cmd_vel = cmd_vel < 0 ? -vm : vm;
            cmd_w = kp_w*error_angle + kd_w*d_error_angle;
            if(fabs(cmd_w)>wm)
                cmd_w = cmd_w < 0 ? -wm : wm;
            cmd_vx = cos(local_target_angle)*cmd_vel;
            cmd_vy = sin(local_target_angle)*cmd_vel;
        }
            break;
        default:
            cmd_vx = 0.0;
            cmd_vy = 0.0;
            cmd_w = 0.0;
            break;
        }
        cmd.Vx = cmd_vx;
        cmd.Vy = cmd_vy;
        cmd.w = cmd_w;
        vel_pub.publish(cmd);
        dribbler_pub.publish(dribbler);
        auto t1 = Clock::now();
        Duration dt = t1-t0;
        if(dt<wait_ms)
            std::this_thread::sleep_for(wait_ms-dt);
    }
    std::cout << "exit control loop\n";
}

void configure(const fukuro_strategy::FukuroStrategyConfig & config, uint32_t level)
{
    std::cout << "Reconfigure :  \n";
    vm = config.vm;
    wm = config.wm;
    kp_vel = config.kp_vel;
    kd_vel = config.kd_vel;
    kp_w = config.kp_angle;
    kd_w = config.kd_angle;
    kp_dribbler_maju = config.kp_dribbler_maju;
    kp_dribbler_mundur = config.kp_dribbler_mundur;
    kp_dribbler_vy = config.kp_dribbler_vy;
    kp_dribbler_w = config.kp_dribbler_w;
    dribbler_radius = config.dribbler_trig;
    dribbler_speed = config.dribbler_speed;
    cyan_goal_cmps = config.cyan_goal_cmps;
    magenta_goal_cmps = config.magenta_goal_cmps;
    std::cout << kp_vel << ", " << kd_vel << ", " << kp_w << ", " << kd_w << '\n';
    std::cout << "Reconfigure end\n";
}

int main(int argc, char **argv)
{
    ros::init(argc,argv,"fukuro_strategy_node");
    ros::NodeHandle node;

    std::string robot_name("fukuro1");
    if(argc>1)
    {
        for(int i=1; i<argc; i++)
        {
            std::string arg(argv[i]);
            std::cout << "[Main] argument " << i << " : " << arg << '\n';
            if(arg.find(std::string("fukuro"))!=std::string::npos)
            {
                robot_name = arg;
            }
            else if(arg.find(std::string("cyan"))!=std::string::npos)
            {
                our_team_is_cyan = true;
            }
            else if(arg.find(std::string("magenta"))!=std::string::npos)
            {
                our_team_is_cyan = false;
            }
        }
    }

    ros::ServiceClient odometry_client = node.serviceClient<fukuro_common::OdometryService>(robot_name+"/odometry_service");
    WorldModelCallback wm_cb = [](const fukuro_common::WorldModel::ConstPtr& model)
    {
        static int i=0;
        i++;
        robot_x = model->pose.x;
        robot_y = model->pose.y;
        robot_theta = model->pose.theta;
        robot_vx = model->velocity.x;
        robot_vy = model->velocity.y;
        robot_w = model->velocity.theta;
        if(model->balls.size())
        {
            //      ball_visible = true;
            ball_x = model->balls[0].x;
            ball_y = model->balls[0].y;
            local_ball_x = model->local_balls[0].x;
            local_ball_y = model->local_balls[0].y;
            local_ball_x_kf = model->local_balls_kf.x;
            local_ball_y_kf = model->local_balls_kf.y;
            angle = atan2(local_ball_y,local_ball_x);
            radius = sqrt(local_ball_x*local_ball_x+local_ball_y*local_ball_y);
            angle_kf = atan2(local_ball_y_kf,local_ball_x_kf);
            radius_kf = sqrt(local_ball_x_kf*local_ball_x_kf+local_ball_y_kf*local_ball_y_kf);
        }
        else
        {
            //      ball_visible = false;
        }
        if(model->ball_visible)
            ball_visible = true;
        else
            ball_visible = false;
        //    if(i==10)
        //    {
        //      std::system("clear");
        //      std::cout << "fukuro_strategy\n";
        //      std::cout << "[subscriber] update : ball "
        //          << ball_visible
        //          << " pos : " << ball_x << ", \t" << ball_y
        //          << '\n'
        //          << "local : " << local_ball_x << ", \t" << local_ball_y
        //          << '\n'
        //          << "angle : " << angle << " \tradius : " << radius
        //          << '\n'
        //          << "robot pos "
        //          << robot_x << ", "
        //          << robot_y << ", "
        //          << robot_theta << '\n';
        //      i=0;
        //    }
    };

    KeypadServiceCallback keypad_cb = [&odometry_client](fukuro_common::KeypadService::Request& req, fukuro_common::KeypadService::Response& res)
    {
        fukuro_common::OdometryService odom;
        switch(req.key)
        {
        case 0 :
            robot_strategy = stop;
            break;
        case 1 : //defender start pos1 kick off teman
        {
            robot_role = defender_attack;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 2 :
        {
            robot_role = defender_full_defend;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 3 : //defender start pos2
        {
            robot_role = defender_attack;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 4 :
        {
            robot_role = defender_full_defend;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 5 : //striker start pos1
        {
            robot_role = striker;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 6 : //striker start pos2
        {
            robot_role = striker;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 7 : //striker start pos1
        {
            robot_role = striker_dribble;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 8 : //striker start pos2
        {
            robot_role = striker_dribble;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 9 : //defender start pos1 kick off lawan
        {
            robot_role = defender_attack;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_lawan;
            kick_off_lawan_t0 = Clock::now();
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 10 : //defender start pos1 kick off lawan
        {
            robot_role = defender_full_defend;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_lawan;
            kick_off_lawan_t0 = Clock::now();
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 11 : //defender start pos2 kick off lawan
        {
            robot_role = defender_attack;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_lawan;
            kick_off_lawan_t0 = Clock::now();
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 12 : //defender start pos2 kick off lawan
        {
            robot_role = defender_full_defend;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_lawan;
            kick_off_lawan_t0 = Clock::now();
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 13 : //striker start pos1 kick off lawan
        {
            robot_role = striker;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_lawan;
            kick_off_lawan_t0 = Clock::now();
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 14 : //striker start pos2 kick off lawan
        {
            robot_role = striker;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_lawan;
            kick_off_lawan_t0 = Clock::now();
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 15 : //striker start pos1 kick off lawan
        {
            robot_role = striker_dribble;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_lawan;
            kick_off_lawan_t0 = Clock::now();
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 16 : //striker start pos2 kick off lawan
        {
            robot_role = striker_dribble;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_lawan;
            kick_off_lawan_t0 = Clock::now();
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 17 : //goalie start pos1
        {
            robot_role = goalie;
            robot_start = start_pos_1;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            goalie_homing1_waypoint_counter = 0;
            odom.request.x = START_POS_1_X;
            odom.request.y = START_POS_1_Y;
            odom.request.w = START_POS_1_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 18 : //goalie start pos2
        {
            robot_role = goalie;
            robot_start = start_pos_2;
            robot_strategy = go_to_home;
            kick_off = kick_off_teman;
            goalie_homing2_waypoint_counter = 0;
            odom.request.x = START_POS_2_X;
            odom.request.y = START_POS_2_Y;
            odom.request.w = START_POS_2_W;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        case 19 : //goalie home
        {
            robot_role = goalie;
            robot_start = home;
            robot_strategy = goalie_exe;
            odom.request.x = -4.4;
            odom.request.y = 0.0;
            odom.request.w = 0.0;
            bool ret = odometry_client.call(odom);
            if(ret)
                std::cout << "[KeypadCallback] Reset Odometry OK\n";
            else
                std::cout << "[KeypadCallback] Reset Odometry failed\n";
        }
            break;
        }
        res.res = 1;
        return true;
    };
    CompassCallback cmps_cb = [](const fukuro_common::Compass::ConstPtr& compass)
    {
        cmps = compass->cmps;
    };

    ros::Subscriber world_model_sub = node.subscribe(robot_name+"/world_model",10,wm_cb);
    ros::Subscriber compass_sub = node.subscribe(robot_name+"/compass",10,cmps_cb);
    ros::ServiceServer keypad_service_server = node.advertiseService(robot_name+"/keypad_service",keypad_cb);
    kick_client = node.serviceClient<fukuro_common::Shoot>(robot_name+"/kick_service");
    dynamic_reconfigure::Server<fukuro_strategy::FukuroStrategyConfig> reconfigure_server;
    reconfigure_server.setCallback(configure);
    vel_pub = node.advertise<fukuro_common::VelCmd>(robot_name+"/vel_cmd",10);
    dribbler_pub = node.advertise<fukuro_common::DribblerControl>(robot_name+"/dribbler",10);

    std::thread strategy_thread(&strategy_loop);
    std::thread control_thread(&control_loop);
    ros::AsyncSpinner spinner(2);

    //  ros::spin();
    spinner.start();
    ros::waitForShutdown();
    strategy_loop_running = false;
    control_loop_running = false;
    if(strategy_thread.joinable())
        strategy_thread.join();
    if(control_thread.joinable())
        control_thread.join();
    return 0;
}

#endif
