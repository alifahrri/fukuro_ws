#ifndef MAP_H
#define MAP_H

#include <mutex>
#include <ros/ros.h>
#include <map>
#include "fukuro_common/WorldModel.h"
#include "fukuro_common/StrategyPositioning.h"

#include "fukuro/core/field.hpp"

class GridMap
{
public:
  GridMap(ros::NodeHandle node_);
  void updateWorld(const fukuro_common::WorldModel::ConstPtr &world);
  void process();
  void publish();
private:
  struct Point
  {
    Point() { x = 0.0; y = 0.0;}
    Point(double x_, double y_) { x = x_; y = y_; }
    double x;
    double y;
  };

  Point robot_pos;
  ros::Publisher strategy_positioning_pub;
  std::vector<Point> obstacles;
  std::vector<Point> kick_pos_test_point;
  std::vector<Point> kick_target_test_point;
  std::mutex mutex;
  double ball_shield_angle;
  double kick_angle;
  Point kick_target;
};

#endif // MAP_H
