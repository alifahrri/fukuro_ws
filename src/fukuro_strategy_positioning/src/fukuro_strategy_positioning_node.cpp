#include <ros/ros.h>
#include "map.h"

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_strategy_positioning");
  ros::NodeHandle node;
  std::string robot_name;
  if(!ros::get_environment_variable(robot_name,"FUKURO"))
  {
    ROS_ERROR("Robot name empty!");
    exit(-1);
  }
  GridMap grid_map(node);
  ros::Subscriber world_model_sub = node.subscribe(robot_name+"/world_model",30,
                                                   &GridMap::updateWorld,&grid_map);
  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::Rate rate(30);
  while(ros::ok())
  {
    grid_map.process();
    grid_map.publish();
    rate.sleep();
  }
  return 0;
}
