#include <ros/ros.h>
#include <fukuro_common/BallPrediction3d.h>
#include <fukuro_common/InterceptPoint.h>

namespace fukuro {

  struct LinearModel {
    double m = 0.0;
    double c = 0.0;
    double operator()(double x);
    double inversed(double y);
  };

  class GoalieInterceptor {
  public:
    GoalieInterceptor(ros::NodeHandle &node);
    ~GoalieInterceptor();
    void update(const fukuro_common::BallPrediction3d::ConstPtr &msg);
    void publish();
  private:
    ros::Publisher pub;
    LinearModel ball_x;
    LinearModel ball_y;
    double last_y_pos = 0.0;
    double intercept_line = -565.0;
    double max_y_line = 110.0;
    double time = 5.0;
  };

  double LinearModel::operator()(double x)
  {
    double y = m*x + c;
    return y;
  }

  double LinearModel::inversed(double y)
  {
    double x = (y-c)/m;
    return x;
  }

  GoalieInterceptor::GoalieInterceptor(ros::NodeHandle &node)
  {
    std::string robot_name;
    ros::get_environment_variable(robot_name,"FUKURO");
    auto topic_name = robot_name + "/goalie_intercept";
    pub = node.advertise<fukuro_common::InterceptPoint>(topic_name, 3);
    double xline;
    double yline;
    double t;
    if(ros::param::get("/intercept_line",xline))
      intercept_line = xline;
    if(ros::param::get("/max_y_line",yline))
      max_y_line = yline;
    if(ros::param::get("/intercept_time",t))
      time = t;
  }

  GoalieInterceptor::~GoalieInterceptor()
  {

  }

  void GoalieInterceptor::update(const fukuro_common::BallPrediction3d::ConstPtr &msg)
  {
    ball_x.c = msg->ax;
    ball_x.m = msg->bx;
    ball_y.c = msg->ay;
    ball_y.m = msg->by;
    ROS_INFO("regression : x(m:%.2f,c:%.2f) y(m:%.2f,%.2f)", ball_x.m, ball_x.c, ball_y.m, ball_y.c);
    publish();
  }

  void GoalieInterceptor::publish()
  {
    fukuro_common::InterceptPoint msg;
    auto intercept_time = -0.0;
    msg.intercept_point.x = intercept_line;
    msg.intercept_point.y = 0.0;
    if(fabs(ball_x.m) > 0.0) {
        intercept_time = ball_x.inversed(intercept_line);
        auto now = ros::Time::now();
        auto valid = !std::isnan(intercept_time);
        msg.valid = valid;
        if(valid) {
            auto intercept_y_pos = ball_y(intercept_time);
            if((intercept_time>0.0) && (intercept_time<time)) {
                msg.intercept_time.data = ros::Time(intercept_time);
                msg.intercept_point.x = intercept_line;
                msg.intercept_point.y = intercept_y_pos;
                if(msg.intercept_point.y > max_y_line)
                  msg.intercept_point.y = max_y_line;
                else if(msg.intercept_point.y < -max_y_line)
                  msg.intercept_point.y = -max_y_line;
              }
              else {
                msg.intercept_point.y = last_y_pos;
              }
          }
        else {
          msg.intercept_point.y = last_y_pos;
        }
      }
    ROS_INFO("intercept : point(%.2f,%.2f,%.2f) time(%.2f) %d, intercept_time : %.2f",
             msg.intercept_point.x, msg.intercept_point.y, msg.intercept_point.theta, msg.intercept_time.data.toSec(), msg.valid, intercept_time);
    pub.publish(msg);
    last_y_pos = msg.intercept_point.y ;
  }

}

int main(int argc, char** argv)
{
  ros::init(argc,argv,"fukuro_goalie_interceptor");
  ros::NodeHandle node;
  std::string robot_name;
  if(!ros::get_environment_variable(robot_name,"FUKURO")) {
    ROS_ERROR("Robot name empty!");
    exit(-1);
  }
  std::string robot;
  ros::get_environment_variable(robot,"FUKURO");
  fukuro::GoalieInterceptor goalie(node);
  ros::Subscriber sub = node.subscribe(robot+"/ball_prediction3d", 1, &fukuro::GoalieInterceptor::update, &goalie);
  ros::spin();
  return 0;
}
