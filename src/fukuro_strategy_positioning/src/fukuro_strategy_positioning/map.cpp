#include "map.h"
#include "geometry_msgs/Point.h"
#include "fukuro_common/Point2d.h"

GridMap::GridMap(ros::NodeHandle node_)
{
  strategy_positioning_pub = node_.advertise<fukuro_common::StrategyPositioning>("/strategy_pos",10);
  kick_target.x = DEFAULT_KICK_TARGET_X;
  kick_target.y = DEFAULT_KICK_TARGET_Y;
}

void GridMap::updateWorld(const fukuro_common::WorldModel::ConstPtr &world)
{
  mutex.lock();
  obstacles.clear();
  robot_pos.x = world->pose.x;
  robot_pos.y = world->pose.y;
  for(size_t i=0; i<world->obstacles.size(); i++)
  {
    auto obs = world->obstacles.at(i);
    if((fabs(obs.x)<MAX_OBSTACLE_X) && (fabs(obs.y)<MAX_OBSTACLE_Y))
      obstacles.push_back(Point(obs.x,obs.y));
  }
  mutex.unlock();
}

void GridMap::process()
{
  Point vector(0.0,0.0);
  mutex.lock();
  Point* nearest_obstacles = NULL;
  double nearest_obstacles_dis(0.0);
  if(obstacles.size())
  {
    nearest_obstacles = &(obstacles.at(0));
    double dx = nearest_obstacles->x-DEFAULT_KICK_TARGET_X;
    double dy = nearest_obstacles->y-DEFAULT_KICK_TARGET_Y;
    nearest_obstacles_dis = sqrt(dx*dx+dy*dy);
  }
  for(size_t i=0; i<obstacles.size(); i++)
  {
    vector.x -= 3.3/(obstacles.at(i).x-robot_pos.x);
    vector.y -= 3.3/(obstacles.at(i).y-robot_pos.y);
    double dx = obstacles.at(i).x-DEFAULT_KICK_TARGET_X;
    double dy = obstacles.at(i).y-DEFAULT_KICK_TARGET_Y;
    double dis = sqrt(dx*dx+dy*dy);
    if(dis<nearest_obstacles_dis)
    {
      nearest_obstacles = &(obstacles.at(i));
      nearest_obstacles_dis = dis;
    }
  }
  if(nearest_obstacles)
  {
    if(nearest_obstacles_dis<GOAL_WIDTH/2.0)
    {
      double kick_target_dx = kick_target.x-nearest_obstacles->x;
      double kick_target_dy = kick_target.y-nearest_obstacles->y;
      double kick_target_dist = sqrt(kick_target_dx*kick_target_dx+
                                     kick_target_dy*kick_target_dy);
      if(kick_target_dist<0.35)
      {
        if(nearest_obstacles->y>0)
          kick_target.y = KICK_TARGET_2_Y;
        else
          kick_target.y = KICK_TARGET_1_Y;
      }
    }
  }
  double robot_dx = robot_pos.x - PENALTY_KICK_POS_X;
  double robot_dy = robot_pos.y;
  double robot_dist = sqrt(robot_dx*robot_dx+robot_dy*robot_dy);
  double dx = kick_target.x - robot_pos.x;
  double dy = kick_target.y - robot_pos.y;
  if(robot_dist<1.0)
  {
    vector.x += 33.0/(dx);
    vector.y += 33.0/(dy);
  }
  kick_angle = atan2(dy,dx);
  mutex.unlock();
  ball_shield_angle = atan2(vector.y,vector.x);
}

void GridMap::publish()
{
  fukuro_common::StrategyPositioning strategy_pos_msg;
  strategy_pos_msg.ball_shield_angle = ball_shield_angle;
  strategy_pos_msg.kick_angle = kick_angle;
  strategy_pos_msg.kick_target.x = kick_target.x;
  strategy_pos_msg.kick_target.y = kick_target.y;
  strategy_positioning_pub.publish(strategy_pos_msg);
}
